/* global analytics Vue Spark _ */
/* eslint-disable import/no-extraneous-dependencies, import/no-unresolved */

/*
 |--------------------------------------------------------------------------
 | Laravel Spark Bootstrap
 |--------------------------------------------------------------------------
 |
 | First, we will load all of the "core" dependencies for Spark which are
 | libraries such as Vue and jQuery. This also loads the Spark helpers
 | for things such as HTTP calls, forms, and form validation errors.
 |
 | Next, we'll create the root Vue application for Spark. This will start
 | the entire application and attach it to the DOM. Of course, you may
 | customize this script as you desire and load your own components.
 |
 */

require('./spark/spark-bootstrap');
require('./components/bootstrap');

const spark = require('./spark/spark');
const { capitalize } = require('./lib/lodash-helper');

import GridItem from './components/vue-grid-layout/GridItem.vue';
import GridLayout from './components/vue-grid-layout/GridLayout.vue';

// identify user and track page
(() => {
  if (Spark.userId) { // if the user is logged, identify him first
    try {
      const { user } = Spark.state;
      const communityMember = user.community_member || {};
      analytics.identify(Spark.userId, {
        // Standardized traits
        createdAt: user.created_at.split(' ').join('T'),
        username: user.email,
        lastName: capitalize(user.lastname),
        firstName: capitalize(user.firstname),
        name: user.fullname,
        email: user.email.toLowerCase(),
        phone: user.telephone || undefined,
        country: user.country || undefined,
        title: _.startCase(user.title) || undefined,
        company: _.startCase(user.company) || undefined,
        industry: _.startCase(user.sector) || undefined,
        website: communityMember.website || undefined,
        // Custom traits
        'User Phone': user.telephone || undefined,
        'User Country Code': user.country || undefined,
        'User Title': _.startCase(user.title) || undefined,
        'User Company Name': _.startCase(user.company) || undefined,
        'User Company Industry': _.startCase(user.sector) || undefined,
        'User Type': _.startCase(user.user_type) || undefined,
        'User Community Type': _.startCase(communityMember.type) || undefined,
        'User Community Name': _.startCase(communityMember.name) || undefined,
        'User Website': communityMember.website || undefined,
        'User Plans': _.map(user.subscriptions, 'name'),
        'User Portfolios Count': user.portfoliosCount || 0,
        'User Symphony Account': user.hasSymphonyAccount || false,
      });
    } catch (e) {
      console.error(e);
    }
  }
  analytics.page(); // then track the loaded page
})();

Vue.component('grid-layout', GridLayout);
Vue.component('grid-item', GridItem);
// Vue.component('vue-html-editor', require('./vue-html-editor'));

Spark.forms.register = {
  firstname: '',
  lastname: '',
  telephone: '',
  company: '',
  title: '',
};

const store = require('./store');

window.app = new Vue({ // eslint-disable-line
  store,
  mixins: [spark],
});
