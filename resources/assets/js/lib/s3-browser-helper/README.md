# s3-browser-helper

Helper to upload files into S3 from a browser.

## Dependencies
- aws-sdk
- moment

## Factory parameters
- opt, environment variables { UPLOAD_BUCKET, UPLOAD_FOLDER, API_BASE_URL }
- http, HTTP client

## Usage
```
const s3HelperFactory = require('../../lib/s3-browser-helper');

const s3Helper = s3HelperFactory(env, this.$http);

s3Helper.getS3WithCredentials({
  sessionId: this.user.id,
})
.then(s3 => s3Helper.uploadDataURL(s3, {
  Body: newPostImage,
  Key: `news/${this.item.id}`,
  ExpiresInDays: 90,
}))
.then(({ Location }) => {
  console.info('Public URL:', Location);
})
.catch(e => console.error(e));
```
