/* global atob Blob */
const S3 = require('aws-sdk/clients/s3');
const moment = require('moment');

const defaultOptions = {
  region: 'us-east-1',
};

const defaultParams = {
  CacheControl: 'no-cache', // check server before using cache
};

/**
 * S3 Helper Factory
 * @param  {Object} opt  options { UPLOAD_BUCKET, UPLOAD_FOLDER, API_BASE_URL }
 * @param  {Object} http http client
 * @return {Object}      helper functions
 */
module.exports = (opt, http) => ({

  /**
   * Get a new Blob from data URL
   * @param  {String} [dataURL=''] data URL
   * @return {Blob}                blob with MIME type
   */
  dataURLtoBlob(dataURL = '') {
    // separate desc and data components
    const [desc, data] = dataURL.split(',');
    // convert base64/URLEncoded data component to raw binary data held in a string
    const isBase64 = desc.includes('base64');
    const bytes = isBase64 ? atob(data) : unescape(data);
    // separate out the MIME component
    const type = desc.split(':')[1].split(';')[0];
    // write the bytes of the string to a typed array
    const ia = new Uint8Array(bytes.length);
    for (let i = 0; i < bytes.length; i++) {
      ia[i] = bytes.charCodeAt(i);
    }
    return new Blob([ia], { type });
  },

  /**
   * Get a credentials from v1/upload/credentials api
   * @param  {Str/Num} sessionId min length 2, unique per user, e.g. userId, email, username
   * @return {Promise}           credentials or error
   */
  getCredentials({ sessionId } = {}) {
    return http.get(`${opt.API_BASE_URL}/v1/upload/credentials`, {
      params: { sessionId },
    }).then(res => res.data);
  },

  /**
   * Get options with credentials
   * @param  {Object}  [params={}]  getCredentials params
   * @param  {Object}  [options={}] S3 constructor options
   * @return {Promise}              options or error
   * @see #getCredentials(Object)
   * @see http://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/S3.html#constructor-property
   */
  getOptionsWithCredentials(params = {}, options = {}) {
    return new Promise((resolve, reject) => {
      if (!options.credentials || options.credentials.expired === true
      || (options.credentials.expireTime
      && moment(options.credentials.expireTime).isSameOrBefore(moment()))) {
        return this.getCredentials(params)
          .then((credentials) => {
            options.credentials = credentials;
            return resolve(options);
          })
          .catch(err => reject(err));
      }
      return resolve(options);
    });
  },

  /**
   * Get a new S3 instance with credentials
   * @param  {Object}  [params={}]  getCredentials params
   * @param  {Object}  [options={}] S3 constructor options
   * @return {Promise}              S3 instance or error
   * @see #getCredentials(Object)
   * @see http://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/S3.html#constructor-property
   */
  getS3WithCredentials(params = {}, options = {}) {
    return new Promise((resolve, reject) => {
      options = Object.assign({}, defaultOptions, options);
      return this.getOptionsWithCredentials(params, options)
        .then(options => resolve(new S3(options)))
        .catch(err => reject(err));
    });
  },

  /**
   * Upload a Body to a s3 bucket
   * @param  {Object}  [s3={}]     an s3 instance with credentials
   * @param  {Object}  [params={}] upload params
   * @return {Promise}             response data { Location, Bucket, Key } or error
   * @see http://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/S3.html#upload-property
   * @see ExpiresInDays, additionnal upload param which computes Expires param from days
   */
  upload(s3 = {}, params = {}) {
    return new Promise((resolve, reject) => {
      try {
        params = Object.assign({ Bucket: opt.UPLOAD_BUCKET }, defaultParams, params);
        params.Key = `${opt.UPLOAD_FOLDER}/${params.Key}`;
        if (params.ExpiresInDays) {
          params.Expires = moment.utc().startOf('day').add(params.ExpiresInDays, 'days').toDate();
        }
        s3.upload(params, (err, data) => {
          if (err) reject(err);
          else {
            data.Location = data.Location.replace('s3.amazonaws.com/', '');
            resolve(data);
          }
        });
      } catch (e) {
        reject(e);
      }
    });
  },

  /**
   * Upload a data URL Body to a s3 bucket
   * @param  {Object}  [s3={}]     an s3 instance with credentials
   * @param  {Object}  [params={}] upload params
   * @return {Promise}             response data { Location, Bucket, Key } or error
   */
  uploadDataURL(s3 = {}, params = {}) {
    return new Promise((resolve, reject) => {
      try {
        const blob = this.dataURLtoBlob(params.Body);
        params.Body = blob;
        params.Key += `.${blob.type.split('/')[1]}`;
        params.ContentType = blob.type;
        return this.upload(s3, params)
          .then(data => resolve(data))
          .catch(err => reject(err));
      } catch (e) {
        reject(e);
      }
    });
  },

});
