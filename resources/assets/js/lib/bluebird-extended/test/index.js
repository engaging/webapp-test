/* eslint-disable prefer-const, no-unused-vars */
const assert = require('assert');
const BbPromise = require('..');

(async function test() {
  const promises = [
    new Promise(resolve => resolve('Resolved native promise')),
    new Promise((resolve, reject) => reject(new Error('Rejected native promise'))),
    BbPromise.resolve('Resolved bluebird promise'),
    BbPromise.reject(new Error('Rejected bluebird promise')),
  ];


  console.info('Test settleAll with native and bluebird promises: 2 resolved and 2 rejected');
  let inspections = await BbPromise.settleAll(promises);
  assert(inspections.length === 4, 'Should be an array of length 4');
  assert(inspections[0].isFulfilled(), 'Should be bulfilled');
  assert(inspections[0].value() === 'Resolved native promise');
  assert(inspections[1].isRejected(), 'Should be rejected');
  assert(inspections[1].reason().message === 'Rejected native promise');
  assert(inspections[2].isFulfilled(), 'Should be bulfilled');
  assert(inspections[2].value() === 'Resolved bluebird promise');
  assert(inspections[3].isRejected(), 'Should be rejected');
  assert(inspections[3].reason().message === 'Rejected bluebird promise');
  console.info('Test settleAll OK');


  const params = [
    'Resolved promise',
    new Error('Rejected promise'),
  ];
  let promise = param => new Promise((resolve, reject) => {
    if (param instanceof Error) reject(param);
    else resolve(param);
  });


  console.info('Test settleMap with native promises: 1 resolved and 1 rejected');
  inspections = await BbPromise.settleMap(params, promise);
  assert(inspections.length === 2, 'Should be an array of length 2');
  assert(inspections[0].isFulfilled(), 'Should be bulfilled');
  assert(inspections[0].value() === 'Resolved promise');
  assert(inspections[1].isRejected(), 'Should be rejected');
  assert(inspections[1].reason().message === 'Rejected promise');
  console.info('Test settleMap OK');


  promise = param => new BbPromise((resolve, reject) => {
    if (param instanceof Error) reject(param);
    else resolve(param);
  });


  console.info('Test settleMap with bluebird promises: 1 resolved and 1 rejected');
  inspections = await BbPromise.settleMap(params, promise);
  assert(inspections.length === 2, 'Should be an array of length 2');
  assert(inspections[0].isFulfilled(), 'Should be bulfilled');
  assert(inspections[0].value() === 'Resolved promise');
  assert(inspections[1].isRejected(), 'Should be rejected');
  assert(inspections[1].reason().message === 'Rejected promise');
  console.info('Test settleMap OK');


  const promiseProps = {
    a: new Promise(resolve => resolve('Resolved native promise')),
    b: new Promise((resolve, reject) => reject(new Error('Rejected native promise'))),
    c: BbPromise.resolve('Resolved bluebird promise'),
    d: BbPromise.reject(new Error('Rejected bluebird promise')),
  };


  console.info('Test settleProps with native and bluebird promises: 2 resolved and 2 rejected');
  let inspectionProps = await BbPromise.settleProps(promiseProps);
  assert(Object.keys(inspectionProps).length === 4, 'Should be an object with 4 props');
  assert(inspectionProps.a.isFulfilled(), 'Should be bulfilled');
  assert(inspectionProps.a.value() === 'Resolved native promise');
  assert(inspectionProps.b.isRejected(), 'Should be rejected');
  assert(inspectionProps.b.reason().message === 'Rejected native promise');
  assert(inspectionProps.c.isFulfilled(), 'Should be bulfilled');
  assert(inspectionProps.c.value() === 'Resolved bluebird promise');
  assert(inspectionProps.d.isRejected(), 'Should be rejected');
  assert(inspectionProps.d.reason().message === 'Rejected bluebird promise');
  console.info('Test settleProps OK');


  inspections = await BbPromise.settleAll(promises);


  console.info('Test reduceInspections with native and bluebird promises: 2 resolved and 2 rejected');
  const { values, reasons } = BbPromise.reduceInspections(inspections);
  assert(values.length === 2, 'Should be an array of length 2');
  assert(reasons.length === 2, 'Should be an array of length 2');
  assert(values[0] === 'Resolved native promise');
  assert(values[1] === 'Resolved bluebird promise');
  assert(reasons[0].message === 'Rejected native promise');
  assert(reasons[1].message === 'Rejected bluebird promise');
  console.info('Test reduceInspections OK');
}());
