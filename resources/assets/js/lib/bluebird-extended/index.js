const Promise = require('bluebird/js/release/promise')();

/**
 * Settle all input by reflection
 * @see http://bluebirdjs.com/docs/api/promise.all.html
 * @see http://bluebirdjs.com/docs/api/reflect.html
 * @see http://bluebirdjs.com/docs/api/promiseinspection.html
 * @param  {Ite|Pro} input an iterable (e.i. array) or a promise of iterable
 * @return {Promise}       a promise of array of PromiseInspection objects
 */
Promise.settleAll = async input => (
  Promise.all(input.map(p => Promise.resolve(p).reflect()))
);

/**
 * Settle map input into mapper by reflection
 * @see http://bluebirdjs.com/docs/api/promise.map.html
 * @see http://bluebirdjs.com/docs/api/reflect.html
 * @see http://bluebirdjs.com/docs/api/promiseinspection.html
 * @param  {Ite|Pro}  input   an iterable (e.i. array) or a promise of iterable
 * @param  {Function} mapper  a mapper function
 * @param  {Object}   options options, defaults to {concurrency: int=Infinity}
 * @return {Promise}          a promise of array of PromiseInspection objects
 */
Promise.settleMap = async (input, mapper, options) => (
  Promise.map(input, i => Promise.resolve(mapper(i)).reflect(), options)
);

/**
 * Settle props of input by reflection
 * @see http://bluebirdjs.com/docs/api/promise.props.html
 * @see http://bluebirdjs.com/docs/api/reflect.html
 * @see http://bluebirdjs.com/docs/api/promiseinspection.html
 * @param  {Obj|Pro} input an object|map or a promise of object|map
 * @return {Promise}       a promise with PromiseInspection objects as props
 */
Promise.settleProps = async input => (
  Promise.props(Object.keys(input).reduce((newInput, key) => {
    newInput[key] = Promise.resolve(input[key]).reflect();
    return newInput;
  }, {}))
);

/**
 * Reduce iterable of PromiseInspection objects to an object of values and reasons
 * @see http://bluebirdjs.com/docs/api/promiseinspection.html
 * @param  {Ite|Pro} inspections an iterable (e.i. array) or a promise of iterable,
 *                                of PromiseInspection objects
 * @return {Object}              an object {values: array=[], reasons: array=[]}
 */
Promise.reduceInspections = inspections => (
  inspections.reduce((output, inspection) => {
    if (inspection.isFulfilled()) {
      output.values.push(inspection.value());
    } else if (inspection.isRejected()) {
      output.reasons.push(inspection.reason());
    }
    return output;
  }, { values: [], reasons: [] })
);

module.exports = Promise;
