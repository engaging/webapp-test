const split = require('lodash/split');
const toLower = require('lodash/toLower');
const startCase = require('lodash/startCase');
const capitalize = require('lodash/capitalize');

/**
 * Converts the first character of each word to upper case and the remaining to lower case
 *
 * @param  {String} string the string to capitalize
 * @return {String}        the capitalized string
 */
module.exports.capitalize = string => split(string, ' ').map(capitalize).join(' ');

/**
 * Converts string to title case
 *
 * @param  {String} string the string to convert
 * @return {String}        the title cased string
 */
module.exports.titleCase = string => startCase(toLower(string));

/**
 * Checks case insensitive strings
 *
 * @param  {Function} checks the checks function
 * @param  {String}   value  the value to check
 * @param  {String}   other  the other value to check
 * @return {Boolean}
 */
module.exports.checksInsensitive = (checks, value, other) =>
  checks(toLower(value), toLower(other));

/**
 * Return case insensitive checks function
 *
 * @param  {Function} checks the checks function
 * @param  {String}   other  the other value to check
 * @return {Function}        the function expecting the value to check
 */
module.exports.checksInsensitiveFactory = (checks, other) => value =>
  checks(toLower(value), toLower(other));

/**
 * Return case insensitive iterate function
 *
 * @param  {String}   key the iterate key
 * @return {Function}     the function expecting the obj to iterate over
 */
module.exports.iterateInsensitiveFactory = key => obj => toLower(obj[key]);

/**
 * Sort object keys
 *
 * @param  {Object} obj the object to sort
 * @return {Object}     new object with sorted keys
 */
module.exports.sortKeys = (obj) => {
  const sorted = {};
  Object.keys(obj).sort().forEach((key) => {
    sorted[key] = obj[key];
  });
  return sorted;
};
