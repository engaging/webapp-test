var fs = require('fs'),
    args = require('system').args,
    page = require('webpage').create();

page.settings.webSecurityEnabled = false;
// page.paperSize = {format: 'A4', orientation: 'portrait', margin: '1cm'};
page.viewportSize = {
  width: 1600,
  height: 1200 
};
page.open(args[1], function (status) {
    if (status !== 'success') {
        console.log('Unable to load the file!');
        fs.write(args[2], 'fail', 'w');
        phantom.exit(1);
    } else {
      var script1 = "function(){ window.generatePdf(); }";
      // var script2 = "function(){ console.log(window.phantomVar); }";
      window.setTimeout(function () {
        page.evaluateJavaScript(script1);
        window.setTimeout(function () {
        fs.write(args[2], page.content, 'w');
        phantom.exit(0);
        }, 1000);
      }, 5000);
    }
});