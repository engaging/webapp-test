/* global Vue */
const URL = {
  BASE: '/api/communities',
};

const getCommunity = () =>
  Vue.http.get(URL.BASE);

const memberRequest = id =>
  Vue.http.get(`${URL.BASE}/${id}/connection_requests`);

const acceptRequest = (id, request) =>
  Vue.http.put(`${URL.BASE}/${id}/connection_accept`, {
    id: request.id,
  });

const rejectRequest = (id, request) =>
  Vue.http.put(`${URL.BASE}/${id}/connection_reject`, {
    id: request.id,
  });

module.exports = {
  getCommunity,
  memberRequest,
  acceptRequest,
  rejectRequest,
};
