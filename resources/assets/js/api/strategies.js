/* global Vue */
const URL = {
  BASE: '/api/strategies',
  FILTERS: '/api/strategies/filters',
  CORRELATION: '/api/strategies/correlation',
};

/**
 * Load strategies
 *
 * @param  {Object} payload payload options
 *  [full=false]    {Boolean} full static info if true
 *  [metrics=false] {Boolean} attach metrics if true
 *  ids             {Array}   strategies ids
 *  only            {Array}   only strategy codes
 *  ignore          {Array}   ignore strategy codes
 *  providers       {Array}   providers names
 *  assetClasses    {Array}   asset classes
 *  factors         {Array}   factors
 *  currencies      {Array}   currencies
 *  regions         {Array}   regions
 *  styles          {Array}   styles
 *  returnTypes     {Array}   return types
 *  types           {Array}   types: Strategy|Benchmark|Private Strategy
 *  keyword         {String}  keyword
 *  from            {String}  updated from date
 *  to              {String}  updated to date
 *  periodKeys      {Array}   override default periodKeys
 *  ignoreCache     {Boolean} not use any cache (only for Private Strategy and custom periods)
 *  reCache         {Boolean} not fetch from cache but store the result in cache
 * @return {Array}
 */
const loadStrategies = payload =>
  Vue.http.get(URL.BASE, {
    params: payload,
  });

/**
 * Load strategies filters
 *
 * @see loadStrategies for params description
 * @param  {Object} payload payload options
 * @return {Object}         strategies filters
 */
const loadFilters = payload =>
  Vue.http.get(URL.FILTERS, {
    params: payload,
  });

/**
 * Load one strategy by id
 *
 * @param  {Number} id      strategy id
 * @param  {Object} payload payload options
 *  [volMeasure='30d'] {String} volatility measure: 30d|60d|120d
 *  maxDate            {String} override default maxDate
 *  periods            {Object} override default periods
 *  ignoreCache        {Boolean} not use any cache
 *  reCache            {Boolean} not fetch from cache but store the result in cache
 * @return {Object}
 */
const loadOneStrategy = (id, payload) =>
  Vue.http.get(`${URL.BASE}/${id}`, {
    params: payload,
  });

/**
 * Load volatility track for given strategy id, period and measure
 *
 * @param  {Number} id      strategy id
 * @param  {Object} payload payload options
 *  [period='5y']   {String} volatility period
 *  [measure='30d'] {String} volatility measure: 30d|60d|120d
 *  maxDate         {String} override default maxDate
 *  periods         {Object} override default periods
 * @return {Array}          volatility track
 */
const loadOneVolatility = (id, payload) =>
  Vue.http.get(`${URL.BASE}/${id}/volatility-track`, {
    params: payload,
  });

/**
 * Load benchmarks correlation for given strategy id and period
 *
 * @param  {Number} id      strategy id
 * @param  {Object} payload payload options
 *  ids           {Array}  benchmarks ids
 *  [period='5y'] {String} volatility period
 *  maxDate       {String} override default maxDate
 *  periods       {Object} override default periods
 * @return {Object}         benchmarks correlation
 */
const loadOneCorrelation = (id, payload) =>
  Vue.http.get(`${URL.BASE}/${id}/correlation`, {
    params: payload,
  });

/**
 * Load strategies correlation
 *
 * @param  {Object} payload payload options
 *  ids           {Array}  strategies ids
 *  [period='5y'] {String} volatility period
 *  maxDate       {String} override default maxDate
 *  periods       {Object} override default periods
 * @return {Object}         strategies correlation
 */
const loadStrategiesCorrelation = payload =>
  Vue.http.get(`${URL.CORRELATION}`, {
    params: payload,
  });

/**
  * Load strategy periods
  *
  */
const loadPeriods = id =>
  Vue.http.get(`${URL.BASE}/${id}/periods`);

module.exports = {
  loadStrategies,
  loadFilters,
  loadOneStrategy,
  loadOneVolatility,
  loadOneCorrelation,
  loadStrategiesCorrelation,
  loadPeriods,
};
