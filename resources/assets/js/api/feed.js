/* global Vue */
const URL = {
  BASE: 'api/feed',
};

const searchFeed = payload =>
  Vue.http.post(URL.BASE, payload);

const deleteFeedById = id =>
  Vue.http.delete(`${URL.BASE}/${id}`);

const putFeedById = (id, payload) =>
  Vue.http.put(`${URL.BASE}/${id}`, payload);

const postFeedById = (id, payload) =>
  Vue.http.post(`${URL.BASE}/${id}`, payload);

module.exports = {
  searchFeed,
  deleteFeedById,
  putFeedById,
  postFeedById,
};
