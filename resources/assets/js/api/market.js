/* global Vue */
const URL = {
  BASE: '/api/market',
};

const getMarket = payload =>
  Vue.http.post(URL.BASE, payload);

module.exports = {
  getMarket,
};
