/* global Vue Spark */
const URL = {
  BASE_LOAD: '/api/portfolios',
  BASE_MUTATE: '/api/portfolio',
};

/**
 * Load portfolios
 *
 * @param  {Object} payload payload options
 *  [metrics=false] {Boolean} attach metrics if true
 *  [shares=false]  {Boolean} attach shares if true
 *  periodKeys      {Array}   override default periodKeys
 * @return {Array}
 */
const loadPortfolios = payload =>
  Vue.http.get(URL.BASE_LOAD, {
    params: payload,
  });

/**
 * Load one portfolio by id
 *
 * @param  {Number} id      portfolio id
 * @param  {Object} payload payload options
 *  [shares=false]     {Boolean} attach shares if true
 *  [volMeasure='30d'] {String}  volatility measure: 30d|60d|120d
 *  maxDate            {String}  override default maxDate
 *  periods            {Object}  override default periods
 *  ignoreCache        {Boolean} not use any cache
 *  reCache            {Boolean} not fetch from cache but store the result in cache
 * @return {Object}
 */
const loadOnePortfolio = (id, payload) =>
  Vue.http.get(`${URL.BASE_LOAD}/${id}`, {
    params: payload,
  });

/**
 * Load one portfolio options by id
 *
 * @param  {Number} id      portfolio id
 * @param  {Object} payload payload options
 *  maxDate            {String}  override default maxDate
 *  periods            {Object}  override default periods
 *  rebalancing        {String}  override default rebalancing
 *  rebalancing_month  {String}  override default rebalancing_month
 *  rebalancing_date   {String}  override default rebalancing_date
 * @return {Object}
 */
const loadOnePortfolioOption = (id, payload) =>
  Vue.http.get(`${URL.BASE_LOAD}/${id}/options`, {
    params: payload,
  });

const loadOnePortfolioAllocation = (id, payload) =>
  Vue.http.post(`${URL.BASE_LOAD}/${id}/allocation`, payload);

const loadOnePortfolioFunding = (id, payload) =>
  Vue.http.post(`${URL.BASE_LOAD}/${id}/funding`, payload);
/**
 * Load volatility track for given portfolio id, period and measure
 *
 * @param  {Number} id      portfolio id
 * @param  {Object} payload payload options
 *  [period='5y']   {String} volatility period
 *  [measure='30d'] {String} volatility measure: 30d|60d|120d
 *  maxDate         {String} override default maxDate
 *  periods         {Object} override default periods
 * @return {Array}          volatility track
 */
const loadOneVolatility = (id, payload) =>
  Vue.http.get(`${URL.BASE_LOAD}/${id}/volatility-track`, {
    params: payload,
  });

/**
 * Load portfolio indices for given portfolio id
 *
 * @param  {Number} id      portfolio id
 * @param  {Object} payload payload options
 *  [metrics=false] {Boolean} attach metrics if true
 *  periodKeys      {Array}   override default periodKeys
 * @return {Array}          portfolio indices
 */
const loadPortfolioIndices = (id, payload) =>
  Vue.http.get(`${URL.BASE_LOAD}/${id}/strategies`, {
    params: payload,
  });

const createPortfolio = createPortfolioForm =>
  Spark.post(URL.BASE_MUTATE, createPortfolioForm);

const addToPortfolio = (id, indices, mode, period) => {
  const url = `${URL.BASE_MUTATE}/${id}`;
  return Vue.http.put(url, {
    indices,
    mode,
    period,
  });
};

const loadOneDraft = slug =>
  Vue.http.get(`${URL.BASE_LOAD}/${slug}/draft`);

/**
  * Load portfolio periods
  *
  */
const loadPeriods = id =>
  Vue.http.get(`${URL.BASE_LOAD}/${id}/periods`);

const putPortfolio = (id, {
  mode = '',
  title = '',
  indices = [],
  allocation = {},
  pins = '',
  weighting = '',
  period = '',
} = {}) =>
  Vue.http.put(`${URL.BASE_MUTATE}/${id}`, {
    allocation,
    indices,
    title,
    mode,
    pins,
    weighting,
    period,
  });

/**
 * Load benchmarks correlation for given portfolio id and period
 *
 * @param  {Number} id      portfolio id
 * @param  {Object} payload payload options
 *  ids                   {Array}   benchmarks ids
 *  [period='5y']         {String}  volatility period
 *  [portfolioOnly=false] {Boolean} ignore portfolio constituents if true
 *  maxDate               {String}  override default maxDate
 *  periods               {Object}  override default periods
 * @return {Object}         benchmarks correlation
 */
const loadOneCorrelation = (id, payload) =>
  Vue.http.get(`${URL.BASE_LOAD}/${id}/correlation`, {
    params: payload,
  });

module.exports = {
  loadPortfolios,
  loadOnePortfolio,
  loadOnePortfolioOption,
  loadOnePortfolioAllocation,
  loadOnePortfolioFunding,
  loadOneDraft,
  loadOneVolatility,
  loadPeriods,
  putPortfolio,
  createPortfolio,
  addToPortfolio,
  loadPortfolioIndices,
  loadOneCorrelation,
};
