/* global Vue */
const URL = {
  BASE: '/api/pca',
};

/**
 * Load one portfolio pca by id
 *
 * @param  {Number} id      portfolio/strategy id
 * @param  {Object} payload payload options
 *  covType            {String} 'Covariance' | 'Correlation
 *  returnInterval     {String} Return Interval
 *  listFilters        {Array}  List of Market Beta
 *  filter             {String} Type to filter the date
 *  maxDate            {String} override default maxDate
 *  periods            {Object} override default periods
 *  period             {String} only one period choose
 * @return {Object}
 */
const loadOnePca = (id, payload) =>
  Vue.http.post(`${URL.BASE}/${id}`, payload);

/**
 * Load one portfolio pca filterd dates by id
 *
 * @param  {Number} id      portfolio/strategy id
 * @param  {Object} payload payload options
 *  covType            {String} 'Covariance' | 'Correlation
 *  returnInterval     {String} Return Interval
 *  listFilters        {Array}  List of Market Beta
 *  filter             {String} Type to filter the date
 *  maxDate            {String} override default maxDate
 *  periods            {Object} override default periods
 *  period             {String} only one period choose
 * @return {Array}
 */
const loadOneDate = (id, payload, vueInstance) =>
  Vue.http.post(`${URL.BASE}/${id}/dates`, payload, {
    before(request) {
      // abort previous request, if exists
      if (vueInstance.previousRequest) {
        vueInstance.previousRequest.abort();
      }
      // set previous request on Vue instance
      vueInstance.previousRequest = request;
    },
  });


/**
 * Set portfolio options
 *
 * @param  {Number} id      portfolio id
 * @param  {Object} payload payload options
 */
const setPcaOptions = (id, payload) =>
  Vue.http.post(`${URL.BASE}/${id}/options`, payload);

/**
 * Get portfolio options
 *
 * @param  {Number} id      portfolio id
 * @return {Object}
 */
const getPcaOptions = id =>
  Vue.http.get(`${URL.BASE}/${id}/options`);

module.exports = {
  loadOnePca,
  loadOneDate,
  setPcaOptions,
  getPcaOptions,
};
