import { apiUrl } from "../types/base";
import { colorPayload } from "../types/api";

/* global Vue */

const url: apiUrl = {
  BASE: '/api/colors',
};

const getColors = (payload: colorPayload) =>
  window.Vue.http.get(url.BASE, {
    params: payload,
  });

module.exports = {
  getColors,
};
