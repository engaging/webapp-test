/* global Vue */
const URL = {
  BASE: '/api/regression',
};

/**
 * Load one portfolio regression by id
 *
 * @param  {Number} id      portfolio/strategy id
 * @param  {String} type    'portfolio' | 'strategy'
 * @param  {Object} payload payload options
 *  movingWindow       {String}  Moving Window
 *  modelInterval      {String}  Model Interval
 *  returnInterval     {String}  Return Interval
 *  selection          {Number}  Top N regression used
 *  metricsOnly        {Boolean} Need track or not
 *  maxDate            {String}  override default maxDate
 *  periods            {Object}  override default periods
 *  period             {String}  only one period choose
 * @return {Object}
 */
const loadOneRegression = (id, type, payload) =>
  Vue.http.post(`${URL.BASE}/${type}/${id}`, payload);

/**
 * Load one portfolio heatmap by id
 *
 * @param  {Number} id      portfolio/strategy id
 * @param  {String} type    'portfolio' | 'strategy'
 * @param  {Object} payload payload options
 *  movingWindow       {String}  Moving Window
 *  modelInterval      {String}  Model Interval
 *  returnInterval     {String}  Return Interval
 *  maxDate            {String}  override default maxDate
 *  periods            {Object}  override default periods
 *  period             {String}  only one period choose
 * @return {Object}
 */
const loadOneHeatmap = (id, type, payload) =>
  Vue.http.post(`${URL.BASE}/${type}/${id}/heatmap`, payload);

/**
 * Set portfolio/strategy options
 *
 * @param  {Number} id      portfolio/strategy id
 * @param  {String} type    'portfolio' | 'strategy'
 * @param  {Object} payload payload options
 */
const setRegressionOptions = (id, type, payload) =>
  Vue.http.post(`${URL.BASE}/${type}/${id}/options`, payload);

/**
 * Get portfolio/strategy options
 *
 * @param  {Number} id      portfolio/strategy id
 * @param  {String} type    'portfolio' | 'strategy'
 * @return {Object}
 */
const getRegressionOptions = (id, type) =>
  Vue.http.get(`${URL.BASE}/${type}/${id}/options`);

module.exports = {
  loadOneRegression,
  loadOneHeatmap,
  setRegressionOptions,
  getRegressionOptions,
};
