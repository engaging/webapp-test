/* global Vue */
const URL = {
  BASE: '/api/filters',
};

const getFilters = () =>
  Vue.http.get(URL.BASE);

module.exports = {
  getFilters,
};
