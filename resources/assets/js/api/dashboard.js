/* global Vue */
const URL = {
  SAVE: '/api/dashboard/save',
};

const save = value =>
  Vue.http.post(URL.SAVE, value);

module.exports = {
  save,
};
