/* global Vue env */
const URL = {
  BASE: `${env.API_BASE_URL}/v1/news`,
};

const getNews = () =>
  Vue.http.get(URL.BASE);

const getNewsById = id =>
  Vue.http.get(`${URL.BASE}/${id}`);

const editNewsById = (id, payload) =>
  Vue.http.patch(`${URL.BASE}/${id}`, payload);

module.exports = {
  getNews,
  getNewsById,
  editNewsById,
};
