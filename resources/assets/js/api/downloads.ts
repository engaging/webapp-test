
type ExcelPayload = {
  period?: string;
  periods?: {
    [period: string]: string;
  }
}

type DownloadExcelPayload = {
  id: string | number;
  type: 'strategy' | 'portfolio';
  tool: string;
  payload: ExcelPayload;
}


/* global Vue */


const downloadExcel = ({
  id = '',
  type = 'strategy',
  tool = 'backtesting',
  payload = {},
}: DownloadExcelPayload) => {
  let url:string = '';
  switch (type) {
    case 'strategy':
      url = `/strategies/${id}/export/${tool}/excel`;
      break;
    case 'portfolio':
      url = `/portfolios/${id}/export/${tool}/excel`;
      break;
  }
  return window.Vue.http.get(url, {
    params: payload,
    responseType: 'arraybuffer',
  });
}

module.exports = {
  downloadExcel,
};