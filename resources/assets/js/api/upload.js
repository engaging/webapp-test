/* global Vue env */
const URL = {
  BASE: `${env.API_BASE_URL}/v1/upload/data-url`,
};

const savePhoto = payload =>
  Vue.http.post(URL.BASE, payload);

module.exports = {
  savePhoto,
};
