/* global Vue */
const URL = {
  BASE: '/api/preference',
};

const setPreference = (type, payload) =>
  Vue.http.post(`${URL.BASE}/${type}`, payload);

module.exports = {
  setPreference,
};
