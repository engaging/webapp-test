/* global window document analytics Vue $ _ */
const ACTION = require('../store/mutation-type');
const apiPortfolios = require('../api/portfolios');
const apiStrategies = require('../api/strategies');
const { basePrefs } = require('../util/config');
const { checksInsensitive } = require('../lib/lodash-helper');
const { metrics, models } = require('../util/config');

const getDefaultData = () => (
  {
    portfoliosLoaded: false,
    indices: [],
    portfolios: [],
    strategy: '', // remove vue warning.
    portfolio: '', // remove vue warning,
    indicesLoaded: false,
    loadingExport: false,
    showBenchmarkMenu1: -1,
    period: '5 Year',
    weightingOptions: models,
    periodKeysForFilters: {
      '10 Year': '10y',
      '5 Year': '5y',
      '3 Year': '3y',
      '1 Year': '1y',
      '6 Month': '6m',
    },
    dataTableCols: metrics,
    tableCols_benchmarks_show: [
      'return',
      'volatility',
      'sharpe',
      'skewness',
      'var',
      'mdd',
    ],
    dataTableColsMax: 6,
    // might have to set groupName:color so the graphs are all consistent
    assetClassColors: {},
    categoryColors: {},
    dataTableColsCount: 6,
    groupType: 'asset',
    groupTypeNames: {
      asset: 'Allocation by Asset Class',
      type: 'Allocation by Factor',
    },
    groupTypesShow: -1,
    sortorder: -1, //desc. datatable order
    sortby: 'title',

    showPortfolioMenu: null,
    showStrategyMenu: null,
    // sharing: null,
    // sharingTitle: '',
    // shareEmails: null,
    // shareCurrent: [],
    // shareResult: '',
    // shareLoading: 0,
    // portfoliosShared: [],
    // loadedAjax: 0,
    showList: '1', // 1=show my portfolios , 2=show those shared with me
    allowSharing: false,
  }
);

Vue.component('portfolios', {
  props: ['user'],

  data() {
    return getDefaultData();
  },
  computed: {
    device() {
      if ($(window).width() <= 768) {
        return 'mobile';
      }
      return 'desktop';
    },
  },
  mounted() {
    $('body').on('click', (e) => {
      if ($('.opened').length) {
        const target = e.toElement || e.target;
        if ($(target).parents('.opened').length === 0) {
          this[$('.opened').data('var')] = -1;
        }
      }
    });
    window.eventBus.$on('removedPortfolio', this.removedPortfolioHandler);
    window.eventBus.$on('renamePortfolio', this.renamePortfolioHandler);
    window.eventBus.$on('duplicatePortfolio', this.duplicatePortfolioHandler);
    window.eventBus.$on('privateStrategiesNoRecache', this.getIndices);
    window.eventBus.$on('privateStrategiesRecache', this.getSingleIndice);
  },
  created() {
    this.getColors();
    this.loadPortfolios();
    this.getIndices();
  },

  watch: {
    portfolios() {
      try {
        analytics.track('Viewed Mylab', _.assign({
          'Mylab Portfolios Count': this.portfolios.length || 0,
          'Mylab Strategies Count': _.sumBy(this.portfolios, 'strategiesCount'),
        }, _.fromPairs(_.map(this.portfolios, (o, idx) => ([
          `Mylab Portfolio${idx + 1} Count`, o.strategiesCount || 0,
        ])))));
      } catch (e) {
        console.error(e);
      }
    },
    groupType() {
      this.groupTypesShow = -1;
    },
    tableCols_benchmarks_show() {
      this.dataTableColsCount = this.tableCols_benchmarks_show.length;
    },
  },

  methods: {
    showAlert(type, message) {
      return this.$store.dispatch(ACTION.SHOW_ALERT_MODAL, { type, message });
    },
    async getIndices() {
      const payload = {
        types: ['Private Strategy'],
        metrics: true,
      };
      this.indicesLoaded = false;
      this.indices = await apiStrategies.loadStrategies(payload).then(o => o.data);
      this.indicesLoaded = true;
    },
    async getSingleIndice({ id = 0 }) {
      const payload = {
        reCache: true,
      };
      this.indicesLoaded = false;
      const idx = this.indices.findIndex(o => o.id === id);
      const data = await apiStrategies.loadOneStrategy(id, payload).then(o => o.data);
      Vue.set(this.indices, idx, data);
      this.indicesLoaded = true;
    },
    removedPortfolioHandler(removedId) {
      const idx = this.portfolios.findIndex(o => o.id === removedId);
      this.portfolios.splice(idx, 1);
    },
    renamePortfolioHandler({ id, title }) {
      const idx = this.portfolios.findIndex(o => o.id === id);
      Vue.set(this.portfolios[idx], 'title', title);
    },
    duplicatePortfolioHandler() {
      this.loadPortfolios();
    },
    async getColors() {
      const fetechData = await this.$store.dispatch(ACTION.API_FETCH_COLORS, {});
      this.assetClassColors = fetechData.assetClass;
      this.categoryColors = fetechData.factor;
    },
    async loadPortfolios() {
      // portfolios you own
      this.portfoliosLoaded = false;
      this.portfolios = await apiPortfolios.loadPortfolios({ metrics: true }).then(o => o.data);
      this.portfoliosLoaded = true;
    },

    openStrategyMenu(id) {
      if (this.showStrategyMenu == id) {
        this.showStrategyMenu = null;
      } else {
        this.showStrategyMenu = id;
      }
    },

    openPortfolioMenu(id) {
      if (this.showPortfolioMenu == id) {
        this.showPortfolioMenu = null;
      } else {
        this.showPortfolioMenu = id;
      }
    },

    sortItems(a, b) {
      if (['title', 'shortName', 'lastDate'].includes(this.sortby)) {
        if (checksInsensitive(_.lt, a[this.sortby], b[this.sortby])) { return -1 * this.sortorder; }
        if (checksInsensitive(_.gt, a[this.sortby], b[this.sortby])) { return 1 * this.sortorder; }
        return 0;
      }
      if (a[this.sortby] === undefined) {
        return -1 * this.sortorder;
      }
      if (b[this.sortby] === undefined) {
        return 1 * this.sortorder;
      }
      return (parseFloat(a[this.sortby]) - parseFloat(b[this.sortby])) * this.sortorder;
    },

    changeSortby(sortby) {
      if (this.sortby === sortby) {
        this.sortorder = this.sortorder * -1;
      } else {
        this.sortorder = -1;
      }
      this.sortby = sortby;
      this.portfolios = this.portfolios.sort(this.sortItems);
    },

    changeSortbyStrategy(sortby) {
      if (this.sortby === sortby) {
        this.sortorder = this.sortorder * -1;
      } else {
        this.sortorder = -1;
      }
      this.sortby = sortby;
      this.indices = this.indices.sort(this.sortItems);
    },

    openDuplicatePortfolio(portfolio) {
      window.eventBus.$emit('editPortfolio', {
        portfolioId: portfolio.id,
        portfolioTitle: `copy of ${portfolio.title}`,
        mode: 'duplicate',
      });
    },

    openEditPortfolio(portfolio) {
      window.eventBus.$emit('editPortfolio', {
        portfolioId: portfolio.id,
        portfolioTitle: portfolio.title,
        mode: 'edit',
      });
    },

    openEditStrategy(strategy) {
      const payload = {};
      if (!strategy) {
        payload.mode = 'new';
      } else {
        const details = {
          assetClass: strategy.assetClass,
          factor: strategy.factor,
          shortName: strategy.shortName,
          id: strategy.id,
          code: strategy.code,
        };
        payload.mode = 'edit';
        payload.strategy = details;
      }
      window.eventBus.$emit('privateStrategies', payload);
    },


    // openSharePortfolio(portfolio) {
    //   this.sharing = portfolio;
    //   this.sharingTitle = portfolio.title;

    //   this.updateShareCurrent();

    //   $('#sharePortfolio').modal('show');
    // },

    // updateShareCurrent() {
    //   // load the shareCurrent list for this portfolio (gotta write an ajax)
    //   const that = this;
    //   that.shareCurrent = [];
    //   this.$http.get('/api/portfolios/sharing', {
    //     element_id: that.sharing.id,
    //     element_type: 'portfolio',
    //   })
    //     .then((response) => {
    //       that.shareCurrent = response.data;
    //     });
    // },

    // copySharedPortfolio(portfolioId) {
    //   // copy a portfolio that was shared with you
    //   swal(
    //     {
    //       title: 'Are you sure you want to share a copy of this portfolio?',
    //       text: 'This action cannot be undone.',
    //       type: 'warning',
    //       showCancelButton: true,
    //       confirmButtonClass: 'btn-danger',
    //       confirmButtonText: 'Yes, share it',
    //     },
    //     async (isConfirm) => {
    //       if (isConfirm) {
    //         this.portfoliosLoaded = false;
    //         this.showList = 1;
    //
    //         const { data } = await this.$http.post('/api/portfolios/sharing', {
    //           id: portfolioId,
    //           mode: 'copy',
    //         });
    //         if (data !== []) {
    //           console.info('done copying shared');
    //           this.loadPortfolios();
    //           this.portfoliosLoaded = true;
    //         }
    //       }
    //     },
    //   );
    // },
    // addShare() {
    //   const that = this;
    //   if (this.sharing != null && this.shareEmails != '') {
    //     this.shareLoading = 1;
    //     const url = '/api/portfolios/sharing';
    //     this.$http.post(url, {
    //       id: that.sharing.id,
    //       email: that.shareEmails,
    //       mode: 'add',
    //     })
    //       .then((response) => {
    //         that.shareLoading = 2;
    //         that.shareResult = response.data.sent;
    //         that.shareCurrent = [];
    //         that.shareEmails = '';

    //         that.updateShareCurrent();
    //       });
    //   } else {
    //     swal('Whoops!', 'Pick a portfolio and enter addresses to share with.', 'error');
    //   }
    // },

    // removeShare(id) {
    //   const that = this;
    //   const url = '/api/portfolios/sharing';
    //   this.$http.post(url, {
    //     id,
    //     mode: 'delete',
    //   })
    //     .then((response) => {
    //       console.log(response);
    //       that.updateShareCurrent();
    //       that.sharing.shares = that.sharing.shares - 1;
    //     });
    // },

    setPeriod(period) {
      this.period = period;
    },

    exportPdf(to) {
      const $pdfInput = $('#pdfinput input[name=html]').val('');

      let prefcss = '';
      const prefs = Object.assign({}, basePrefs, _.pickBy(this.user, _.negate(_.isEmpty)));

      if (prefs.logoImage.length) {
        const imageUrl = `background-image:url(${prefs.logoImage})`;
        $('.print-header .print-border-image').attr('style', imageUrl);
      }

      prefcss += "<style type='text/css'>";
      prefcss += `.navbar-inverse.user { background:${prefs.navbarColor}; color: ${prefs.navbarTextColor}; }\n`;
      prefcss += `.print-border-right { border-color: ${prefs.navbarLineColor}; }\n`;
      prefcss += `h4.print-border { color:${prefs.headerColor}; border-color:${prefs.headerLineColor};}\n`;
      prefcss += '.print-hide { display: none !important; }\n';
      prefcss += '</style>';


      let html = document.getElementById('print-container').innerHTML;
      const printheader = document.getElementById('print-header').innerHTML;
      const header = `<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /><link href="${window.location.origin}/css/print.css" rel="stylesheet">${prefcss}</head><body>`;//dont use fintech.css ugh overflow/page break issues
      const footer = '</body></html>';


      html = html.replace(/<div class="print-header"><\/div>/g, printheader);
      html = header + html + footer;

      const $pdfForm = $('#pdfinput');
      $pdfForm.unbind('submit');
      $pdfForm.submit((e) => {
        e.preventDefault();
        switch (to) {
          case 'download':
            $pdfInput.val(html);
            $pdfForm.unbind('submit');
            $pdfForm.submit();
            break;
          default: {
            const action = $pdfForm.prop('action');
            this.loadingExport = true;
            this.$http.post(`${action}?to=${to}`, { html })
              .then(() => {
                this.loadingExport = false;
                return this.showAlert('success', `The PDF has been exported to ${_.startCase(to)}`);
              })
              .catch((e) => {
                console.error(e);
                this.loadingExport = false;
                this.showAlert('danger', '<strong>Whoops!</strong> Something went wrong! Please retry or contact support on <a href="mailto:admin@engagingenterprises.co" class="alert-link">admin@engagingenterprises.co</a>.');
              });
          }
        }
      });
      $('#pdfinput').submit();
    },

  },
});
