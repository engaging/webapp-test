const apiCommunity = require('../../api/community');

Vue.component('usernetwork', {
  props: ['user'],
  data() {
    return {
      Limit: 5,
      providerData: [],
      assetManagerData: [],
      showFilters: true,
    };
  },
  methods: {
    toggleSidebar() {
      this.showFilters = !this.showFilters;
    },

    async loadCommunity() {
      const { sponsors = [], assetManagers = [] } = await apiCommunity.getCommunity()
        .then(o => o.data);
      const userCommunityMemberId = this.user.community_member && this.user.community_member.id;
      const keepOthers = a => a.id !== userCommunityMemberId;
      const byName = (a, b) => {
        const x = (a.name || '').toLowerCase();
        const y = (b.name || '').toLowerCase();
        if (x < y) return -1;
        if (x > y) return 1;
        return 0;
      };
      this.providerData = sponsors
        .filter(keepOthers)
        .sort(byName);
      if (this.user.user_type === 'admin') { // asset managers viewed by admin only
        this.assetManagerData = assetManagers
          .filter(keepOthers)
          .sort(byName);
      }
    },
  },
  mounted() {
    if ($(window).width() <= 768) {
      this.showFilters = false;
    }
    this.loadCommunity();

    $(window).on('resize', () => {
      $('.sidebar').height($(window).height());
      $('.scrolling').height($('.sidebar').height() - 100);
    });
    $(window).trigger('resize');

    $(window).on('scroll', () => {
      const top = $(window).scrollTop();
      if (top > $('.navbar').height()) {
        $('.sidebar').width($('.sidebar').width()).addClass('fixed');
      } else {
        $('.sidebar').removeClass('fixed');
      }
    });
  },
});
