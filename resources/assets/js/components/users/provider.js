/* global window analytics env FileReader Vue $ _ */
import VueCropper from 'vue-cropperjs';
import Dropzone from 'vue2-dropzone';

const map = require('lodash/fp/map');
const has = require('lodash/fp/has');
const pipe = require('lodash/fp/pipe');
const take = require('lodash/fp/take');
const head = require('lodash/fp/head');
const filter = require('lodash/fp/filter');
const orderBy = require('lodash/fp/orderBy');
const ACTION = require('../../store/mutation-type');
const { shortenReturnProps } = require('../../util/strategy');
const { checksInsensitive } = require('../../lib/lodash-helper');
const { metrics } = require('../../util/config');

Vue.component('userprovider', {
  props: ['user', 'provider_data', 'canedit'],
  test: true,
  components: { VueCropper, Dropzone },
  data() {
    return {
      //add pagination to remove warning
      pagination: {
        from: null,
        to: null,
        total: 0,
        current_page: 0,
        last_page: 0,
      },
      saving: false,
      dropzoneOptions: {
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
        },
      },
      activeEditTab: null,
      host: `${window.location.protocol}//${window.location.hostname}/`,
      coverImageTransform: 'w_1400,q_80,f_jpg/',
      loading: false,
      documentSaving: false,
      showTableOption: -1,
      maxHeight: 200,
      labels: metrics,
      dataTableCols_raw: [
        'return',
        'volatility',
        'sharpe',
      ],
      dataTableColsCount: 3,
      dataTableColsMax: 4,
      sortorder: 1, // asc
      sortby: 'shortName',
      periodKeys: {
        '6 Month': '6m',
        '1 Year': '1y',
        '3 Year': '3y',
        '5 Year': '5y',
        '10 Year': '10y',
      },
      periodKeysForFilters: {
        '10 Year': '10y',
        '5 Year': '5y',
        '3 Year': '3y',
        '1 Year': '1y',
        '6 Month': '6m',
      },
      period: '5 Year',
      metric: 'sharpe',
      topSize: 10,
      addingStrategy: {},

      provider: {},
      provider_loaded: false,
      colorsAll: {},

      shareOptions: {
        all: 'All Community',
        assetManagement: 'Asset Management',
        bank: 'Bank',
        hedgeFund: 'Hedge Fund',
        insurancePension: 'Insurance & Pension',
        investmentConsulting: 'Investment Consulting',
        privateBank: 'Private Bank & Wealth Management',
      },
      shareOptionsActive: [],

      // showShareOptions: -1,
      newPostTitle: '',
      newPostDescription: '',
      newPostImage: null,
      tags: [],
      tagsActive: [],
      showTagOptions: -1,

      newDocumentTitle: '',
      newDocumentDescription: '',
      newDocumentWebsite: '',
      newDocumentLocked: 0,
      newDocumentFile: null,
      newDocumentFilename: null,
      leftDrawdown: {},
      rightDrawdown: {},

      indices: [], //shown indices
      indicesIds: [], //shown idices ids
      indicesIdsTemp: [], //in editmodal in cas ethey decide not to save
      indicesSearch: '',
      showLogoUpload: false,
      showBannerUpload: false,
      newName: '',
      newWebsite: '',
      newTopDescription: '',
      newTopImage: '',
      isTopImageCropped: false,
      logoPreviewImage: '',
      newDisclaimer: '',
      newDescriptionGraph: '',
      newDisclaimerGraph: '',
      newFeatureStrategy: null,
      newFeatureGraph: null,
      newFeaturePeriod: '',
      newFeatureStep: 1, // 1 is strat select, 2 is graphtype select
      newFeatureOptions: metrics,
      newFeatureGraphOptions: [
        {
          name: 'Track Record',
          image: '/community-demo/track-icon.png',
        },
        {
          name: 'Metrics',
          image: '/community-demo/radar-icon.png',
        },
        {
          name: 'Drawdown',
          image: '/community-demo/drawdown-icon.png',
        },
        {
          name: 'Monthly Return Distribution',
          image: '/community-demo/bar-icon.png',
        },
      ],
      maxNewFeatureOptions: 4,
      newFeatureOptionsActive: ['return'],
      showFeatureTableMenu: -1,
      // sort by date in api
      editingDocument: null,
      documents: [],
      documentsLoading: false,
      formData: null,
      // no loadmore? sort by date in api
      editingFeed: null,
      feedData: [],

      // graphs - graph=1straegy. will need return/vol/drawdown/mdd/sharpe/skew
      features: [],
      featuresOpt: {
        period: '5 Year',
        metric: 'return',
        assetClass: 'All Asset Classes',
        topSize: 2,
        loading: true,
      },
      assetClasses: [
        'All Asset Classes',
        'Equity',
        'Fixed Income',
        'Foreign Exchange',
        'Credit',
        'Commodity',
        'Multi Assets',
      ],
    };
  },
  watch: {
    dataTableCols_raw() {
      this.dataTableColsCount = this.dataTableCols_raw.length;
    },
    activeEditTab() {
      this.resetEditing('1');
    },
    period() {
      this.updateTopStrategies();
    },
    metric() {
      this.updateTopStrategies();
    },
    'featuresOpt.period'() {
      this.updateTopFeaturedStrategies();
    },
    'featuresOpt.metric'() {
      this.updateTopFeaturedStrategies();
    },
    'featuresOpt.assetClass'() {
      this.updateTopFeaturedStrategies();
    },
  },
  computed: {
    strategies() {
      return this.$store.state.strategies;
    },
    indicesAll() {
      return Object.values(this.strategies);
    },
  },
  methods: {
    showSuccess(file) {
      console.log('A file was successfully uploaded');
    },
    fetchDocuments() {
      const that = this;
      that.$http.get(`/api/communities/${that.provider.id}/documents`)
        .then((response) => {
          that.documents = response.data;
          that.documentsLoading = false;
        });
    },
    previewBg(e) {
      const cropper = this.$refs.cropper;
      const cropCanvas = cropper && cropper.getCroppedCanvas();
      this.cropImg = cropCanvas && cropCanvas.toDataURL();
      this.provider.bannerImage = this.cropImg;
    },
    uploadNew() {
      this.showBannerUpload = !this.showBannerUpload;
      if (this.showBannerUpload) {
        this.newPostImage = null;
      }
    },
    uploadNewLogo() {
      this.showLogoUpload = !this.showLogoUpload;
    },
    promptConnect() {
      $('#prompt_connect').modal('show');
    },
    sortIndices() {
      // sort the data instead of |orderBy sortby sortorder in template
      const that = this;
      const byField = (a, b) => {
        if (that.sortby === 'shortName') {
          if (checksInsensitive(_.lt, a[that.sortby], b[that.sortby])) { return -1; }
          if (checksInsensitive(_.gt, a[that.sortby], b[that.sortby])) { return 1; }
          return 0;
        }
        return parseFloat(a[that.sortby]) - parseFloat(b[that.sortby]);
      };
      let indices_save = that.indices.sort(byField);
      if (that.sortorder !== 1) {
        indices_save = indices_save.reverse();
      }
      that.indices = indices_save;
    },
    changeSortby(sortby) {
      if (this.sortby === sortby) {
        this.sortorder = this.sortorder * -1;
      } else {
        this.sortby = sortby;
        this.sortorder = -1;
      }
      this.sortIndices();
    },
    resetSortby() {
      this.sortorder = 1;
      this.sortby = 'shortName';
      this.sortIndices();
    },
    saveOneStrategy(id) {
      this.addingStrategy = this.strategies[id];
      $('#addStrategies').modal('show');
    },
    resetEditing(initrichtext) {
      // document dl
      this.formData = null;
      this.editingDocument = null;
      this.newDocumentTitle = '';
      this.newDocumentDescription = '';
      this.newDocumentWebsite = '';
      this.newDocumentLocked = 0;
      this.newDocumentFile = null; //might need some jquery hack to clear files...
      this.newDocumentFilename = null;
      this.logoPreviewImage = this.provider.image;
      // update/feed
      this.newPostTitle = '';
      this.newPostDescription = '';
      this.newPostImage = '';
      this.tagsActive = [];
      this.editingFeed = null;
      this.shareOptionsActive = [];
      this.showTagOptions = -1;

      // new feature graph
      this.newFeatureStep = 1;
      this.newFeatureStrategy = null;

      // main sponsor info
      this.newTopDescription = this.provider.description;
      this.newName = this.provider.name;
      this.newWebsite = this.provider.website;
      this.newTopImage = null;//this.provider.bannerImage;
      this.isTopImageCropped = false;
      this.newDisclaimer = this.provider.disclaimer;
      this.newDescriptionGraph = this.provider.descriptionGraph;
      this.newDisclaimerGraph = this.provider.disclaimerGraph;

      console.log('Reset Editing', initrichtext);
      if (initrichtext == '1') { //cant run on mounted()
        $('#topdescription-editor').wysiwyg({ toolbarSelector: '[data-target="#topdescription-editor"]' });
        $('#document-editor').wysiwyg({ toolbarSelector: '[data-target="#document-editor"]' });
        $('#disclaimer-editor').wysiwyg({ toolbarSelector: '[data-target="#disclaimer-editor"]' });
        $('#description-graph-editor').wysiwyg({ toolbarSelector: '[data-target="#description-graph-editor"]' });
        $('#disclaimer-graph-editor').wysiwyg({ toolbarSelector: '[data-target="#disclaimer-graph-editor"]' });
      }
    },
    editDocument(index) {
      this.editingDocument = index;
      const editing = _.cloneDeep(this.documents[index]);

      console.log(`edit doc${index}`, editing);

      if (editing != undefined) {
        this.newDocumentTitle = editing.title;
        $('#document-editor').html(editing.description);
        this.newDocumentWebsite = editing.website;
        this.newDocumentLocked = parseInt(editing.locked);
        this.newDocumentFile = editing.link;
        this.newDocumentFilename = (editing.link || '').split('?')[0].split('/').pop();
        this.shareOptionsActive = editing.shareWith;

        if ($('#edit_documents').length) {
          $('#edit_documents').modal('show');
        }
      }
    },
    async saveDocument() {
      const newDocumentDescription = $('#document-editor').html(); //get richtext raw
      if (this.newDocumentTitle) {
        this.documentSaving = true;
        try {
          if (this.editingDocument != null) {
            const existingDocument = this.documents[this.editingDocument];

            let { link } = existingDocument;
            if (this.newDocumentFile !== link) {
              if (this.newDocumentFile) {
                link = await this.$http.post(`${env.API_BASE_URL}/v1/upload/data-url`, {
                  Body: this.newDocumentFile,
                  Key: `document/${this.newDocumentFilename}`,
                  Bucket: 'files',
                }).then(o => o.data.Key);
              } else {
                link = '';
              }
            }

            const editing = Object.assign({}, existingDocument, {
              title: this.newDocumentTitle,
              description: newDocumentDescription,
              website: this.newDocumentWebsite,
              locked: parseInt(this.newDocumentLocked, 10),
              shareWith: this.shareOptionsActive,
              link,
            });
            console.log({ editing });
            const response = await this.$http.post(`/api/communities/${this.provider.id}/documents/${editing.id}`, editing);
            if (!response.data) { // deleted document
              return window.location.reload();
            }
            Object.assign(existingDocument, response.data);
            this.resetEditing();

            if ($('#edit_documents').length) {
              $('#edit_documents').modal('hide');
            }
          } else {
            // making new
            const today = new Date();
            const months = [
              'January', 'February', 'March', 'April', 'May', 'June',
              'July', 'August', 'September', 'October', 'November', 'December',
            ];
            const date = `${months[today.getMonth()]} ${today.getDate()} ${today.getFullYear()}`;

            let link;
            if (this.newDocumentFile) {
              link = await this.$http.post(`${env.API_BASE_URL}/v1/upload/data-url`, {
                Body: this.newDocumentFile,
                Key: `document/${this.newDocumentFilename}`,
                Bucket: 'files',
              }).then(o => o.data.Key);
            }

            const newdocument = {
              date,
              link,
              description: newDocumentDescription,
              title: this.newDocumentTitle,
              website: this.newDocumentWebsite,
              locked: this.newDocumentLocked,
              shareWith: this.shareOptionsActive,
              new: true, // ignore this prop on db save
            };

            const response = await this.$http.post(`/api/communities/${this.provider.id}/documents`, newdocument);
            this.documents.unshift(response.data);
            this.resetEditing();

            if ($('#edit_documents').length) {
              $('#edit_documents').modal('hide');
            }
          }
        } catch (e) {
          console.error(e);
        }
        this.documentSaving = false;
      }
    },
    removeDocument() {
      this.newDocumentWebsite = '';
      this.newDocumentFile = '';
      this.saveDocument();
    },
    editPost(index) {
      this.editingFeed = index;
      const editing = _.cloneDeep(this.feedData[index]);

      if (editing != undefined) {
        this.newPostTitle = editing.title;
        this.newPostDescription = editing.description;
        this.newPostImage = editing.image;

        this.shareOptionsActive = editing.shareWith;

        if ($('#edit_updates').length) {
          $('#edit_updates').modal('show');
        }
      }
    },
    async sharePost() {
      const that = this;

      // richtext test
      that.newPostDescription = $('.richtext-editor#description-editor').html();

      if (that.newPostTitle == '' || that.newPostDescription == '') {
        swal('Whoops!', 'Please enter a title and text for your new post.', 'error');
        return;
      }

      that.saving = true;

      if (that.itemBeingEdited && that.itemBeingEdited.id) { // edit an `update` feed
        let { image } = that.itemBeingEdited;
        if (this.newPostImage !== image) {
          if (this.newPostImage) {
            image = await this.$http.post(`${env.API_BASE_URL}/v1/upload/data-url`, {
              Body: this.newPostImage,
              Key: `feed/${that.itemBeingEdited.id}`,
              Bucket: 'assets',
            }).then(o => o.data.Location);
          } else {
            image = false;
          }
        }

        //save it.
        const newfeed = {
          title: this.newPostTitle,
          description: this.newPostDescription,
          shareWith: this.shareOptionsActive,
          image,
        };

        that.itemBeingEdited.title = newfeed.title;
        that.itemBeingEdited.description = newfeed.description;
        that.itemBeingEdited.shareWith = newfeed.shareOptionsActive;

        const response = await that.$http.put(`/api/feed/${that.itemBeingEdited.id}`, newfeed);
        if (this.mode === 'article') {
          window.location.reload();
        }
        that.feedData = response.data.data;
        for (const key in response.data) {
          if (key !== 'data') {
            that.pagination[key] = response.data[key];
          }
        }
      } else if (that.provider && that.provider.id) { // create an `update` feed
        const newfeed = {
          title: this.newPostTitle,
          description: this.newPostDescription,
          shareWith: this.shareOptionsActive,
        };
        const response = await that.$http.post(`/api/feed/${that.provider.id}`, newfeed);
        if (this.newPostImage) {
          const itemBeingCreated = pipe(
            filter(o => o.community_member_id === that.provider.id),
            orderBy('value', 'desc'),
            head,
          )(response.data.data);
          const image = await this.$http.post(`${env.API_BASE_URL}/v1/upload/data-url`, {
            Body: this.newPostImage,
            Key: `feed/${itemBeingCreated.id}`,
            Bucket: 'assets',
          }).then(o => o.data.Location);
          //save image.
          await that.$http.put(`/api/feed/${itemBeingCreated.id}`, { image });
        }
        window.location.reload(); // workaround: feedData don't refresh the vue
        // that.feedData = response.data.data;
        // for (const key in response.data) {
        //   if (key !== 'data') {
        //     that.pagination[key] = response.data[key];
        //   }
        // }
      } else {
        console.error('endpoint /api/feed/news deprecated !!!');
      }

      that.saving = false;
      this.resetEditing();

      if ($('#edit_updates').length) {
        $('#edit_updates').modal('hide');
      }
    },
    async getIndices() {
      // fetch provider indices
      const payload = {
        metrics: true,
        types: ['Strategy'],
        providers: [this.provider.name],
      };
      await this.$store.dispatch(ACTION.API_FETCH_STRATEGY, { payload });

      return Promise.settleAll([
        this.updateTopFeaturedStrategies(), // update top featured strategies
        this.updateTopStrategies(), // update top 10 strategies
        this.$store.dispatch(ACTION.API_FETCH_PORTFOLIO, {}), // fetch portfolios
      ]);
    },
    async updateTopFeaturedStrategies() {
      this.featuresOpt.loading = true;
      try {
        const period = this.periodKeysForFilters[this.featuresOpt.period];
        const field = `${period}_${this.featuresOpt.metric}`;
        const sortOrder = this.getSortOrder(this.metric);
        const order = (sortOrder > 0) ? 'asc' : 'desc';
        const byAssetClass = o => has(field, o)
          && ['All Asset Classes', o.assetClass].includes(this.featuresOpt.assetClass);

        const topFeaturedStrategyIds = pipe(
          filter(byAssetClass),
          orderBy(field, order),
          take(this.featuresOpt.topSize),
          map('id'),
        )(this.indicesAll);

        const ids = topFeaturedStrategyIds.filter(id => !this.strategies[id] || !this.strategies[id]['6m_track']);
        await this.$store.dispatch(ACTION.API_FETCH_STRATEGY_DETAILS, { ids });

        this.features = topFeaturedStrategyIds.map(id => ({
          graphType: 'Track Record',
          indexId: id,
          datasets: [this.strategies[id]],
          main: this.strategies[id],
          period,
        }));
      } catch (e) {
        console.error(e);
      }
      this.featuresOpt.loading = false;
    },
    async updateTopStrategies() {
      if (!this.dataTableCols_raw.includes(this.metric)) {
        if (this.dataTableCols_raw.length >= this.dataTableColsMax) {
          this.dataTableCols_raw.pop();
        }
        this.dataTableCols_raw.push(this.metric);
      }

      const period = this.periodKeysForFilters[this.period];
      this.sortby = `${period}_${this.metric}`;
      this.sortorder = this.getSortOrder(this.metric);
      const field = this.sortby;
      const order = (this.sortorder > 0) ? 'asc' : 'desc';

      this.indices = pipe(
        filter(has(field)),
        orderBy(field, order),
        take(this.topSize),
        map(shortenReturnProps),
      )(this.indicesAll);

      this.sortIndices();
    },
    getSortOrder(metric) {
      let order = -1; // desc order by default
      switch (metric) {
        case 'volatility':
        case 'kurtosis':
        case 'var':
        case 'mvar':
        case 'cvar':
          order *= -1; // inverse order
          break;
      }
      return order;
    },
    async saveTop() {
      const that = this;
      that.loading = true;

      const saveCommunityMember = (toSave, toShow) => (
        that.$http.put(`/api/communities/${that.provider.id}`, {
          fields: toSave,
        })
          .then(() => {
            Object.assign(that.provider, toSave, toShow);
            that.loading = false;
            if ($('#edit_top').length) {
              $('#edit_top').modal('hide');
            }
            if (that.provider.name !== that.provider_data.name) {
              return that.getIndices(); // if the name changed.
            }
            return that.resetEditing('1');
          })
      );

      await Promise.delay(300);

      const toSave = {};
      that.newTopDescription = $('#topdescription-editor').html(); //get richtext raw
      that.newDisclaimer = $('#disclaimer-editor').html(); //get richtext raw
      that.newDescriptionGraph = $('#description-graph-editor').html(); //get richtext raw
      that.newDisclaimerGraph = $('#disclaimer-graph-editor').html(); //get richtext raw

      that.provider.description = that.newTopDescription;
      that.provider.disclaimer = that.newDisclaimer;
      that.provider.descriptionGraph = that.newDescriptionGraph;
      that.provider.disclaimerGraph = that.newDisclaimerGraph;

      toSave.description = that.newTopDescription;
      toSave.disclaimer = that.newDisclaimer;
      toSave.descriptionGraph = that.newDescriptionGraph;
      toSave.disclaimerGraph = that.newDisclaimerGraph;
      toSave.name = that.newName;
      toSave.website = that.newWebsite;

      const toShow = {};
      const toUpload = {};
      if (that.newLogoImage && that.newLogoImage.length) {
        toShow.image = that.newLogoImage;
        toUpload.image = this.$http.post(`${env.API_BASE_URL}/v1/upload/data-url`, {
          Body: that.newLogoImage,
          Key: `image/${that.provider.id}`,
          Bucket: 'assets',
        }).then(o => o.data.Location);
      }
      if (that.newTopImage && that.newTopImage.length) {
        toShow.bannerImageOriginal = that.newTopImage;
        toUpload.bannerImageOriginal = this.$http.post(`${env.API_BASE_URL}/v1/upload/data-url`, {
          Body: that.newTopImage,
          Key: `bannerImageOriginal/${that.provider.id}`,
          Bucket: 'assets',
        }).then(o => o.data.Location);
      }
      if (that.isTopImageCropped === true) {
        that.previewBg();
        const { cropper } = that.$refs;
        const cropCanvas = cropper && cropper.getCroppedCanvas();
        const bannerImage = cropCanvas && cropCanvas.toDataURL();
        toShow.bannerImage = bannerImage;
        toUpload.bannerImage = this.$http.post(`${env.API_BASE_URL}/v1/upload/data-url`, {
          Body: bannerImage,
          Key: `bannerImage/${that.provider.id}`,
          Bucket: 'assets',
        }).then(o => o.data.Location);
      }

      if (!_.isEmpty(toUpload)) {
        const newImageLocations = await Promise.props(toUpload);
        Object.assign(toSave, newImageLocations);
      }
      return saveCommunityMember(toSave, toShow);
    },
    getColors() {
      const that = this;
      this.$http.get('/api/colors')
        .then((response) => {
          const colors = response.data.benchmarks;
          for (const x in colors) {
            const color = colors[x];
            if (color.length == 7) {
              that.colorsAll[x] = color;
            }
          }
        });
    },

    filePreview(e) {
      const that = this;
      const files = e.target.files || e.dataTransfer.files;
      if (!files.length) {
        this.newDocumentFile = null;
        return;
      }
      const file = files[0];
      const reader = new FileReader();
      reader.onload = (e) => {
        that.newDocumentFile = e.target.result;
      };
      that.showBannerUpload = false;
      reader.readAsDataURL(file);
      this.newDocumentFilename = file.name;
    },

    bannerPreview(e) {
      const that = this;
      that.newTopImage = null;
      const files = e.target.files || e.dataTransfer.files;
      if (!files.length) return;
      const file = files[0];
      const reader = new FileReader();
      reader.onload = (e) => {
        that.newTopImage = e.target.result;
      };
      that.showBannerUpload = false;
      reader.readAsDataURL(file);
    },

    logoPreview(e) {
      const that = this;
      that.newLogoImage = null;
      const files = e.target.files || e.dataTransfer.files;
      if (!files.length) return;
      const file = files[0];
      const reader = new FileReader();
      reader.onload = (e) => {
        that.logoPreviewImage = e.target.result;
        that.newLogoImage = e.target.result;
      };
      that.showLogoUpload = false;
      reader.readAsDataURL(file);
    },

    imagePreview(e) {
      const that = this;
      that.newPostImage = null;
      const files = e.target.files || e.dataTransfer.files;
      if (!files.length) return;
      const file = files[0];
      const reader = new FileReader();
      reader.onload = (e) => {
        that.newPostImage = e.target.result;
      };
      reader.readAsDataURL(file);
    },

    imageCropped() {
      this.isTopImageCropped = true;
    },

  },
  created() {
    if (this.provider_data) {
      this.provider = this.provider_data;
    }
    this.resetEditing();
    this.provider_loaded = true;
    this.getIndices();
    this.getColors();
    this.fetchDocuments();

    analytics.track(`Viewed Community ${this.provider.name}`);
  },
});
