/* global Vue Spark SparkForm */

Vue.component('symphony-account', {
  props: ['user'],
  data() {
    return {
      account: null,
      form: new SparkForm({
        email: '',
      }),
      verifyForm: new SparkForm({
        code: '',
      }),
      messages: {
        verify: 'A verification code has been sent to your Symphony account.',
        linked: 'Your Symphony account has been linked!',
      },
    };
  },
  async created() {
    await this.getAccount();
  },
  computed: {
    status() {
      if ((this.account && this.account.approved) || this.verifyForm.successful) {
        return 'linked';
      }
      if ((this.account && !this.account.approved) || this.form.successful) {
        return 'verify';
      }
      return 'unlinked';
    },
  },
  methods: {
    async getAccount() {
      const { data: account } = await this.$http.get('/api/symphony') || {};
      if (account) {
        this.account = account;
        this.form.email = account.account_email;
      }
    },
    async linkAccount() {
      this.account = await Spark.post('/api/symphony', this.form);
    },
    async verifyAccount() {
      await Spark.post('/api/symphony/verify', this.verifyForm);
    },
    async unlinkAccount() {
      await this.$http.delete(`/api/symphony/${this.account.id}`);
      this.form = new SparkForm({
        email: '',
      });
      this.verifyForm = new SparkForm({
        code: '',
      });
      this.account = null;
    },
  },
});
