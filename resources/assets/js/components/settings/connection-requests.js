const apiCommunity = require('../../api/community');

Vue.component('connection-requests', {
  props: ['user', 'communitymemberid', 'canedit', 'isowner', 'isadmin'],
  data() {
    return {
      requests: {
        pending: [],
        accepted: [],
        rejected: [],

      },
      userChecked: [],
      connectionRequestsLoaded: false,
      communitymember: null,
      showPanel: 1,
    };
  },
  mounted() {
    this.fetchData();
  },
  methods: {
    async fetchData() {
      this.connectionRequestsLoaded = false;
      this.request = await apiCommunity.memberRequest(this.communitymemberid).then(o => o.data);
      this.connectionRequestsLoaded = true;
    },
    async acceptRequest(request) {
      await apiCommunity.acceptRequest(this.communitymemberid, request);
      await this.fetchData();
    },
    async rejectRequest(request) {
      await apiCommunity.rejectRequest(this.communitymemberid, request);
      await this.fetchData();
    },
  },
});
