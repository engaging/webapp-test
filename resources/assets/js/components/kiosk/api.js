/* global Vue Spark SparkForm $ */
const ACTION = require('../../../../../resources/assets/js/store/mutation-type');
const { filterByKeyword } = require('../../../../../resources/assets/js/util/user');

const createApiKeyFormBase = {
  api_key: '',
  api_secret: '',
  api_secret_confirmation: '',
  api_scope: '',
};
const updateApiKeyFormBase = {
  api_secret: '',
  api_secret_confirmation: '',
  api_scope: '',
};
const deleteApiKeyFormBase = {
  api_key: null,
  api_secret: null,
  api_scope: null,
};

Vue.component('spark-kiosk-api', {
  props: ['user'],

  data() {
    return {
      users: [],
      rights: [],
      updatingApiKey: null,
      deletingApiKey: null,
      createApiKeyForm: new SparkForm(createApiKeyFormBase),
      updateApiKeyForm: new SparkForm(updateApiKeyFormBase),
      deleteApiKeyForm: new SparkForm(deleteApiKeyFormBase),
      keyword: '',
      autoCreateApi: false,
      findUsers: [],
    };
  },

  created() {
    this.getUsersAndRights();
  },

  watch: {
    keyword(value) {
      if (value) {
        this.findUsers = filterByKeyword(value, this.users);
      } else {
        this.findUsers = [];
      }
    },
  },

  methods: {
    showAlert(type, message) {
      return this.$store.dispatch(ACTION.SHOW_ALERT_MODAL, { type, message });
    },
    async getUsersAndRights() {
      const { data: rights = [] } = await this.$http.get('api/admin/user-rights') || {};
      this.rights = rights.filter(o => o.hasApiKey === true);
      this.users = rights.filter(o => o.hasApiKey === false && o.user).map(o => o.user);
    },
    addApiKey() {
      this.keyword = '';
      this.createApiKeyForm.api_key = '';
      $('#modal-create-api-key').modal('show');
    },
    async createApiKey(user) {
      const errors = {};
      if (!this.createApiKeyForm.api_key) {
        errors.api_key = ['The api key field is required.'];
      }
      if (!this.createApiKeyForm.api_secret) {
        errors.api_secret = ['The api secret field is required.'];
      }
      if (!this.createApiKeyForm.api_secret_confirmation) {
        errors.api_secret_confirmation = ['The api secret confirmation field is required.'];
      }
      if (Object.keys(errors).length) {
        this.createApiKeyForm.errors.set(errors);
        return;
      }
      try {
        await Spark.put(`/api/admin/users/${user.id}/right`, this.createApiKeyForm);
        await this.getUsersAndRights();
        $('#modal-create-api-key').modal('hide');
        this.showAlert('success', `API Key added for ${user.fullname}`);
        this.createApiKeyForm = new SparkForm(createApiKeyFormBase);
      } catch (e) {
        console.error(e);
        this.showAlert('danger', `Failed to add the API Key for ${user.fullname}`);
      }
    },
    editApiKey(right) {
      this.updatingApiKey = right;
      this.updateApiKeyForm.api_scope = right.api_scope;
      $('#modal-update-api-key').modal('show');
    },
    async updateApiKey() {
      try {
        await Spark.put(`/api/admin/users/${this.updatingApiKey.user.id}/right`, this.updateApiKeyForm);
        await this.getUsersAndRights();
        $('#modal-update-api-key').modal('hide');
        this.showAlert('success', `API Key updated for ${this.updatingApiKey.user.fullname}`);
        this.updateApiKeyForm = new SparkForm(updateApiKeyFormBase);
      } catch (e) {
        console.error(e);
        this.showAlert('danger', `Failed to update the API Key for ${this.updatingApiKey.user.fullname}`);
      }
    },
    approveApiKeyDelete(right) {
      this.deletingApiKey = right;
      $('#modal-delete-api-key').modal('show');
    },
    async deleteApiKey() {
      try {
        await Spark.put(`/api/admin/users/${this.deletingApiKey.user.id}/right`, this.deleteApiKeyForm);
        await this.getUsersAndRights();
        $('#modal-delete-api-key').modal('hide');
        this.showAlert('success', `API Key deleted for ${this.deletingApiKey.user.fullname}`);
        this.deleteApiKeyForm = new SparkForm(deleteApiKeyFormBase);
      } catch (e) {
        console.error(e);
        this.showAlert('danger', `Failed to delete the API Key for ${this.deletingApiKey.user.fullname}`);
      }
    },
  },
});
