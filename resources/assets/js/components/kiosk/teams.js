/* global Vue Spark SparkForm $ */
const ACTION = require('../../../../../resources/assets/js/store/mutation-type');
const { filterByKeyword } = require('../../../../../resources/assets/js/util/user');

const createTeamFormBase = {
  name: '',
  type: '',
};

Vue.component('spark-kiosk-teams', {
  props: ['user'],

  data() {
    return {
      users: [],
      teams: [],
      updatingTeam: null,
      deletingTeam: null,
      createTeamForm: new SparkForm(createTeamFormBase),
      keyword: '',
      findUsers: [],
    };
  },

  created() {
    this.getUsers();
    this.getTeams();
  },

  watch: {
    keyword(value) {
      if (value) {
        this.findUsers = filterByKeyword(value, this.users);
      } else {
        this.findUsers = [];
      }
    },
  },

  methods: {
    showAlert(type, message) {
      return this.$store.dispatch(ACTION.SHOW_ALERT_MODAL, { type, message });
    },
    async getUsers() {
      try {
        const { data: users } = await this.$http.post('/spark/kiosk/users/search') || {};
        this.users = users;
      } catch (e) {
        console.error(e);
        this.showAlert('danger', 'Failed to load users.');
      }
    },
    async getTeams() {
      try {
        const { data: teams } = await this.$http.get('/api/admin/teams') || {};
        this.teams = teams;
      } catch (e) {
        console.error(e);
        this.showAlert('danger', 'Failed to load teams.');
      }
    },
    addTeam() {
      this.createTeamForm.api_key = '';
      $('#modal-create-team').modal('show');
    },
    async createTeam() {
      const errors = {};
      if (!this.createTeamForm.name) {
        errors.name = ['The name field is required.'];
      }
      if (Object.keys(errors).length) {
        this.createTeamForm.errors.set(errors);
        return;
      }
      try {
        await Spark.post('/api/admin/teams', this.createTeamForm);
        await this.getTeams();
        $('#modal-create-team').modal('hide');
        this.showAlert('success', `${this.createTeamForm.name} team created`);
        this.createTeamForm = new SparkForm(createTeamFormBase);
      } catch (e) {
        console.error(e);
        this.showAlert('danger', `Failed to create ${this.createTeamForm.name} team`);
      }
    },
    editTeam(team) {
      this.keyword = '';
      this.updatingTeam = team;
      $('#modal-update-team').modal('show');
    },
    async createTeamUser(user) {
      try {
        await this.$http.put(`/api/admin/teams/${this.updatingTeam.id}/users/${user.id}`);
        await this.getTeams();
        this.updatingTeam = this.teams.find(o => o.id === this.updatingTeam.id);
        this.showAlert('success', `${user.fullname} added to ${this.updatingTeam.name} team`);
      } catch (e) {
        console.error(e);
        this.showAlert('danger', `Failed to add ${user.fullname} to ${this.updatingTeam.name} team`);
      }
    },
    async deleteTeamUser(user) {
      try {
        await this.$http.delete(`/api/admin/teams/${this.updatingTeam.id}/users/${user.id}`);
        await this.getTeams();
        this.updatingTeam = this.teams.find(o => o.id === this.updatingTeam.id);
        this.showAlert('success', `${user.fullname} deleted from ${this.updatingTeam.name} team`);
      } catch (e) {
        console.error(e);
        this.showAlert('danger', `Failed to delete ${user.fullname} from ${this.updatingTeam.name} team`);
      }
    },
    approveTeamDelete(team) {
      this.deletingTeam = team;
      $('#modal-delete-team').modal('show');
    },
    async deleteTeam() {
      try {
        await this.$http.delete(`/api/admin/teams/${this.deletingTeam.id}`);
        await this.getTeams();
        $('#modal-delete-team').modal('hide');
        this.showAlert('success', `${this.deletingTeam.name} team deleted`);
      } catch (e) {
        console.error(e);
        this.showAlert('danger', `Failed to delete ${this.deletingTeam.name} team`);
      }
    },
  },
});
