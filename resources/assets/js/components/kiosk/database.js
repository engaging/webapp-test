/* global Vue */
const ACTION = require('../../../../../resources/assets/js/store/mutation-type');

Vue.component('spark-kiosk-database', {
  props: ['user'],

  data() {
    return {
      cutOffDate: null,
    };
  },

  async created() {
    await this.getCutOffDate();
  },

  watch: {

  },

  methods: {
    showAlert(type, message) {
      return this.$store.dispatch(ACTION.SHOW_ALERT_MODAL, { type, message });
    },
    async getCutOffDate() {
      const { data = {} } = await this.$http.get('/api/cut-off-date') || {};
      this.cutOffDate = data.maxDate;
    },
    async updateCutOffDate() {
      try {
        const { data = {} } = await this.$http.get('/api/admin/cut-off-date/update', {
          params: {
            maxDate: this.cutOffDate || undefined,
          },
        });
        this.cutOffDate = data.maxDate;
        this.showAlert('success', `Cut-off date updated to ${
          this.cutOffDate} (Warning: does not re-generate the tracks!).`);
      } catch (e) {
        console.error(e);
        this.showAlert('danger', 'Failed to update the cut-off date.');
      }
    },
    async generateTracks(datafeed = false) {
      try {
        const { data = {} } = await this.$http.get('/api/admin/strategies/gen-tracks', {
          params: {
            maxDate: this.cutOffDate || undefined,
            datafeed,
          },
        });
        this.cutOffDate = data.maxDate;
        const message = (datafeed === true)
          ? `Running datafeed and generating tracks to ${this.cutOffDate} cut-off date (~20mn).`
          : `Generating tracks to ${this.cutOffDate} cut-off date (~10mn).`;
        this.showAlert('success', message);
      } catch (e) {
        console.error(e);
        this.showAlert('danger', 'Failed to submit the batch job.');
      }
    },
    async generateDiscover() {
      try {
        await this.$http.get('/api/admin/strategies/gen-metrics');
        this.showAlert('success', 'Generating discover (~10mn).');
      } catch (e) {
        console.error(e);
        this.showAlert('danger', 'Failed to submit the batch job.');
      }
    },
    async generateMylab() {
      try {
        await this.$http.get('/api/admin/portfolios/gen-metrics');
        this.showAlert('success', 'Mylab generated and reports sent.');
      } catch (e) {
        console.error(e);
        this.showAlert('danger', 'Failed to generate mylab.');
      }
    },
  },
});
