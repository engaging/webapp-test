Vue.component('spark-kiosk-colors', {
  props: ['user'],

  data() {
    return {
      colors: {
        color1: '#4a7c59',
        color2: '#68b0ab',
        color3: '#35a03a',
        color4: '#53ad8a',
        color5: '#7aa559',
        color6: '#9c89b8',
        color7: '#705695',
        color8: '#573b8c',
        color9: '#866487',
        color10: '#723f62',
        color11: '#dddddd',
        color12: '#cccccc',
      },
      loading: true,
      numColors: 12,
      colorsForm: false,
    };
  },
  mounted() {
    const that = this;
    that.numColors++;
    const colorsForm = function () {
      const fields = [];
      const obj = {
        assetClass: {},
        factor: {},
      };
      for (var x = 1; x <= that.numColors; x++) {
        fields.push(`color${x}`);
        obj[`color${x}`] = that.colors[`color${x}`];
      }

      for (var x in that.assetClass) {
        obj.assetClass[x] = that.assetClass[x];
      }

      for (var x in that.factor) {
        obj.factor[x] = that.factor[x];
      }

      return obj;
    };

    this.$http.get('/api/colors')
      .then((response) => {
        const colors = response.data.benchmarks;
        for (const x in colors) {
          const color = colors[x];
          if (color.length == 7) {
            that.colors[x] = color;
          }
        }
        that.assetClass = response.data.assetClass;
        that.factor = response.data.factor;
        that.colorsForm = new SparkForm(colorsForm());
        that.loading = false;
      });
  },
  methods: {
    update() {
      Spark.put('/api/colors', this.colorsForm)
        .then(() => {
          swal('Success', 'Colour scheme updated.', 'success');
        });
    },
  },
});
