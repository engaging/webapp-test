/* global Vue window moment env swal $ _ */
const ACTION = require('../../../../../resources/assets/js/store/mutation-type');
const { iterateInsensitiveFactory } = require('../../../../../resources/assets/js/lib/lodash-helper');

Vue.directive('focus', {
  // When the bound element is inserted into the DOM...
  inserted(el) {
    // Focus the element
    el.blur();
    setTimeout(() => {
      el.focus();
    }, 1000);
  },
});

Vue.component('spark-kiosk-news', {
  props: ['user'],

  data() {
    return {
      date: moment().format('YYYY-MM-DD'),
      loading: true,
      ids: '',
      filter: {
        from: moment().format('YYYY-MM-DD'),
        to: moment().format('YYYY-MM-DD'),
        source: '',
        status: ['unpublished', 'published'],
      },
      status: [
        { key: 'hidden', label: 'Delete' },
        { key: 'unpublished', label: 'Unpublish' },
        { key: 'published', label: 'Publish' },
      ],
      bulk: {
        status: null,
      },
      items: [],
      sources: [],
      listItems: [],
      keyword: '',
      stage: 'list',
      select_all_btn: false,
      currentItem: null,
      sortIndex: 'addedAt',
      sortDirection: 'asc',
    };
  },

  mounted() {
    this.searchNews();
    this.loadSources();
  },

  watch: {
    select_all_btn() {
      const that = this;
      _.forEach(that.listItems, (value, key) => {
        value.selected = that.select_all_btn;
        Vue.set(that.listItems, key, value);
      });
    },
  },

  methods: {
    showAlert(type, message) {
      return this.$store.dispatch(ACTION.SHOW_ALERT_MODAL, { type, message });
    },
    async searchNews() {
      this.loading = true;

      try {
        const { data: listItems } = await this.$http.post(`${env.API_BASE_URL}/v1/news/query`, this.filter);
        this.listItems = listItems;
        this.sortDirection = (this.sortDirection === 'asc') ? 'desc' : 'asc';
        this.sortBy(this.sortIndex);
      } catch (e) {
        console.error(e);
        this.showAlert('danger', 'Failed to search news items.');
      }
      this.loading = false;
    },
    async loadSources() {
      this.loading = true;

      try {
        const { data: sources = [] } = await this.$http.get(`${env.API_BASE_URL}/v1/news/sources`);
        this.sources = sources;
      } catch (e) {
        console.error(e);
        this.showAlert('danger', 'Failed to load sources.');
      }
      this.loading = false;
    },
    async showDetail(itemIdx) {
      _.forEach(this.listItems, (value, key) => {
        value.clicked = false;
        Vue.set(this.listItems, key, value);
      });

      this.stage = 'detail';
      let item = this.listItems[itemIdx];
      item.prev = (itemIdx > 0) ? itemIdx - 1 : null;
      item.next = (itemIdx < this.listItems.length - 1) ? itemIdx + 1 : null;
      item.clicked = true;
      this.items = [item];
      this.currentItem = item;
      setTimeout(() => {
        $(window).scrollTop(0);
      }, 50);

      if (!item.article) {
        try {
          const { data: itemDetail } = await this.$http.get(`${env.API_BASE_URL}/v1/news/${item.id}`);
          item = { ...item, ...itemDetail };
          this.items = [item];
          this.currentItem = item;

          const itemIdx = this.listItems.findIndex(o => o.id === item.id);
          if (itemIdx > -1) {
            this.listItems[itemIdx] = item;
          }
        } catch (e) {
          console.error(e);
          this.showAlert('danger', `Failed to load detail for the news id: ${item.id}.`);
        }
      }
    },
    showList() {
      this.stage = 'list';
      this.select_all_btn = false;

      const item = this.currentItem;
      if (item) {
        setTimeout(() => {
          const $item = $(`#${item.id}`);
          if ($item.length > 0) {
            $(window).scrollTop($item.offset().top - 120);
          }
        }, 50);
      }
    },
    async unpublishItem() {
      const item = this.currentItem;
      if (!item) {
        return this.showAlert('warning', 'A news should be <strong>open</strong> before trigger an action.');
      }
      if (item.status === 'unpublished') {
        return this.showAlert('warning', `News from ${item.source} <strong>already</strong> unpublished.`);
      }
      this.loading = true;

      try {
        const status = 'unpublished';
        await this.$http.patch(`${env.API_BASE_URL}/v1/news/${item.id}`, {
          status,
        });
        item.status = status;

        this.showAlert('success', `News from ${item.source} <strong>unpublished</strong>.`);
        this.showList();
      } catch (e) {
        console.error(e);
        this.showAlert('danger', `Failed to unpublish the news id: ${item.id}.`);
      }
      this.loading = false;
    },
    async publishItem() {
      const item = this.currentItem;
      if (!item) {
        return this.showAlert('warning', 'A news should be <strong>open</strong> before trigger an action.');
      }
      if (item.status === 'published') {
        return this.showAlert('warning', `News from ${item.source} <strong>already</strong> published.`);
      }
      this.loading = true;

      try {
        const status = 'published';
        await this.$http.patch(`${env.API_BASE_URL}/v1/news/${item.id}`, {
          status,
        });
        item.status = status;

        this.showAlert('success', `News from ${item.source} <strong>published</strong>.`);
        this.showList();
      } catch (e) {
        console.error(e);
        this.showAlert('danger', `Failed to publish the news id: ${item.id}.`);
      }
      this.loading = false;
    },
    async deleteItem() {
      const item = this.currentItem;
      if (!item) {
        return this.showAlert('warning', 'A news should be <strong>open</strong> before trigger an action.');
      }
      if (item.status === 'hidden') {
        return this.showAlert('warning', `News from ${item.source} <strong>already</strong> deleted.`);
      }
      swal(
        {
          title: 'Are you sure to delete this news?',
          text: 'This action cannot be undone.',
          type: 'warning',
          showCancelButton: true,
          confirmButtonClass: 'btn-danger',
          confirmButtonText: 'Yes, delete it',
        },
        async (isConfirm) => {
          if (isConfirm) {
            this.loading = true;
            try {
              const status = 'hidden';
              await this.$http.patch(`${env.API_BASE_URL}/v1/news/${item.id}`, {
                status,
              });
              item.status = status;

              const itemIdx = this.listItems.findIndex(o => o.id === item.id);
              if (itemIdx > -1) {
                this.listItems.splice(itemIdx, 1);
              }

              this.showAlert('success', `News from ${item.source} <strong>deleted</strong>.`);
              this.showList();
            } catch (e) {
              console.error(e);
              this.showAlert('danger', `Failed to delete the news id: ${item.id}.`);
            }
            this.loading = false;
          }
        },
      );
    },
    sortBy(index) {
      if (this.sortIndex === index) {
        this.sortDirection = (this.sortDirection === 'asc') ? 'desc' : 'asc';
      } else {
        this.sortDirection = 'asc';
        this.sortIndex = index;
      }
      const iterate = iterateInsensitiveFactory(index);
      this.listItems = _.orderBy(this.listItems, iterate, this.sortDirection);
    },
    // async massUpdate(ids, action) {
    //   this.loading = true;
    //
    //   try {
    //     await this.$http.post(`${env.API_BASE_URL}/v1/news/mass-edit`, {
    //       action, ids,
    //     });
    //
    //     this.showAlert('success', `${ids.length} news <strong>mass edited</strong>.`);
    //     this.showList();
    //   } catch (e) {
    //     console.error(e);
    //     this.showAlert('danger', `Failed to mass edit ${ids.length} news.`);
    //   }
    //   this.loading = false;
    // },
    // async bulkStatus() {
    //   return this.massUpdate(_.map(_.filter(this.listItems, {
    //     selected: true,
    //   }), 'id'), this.bulk.status);
    // },
  },
});
