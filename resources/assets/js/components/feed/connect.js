Vue.component('connect', {
  props: ['user', 'provider'],
  data() {
    return {
    };
  },
  watch: {
    'provider.pending': function () {
      this.toggleConnect();
    },
    'provider.following': function () {
      this.toggleConnect();
    },
  },
  methods: {
    toggleConnect() {
      const that = this;
      that.$http.post(`/api/communities/${that.provider.id}/connect`)
        .then((response) => {
          that.provider = response.data;
        });
    },
  },
});
