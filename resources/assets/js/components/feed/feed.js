/* global window document analytics env swal Vue $ _ */
const apiNews = require('../../api/news');
const apiUpload = require('../../api/upload');
const ACTION = require('../../store/mutation-type');

Vue.component('feed', {
  props: ['user', 'provider', 'canedit', 'mode', 'items'],
  data() {
    return {
      saving: false,
      loading: true,
      feedData: [],
      editingFeed: null,
      itemBeingEdited: null,
      shareOptions: {
        all: 'All Community',
        assetManagement: 'Asset Management',
        bank: 'Bank',
        hedgeFund: 'Hedge Fund',
        insurancePension: 'Insurance & Pension',
        investmentConsulting: 'Investment Consulting',
        privateBank: 'Private Bank & Wealth Management',
      },
      shareOptionsActive: [],
      newPostTitle: '',
      newPostDescription: '',
      full_article: '',
      newPostSource: '',
      newPostImage: null,
      pagination: {
        from: null,
        to: null,
        total: 0,
        current_page: 0,
        last_page: 0,
      },
      feedtypes: ['All', 'Updates', 'News'],
      feedActive: 'All',
      tags: [],
      tagsActive: [],
      keywordActive: '',
      showTagOptions: -1,
      item: {},
      currentFacNews: 0,
      scrollPos: 0,

    };
  },
  watch: {
    keywordActive: _.debounce(function (v) {
      this.searchFeed();
    }, 300),
    // tagsActive() {
    //   this.pagination.current_page = 0;
    //   this.fetchFeed();
    // },
    feedActive() {
      this.searchFeed();
    },
  },
  computed: {
    feed() {
      return this.$store.state.feed;
    },
    news() {
      return this.$store.state.news;
    },
  },
  created() {
    switch (this.mode) {
      case 'news':
        this.initNews();
        break;
      case 'updates':
        this.initUpdates();
        break;
      case 'feed':
        this.initFeed();
        break;
      case 'article':
        this.item = this.items[0];
        this.$store.commit(ACTION.API_FETCH_FEED, this.items);
        break;
      case 'preview':
        this.feedData = this.items;
        break;
    }
  },
  methods: {
    closeFacNew() {
      this.currentFacNews = 0;
      document.body.scrollTop = this.scrollPos;
    },
    searchFeed() {
      switch (this.feedActive) {
        case 'Updates':
          this.feedData = this.feed;
          break;
        case 'News':
          this.feedData = this.news;
          break;
        default:
          this.feedData = this.feed.concat(this.news);
      }
      if (this.keywordActive !== '') {
        this.feedData = _.filter(this.feedData, item => item.title.toLowerCase().indexOf(this.keywordActive.toLowerCase()) !== -1);
      }
    },
    async showFacNews(id) {
      this.scrollPos = document.body.scrollTop;
      this.loading = true;
      const index = this.news.findIndex(o => o.id === id);
      if (index > -1 && !this.news[index].article) {
        await this.$store.dispatch(ACTION.API_FETCH_NEWS_DETAILS, { id, index });
      }
      this.currentFacNews = this.news[index];
      this.loading = false;
      analytics.track('Viewed Feed', {
        'Feed Type': 'News',
        'Feed Source': this.currentFacNews.source || undefined,
      });
    },
    async initNews() {
      if (this.news.length === 0) {
        await this.$store.dispatch(ACTION.API_FETCH_NEWS);
      }
      this.feedData = this.news;
      this.loading = false;
      this.feedData = _.sortBy(this.feedData, 'created_at').reverse();
    },
    async initUpdates() {
      if (this.feed.length === 0) {
        if (!this.provider) {
          await this.$store.dispatch(ACTION.API_FETCH_FEED, {});
        } else {
          await this.$store.dispatch(ACTION.API_FETCH_FEED, {
            provider: this.provider.id,
          });
        }
      }
      this.feedData = this.feed;
      this.loading = false;
      this.feedData = _.sortBy(this.feedData, 'created_at').reverse();
      if (this.feed.length > 0) {
        this.pagination.total = 1;
      }
    },
    async initFeed() {
      const vuexActions = [
        this.$store.dispatch(ACTION.API_FETCH_FEED, {}),
        this.$store.dispatch(ACTION.API_FETCH_NEWS),
      ];
      await Promise.settleAll(vuexActions);
      this.feedData = this.feed;
      let newFeed = this.news;
      if (this.keywordActive !== '') {
        newFeed = _.filter(newFeed, item => item.title.toLowerCase().indexOf(this.keywordActive.toLowerCase()) !== -1);
      }
      this.loading = false;
      this.feedData = this.feedData.concat(newFeed);
      if (this.feed.length > 0) {
        this.pagination.total = 1;
      }
      //sort by date
      this.feedData = _.sortBy(this.feedData, 'created_at').reverse();
    },
    removePost() {
      const that = this;
      swal(
        {
          title: 'Are you sure?',
          text: 'This action cannot be undone.',
          type: 'warning',
          showCancelButton: true,
          confirmButtonClass: 'btn-danger',
          confirmButtonText: 'Yes, delete it',
        },
        (isConfirm) => {
          if (isConfirm) {
            that.removePostFuction();
          }
        },
      );
    },
    async removePostFuction() {
      this.saving = true;
      await this.$store.dispatch(ACTION.API_FETCH_FEED_DETAILS, {
        id: this.itemBeingEdited.id,
        method: 'delete',
      });
      if (this.feed.length === 0) {
        this.pagination.total = 0;
      } else {
        this.pagination.total = 1;
      }
      swal('Success', 'Your post has been deleted.', 'success');
      this.saving = false;
      this.searchFeed();
      $('#edit_updates').modal('hide');
      if (this.mode === 'article') {
        window.location.href = '/community/';
      }
    },
    editFacPost(editing) {
      const that = this;
      that.itemBeingEdited = editing;
      if (editing !== undefined) {
        setTimeout(() => {
          apiNews.getNewsById(that.itemBeingEdited.id)
            .then((o) => {
              that.itemBeingEdited = o.data;
              that.newPostImage = that.itemBeingEdited.image;
              return null;
            })
            .catch(e => console.error(e));

          if ($('#edit_fac_updates').length) {
            $('#edit_fac_updates').modal('show');
          }
        }, 200);
      }
    },

    async saveFacPhoto() {
      this.saving = true;

      let { image } = this.itemBeingEdited;
      if (this.newPostImage !== image) {
        if (this.newPostImage) {
          image = await apiUpload.savePhoto({
            Body: this.newPostImage,
            Key: `news/${this.itemBeingEdited.id}`,
            Bucket: 'assets',
            ExpiresInDays: 90,
          }).then(o => o.data.Location);
        } else {
          image = '';
        }
      }

      await apiNews.editNewsById(this.itemBeingEdited.id, { image });
      const item = this.news.find(o => o.id === this.itemBeingEdited.id);
      item.image = image; //direct change vuex object, without trigger anything.
      this.searchFeed();
      this.saving = false;
      swal('Success', 'The news has been edited.', 'success');
    },
    editPost(editing) {
      const that = this;
      that.itemBeingEdited = editing;
      if (editing !== undefined) {
        setTimeout(() => {
          that.newPostTitle = editing.title;
          that.newPostDescription = editing.description;
          if (editing.type === 'News') {
            that.full_article = editing.full_article;
            that.newPostSource = editing.source;
          }
          that.newPostImage = editing.image;
          that.shareOptionsActive = editing.shareWith;
          if ($('#edit_updates').length) {
            $('#edit_updates').modal('show');
          }

          // richtext test
          $('.richtext-editor#description-editor').wysiwyg();
          if (editing.type === 'News') {
            $('.richtext-editor#full_article-editor').wysiwyg();
          }
        }, 200);
      }
    },
    resetEditing() {
      // update/feed
      this.newPostTitle = '';
      this.newPostDescription = '';
      this.newPostImage = '';
      this.full_article = '';
      this.newPostSource = '';
      this.tagsActive = [];
      this.editingFeed = null;
      this.shareOptionsActive = [];
      this.showTagOptions = -1;
      this.itemBeingEdited = null;
    },

    imagePreview(e) {
      const that = this;
      const files = e.target.files || e.dataTransfer.files;
      if (!files.length) {
        that.newPostImage = null;
        return;
      }
      const file = files[0];
      const reader = new FileReader();
      reader.onload = (e) => {
        that.newPostImage = e.target.result;
      };
      reader.readAsDataURL(file);
    },


    async sharePost() {
      const that = this;
      if (that.newPostTitle === '' || that.newPostDescription === '') {
        swal('Whoops!', 'Please enter a title and text for your new post.', 'error');
        return;
      }

      that.saving = true;

      // richtext test
      that.newPostDescription = $('.richtext-editor#description-editor').html();
      const full_article = $('.richtext-editor#full_article-editor').html();

      if (that.itemBeingEdited && that.itemBeingEdited.id) { // edit an `update` feed
        let { image } = that.itemBeingEdited;
        if (this.newPostImage !== image) {
          if (this.newPostImage) {
            image = await apiUpload.savePhoto({
              Body: this.newPostImage,
              Key: `feed/${that.itemBeingEdited.id}`,
              Bucket: 'assets',
            }).then(o => o.data.Location);
          } else {
            image = false;
          }
        }

        //save it.
        const newfeed = {
          title: this.newPostTitle,
          description: this.newPostDescription,
          shareWith: this.shareOptionsActive,
          source: this.newPostSource,
          full_article,
          image,
        };

        that.itemBeingEdited.title = newfeed.title;
        that.itemBeingEdited.description = newfeed.description;
        that.itemBeingEdited.shareWith = newfeed.shareOptionsActive;

        await this.$store.dispatch(ACTION.API_FETCH_FEED_DETAILS, {
          id: that.itemBeingEdited.id,
          payload: newfeed,
          method: 'put',
        });
        this.pagination.total = 1;
        if (this.mode === 'article') {
          this.item = this.feed.find(o => o.id === that.itemBeingEdited.id,);
        } else {
          this.searchFeed();
        }
      } else if (that.provider && that.provider.id) { // create an `update` feed
        const newfeed = {
          title: this.newPostTitle,
          description: this.newPostDescription,
          shareWith: this.shareOptionsActive,
          source: this.newPostSource,
          full_article,
        };
        await this.$store.dispatch(ACTION.API_FETCH_FEED_DETAILS, {
          id: that.provider.id,
          payload: newfeed,
          method: 'post',
        });
        this.pagination.total = 1;
        this.searchFeed();
      } else {
        console.error('endpoint /api/feed/news deprecated !!!');
      }

      that.saving = false;
      this.resetEditing();

      if ($('#edit_updates').length) {
        $('#edit_updates').modal('hide');
      }
    },
  },
});
