
/*
 |--------------------------------------------------------------------------
 | Laravel Spark Components
 |--------------------------------------------------------------------------
 |
 | Here we will load the Spark components which makes up the core client
 | application. This is also a convenient spot for you to load all of
 | your components that you write while building your applications.
 */
/* global window document Vue */
require('./../spark-components/bootstrap');
require('./fade');
require('./filter');
require('./home');
require('./strategy');
// require('./strategy-comparison');
require('./strategy-detail');
require('./portfolio');
require('./portfolios');
// require('./analyse');
require('./sponsor-portfolio');
require('./kiosk/database');
require('./kiosk/colors');
require('./kiosk/news');
require('./kiosk/api');
require('./kiosk/teams');
require('./users/network');
require('./users/provider');
require('./feed/feed');
require('./feed/connect');
require('./settings/symphony-account');
require('./settings/connection-requests');

//restruct new chart
require('./chart/radar');
require('./chart/drawdowntrack');
require('./chart/volatilitytrack');
require('./chart/correlationscatter');
require('./chart/correlation-matrix');
require('./chart/track');
require('./chart/returnbar');
require('./chart/returnscatter');
require('./chart/pie');
require('./chart/xy');
require('./chart/contribution');
require('./chart/allocation');
require('./chart/leverage');
require('./chart/marketmonitor');
require('./chart/data');
require('./chart/portfoliobar');
require('./chart/footer-bar');
require('./chart/candlestick');
require('./chart/donut-pie');
require('./chart/stacked-bar');
require('./chart/correlation-circle');
require('./chart/gantt-chart');
require('./chart/heatmap');

import rolling from './chart/rolling.ts';
import alphatrack from './chart/alphatrack.ts';

Vue.component('chart-alphatrack', alphatrack);
Vue.component('chart-rolling', rolling);
//new chart
require('./chart/newcorrelationscatter');
require('./chart/newvolatilitytrack');

import newComparisonTable from './chart/comparisontable.vue';
import newMonitorTable from './chart/monitortable.vue';

Vue.component('monitor-table', newMonitorTable);
Vue.component('comparison-table', newComparisonTable);

require('./widgets');
require('./widget-blocks');
require('./messages');
require('./dumb/portfolio-composition');
require('./dumb/premium-svg');

//modal part
require('./modal/alert');

import privatestrategies from './modal/private-strategies.vue';
import addstrategies from './modal/add-strategies.vue';
import createPortfolio from './modal/create-portfolio.vue';
import editPortfolio from './modal/edit-portfolio.vue';
import widgetModal from './modal/widget-modal.vue';
import svgIcon from './modal/svg-icon.vue';

Vue.component('add-strategies', addstrategies);
Vue.component('private-strategies', privatestrategies);
Vue.component('create-portfolio', createPortfolio);
Vue.component('edit-portfolio', editPortfolio);
Vue.component('widget-modal', widgetModal);
Vue.component('svg-icon', svgIcon);

// lab container
import labContainer from './lab/lab-container.vue';

Vue.component('lab-container', labContainer);
