/* global window document analytics Vue AmCharts $ _ */
const ACTION = require('../store/mutation-type');
const bowser = require('bowser');
const apiPreference = require('../api/preference');
const { shortenReturnProps, byKeywordFactory, getStrategyAnalytics } = require('../util/strategy');
const { iterateInsensitiveFactory } = require('../lib/lodash-helper');
const { metrics } = require('../util/config');

if (window.AmCharts) {
  AmCharts.baseHref = true;
}

Date.prototype.yyyymmdd = function () {
  const yyyy = this.getFullYear().toString();
  const mm = (this.getMonth() + 1).toString(); // getMonth() is zero-based
  const dd = this.getDate().toString();
  return `${yyyy}-${mm[1] ? mm : `0${mm[0]}`}-${dd[1] ? dd : `0${dd[0]}`}`; // padding
};

function getDefaultData() {
  return {
    // free user test. Show premium ads and hide filters:
    warning: false,
    countActiveFilters: 0,
    loadingNewData: true,
    showFilters: false,
    showPinnedGraphics: false,
    indices: [],
    pinnedIndices: [],
    pinnedIndicesIds: [],
    pinnedIndicesMax: 5,
    indicesShowing: {},
    filters: {},
    indicesLoaded: false,
    filtersLoaded: false,
    editing: true,
    sortorder: -1, // desc
    sortby: '',
    onDisplay: 25,
    paginationSize: 50,

    // sidebar filters - show or hide
    AssetClassCheckboxInactive: true,
    RegionCheckboxInactive: true,
    CurrencyCheckboxInactive: true,
    FactorCheckboxInactive: true,
    StyleCheckboxInactive: true,
    TypeCheckboxInactive: true,
    ReturnTypeCheckboxInactive: true,
    SponsorCheckboxInactive: true,
    HistoryCheckboxInactive: true,

    newportfolioName: '',
    portfolio: '',
    strategy: '', // to remove vue warning (used in periodmenu)

    slider1: '', // volatility
    slider2: '', // return
    slider3: '', // sharpe
    sliderRange1: [0, 100],
    sliderRange2: [0, 100],
    sliderRange3: [0, 100],
    slider1Step: 1,
    slider2Step: 1,
    slider1Text: 'Volatility ',
    slider2Text: 'Return p.a. ',
    slider3Text: 'Sharpe ',
    slider1Value: '',
    slider2Value: '',
    slider3Value: '',
    slider1_slider2: false,
    slider2_slider3: false,
    slider1_slider3: false,
    showAllButton: false,

    strategyName: '',
    factors: [],
    styles: [],
    returnTypes: [],
    sponsors: [],
    history: [],
    regions: [],
    assetClasses: [],
    currencies: [],
    types: [],
    initdata: false,
    portfoliosLoaded: false,
    indicesBasket: [],

    keywords: '',
    searchTimeout: null,
    riskFreeMonthly: 1,
    period: '5 Year',
    periodKeysForFilters: {
      '10 Year': '10y',
      '5 Year': '5y',
      '3 Year': '3y',
      '1 Year': '1y',
      '6 Month': '6m',
      // '3 Month': '3m',
      // '1 Month': '1m',
    },
    // stats: {},
    statsLoaded: true,
    chartEmpty: false,
    xAxis: 'volatility',
    yAxis: 'return',
    labels: metrics,
    dataTableCols_raw: [
      'return',
      'volatility',
      'sharpe',
    ],
    dataTableColsMax: 4,
    dataTableColsCount: 3,
    showTableOption: -1,

    columnHighlight: '',
    dotColors: {
      strategy: '#123B69',
      benchmark: '#FF8935',
      'private strategy': '#996699',
      'pure factor': '#1A85A1',
    },
    pinnedColorsAll: {
      color1: '#4a7c59',
      color2: '#68b0ab',
      color3: '#35a03a',
      color4: '#53ad8a',
      color5: '#7aa559',
    },
    pinnedColorOrder: {},
    hideFilters: [], //would put volatility, return, etc i fyou wanted to hide from sidebar
    preference: {
      xAxis: 'volatility',
      yAxis: 'return',
      performance: ['return', 'volatility', 'sharpe'],
      sortBy: '',
      sortOrder: -1,
      tools: [],
    },
  };
}

Vue.component('strategy', {
  props: ['user', 'requestFilters', 'cutOffDate'],

  data() {
    return getDefaultData(this);
  },

  created() {
    const that = this;
    this.createCssColorRules();
    this.initialiseFromRequest();
    this.initPreference();
    this.loadStrategies();
    this.getColors();
    this.loadPortfolios();
    $('body').on('click', (e) => {
      if ($('.opened').length) {
        const target = e.toElement || e.target;
        if ($(target).parents('.opened').length === 0) {
          that[$('.opened').data('var')] = -1;
        }
      }
    });
  },

  beforeDestroy() {
    //remove component event listeners and golbal variable.
    $('body').off('click');
    $('#ex1').off('change').off('slideStop');
    $('#ex2').off('change').off('slideStop');
    $('#ex3').off('change').off('slideStop');
    if (window.charts.chartdiv) {
      window.charts.chartdiv.clear();
      delete window.charts.chartdiv;
    }
    while (this.$children.length) {
      this.$children[0].$destroy();
    }
  },

  computed: {
    portfolios() {
      return Object.values(this.$store.state.portfolios);
    },
    strategies() {
      const { strategies } = this.$store.state;
      const sort = [];
      for (const id in strategies) {
        sort.push(strategies[id]);
      }
      return sort;
    },
  },

  watch: {
    pinnedIndicesIds(val) {
      if (val.length === 0) {
        this.showPinnedGraphics = false;
      }
    },
    slider1() {
      this.slider1Value = this.setSliderValue(this.slider1, this.slider1Step, this.sliderRange1);
    },
    slider2() {
      this.slider2Value = this.setSliderValue(this.slider2, this.slider2Step, this.sliderRange2);
    },
    slider3() {
      this.slider3Value = this.setSliderValue(this.slider3, 0.1, this.sliderRange3);
    },
    regions() {
      this.filterChanged({ resetSliderRange: true });
    },
    assetClasses() {
      this.filterChanged({ resetSliderRange: true });
    },
    currencies() {
      this.filterChanged({ resetSliderRange: true });
    },
    factors() {
      this.filterChanged({ resetSliderRange: true });
    },
    styles() {
      this.filterChanged({ resetSliderRange: true });
    },
    types() {
      this.filterChanged({ resetSliderRange: true });
    },
    returnTypes() {
      this.filterChanged({ resetSliderRange: true });
    },
    sponsors() {
      this.filterChanged({ resetSliderRange: true });
    },
    history() {
      if (this.history.length > 1) {
        this.history = [this.history.pop()];
      } else {
        this.filterChanged({ resetSliderRange: true });
      }
    },
    keywords() {
      this.filterChanged({ resetSliderRange: true });
    },
    xAxis() {
      this.loadingNewData = true;
      this.slider1Text = `${this.labels[this.xAxis]} `;
      this.setLink();
      if (this.slider1_slider2) {
        this.slider1 = this.slider2;
      } else if (this.slider1_slider3) {
        this.slider1 = this.slider3;
      } else {
        this.slider1 = '';
      }
      this.getIndices({
        resetSliderRange: true,
        resetGraph: true,
      });
    },
    yAxis() {
      this.loadingNewData = true;
      this.slider2Text = `${this.labels[this.yAxis]} `;
      this.setLink();
      if (this.slider1_slider2) {
        this.slider2 = this.slider1;
      } else if (this.slider2_slider3) {
        this.slider2 = this.slider3;
      } else {
        this.slider2 = '';
      }
      this.getIndices({
        resetSliderRange: true,
        resetGraph: true,
      });
    },
    preference(newValue) {
      apiPreference.setPreference('discover', newValue);
    },
    async period() {
      if (!this.initdata) {
        return;
      }
      this.loadingNewData = true;
      const payload = {
        ignore: ['VIX'],
        metrics: true,
        periodKeys: [this.periodKeysForFilters[this.period]],
      };
      if (this.strategies[0][`${this.periodKeysForFilters[this.period]}_active`] === undefined) {
        this.loadFilters(payload);
        await this.$store.dispatch(ACTION.API_FETCH_STRATEGY, {
          payload,
          transform: o => shortenReturnProps(o, 'Format'),
          freeze: true,
        });
        this.setPinnedItems();
      }
      clearTimeout(this.searchTimeout);
      const that = this;
      this.searchTimeout = setTimeout(() => {
        that.loadingNewData = true;
        $('#ex1').slider('setAttribute', 'value', that.sliderRange1).slider('refresh');
        $('#ex2').slider('setAttribute', 'value', that.sliderRange2).slider('refresh');
        $('#ex3').slider('setAttribute', 'value', that.sliderRange3).slider('refresh');
        that.slider1 = '';
        that.slider2 = '';
        that.slider3 = '';
        that.getIndices({
          resetSliderRange: true,
          resetGraph: true,
        });
      }, 100);
    },
    dataTableCols_raw() {
      this.dataTableColsCount = this.dataTableCols_raw.length;
      this.setPreference('performance', this.dataTableCols_raw);
    },
  },

  methods: {
    async loadStrategies() {
      const payload = {
        ignore: ['VIX'],
        metrics: true,
      };
      if (!bowser.msie) {
        payload.periodKeys = [this.periodKeysForFilters[this.period]];
      }
      this.loadFilters(payload);
      await this.$store.dispatch(ACTION.API_FETCH_STRATEGY, {
        payload,
        transform: o => shortenReturnProps(o, 'Format'),
        freeze: true,
      });

      this.initdata = true;
      this.getIndices({
        resetSliderRange: true,
        resetGraph: true,
      });
    },
    async getColors() {
      const fetechData = await this.$store.dispatch(ACTION.API_FETCH_COLORS, {});
      Object.keys(this.pinnedColorsAll).forEach((key) => {
        this.pinnedColorsAll[key] = fetechData.benchmarks[key];
      });
    },
    async loadPortfolios() {
      await this.$store.dispatch(ACTION.API_FETCH_PORTFOLIO, {});
      this.portfoliosLoaded = true;
    },
    async loadFilters(payload) {
      await this.$store.dispatch(ACTION.API_FETCH_FILTER, payload);
      this.filters = this.$store.state.filters;
      this.filtersLoaded = true;
    },
    initPreference() {
      if (this.user.preference && this.user.preference.discover) {
        this.preference = JSON.parse(this.user.preference.discover);
        this.xAxis = this.preference.xAxis;
        this.yAxis = this.preference.yAxis;
        this.dataTableCols_raw = this.preference.performance;
        if (this.preference.sortBy !== '' && this.preference.sortBy !== 'shortName') {
          this.sortby = `${this.periodKeysForFilters[this.period]}_${this.preference.sortBy}`;
        } else {
          this.sortby = this.preference.sortBy;
        }
        this.sortorder = this.preference.sortOrder;
      }
    },
    setPreference(key, value) {
      this.preference = { ...this.preference, [key]: value };
    },
    setPinnedStyle(id) {
      const color = this.pinnedColorsAll[this.pinnedColorOrder[id]];
      return `background-color:${color};`;
    },
    setPinnedItems() {
      const { strategies } = this.$store.state;
      this.pinnedIndices = this.pinnedIndicesIds.map(id => strategies[id]);
    },
    setSliderValue(slider, step, range) {
      if (slider === '' || slider === `${range[0]},${range[1]}`) {
        return '';
      }
      const values = slider.split(',');
      let tofixed = 0;
      if (step < 1) {
        tofixed = 1;
      }
      const value0 = `${values[0] > 0 ? '+' : ''}${parseFloat(values[0]).toFixed(tofixed)}`;
      const value1 = `${values[1] > 0 ? '+' : ''}${parseFloat(values[1]).toFixed(tofixed)}`;
      return `(${value0} - ${value1})`;
    },
    getDisplayIndices(indices) {
      return indices.slice(0, this.onDisplay);
    },
    initialiseFromRequest() {
      if (this.requestFilters && Object.keys(this.requestFilters).length) {
        this.showFilters = true;
        if (this.requestFilters.types) {
          this.TypeCheckboxInactive = false;
          this.types = this.requestFilters.types;
        }
        if (this.requestFilters.assetClasses) {
          this.AssetClassCheckboxInactive = false;
          this.assetClasses = this.requestFilters.assetClasses;
        }
        if (this.requestFilters.factors) {
          this.FactorCheckboxInactive = false;
          this.factors = this.requestFilters.factors;
        }
        if (this.requestFilters.regions) {
          this.RegionCheckboxInactive = false;
          this.regions = this.requestFilters.regions;
        }
        if (this.requestFilters.currencies) {
          this.CurrencyCheckboxInactive = false;
          this.currencies = this.requestFilters.currencies;
        }
        if (this.requestFilters.styles) {
          this.StyleCheckboxInactive = false;
          this.styles = this.requestFilters.styles;
        }
        if (this.requestFilters.returnTypes) {
          this.ReturnTypeCheckboxInactive = false;
          this.returnTypes = this.requestFilters.returnTypes;
        }
        if (this.requestFilters.period) {
          let period = this.period;
          for (const key in this.periodKeysForFilters) {
            if (this.periodKeysForFilters[key] === this.requestFilters.period) {
              period = key;
            }
          }
          this.period = period;
        }
      }
    },
    createCssColorRules() {
      //highlight-benchmark
      const style = document.createElement('style');
      style.type = 'text/css';
      style.innerHTML = `.highlight-benchmark { color: ${this.dotColors.benchmark}; }`;
      style.innerHTML += '\n\r';
      style.innerHTML += `.highlight-private_track { color: ${this.dotColors['private strategy']}; }`;
      style.innerHTML += '\n\r';
      style.innerHTML += `.highlight-pure_factor { color: ${this.dotColors['pure factor']}; }`;
      document.getElementsByTagName('head')[0].appendChild(style);
    },
    clearSearchTimeout() {
      clearTimeout(this.searchTimeout);
    },
    filterChanged({ resetSliderRange = true }) {
      if (this.filtersLoaded === true) {
        this.loadingNewData = true;
        clearTimeout(this.searchTimeout);
        const that = this;
        this.searchTimeout = setTimeout(() => {
          that.getIndices({
            resetSliderRange,
            resetGraph: true,
          });
        }, 200);
      }
    },
    showMore() {
      this.onDisplay += this.paginationSize;
    },
    setLink() {
      this.slider1_slider2 = this.slider1Text === this.slider2Text;
      this.slider1_slider3 = this.slider1Text === this.slider3Text;
      this.slider2_slider3 = this.slider2Text === this.slider3Text;
    },
    getStrategyType(index) {
      if (index.type === 'Private Strategy') return 'Private Track';
      return $.trim(index.type) || 'Strategy';
    },
    getStrategyTypeForCss(index) {
      let type = this.getStrategyType(index);
      type = type.replace(' ', '_');
      return type;
    },
    getUniverseXY() {
      console.log('making graph');
      const that = this;
      let dataProvider = {};
      const graphs = [];

      let xUnit = '';
      let yUnit = '';
      const xKey = `${this.periodKeysForFilters[this.period]}_${this.xAxis}`;
      const yKey = `${this.periodKeysForFilters[this.period]}_${this.yAxis}`;
      if (this.indices.length) {
        xUnit = this.indices[0][`um_${this.xAxis}`] || '';
        yUnit = this.indices[0][`um_${this.yAxis}`] || '';
      }

      for (let idx = 0; idx < this.indices.length; idx++) {
        dataProvider[`${idx}x`] = this.indices[idx][xKey];
        dataProvider[`${idx}y`] = this.indices[idx][yKey];

        const type = this.indices[idx].type || 'Strategy';
        let color = this.dotColors[type.toLowerCase()];
        let size = 3;

        // const searchIdx = this.pinnedIndicesIds.findIndex(o => o === this.indices[idx].id);
        if (this.pinnedColorOrder[this.indices[idx].id]) {
          color = this.pinnedColorsAll[this.pinnedColorOrder[this.indices[idx].id]];
          size = 15;
        }

        graphs.push({
          balloonText: `${this.getStrategyType(this.indices[idx])}<br />${this.indices[idx].shortName || ''}<br>${this.indices[idx].assetClass || ''} | ${this.indices[idx].currency || ''} ${this.indices[idx].returnCategoryFormat || ''}${this.indices[idx].returnTypeFormat || ''} ${this.indices[idx].volTarget || ''}<br><span style="white-space: nowrap;">${this.labels[this.xAxis]}:[[x]]${xUnit} ${this.labels[this.yAxis]}:[[y]]${yUnit}</span>`,
          bullet: 'round',
          lineAlpha: 0,
          xField: `${idx}x`,
          yField: `${idx}y`,
          lineColor: color,
          fillAlphas: 0,
          bulletSize: size,
          indexObject: this.indices[idx].id,
          id: `xy${this.indices[idx].id}`,
        });
      }

      dataProvider = [dataProvider];

      const selectedEventHandler = (event) => {
        that.loadingNewData = true;
        that.searchTimeout = setTimeout(() => {
          let { xStart, yStart, xEnd, yEnd } = that.calPlace(event);
          if (that.slider1Step < 1) {
            that.slider1 = `${xStart.toFixed(1)},${xEnd.toFixed(1)}`;
          } else {
            that.slider1 = `${xStart.toFixed(0)},${xEnd.toFixed(0)}`;
          }
          if (that.slider2Step < 1) {
            that.slider2 = `${yStart.toFixed(1)},${yEnd.toFixed(1)}`;
          } else {
            that.slider2 = `${yStart.toFixed(0)},${yEnd.toFixed(0)}`;
          }
          if (that.slider1_slider2) {
            yStart = xStart;
            yEnd = xEnd;
            that.slider2 = that.slider1;
          }
          if (that.slider1_slider3) {
            that.slider3 = that.slider1;
            $('#ex3').slider('setAttribute', 'value', [xStart, xEnd]).slider('refresh');
          }
          if (that.slider2_slider3) {
            that.slider3 = that.slider2;
            $('#ex3').slider('setAttribute', 'value', [yStart, yEnd]).slider('refresh');
          }
          $('#ex1').slider('setAttribute', 'value', [xStart, xEnd]).slider('refresh');
          $('#ex2').slider('setAttribute', 'value', [yStart, yEnd]).slider('refresh');
          that.getIndices({
            resetSliderRange: false,
            resetGraph: true,
          });
        }, 20); // to show the loading before the event handling
      };

      if (window.charts.chartdiv) {
        window.charts.chartdiv.graphs = graphs;
        window.charts.chartdiv.dataProvider = dataProvider;
        window.charts.chartdiv.valueAxes[0].title = this.labels[this.xAxis] + (xUnit && ` (${xUnit})`);
        window.charts.chartdiv.valueAxes[1].title = this.labels[this.yAxis] + (yUnit && ` (${yUnit})`);
        window.requestAnimationFrame(() => {
          window.charts.chartdiv.validateData();
          that.loadingNewData = false;
        });
      } else {
        window.requestAnimationFrame(() => {
          window.charts.chartdiv = AmCharts.makeChart('chartdiv', {
            hideCredits: true,
            type: 'xy',
            theme: 'light',
            autoMarginOffset: 20,
            addClassNames: true,
            dataProvider,
            valueAxes: [{
              position: 'bottom',
              axisAlpha: 0.1,
              dashLength: 1,
              title: this.labels[this.xAxis] + (xUnit && ` (${xUnit})`),
              gridThickness: 1,
              gridAlpha: 0.1,
            }, {
              axisAlpha: 0.1,
              dashLength: 1,
              position: 'left',
              title: this.labels[this.yAxis] + (yUnit && ` (${yUnit})`),
              gridAlpha: 0.1,
              gridThickness: 1,
            }],
            startDuration: 0,
            graphs,
            marginLeft: 64,
            marginBottom: 60,
            chartCursor: {
              cursorAlpha: 0,
              selectWithoutZooming: true,
              listeners: [
                {
                  event: 'selected',
                  method: selectedEventHandler,
                },
              ],
            },
          });
          window.charts.chartdiv.addListener('clickGraphItem', (event) => {
            that.pinIndex(that.indicesShowing[event.item.graph.indexObject]);
          });
          that.loadingNewData = false;
        });
      }
    },
    setXAxis(key) {
      this.xAxis = key;
      this.setPreference('xAxis', key);
    },
    setYAxis(key) {
      this.yAxis = key;
      this.setPreference('yAxis', key);
    },
    calPlace(event) {
      const xAxis = event.chart.valueAxes[0];
      const yAxis = event.chart.valueAxes[1];
      const xSmall = event.startX < event.endX ? event.startX : event.endX;
      const xLarge = event.startX > event.endX ? event.startX : event.endX;
      const ySmall = event.startY < event.endY ? event.startY : event.endY;
      const yLarge = event.startY > event.endY ? event.startY : event.endY;
      const xStartValue = (xAxis.fullMax - xAxis.fullMin) * xSmall + xAxis.fullMin;
      const xEndValue = (xAxis.fullMax - xAxis.fullMin) * xLarge + xAxis.fullMin;
      const yStartValue = (yAxis.fullMax - yAxis.fullMin) * ySmall + yAxis.fullMin;
      const yEndValue = (yAxis.fullMax - yAxis.fullMin) * yLarge + yAxis.fullMin;

      let xStart = Math.floor(xStartValue / this.slider1Step) * this.slider1Step;
      let xEnd = Math.ceil(xEndValue / this.slider1Step) * this.slider1Step;
      xStart = xStart < this.sliderRange1[0] ? this.sliderRange1[0] : xStart;
      xEnd = xEnd > this.sliderRange1[1] ? this.sliderRange1[1] : xEnd;

      let yStart = Math.floor(yStartValue / this.slider2Step) * this.slider2Step;
      let yEnd = Math.ceil(yEndValue / this.slider2Step) * this.slider2Step;
      yStart = yStart < this.sliderRange2[0] ? this.sliderRange2[0] : yStart;
      yEnd = yEnd > this.sliderRange2[1] ? this.sliderRange2[1] : yEnd;
      return {
        xStart, yStart, xEnd, yEnd,
      };
      // const sliderValue = [finalStart, finalEnd];
    },
    resetSldierFilters() {
      const that = this;
      this.loadingNewData = true;
      this.slider1 = '';
      this.slider2 = '';
      this.slider3 = '';
      this.showAllButton = false;
      this.searchTimeout = setTimeout(() => {
        that.getIndices({
          resetSliderRange: true,
          resetGraph: true,
        });
      }, 200);
    },
    setPeriod(period) {
      this.period = period;
    },
    getPinnedName(findId) {
      return this.pinnedIndices.find(o => o.id.toString() === findId, 10).shortName;
    },
    pinIndex(index) {
      if (this.pinnedIndicesIds.indexOf(index.id) >= 0) {
        this.unpinIndex(index);
      } else if (this.pinnedIndicesIds.length < this.pinnedIndicesMax) {
        const i = this.pinnedIndicesIds.length;
        this.pinnedIndices.push(index);
        this.pinnedIndicesIds.push(index.id);
        const idTarget = `xy${index.id}`;
        const colors = Object.keys(this.pinnedColorsAll);
        const usedColors = Object.values(this.pinnedColorOrder);
        const color = colors.find(x => !usedColors.includes(x));
        this.$set(this.pinnedColorOrder, index.id, color);
        this.sortPinndeIndices();
        $(`.amcharts-graph-${idTarget} .amcharts-graph-bullet`).attr('r', 5);
        $(`.amcharts-graph-${idTarget} .amcharts-graph-bullet`).attr('fill', this.pinnedColorsAll[color]);
      }
      analytics.track('Pinned Strategy', getStrategyAnalytics(index));
    },
    unpinStringIndex(stringIndex) {
      this.unpinIndex({ id: +stringIndex });
    },
    unpinIndex(index) {
      const idx = this.pinnedIndicesIds.findIndex(o => o === index.id);
      this.pinnedIndices.splice(idx, 1);
      this.pinnedIndicesIds.splice(idx, 1);
      this.$delete(this.pinnedColorOrder, index.id);
      const idTarget = `xy${index.id}`;
      $(`.amcharts-graph-${idTarget} .amcharts-graph-bullet`).attr('r', 2);

      const type = index.type || 'Strategy';
      const color = this.dotColors[type.toLowerCase()];
      $(`.amcharts-graph-${idTarget} .amcharts-graph-bullet`).attr('fill', color);
    },
    unpinAll() {
      this.pinnedIndices.map((o) => {
        const idTarget = `xy${o.id}`;
        $(`.amcharts-graph-${idTarget} .amcharts-graph-bullet`).attr('r', 2);
        const type = o.type || 'Strategy';
        const color = this.dotColors[type.toLowerCase()];
        $(`.amcharts-graph-${idTarget} .amcharts-graph-bullet`).attr('fill', color);
      });
      this.pinnedIndicesIds = [];
      this.pinnedIndices = [];
      this.pinnedColorOrder = {};
    },
    resetFilters() {
      this.loadingNewData = true;
      const filtersToReset = ['regions', 'assetClasses', 'currencies', 'factors', 'styles', 'returnTypes', 'sponsors', 'types'];
      for (const i in filtersToReset) {
        const key = filtersToReset[i];
        this[key] = [];
      }
      $('#ex1').slider('setAttribute', 'value', this.sliderRange1).slider('refresh');
      $('#ex2').slider('setAttribute', 'value', this.sliderRange2).slider('refresh');
      $('#ex3').slider('setAttribute', 'value', this.sliderRange3).slider('refresh');
      this.slider1 = '';
      this.slider2 = '';
      this.slider3 = '';
      this.history = [];
      this.keywords = '';
      this.countActiveFilters = 0;
    },
    getIndices({ resetSliderRange = false, resetGraph = true }) {
      if (!this.initdata) {
        return;
      }
      if (this.searchTimeout !== null) {
        clearTimeout(this.searchTimeout);
      }
      this.loadingNewData = true;
      const that = this;
      const period = this.periodKeysForFilters[this.period];

      const activeStrategies = this.strategies.filter((o) => {
        if (o[`${period}_active`] !== 1) { return 0; }
        return 1;
      });

      this.loadingNewData = true;

      const filters = {
        regions: this.regions,
        assetClasses: this.assetClasses,
        currencies: this.currencies,
        factors: this.factors,
        styles: this.styles,
        types: this.types,
        returnTypes: this.returnTypes,
        sponsors: this.sponsors,
        history: this.history.length === 0 ? '' : this.history[0],
        keywords: this.keywords,
        period,
      };

      const filteredData = this.filterIndicesWithoutSilder(activeStrategies, filters);

      if (resetSliderRange) {
        this.setSliderRange(filteredData, period);
      }

      this.filterIndicesOnlySilder(filteredData);

      try {
        const getMin = str => (str !== '') ? Math.floor(str.split(',')[0]) : undefined;
        const getMax = str => (str !== '') ? Math.ceil(str.split(',')[1]) : undefined;
        analytics.track('Viewed Universe', _.mapKeys(Object.assign({}, _.omit(filters, [
          'sponsors',
        ]), {
          [`${that.xAxis}Min`]: getMin(this.slider1),
          [`${that.xAxis}Max`]: getMax(this.slider1),
          [`${that.yAxis}Min`]: getMin(this.slider2),
          [`${that.yAxis}Max`]: getMax(this.slider2),
          sharpeMin: getMin(this.slider3),
          sharpeMax: getMax(this.slider3),
          period: this.period,
          activesCount: this.countActiveFilters,
        }), (value, key) => `Filter ${_.startCase(key)}`));
      } catch (e) {
        console.error(e);
      }
      this.getActiveFilters(filters);
      this.sortIndices();
      this.indicesLoaded = true;
      if (resetGraph) {
        this.getUniverseXY();
      } else {
        this.loadingNewData = false;
      }
    },

    filterIndicesWithoutSilder(indices = [], filters) {
      const that = this;
      let maxDate;
      if (filters.history) { // for history filtering
        const history = filters.history.split(' ');
        maxDate = moment.utc(that.cutOffDate);
        maxDate.subtract(+history[0], history[1].toLowerCase());
      }

      const checkKeyword = byKeywordFactory(filters.keywords, this.portfolios);

      const reponsedata = indices.filter((o) => {
        if (filters.keywords.length && !checkKeyword(o)) { return 0; }
        if (filters.factors.length && filters.factors.indexOf(o.factor === 'Benchmark' ? 'Multi' : o.factor) < 0) { return 0; }
        if (filters.assetClasses.length && filters.assetClasses.indexOf(o.assetClass) < 0) { return 0; }
        if (filters.regions.length && filters.regions.indexOf(o.region) < 0) { return 0; }
        if (filters.currencies.length && filters.currencies.indexOf(o.currency) < 0) { return 0; }
        if (filters.styles.length && filters.styles.indexOf(o.style) < 0) { return 0; }
        if (filters.types.length && filters.types.indexOf(o.type) < 0) { return 0; }
        if (filters.returnTypes.length && filters.returnTypes.indexOf(o.returnType) < 0) { return 0; }
        if (filters.sponsors.length && filters.sponsors.indexOf(o.provider) < 0) { return 0; }
        if (filters.history && moment.utc(o.historyStartDate).isAfter(maxDate)) { return 0; }
        return 1;
      });
      console.log(`Filtered ${reponsedata.length}/${indices.length} strategies.`);
      return reponsedata;
    },

    filterIndicesOnlySilder(indices = []) {
      const slider1Value = this.slider1.split(',').map(o => parseFloat(o));
      const slider2Value = this.slider2.split(',').map(o => parseFloat(o));
      const sharpeValues = this.slider3.split(',').map(o => parseFloat(o));
      const period = this.periodKeysForFilters[this.period];
      const reponsedata = indices.filter((o) => {
        if (slider1Value.length === 2 && (o[`${period}_${this.xAxis}`] < +slider1Value[0] || o[`${period}_${this.xAxis}`] > +slider1Value[1])) { return 0; }
        if (slider2Value.length === 2 && (o[`${period}_${this.yAxis}`] < +slider2Value[0] || o[`${period}_${this.yAxis}`] > +slider2Value[1])) { return 0; }
        if (sharpeValues.length === 2 && (o[`${period}_sharpe`] < +sharpeValues[0] || o[`${period}_sharpe`] > +sharpeValues[1])) { return 0; }
        return 1;
      });
      console.log(`Filtered ${reponsedata.length}/${indices.length} strategies.`);

      const indicesnew = [];
      const indicesShowingNew = {};
      reponsedata.forEach((o) => {
        indicesShowingNew[o.id] = o;
        indicesnew.push(o);
      });
      // this.pinnedIndicesIds.forEach((id) => {
      //   if (!indicesShowingNew[id]) {

      //   }
      // });
      this.indicesShowing = indicesShowingNew;
      this.indices = indicesnew;
    },
    getActiveFilters(filters) {
      this.countActiveFilters = 0;
      this.showAllButton = false;
      const filtersToCount = ['regions', 'assetClasses', 'currencies', 'factors', 'styles', 'returnTypes', 'keywords', 'sponsors', 'history', 'types'];
      for (const i in filtersToCount) {
        const key = filtersToCount[i];
        if (filters[key] && filters[key].length) {
          this.countActiveFilters++;
        }
      }

      if (this.slider1 !== '') {
        if (this.slider1 !== `${this.sliderRange1[0]},${this.sliderRange1[1]}`) {
          this.showAllButton = true;
          this.countActiveFilters++;
        }
      }

      if (this.slider2 !== '') {
        if (this.slider2 !== `${this.sliderRange2[0]},${this.sliderRange2[1]}`) {
          this.showAllButton = true;
          this.countActiveFilters++;
        }
      }

      if (this.slider3 !== '') {
        if (this.slider3 !== `${this.sliderRange3[0]},${this.sliderRange3[1]}`) {
          this.showAllButton = true;
          this.countActiveFilters++;
        }
      }
    },
    getSliderRange(previousRange, previousValue, currentRange, step, sliderNo) {
      const sliderKey = `slider${sliderNo}`;
      if (previousValue.length < 2) {
        this[sliderKey] = currentRange.join(',');
        return currentRange;
      }
      let firstEqual = false;
      let secondEqual = false;
      if (previousRange[0] === previousValue[0]) {
        firstEqual = true;
      }
      if (previousRange[1] === previousValue[1]) {
        secondEqual = true;
      }
      if (firstEqual && secondEqual) {
        this[sliderKey] = currentRange.join(',');
        return currentRange;
      }
      const sliderMax = currentRange[1] >= previousValue[1] ? currentRange[1] : previousValue[1] + step;
      const sliderMin = currentRange[0] <= previousValue[0] ? currentRange[0] : previousValue[0] - step;
      this[sliderKey] = `${previousValue[0]},${previousValue[1]}`;
      return [sliderMin, sliderMax];
    },
    setSliderRange(indices, period) {
      const getRange = (a, f) => [ // return [min, max]
        a.length ? Math.floor(+a.reduce((t, o) => +t[f] < +o[f] ? t : o)[f]) : 0,
        a.length ? Math.ceil(+a.reduce((t, o) => +t[f] > +o[f] ? t : o)[f]) : 100,
      ];

      const newSliderRange1 = getRange(indices, `${period}_${this.xAxis}`);
      const newSliderRange2 = getRange(indices, `${period}_${this.yAxis}`);
      const newSliderRange3 = getRange(indices, `${period}_sharpe`);

      this.slider1Step = this.setStep(newSliderRange1);
      this.slider2Step = this.setStep(newSliderRange2);

      let slider1Value = this.slider1.split(',').map(o => parseFloat(o));
      let slider2Value = this.slider2.split(',').map(o => parseFloat(o));
      let slider3Value = this.slider3.split(',').map(o => parseFloat(o));

      this.sliderRange1 = this.getSliderRange(this.sliderRange1, slider1Value, newSliderRange1.slice(-2), this.slider1Step, 1);
      this.sliderRange2 = this.getSliderRange(this.sliderRange2, slider2Value, newSliderRange2.slice(-2), this.slider2Step, 2);
      this.sliderRange3 = this.getSliderRange(this.sliderRange3, slider3Value, newSliderRange3.slice(-2), 0.1, 3);

      slider1Value = this.slider1.split(',').map(o => parseFloat(o));
      slider2Value = this.slider2.split(',').map(o => parseFloat(o));
      slider3Value = this.slider3.split(',').map(o => parseFloat(o));

      const that = this;

      $('#ex1').slider('setAttribute', 'value', slider1Value)
        .slider('setAttribute', 'min', this.sliderRange1[0])
        .slider('setAttribute', 'max', this.sliderRange1[1])
        .slider('setAttribute', 'step', this.slider1Step)
        .slider('refresh')
        .on('change', (event) => {
          const newValue = `${event.value.newValue[0]},${event.value.newValue[1]}`;
          const oldValue = `${event.value.oldValue[0]},${event.value.oldValue[1]}`;
          if (newValue !== oldValue) {
            if (that.slider1_slider2) {
              that.slider2 = newValue;
              $('#ex2').slider('setAttribute', 'value', event.value.newValue).slider('refresh');
            }
            if (that.slider1_slider3) {
              that.slider3 = newValue;
              $('#ex3').slider('setAttribute', 'value', event.value.newValue).slider('refresh');
            }
            that.slider1 = newValue;
          }
        })
        .on('slideStop', () => {
          that.filterChanged({ resetSliderRange: false });
        });
      $('#ex2').slider('setAttribute', 'value', slider2Value)
        .slider('setAttribute', 'min', this.sliderRange2[0])
        .slider('setAttribute', 'max', this.sliderRange2[1])
        .slider('setAttribute', 'step', this.slider2Step)
        .slider('refresh')
        .on('change', (event) => {
          const newValue = `${event.value.newValue[0]},${event.value.newValue[1]}`;
          const oldValue = `${event.value.oldValue[0]},${event.value.oldValue[1]}`;
          if (newValue !== oldValue) {
            if (that.slider1_slider2) {
              that.slider1 = newValue;
              $('#ex1').slider('setAttribute', 'value', event.value.newValue).slider('refresh');
            }
            if (that.slider2_slider3) {
              that.slider3 = newValue;
              $('#ex3').slider('setAttribute', 'value', event.value.newValue).slider('refresh');
            }
            that.slider2 = newValue;
          }
        })
        .on('slideStop', () => {
          that.filterChanged({ resetSliderRange: false });
        });
      $('#ex3').slider('setAttribute', 'value', slider3Value)
        .slider('setAttribute', 'min', this.sliderRange3[0])
        .slider('setAttribute', 'max', this.sliderRange3[1])
        .slider('refresh')
        .on('change', (event) => {
          const newValue = `${event.value.newValue[0]},${event.value.newValue[1]}`;
          const oldValue = `${event.value.oldValue[0]},${event.value.oldValue[1]}`;
          if (newValue !== oldValue) {
            if (that.slider2_slider3) {
              that.slider2 = newValue;
              $('#ex2').slider('setAttribute', 'value', event.value.newValue).slider('refresh');
            }
            if (that.slider1_slider3) {
              that.slider1 = newValue;
              $('#ex1').slider('setAttribute', 'value', event.value.newValue).slider('refresh');
            }
            that.slider3 = newValue;
          }
        })
        .on('slideStop', () => {
          that.filterChanged({ resetSliderRange: false });
        });
    },
    setStep(range) {
      const rangeStep = (range[1] - range[0]) / 100;
      // if (rangeStep <= 0.02) {
      //   return 0.02;
      // }
      // if (rangeStep <= 0.05) {
      //   return 0.05;
      // }
      if (rangeStep <= 0.1) {
        return 0.1;
      }
      if (rangeStep <= 0.2) {
        return 0.2;
      }
      if (rangeStep <= 0.5) {
        return 0.5;
      }
      return 1;
    },
    toggleSidebar() {
      this.showFilters = !this.showFilters;
    },
    sortPinndeIndices() {
      let iterate = this.sortby;
      if (this.sortby === 'shortName') {
        iterate = [
          iterateInsensitiveFactory(this.sortby),
          'id',
        ];
      }
      this.pinnedIndices = _.orderBy(this.pinnedIndices, iterate, this.sortorder === 1 ? 'asc' : 'desc');
      this.pinnedIndicesIds = this.pinnedIndices.map(o => o.id);
    },
    sortIndices() {
      let iterate = this.sortby;
      if (this.sortby === 'shortName') {
        iterate = [
          iterateInsensitiveFactory(this.sortby),
          'id',
        ];
      }
      this.indices = _.orderBy(this.indices, iterate, this.sortorder === 1 ? 'asc' : 'desc');
      this.sortPinndeIndices();
    },
    checkHistoryData(historyItem) {
      if (this.history === historyItem) {
        this.history = '';
      }
    },
    changeSortby(sortby) {
      if (this.sortby === sortby) {
        this.sortorder = this.sortorder * -1;
        this.setPreference('sortOrder', this.sortorder);
      } else {
        this.sortby = sortby;
        const keywords = sortby.split('_');
        if (keywords.length === 2) {
          this.setPreference('sortBy', keywords[1]);
        } else {
          this.setPreference('sortBy', keywords[0]);
        }
        this.setPreference('sortOrder', -1);
        this.sortorder = -1;
      }
      this.sortIndices();
    },
    toggle(property) {
      this[property] = !this[property];
    },
    createPortfolio() {
      this.indicesBasket = this.pinnedIndices;
      $('#createProtfolio').modal('show');
    },
    saveStrategies(indexes) {
      this.indicesBasket = indexes;
      $('#addStrategies').modal('show');
    },
  },
});
