Vue.component('strategy-comparison', {
  props: ['pinnedIndices'],

  data() {
    return {
	    
    }
  },

  created() {
    const that = this;
  },

  beforeDestroy() {
  },

  computed: {
  },

  watch: {
  },

  methods: {
    changeSortby(sortby) {
      if (this.sortby === sortby) {
        this.sortorder = this.sortorder * -1;
      } else {
        this.sortby = sortby;
        this.sortorder = -1;
      }
      this.sortIndices();
    },

  },
});
