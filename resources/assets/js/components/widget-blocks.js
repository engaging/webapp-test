Vue.component('widget-block', {
  props: ['user', 'widgetData', 'block', 'idx', 'period', 'properties', 'portfolioLoaded', 'strategiesLoaded', 'trackLoaded', 'volatilityLoading'],
  data() {
    return {
      periodKeysForFilters: {
        '10 Year': '10y',
        '5 Year': '5y',
        '3 Year': '3y',
        '1 Year': '1y',
        '6 Month': '6m',
      },
      data: {},
      composition_sortBy: '',
      composition_sortOrder: -1,
      sortDir: -1,
      sortingBy: '',
      showTable: false,
      datasets: [],
      main: {},
    };
  },
  computed: {
    totalLoaded() {
      switch (this.currentData.componenttype) {
        case 'bubble':
        case 'news':
        case 'feed':
          return true;
        case 'track':
        case 'volatilitytrack':
          return this.currentTrackLoaded;
        case 'xy':
          return this.portfolioLoaded && this.strategiesLoaded && this.currentTrackLoaded;
        default:
          return this.portfolioLoaded && this.strategiesLoaded;
      }
    },
    colorsAll() {
      return this.$store.state.colorsAll.benchmarks || {};
    },
    strategies() {
      return this.$store.state.strategies;
    },
    portfolios() {
      return this.$store.state.portfolios;
    },
    currentData() {
      return this.widgetData[this.idx];
    },
    currentTrackLoaded() {
      return this.trackLoaded[this.idx];
    },
  },
  watch: {
    volatilityLoading(val) {
      if (!val) {
        switch (this.currentData.componenttype) {
          case 'volatilitytrack':
            this.setDatasets();
            break;
          default:
            break;
        }
      }
    },
    totalLoaded(val) {
      if (val) {
        switch (this.currentData.componenttype) {
          case 'xy':
          case 'track':
          case 'volatilitytrack':
            this.setDatasets();
            break;
          default:
            break;
        }
      }
    },
  },
  created() {
    if (this.totalLoaded) {
      switch (this.currentData.componenttype) {
        case 'xy':
        case 'track':
        case 'volatilitytrack':
          this.setDatasets();
          break;
        default:
          break;
      }
    }
  },
  methods: {
    setDatasets() {
      const datasets = [];
      this.currentData.properties.datasets.forEach((o) => {
        if (o.type === 'portfolio' && this.portfolios[o.id]) {
          this.portfolios[o.id].color = o.color;
          datasets.push(this.portfolios[o.id]);
        }
        if (o.type !== 'portfolio' && this.strategies[o.id]) {
          this.strategies[o.id].color = o.color;
          datasets.push(this.strategies[o.id]);
        }
      });
      this.datasets = datasets;
    },
    showEditMenu(type) {
      switch (type) {
        default:
          return true;
      }
    },
    allowEdit(type) {
      switch (type) {
        case 'bubble':
        case 'news':
        case 'feed':
          return false;
        default:
          return true;
      }
    },
    allowDelete(type) {
      switch (type) {
        case 'bubble':
          return false;
        default:
          return true;
      }
    },
    allowMoveAndResize(type) {
      switch (type) {
        case 'bubble':
          return false;
        default:
          return true;
      }
    },
    showMenu(block) {
      block.showMenu = !block.showMenu;
    },
    editWidgetModal(idx) { //will edit widget obj and replace idx in widgetData
      window.eventBus.$emit('editWidgetModal', {
        editWidget: this.widgetData[idx],
        idx,
      });
    },
    removeWidget(idx) {
      console.log('widgetUpdated', idx);
      window.eventBus.$emit('widgetUpdated', {
        method: 'removeWidget',
        idx,
      });
    },
  },
});
