/* global window Vue AmCharts */
const ACTION = require('../../store/mutation-type');
const { PERIOD } = require('../../util/period');

/**
 * you will only ever have 1 bench selected (vs 1 main).
 * benchmarks and portfolios are lists of all.
 */
Vue.component('chart-returnscatter', {
  template: `
    <div>
      <h4 class="print-border charttitle">
        {{title}}
      </h4>
      <div class="chartfilter">
        <div class="clearfix" style="position:relative">
          <button type="button" class="btn btn-green dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <strong :class="{ 'no-data': (monthlyXYBenchmark.id && monthlyXYBenchmark[period + '_active'] === 0) }">
              {{monthlyXYBenchmarkTitle}}
            </strong>
            <span class="caret"></span>
          </button>
          <ul class="dropdown-menu dark" v-if="benchmarks!=undefined">
            <li v-for="benchmark in benchmarks" :class="{ active: benchmark === monthlyXYBenchmark }">
              <a @click="monthlyXYBenchmark=benchmark">
                {{benchmark.shortName}} ({{benchmark.code}})
              </a>
            </li>
            <li v-for="(benchmark,key,index) in portfolios" :class="{ 'border-top': index === 0, active: benchmark === monthlyXYBenchmark }">
              <a @click="monthlyXYBenchmark=benchmark">
                {{benchmark.title}}
              </a>
            </li>
          </ul>
        </div>
      </div>
      <div v-show="dataLoading===true" class="abs-loader">
        <div class="loader-inner ball-pulse"><div></div><div></div><div></div></div>
      </div>
      <div class="chart chart-returnscatter" :id="id" style="height:317px"></div>
    </div>
  `,
  props: [
    'title', 'idname', 'type', 'main', 'period', 'pdfoptions', 'basePayload',
  ],
  computed: {
    id() {
      const add = this.idname !== undefined ? this.idname : 'no-id';
      return `returnscatter-chart-${add}`;
    },
    portfolios() {
      return this.$store.state.portfolios;
    },
    strategies() {
      return this.$store.state.strategies;
    },
    benchmarks() {
      const results = {};
      for (const idx in this.strategies) {
        if (this.strategies[idx].type === 'Benchmark') {
          results[idx] = this.strategies[idx];
        }
      }
      return results;
    },
  },
  data() {
    return {
      monthlyXYBenchmarkTitle: 'Select',
      monthlyXYBenchmark: {},
      // chart: null,
      dataLoading: false,
      // showMenu: false,
    };
  },
  watch: {
    period() {
      this.loadChart();
    },
    monthlyXYBenchmark(newValue) {
      if (!newValue) return;
      this.monthlyXYBenchmarkTitle = newValue.code || newValue.title;
      this.fetchBenchmarks();
    },
  },
  methods: {
    async fetchBenchmarks(reset = false) {
      if (this.monthlyXYBenchmarkTitle === 'Select') return;
      if (reset || this.monthlyXYBenchmark[`${this.period}_active`] === undefined) {
        this.dataLoading = true;
        if (this.monthlyXYBenchmark.code) {
          await this.$store.dispatch(ACTION.API_FETCH_STRATEGY_DETAILS, {
            ids: [this.monthlyXYBenchmark.id],
            payload: { ...this.basePayload },
          });
          this.monthlyXYBenchmark = this.strategies[this.monthlyXYBenchmark.id];
        } else {
          await this.$store.dispatch(ACTION.API_FETCH_PORTFOLIO_DETAILS, {
            ids: [this.monthlyXYBenchmark.id],
            payload: { ...this.basePayload },
          });
          this.monthlyXYBenchmark = this.portfolios[this.monthlyXYBenchmark.id];
        }
      } else {
        this.loadChart();
      }
    },
    resetBenchmarks() {
      this.fetchBenchmarks(true);
    },
    loadChart() {
      console.log('small regression scatter');
      const { period } = this;

      // Return Regression with 1 benchmarks to select (X-Y Graph of Monthly returns)
      // to add a selector for 1 benchmark
      // placeholder data
      let data = [{
        strategy1x: 0, strategy2x: 0, strategy1y: 10, strategy2y: 10,
      }];
      let graphs = [
        { xField: 'strategy1x', yField: 'strategy1y', lineColor: '#fff' },
        { xField: 'strategy2x', yField: 'strategy2y', lineColor: '#fff' },
      ];

      const rawData1 = this.main[`${period}_monthly_returns`];
      const rawData2 = this.monthlyXYBenchmark[`${period}_monthly_returns`];

      const axis1 = this.main.title; //change to portfolio.title on a portfolio
      const axis2 = this.monthlyXYBenchmarkTitle; //strategy titles are too long for the graph space

      if (rawData1 && rawData2 && this.monthlyXYBenchmark[`${period}_active`] === 1) {
        graphs = [];
        data = {};
        // each dot is a month from bm vs month from this strat
        for (const y in rawData1) {
          if (rawData2[y] !== undefined) { //data for this year exists
            for (const m in rawData1[y]) {
              if (rawData2[y][m] !== undefined && m !== '0') { //data for this year-month exists, not counting the year total
                // console.log('month', m);
                data[`${y}_${m}_strat1`] = parseFloat(rawData1[y][m]).toFixed(2);
                data[`${y}_${m}_strat2`] = parseFloat(rawData2[y][m]).toFixed(2);
                graphs.push({
                  balloonText: `${m}/${y}<br>${axis1}: [[x]]%<br>${axis2}: [[y]]%`,
                  xField: `${y}_${m}_strat1`,
                  yField: `${y}_${m}_strat2`,
                  bullet: 'round',
                  lineAlpha: 0,
                  lineColor: '#222222',
                  fillAlphas: 0,
                  bulletSize: 3,
                });
              }
            }
          }
        }
        data = [data];
      }
      const { id } = this;
      if (window.charts[id]) {
        window.charts[id].graphs = graphs;
        window.charts[id].dataProvider = data;
        window.charts[id].valueAxes[0].title = axis1 && `${axis1} (%)`;
        window.charts[id].valueAxes[1].title = axis2 !== 'Select' ? `${axis2} (%)` : axis2;
        window.charts[id].validateData();
      } else {
        window.charts[id] = window.AmCharts.makeChart(this.id, {
          hideCredits: true,
          type: 'xy',
          fontFamily: 'Fira Sans',
          theme: 'light',
          autoMarginOffset: 20,
          dataProvider: data,
          // "titles": [{
          //     "text": "Return Regression for "+this.period
          // }],
          addClassNames: true,
          valueAxes: [{
            position: 'bottom',
            axisAlpha: 0,
            dashLength: 1,
            title: axis1 && `${axis1} (%)`,
          }, {
            axisAlpha: 0,
            dashLength: 1,
            position: 'left',
            title: axis2 !== 'Select' ? `${axis2} (%)` : axis2,
          }],
          startDuration: 0,
          graphs,
          trendLines: [],
          marginLeft: 64,
          marginBottom: 60,
          chartScrollbarOff: {},
          chartCursor: {
            cursorPosition: 'start',
            pan: false,
            valueLineEnabled: false,
            valueLineBalloonEnabled: false,
            valueBalloonsEnabled: true,
            cursorAlpha: 0,
            oneBalloonOnly: true,
          },
          pathToImages: 'https://cdn.amcharts.com/lib/3/images/',
        });
      }
      this.dataLoading = false;
    },
  },
  mounted() {
    if (this.pdfoptions) {
      let data = null;
      if (this.pdfoptions.type === 'strategy') {
        data = this.strategies;
      } else {
        data = this.portfolios;
      }
      this.monthlyXYBenchmark = data[this.pdfoptions.id];
    }
    this.loadChart();
    window.eventBus.$on('cust-period.fetch', this.resetBenchmarks);
    window.eventBus.$on('cust-period.load', this.loadChart);
  },
  beforeDestroy() {
    window.eventBus.$off('cust-period.fetch', this.resetBenchmarks);
    window.eventBus.$off('cust-period.load', this.loadChart);
    const { id } = this;
    if (window.charts[id]) {
      window.charts[id].clear();
      delete window.charts[id];
    }
    this.$off();
  },
});
