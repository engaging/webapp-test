import { Vue, Component, Prop, Watch } from 'vue-property-decorator'
import AmStockChart from 'amcharts/AmStockChart';
import { State, Getter, Action } from 'vuex-class'

type DatasetsType = {
  dates: string[];
  tracks: number[];
}

type GraphDataType = {
  date: string;
  track: number;
}

@Component({
  template:
    `<div>
      <h4 class="print-border charttitle">
        {{title + " (%)"}}
      </h4>
      <div v-show="hidden" class="chart chart-alpha-track chart-not-available"></div>
      <div v-show="!hidden" class="chart chart-alpha-track" :id="id" :style="chartHeight" style="max-width:560px;"></div>
    </div>`,
})

export default class AplhaTrackChart extends Vue {
  @Prop({
    type: String,
    default: '',
  })
  title!: string;

  @Prop({
    type: String,
    default: 'no-id',
  })
  idname!: string;

  @Prop({
    type: Object,
    default: () => {},
  })
  datasets!: DatasetsType;

  @Prop({
    type: Boolean,
    default: false,
  })
  hidden!: boolean;

  @Prop({
    type: Number,
    default: 333,
  })
  height!: number;


  get id() {
    return `alpha-track-chart-${this.idname}`;
  }

  get chartHeight() {
    return `height:${this.height}px;`;
  }

  @Watch('datasets')
  onDatasetsChange() {
    this.loadChart();
  }

  beforeDestroy() {
    const { id } = this;
    if (window.charts[id]) {
      window.charts[id].clear();
      delete window.charts[id];
    }
    this.$off();
  }

  mounted() {
    this.loadChart();
  }

  loadChart() {
    console.log('small alpha charts');
    const that = this;
    const trackData:GraphDataType[] = [];
    this.datasets.dates.forEach((date, idx) => {
      const baseGraphType:GraphDataType = {
        date,
        track: this.datasets.tracks[idx],
      }
      trackData.push(baseGraphType);
    })

    const graphs = [{
      id: 'alpha-track',
      valueField: 'track',
      // lineColor: color,
      lineColor: '#1A85A1',
      negativeLineColor: '#b7294f',
      lineThickness: 2,
      fillAlphas: 0.2,
      negativeFillAlphas: 0.2,
      type: 'line',
      balloonText: 'Alpha<br>[[value]]%',
    }];
    const { id } = this;
    if (window.charts[id]) {
      window.charts[id].graphs = graphs;
      window.charts[id].dataProvider = trackData;
      window.charts[id].validateData();
    } else {
      window.charts[id] = window.AmCharts.makeChart(that.id, {
        hideCredits: true,
        fontFamily: 'Fira Sans',
        type: 'serial',
        theme: 'light',
        addClassNames: true,
        dataDateFormat: 'YYYY-MM-DD',
        graphs,
        categoryField: 'date',
        valueAxes: [{
          id: 'v1',
          dashLength: 0,
          axisAlpha: 0,
        }],
        chartCursor: {
          cursorPosition: 'start',
          categoryBalloonEnabled: true,
          valueLineBalloonEnabled: false,
          valueLineAlpha: 0,
          cursorAlpha: 0,
          oneBalloonOnly: true,
        },
        balloon: {
          borderThickness: 2,
          shadowAlpha: 0,
        },
        categoryAxis: {
          fillAlpha: 0,
          gridAlpha: 0,
          parseDates: true,
        },
        dataProvider: trackData,
        pathToImages: 'https://cdn.amcharts.com/lib/3/images/',
      });
    }
  }
}

