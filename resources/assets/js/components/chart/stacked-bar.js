/* global window document Vue */
Vue.component('chart-stacked-bar', {
  props: {
    idname: {
      type: String,
      default: 'no-id',
    },
    main: {
      type: Object,
      default: () => {},
    },
    hideValueAxis: {
      type: Boolean,
      default: true,
    },
    height: {
      type: Number,
      default: 80,
    },
    keyField: {
      type: String,
      default: 'varDecompose',
    },
    maxLength: {
      type: Number,
      default: 100,
    },
  },
  data() {
    return {
      graphText: '',
      // trackData: {},
      // chart: null,
    };
  },
  computed: {
    id() {
      return `stacked-bar-chart-${this.idname}`;
    },
    colors() {
      return this.$store.state.colorsAll.pcas;
    },
  },
  watch: {
    main() {
      this.loadChart();
    },
  },
  mounted() {
    this.loadChart();
  },
  beforeDestroy() {
    const { id } = this;
    if (window.charts[id]) {
      window.charts[id].clear();
      delete window.charts[id];
    }
    this.$off();
  },
  methods: {
    loadChart() {
      const dataObject = {
        strategy: this.idname,
      };
      if (!this.main) {
        return null;
      }
      const graphs = [];
      if (this.main[this.keyField]) {
        // let max = 0;
        // let maxPc = '';
        for (const pc in this.main[this.keyField]) {
          dataObject[pc] = this.main[this.keyField][pc].toFixed(2);
          // if (dataObject[pc] > max) {
          //   max = dataObject[pc];
          //   maxPc = pc;
          // }
          let color = this.colors[pc] || '#ccc';
          if (!pc.startsWith('pc')) color = '#808080'; // undermined part
          graphs.push({
            fillAlphas: 0.9,
            fixedColumnWidth: 20,
            lineAlpha: 0,
            type: 'column',
            valueField: pc,
            labelText: '',
            balloonText: `${pc.replace('pc', 'PC')}: [[value]]%`,
            fillColors: color,
            lineColor: color,
          });
        }
        // this.graphText = `${maxPc.replace('pc', 'PC')}: ${max}%`;
      } else {
        dataObject.empty = this.maxLength;
        graphs.push({
          fixedColumnWidth: 20,
          fillAlphas: 0,
          lineAlpha: 0,
          labelText: '',
          type: 'column',
          balloonText: '',
          valueField: 'empty',
        });
        // this.graphText = '';
      }

      const data = [dataObject];
      const { id } = this;
      if (window.charts[id]) {
        window.charts[id].graphs = graphs;
        window.charts[id].dataProvider = data;
        window.charts[id].valueAxes[0].maximum = this.maxLength;
        window.charts[id].valueAxes[0].guides[1].value = this.maxLength;
        // window.charts[id].valueAxes[0].guides[1].label = `${this.maxLength}%`;
        window.charts[id].validateData();
      } else {
        const options = {
          hideCredits: true,
          type: 'serial',
          theme: 'light',
          rotate: true,
          fontFamily: 'Fira Sans',
          dataProvider: data,
          startDuration: 0,
          graphs,
          categoryField: 'indexvalue',
          categoryAxis: {
            gridPosition: 'start',
            gridAlpha: 0,
            axisAlpha: 0,
            position: 'top',
            labelsEnabled: false,
          },
          valueAxes: [{
            autoGridCount: false,
            stackType: 'regular',
            minimum: 0,
            maximum: this.maxLength,
            strictMinMax: true,
            gridAlpha: 0.2,
            gridCount: 10,
            fontSize: 13,
            axisAlpha: 0,
            gridThickness: 1,
            ignoreAxisWidth: true,
            labelsEnabled: !this.hideValueAxis,
            labelOffset: -10,
            tickLength: 0,
            // showLastLabel: false,
            guides: [{
              value: 0,
              lineAlpha: 1,
              lineThickness: 2,
              lineColor: '#777777',
            }, {
              value: this.maxLength,
              // label: `${this.maxLength}%`,
              lineAlpha: 1,
              lineThickness: 2,
              lineColor: '#777777',
            }],
            position: 'bottom',
          }],
          chartCursor: {
            valueBalloonsEnabled: false,
            cursorAlpha: 0,
            categoryBalloonEnabled: false,
            oneBalloonOnly: true,
          },
          autoMargins: false,
          marginTop: 0,
          marginBottom: this.hideValueAxis ? 0 : 15,
          marginLeft: 9,
          marginRight: 14,
          pullOutRadius: 0,
          pathToImages: 'https://cdn.amcharts.com/lib/3/images/',
        };
        if (!this.hideValueAxis) {
          const labelFunction = value => `${value}%`;
          options.valueAxes[0].labelFunction = labelFunction;
        }
        window.charts[id] = window.AmCharts.makeChart(this.id, options);
      }
    },
  },
  template: '<div style="position:relative;"><div v-if="graphText" style="top:5px;left:15px;position:absolute">{{graphText}}</div><div class="chart chart-stacked-bar" :id="id" :style="{ height: height + \'px\' }"></div></div>',
});
