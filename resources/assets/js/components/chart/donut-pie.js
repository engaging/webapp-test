/* global window document Vue */

Vue.component('chart-donut-pie', {
  props: {
    title: {
      type: String,
      default: '',
    },
    idname: {
      type: String,
      default: 'no-id',
    },
    main: {
      type: Object,
      default: () => {},
    },
    height: {
      type: Number,
      default: 80,
    },
    max: {
      type: Number,
      default: 1.5,
    },
    radius: {
      type: Number,
      default: 35,
    },
  },
  data() {
    return {
      // trackData: {},
      highlightId: null,
    };
  },
  computed: {
    id() {
      return `donut-pie-chart-${this.idname}`;
    },
    customTitle() {
      if (!this.title) return '';
      if (this.title === 'rSquared') return 'R\u00B2';
      return `${this.title}`;
    },
  },
  watch: {
    main() {
      this.loadChart();
    },
  },
  mounted() {
    this.loadChart();
  },
  beforeDestroy() {
    const { id } = this;
    if (window.charts[id]) {
      window.charts[id].clear();
      delete window.charts[id];
    }
    this.$off();
  },
  methods: {
    loadChart() {
      console.log('small donut-pi charts');
      const { value } = this.main;
      if (value === undefined) return null;
      let data = [];
      if (value >= 0) {
        const item = {
          value,
          color: '#123B69',
          title: 'Beta',
        };
        const maxValue = value > this.max ? 0.3 : this.max - value;
        const total = {
          value: maxValue,
          color: '#F3F3F3',
          title: 'Sum',
        };
        data = [item, total];
      } else if (value === '-') {
        const item = {
          value: 0,
          color: '#123B69',
          title: 'Beta',
        };
        const maxValue = this.max;
        const total = {
          value: maxValue,
          color: '#F3F3F3',
          title: 'Sum',
        };
        data = [item, total];
      } else {
        const posValue = Math.abs(value);
        const item = {
          value: posValue,
          color: '#b7294f',
          title: 'Beta',
        };
        const maxValue = posValue > this.max ? 0.3 : this.max - posValue;
        const total = {
          value: maxValue,
          color: '#F3F3F3',
          title: 'Sum',
        };
        data = [total, item];
      }
      const { id } = this;
      if (window.charts[id]) {
        window.charts[id].dataProvider = data;
        window.charts[id].allLabels[0].text = `${this.main.value}`;
        window.charts[id].validateData();
      } else {
        const labels = [{
          text: `${value}`,
          align: 'center',
          size: 14,
          bold: true,
          y: 26,
        }];
        if (this.customTitle) {
          labels[0].y = 39;
          labels.push({
            text: `${this.customTitle}`,
            align: 'center',
            size: 14,
            bold: true,
            y: 23,
          });
        }
        window.charts[id] = window.AmCharts.makeChart(this.id, {
          hideCredits: true,
          type: 'pie',
          fontFamily: 'Fira Sans',
          theme: 'light',
          dataProvider: data,
          titleField: 'title',
          valueField: 'value',
          colorField: 'color',
          labelRadius: 0,
          marginTop: 0,
          autoMargins: false,
          marginLeft: 0,
          marginRight: 0,
          startDuration: 0,
          radius: this.radius,
          innerRadius: (this.radius - 10),
          startAngle: 90,
          labelsEnabled: false,
          balloon: {
            enabled: false,
          },
          allLabels: labels,
          pathToImages: 'https://cdn.amcharts.com/lib/3/images/',
        });
      }
    },
  },
  template: '<div><div class="chart chart-donut-pie" :id="id" :style="{ height: height + \'px\' }"></div></div>',
});
