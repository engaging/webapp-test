const apiMarket = require('../../api/market');

/* global window analytics Vue $ _ */
Vue.component('market-monitor', {
  props: [
    'widgetid', 'widget', 'user', 'period', 'idname', 'showTable',
  ],
  computed: {
    id() {
      const add = this.idname !== undefined ? this.idname : 'no-id';
      return `returnbubble-chart-${add}`;
    },
    colors() {
      return this.$store.state.colorsAll;
    },
    filters() {
      return this.$store.state.filters;
    },
    countActiveFilters() {
      const filters = this.getFilters;
      let countActiveFilters = 0;
      const filtersToMap = {
        regions: 'region',
        assetClasses: 'assetClass',
        currencies: 'currency',
        factors: 'factor',
        styles: 'style',
        returnTypes: 'returnType',
      };
      const filtersToCount = ['regions', 'assetClasses', 'currencies', 'factors', 'styles', 'returnTypes'];
      filtersToCount.forEach((x) => {
        if (filters[x] && filters[x].length && (filters[x].length !== this.filters[filtersToMap[x]].length)) {
          countActiveFilters++;
        }
      });
      if (this.filtersLoaded === true) {
        try {
          analytics.track('Viewed Market Monitor', _.mapKeys(Object.assign({}, _.omit(filters, [
            'volatilityValues',
            'returnValues',
            'sharpeValues',
            'sponsors',
            'history',
            'keywords',
          ]), {
            period: _.findKey(this.periodKeysForFilters, v => v === this.period) || undefined,
            activesCount: countActiveFilters,
          }), (value, key) => `Filter ${_.startCase(key)}`));
        } catch (e) {
          console.error(e);
        }
      }
      return countActiveFilters;
    },
    getFilters() {
      const filters = {
        regions: this.regions,
        assetClasses: this.assetClasses,
        currencies: this.currencies,
        factors: this.factors,
        styles: this.styles,
        returnTypes: this.returnTypes,
        period: this.period,
      };
      return filters;
    },
  },
  data() {
    return {
      //add lack things in php template, copy strategy.js
      AssetClassCheckboxInactive: false,
      RegionCheckboxInactive: false,
      CurrencyCheckboxInactive: false,
      FactorCheckboxInactive: false,
      StyleCheckboxInactive: false,
      TypeCheckboxInactive: false,
      ReturnTypeCheckboxInactive: false,
      SponsorCheckboxInactive: false,
      HistoryCheckboxInactive: false,
      editing: false,

      showFilters: false,
      filtersLoaded: false,
      dataLoaded: true,

      hideFilters: ['keywords', 'volatility', 'return', 'sharpe', 'sponsor', 'history', 'type'],
      factors: [],
      styles: [],
      returnTypes: [],
      regions: [],
      assetClasses: [],
      currencies: [],

      assetFromSlug: {},
      factorFromSlug: {},
      assetColors: {},

      groupBySelection: 'asset',
      strategyAssets: ['Commodity', 'Credit', 'Equity', 'Fixed Income', 'Foreign Exchange', 'Multi Assets'],
      strategyFactors: ['Carry', 'Volatility', 'Momentum', 'Value', 'Low Vol', 'Quality', 'Size', 'Multi'],
      tableData: null,
      tableDataBySlug: null,
      tableDataBefore: null,
      marketData: [],
      allBubbleIds: {},
      periodKeysForFilters: {
        '10 Year': '10y',
        '5 Year': '5y',
        '3 Year': '3y',
        '1 Year': '1y',
        '6 Month': '6m',
      },
      groupByTextList: {
        asset: 'Group by Asset Class',
        factor: 'Group by Factor',
      },
      groupByText: 'Group by Asset Class',
    };
  },
  watch: {
    groupBySelection(groupBy) {
      if (this.marketData.length) {
        this.group_by(groupBy);
      }
    },
    regions() {
      this.dataLoaded = false;
      this.filterChanged.run();
    },
    assetClasses() {
      this.dataLoaded = false;
      this.filterChanged.run();
    },
    currencies() {
      this.dataLoaded = false;
      this.filterChanged.run();
    },
    factors() {
      this.dataLoaded = false;
      this.filterChanged.run();
    },
    styles() {
      this.dataLoaded = false;
      this.filterChanged.run();
    },
    returnTypes() {
      this.dataLoaded = false;
      this.filterChanged.run();
    },
    period() {
      this.dataLoaded = false;
      this.filterChanged.run();
    },
    colors(newVal) {
      this.assetColors = $.extend(newVal.assetClass, newVal.factor);
      for (const y in this.assetColors) {
        this.assetColors[y] = this.convertHex(this.assetColors[y], 100);
      }
    },
  },
  tasks: t => ({
    filterChanged: t(function* filterChanged() {
      this.setupData();
    }).flow('restart', { delay: 600 }),
  }),
  methods: {
    chooseItem(val) {
      if (this.groupBySelection !== val) {
        this.groupBySelection = val;
        this.groupByText = this.groupByTextList[val];
      }
    },
	  slugify(text) {
      return text.toString().toLowerCase()
        .replace(/\s+/g, '-') // Replace spaces with -
        .replace(/[^\w\-]+/g, '') // Remove all non-word chars
        .replace(/\-\-+/g, '-') // Replace multiple - with single -
        .replace(/^-+/, '') // Trim - from start of text
        .replace(/-+$/, ''); // Trim - from end of text
    },
	  convertHex(hex, opacity) {
      hex = hex.replace('#', '');
      const r = parseInt(hex.substring(0, 2), 16);
      const g = parseInt(hex.substring(2, 4), 16);
      const b = parseInt(hex.substring(4, 6), 16);
      const result = `rgba(${r},${g},${b},${opacity / 100})`;
      return result;
    },
    async initMarketData() {
      await this.getMarketData(this.getFilters);
      this.filtersLoaded = true;
    },
    async getMarketData(filters) {
      filters = _.pick(filters, [
        'period',
        'factors',
        'assetClasses',
        'regions',
        'currencies',
        'styles',
        'returnTypes',
      ]);
      const rows = await apiMarket.getMarket(filters).then(o => o.data || []);
      this.marketData = rows.map(o => Object.assign({ filters }, o));

      const lines = {};
      for (let r = 0; r < rows.length; r++) { //skip header row
        const row = rows[r];
        if (!lines[row.asset]) {
          lines[row.asset] = {};
        }
        if (!lines[row.asset][row.factor]) {
          lines[row.asset][row.factor] = row.return;
        }
        const bubbleId = `${this.slugify(row.asset)}_____${this.slugify(row.factor)}`;
        this.assetFromSlug[this.slugify(row.asset)] = row.asset;
        this.factorFromSlug[this.slugify(row.factor)] = row.factor;
        if (!this.allBubbleIds[bubbleId]) {
          this.allBubbleIds[bubbleId] = bubbleId;
        }
      }
      for (let x = 0; x < this.strategyAssets.length; x++) {
        const asset = this.strategyAssets[x];
        if (!lines[asset]) {
          lines[asset] = {};
        }
        for (let y = 0; y < this.strategyFactors.length; y++) {
          const factor = this.strategyFactors[y];
          if (!lines[asset][factor]) {
            lines[asset][factor] = 'n/a';
          }
          const bubbleOtherId = `${this.slugify(asset)}_____${this.slugify(factor)}`;
          if (!this.allBubbleIds[bubbleOtherId]) {
            this.allBubbleIds[bubbleOtherId] = bubbleOtherId;
          }
        }
      }

      this.tableDataBefore = this.tableData;
      this.tableData = lines;
      this.tableDataBySlug = {};
      for (const asset in this.tableData) {
        this.tableDataBySlug[this.slugify(asset)] = {};
        for (const factor in this.tableData[asset]) {
          this.tableDataBySlug[this.slugify(asset)][this.slugify(factor)] = this.tableData[asset][factor];
          if (this.tableData[asset][factor] !== 'n/a') {
            this.tableData[asset][factor] = parseFloat(this.tableData[asset][factor]) * 100;
            this.tableData[asset][factor] = this.tableData[asset][factor].toFixed(2);
          }
        }
      }
      const that = this;
      setTimeout(() => {
        // console.log('svgChart:', !!window.svgChart);
        if (!window.svgChart) {
          that.setupChart();
          that.group_by(that.groupBySelection);
        } else {
          that.displayDiff();
        }
      }, 50);
    },
    async setupData() {
      await this.getMarketData(this.getFilters);
      this.dataLoaded = true;
    },

    sortTable(field) {
      if (this.widget.properties.sortBy === field) {
        // flip the order
        if (this.widget.properties.sortDirection === 'asc') {
          this.widget.properties.sortDirection = 'desc';
        } else {
          this.widget.properties.sortDirection = 'asc';
        }
      }
      this.widget.properties.sortBy = field;

      // sort the tableData here - actually are sorting the val of tabledata[sortby] and getting the factor order
      const tosort = this.tableData[this.widget.properties.sortBy];

      const sortable = [];
      const factorsNew = [];
      for (const item in tosort) {
        sortable.push([item, tosort[item]]);
      }

      if (this.widget.properties.sortDirection === 'asc') {
        sortable.sort((a, b) => a[1] - b[1]);
      } else {
        sortable.sort((a, b) => b[1] - a[1]);
      }

      for (let i = 0; i < sortable.length; i++) {
        factorsNew.push();
      }
      for (let i = 0; i < sortable.length; i++) {
        factorsNew.push(sortable[i][0]);
      }
      this.strategyFactors = factorsNew;
    },

    render_chart(csv) {
      const that = this;
      window.svgChart = new BubbleChart(csv);
      window.svgChart.start();
      return that.display_all();
    },

    render_vis(csv) {
      const that = this;
      return that.render_chart(csv);
    },

    display_all() {
      const a = window.svgChart.display_group_all();
      return a;
    },

    group_by(groupBy) {
      if (groupBy === '') {
        return window.svgChart.display_group_all();
      }
      return window.svgChart.group_by(groupBy);
	  },
	  displayDiff() {
      const that = this;
      let min = 0;
      let max = 0;
      for (let j = 0; j < that.marketData.length; j++) {
        let x = that.marketData[j].return;
        x = parseFloat(x);
        if (x < min) {
          min = x;
        }
        if (x > max) {
          max = x;
        }
      }
      for (const bubbleId in this.allBubbleIds) {
        const toks = bubbleId.split('_____');
        const asset = toks[0];
        const factor = toks[1];
        if (that.tableDataBySlug[asset] && that.tableDataBySlug[asset][factor]) {
          if (that.tableDataBySlug[asset][factor] === 'n/a') {
            d3.select(`#bubble_${bubbleId}`).transition().attr('r', 0);
          } else {
            const node = window.svgChart.get_node_info({ return: that.tableDataBySlug[asset][factor], slug: bubbleId }, 0, this.marketData);
            const original = JSON.stringify(this.getData(asset, factor));
            const fill = window.svgChart.get_rgb(parseFloat(that.tableDataBySlug[asset][factor]), min, max);
            d3.select(`#bubble_${bubbleId}`)
              .transition()
              .attr('original', original)
              .attr('cy', node.y)
              .attr('r', node.radius)
              .style('fill', fill);
          }
        }
      }
      that.group_by(that.groupBySelection);
    },
    getData(asset, factor) {
      asset = this.assetFromSlug[asset];
      factor = this.factorFromSlug[factor];
      for (let i = 0; i < this.marketData.length; i++) {
        if (this.marketData[i].asset === asset && this.marketData[i].factor === factor) {
          return this.marketData[i];
        }
      }
      return {};
    },
    setupChart() {
      return this.render_vis(this.marketData);
    },
  },
  created() {
    this.initMarketData();
  },
  mounted() {
    window.svgChart = null;
    window.groupIntoOther = ['Size', 'Quality', 'Low Vol'];

    if ($(window).width() <= 768) {
      this.showTable = true;
    }
    $(this.$el).hover(() => {}, () => {
      $('#popover-test').removeClass('in');
    });
  },
});
