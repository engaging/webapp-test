/* global Vue _ */
Vue.component('chart-contribution', {
  template: '<div><h4 class="print-border charttitle">{{title + " (%)"}}</h4><div v-show="hiddenPeriods[period]" class="chart chart-contribution chart-not-available"></div><div v-show="!hiddenPeriods[period] && !isAvailable" class="chart chart-contribution chart-not-available">This chart is not available for this portfolio.</div><div v-show="!hiddenPeriods[period] && isAvailable" class="chart chart-contribution" :id="id" style="height:308px;"></div></div>',
  props: [
    'title', 'idname', 'type', 'main', 'period', 'indices', 'setEvent', 'datasets', 'colors',
  ],
  data() {
    return {
      typeSelect: {
        var: 'incremental_var',
        volatility: 'incremental_volatility',
        return: 'incremental_return',
      },
      isAvailable: true,
      // chart: null,
    };
  },
  computed: {
    id() {
      const add = this.idname !== undefined ? this.idname : 'no-id';
      return `contributio-chart-${add}`;
    },
    hiddenPeriods() {
      return this.$store.state.hiddenPeriods;
    },
  },
  watch: {
    period() {
      this.loadChart();
    },
    type() {
      this.loadChart();
    },
    main() {
      if (!this.colors) {
        this.loadChart();
      }
    },
    datasets() {
      this.loadChart();
    },
  },
  methods: {
    loadChart() {
      console.log('small return contribution bars');
      const that = this;
      const current = this.main;
      const period = this.period;
      let rawData = {};
      const dataKey = `${period}_${this.typeSelect[this.type]}`;
      if (current[dataKey] !== undefined) {
        rawData = current[dataKey];
      }

      this.isAvailable = !_.isEmpty(rawData);
      if (!this.isAvailable) {
        return;
      }

      const coloredColumn = {};
      if (this.colors) {
        for (let idx = 1; idx < this.datasets.length; idx++) {
          const item = this.datasets[idx];
          coloredColumn[item.code] = this.colors[item.color];
        }
      }

      const data = [];
      let min = 0;
      for (const i in rawData) {
        const indexInfo = this.indices.find(o => o.indexinfo.code === i).indexinfo;
        const floatData = parseFloat(rawData[i]);
        const dataToPush = {
          title: indexInfo.shortName,
          value: rawData[i],
          itemId: `contribution-${indexInfo.id}`,
          itemCode: indexInfo.code,
        };
        if (coloredColumn[i] !== undefined) {
          dataToPush.fillColor = coloredColumn[i];
        } else if (floatData < 0) {
          dataToPush.fillColor = '#999999';
        } else {
          dataToPush.fillColor = '#123B69';
        }
        if (floatData < min && floatData < 0) {
          min = floatData;
        }
        data.push(dataToPush);
      }

      const graphs = [{
        id: `${this.type}-${this.main.id}`,
        balloonText: '[[title]]<br>[[value]] %',
        valueField: 'value',
        type: 'column',
        lineColorField: 'fillColor',
        fillColorsField: 'fillColor',
        fillAlphas: 1,
        negativeBase: 0,
        negativeFillAlphas: 1,
        classNameField: 'itemId',
      },
      ];
      if (data.length <= 8) {
        graphs[0].fixedColumnWidth = 25;
      }
      const { id } = this;
      if (window.charts[id]) {
        window.charts[id].graphs = graphs;
        window.charts[id].valueAxes[0].minimum = min;
        window.charts[id].dataProvider = data;
        window.charts[id].validateData();
      } else {
        window.charts[id] = AmCharts.makeChart(that.id, {
          hideCredits: true,
          fontFamily: 'Fira Sans',
          type: 'serial',
          theme: 'light',
          autoMarginOffset: 30,
          addClassNames: true,
          guides: [{
            lineAlpha: 1,
            value: 0,
          }],
          graphs,
          categoryField: 'itemCode',
          valueAxes: [{
            minimum: min,
            axisAlpha: 0,
          }],
          categoryAxis: {
            minHorizontalGap: 30,
            gridAlpha: 0,
            gridThickness: 0,
            minorGridEnabled: true,
            labelsEnabled: false,
            axisAlpha: 0,
          },
          dataProvider: data,
        });
        if (this.setEvent) {
          this.setEvents();
        }
      }
    },
    setEvents() {
      const { id } = this;
      window.charts[id].addListener('rollOverGraph', (event) => {
        const className = event.event.currentTarget.classList;
        const id = className[className.length - 1].split('-')[1];
        const indexToHighlight = {
          indexinfo: {
            id,
          },
        };
        window.eventBus.$emit('highlightIndex', indexToHighlight);
      });

      window.charts[id].addListener('rollOutGraph', (event) => {
        const className = event.event.currentTarget.classList;
        const id = className[className.length - 1].split('-')[1];
        const indexToHighlight = {
          indexinfo: {
            id,
          },
        };
        window.eventBus.$emit('unhighlightIndex', indexToHighlight);
      });
    },
  },

  mounted() {
    this.loadChart();
  },

  beforeDestroy() {
    const { id } = this;
    if (window.charts[id]) {
      window.charts[id].clear();
      delete window.charts[id];
    }
    this.$off();
  },

});
