/* global window Vue AmCharts $ */
import groupSelection from '../vue-select/group-selection.vue';

const ACTION = require('../../store/mutation-type');
const { PERIOD } = require('../../util/period');

Vue.component('new-chart-correlationscatter', {
  components: {
    'group-selection': groupSelection,
  },
  props: {
    idname: {
      type: String,
      default: 'no-id',
    },
    type: {
      type: String,
      default: 'portfolio',
    },
    mainId: {
      type: Number,
      default: 0,
    },
    datasets: {
      type: Array,
      default: () => [],
    },
    loading: {
      type: Boolean,
      default: false,
    },
    isInvalid: {
      type: Boolean,
      default: false,
    },
    activeStrategies: {
      type: Number,
      default: 0,
    },
    isSubgraphy: {
      type: Boolean,
      default: false,
    },
    colors: {
      type: Object,
      default: () => {},
    },
    periodGroup: {
      type: Object,
      default: () => {},
    },
  },
  data() {
    return {
      benchmark1Selection: [780], // 'SPX'
      benchmark2Selection: [809], //'ITRROV'
      benchmark1title: 'Select',
      benchmark2title: 'Select',
      needToLoad: false,
      trackLoading: false,
    };
  },
  computed: {
    id() {
      return `correlationscatter-chart-${this.idname}`;
    },
    strategies() {
      return this.$store.state.cor.strategies || {};
    },
    portfolios() {
      return this.$store.state.cor.portfolios || {};
    },
    benchmarks() {
      const { strategies } = this.$store.state;
      const benchmarks = {};
      for (const id in strategies) {
        if (strategies[id] && strategies[id].type === 'Benchmark') {
          benchmarks[id] = strategies[id];
        }
      }
      return benchmarks;
    },
    benchmarkOptions() {
      const groups = {};
      for (const id in this.benchmarks) {
        const o = this.benchmarks[id];
        if (!groups[o.assetClass]) {
          groups[o.assetClass] = { [o.id]: `${o.shortName} (${o.code})` };
        } else {
          groups[o.assetClass][o.id] = `${o.shortName} (${o.code})`;
        }
      }
      return groups;
    },
    hiddenPeriods() {
      return this.$store.state.hiddenPeriods;
    },
  },
  watch: {
    datasets() {
      this.start();
    },
  },
  mounted() {
    console.log('picture mounted');
    const benchmark1 = this.benchmarks[this.benchmark1Selection[0]];
    this.benchmark1title = !this.isSubgraphy
      ? `${benchmark1.shortName} (${benchmark1.code})`
      : benchmark1.code;
    const benchmark2 = this.benchmarks[this.benchmark2Selection[0]];
    this.benchmark2title = !this.isSubgraphy
      ? `${benchmark2.shortName} (${benchmark2.code})`
      : benchmark2.code;
    this.start();
    // this.loadData();
  },
  beforeDestroy() {
    const { id } = this;
    if (window.charts[id] != null) {
      window.charts[id].clear();
      delete window.charts[id];
    }
    this.$off();
  },
  methods: {
    setListItem1(id) {
      const newValue = this.benchmarks[id];
      this.benchmark1Selection = [id];
      this.benchmark1title = !this.isSubgraphy
        ? `${newValue.shortName} (${newValue.code})`
        : newValue.code;
      this.start();
    },
    setListItem2(id) {
      const newValue = this.benchmarks[id];
      this.benchmark2Selection = [id];
      this.benchmark2title = !this.isSubgraphy
        ? `${newValue.shortName} (${newValue.code})`
        : newValue.code;
      this.start();
    },
    async start() {
      if (!this.activeStrategies || this.isInvalid) return null;
      if (this.benchmark1Selection.length > 0 && this.benchmark2Selection.length > 0 && this.mainId) {
        const { mainId } = this;
        const ids = [this.benchmark1Selection[0], this.benchmark2Selection[0]];
        const otherIds = [];
        if (this.datasets.length > 1) {
          for (let x = 1; x < this.datasets.length; x++) {
            otherIds.push(this.datasets[x].id);
          }
        }

        const needToLoad = this.checkData(mainId, ids, otherIds);

        this.trackLoading = true;
        if (needToLoad) {
          await this.$store.dispatch(ACTION.API_FETCH_BENCHMARKS_CORRELATION, {
            id: mainId,
            type: this.type,
            freeze: true,
            payload: {
              periods: this.periodGroup.periods,
              maxDate: this.periodGroup.dataPeriod === PERIOD.CUST ? this.periodGroup.custMaxDate : this.periodGroup.maxDate,
              ids: ids,
              period: this.periodGroup.dataPeriod,
              portfolioOnly: false,
            },
          });
        }
        this.loadChart(mainId, ids, otherIds);
      }
    },
    checkData(mainId, ids, otherIds) {
      const period = this.periodGroup.dataPeriod;
      const strategyData = this.strategies[period];
      const portfolioData = this.portfolios[period];
      if (!portfolioData || Object.keys(portfolioData).length === 0) return true;
      if (!portfolioData[`${mainId}-${ids[0]}`] || !portfolioData[`${mainId}-${ids[1]}`]) return true;
      if (!strategyData || Object.keys(strategyData).length === 0) return true;
      for (let idx = 0; idx < otherIds.length; idx++) {
        const key1 = otherIds[idx] < ids[0] ? `${otherIds[idx]}-${ids[0]}`
          : `${ids[0]}-${otherIds[idx]}`;
        const key2 = otherIds[idx] < ids[1] ? `${otherIds[idx]}-${ids[1]}`
          : `${ids[1]}-${otherIds[idx]}`;
        if (!strategyData[key1] || !strategyData[key2]) return true;
      }
      return false;
    },
    loadChart(mainId, ids, otherIds) {
      console.log('small correlationscatter');
      const benchmark1 = this.benchmarks[this.benchmark1Selection[0]];
      const benchmark2 = this.benchmarks[this.benchmark2Selection[0]];
      const { id } = this;
      const graphs = [];
      const data = {};
      // Options have been chosen
      const key = this.periodGroup.dataPeriod;
      const mainData = this.type === 'strategy' ? this.strategies[key] : this.portfolios[key];
      const key1 = mainId < ids[0] ? `${mainId}-${ids[0]}`
        : `${ids[0]}-${mainId}`;
      const key2 = mainId < ids[1] ? `${mainId}-${ids[1]}`
        : `${ids[1]}-${mainId}`;
      data.benchmarkx = parseFloat(mainData[key1]).toFixed(2);
      data.benchmarky = (ids[0] === ids[1]) ? data.benchmarkx : parseFloat(mainData[key2]).toFixed(2);
      graphs.push({
        // "balloonText": 'Correlation<br>',
        balloonText: `Portfolio:<br>${benchmark1.shortName}: [[x]]<br>${benchmark2.shortName}: [[y]]`,
        bullet: 'round',
        lineAlpha: 0,
        xField: 'benchmarkx',
        yField: 'benchmarky',
        lineColor: '#123B69',
        fillAlphas: 0,
        id_content: 'avg',
      });

      otherIds.forEach((x) => {
        const key1 = x < ids[0] ? `${x}-${ids[0]}`
          : `${ids[0]}-${x}`;
        const key2 = x < ids[1] ? `${x}-${ids[1]}`
          : `${ids[1]}-${x}`;
        data[`strategyx-${x}`] = parseFloat(this.strategies[key][key1]).toFixed(2);
        data[`strategyy-${x}`] = (ids[0] === ids[1]) ? data[`strategyx-${x}`] : parseFloat(this.strategies[key][key2]).toFixed(2);

        let color = '#999999';
        let size = 6;
        let title = '';
        const idx = parseInt(x, 10);
        for (let y = 1; y < this.datasets.length; y++) {
          if (idx === this.datasets[y].id) {
            title = this.datasets[y].shortName;
            if (this.datasets[y].color) { //pinned point
              color = this.colors[this.datasets[y].color];
              size = 10;
            }
          }
        }

        graphs.push({
          // "balloonText": 'Correlation<br>',
          showBalloon: true,
          balloonText: `${title}<br>${benchmark1.shortName}: [[x]]<br>${benchmark2.shortName}: [[y]]`,
          bullet: 'round',
          lineAlpha: 0,
          xField: `strategyx-${x}`,
          yField: `strategyy-${x}`,
          lineColor: color,
          fillAlphas: 0,
          bulletSize: size,
          id_content: x,
          id: `correlationscatter-${x}`,
        });
      });

      if (!window.charts[id]) {
        window.charts[id] = AmCharts.makeChart(id, {
          hideCredits: true,
          type: 'xy',
          theme: 'light',
          autoMarginOffset: 20,
          // autoMargins: false, // breaking display
          fontFamily: 'Fira Sans',
          dataProvider: [data],
          height: this.isSubgraphy ? 305 : 450,
          addClassNames: true,
          valueAxes: [{
            position: 'bottom',
            axisAlpha: 0,
            dashLength: 1,
            title: this.benchmark1title,
            maximum: 1,
            minimum: -1,
            id: 'x_axis',
          }, {
            axisAlpha: 0,
            dashLength: 1,
            position: 'left',
            title: this.benchmark2title,
            maximum: 1,
            minimum: -1,
            id: 'y_axis',
            synchronizeWith: 'x_axis',
            synchronizationMultiplier: 1,
          }],
          startDuration: 0,
          graphs,
          trendLines: [],
          marginLeft: 64,
          marginBottom: 60,
          chartScrollbarOff: {},
          chartCursor: {
            cursorPosition: 'start',
            pan: false,
            valueLineEnabled: false,
            valueLineBalloonEnabled: false,
            valueBalloonsEnabled: true,
            cursorAlpha: 0,
            oneBalloonOnly: true,
          },
          // marginTop: 20,
          // marginRight: 20,
          zoomOutText: '',
          zoomOutButtonImage: '',
          maxZoomFactor: 1,
          pathToImages: 'https://cdn.amcharts.com/lib/3/images/',
        });
        this.setEvents();
        // window.charts[id].validateData();
      } else {
        window.charts[id].valueAxes[0].title = this.benchmark1title;
        window.charts[id].valueAxes[1].title = this.benchmark2title;
        window.charts[id].graphs = graphs;
        window.charts[id].dataProvider = [data];
        window.charts[id].validateData();
      }
      this.trackLoading = false;
    },
    setEvents() {
      const that = this;
      const { id } = this;
      if (!this.isSubgraphy) {
        window.charts[id].addListener('rollOverGraph', (event) => {
          const id = event.graph.id_content;
          if (id !== undefined && id !== that.mainId) {
            const indexToHighlight = {
              indexinfo: {
                id,
              },
            };
            window.eventBus.$emit('highlightIndex', indexToHighlight);
          }
        });

        window.charts[id].addListener('rollOutGraph', (event) => {
          const id = event.graph.id_content;
          if (id !== undefined && id !== that.mainId) {
            const indexToHighlight = {
              indexinfo: {
                id,
              },
            };
            window.eventBus.$emit('unhighlightIndex', indexToHighlight);
          }
        });

        window.charts[id].addListener('clickGraphItem', (event) => {
          const id = event.graph.id_content;
          if (id !== undefined && id !== that.mainId) {
            const $buttons = $(`#row-${id}`).find('button');
            if ($buttons.length) $buttons[0].click();
          }
        });
      }
    },
  },
  template: `
    <div style="position:relative;">
      <div v-show="hiddenPeriods[periodGroup.dataPeriod]" :class="{ 'chart-correlationscatter-big': !isSubgraphy }" class="chart chart-proportionate chart-correlationscatter chart-not-available">
      </div>
      <div v-show="!hiddenPeriods[periodGroup.dataPeriod]">
        <div class="chartfilter">
          <div class="clearfix">
            <div class="col-xs-6">
              <group-selection
                :selection="benchmark1Selection"
                :set-list-item="setListItem1"
                :groups="benchmarkOptions"
                :shown-selection="benchmark1title"
                :is-primary="true"/>
            </div>
            <div class="col-xs-6">
              <group-selection
                :selection="benchmark2Selection"
                :set-list-item="setListItem2"
                :groups="benchmarkOptions"
                :shown-selection="benchmark2title"
                :is-primary="true"/>
            </div>
          </div>
        </div>
        <div class="abs-loader" v-show="!loading && trackLoading">
          <div class="loader-inner ball-pulse"><div></div><div></div><div></div></div>
        </div>
        <div :class="{ 'chart-correlationscatter-big': !isSubgraphy }" class="chart chart-proportionate chart-correlationscatter" :id="id"></div>
      </div>
    </div>
  `,
});
