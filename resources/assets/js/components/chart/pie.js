Vue.component('chart-pie', {
  template: '<div><h4 v-if="title && title.length" style="margin-bottom:30px;" class="charttitle">{{title}}</h4><div class="chart chart-pie" :id="id"></div> <div v-show="dataLoading == true" class="abs-loader"><div class="loader-inner ball-pulse"><div></div><div></div><div></div></div></div></div>',
  props: [
    'title', 'middletitle', 'idname', 'type', 'main', 'datasets', 'legend', 'legendposition', 'legendtop', 'radius', 'innerradius', 'colors',
  ],
  computed: {
    id() {
      const add = this.idname !== undefined ? this.idname : 'no-id';
      return `pie-chart-${add}`;
    },
  },
  data() {
    return {
      dataLoading: true,
      // chart: null,
    };
  },
  watch: {
    datasets() {
      this.loadChart();
    },
    colors() {
      this.loadChart();
    },
  },
  methods: {
    loadChart() {
      const datasets = this.datasets;
      const data = [];
      for (const dataset of datasets) {
        let color = '#dddddd';
        if (this.colors[dataset.title] !== undefined) {
          color = this.colors[dataset.title];
        }

        data.push({
          field: dataset.title,
          value: parseFloat(dataset.amount).toFixed(2),
          color,
        });
      }

      const that = this;
      const title = that.middletitle !== undefined ? that.middletitle : '';

      const options = {
        hideCredits: true,
        type: 'pie',
        fontFamily: 'Fira Sans',
        theme: 'light',
        // radius: 150,
        // innerRadius: 80,
        radius: that.radius,
        innerRadius: that.innerradius,
        labelsEnabled: false,
        autoMargins: false,
        marginTop: -90,
        marginLeft: 0,
        marginRight: 0,
        dataProvider: data,
        valueField: 'value',
        titleField: 'field',
        colorField: 'color',
        labelColorField: 'color',
        startDuration: 0,
        addClassNames: true,
        outlineThickness: 0,
        pullOutEffect: 'easeOutSine',
        pullOutDuration: 0.3,
        pullOutRadius: 10,
        balloonText: '[[title]]: [[percents]]%',
        balloon: {
          fontSize: 10,
        },
        allLabels: [{
          y: '39%',
          // y: that.innerradius,
          align: 'center',
          size: 14,
          text: title,
          color: '#555',
        }],
      };

      if (this.legend === 'true' || this.legend === true) {
        options.legend = {
          switchable: false,
          top: that.legendtop ? that.legendtop : (that.radius * 2.5),
          spacing: that.radius > 100 ? 8 : 1,
          labelWidth: 75,
          width: that.radius * 2,
          maxColumns: 3,
          equalWidths: true,
          fontSize: 10,
          valueText: '([[percents]]%)',
          // position: 'absolute',
          // left: 0,
        };

        if (this.legendposition && this.legendposition !== 'absolute') {
          options.legend.position = this.legendposition;
        } else {
          options.legend.position = 'absolute';
          options.legend.left = 0;
        }
      }
      const { id } = this;
      if (window.charts[id]) {
        window.charts[id].allLabels[0].text = title;
        window.charts[id].dataProvider = data;
        window.charts[id].validateData();
      }
      window.charts[id] = window.AmCharts.makeChart(that.id, options);
      window.charts[id].addListener('rollOverSlice', (e) => {
        window.charts[id].clickSlice(e.dataItem);
      });
      window.charts[id].addListener('rollOutSlice', (e) => {
        window.charts[id].clickSlice(e.dataItem);
      });

      this.dataLoading = false;
    },
  },
  mounted() {
    this.loadChart();
  },
  beforeDestroy() {
    const { id } = this;
    if (window.charts[id]) {
      window.charts[id].clear();
      delete window.charts[id];
    }
    this.$off();
  },
});
