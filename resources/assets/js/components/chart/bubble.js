Vue.component('chart-bubble', {
  template: '<div><h4 v-show="title && title.length" class="print-border charttitle">{{title}}</h4> <div class="chart-bubble" :id="id"></div></div>',
  props: [
    'title', 'idname', 'properties', 'colorby', 'width', 'height',
  ],
  computed: {
    id() {
      const add = this.idname != undefined ? this.idname : 'no-id';
      return `returnbubble-chart-${add}`;
    },
  },
  data() {
    return {
      assetColors: {
        Carry: '#85438B',
        Volatility: '#037870',
        Momentum: '#C56245',
        Value: '#4CADB3',
        LowVol: '#25AA4B',
        Quality: '#0360C9',
        Size: '#A8AE59',
        Multi: '#dddddd',
        Commodity: '#85438B',
        Credit: '#037870',
        Equity: '#C56245',
        FixedIncome: '#25AA4B',
        ForeignExchange: '#0360C9',
        MultiAssets: '#A8AE59',

      },
      doneLoading: false,
      colorBy: 'return',
    };
  },
  watch: {

    colorby() {
      console.log('update bubble');
      this.loadChart();
    },

  },
  methods: {

    loadChart() {
      console.log(`load bubble chart ${this.id}`);

      const that = this;

      if (this.properties) {
        if (this.properties.colorBy) {
          this.colorBy = this.properties.colorBy;
        }
      }
      // clear old graphs
      d3.select(`#${that.id}`).selectAll('svg').remove();


      let width = 130,
        height = 370,
        padding = 10, // separation between same-color nodes
        clusterPadding = 10, // separation between different-color nodes
        maxRadius = 5;
      const data = [
        {
          name: 'Carry',
          return: (Math.random() * 200) - 100,
        },
        {
          name: 'Volatility',
          return: (Math.random() * 200) - 100,
        },
        {
          name: 'Momentum',
          return: (Math.random() * 200) - 100,
        },
        {
          name: 'Value',
          return: (Math.random() * 200) - 100,
        },
        {
          name: 'LowVol',
          return: (Math.random() * 200) - 100,
        },
        {
          name: 'Quality',
          return: (Math.random() * 200) - 100,
        },
        {
          name: 'Size',
          return: (Math.random() * 200) - 100,
        },
        {
          name: 'Multi',
          return: (Math.random() * 200) - 100,
        },
      ]; //order them by return and the graph will be mostly green > red unless all dots small

      //issue: lots of large dots willnot be able to share space without overlap. could happen.
      ///might have to scale so the total circlearea dont equal ~50% of the w*h?
      let radiusscale = 1;
      let area = 0;
      for (let i = data.length - 1; i >= 0; i--) {
        const r = Math.abs(data[i].return);
        area += (r * r * Math.PI);
      }

      if (area > (width * height) * 0.6) {
        radiusscale = 0.8; //ugh
      }
      // means you shouldnt use y axis to represent % as well as size....

      let n = data.length, // total number of nodes
        m = 1; // number of distinct clusters
      //making a scale

      const scale = d3.scale.linear().domain([0, 100]).range([5, 50]);
      const color = d3.scale.category10().domain(d3.range(m));

      // The largest node for each cluster.
      const clusters = new Array(1);
      const nodes = data.map((d) => {
        var i = 0,
          // r = scale(d.value),
          r = scale(Math.floor(Math.abs(d.return))),
          d = {
            cluster: i, radius: r * radiusscale, name: d.name, return: d.return,
          };
        if (!clusters[i] || (r > clusters[i].radius)) { clusters[i] = d; }
        return d;
      });


      // Use the pack layout to initialize node positions.
      d3.layout.pack()
        .sort(null)
        .size([width, height])
        .children(d => d.values)
        .value(d => d.radius * d.radius)
        .nodes({
          values: d3.nest()
            .key(d => d.cluster)
            .entries(nodes),
        });

      const force = d3.layout.force()
        .nodes(nodes)
        .size([width, height])
        .gravity(0.02)
        .charge(0)
        .on('tick', tick)
        .start();

      const svg = d3.select(`#${that.id}`).append('svg')
        .attr('width', width)
        .attr('height', height);

      const node = svg.selectAll('circle')
        .data(nodes)
        .enter().append('g')
        .attr('class', 'node')
        .append('circle')
        .style('fill', (d) => {
          if (that.colorBy == 'factor') {
            if (d.name in that.assetColors) {
              return that.assetColors[d.name];
            }

            return '#ddd';
          }

          const green = { r: 41, g: 185, b: 153 };
          const red = { r: 250, g: 41, b: 79 };
          const returnratio = (d.return + 100) / 200;
          const newr = red.r + Math.floor((green.r - red.r) * returnratio);
          const newg = red.g + Math.floor((green.g - red.g) * returnratio);
          const newb = red.b + Math.floor((green.b - red.b) * returnratio);
          return `rgb(${newr},${newg},${newb})`;
        });

      svg.selectAll('g')
        .append('text')
        .style('text-anchor', 'middle')
        .attr('font-size', '12')
        .attr('fill', '#444')
        .attr('transform', d => `translate(${d.x},${d.y})`)
        .text(d => `${d.name} (${d.return.toFixed(2)}%)`)
        .on('click', () => {
          // maybe go to a listing
          console.log('clicked a type');
          d3.event.stopPropagation();
          window.open('/strategy');
        });


      node.transition()
        .duration(750)
        .delay((d, i) => i * 5)
        .attrTween('r', (d) => {
          const i = d3.interpolate(0, d.radius);
          return function (t) { return d.radius = i(t); };
        });


      function tick(e) {
        node
          .each(cluster(10 * e.alpha * e.alpha))
          .each(collide(0.5))
          .attr('cx', d => d.x = Math.max(50, Math.min(width - 50, d.x)))
          .attr('cy', d => d.y = Math.max(50, Math.min(height - 50, d.y)));

        svg.selectAll('text').attr('transform', d => `translate(${d.x},${d.y})`);
      }
      // Move d to be adjacent to the cluster node.
      function cluster(alpha) {
        return function (d) {
          const cluster = clusters[d.cluster];
          if (cluster === d) return;
          let x = d.x - cluster.x,
            y = d.y - cluster.y,
            l = Math.sqrt(x * x + y * y),
            r = d.radius + cluster.radius;
          if (l != r) {
            l = (l - r) / l * alpha;
            d.x -= x *= l;
            d.y -= y *= l;
            cluster.x += x;
            cluster.y += y;
          }
        };
      }
      // Resolves collisions between d and all other circles.
      function collide(alpha) {
        const quadtree = d3.geom.quadtree(nodes);
        return function (d) {
          let r = d.radius + maxRadius + Math.max(padding, clusterPadding),
            nx1 = d.x - r,
            nx2 = d.x + r,
            ny1 = d.y - r,
            ny2 = d.y + r;
          quadtree.visit((quad, x1, y1, x2, y2) => {
            if (quad.point && (quad.point !== d)) {
              let x = d.x - quad.point.x,
                y = d.y - quad.point.y,
                l = Math.sqrt(x * x + y * y),
                r = d.radius + quad.point.radius + (d.cluster === quad.point.cluster ? padding : clusterPadding);
              if (l < r) {
                l = (l - r) / l * alpha;
                d.x -= x *= l;
                d.y -= y *= l;
                quad.point.x += x;
                quad.point.y += y;
              }
            }
            return x1 > nx2 || x2 < nx1 || y1 > ny2 || y2 < ny1;
          });
        };
      }
    },
  },
  mounted() {
    // this.$nextTick(
    //   () => {
    this.loadChart();
    //   }
    // )
  },
});
