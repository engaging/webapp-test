Vue.component('chart-returnbar', {
  template: '<div><h4 class="print-border charttitle">{{title}}</h4><div v-show="hiddenPeriods[period]" class="chart chart-returnbar chart-not-available"></div><div v-show="!hiddenPeriods[period]" class="chart chart-returnbar" :id="id" style="height:360px;"></div></div>',
  props: [
    'title', 'idname', 'type', 'main', 'period',
  ],
  data() {
    return {
      trackData: {},
    };
  },
  computed: {
    id() {
      const add = this.idname !== undefined ? this.idname : 'no-id';
      return `returnbar-chart-${add}`;
    },
    hiddenPeriods() {
      return this.$store.state.hiddenPeriods;
    },
  },
  watch: {
    period() {
      this.loadChart();
    },
    main() {
      this.loadChart();
    },
  },
  methods: {
    // normalDensityZx(x, Mean, StdDev) {
    //   const a = x - Mean;
    //   return Math.exp(-(a * a) / (2 * StdDev * StdDev)) / (Math.sqrt(2 * Math.PI) * StdDev);
    // },
    // changeData(min, max, mean, stdDev) {
    //   const data = [];
    //   for (let idx = min * 10 - 4; idx <= max * 10 + 4; idx++) {
    //     if (idx % 10 !== 0) {
    //       const xAxisValue = idx / 10;
    //       const yAxisValue = this.normalDensityZx(xAxisValue, mean, stdDev);
    //       const value = {
    //         return: xAxisValue.toFixed(1),
    //         lineReturn: yAxisValue.toFixed(5),
    //         lineTitle: `${xAxisValue.toFixed(1)}%`,
    //         lineValue: yAxisValue.toFixed(3),
    //       };
    //       const mol = idx % 10;
    //       if (mol >= 7 || (mol < 0 && mol > -3)) {
    //         const currentIdx = Math.floor(idx / 10 + 1);
    //         value.columnReturn = this.trackData[currentIdx].columnReturn;
    //         value.columnTitle = `${currentIdx}% to ${currentIdx + 1}%`;
    //       } else if ((mol <= 3 && mol > 0) || mol <= -7) {
    //         const currentIdx = Math.floor(idx / 10);
    //         value.columnReturn = this.trackData[currentIdx].columnReturn;
    //         value.columnTitle = `${currentIdx}% to ${currentIdx + 1}%`;
    //       }
    //       data.push(value);
    //       this.trackData[xAxisValue] = value;
    //     } else {
    //       const index = (idx / 10).toFixed(0);
    //       data.push(this.trackData[index]);
    //     }
    //   }
    //   return data;
    // },
    loadChart() {
      if (this.toload !== '' && this.main === undefined) {
        return;
      }

      console.log('small return distribution bars');

      const that = this;
      const current = this.main;
      const period = this.period;
      let rawData = [];
      if (current[`${period}_monthly_returns`] !== undefined) {
        rawData = current[`${period}_monthly_returns`];
      }
      const data_obj = {};
      // let monthCount = 0;
      // let totalValue = 0;
      // let totalSquareValue = 0;
      for (const i in rawData) {
        const year = rawData[i];
        for (const i2 in year) {
          if (i2 > 0) { //dont count the yearly total
            // monthCount++;
            // const value = parseFloat(year[i2]);
            // totalValue += value;
            // totalSquareValue += (value * value);
            const floor = Math.floor(year[i2]);//means the val is between floor and floor+1
            if (data_obj[floor] === undefined) data_obj[floor] = 0;
            data_obj[floor]++;
          }
        }
      }

      // const mean = totalValue / monthCount;
      // const stdDev = Math.sqrt(totalSquareValue - mean * mean);

      const allReturnkey = Object.keys(data_obj).map(o => parseInt(o, 10)).sort();
      let max = allReturnkey[allReturnkey.length - 1];
      let min = allReturnkey[0];
      if (Math.abs(min) > max) {
        max = Math.abs(min);
      } else {
        min = max * -1;
      }

      // const guides = [];
      // for (let idx = min; idx <= max; idx++) {
      //   const xAxisValue = idx;
      //   const yAxisValue = this.normalDensityZx(xAxisValue, mean, stdDev);
      //   this.trackData[idx] = {
      //     return: xAxisValue,
      //     lineReturn: yAxisValue.toFixed(5),
      //     lineTitle: `${xAxisValue}%`,
      //     lineValue: yAxisValue.toFixed(3),
      //     columnTitle: `${xAxisValue}% to ${xAxisValue + 1}%`,
      //   };
      //   if (data_obj[xAxisValue] === undefined) {
      //     this.trackData[idx].columnReturn = 0;
      //   } else {
      //     this.trackData[idx].columnReturn = data_obj[xAxisValue];
      //   }
      //   guides.push({
      //     lineAlpha: 0,
      //     category: xAxisValue.toFixed(0),
      //     label: xAxisValue.toFixed(0),
      //   });
      // }
      // const data = this.changeData(min, max, mean, stdDev);

      const data = [];
      for (let idx = min; idx <= max; idx++) {
        const xAxisValue = idx;
        const trackData = {
          return: xAxisValue,
          columnTitle: `${xAxisValue}% to ${xAxisValue + 1}%`,
        };
        if (data_obj[xAxisValue] === undefined) {
          trackData.columnReturn = 0;
        } else {
          trackData.columnReturn = data_obj[xAxisValue];
        }
        data.push(trackData);
      }

      const graphs = [{
        valueAxis: 'v1',
        balloonText: '[[columnTitle]]<br>[[value]] months',
        valueField: 'columnReturn',
        type: 'column',
        lineAlpha: 1,
        lineThickness: 2,
        lineColor: '#123B69',
        fillColor: '#123B69',
        fillAlphas: 1,
      },
      // {
      //   valueAxis: 'v2',
      //   balloonText: '[[lineTitle]]<br>[[lineValue]] months',
      //   // bullet: 'round',
      //   // bulletSize: 7,
      //   // bulletBorderAlpha: 1,
      //   // bulletColor: '#FFFFFF',
      //   // useLineColorForBulletBorder: true,
      //   // bulletBorderThickness: 3,
      //   bulletSize: 0,
      //   valueField: 'lineReturn',
      //   lineColor: '#b7294f',
      //   fillAlphas: 0,
      //   lineThickness: 2,
      // }
      ];
      const { id } = this;
      if (window.charts[id]) {
        window.charts[id].graphs = graphs;
        window.charts[id].dataProvider = data;
        window.charts[id].validateData();
      } else {
        window.charts[id] = window.AmCharts.makeChart(that.id, {
          hideCredits: true,
          fontFamily: 'Fira Sans',
          type: 'serial',
          theme: 'light',
          autoMarginOffset: 30,
          addClassNames: true,
          // guides,
          graphs,
          columnWidth: 0.7,
          categoryField: 'return',
          valueAxes: [{
            id: 'v1',
            // axisAlpha: 0,
            minimum: 0,
            dashLength: 1,
            axisColor: '#123B69',
            position: 'left',
            title: 'Number of months',
          }],
          chartCursor: {
            cursorPosition: 'start',
            categoryBalloonEnabled: false,
            valueLineBalloonEnabled: false,
            valueLineAlpha: 0,
            cursorAlpha: 0,
          },
          categoryAxis: {
            integersOnly: true,
            title: 'Monthly variation (%)',
            minHorizontalGap: 30,
            gridAlpha: 0,
            gridThickness: 0,
            // labelFrequency: 1,
            // labelsEnabled: false,
          },
          dataProvider: data,
          pathToImages: 'https://cdn.amcharts.com/lib/3/images/',
        });
      }
    },
  },

  beforeDestroy() {
    const { id } = this;
    if (window.charts[id]) {
      window.charts[id].clear();
      delete window.charts[id];
    }
    this.$off();
  },

  mounted() {
    this.loadChart();
  },

});
