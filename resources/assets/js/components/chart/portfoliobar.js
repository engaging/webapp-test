const { metrics } = require('../../util/config');

Vue.component('chart-portfoliobar', {
  template: '<div><h4 class="print-border charttitle">{{title}}</h4> <div v-show="loading == true" class="abs-loader"><div class="loader-inner ball-pulse"><div></div><div></div><div></div></div></div> <div class="chart chart-portfoliobar" :id="id" style="height:300px;"></div></div>',
  props: [
    'title', 'idname', 'dataLoaded', 'widgetData', 'idx', 'period',
  ],
  data() {
    return {
      data: [],
      unit: '',
      label: '',
      dualColor: false,
      // chart: null,
      loading: true,
      labels: metrics,
    };
  },
  computed: {
    id() {
      const add = this.idname != undefined ? this.idname : 'no-id';
      return `returnbar-chart-${add}`;
    },
    portfolios() {
      return this.$store.state.portfolios;
    },
    datasets() {
      return this.widgetData[this.idx].properties.datasets || [];
    },
    fields() {
      return this.widgetData[this.idx].field || 'return';
    },
  },
  watch: {
    datasets() {
      this.loadData();
    },
    period() {
      this.loadData();
    },
  },
  methods: {
    loadData() {
      let data = [];

      this.label = this.labels[this.fields];

      switch (this.fields) {
        case 'returnCum':
        case 'return':
        case 'sharpe':
        case 'calmar':
        case 'sortino':
          this.dualColor = true;
          break;
        default:
          this.dualColor = false;
      }

      if (this.datasets.length) {
        for (let i = 0; i < this.datasets.length; i++) {
          const item = this.portfolios[this.datasets[i].id];
          this.unit = item[`um_${this.fields}`] || '';
          const newitem = {
            title: item.title,
            value: parseFloat(item[`${this.period}_${this.fields}`]).toFixed(2),
          };
          data.push(newitem);
        }
      }
      function compare(a, b) {
        if (parseFloat(a.value) < parseFloat(b.value)) { return 1; }
        if (parseFloat(a.value) > parseFloat(b.value)) { return -1; }
        return 0;
      }
      data = data.sort(compare);
      console.log(JSON.stringify(data));
      this.loadChart(data);
    },

    loadChart(data) {
      const graphs = [
        {
          balloonText: `[[category]]<br />${this.label}: [[value]]${this.unit}`,
          labelText: '',
          labelPosition: 'bottom',
          title: this.label,
          type: 'column',
          valueField: 'value',
          lineColor: 'transparent',
          fillAlphas: 1,
          fillColors: this.dualColor ? '#29B999' : '#123B69',
          negativeBase: 0,
          negativeFillAlphas: 1,
          negativeFillColors: this.dualColor ? '#b7294f' : '#123B69',
        },
      ];
      const { id } = this;
      if (window.charts[id]) {
        window.charts[id].valueAxes[0].title = this.label + (this.unit && ` (${this.unit})`);
        window.charts[id].graphs = graphs;
        window.charts[id].dataProvider = data;
        window.charts[id].validateData();
      } else {
        window.charts[id] = window.AmCharts.makeChart(this.id, {
          hideCredits: true,
          type: 'serial',
          theme: 'light',
          categoryField: 'title',
          rotate: true,
          categoryAxis: {
            labelsEnabled: false,
            gridThickness: 0,
            axisAlpha: 0,
          },
          valueAxes: [
            {
              id: 'barValueAxis-1',
              position: 'bottom',
              axisAlpha: 0.5,
              gridThickness: 0,
              reversed: false,
              title: this.label + (this.unit && ` (${this.unit})`),
            },
          ],
          trendLines: [],
          graphs: [
            {
              balloonText: `[[category]]<br />${this.label}: [[value]]${this.unit}`,
              labelText: '',
              labelPosition: 'bottom',
              title: this.label,
              type: 'column',
              valueField: 'value',
              lineColor: 'transparent',
              fillAlphas: 1,
              fillColors: this.dualColor ? '#29B999' : '#123B69',
              negativeBase: 0,
              negativeFillAlphas: 1,
              negativeFillColors: this.dualColor ? '#b7294f' : '#123B69',
            },
          ],
          guides: [],
          pathToImages: 'https://cdn.amcharts.com/lib/3/images/',
          dataProvider: data,
        });
      }
      this.loading = false;
    },
  },
  mounted() {
    this.loadData();
  },
  beforeDestroy() {
    const { id } = this;
    if (window.charts[id]) {
      window.charts[id].clear();
      delete window.charts[id];
    }
    this.$off();
  },
});
