/* global window Vue $ */
const ACTION = require('../../store/mutation-type');
const { PERIOD } = require('../../util/period');

Vue.component('correlation-matrix', {
  props: {
    periodGroup: {
      type: Object,
      default: () => {},
    },
    correlateWithObjects: {
      type: Array,
      default: () => [],
    },
    isInvalid: {
      type: Boolean,
      default: false,
    },
    colors: {
      type: Object,
      default: () => {},
    },
    loading: {
      type: Boolean,
      default: false,
    },
  },
  data() {
    return {
      loaded: false,
      showPopover: false,
      comparePeriods: {},
      correlationMatrix: {},
      popover: {
        correlation: '',
        firstItem: {},
        secondItem: {},
      },
      timeOut: null,
      currentHover: null,
      currentHoverIndex: null,
      popverClass: 'left',
      popverHeight: 98,
      popverWidth: 0,
      popoverLeft: 0,
      popverTop: 0,
      totalLength: 0,
    };
  },
  computed: {
    strategies() {
      return this.$store.state.cor.strategies || {};
    },
    ids() {
      const list = this.correlateWithObjects;
      const ids = [];
      if (list.length < 1) {
        return [];
      }
      for (let x = 1; x < list.length; x++) { // no need main portfolio
        ids.push(list[x].id);
      }
      return ids;
    },
    correlateWithObjectsSorted() {
      const list = this.correlateWithObjects;
      const filteredList = [];
      if (list.length < 2) {
        return [];
      }

      for (let x = 1; x < list.length; x++) { // no need main portfolio
        filteredList.push(list[x]);
      }
      return filteredList;
    },
    popoverStyle() {
      return `display:block;left:${this.popoverLeft}px;top:${this.popverTop}px`;
    },
    hiddenPeriods() {
      return this.$store.state.hiddenPeriods;
    },
  },
  watch: {
    correlateWithObjects() {
      this.start();
    },
  },
  // mounted() {
  //   this.start();
  //   // this.setEvents();
  // },
  beforeDestroy() {
    this.$off();
  },
  methods: {
    pinStrategy(id) {
      // jquery find button;
      const $buttons = $(`#row-${id}`).find('button');
      if ($buttons.length) $buttons[0].click();
    },
    highlightIndex(id) {
      const indexToHighlight = {
        indexinfo: {
          id,
        },
      };
      window.eventBus.$emit('highlightIndex', indexToHighlight);
    },
    unhighlightIndex(id) {
      const indexToHighlight = {
        indexinfo: {
          id,
        },
      };
      window.eventBus.$emit('unhighlightIndex', indexToHighlight);
    },
    showPopversDelay(strategy, strategyCompared, index, event) {
      if (this.timeOut) {
        clearTimeout(this.timeOut);
      }
      const that = this;
      this.timeOut = setTimeout(() => {
        that.showPopovers(strategy, strategyCompared, index, event);
      }, 200);
    },
    showPopovers(strategy, strategyCompared, index, event) {
      console.log('start case');
      const key = `${strategy.id}-${strategyCompared.id}`;
      if (strategy.id === strategyCompared.id) {
        this.currentHover = key;
        this.currentHoverIndex = index;
        this.showPopover = false;
        return null;
      }
      if (this.currentHover === key) {
        return null;
      }
      const totalLength = this.correlateWithObjectsSorted.length;
      this.currentHover = key;
      this.popover.firstItem = strategy;
      this.popover.secondItem = strategyCompared;
      this.popover.correlation = this.getValue(strategy.id, strategyCompared.id);
      if (this.currentHoverIndex !== null) {
        if (this.currentHoverIndex <= index) {
          this.currentHoverIndex = index;
          this.showPopover = true;
          this.$nextTick(() => {
            this.setPosition(event, 'left');
          });
        } else {
          this.currentHoverIndex = index;
          this.setPosition(event, 'right');
          this.showPopover = true;
        }
      } else if (index <= (totalLength / 2)) {
        this.currentHoverIndex = index;
        this.showPopover = true;
        this.$nextTick(() => {
          this.setPosition(event, 'left');
        });
      } else {
        this.currentHoverIndex = index;
        this.setPosition(event, 'right');
        this.showPopover = true;
      }
    },
    hidePopovers() {
      this.currentHover = null;
      this.currentHoverIndex = null;
      this.showPopover = false;
    },
    setPosition(event, pointer) {
      const paddingLeft = 15;
      const dom = event.target;
      this.popverClass = pointer;
      if (pointer === 'left') {
        this.popverWidth = this.$refs.popver.clientWidth;
        this.popoverLeft = dom.offsetLeft - this.popverWidth + paddingLeft;
        this.popverTop = dom.offsetTop + Math.round(dom.clientHeight / 2) - (this.popverHeight / 2);
      } else {
        this.popverTop = dom.offsetTop + Math.round(dom.clientHeight / 2) - (this.popverHeight / 2); // - TOPBARLENGTH
        this.popoverLeft = dom.offsetLeft + dom.clientWidth + paddingLeft;
      }
    },
    getRowClass(strategy) {
      if (strategy.color) {
        return 'pinned-row-item';
      }
      return '';
    },
    getRowColor(strategy) {
      if (strategy.color) {
        return this.colors[strategy.color];
      }
      return '';
    },
    async start() {
      if (this.ids.length === 0 || this.isInvalid) return null;
      const needToLoad = this.checkData();
      if (needToLoad) {
        await this.loadData();
      }
      this.loaded = true;
    },
    async loadData() {
      this.loaded = false;
      const payload = {
        periods: this.periodGroup.periods,
        maxDate: this.periodGroup.dataPeriod === PERIOD.CUST ? this.periodGroup.custMaxDate : this.periodGroup.maxDate,
        ids: this.ids,
        period: this.periodGroup.dataPeriod,
      };
      await this.$store.dispatch(ACTION.API_FETCH_STRATEGIES_CORRELATION, {
        payload,
        freeze: true,
      });
    },
    checkData() {
      const period = this.periodGroup.dataPeriod;
      const data = this.strategies[period];
      if (!data || Object.keys(data).length === 0) {
        return true;
      }
      for (let idx = 0; idx < this.ids.length; idx++) {
        for (let subIdx = idx + 1; subIdx < this.ids.length; subIdx++) {
          const key = this.ids[idx] < this.ids[subIdx] ? `${this.ids[idx]}-${this.ids[subIdx]}`
            : `${this.ids[subIdx]}-${this.ids[idx]}`;
          if (!data[key]) return true;
        }
      }
      return false;
    },
    isSame(a, b) {
      if (a.id === b.id) {
        return true;
      }
      return false;
    },
    getValue(a, b) {
      const period = this.periodGroup.dataPeriod;
      const data = this.strategies[period];
      const key = a < b ? `${a}-${b}` : `${b}-${a}`;
      let valueOnScale = 0;
      try {
        valueOnScale = data[key] || 0;
      } catch (error) {
        valueOnScale = 0;
      }
      valueOnScale = parseFloat(valueOnScale);
      return valueOnScale.toFixed(2);
    },
    getColor(a, b) {
      if (a === b) {
        return '#FFFFFF';
      }
      const period = this.periodGroup.dataPeriod;
      const data = this.strategies[period];
      const key = a < b ? `${a}-${b}` : `${b}-${a}`;
      let valueOnScale = 0;
      try {
        valueOnScale = data[key] || 0;
      } catch (error) {
        valueOnScale = 0;
      }
      if (valueOnScale === 0) {
        return '#FFFFFF';
      }

      // const flipped = 1 + (valueOnScale * -1);
      // const percent = flipped / 2;
      if (valueOnScale < 0) {
        valueOnScale = Math.abs(valueOnScale);
        return `rgba(69,161,99,${valueOnScale})`;
      }
      valueOnScale = Math.abs(valueOnScale);
      return `rgba(255,0,0,${valueOnScale})`;
    },
  },
  template: `
    <div class="col-sm-12" @mouseleave="hidePopovers()" style="position:relative">
      <div v-show="hiddenPeriods[periodGroup.dataPeriod]" class="chart-not-available">
      </div>
      <div v-show="!hiddenPeriods[periodGroup.dataPeriod]">
        <!-- use bootstrap css to write the popover -->
        <div class="abs-loader" v-show="!loaded && !loading">
          <div class="loader-inner ball-pulse">
            <div></div>
            <div></div>
            <div></div>
          </div>
        </div>
        <table class="table correlation_matrix_table" v-if="loaded">
          <tbody>
            <tr v-for="(strategy, idx) in correlateWithObjectsSorted" class="correlation-matrix-row" :class="getRowClass(strategy)"
              :key="idx" :id="'correlation-matrix-' + strategy.id" :data-strategy="strategy.id" @mouseenter="highlightIndex(strategy.id)" @mouseleave="unhighlightIndex(strategy.id)">
              <td v-for="(strategyCompared, idx2) in correlateWithObjectsSorted" :key="idx2" @mouseenter="showPopversDelay(strategy, strategyCompared, idx2, $event)"
                :class="{'col-first': idx2 === 0, 'modified correl-same': isSame(strategy, strategyCompared)}"
                :style="{'background-color': getColor(strategy.id, strategyCompared.id), 'border-color': getRowColor(strategy) }">&nbsp;
              </td>
            </tr>
          </tbody>
        </table>
        <div class="popover-area" v-if="showPopover">
          <div ref="popver" class="popover fade in" :class="popverClass" :style="popoverStyle">
            <div class="arrow" style="top: 50%;"></div>
            <h3 class="popover-title" v-if="popover.correlation">{{popover.correlation}}</h3>
            <div class="popover-content" v-if="popover.firstItem">
              <span @click="pinStrategy(popover.firstItem.id)">{{popover.firstItem.shortName}}</span>
              <br>
              <span @click="pinStrategy(popover.secondItem.id)">{{popover.secondItem.shortName}}</span>
            </div>
          </div>
        </div>
      </div>
    </div>
  `,
});
// </script>
// <style>
// .correlation_matrix_table td{
//   padding: 8px;
//   line-height: 1.8;
//   border-top: 1px solid #ddd;
// }
// </style>
