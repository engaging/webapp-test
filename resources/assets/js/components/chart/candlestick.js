/* global window document Vue */

Vue.component('chart-candlestick', {
  props: {
    title: {
      type: String,
      default: '',
    },
    idname: {
      type: String,
      default: 'no-id',
    },
    datasets: {
      type: Array,
      default: () => [],
    },
    colors: {
      type: Object,
      default: () => {},
    },
    hidden: {
      type: Boolean,
      default: false,
    },
  },
  data() {
    return {
      // trackData: {},
      // chart: null,
      highlightId: null,
    };
  },
  computed: {
    id() {
      return `candlestick-chart-${this.idname}`;
    },
  },
  watch: {
    datasets() {
      this.loadChart();
    },
  },
  mounted() {
    this.loadChart();
  },
  beforeDestroy() {
    const { id } = this;
    if (window.charts[id]) {
      window.charts[id].clear();
      delete window.charts[id];
    }
    this.$off();
  },
  methods: {
    getSquare(fixedColumnWidth) {
      // normally graph: 450 * 350, max width: 25px,
      // let currentlength = 2;
      let min = -1;
      let max = 1;
      this.datasets.forEach((o) => {
        // const dataLength = o.upperBound - o.lowerBound;
        // if (dataLength < currentlength) currentlength = dataLength;
        if (o.lowerBound < min) min = o.lowerBound;
        if (o.upperBound > max) max = o.upperBound;
      });
      const totalLength = max - min;
      // const squareHeight = Math.round((350 * currentlength) / (totalLength * 2)) < 15 ? 15 : Math.round((350 * currentlength) / (totalLength * 2));
      // const squareWidth = Math.round(450 / (this.datasets.length + 5)) < 20 ? Math.round(450 / (this.datasets.length + 5)) : 20;
      // if (squareWidth >= squareHeight) {
      //   const newlength = (squareHeight / 350) * totalLength / 2;
      //   return {
      //     fixedColumnWidth: squareHeight,
      //     fixedLengh: newlength,
      //   };
      // }
      const newlength = (fixedColumnWidth / 350) * totalLength / 2;
      return {
        // fixedColumnWidth: squareWidth,
        min,
        max,
        fixedLengh: newlength,
      };
    },
    loadChart() {
      console.log('small candlestick charts');
      const data = [];
      let currentlength = 1;
      this.datasets.forEach((o) => {
        const dataLength = o.upperBound - o.lowerBound;
        if (dataLength < currentlength) currentlength = dataLength;
      });
      const fixedColumnWidth = 15;
      const { fixedLengh, min, max } = this.getSquare(fixedColumnWidth);
      this.datasets.forEach((o) => {
        const color = o.color ? this.colors[o.color] : this.getTypeColor(o.type);
        const item = {
          name: o.shortName,
          open: o.beta - fixedLengh,
          close: o.beta + fixedLengh,
          low: o.lowerBound,
          high: o.upperBound,
          beta: o.beta,
          balloonValue: `${o.beta}`,
          fillColor: color,
          itemCode: `candle-${o.code}`,
          fixedColumnWidth,
        };
        data.push(item);
      });
      const graphs = [{
        id: 'g1',
        // proCandlesticks: true,
        balloonText: '[[name]]<br>Beta:<b>[[value]]</b>',
        closeField: 'close',
        openField: 'open',
        lowField: 'low',
        valueField: 'beta',
        highField: 'high',
        classNameField: 'itemCode',
        type: 'candlestick',
        lineColorField: 'fillColor',
        fillColorsField: 'fillColor',
        fillAlphas: 1,
        lineAlpha: 1,
      }];
      graphs[0].fixedColumnWidth = fixedColumnWidth;
      const { id } = this;
      if (window.charts[id]) {
        window.charts[id].graphs = graphs;
        window.charts[id].dataProvider = data;
        window.charts[id].valueAxes[0].maximum = max;
        window.charts[id].valueAxes[0].minimum = min;
        window.charts[id].validateData();
      } else {
        window.charts[id] = window.AmCharts.makeChart(this.id, {
          hideCredits: true,
          fontFamily: 'Fira Sans',
          type: 'serial',
          theme: 'light',
          dataProvider: data,
          autoMarginOffset: 30,
          addClassNames: true,
          valueAxes: [{
            axisAlpha: 0,
            gridAlpha: 0.1,
            fontSize: 13,
            maximum: max,
            minimum: min,
          }],
          guides: [{
            lineAlpha: 1,
            value: 0,
          }],
          graphs: graphs,
          categoryField: 'name',
          categoryAxis: {
            minHorizontalGap: 30,
            gridThickness: 0,
            labelsEnabled: false,
            axisAlpha: 0,
            gridAlpha: 0,
          },
          pathToImages: 'https://cdn.amcharts.com/lib/3/images/',
        });
      }
    },
    getTypeColor(type) {
      switch (type) {
        case 'alpha':
          return '#5A5A5A';
        case 'Benchmark':
          return '#FF8935';
        case 'Private Strategy':
          return '#996699';
        case 'Strategy':
          return '#123B69';
        case 'pca':
          return '#ccc';
        default:
          return '#1A85A1';
      }
    },
  },
  template: '<div><h4 class="print-border charttitle">{{title}}</h4><div v-show="hidden" class="chart chart-candlestick chart-not-available"></div><div v-show="!hidden" class="chart chart-candlestick" :id="id" style="height:400px;max-width:560px;"></div></div>',
});
