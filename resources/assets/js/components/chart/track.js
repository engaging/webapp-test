Vue.component('chart-track', {
  template: '<div><h4 class="print-border charttitle" v-if="title.length">{{title}}</h4> <div v-show="trackLoading == true" class="abs-loader"><div class="loader-inner ball-pulse"> <div></div><div></div><div></div> </div></div> <div v-show="hiddenPeriods[period]" class="chart chart-big chart-not-available"></div><div v-show="!hiddenPeriods[period]" class="chart chart-big" :id="id"></div></div>',
  props: {
    title: {},
    idname: {},
    type: {},
    period: {},
    colors: {},
    loading: {
      type: Boolean,
      default: false,
    },
    datasets: {
      type: Array,
      default: () => [],
    },
    pinned: {
      type: Boolean,
      default: false,
    },
    main: {
      type: Object,
      default: () => {},
    },
  },
  computed: {
    id() {
      const add = this.idname !== undefined ? this.idname : 'no-id';
      return `track-chart-${add}`;
    },
    keyword() {
      return `${this.period}_track`;
    },
    dataKeyword() {
      return `${this.period}_date_track`;
    },
    strategies() {
      return this.$store.state.strategies;
    },
    portfolios() {
      return this.$store.state.portfolios;
    },
    hiddenPeriods() {
      return this.$store.state.hiddenPeriods;
    },
  },
  data() {
    return {
      trackLoading: true,
      // trackData: [], //organized data
      // trackLines: [], //organized lines
      // trackChart: null,
    };
  },
  watch: {
    loading(newValue) {
      if (newValue) {
        this.trackLoading = newValue;
      }
    },
    datasets(newValue, oldValue) {
      if (oldValue && oldValue.length > 0 || this.pinned) {
        this.getTrackRecords();
      }
    },
    period() {
      this.getTrackRecords();
    },
  },
  methods: {
    start() {
      this.getTrackRecords();
    },

    getTrackRecords() {
      console.log('get track data');
      this.trackLoading = true;
      const trackData = [];
      const trackLines = [];

      const data = [];
      const graphs = {};

      for (let x = 0; x < this.datasets.length; x++) {
        const colorType = this.datasets[x].color;
        const isPortfolio = !!this.datasets[x].title && !this.datasets[x].shortName;
        const strategy = isPortfolio ? this.portfolios[this.datasets[x].id] : this.strategies[this.datasets[x].id];
        let color = '#123B69';
        let size = 2;
        if (!colorType) {
          color = '#ccc';
        } else {
          color = this.colors[colorType];
        }
        if (this.pinned && colorType) {
          size = 3;
        } else {
          size = this.datasets.length > 20 ? 1 : 2;
        }

        if (strategy.id === this.main.id) { // special main one
          color = '#123B69';
          size = 3;
        }

        if (isPortfolio) {
          const id = `p${strategy.id}`;
          if (!graphs[id]) {
            const graphtitle = strategy.title;
            graphs[id] = {
              id,
              showBalloon: true,
              valueField: `portfolio${strategy.id}`,
              lineColor: color,
              lineThickness: size,
              balloonText: `${graphtitle}<br>[[value]]`,
              type: 'smoothedLine',
            };
          }
        } else {
          const id = `g${strategy.id}`;
          if (!graphs[id]) {
            const graphtitle = strategy.shortName;
            graphs[id] = {
              id,
              valueField: `strategy${strategy.id}`,
              lineColor: color,
              lineThickness: size,
              balloonText: `${graphtitle}<br>[[value]]`,
              type: 'smoothedLine',
            };
            if (this.pinned) { // for strategy pinned(analyse page)
              const extraOption = {
                id_content: strategy.id,
                hideBulletsCount: 5000,
                bullet: 'round',
                bulletSize: 1,
                bulletAlpha: 0,
              };
              Object.assign(graphs[id], extraOption);
            }
          }
        }

        const datapoints = strategy[this.keyword] || [];
        const currentDate = strategy[this.dataKeyword] || [];
        datapoints.forEach((point, index) => {
          const newDate = currentDate[index];
          if (data[newDate] === undefined) {
            data[newDate] = {
              date: newDate,
            };
          }
          if (!isPortfolio) {
            data[newDate][`strategy${strategy.id}`] = point;
          } else {
            data[newDate][`portfolio${strategy.id}`] = point;
          }
        });
      }

      for (const i in data) {
        trackData.push(data[i]);
      }

      trackData.sort((a, b) => {
        const a1 = new Date(b.date);
        const b1 = new Date(a.date);
        return a1 > b1 ? -1 : a1 < b1 ? 1 : 0;
      });
      for (const i in graphs) {
        trackLines.push(graphs[i]);
      }

      this.loadChart(trackData, trackLines);
    },

    loadChart(trackData, trackLines) {
      console.log('load main track');
      const that = this;
      const data = trackData; //{'date':'2016-01-01','strat1':val,'strat2,'val'}...
      const graphs = trackLines;
      //{'id':'strat1line',"valueField": "strat1",'linecolor':...}...
      if (graphs.length > 20 && this.pinned) {
        $('#chartCtx-wrapper').removeClass('animatelines');
      } else {
        $('#chartCtx-wrapper').addClass('animatelines');
      }
      const { id } = this;
      if (window.charts[id] != null) {
        window.charts[id].dataProvider = data;
        window.charts[id].graphs = graphs;
        window.requestAnimationFrame(() => {
          window.charts[id].validateData();
          that.trackLoading = false;
        });
      } else {
        window.charts[id] = AmCharts.makeChart(this.id, {
          hideCredits: true,
          type: 'serial',
          theme: 'light',
          marginRight: 40,
          marginLeft: 40,
          autoMarginOffset: 20,
          dataDateFormat: 'YYYY-MM-DD',
          addClassNames: true,
          valueAxes: [{
            id: 'v1',
            axisAlpha: 0,
            position: 'left',
          }],
          chartCursor: {
            cursorPosition: 'start',
            categoryBalloonEnabled: true,
            valueLineBalloonEnabled: false,
            valueLineAlpha: 0,
            cursorAlpha: 0,
            oneBalloonOnly: true,
            bulletSize: false,
          },
          balloon: {
            borderThickness: 2,
            shadowAlpha: 0,
          },
          categoryField: 'date',
          categoryAxis: {
            parseDates: true,
            // "dashLength": 1,
            // "minorGridEnabled": true,
            gridAlpha: 0,
            gridThickness: 0,
          },
          dataProvider: data,
          graphs,
          pathToImages: 'https://cdn.amcharts.com/lib/3/images/',
        });
        if (this.pinned) {
          
          window.charts[id].addListener('clickGraphItem', (event) => {
            const id = event.graph.id_content;
            if (id !== undefined && id !== that.main.id) {
              const $buttons = $(`#row-${id}`).find('button');
              if ($buttons.length) $buttons[0].click();
            }
          });

          window.charts[id].addListener('rollOverGraph', (event) => {
            const id = event.graph.id_content;
            const indexToHighlight = {
              indexinfo: {
                id,
              },
            };
            window.eventBus.$emit('highlightIndex', indexToHighlight);
          });

          window.charts[id].addListener('rollOutGraph', (event) => {
            const id = event.graph.id_content;
            const indexToHighlight = {
              indexinfo: {
                id,
              },
            };
            window.eventBus.$emit('unhighlightIndex', indexToHighlight);
          });
        }
        this.trackLoading = false;
      }
    },
  },
  mounted() {
    this.start();
  },
  beforeDestroy() {
    const { id } = this;
    if (window.charts[id] !== null) {
      window.charts[id].clear();
      delete window.charts[id];
    }
    this.$off();
  },
});
