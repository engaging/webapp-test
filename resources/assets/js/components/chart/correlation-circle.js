/* global window document Vue AmCharts */
Vue.component('chart-correlation-circle', {
  props: {
    title: {
      type: String,
      default: '',
    },
    idname: {
      type: String,
      default: 'no-id',
    },
    pinned: {
      type: Object,
      default: () => {},
    },
    main: {
      type: Object,
      default: () => {},
    },
    portfolio: {
      type: Object,
      default: () => {},
    },
    colors: {
      type: Object,
      default: () => {},
    },
    buttonColors: {
      type: Object,
      default: () => {},
    },
    hidden: {
      type: Boolean,
      default: false,
    },
  },
  data() {
    return {
      leftSelected: 'pc1',
      rightSelected: 'pc2',
      defaultColor: '#808080',
      portfolioColor: '#396189',
    };
  },
  computed: {
    id() {
      return `correlation-circle-chart-${this.idname}`;
    },
    menu() {
      const menu = {};
      for (const key in this.main) {
        if (key !== 'items') {
          const name = this.main[key].shortName;
          menu[key] = `${name} (${this.main[key].weight}%)`;
        }
      }
      return menu;
    },
    leftSelectionText() {
      return this.menu[this.leftSelected];
    },
    rightSelectionText() {
      return this.menu[this.rightSelected];
    },
    leftDropdownColor() {
      const color = this.buttonColors[this.leftSelected] || '#ccc';
      return `color: #fff;background:${color}`;
    },
    rightDropdownColor() {
      const color = this.buttonColors[this.rightSelected] || '#ccc';
      return `color: #fff;background:${color}`;
    },
    finalTitle() {
      if (this.hidden || !Object.keys(this.main).length) {
        return this.title;
      }
      const total = this.main[this.leftSelected].weight + this.main[this.rightSelected].weight;
      return `${this.title} | VARIANCE: ${total.toFixed(2)}%`;
    },
  },
  watch: {
    main() {
      this.loadChart();
    },
  },
  mounted() {
    this.loadChart();
  },
  beforeDestroy() {
    const { id } = this;
    if (window.charts[id]) {
      window.charts[id].clear();
      delete window.charts[id];
    }
    this.$off();
  },
  methods: {
    setLeftMenu(key) {
      this.leftSelected = key;
      this.loadChart();
    },
    setRightMenu(key) {
      this.rightSelected = key;
      this.loadChart();
    },
    checkSelection() {
      if (!this.main[this.leftSelected]) this.leftSelected = 'pc1';
      if (!this.main[this.rightSelected]) this.rightSelected = 'pc2';
      if (!this.main[this.rightSelected]) this.rightSelected = 'pc1';
    },
    newTrendline(color, dataObject, id) {
      const image = new AmCharts.Image();
      image.svgPath = `M0,0 L1,${Math.sqrt(4)} L2,0 Z`;// A simple triangle in SVG.
      image.color = color;
      image.width = 10;
      image.height = 10;

      const rad = Math.atan(dataObject[`${id}x`] / dataObject[`${id}y`]);
      if (Number.isFinite(rad)) {
        let deg = (rad * (180 / Math.PI));
        if (dataObject[`${id}y`] > 0) {
          deg += 180;//upside down triangle
        }
        image.rotation = deg;
      }
      return {
        initialValue: dataObject[`${id}y`],
        initialXValue: dataObject[`${id}x`],
        finalValue: 0,
        finalXValue: 0,
        initialImage: image,
        lineAlpha: 1,
        lineColor: color,
        lineThickness: 1,
        id,
      };
    },
    dataToTrendLines(dataObject) {
      const lines = [
        this.newTrendline(this.portfolioColor, dataObject, 'portfolio'),
      ];
      for (const id in this.main.items) {
        lines.push(this.newTrendline(this.defaultColor, dataObject, id));
      }

      //generate circle
      const image = new AmCharts.Image();
      image.svgPath = 'M-137,0 a137,137 0 1,0 274,0 a137,137 0 1,0 -274,0'; // A simple triangle in SVG.
      image.color = 'transparent';
      image.width = 274;
      image.height = 274;
      image.offsetX = 137;
      image.offsetY = 137;

      const circleTrend = {
        initialValue: 0,
        initialXValue: 0,
        finalValue: 0,
        finalXValue: 0,
        initialImage: image,
        lineAlpha: 0,
        id: 'circle',
      };
      lines.push(circleTrend);
      return lines;
    },
    loadChart() {
      if (!Object.keys(this.main).length || !Object.keys(this.portfolio).length) {
        return null;
      }
      this.checkSelection();
      const dataObject = {
        portfolioy: this.portfolio.correlation[this.leftSelected],
        portfoliox: this.portfolio.correlation[this.rightSelected],
      };
      const titleY = this.main[this.leftSelected].shortName;
      const titleX = this.main[this.rightSelected].shortName;
      const colorY = this.buttonColors[this.leftSelected] || this.defaultColor;
      const colorX = this.buttonColors[this.rightSelected] || this.defaultColor;
      for (const id in this.main[this.leftSelected].correlation) {
        dataObject[`${id}y`] = this.main[this.leftSelected].correlation[id];
      }
      for (const id in this.main[this.rightSelected].correlation) {
        dataObject[`${id}x`] = this.main[this.rightSelected].correlation[id];
      }
      const trendLines = this.dataToTrendLines(dataObject);
      const graphs = [{
        bullet: 'triangleUp',
        bulletAlpha: 0,
        // bulletColor: 'transparent',
        type: 'line',
        valueField: 'value',
        pointPosition: 'start',
        title: this.portfolio.shortName,
        labelText: '',
        clustered: false,
        lineColor: this.portfolioColor,
        xField: 'portfoliox',
        yField: 'portfolioy',
        balloonText: `${this.portfolio.shortName}<br>${this.main[this.leftSelected].shortName}: ${dataObject.portfolioy} ${this.main[this.rightSelected].shortName}: ${dataObject.portfoliox}`,
      }];
      for (const id in this.main.items) {
        graphs.push({
          bullet: 'triangleUp',
          bulletAlpha: 0,
          // bulletColor: 'transparent',
          type: 'line',
          valueField: 'value',
          pointPosition: 'start',
          title: this.main.items[id].shortName,
          labelText: '',
          clustered: false,
          lineColor: this.pinned[id] ? this.colors[this.pinned[id]] : this.defaultColor,
          xField: `${id}x`,
          yField: `${id}y`,
          balloonText: `${this.main.items[id].shortName}<br>${this.main[this.leftSelected].shortName}: ${dataObject[`${id}y`]} ${this.main[this.rightSelected].shortName}: ${dataObject[`${id}x`]}`,
        });
      }
      const { id } = this;
      if (window.charts[id]) {
        window.charts[id].graphs = graphs;
        window.charts[id].dataProvider = [dataObject];
        window.charts[id].trendLines = trendLines;
        window.charts[id].valueAxes[0].guides[0].label = titleY;
        window.charts[id].valueAxes[1].guides[0].label = titleX;
        window.charts[id].valueAxes[0].guides[0].lineColor = colorY;
        window.charts[id].valueAxes[1].guides[0].lineColor = colorX;
        window.charts[id].valueAxes[0].guides[0].color = colorY;
        window.charts[id].valueAxes[1].guides[0].color = colorX;
        window.charts[id].validateData();
      } else {
        const updateRotations = (event) => {
          event.chart.trendLines.forEach((trendline) => {
            // const deltaX = trendline.initialXValue;
            // const deltaY = trendline.initialValue;
            // const rad = Math.atan(deltaX / deltaY);
            // let deg = (rad * (180 / Math.PI));
            // if (deltaY > 0) {
            //   deg += 180;//upside down triangle
            // }
            if (!trendline.initialImage.set) return null;
            const { node } = trendline.initialImage.set;
            // if (!node.getAttribute('origtrans') && trendline.id !== 'circle') {
            //   node.setAttribute('origtrans', node.getAttribute('transform'));
            //   node.setAttribute('transform', `${node.getAttribute('origtrans')} rotate(${deg} 0 0)`);
            // }
            if (trendline.id === 'circle') {
              node.setAttribute('stroke', '#808080');
            }
          });
        };
        window.charts[id] = window.AmCharts.makeChart(this.id, {
          hideCredits: true,
          type: 'xy',
          theme: 'light',
          marginTop: 25,
          marginBottom: 25,
          fontFamily: 'Fira Sans',
          marginRight: 25,
          marginLeft: 25,
          dataProvider: [dataObject],
          startDuration: 0,
          graphs,
          trendLines: trendLines,
          zoomOutText: '',
          zoomOutButtonImage: '',
          maxZoomFactor: 1,
          valueAxes: [{
            minimum: -1,
            maximum: 1,
            gridAlpha: 0.2,
            gridCount: 4,
            autoGridCount: false,
            fontSize: 13,
            axisThickness: 0,
            gridThickness: 1,
            ignoreAxisWidth: true,
            // labelsEnabled: false,
			      tickLength: 0,
            guides: [{
              above: true,
              value: 0,
              lineAlpha: 1,
              lineThickness: 1,
              lineColor: colorY,
              color: colorY,
              boldLabel: true,
              label: titleY,
              position: 'top',
            }],
            position: 'bottom',
            id: 'xAxis',
          }, {
            minimum: -1,
            maximum: 1,
            gridAlpha: 0.2,
            gridCount: 4,
            autoGridCount: false,
            fontSize: 13,
            axisThickness: 0,
            gridThickness: 1,
            ignoreAxisWidth: true,
            // labelsEnabled: false,
			      tickLength: 0,
            guides: [{
	          above: true,
              value: 0,
              lineAlpha: 1,
              lineThickness: 1,
              lineColor: colorX,
              color: colorX,
              boldLabel: true,
              label: titleX,
              position: 'right',
            }],
            position: 'left',
            id: 'yAxis',
            synchronizeWith: 'xAxis',
            synchronizationMultiplier: 1,
          }],
          chartCursor: {
            valueBalloonsEnabled: false,
            zoomable: false,
            cursorAlpha: 0.05,
          },
          pathToImages: 'https://cdn.amcharts.com/lib/3/images/',
          listeners: [{
            event: 'drawn',
            method: updateRotations,
          }],
        });
      }
    },
  },
  template: `
    <div>
      <h4 class="print-border charttitle">{{finalTitle}}</h4>
      <div v-show="hidden" class="chart chart-bar chart-not-available">
      </div>
      <div v-show="!hidden">
        <div
          v-show="Object.keys(menu).length>0"
          class="row correlation-circle-buttons">
          <div class="col-sm-5">
            <div class="dropdown">
              <button
                :style="leftDropdownColor"
                id="yAxisDropdown"
                type="button"
                class="btn btn-block btn-default dropdown-toggle btn-small"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="true">
                {{ leftSelectionText }}
                <span class="caret"/>
              </button>
              <ul aria-labelledby="yAxisDropdown" class="dropdown-menu">
                <li
                  v-for="(value, key) in menu"
                  :key="key"
                  @click="setLeftMenu(key)"><a>{{ value }}</a></li>
              </ul>
            </div>
          </div>
    		  <div class="col-sm-5 col-sm-offset-2">
            <div class="dropdown">
              <button
                :style="rightDropdownColor"
                id="xAxisDropdown"
                type="button"
                class="btn btn-block btn-default dropdown-toggle btn-small"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false">
                {{ rightSelectionText }}
                <span class="caret"/>
              </button>
              <ul aria-labelledby="xAxisDropdown" class="dropdown-menu">
                <li
                  v-for="(value, key) in menu"
                  :key="key"
                  @click="setRightMenu(key)"><a>{{ value }}</a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="chart chart-bar" :id="id" style="width: 325px;height:325px"></div>
        <div class="col-xs-12">
          <div class="text-center">
            <span><i class="fa fa-square" :style="'color:' + portfolioColor"></i>
              Portfolio&nbsp;&nbsp;
            </span>
            <span><i class="fa fa-square" :style="'color:' + defaultColor"></i>
              Strategy&nbsp;&nbsp;
            </span>
          </div>
        </div>
      </div>
    </div>
  `,
});
