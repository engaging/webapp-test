/* global window document Vue */
Vue.component('chart-footer-bar', {
  props: {
    title: {
      type: String,
      default: '',
    },
    idname: {
      type: String,
      default: 'no-id',
    },
    datasets: {
      type: Array,
      default: () => [],
    },
    colors: {
      type: Object,
      default: () => {},
    },
    height: {
      type: Number,
      default: 400,
    },
    valueField: {
      type: String,
      default: 'returnAttr',
    },
    hidden: {
      type: Boolean,
      default: false,
    },
  },
  data() {
    return {
      // trackData: {},
      // chart: null,
      highlightId: null,
    };
  },
  computed: {
    id() {
      return `footer-chart-${this.idname}`;
    },
    graphStyle() {
      return `height:${this.height}px;max-width:560px;`;
    },
  },
  watch: {
    datasets() {
      this.loadChart();
    },
  },
  mounted() {
    this.loadChart();
  },
  beforeDestroy() {
    const { id } = this;
    if (window.charts[id]) {
      window.charts[id].clear();
      delete window.charts[id];
    }
    this.$off();
  },
  methods: {
    loadChart() {
      console.log('small return waterfall charts');
      const data = [];
      let min = 0;
      this.datasets.forEach((o) => {
        const color = o.color && this.colors[o.color] ? this.colors[o.color] : this.getTypeColor(o.type);
        const item = {
          name: o.shortName,
          value: o[this.valueField],
          fillColor: color,
          itemCode: `waterfall-${o.code}`,
          title: o.shortName,
        };
        if (o[this.valueField] < min && o[this.valueField] < 0) {
          min = o[this.valueField];
        }
        data.push(item);
      });
      const fixedColumnWidth = Math.round(450 / (this.datasets.length + 5)) < 25 ? Math.round(450 / (this.datasets.length + 5)) : 25;
      const graphs = [{
        id: 'waterfall-graph',
        balloonText: '[[title]]<br>[[value]] %',
        valueField: 'value',
        type: 'column',
        lineColorField: 'fillColor',
        fillColorsField: 'fillColor',
        fillAlphas: 1,
        negativeBase: 0,
        negativeFillAlphas: 1,
        classNameField: 'itemCode',
        fixedColumnWidth,
      },
      ];
      const { id } = this;
      if (window.charts[id]) {
        window.charts[id].graphs = graphs;
        window.charts[id].valueAxes[0].minimum = min;
        window.charts[id].dataProvider = data;
        window.charts[id].validateData();
      } else {
        window.charts[id] = window.AmCharts.makeChart(this.id, {
          hideCredits: true,
          fontFamily: 'Fira Sans',
          type: 'serial',
          theme: 'light',
          autoMarginOffset: 30,
          addClassNames: true,
          guides: [{
            lineAlpha: 1,
            value: 0,
          }],
          graphs,
          categoryField: 'itemCode',
          valueAxes: [{
            minimum: min,
            axisAlpha: 0,
            fontSize: 13,
          }],
          categoryAxis: {
            minHorizontalGap: 30,
            gridAlpha: 0,
            gridThickness: 0,
            labelsEnabled: false,
            axisAlpha: 0,
          },
          dataProvider: data,
        });
      }
    },
    getTypeColor(type) {
      switch (type) {
        case 'alpha':
          return '#5A5A5A';
        case 'Benchmark':
          return '#FF8935';
        case 'Private Strategy':
          return '#996699';
        case 'Strategy':
          return '#123B69';
        case 'pca':
          return '#ccc';
        default:
          return '#1A85A1';
      }
    },
  },
  template: '<div><h4 class="print-border charttitle">{{title}}</h4><div v-show="hidden" class="chart chart-footer-bar chart-not-available"></div><div v-show="!hidden" class="chart chart-footer-bar" :id="id" :style="graphStyle"></div></div>',
});
