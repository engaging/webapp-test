const { metrics } = require('../../util/config');

Vue.component('chart-data', {
  template: `
  <div>
    <h2 class="stat" v-if="dataLoaded">
      <strong v-if="showData.stat>0">+</strong>{{showData.stat+showData.unit}}&nbsp;
      <span class="text-green" v-if="showData.stat>0">
        <img src="/img/design/icon-arrow-up.png" alt="up" width="30px;">
      </span>
      <span class="text-red" v-if="showData.stat<0">
        <img src="/img/design/icon-arrow-down.png" alt="down" width="30px;">
      </span>
    </h2> 
    <strong v-if="dataLoaded" class="text-gray" style="text-transform:capitalize">{{labels[properties.field]}}</strong>
  </div>`,
  props: [
    'dataLoaded', 'widgetData', 'idx', 'period',
  ],
  data() {
    return {
      stat: 0,
      labels: metrics,
    };
  },
  computed: {
    strategies() {
      return this.$store.state.strategies;
    },
    portfolios() {
      return this.$store.state.portfolios;
    },
    properties() {
      return this.widgetData[this.idx].properties;
    },
    main() {
      return this.properties.main;
    },
    showData() {
      const field = `${this.period}_${this.properties.field}`;
      let stat = 0;
      let unit = '';
      if (this.main.type === 'portfolio') {
        stat = this.portfolios[this.main.id][field];
        unit = this.portfolios[this.main.id][`um_${this.properties.field}`];
      } else {
        stat = this.strategies[this.main.id][field];
        unit = this.strategies[this.main.id][`um_${this.properties.field}`];
      }
      return {
        stat,
        unit,
      };
    },
  },
});
