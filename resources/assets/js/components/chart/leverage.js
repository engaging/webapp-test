Vue.component('chart-leverage', {
  template: '<div><h4 class="print-border charttitle">{{title + " (%)"}}</h4><div v-show="hiddenPeriods[period]" class="chart chart-leverage chart-not-available"></div><div v-show="!hiddenPeriods[period]" class="chart chart-leverage" :id="id" style="height:333px;"></div></div>',
  props: [
    'title', 'idname', 'type', 'main', 'period', 'indices',
  ],
  data() {
    return {
      defaultYAxis: {
        max: 150,
        min: 50,
      },
      // trackData: {},
      // chart: null,
    };
  },
  computed: {
    id() {
      const add = this.idname !== undefined ? this.idname : 'no-id';
      return `leverage-chart-${add}`;
    },
    hiddenPeriods() {
      return this.$store.state.hiddenPeriods;
    },
  },
  watch: {
    period() {
      this.loadChart();
    },
    main() {
      this.loadChart();
    },
  },
  methods: {
    loadChart() {
      console.log('small return leverage charts');
      const that = this;
      const yAxis = {
        max: 150,
        min: 50,
      };
      const current = this.main;
      const period = this.period;
      const dateKey = `${period}_date_track`;
      const date = current[dateKey] || [];
      const dataKey = `${period}_daily_leverage`;
      const rawData = current[dataKey] || {};
      const lastDay = date[date.length - 1];
      const trackData = {};
      for (const time in rawData) {
        const dataPoint = {};
        const win = rawData[time][0];
        const lose = rawData[time][1];
        const leverage = (100 + win - lose);
        if (leverage > yAxis.max) {
          yAxis.max = leverage;
        } else if (leverage < yAxis.min) {
          yAxis.min = leverage;
        }
        dataPoint.leverage = leverage.toFixed(2);
        trackData[time] = dataPoint;
      }
      date.forEach((o) => {
        if (trackData[o] === undefined) {
          trackData[o] = null;
        }
      });
      const loopDate = Object.keys(trackData).sort();
      if (trackData[lastDay] === null) {
        loopDate.pop();
      }
      const data = [];
      for (let index = 1; index < loopDate.length; index++) {
        if (trackData[loopDate[index]] === null) {
          trackData[loopDate[index]] = trackData[loopDate[index - 1]];
        }
        const finalData = _.cloneDeep(trackData[loopDate[index]]);
        finalData.date = loopDate[index];
        data.push(finalData);
      }
      const graphs = [{
        id: `leverage-${this.main.id}`,
        valueField: 'leverage',
        // lineColor: color,
        lineColor: '#123B69',
        negativeLineColor: '#b7294f',
        negativeBase: 99,
        lineThickness: 2,
        fillAlphas: 0.2,
        negativeFillAlphas: 0.2,
        type: 'step',
        balloonText: 'leverage<br>[[value]]%',
        lineAlpha: 1,
      }];
      const { id } = this;
      if (window.charts[id]) {
        window.charts[id].graphs = graphs;
        window.charts[id].valueAxes[0].maximum = yAxis.max;
        window.charts[id].valueAxes[0].minimum = yAxis.min;
        window.charts[id].chartCursor.oneBalloonOnly = (graphs.length > 5);
        window.charts[id].dataProvider = data;
        window.charts[id].validateData();
      } else {
        window.charts[id] = window.AmCharts.makeChart(that.id, {
          hideCredits: true,
          fontFamily: 'Fira Sans',
          type: 'serial',
          theme: 'light',
          autoMarginOffset: 30,
          addClassNames: true,
          dataDateFormat: 'YYYY-MM-DD',
          graphs,
          guides: [{
            lineAlpha: 1,
            value: 100,
            dashLength: 6,
          }],
          categoryField: 'date',
          valueAxes: [{
            maximum: yAxis.max,
            minimum: yAxis.min,
            id: 'v1',
            dashLength: 0,
            axisAlpha: 0,
          }],
          chartCursor: {
            cursorPosition: 'start',
            categoryBalloonEnabled: true,
            valueLineBalloonEnabled: false,
            valueLineAlpha: 0,
            cursorAlpha: 0,
            oneBalloonOnly: (graphs.length > 5),
          },
          balloon: {
            borderThickness: 2,
            shadowAlpha: 0,
          },
          categoryAxis: {
            fillAlpha: 0,
            gridAlpha: 0,
            parseDates: true,
            equalSpacing: true,
          },
          dataProvider: data,
          pathToImages: 'https://cdn.amcharts.com/lib/3/images/',
        });
      }
    },
  },
  beforeDestroy() {
    const { id } = this;
    if (window.charts[id]) {
      window.charts[id].clear();
      delete window.charts[id];
    }
    this.$off();
  },
  mounted() {
    this.loadChart();
  },

});
