/*
 * NOTE: if this graph appears empty on a Strat/Portfolio page,
 * it might be because Vol Track data doesnt exist yet (api/indices/getVolatilityTrack)
 */
const { PERIOD } = require('../../util/period');

Vue.component('chart-volatilitytrack', {
  template: '<div><h4 class="print-border charttitle">{{title + (isWidget ? "" : " (%)")}}</h4> <div class="clearfix chartfilter text-center"><button class="btn" v-bind:class="{ \'btn-blue\':volatilityPeriod_small===p, \'btn-outline\':volatilityPeriod_small!==p}" v-for="p in volatilityPeriods_small" @click="volatilityPeriod_small=p">{{volatilityPeriods_smallLabels[p]}}</button></div><div class="volatility-track-print">{{volatilityPeriods_FullLabels[volatilityPeriod_small]}}</div><div v-show="trackLoading === true" class="abs-loader"><div class="loader-inner ball-pulse"><div></div><div></div><div></div></div></div><div class="chart chart-volatilitytrack" :id="id"></div></div>',
  props: {
    title: {
      type: String,
      default: '',
    },
    idname: {
      type: String,
      default: 'no-id',
    },
    period: {
      type: String,
      default: '5y',
    },
    datasets: {
      type: Array,
      default: [],
    },
    colors: {
      type: Object,
      default: {},
    },
    isWidget: {
      type: Boolean,
      default: false,
    },
    loading: {
      type: Boolean,
      default: false,
    },
    loadingList: {
      type: Object,
      default: {},
    },
    measure: {
      type: String,
      default: '30d',
    },
    idx: {
      type: Number,
      default: 0,
    },
    pdfoptions: {},
  },
  computed: {
    id() {
      return `volatilitytrack-chart-${this.idname}`;
    },
    currentLoading() {
      if (this.volatilityPeriod_small && Object.keys(this.loadingList).length > 0) {
        return this.loadingList[this.volatilityPeriod_small];
      }
      return this.loading;
    },
  },
  data() {
    return {
      volatilityPeriods_small: ['30d', '60d', '120d'],
      volatilityPeriods_smallLabels: { '30d': '30d', '60d': '60d', '120d': '120d' },
      volatilityPeriods_FullLabels: { '30d': '30 Days', '60d': '60 Days', '120d': '120 Days' },
      volatilityPeriod_small: null,
      trackLoading: true,
    };
  },
  watch: {
    currentLoading(val) {
      if (!val) {
        console.log('currentLoading start');
        this.start();
      } else {
        this.trackLoading = val;
      }
    },
    datasets() {
      console.log('datasets start');
      if (!this.currentLoading) {
        this.start();
      }
      // this.loadChart();
    },
    period(period) {
      this.trackLoading = true;
      this.checkData(period, this.volatilityPeriod_small);
    },
    volatilityPeriod_small(newDays, oldDays) {
      window.eventBus.$emit('volatility.change', {
        newDays,
        oldDays,
        period: this.period,
        idx: this.idx ? this.idx : null,
      });
      this.trackLoading = true;
      this.checkData(this.period, newDays);
    },
  },

  methods: {
    checkData(period, volatilityPeriod_small) {
      const key = `${period}_volatility_track_${volatilityPeriod_small}`;
      const validKey = `${period}_active`;
      const needToload = this.datasets.filter(o => !o[key] && o[validKey] === 1);
      if (period === PERIOD.CUST || needToload.length > 0) {
        this.containerCallApi();
      } else {
        this.start();
      }
    },
    initButton() {
      for (const key in this.volatilityPeriods_smallLabels) {
        if (this.volatilityHasDays[key].length > 0) {
          this.volatilityPeriod_small = key;
          break;
        }
      }
    },
    containerCallApi() {
      window.eventBus.$emit('volatility.api', {
        volatilityPeriod_small: this.volatilityPeriod_small,
        period: this.period,
        idx: this.idx ? this.idx : null,
        reset: this.period === PERIOD.CUST,
      });
    },
    start() {
      this.loadChart();
    },
    shortenReturnType(str) {
      if (!str) {
        return '';
      }
      str = str.replace('Excess Return', 'ER');
      str = str.replace('Total Return', 'TR');
      return str;
    },
    shortenReturnCategory(str) {
      if (!str) {
        return '';
      }
      str = str.replace('Net', 'N');
      return str;
    },
    loadChart() {
      this.trackLoading = true;
      const graphs = [];
      const data_obj = {};
      for (let i = 0; i < this.datasets.length; i++) {
        const benchmark = this.datasets[i];

        let color = this.colors[benchmark.color];
        if (benchmark.color === '' || benchmark.color === undefined) {
          color = '#123B69';
        }
        let type = 'unknown';
        if (benchmark.type === 'portfolio' || (benchmark.title && !benchmark.shortName)) {
          type = 'portfolio';
        } else {
          type = 'strategy';
        }
        let graphtitle = '';
        if (benchmark.title !== undefined) {
          graphtitle = benchmark.title;
        }
        if (benchmark.shortName !== undefined) {
          graphtitle = `${benchmark.shortName}<br />${benchmark.assetClass} | ${benchmark.currency} ${this.shortenReturnCategory(benchmark.returnCategory)}${this.shortenReturnType(benchmark.returnType)} ${benchmark.volTarget}`;
        }
        if (graphtitle !== '') {
          graphs.push({
            balloonText: `${graphtitle}<br>[[value]]%`,
            valueField: type + benchmark.id,
            // bullet: 'round',
            bulletSize: 0,
            // bulletBorderThickness: 0,
            type: 'smoothedLine',
            lineColor: color,
            lineThickness: 2,
            fillAlphas: 0,
            // bulletAlpha: 0,
            // bulletBorderAlpha: 0,
          });
        }
        const key = `${this.period}_volatility_track_${this.volatilityPeriod_small}`;
        const track = benchmark[key];
        let points = [];
        if (track) {
          points = track;
        }
        //   else {
        //   const activeKey = `${this.period}_active`;
        //   // TODO fix infinite loop if error
        //   if (benchmark[activeKey] === 1) {
        //     this.containerCallApi();
        //     return;
        //   }
        // }

        if (points.length > 0) {
          for (let y = 0; y < points.length; y++) {
            const newDate = points[y][0];
            if (!data_obj[newDate]) {
              data_obj[newDate] = {
                date: newDate,
              };
            }
            data_obj[newDate][`${type}${benchmark.id}`] = points[y][1]; //*-1;
          }
        }
      }

      this.loadChart2(data_obj, graphs);
    },
    loadChart2(data_obj, graphs) {
      console.log('small volatility track');

      const that = this;
      if (JSON.stringify(this.colors) === '{}') {
        return;
      }

      const dataMapped = [];
      for (const day in data_obj) {
        data_obj[day].date = day;
        dataMapped.push(data_obj[day]);
      }
      dataMapped.sort((a, b) => {
        const a1 = new Date(b.date);
        const b1 = new Date(a.date);
        return a1 > b1 ? -1 : a1 < b1 ? 1 : 0;
      });
      const data = dataMapped;
      const { id } = this;
      if (window.charts[id]) {
        window.charts[id].graphs = graphs;
        window.charts[id].dataProvider = data;
        window.charts[id].validateData();
      } else {
        window.charts[id] = window.AmCharts.makeChart(that.id, {
          hideCredits: true,
          fontFamily: 'Fira Sans',
          type: 'serial',
          theme: 'light',
          dataDateFormat: 'YYYY-MM-DD',
          autoMarginOffset: 30,
          addClassNames: true,
          valueAxes: [{
            id: 'v1',
            axisAlpha: 0,
            minimum: 0,
            position: 'left',
            labelsEnabled: true,
            title: that.isWidget && 'Volatility (%)',
          }],
          chartCursor: {
            cursorPosition: 'start',
            categoryBalloonEnabled: true,
            valueLineBalloonEnabled: false,
            valueLineAlpha: 0,
            cursorAlpha: 0,
            oneBalloonOnly: true,
          },
          categoryField: 'date',
          categoryAxis: {
            parseDates: true,
            gridAlpha: 0,
            fillAlpha: 0,
            equalSpacing: true,
          },
          graphs: graphs,
          dataProvider: data,
          pathToImages: 'https://cdn.amcharts.com/lib/3/images/',
        });
      }
      this.trackLoading = false;
    },
  },
  mounted() {
    console.log('mounted start');
    this.loadChart();
    this.volatilityPeriod_small = this.measure; // trigger load data;
    if (this.pdfoptions) {
      this.volatilityPeriod_small = this.pdfoptions.days;
    }
  },
  beforeDestroy() {
    const { id } = this;
    window.eventBus.$emit('volatility.change', {
      newDays: null,
      oldDays: this.volatilityPeriod_small,
      period: this.period,
      idx: this.idx ? this.idx : null,
    });
    if (window.charts[id]) {
      window.charts[id].clear();
      delete window.charts[id];
    }
    this.$off();
  },
});
