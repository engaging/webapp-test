Vue.component('chart-drawdowntrack', {
  template: '<div><h4 class="charttitle print-border">{{title + (isWidget ? "" : " (%)")}}</h4> <div v-show="trackLoading == true" class="abs-loader"><div class="loader-inner ball-pulse"> <div></div><div></div><div></div> </div></div><div v-show="hiddenPeriods[period]" class="chart chart-drawdowntrack chart-not-available"></div><div v-show="!hiddenPeriods[period]" class="chart chart-drawdowntrack" :id="id"></div></div>',
  props: [
    'title', 'idname', 'period', 'type', 'main', 'datasets', 'colors', 'isWidget', 'loading',
  ],
  computed: {
    id() {
      const add = this.idname !== undefined ? this.idname : 'no-id';
      return `drawdowntrack-chart-${add}`;
    },
    dataKeyword() {
      return `${this.period}_date_track`;
    },
    hiddenPeriods() {
      return this.$store.state.hiddenPeriods;
    },
  },
  data() {
    return {
      // data_obj: {},
      // graphs_obj: [],
      trackLoading: false,
      // chart: null,
    };
  },
  watch: {
    loading(value) {
      if (value) {
        this.trackLoading = true;
      }
    },
    datasets() {
      this.loadChart();
    },
    period() {
      this.trackLoading = true;
      this.loadChart();
    },
    colors() {
      this.trackLoading = true;
      this.loadChart();
    },
  },
  methods: {
    shortenReturnType(str) {
      if (!str) {
        return '';
      }
      str = str.replace('Excess Return', 'ER');
      str = str.replace('Total Return', 'TR');
      return str;
    },
    shortenReturnCategory(str) {
      if (!str) {
        return '';
      }
      str = str.replace('Net', 'N');
      return str;
    },

    loadChart2(data_obj, graphs_obj) {
      if (JSON.stringify(this.colors) == '{}') {
        return;
      }

      const dataMapped = [];
      for (const day in data_obj) {
        data_obj[day].date = day;
        dataMapped.push(data_obj[day]);
      }
      dataMapped.sort((a, b) => {
        const a1 = new Date(b.date);
        const b1 = new Date(a.date);
        return a1 > b1 ? -1 : a1 < b1 ? 1 : 0;
      });
      const graphs = graphs_obj;
      const data = dataMapped;

      // console.log('render chart');
      // console.log(graphs.length, data.length, JSON.stringify(this.colors) );
      const { id } = this;
      if (window.charts[id]) {
        window.charts[id].graphs = graphs;
        window.charts[id].dataProvider = data;
        window.charts[id].validateData();
      } else {
        window.charts[id] = window.AmCharts.makeChart(this.id, {
          hideCredits: true,
          fontFamily: 'Fira Sans',
          type: 'serial',
          theme: 'light',
          dataDateFormat: 'YYYY-MM-DD',
          autoMarginOffset: 30,
          // "fontFamily": 'Fira Sans',
          addClassNames: true,
          valueAxes: [{
            id: 'v2',
            axisAlpha: 0,
            position: 'left',
            labelsEnabled: true,
            maximum: 0,
            title: this.isWidget && 'Drawdown (%)',
          }],
          chartCursor: {
            cursorPosition: 'start',
            categoryBalloonEnabled: true,
            valueLineBalloonEnabled: false,
            valueLineAlpha: 0,
            cursorAlpha: 0,
            oneBalloonOnly: true,
          },
          categoryField: 'date',
          categoryAxis: {
            equalSpacing: true,
            parseDates: true, //pdf fails
            gridAlpha: 0,
            fillAlpha: 0,
            position: 'top',
          },
          graphs,
          dataProvider: data,
          pathToImages: 'https://cdn.amcharts.com/lib/3/images/',
        });
      }
      this.trackLoading = false;
    },
    loadChart() {
      console.log('small underwater track');

      if (this.datasets.length === 0 || this.colors.length === 0) {
        return;
      }
      const graphs_obj = [];
      const data_obj = {};


      for (let i = 0; i < this.datasets.length; i++) {
        const benchmark = this.datasets[i];
        if (benchmark[`${this.period}_drawdown_track`] === undefined) {
          // no this periods's data;
          continue;
        } else {
          //make line settings for it
          let color = '#ddd';
          if (benchmark.color !== undefined) {
            color = this.colors[benchmark.color];
          }
          if (benchmark.id === this.main.id) {
            color = '#123B69';
          }
          let graphtitle = '';
          if (benchmark.shortName !== undefined && benchmark.id === this.main.id) {
            graphtitle = `<br />${benchmark.assetClass} | ${benchmark.currency} ${this.shortenReturnCategory(benchmark.returnCategory)}${this.shortenReturnType(benchmark.returnType)} ${benchmark.volTarget}`;
          }
          graphs_obj.push({
            balloonText: `${benchmark.title + graphtitle}<br>[[value]]%`,
            valueField: `benchmark${i}`,
            bulletSize: 0,
            lineColor: color,
            lineThickness: 2,
            fillAlphas: 0,
            type: 'smoothedLine',
          });

          // set up the data points
          const period = this.period;
          const track = benchmark[`${period}_drawdown_track`] || [];
          const currentDate = benchmark[this.dataKeyword];
          track.forEach((point, index) => {
            const newDate = currentDate[index];
            if (!data_obj[newDate]) {
              data_obj[newDate] = {
                date: newDate,
              };
            }
            let datePoint = point;
            if (datePoint > 0) {
              datePoint *= -1;
            }
            data_obj[newDate][`benchmark${i}`] = datePoint; //*-1;
          });
        }
      }
      this.loadChart2(data_obj, graphs_obj);
    },
  },

  mounted() {
    this.loadChart();
  },

  beforeDestroy() {
    const { id } = this;
    if (window.charts[id]) {
      window.charts[id].clear();
      delete window.charts[id];
    }
    this.$off();
  },
});
