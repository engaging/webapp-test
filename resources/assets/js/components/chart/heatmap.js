/* global window Vue AmCharts $ */

Vue.component('chart-heatmap', {
  props: {
    title: {
      type: String,
      default: null,
    },
    idname: {
      type: String,
      default: 'no-id',
    },
    main: {
      type: Object,
      default: () => {},
    },
    datasets: {
      type: Array,
      default: () => [],
    },
    subgraph: {
      type: Boolean,
      default: true,
    },
    hidden: {
      type: Boolean,
      default: false,
    },
  },
  data() {
    return {
      colors: {
        neg: '#b7294f',
        pos: '#123B69',
      },
      // showMenu: false,
    };
  },
  computed: {
    id() {
      return `heatmap-chart-${this.idname}`;
    },
    maxWidth() {
      if (!this.subgraph) return 'max-width:2000px;';
      return 'max-width:560px;';
    },
  },
  watch: {
    main() {
      this.loadChart();
    },
  },
  mounted() {
    this.loadChart();
  },
  beforeDestroy() {
    const { id } = this;
    if (window.charts[id] != null) {
      window.charts[id].clear();
      delete window.charts[id];
    }
    this.$off();
  },
  methods: {
    loadChart() {
      console.log('small xy');
      if (Object.keys(this.main).length === 0) return null;
      // const sortedKeys = Object.keys(this.main).sort((a, b) => (b.length - a.length));
      // const xAxis = [];
      // for (let i = 0; i <= Math.floor((sortedKeys.length - 1) / 2); i++) {
      //   xAxis.push(sortedKeys[i]);
      //   if (i !== (sortedKeys.length - 1 - i)) {
      //     xAxis.push(sortedKeys[sortedKeys.length - 1 - i]);
      //   }
      // }
      // const yAxis = ['Market Beta'];
      // for (const groupName in this.main) {
      //   if (this.main[groupName]) {
      //     for (const factorName in this.main[groupName]) {
      //       if (!yAxis.includes(factorName) && factorName !== 'Other') yAxis.push(factorName);
      //     }
      //   }
      // }

      // yAxis.push('Other');
      // yAxis.reverse();
      const xAxis = ['Foreign Exchange', 'Credit', 'Fixed Income', 'Equity', 'Commodity'];
      const yAxis = ['Other', 'Volatility', 'Value', 'Momentum', 'Low Vol', 'Carry', 'Benchmark'];
      const xGuides = [];
      const yGuides = [];
      const graphs = [];
      xAxis.forEach((groupName, groupIndex) => {
        xGuides.push({
          lineThickness: 1,
          lineAlpha: 0,
          value: groupIndex * 10 + 5,
          label: groupName,
          fontSize: 13,
        });
      });

      yAxis.forEach((factorName, factorIndex) => {
        yGuides.push({
          lineThickness: 1,
          lineAlpha: 0,
          value: factorIndex * 10 + 5,
          label: factorName,
          fontSize: 13,
        });
      });

      const dataObject = {};
      xAxis.forEach((groupName, groupIndex) => {
        yAxis.forEach((factorName, factorIndex) => {
          const key = `${groupName}-${factorName}`;
          let color = this.colors.pos;
          const value = this.main[groupName][factorName];
          if (value !== undefined) {
            dataObject[`${key}x`] = groupIndex * 10 + 5;
            dataObject[`${key}y`] = factorIndex * 10 + 5;
            const balloonText = `${groupName} ${factorName} factor<br> ${value}%`;
            if (value < 0) {
              color = this.colors.neg;
            }
            dataObject[`${key}value`] = Math.abs(value);
            graphs.push({
              key,
              balloonText,
              bullet: 'round',
              lineAlpha: 0,
              xField: `${key}x`,
              yField: `${key}y`,
              lineColor: color,
              fillAlphas: 0,
              // bulletSize: Math.abs(value),
              valueField: `${key}value`,
            });
          }
        });
      });

      const dataProvider = [dataObject];
      // clear old data
      const { id } = this;
      if (window.charts[id] != null) {
        window.charts[id].dataProvider = dataProvider;
        window.charts[id].graphs = graphs;
        window.charts[id].validateData();
      } else {
        window.charts[id] = AmCharts.makeChart(this.id, {
          hideCredits: true,
          type: 'xy',
          theme: 'light',
          autoMarginOffset: 0,
          dataProvider,
          fontFamily: 'Fira Sans',
          balloon: { cornerRadius: 5 },
          addClassNames: true,
          valueAxes: [{
            position: 'top',
            axisAlpha: 0,
            gridAlpha: 0,
            labelsEnabled: false,
            guides: xGuides,
            minimum: 0,
            maximum: xAxis.length * 10,
            strictMinMax: true,
            // title: this.labels[this.xAxis] + (xUnit && ` (${xUnit})`),
          }, {
            axisAlpha: 0,
            gridAlpha: 0,
            position: 'left',
            labelsEnabled: false,
            guides: yGuides,
            minimum: 0,
            maximum: yAxis.length * 10,
            strictMinMax: true,
            // title: this.labels[this.yAxis] + (yUnit && ` (${yUnit})`),
          }],
          graphs,
          maxZoomFactor: 1,
          trendLines: [],
          marginLeft: 64,
          marginBottom: 0,
          chartCursor: {
            cursorPosition: 'start',
            pan: false,
            valueLineEnabled: false,
            valueLineBalloonEnabled: false,
            valueBalloonsEnabled: true,
            cursorAlpha: 0,
            oneBalloonOnly: true,
          },
          zoomControl: {
            zoomControlEnabled: false,
          },
          pathToImages: 'https://cdn.amcharts.com/lib/3/images/',
        });
      }
    },
  },
  template: '<div><h4 class="print-border charttitle">{{title}}</h4><div v-show="hidden" class="chart chart-heatmap chart-not-available"></div><div v-show="!hidden" class="chart chart-heatmap" :id="id" :style="maxWidth" style="height:400px;margin-top:20px;"></div></div>',
});
