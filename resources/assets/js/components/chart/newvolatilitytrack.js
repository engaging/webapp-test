/*
 * NOTE: if this graph appears empty on a Strat/Portfolio page,
 * it might be because Vol Track data doesnt exist yet (api/indices/getVolatilityTrack)
 */
const { PERIOD } = require('../../util/period');
const ACTION = require('../../store/mutation-type');

Vue.component('new-chart-volatilitytrack', {
  props: {
    title: {
      type: String,
      default: '',
    },
    idname: {
      type: String,
      default: 'no-id',
    },
    periodGroup: {
      type: Object,
      default: () => {},
    },
    datasets: {
      type: Array,
      default: () => [],
    },
    colors: {
      type: Object,
      default: () => {},
    },
    activeStrategies: {
      type: Number,
      default: 0,
    },
    isWidget: {
      type: Boolean,
      default: false,
    },
    loading: {
      type: Boolean,
      default: false,
    },
  },
  data() {
    return {
      volatilityPeriods_small: ['30d', '60d', '120d'],
      volatilityPeriods_smallLabels: { '30d': '30d', '60d': '60d', '120d': '120d' },
      volatilityPeriods_FullLabels: { '30d': '30 Days', '60d': '60 Days', '120d': '120 Days' },
      volatilityPeriod_small: '30d',
      trackLoading: true,
    };
  },
  computed: {
    id() {
      return `volatilitytrack-chart-${this.idname}`;
    },
    portfolios() {
      return this.$store.state.vol.portfolios;
    },
    hiddenPeriods() {
      return this.$store.state.hiddenPeriods;
    },
  },
  watch: {
    datasets() {
      console.log('datasets start');
      this.checkData(this.periodGroup.dataPeriod, this.volatilityPeriod_small);
    },
    volatilityPeriod_small(newDays) {
      this.checkData(this.periodGroup.dataPeriod, newDays);
    },
  },
  mounted() {
    console.log('mounted start');
    this.checkData(this.periodGroup.dataPeriod, this.volatilityPeriod_small);
  },
  beforeDestroy() {
    const { id } = this;
    if (window.charts[id]) {
      window.charts[id].clear();
      delete window.charts[id];
    }
    this.$off();
  },

  methods: {
    checkData(period, volatilityPeriod_small) {
      if (!this.activeStrategies) return null;
      this.trackLoading = true;
      const key = `${period}_volatility_track_${volatilityPeriod_small}`;
      const needToload = this.datasets.filter(o => !this.portfolios[o.id] || !this.portfolios[o.id][key]);
      if (needToload.length > 0) {
        this.callApi(needToload);
      } else {
        this.start();
      }
    },
    async callApi(datasets) {
      const ids = datasets.map(o => o.id);
      const payload = {
        periods: this.periodGroup.periods,
        maxDate: this.periodGroup.dataPeriod === PERIOD.CUST ? this.periodGroup.custMaxDate : this.periodGroup.maxDate,
        measure: this.volatilityPeriod_small,
        period: this.periodGroup.dataPeriod,
      };
      await this.$store.dispatch(ACTION.API_FETCH_PORTFOLIO_VOLATILITY, {
        ids,
        payload,
        freeze: true,
      });
      this.start();
    },
    start() {
      this.loadChart();
    },
    shortenReturnType(str) {
      if (!str) {
        return '';
      }
      str = str.replace('Excess Return', 'ER');
      str = str.replace('Total Return', 'TR');
      return str;
    },
    shortenReturnCategory(str) {
      if (!str) {
        return '';
      }
      str = str.replace('Net', 'N');
      return str;
    },
    loadChart() {
      this.trackLoading = true;
      const graphs = [];
      const data_obj = {};
      for (let i = 0; i < this.datasets.length; i++) {
        const benchmark = this.datasets[i];
        let color = this.colors[benchmark.color];
        if (benchmark.color === '' || benchmark.color === undefined) {
          color = '#123B69';
        }
        const type = 'portfolios';
        const graphtitle = benchmark.title;
        if (graphtitle !== '') {
          graphs.push({
            balloonText: `${graphtitle}<br>[[value]]%`,
            valueField: type + benchmark.id,
            // bullet: 'round',
            bulletSize: 0,
            // bulletBorderThickness: 0,
            type: 'smoothedLine',
            lineColor: color,
            lineThickness: 2,
            fillAlphas: 0,
            // bulletAlpha: 0,
            // bulletBorderAlpha: 0,
          });
        }
        const key = `${this.periodGroup.dataPeriod}_volatility_track_${this.volatilityPeriod_small}`;
        const data = this[type][benchmark.id];
        let points = [];
        if (data && data[key]) {
          points = data[key];
        }

        if (points.length > 0) {
          for (let y = 0; y < points.length; y++) {
            const newDate = points[y][0];
            if (!data_obj[newDate]) {
              data_obj[newDate] = {
                date: newDate,
              };
            }
            data_obj[newDate][`${type}${benchmark.id}`] = points[y][1]; //*-1;
          }
        }
      }

      this.loadChart2(data_obj, graphs);
    },
    loadChart2(data_obj, graphs) {
      console.log('small volatility track');

      const that = this;
      if (JSON.stringify(this.colors) === '{}') {
        return;
      }

      const dataMapped = [];
      for (const day in data_obj) {
        data_obj[day].date = day;
        dataMapped.push(data_obj[day]);
      }
      dataMapped.sort((a, b) => {
        const a1 = new Date(b.date);
        const b1 = new Date(a.date);
        return a1 > b1 ? -1 : a1 < b1 ? 1 : 0;
      });
      const data = dataMapped;
      const { id } = this;
      if (window.charts[id]) {
        window.charts[id].graphs = graphs;
        window.charts[id].dataProvider = data;
        window.charts[id].validateData();
      } else {
        window.charts[id] = window.AmCharts.makeChart(that.id, {
          hideCredits: true,
          fontFamily: 'Fira Sans',
          type: 'serial',
          theme: 'light',
          dataDateFormat: 'YYYY-MM-DD',
          autoMarginOffset: 30,
          addClassNames: true,
          valueAxes: [{
            id: 'v1',
            axisAlpha: 0,
            minimum: 0,
            position: 'left',
            labelsEnabled: true,
            title: that.isWidget && 'Volatility (%)',
          }],
          chartCursor: {
            cursorPosition: 'start',
            categoryBalloonEnabled: true,
            valueLineBalloonEnabled: false,
            valueLineAlpha: 0,
            cursorAlpha: 0,
            oneBalloonOnly: true,
          },
          categoryField: 'date',
          categoryAxis: {
            parseDates: true,
            gridAlpha: 0,
            fillAlpha: 0,
            equalSpacing: true,
          },
          graphs: graphs,
          dataProvider: data,
          pathToImages: 'https://cdn.amcharts.com/lib/3/images/',
        });
      }
      this.trackLoading = false;
    },
  },
  template: `
    <div>
      <h4 class="print-border charttitle">
        {{ title + (isWidget ? '' : ' (%)') }}
      </h4>
      <div v-show="hiddenPeriods[periodGroup.dataPeriod]" class="chart chart-volatilitytrack chart-not-available">
      </div>
      <div v-show="!hiddenPeriods[periodGroup.dataPeriod]">
        <div class="clearfix chartfilter text-center">
          <button class="btn" v-bind:class="{ 'btn-blue': volatilityPeriod_small === p, 'btn-outline': volatilityPeriod_small !== p }" v-for="p in volatilityPeriods_small" @click="volatilityPeriod_small = p">
            {{ volatilityPeriods_smallLabels[p] }}
          </button>
        </div>
        <div class="volatility-track-print">{{volatilityPeriods_FullLabels[volatilityPeriod_small]}}</div>
        <div v-show="trackLoading === true" class="abs-loader"><div class="loader-inner ball-pulse"><div></div><div></div><div></div></div></div>
        <div class="chart chart-volatilitytrack" :id="id"></div>
      </div>
    </div>
  `,
});
