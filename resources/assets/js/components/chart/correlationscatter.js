/* global window Vue AmCharts */
const ACTION = require('../../store/mutation-type');
const { PERIOD } = require('../../util/period');

Vue.component('chart-correlationscatter', {
  template: `
    <div>
      <h4 class="charttitle print-border">
        {{title}}
      </h4>
      <div class="chartfilter">
        <div class="clearfix">
          <div class="btn-group col-xs-6" role="group">
            <button type="button" class="pull-right btn btn-green dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <strong :class="{ 'no-data': (benchmark2.id && main[period + '_correlation'] && main[period + '_correlation'][benchmark2.id] === 999) }">
                {{benchmark1title}}
              </strong>
              <span class="caret"></span>
            </button>
            <ul class="dropdown-menu dark">
              <li v-for="benchmark in benchmarks" :class="{ active: benchmark === benchmark1 }">
                <a @click="benchmark1 = benchmark">
                  {{benchmark.shortName}} ({{benchmark.code}})
                </a>
              </li>
            </ul>
          </div>
          <div class="btn-group col-xs-6" role="group">
            <button type="button" class="btn btn-green dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <strong :class="{ 'no-data': (benchmark1.id && main[period + '_correlation'] && main[period + '_correlation'][benchmark1.id] === 999) }">
                {{benchmark2title}}
              </strong>
              <span class="caret"></span>
            </button>
            <ul class="dropdown-menu dark">
              <li v-for="benchmark in benchmarks" :class="{ active: benchmark === benchmark2 }">
                <a @click="benchmark2 = benchmark">
                  {{benchmark.shortName}} ({{benchmark.code}})
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <div v-show="trackLoading === true && hideLoading !== true" class="abs-loader">
        <div class="loader-inner ball-pulse"><div></div><div></div><div></div></div>
      </div>
      <div class="chart chart-proportionate chart-correlationscatter" :style="pictureHeight" :id="id"></div>
    </div>
  `,
  props: [
    'title', 'idname', 'period', 'type', 'main', 'datasets', 'benchmarks',
    'showBenchmarkFullTitle', 'colors', 'correlation', 'changeview',
    'isSubgraphy', 'pdfoptions', 'basePayload', 'hideLoading'
  ],
  computed: {
    id() {
      const add = this.idname !== undefined ? this.idname : 'no-id';
      return `correlationscatter-chart-${add}`;
    },
    pictureHeight() {
      const height = this.isSubgraphy ? 305 : 450;
      return `height:${height}px;`;
    },
    strategies() {
      return this.$store.state.strategies;
    },
    portfolios() {
      return this.$store.state.portfolios;
    },
  },
  data() {
    return {
      benchmark1: {},
      benchmark2: {},
      benchmark1title: 'Select',
      benchmark2title: 'Select',
      benchmark1default: 'SPX',
      benchmark2default: 'ITRROV',
      needToLoad: false,
      loadedEvents: false,
      trackLoading: false,
    };
  },
  watch: {
    period(newValue) {
      if (newValue !== PERIOD.CUST && this.datasets === undefined) {
        this.start(); //only single part will consider the period;
      }
    },
    benchmark1(newValue, oldValue) {
      this.benchmark1title = this.showBenchmarkFullTitle === true
        ? `${newValue.shortName} (${newValue.code})`
        : newValue.code;
      if (Object.keys(oldValue).length !== 0) {
        this.start();
      }
    },
    changeview(val) {
      if (val) {
        this.start();
      }
    },
    benchmark2() {
      this.benchmark2title = this.showBenchmarkFullTitle === true
        ? `${this.benchmark2.shortName} (${this.benchmark2.code})`
        : this.benchmark2.code;
      this.start();
    },
    datasets() {
      if (this.isSubgraphy) {
        this.start();
      } else if (this.changeview) {
        this.start();
      }
    },
  },
  methods: {
    initGraph() {
      const data = {};
      const graphs = [];
      const that = this;
      data.benchmarkx = 0;
      data.benchmarky = 0;
      graphs.push({
        balloonText: 'Please choose 2 benchmarks.<br>',
        bullet: 'round',
        lineAlpha: 0,
        xField: 'benchmarkx',
        yField: 'benchmarky',
        lineColor: '#123B69',
        fillAlphas: 0,
      });
      const { id } = this;
      window.charts[id] = AmCharts.makeChart(that.id, {
        hideCredits: true,
        type: 'xy',
        theme: 'light',
        //"autoMarginOffset": 0,
        autoMargins: false,
        fontFamily: 'Fira Sans',
        dataProvider: [data],
        height: that.isSubgraphy ? 305 : 450,
        addClassNames: true,
        valueAxes: [{
          position: 'bottom',
          axisAlpha: 0,
          dashLength: 1,
          title: that.benchmark1title,
          maximum: 1,
          minimum: -1,
          id: 'x_axis',
        }, {
          axisAlpha: 0,
          dashLength: 1,
          position: 'left',
          title: that.benchmark2title,
          maximum: 1,
          minimum: -1,
          id: 'y_axis',
          synchronizeWith: 'x_axis',
          synchronizationMultiplier: 1,
        }],
        startDuration: 0,
        graphs,
        trendLines: [],
        marginLeft: 60,
        marginBottom: 60,
        marginTop: 20,
        marginRight: 20,
        zoomOutText: '',
        zoomOutButtonImage: '',
        maxZoomFactor: 1,
        pathToImages: 'https://cdn.amcharts.com/lib/3/images/',
      });
      if (!this.isSubgraphy) {
        window.charts[id].addListener('rollOverGraph', (event) => {
          const id = event.graph.id_content;
          if (id !== undefined && id !== that.main.id) {
            const indexToHighlight = {
              indexinfo: {
                id,
              },
            };
            window.eventBus.$emit('highlightIndex', indexToHighlight);
          }
        });

        window.charts[id].addListener('rollOutGraph', (event) => {
          const id = event.graph.id_content;
          if (id !== undefined && id !== that.main.id) {
            const indexToHighlight = {
              indexinfo: {
                id,
              },
            };
            window.eventBus.$emit('unhighlightIndex', indexToHighlight);
          }
        });

        window.charts[id].addListener('clickGraphItem', (event) => {
          const id = event.graph.id_content;
          if (id !== undefined && id !== that.main.id) {
            const $buttons = $(`#row-${id}`).find('button');
            if ($buttons.length) $buttons[0].click();
          }
        });
      }
      window.charts[id].validateData();
    },
    async start(reset = false) {
      if (Object.keys(this.benchmark1).length > 0 && Object.keys(this.benchmark2).length > 0) {
        let mainId = this.main.id;
        if (!this.main.id && this.main.portfolio_id) {
          mainId = this.main.portfolio_id;
        }
        const ids = [this.benchmark1.id, this.benchmark2.id];
        this.benchmark1.title = this.benchmark1.shortName;
        this.benchmark2.title = this.benchmark2.shortName;
        const otherIds = [];
        if (this.datasets && this.datasets.length > 1) {
          for (let x = 1; x < this.datasets.length; x++) {
            if (this.datasets[x].is_suspended === 0) {
              otherIds.push(this.datasets[x].id);
            }
          }
        }
        if (this.datasets && this.datasets.length < 1 && !this.isSubgraphy) {
          return null; //not ready to draw graph, subdata is not ready.
        }
        let loadIds = ids;
        let loadOtherIds = otherIds;
        if (!reset) {
          ({ loadIds, loadOtherIds } = this.checkData(mainId, ids, otherIds));
        }
        this.trackLoading = true;
        if (loadIds.length > 0 || loadOtherIds.length > 0) {
          await this.$store.dispatch(ACTION.API_FETCH_BENCHMARKS_CORRELATION, {
            id: mainId,
            type: this.type,
            payload: {
              ...this.basePayload,
              ids,
              period: this.period,
              portfolioOnly: !loadOtherIds.length,
            },
          });
        }
        this.loadChart(mainId, ids, otherIds);
      }
    },
    reset() {
      this.start(true);
    },
    checkData(mainId, ids, otherIds) {
      const key = `${this.period}_correlation`;
      const mainData = this.type === 'strategy'
        ? this.strategies[mainId]
        : this.portfolios[mainId];
      const loadIds = ids.filter(id => !(mainData[key]
        && (mainData[key][id] !== undefined)));
      const loadOtherIds = otherIds.filter(id => !(this.strategies[id][key]
        && (this.strategies[id][key][ids[0]] !== undefined)
        && (this.strategies[id][key][ids[1]] !== undefined)));
      return { loadIds, loadOtherIds };
    },
    getBenchmarkFromList(code) {
      code = code.trim();
      for (const x in this.benchmarks) {
        console.log(this.benchmarks[x].code.trim(), code);
        if (this.benchmarks[x].code.trim() === code) {
          return this.benchmarks[x];
        }
      }
      return {};
    },
    loadChart(mainId, ids, otherIds) {
      console.log('small correlationscatter');
      const { id } = this;
      const graphs = [];
      const data = {};
      // Options have been chosen
      const key = `${this.period}_correlation`;
      const mainData = this.type === 'strategy' ? this.strategies[mainId][key] : this.portfolios[mainId][key];
      data.benchmarkx = parseFloat(mainData[ids[0]]).toFixed(2);
      data.benchmarky = (ids[0] === ids[1]) ? data.benchmarkx : parseFloat(mainData[ids[1]]).toFixed(2);
      if (otherIds.length === 0) { //small graph
        graphs.push({
          // "balloonText": 'Correlation<br>',
          showBalloon: true,
          balloonText: `${this.benchmark1.title}: [[x]]<br>${this.benchmark2.title}: [[y]]`,
          bullet: 'round',
          lineAlpha: 0,
          xField: 'benchmarkx',
          yField: 'benchmarky',
          lineColor: '#123B69',
          fillAlphas: 0,
        });

        const xAxis = window.charts[id].getValueAxisById('x_axis');
        const yAxis = window.charts[id].getValueAxisById('y_axis');
        xAxis.title = this.benchmark1title;
        yAxis.title = this.benchmark2title;
        window.charts[id].graphs = graphs;
        window.charts[id].dataProvider = [data];
        window.charts[id].validateData();
      } else { //mulipoints
        graphs.push({
          // "balloonText": 'Correlation<br>',
          balloonText: `Portfolio: ${this.benchmark1.title}: [[x]]<br>${this.benchmark2.title}: [[y]]`,
          bullet: 'round',
          lineAlpha: 0,
          xField: 'benchmarkx',
          yField: 'benchmarky',
          lineColor: '#123B69',
          fillAlphas: 0,
          id_content: 'avg',
        });

        otherIds.forEach((x) => {
          data[`strategyx-${x}`] = parseFloat(this.strategies[x][key][ids[0]]).toFixed(2);
          data[`strategyy-${x}`] = (ids[0] === ids[1]) ? data[`strategyx-${x}`] : parseFloat(this.strategies[x][key][ids[1]]).toFixed(2);

          let color = '#999999';
          let size = 6;
          let title = '';
          const idx = parseInt(x, 10);
          for (let y = 1; y < this.datasets.length; y++) {
            if (idx === this.datasets[y].id) {
              title = this.datasets[y].shortName;
              if (this.datasets[y].color) { //pinned point
                color = this.colors[this.datasets[y].color];
                size = 10;
              }
            }
          }

          graphs.push({
            // "balloonText": 'Correlation<br>',
            showBalloon: true,
            balloonText: `${title}<br />${this.benchmark1.title}: [[x]]<br>${this.benchmark2.title}: [[y]]`,
            bullet: 'round',
            lineAlpha: 0,
            xField: `strategyx-${x}`,
            yField: `strategyy-${x}`,
            lineColor: color,
            fillAlphas: 0,
            bulletSize: size,
            id_content: x,
            id: `correlationscatter-${x}`,
          });
        });

        const xAxis = window.charts[id].getValueAxisById('x_axis');
        const yAxis = window.charts[id].getValueAxisById('y_axis');
        xAxis.title = this.benchmark1title;
        yAxis.title = this.benchmark2title;

        window.charts[id].graphs = graphs;
        window.charts[id].dataProvider = [data];
        window.charts[id].validateData();
      }
      this.trackLoading = false;
    },
  },
  mounted() {
    console.log('picture mounted');
    if (this.pdfoptions) {
      this.benchmark1 = this.getBenchmarkFromList(this.pdfoptions.codes[0]);
      this.benchmark2 = this.getBenchmarkFromList(this.pdfoptions.codes[1]);
    } else {
      this.benchmark1 = this.getBenchmarkFromList(this.benchmark1default);
      this.benchmark2 = this.getBenchmarkFromList(this.benchmark2default);
    }
    this.initGraph();
    window.eventBus.$on('cust-period.fetch', this.reset);
    // this.loadData();
  },
  beforeDestroy() {
    window.eventBus.$off('cust-period.fetch', this.reset);
    const { id } = this;
    if (window.charts[id] != null) {
      window.charts[id].clear();
      delete window.charts[id];
    }
    this.$off();
  },
});
