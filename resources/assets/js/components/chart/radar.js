const bowser = require('bowser');
const { metrics } = require('../../util/config');

Vue.component('chart-radar', {
  template: '<div class="chart-wrapper"><h4 class="print-border charttitle">{{title}}</h4> <div v-show="doneLoading === false" class="abs-loader"><div class="loader-inner ball-pulse"><div></div><div></div><div></div></div></div><div v-show="hiddenPeriods[period]" class="chart chart-radar chart-not-available"></div><div v-show="!hiddenPeriods[period]" class="chart chart-radar" :style="pinnedHeight" :id="id" @click="closeMenu"></div><ul id="right-click-menu" class="dropdown-menu" tabindex="-1" ref="right" v-show="viewMenu" @blur="closeMenu" :style="{top:top, left:left}"><li @click="closeMenu" style="border-bottom:1px solid #E0E0E0"><a>{{ rightClickActive }}</a></li><li v-for="(header, field) in availableValues" v-if="header != rightClickActive" :key="field" @click="selectValue(field)"><a>{{ header }}</a></li></ul></div>',
  props: [
    'title', 'idname', 'type', 'main', 'datasets', 'period', 'colors', 'loading', 'preference', 'setPreference', 'pinnedColors',
  ],
  computed: {
    id() {
      const add = this.idname !== undefined ? this.idname : 'no-id';
      return `radar-chart-${add}`;
    },
    strategies() {
      return this.$store.state.strategies;
    },
    portfolios() {
      return this.$store.state.portfolios;
    },
    hiddenPeriods() {
      return this.$store.state.hiddenPeriods;
    },
  },
  data() {
    return {
      pinnedHeight: '',
      chart: null,
      doneLoading: false,
      viewMenu: false,
      top: '0px',
      left: '0px',
      rightClickActive: '',
      rightClickIndex: 0,
      initEvents: false,
      availableValues: metrics,
      valuesFor: [
        ['volatility', 'Volatility'],
        ['return', 'Return p.a.'],
        ['mdd', 'Max DD'],
        ['sortino', 'Sortino'],
        ['sharpe', 'Sharpe'],
      ],
    };
  },
  watch: {
    loading(value) {
      if (value) {
        this.doneLoading = false;
      }
    },
    datasets() {
      this.start();
    },
    period() {
      this.doneLoading = false;
      this.start();
    },
  },
  methods: {
    start() {
      this.loadChart();
    },
    shortenReturnType(str) {
      if (!str) {
        return '';
      }
      str = str.replace('Excess Return', 'ER');
      str = str.replace('Total Return', 'TR');
      return str;
    },
    shortenReturnCategory(str) {
      if (!str) {
        return '';
      }
      str = str.replace('Net', 'N');
      return str;
    },

    loadChart() {
      const that = this;
      console.log('small radar graph');

      if (
        !this.datasets ||
        this.datasets.length === 0 ||
        this.colors.length === 0
      ) {
        // don't touch
        return;
      }

      const { valuesFor } = this;
      const data_obj = {};

      const graphs = [];
      const { period } = this;
      const tempBoundaries = {};

      const units = {};
      if (this.datasets.length) {
        const [benchmark] = this.datasets;
        for (const [fieldprop] of valuesFor) {
          units[fieldprop] = benchmark[`um_${fieldprop}`] || '';
        }
      }

      for (let i = 0; i < this.datasets.length; i++) {
        const benchmark = this.datasets[i];
        for (const i2 in valuesFor) {
          const fieldprop = valuesFor[i2][0];
          if (!tempBoundaries[fieldprop]) {
            tempBoundaries[fieldprop] = { min: 0, max: 0 };
            switch (fieldprop) {
              case 'mdd':
                tempBoundaries[fieldprop].min = -50;
                tempBoundaries[fieldprop].max = 0;
                break;
              case 'volatility':
                tempBoundaries[fieldprop].min = 0;
                tempBoundaries[fieldprop].max = 50;
                break;
              case 'sharpe':
                tempBoundaries[fieldprop].min = -4;
                tempBoundaries[fieldprop].max = 4;
                break;
              case 'sortino':
                tempBoundaries[fieldprop].min = -5;
                tempBoundaries[fieldprop].max = 5;
                break;
              case 'return':
                tempBoundaries[fieldprop].min = -10;
                tempBoundaries[fieldprop].max = 25;
                break;
              case 'skewness':
              case 'posMonths':
              case 'posWeeks':
                tempBoundaries[fieldprop].min = -3;
                tempBoundaries[fieldprop].max = 3;
                break;
              case 'kurtosis':
                tempBoundaries[fieldprop].min = 0;
                tempBoundaries[fieldprop].max = 100;
                break;
              case 'calmar':
                tempBoundaries[fieldprop].min = -5;
                tempBoundaries[fieldprop].max = 5;
                break;
              case 'omega':
                tempBoundaries[fieldprop].min = 0;
                tempBoundaries[fieldprop].max = 10;
                break;
              case 'var':
              case 'mvar':
              case 'cvar':
                tempBoundaries[fieldprop].min = 0;
                tempBoundaries[fieldprop].max = 2.5;
                break;
              case 'avg_drawdown':
                tempBoundaries[fieldprop].min = -5;
                tempBoundaries[fieldprop].max = 0;
                break;
              case 'worst20d':
                tempBoundaries[fieldprop].min = -30;
                tempBoundaries[fieldprop].max = 0;
                break;
            }
          }

          const value = parseFloat(benchmark[`${period}_${fieldprop}`]);

          if (tempBoundaries[fieldprop].max < value) {
            tempBoundaries[fieldprop].max = this.formatValue(fieldprop, value);
          }

          if (tempBoundaries[fieldprop].min > value) {
            tempBoundaries[fieldprop].min = this.formatValue(fieldprop, value);
          }
        }
      }

      // tempBoundaries.mdd.max = Math.ceil(tempBoundaries.mdd.max);
      // tempBoundaries.volatility.max = Math.ceil(tempBoundaries.volatility.max);
      // tempBoundaries.return.max = Math.ceil(tempBoundaries.return.max);
      // tempBoundaries.sortino.max = tempBoundaries.sortino.max.toFixed(2);
      // tempBoundaries.sharpe.max = tempBoundaries.sharpe.max.toFixed(2);

      // tempBoundaries.mdd.min = Math.ceil(tempBoundaries.mdd.min);
      // tempBoundaries.volatility.min = Math.ceil(tempBoundaries.volatility.min);
      // tempBoundaries.return.min = Math.ceil(tempBoundaries.return.min);
      // tempBoundaries.sortino.min = tempBoundaries.sortino.min.toFixed(2);
      // tempBoundaries.sharpe.min = tempBoundaries.sharpe.min.toFixed(2);

      for (const i in valuesFor) {
        const fieldprop = valuesFor[i][0];
        const fieldtitle = valuesFor[i][1];
        let unit = units[fieldprop] && ` (${units[fieldprop]})`;
        if (this.type === 'pinned' && (bowser.msie || bowser.msedge)) {
          unit = ' ▼';
        } else if (this.type === 'pinned') {
          unit = ' <tspan font-size="10px">▼</tspan>';
        }
        data_obj[fieldprop] = {
          metric: `${fieldtitle}${unit}\n\r[${tempBoundaries[fieldprop].min}${
            units[fieldprop]
          } to ${tempBoundaries[fieldprop].max}${units[fieldprop]}]`,
        };
      }

      for (let i = 0; i < this.datasets.length; i++) {

        const isPortfolio = this.datasets[i].shortName === undefined;
        const benchmark = !isPortfolio ? this.strategies[this.datasets[i].id] : this.portfolios[this.datasets[i].id];
        for (const i2 in valuesFor) {
          const fieldprop = valuesFor[i2][0];

          let value = parseFloat(benchmark[`${period}_${fieldprop}`]);
          data_obj[fieldprop][`original_benchmark${i}`] = `${parseFloat(value).toFixed(2)}${units[fieldprop]}`;
          switch (fieldprop) {
            case 'volatility':
            case 'kurtosis':
            case 'var':
            case 'mvar':
            case 'cvar': {
              const scale_max = tempBoundaries[fieldprop].max;
              const scale_min = tempBoundaries[fieldprop].min;
              value = (value - scale_min) / (scale_max - scale_min); //scale it to % of all strategies.
              value = 100 * (1 - value);
              break;
            }
            default: {
              const scale_max = tempBoundaries[fieldprop].max;
              const scale_min = tempBoundaries[fieldprop].min;
              value = (value - scale_min) / (scale_max - scale_min);
              value *= 100;
              break;
            }
          }
          // if (fieldprop === 'volatility') {
          //   const scale_max = tempBoundaries[fieldprop].max;
          //   const scale_min = tempBoundaries[fieldprop].min;
          //   value = (value - scale_min) / (scale_max - scale_min); //scale it to % of all strategies.
          //   value = 100 * (1 - value);
          // } else {
          //   const scale_max = tempBoundaries[fieldprop].max;
          //   const scale_min = tempBoundaries[fieldprop].min;
          //   value = (value - scale_min) / (scale_max - scale_min);
          //   value *= 100;
          // }
          data_obj[fieldprop][`benchmark${i}`] = value;
        }

        let { color } = this.datasets[i];
        if (this.type === 'pinned') {
          color = this.pinnedColors[this.colors[benchmark.id]];
        } else if (!color) {
          color = '#ddd';
        } else {
          color = this.colors[color];
        }
        if (benchmark.id === this.main.id) {
          color = '#123B69';
        }

        let graphtitle = '';
        if (benchmark.type === 'portfolio' || (benchmark.title && !benchmark.shortName)) {
          // its a portfolio
          graphtitle = benchmark.title;
        } else {
          // its a strat
          graphtitle = `${benchmark.shortName}<br />${benchmark.assetClass} | ${benchmark.currency} ${this.shortenReturnCategory(benchmark.returnCategory)}${this.shortenReturnType(benchmark.returnType)} ${benchmark.volTarget}`;
        }

        graphs.push({
          balloonText: `${graphtitle}<br>[[description]]`,
          bullet: 'round',
          bulletSize: 2,
          valueField: `benchmark${i}`,
          descriptionField: `original_benchmark${i}`,
          lineColor: color,
          lineThickness: 2,
        });
      }

      const data = this.valuesFor.map(o => data_obj[o[0]]);

      const { id } = this;
      if (window.charts[id]) {
        window.charts[id].graphs = graphs;
        window.charts[id].dataProvider = data;
        window.charts[id].validateData();
      } else {
        window.charts[id] = window.AmCharts.makeChart(that.id, {
          hideCredits: true,
          type: 'radar',
          theme: 'light',
          addClassNames: true,
          dataProvider: data,
          startDuration: 0,
          graphs,
          categoryField: 'metric',
          fontFamily: 'Fira Sans',
          valueAxes: [
            {
              maximum: 100,
              minimum: 0,
              axisAlpha: 0.2,
              axisTitleOffset: 5,
            },
          ],
        });
        if (this.type === 'pinned') {
          window.charts[id].addListener('drawn', () => {
            that.clickAxis();
          });
        } else {
          window.charts[id].addListener('drawn', () => {
            const labels = document.querySelectorAll(`#${that.id} .amcharts-axis-title`);
            that.setTitle(labels);
          });
        }
      }
      this.doneLoading = true;
    },
    formatValue(field, value) {
      switch (field) {
        case 'mdd':
        case 'volatility':
        case 'sharpe':
          return Math.ceil(value);
        case 'sortino':
        case 'return':
          return Math.ceil(value * 100) / 100;
      }
      return value;
    },
    setEventListener() {
      const labels = document.querySelectorAll('.chart-radar .amcharts-axis-title');
      if (labels.length !== 5) {
        return '';
      }
      this.initEvents = true;
      this.clickAxis();
    },
    setTitle(labels) {
      if (labels.length <= 0) {
        return null;
      }
      const isIe = bowser.msie || bowser.msedge;
      const trasform = isIe ? $(labels[0]).attr('transform').split(' ') : $(labels[0]).attr('transform').split(',');
      if (trasform.length !== 2) {
        return null;
      }
      const height = parseInt(trasform[1].replace(')', ''), 10);
      if (isIe) {
        $(labels[0]).attr('transform', `translate(0 ${height - 15})`);
      } else {
        $(labels[0]).attr('transform', `translate(0,${height - 15})`);
      }
    },
    setTimeoutTitle() {
      const labels = document.querySelectorAll(`#${this.id} .amcharts-axis-title`);
      if (labels.length === 5) {
        this.initEvents = true;
        this.setTitle(labels);
      }
    },
    clickAxis() {
      const that = this;
      const labels = document.querySelectorAll('.chart-radar .amcharts-axis-title');
      this.setPinnedTitle(labels);
      for (let i = 0; i < labels.length; i++) {
        labels[i].addEventListener('click', (e) => {
          that.openMenu(e);
        });
      }
    },
    setPinnedTitle(labels) {
      const isIe = bowser.msie || bowser.msedge;
      const trasform = isIe ? $(labels[0]).attr('transform').split(' ') : $(labels[0]).attr('transform').split(',');
      if (trasform.length !== 2) {
        return null;
      }
      const height = parseInt(trasform[1].replace(')', ''), 10);
      if (isIe) {
        $(labels[0]).attr('transform', `translate(0 ${height - 20})`);
      } else {
        $(labels[0]).attr('transform', `translate(0,${height - 20})`);
      }
      $(labels).each((idx) => {
        $(labels[idx]).attr('font-size', '14px');
        $(labels[idx]).children().last().attr('y', 25);
        $(labels[idx]).children().first().html($(labels[idx]).children().first().text());
      });
    },
    openMenu(e) {
      const idx = (Array.prototype.indexOf.call(e.currentTarget.parentNode.childNodes, e.currentTarget) - 1) / 2;
      this.rightClickIndex = idx;
      this.rightClickActive = this.valuesFor[this.rightClickIndex][1];
      this.viewMenu = true;
      this.$nextTick(() => {
        this.$refs.right.focus();
        this.top = '90px';
        this.left = `${e.layerX + 60}px`;
      });
      e.preventDefault();
      e.stopPropagation();
    },
    closeMenu() {
      this.viewMenu = false;
    },
    selectValue(field) {
      this.viewMenu = false;
      this.valuesFor[this.rightClickIndex] = [field, this.availableValues[field]];
      const data = _.cloneDeep(this.valuesFor);
      this.setPreference('tools', data);
      this.loadChart();
    },
  },
  created() {
    if (this.type === 'pinned') {
      this.pinnedHeight = 'height: 500px';
    }
  },
  mounted() {
    const that = this;
    if (this.preference && this.preference.length === 5) {
      this.valuesFor = this.preference;
    }
    this.start();
    if (this.type === 'pinned') {
      this.$nextTick(() => {
        console.log('start to add events');
        that.setEventListener();
      });
    } else {
      this.$nextTick(() => {
        console.log('start to add events');
        that.setTimeoutTitle();
      });
    }
  },
  beforeDestroy() {
    const { id } = this;
    if (window.charts[id]) {
      if (this.type === 'pinned') {
        $(`#${id}`).children().off(); //remove manually events
      }
      window.charts[id].clear();
      delete window.charts[id];
    }
    this.$off();
  },
});
