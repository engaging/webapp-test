Vue.component('chart-weighting', {
  template: '<div><h4 class="print-border charttitle">{{title + " (%)"}}</h4><div v-show="hiddenPeriods[period]" class="chart chart-weighting chart-not-available"></div><div v-show="!hiddenPeriods[period]" class="chart chart-weighting" :id="id" style="height:333px;"></div></div>',
  props: [
    'title', 'idname', 'type', 'main', 'period', 'indices', 'setEvent', 'datasets', 'colors',
  ],
  data() {
    return {
      // trackData: {},
      // chart: null,
      highlightId: null,
    };
  },
  computed: {
    id() {
      const add = this.idname !== undefined ? this.idname : 'no-id';
      return `allocation-chart-${add}`;
    },
    hiddenPeriods() {
      return this.$store.state.hiddenPeriods;
    },
  },
  watch: {
    period() {
      this.loadChart();
    },
    indices(newValue, oldValue) {
      if (oldValue.length === 0 && newValue.length !== 0) {
        this.loadChart();
      }
    },
    main() {
      if (!this.colors) {
        this.loadChart();
      }
    },
    datasets() {
      this.loadChart();
    },
  },
  methods: {
    loadChart() {
      console.log('small return allocations charts');
      const that = this;
      const current = this.main;
      const period = this.period;
      const dateKey = `${period}_date_track`;
      const date = current[dateKey] || [];
      const dataKey = `${period}_daily_weights`;
      const rawData = current[dataKey] || {};
      const lastDay = date[date.length - 1];
      const trackData = {};
      for (const time in rawData) {
        const dataPoint = {};
        let totalMonthWeights = 0;
        for (const idx in rawData[time]) {
          dataPoint[idx] = rawData[time][idx];
          totalMonthWeights += dataPoint[idx];
        }
        if (totalMonthWeights !== 100.00) {
          for (const idx in dataPoint) {
            dataPoint[idx] = parseFloat((100 * dataPoint[idx] / totalMonthWeights).toFixed(2));
          }
        }
        trackData[time] = dataPoint;
      }
      date.forEach((o) => {
        if (trackData[o] === undefined) {
          trackData[o] = null;
        }
      });
      const loopDate = Object.keys(trackData).sort();
      if (trackData[lastDay] === null) {
        loopDate.pop();
      }
      const data = [];
      for (let index = 0; index < loopDate.length; index++) {
        if (trackData[loopDate[index]] === null) {
          trackData[loopDate[index]] = trackData[loopDate[index - 1]];
        }
        const finalData = _.cloneDeep(trackData[loopDate[index]]);
        finalData.date = loopDate[index];
        data.push(finalData);
      }
      const graphs = [];
      let count = 0;
      const coloredColumn = {};
      if (this.colors) {
        for (let idx = 1; idx < this.datasets.length; idx++) {
          const item = this.datasets[idx];
          coloredColumn[item.code] = this.colors[item.color];
        }
      }
      for (const idCode in data[0]) {
        if (idCode !== 'date') {
          const indexinfo = this.indices.find(o => o.indexinfo.code === idCode).indexinfo;
          // const color = (count % 2 === 1) ? '#939393' : '#bababa';
          let fillColor = (count % 2 === 1) ? '#bababa' : '#123B69';
          let lineColor = '#123B69';
          if (coloredColumn[idCode] !== undefined) {
            fillColor = coloredColumn[idCode];
            lineColor = coloredColumn[idCode];
          }
          count++;
          graphs.push({
            id: `allocation-${indexinfo.id}`,
            valueField: idCode,
            // lineColor: color,
            lineColor,
            fillColors: fillColor,
            lineThickness: 2,
            type: 'step',
            balloonText: `${indexinfo.shortName}<br>[[value]]%`,
            fillAlphas: 0.2,
            lineAlpha: 1,
            id_content: indexinfo.id,
          });
        }
      }

      const { id } = this;
      if (window.charts[id]) {
        window.charts[id].graphs = graphs;
        window.charts[id].dataProvider = data;
        window.charts[id].validateData();
      } else {
        const chartCursor = {
          cursorPosition: 'mouse',
          categoryBalloonEnabled: true,
          valueLineBalloonEnabled: false,
          valueLineAlpha: 0,
          cursorAlpha: 0,
          oneBalloonOnly: true,
        };
        if (this.setEvent) {
          chartCursor.listeners = this.setEvents();
        }
        window.charts[id] = window.AmCharts.makeChart(that.id, {
          hideCredits: true,
          fontFamily: 'Fira Sans',
          type: 'serial',
          theme: 'light',
          autoMarginOffset: 30,
          addClassNames: true,
          dataDateFormat: 'YYYY-MM-DD',
          graphs,
          categoryField: 'date',
          valueAxes: [{
            fillAlpha: 0,
            gridAlpha: 0,
            id: 'v1',
            stackType: '100%',
          }],
          chartCursor,
          balloon: {
            borderThickness: 2,
            shadowAlpha: 0,
          },
          categoryAxis: {
            // centerLabelOnFullPeriod: false,
            fillAlpha: 0,
            gridAlpha: 0,
            parseDates: true,
            equalSpacing: true,
          },
          dataProvider: data,
          pathToImages: 'https://cdn.amcharts.com/lib/3/images/',
        });
      }
    },
    setEvents() {
      const that = this;
      const { id } = this;
      const onHideCursorHandler = () => {
        window.eventBus.$emit('unhighlightIndex', {
          indexinfo: {
            id: that.highlightId,
          },
        });
        that.highlightId = null;
      };
      const movedHandler = (event) => {
        if (event.mostCloseGraph) {
          const id = event.mostCloseGraph.id_content;
          const indexToHighlight = {
            indexinfo: {
              id,
            },
          };
          if (id !== that.highlightId) {
            if (that.highlightId !== null) {
              window.eventBus.$emit('unhighlightIndex', {
                indexinfo: {
                  id: that.highlightId,
                },
              });
            }
            window.eventBus.$emit('highlightIndex', indexToHighlight);
            that.highlightId = id;
          }
        }
      };
      const func = [{
        event: 'onHideCursor',
        method: onHideCursorHandler,
      }, {
        event: 'moved',
        method: movedHandler,
      }];
      return func;
    },
  },

  mounted() {
    if (this.indices.length > 0) {
      this.loadChart();
    }
  },
  beforeDestroy() {
    const { id } = this;
    if (window.charts[id]) {
      window.charts[id].clear();
      delete window.charts[id];
    }
    this.$off();
  },
});
