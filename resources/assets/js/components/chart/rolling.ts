/* global window document Vue $ */
import { Vue, Component, Prop, Watch } from 'vue-property-decorator'
import AmStockChart from 'amcharts/AmStockChart';

type BaseDatasetType = {
  code: string;
  color: string;
  id: number;
  shortName: string;
  track: number[];
};

type ChartEvent = {
  startDate: Date;
  endDate: Date;
  chart: AmStockChart;
};

type DatasetsType = {
  dates: string[];
  summary: {
    rSquared_track: number[];
  };
  main: BaseDatasetType[];
  alphaTrack: {
    dates: string[];
    tracks: number[];
  }
};

type GraphMenu = {
  rSquared: string,
  alphaTrack: string,
};

type GraphType = {
  id: string,
  id_content: number,
  lineColor: string,
  lineThickness: number,
  valueField: string,
  showBalloon: boolean,
  useDataSetColors: boolean,
  balloonText: string,
};

type GraphDataType = {
  date: string;
  rSquared: number;
  alphaTrack: number;
  [code: string]: number | string;
};

// <div v-if="!hidden" class="dropdown graph-options print-hide pull-right"
//   style="z-index: 20;top: 525px;">
//   <button class="btn btn-default dropdown-toggle" type="button"
//     id="rollingChartMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
//     <i class="fa fa-ellipsis-v"></i>
//   </button>
//   <ul class="dropdown-menu" aria-labelledby="rollingChartMenu">
//     <li v-for="(val, key) in dropdownMenu">
//       <a v-bind:class="{'active':chooseItem===val}" @click="setGraph(val)">
//         {{ val }}
//       </a>
//     </li>
//   </ul>
// </div>

@Component({
  template:
    `<div style="position:relative;">
      <a class="button" id="show-all-button"
        style="position:absolute;top:39px;right:20px;display:none;z-index:20;">
        <img width="19" height="19" class="amcharts-zoom-out-image"
          src="https://cdn.amcharts.com/lib/3/images/lens.svg">
        <span>Show all</span>
      </a>
      <h4 v-if="title" class="print-border charttitle">{{title}}</h4>
      <div v-show="hidden" class="chart chart-rolling chart-not-available"></div>
      <div v-show="!hidden" class="chart chart-rolling" :id="id" style="height:700px;"></div>
    </div>`,
})
export default class RollingChart extends Vue {
  @Prop({
    type: String,
    default: '',
  })
  title!: string;

  @Prop({
    type: String,
    default: 'no-id',
  })
  idname!: string;

  @Prop({
    type: Object,
    default: () => {},
  })
  datasets!: DatasetsType;

  @Prop({
    type: Object,
    default: () => {},
  })
  colors!: {[colorname: string]: string};

  @Prop({
    type: Boolean,
    default: false,
  })
  hidden!: boolean;

  get id() {
    return `rolling-chart-${this.idname}`;
  }

  dropdownMenu: GraphMenu = {
    rSquared: 'R Squared',
    alphaTrack: 'Cumulated Alpha',
  };

  chooseItem:string = 'R Squared';

  @Watch('datasets')
  onDatasetChange() {
    this.loadChart();
  }

  mounted() {
    const { id } = this;
    this.loadChart();
    $('#show-all-button').click(() => {
      window.charts[id].zoomOut();
    });
  }

  beforeDestroy() {
    const { id } = this;
    $('#show-all-button').off();
    if (window.charts[id]) {
      window.charts[id].clear();
      delete window.charts[id];
    }
    this.$off();
  }

  setGraph(val: string) {
    this.chooseItem = val;
    this.loadChart();
  }

  loadChart() {
    console.log('small return rolling charts');
    const that = this;
    const data: GraphDataType[] = [];
    const upperGraphs: GraphType[] = [];
    const isRSquared = this.chooseItem === this.dropdownMenu.rSquared;
    const lowerGraph = [{
      id: 'g0',
      valueField: isRSquared ? 'rSquared' : 'alphaTrack',
      balloonText: `${isRSquared ? 'R<sup>2</sup>' : 'Alpha'}<br>[[value]]%`,
      type: 'line',
      showBalloon: true,
      fillAlphas: isRSquared ? 0 : 0.2,
      negativeFillAlphas: isRSquared ? 0 : 0.2,
      negativeLineColor: '#b7294f',
      useDataSetColors: false,
      lineColor: '#1A85A1',
    }];

    const fieldMappings = isRSquared ? [{
      fromField: 'rSquared',
      toField: 'rSquared',
    }] : [{
      fromField: 'alphaTrack',
      toField: 'alphaTrack',
    }];

    if (this.datasets.dates && this.datasets.dates.length > 0) {
      this.datasets.dates.forEach((date, idx) => {
        const rSquared = this.datasets.summary.rSquared_track[idx];
        const dateData:GraphDataType = {
          date,
          rSquared: Math.round(rSquared * 100),
          alphaTrack: this.datasets.alphaTrack.tracks[idx],
        };
        this.datasets.main.forEach((o) => {
          dateData[o.code] = o.track[idx];
        });
        data.push(dateData);
      });
      this.datasets.main.forEach((o, idx) => {
        let color = this.colors[o.color] || '#cccccc';
        let size = this.colors[o.color] ? 3 : 2;
        if (o.code === 'alpha') {
          size = 3;
          color = '#123B69';
        }
        fieldMappings.push({
          fromField: o.code,
          toField: o.code,
        });
        upperGraphs.push({
          id: `g${o.id}`,
          id_content: o.id,
          lineColor: color,
          lineThickness: size,
          valueField: o.code,
          showBalloon: true,
          useDataSetColors: false,
          balloonText: `${o.shortName}<br>[[value]]`,
          // type: 'smoothedLine',
        });
      });
    }
    const { id } = this;
    if (window.charts[id]) {
      window.charts[id].dataSets[0].fieldMappings = fieldMappings;
      window.charts[id].dataSets[0].dataProvider = data;
      window.charts[id].panels[0].stockGraphs = upperGraphs;
      window.charts[id].panels[1].stockGraphs = lowerGraph;
      window.charts[id].panels[1].title = `${this.chooseItem} (%)`;
      window.charts[id].needZoomout = true;
      window.charts[id].validateNow(true, true);
    } else {
      const rollOverGraphHandler = (event: {
        graph: GraphType,
      }) => {
        const id = event.graph.id_content;
        if (id) {
          const indexToHighlight = {
            indexinfo: {
              id,
            },
          };
          window.eventBus.$emit('highlightIndex', indexToHighlight);
        }
      };
      const rollOutGraphHandler = (event: {
        graph: GraphType,
      }) => {
        const id = event.graph.id_content;
        if (id) {
          const indexToHighlight = {
            indexinfo: {
              id,
            },
          };
          window.eventBus.$emit('unhighlightIndex', indexToHighlight);
        }
      };

      window.charts[id] = window.AmCharts.makeChart(that.id, {
        hideCredits: true,
        type: 'stock',
        legend: {
          useGraphSettings: true,
        },
        addClassNames: true,
        dataSets: [{
          fieldMappings,
          dataProvider: data,
          categoryField: 'date',
        }],
        panels: [{
          showCategoryAxis: true,
          type: 'serial',
          categoryAxis: {
            gridThickness: 0,
            gridAlpha: 0,
            fillAlpha: 0,
            axisAlpha: 1,
            position: 'top',
          },
          hideCredits: true,
          title: '',
          guides: [{
            lineAlpha: 1,
            value: 0,
          }],
          listeners: [
            {
              event: 'rollOverGraph',
              method: rollOverGraphHandler,
            },
            {
              event: 'rollOutGraph',
              method: rollOutGraphHandler,
            },
          ],
          percentHeight: 70,
          stockGraphs: upperGraphs,
          chartCursor: {
            // cursorPosition: 'start',
            categoryBalloonEnabled: true,
            valueLineAlpha: 0,
            cursorAlpha: 0,
            oneBalloonOnly: true,
          },
        }, {
          title: `${this.chooseItem} (%)`,
          // marginTop: 1,
          percentHeight: 30,
          categoryAxis: {
            gridThickness: 0,
            gridAlpha: 0,
            fillAlpha: 0,
            axisAlpha: 1,
          },
          hideCredits: true,
          stockGraphs: lowerGraph,
          stockLegend: {
            fontSize: 12,
            valueText: '',
            labelText: '',
            markerSize: 0,
            markerType: 'none',
          },
        }],

        chartScrollbarSettings: {
          enabled: false,
        },

        valueAxesSettings: {
          inside: false,
          includeGuidesInMinMax: true,
          zeroGridAlpha: 0,
        },

        categoryAxesSettings: {
          axisAlpha: 0.3,
        },

        legendSettings: {
          marginTop: 5,
          marginBottom: 10,
        },

        panelsSettings: {
          marginLeft: 35,
          marginBottom: 30,
        },
        zoomOutOnDataSetChange: true,
        chartCursorSettings: {
          valueBalloonsEnabled: true,
          // fullWidth: true,
          cursorColor: '#000',
          cursorAlpha: 1,
          oneBalloonOnly: true,
          // valueLineBalloonEnabled: true,
        },
        pathToImages: 'https://cdn.amcharts.com/lib/3/images/',
      });
      window.charts[id].addListener('zoomed', (e: ChartEvent) => {
        const startDate = e.startDate.toISOString().slice(0, 10);
        const endDate = e.endDate.toISOString().slice(0, 10);
        const data = e.chart.dataSets[0].dataProvider;
        if (startDate >= data[0].date || endDate <= data[data.length - 1].date) {
          $('#show-all-button').show();
        } else {
          $('#show-all-button').hide();
        }
      });
      window.charts[id].addListener('dataUpdated', (e: ChartEvent) => {
        if (e.chart && e.chart.zoomOut) e.chart.zoomOut();
      });
    }
  }
}
