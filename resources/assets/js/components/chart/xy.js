/* global window Vue AmCharts $ */
const { metrics } = require('../../util/config');

Vue.component('chart-xy', {
  props: {
    title: {
      type: String,
      default: null,
    },
    idname: {
      type: String,
      default: 'no-id',
    },
    main: {
      type: Object,
      default: () => {},
    },
    period: {
      type: String,
      default: '5y',
    },
    datasets: {
      type: Array,
      default: () => [],
    },
    colors: {
      type: Object,
      default: () => {},
    },
    isSubgraphy: {
      type: Boolean,
      default: true,
    },
  },
  data() {
    return {
      xAxis: 'volatility',
      yAxis: 'return',
      labels: metrics,
      dataLoading: true,
      // showMenu: false,
    };
  },
  computed: {
    id() {
      return `xy-chart-${this.idname}`;
    },
    subgraphyPadding() {
      return this.isSubgraphy ? 'padding-left: 0px;padding-right: 30px;' : '';
    },
    strategies() {
      return this.$store.state.strategies;
    },
    portfolios() {
      return this.$store.state.portfolios;
    },
    hiddenPeriods() {
      return this.$store.state.hiddenPeriods;
    },
  },
  watch: {
    datasets() {
      this.loadChart();
    },
    period() {
      this.loadChart();
    },
    xAxis() {
      this.loadChart();
    },
    yAxis() {
      this.loadChart();
    },
  },
  mounted() {
    this.loadChart();
  },
  beforeDestroy() {
    const { id } = this;
    if (window.charts[id] != null) {
      window.charts[id].clear();
      delete window.charts[id];
    }
    this.$off();
  },
  methods: {
    loadChart() {
      console.log('small xy');
      const graphs = [];
      let dataProvider = {};
      let xUnit = '';
      let yUnit = '';

      if (this.datasets.length > 0) {
        xUnit = this.datasets[0][`um_${this.xAxis}`] || '';
        yUnit = this.datasets[0][`um_${this.yAxis}`] || '';

        for (let idx = 0; idx < this.datasets.length; idx++) {
          // For each index to show add a dot...
          const isPortfolio = !!this.datasets[idx].title && !this.datasets[idx].shortName;
          const index = isPortfolio ? this.portfolios[this.datasets[idx].id] : this.strategies[this.datasets[idx].id];

          const colorType = this.datasets[idx].color;
          let size = 6;
          let color = '#A9A9A9';
          if (colorType) {
            color = this.colors[colorType];
            size = 10;
          }
          if (index.id === this.main.id) {
            color = '#123b69';
            size = 10;
          }

          dataProvider[`${idx}x`] = parseFloat(index[`${this.period}_${this.xAxis}`]).toFixed(2);
          dataProvider[`${idx}y`] = parseFloat(index[`${this.period}_${this.yAxis}`]).toFixed(2);
          const indexid = index.id;

          let graphtitle = index.title;
          if (index.shortName !== undefined) {
            graphtitle = `${index.shortName}<br>${index.assetClass} | ${index.currency} ${index.returnCategory}${index.returnType} ${index.volTarget}`;
          }

          const id = isPortfolio ? `p${index.id}` : `g${index.id}`;
          graphs.push({
            id,
            balloonText: `${graphtitle}<br>${this.labels[this.xAxis]}:[[x]]${xUnit} ${this.labels[this.yAxis]}:[[y]]${yUnit}`,
            bullet: 'round',
            lineAlpha: 0,
            xField: `${idx}x`,
            yField: `${idx}y`,
            lineColor: color,
            fillAlphas: 0,
            bulletSize: size,
            indexObject: indexid,
          });
        }
      }

      dataProvider = [dataProvider];
      // clear old data
      const { id } = this;
      if (window.charts[id] != null) {
        window.charts[id].dataProvider = dataProvider;
        window.charts[id].valueAxes[0].title = this.labels[this.xAxis] + (xUnit && ` (${xUnit})`);
        window.charts[id].valueAxes[1].title = this.labels[this.yAxis] + (yUnit && ` (${yUnit})`);
        window.charts[id].graphs = graphs;
        window.charts[id].validateData();
      } else {
        window.charts[id] = AmCharts.makeChart(this.id, {
          hideCredits: true,
          type: 'xy',
          theme: 'light',
          autoMarginOffset: 20,
          dataProvider,
          fontFamily: 'Fira Sans',
          balloon: { cornerRadius: 5 },
          addClassNames: true,
          valueAxes: [{
            position: 'bottom',
            axisAlpha: 0,
            dashLength: 1,
            title: this.labels[this.xAxis] + (xUnit && ` (${xUnit})`),
          }, {
            axisAlpha: 0,
            dashLength: 1,
            position: 'left',
            title: this.labels[this.yAxis] + (yUnit && ` (${yUnit})`),
          }],
          graphs,
          trendLines: [],
          marginLeft: 64,
          marginBottom: 60,
          chartScrollbarOff: {},
          chartCursor: {
            cursorPosition: 'start',
            pan: false,
            valueLineEnabled: false,
            valueLineBalloonEnabled: false,
            valueBalloonsEnabled: true,
            cursorAlpha: 0,
            oneBalloonOnly: true,
          },
          pathToImages: 'https://cdn.amcharts.com/lib/3/images/',
        });
        if (!this.isSubgraphy) {
          this.setEvents();
        }
      }
      this.dataLoading = false;
    },
    setEvents() {
      const { id } = this;
      const that = this;
      window.charts[id].addListener('rollOverGraph', (event) => {
        const id = event.graph.indexObject;
        if (id !== undefined && id !== that.main.id) {
          const indexToHighlight = {
            indexinfo: {
              id,
            },
          };
          window.eventBus.$emit('highlightIndex', indexToHighlight);
        }
      });
      window.charts[id].addListener('rollOutGraph', (event) => {
        const id = event.graph.indexObject;
        if (id !== undefined && id !== that.main.id) {
          const indexToHighlight = {
            indexinfo: {
              id,
            },
          };
          window.eventBus.$emit('unhighlightIndex', indexToHighlight);
        }
      });
      window.charts[id].addListener('clickGraphItem', (event) => {
        const id = event.graph.indexObject;
        if (id !== undefined && id !== that.main.id) {
          const $buttons = $(`#row-${id}`).find('button');
          if ($buttons.length) $buttons[0].click();
        }
      });
    },
  },
  template: `
    <div><h4 v-if="isSubgraphy" class="print-border charttitle clearfix">{{title}}</h4><div v-show="hiddenPeriods[period]" :class="{ 'chart-big': !isSubgraphy }" class="chart chart-xy chart-not-available"></div><div v-show="!hiddenPeriods[period]"><div class="chartfilter"><div class="clearfix"><div class="btn-group col-xs-6 pull-right" :style="subgraphyPadding" role="group"><div class="btn-group" role="group"><button type="button" class="btn btn-blue dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">X Axis ({{labels[xAxis]}})<span class="caret"></span></button><ul class="dropdown-menu"><li v-for="(label,key) in labels"><a @click="xAxis=key">{{label}}</a></li></ul></div></div><div class="btn-group col-xs-6" role="group"><div class="btn-group" role="group"><button type="button" class="btn btn-blue dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Y Axis ({{labels[yAxis]}})<span class="caret"></span></button><ul class="dropdown-menu"><li v-for="(label,key) in labels"><a @click="yAxis=key">{{label}}</a></li></ul></div></div></div></div><div :class="{ 'chart-big': !isSubgraphy }" class="chart chart-xy" :id="id"></div></div><div v-show="dataLoading == true" class="abs-loader"><div class="loader-inner ball-pulse"><div></div><div></div><div></div></div></div></div>
  `,
});
