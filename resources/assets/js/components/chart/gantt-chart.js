/* global window document Vue moment */
Vue.component('chart-gantt-chart', {
  props: {
    title: {
      type: String,
      default: '',
    },
    idname: {
      type: String,
      default: 'no-id',
    },
    datasets: {
      type: Array,
      default: () => [],
    },
    timeType: {
      type: String,
      default: 'Daily',
    },
    maxDate: {
      type: String,
      default: '',
    },
    minDate: {
      type: String,
      default: '',
    },
  },
  data() {
    return {
      // trackData: {},
      // chart: null,
      highlightId: null,
    };
  },
  computed: {
    id() {
      return `gantt-chart-${this.idname}`;
    },
  },
  watch: {
    datasets() {
      this.loadChart();
    },
  },
  mounted() {
    this.loadChart();
  },
  beforeDestroy() {
    const { id } = this;
    if (window.charts[id]) {
      window.charts[id].clear();
      delete window.charts[id];
    }
    this.$off();
  },
  methods: {
    getPeriodType(count) {
      switch (this.timeType) {
        case 'Monthly':
          return 'month';
        case 'Weekly':
          if (count < 2) return 'week';
          return 'weeks';
        default:
          if (count < 2) return 'day';
          return 'days';
      }
    },
    getWeekdaysByType(startDate, endDate) {
      switch (this.timeType) {
        case 'Monthly':
          return 1;
        case 'Weekly':
          return this.calcBusinessWeeks(startDate, endDate);
        default:
          return this.calcBusinessDays(startDate, endDate);
      }
    },
    calcBusinessWeeks(startDate, endDate) {
      const day = moment(startDate);
      let businessDays = 0;
      while (day.isSameOrBefore(endDate, 'day')) {
        if (day.day() === 5) businessDays++;
        day.add(1, 'day');
      }
      return businessDays;
    },
    calcBusinessDays(startDate, endDate) {
      const day = moment(startDate);
      let businessDays = 0;

      while (day.isSameOrBefore(endDate, 'day')) {
        if (day.day() !== 0 && day.day() !== 6) businessDays++;
        day.add(1, 'day');
      }
      return businessDays;
    },
    calUsedDate() {
      const data = [];
      const activeMonth = {};
      const monthDate = {};

      this.datasets.forEach((date) => {
        const key = moment(date).format('YYYY MMM');
        if (!activeMonth[key]) activeMonth[key] = 0;
        if (this.timeType === 'Monthly' && activeMonth[key] >= 1) return null;
        activeMonth[key]++;
      });

      const startMonth = moment(this.minDate.substring(0, 7));
      const endMonth = moment(this.maxDate.substring(0, 7));
      const monthLength = endMonth.diff(startMonth, 'month') + 1;
      const currentMonth = startMonth;
      //get start date's used date;
      let currentKey = currentMonth.format('YYYY MMM');
      if (activeMonth[currentKey]) {
        data.push({
          start: 0,
          duration: activeMonth[currentKey],
          title: `${currentMonth.format('MMM YYYY')}: ${activeMonth[currentKey]} ${this.getPeriodType(activeMonth[currentKey])}`,
        });
      }
      let lastMonthCount = 0;
      lastMonthCount = this.getWeekdaysByType(this.minDate, startMonth.endOf('month').format('YYYY-MM-DD'));
      monthDate[currentKey] = lastMonthCount;
      if (activeMonth[currentKey] && activeMonth[currentKey] > lastMonthCount) lastMonthCount = activeMonth[currentKey];
      currentMonth.add(1, 'month');
      if (monthLength > 2) {
        for (let step = 0; step < monthLength - 2; step++) {
          currentKey = currentMonth.format('YYYY MMM');
          if (activeMonth[currentKey]) {
            data.push({
              start: lastMonthCount,
              duration: activeMonth[currentKey],
              title: `${currentMonth.format('MMM YYYY')}: ${activeMonth[currentKey]} ${this.getPeriodType(activeMonth[currentKey])}`,
            });
          }
          const periodsDate = this.getWeekdaysByType(currentMonth.startOf('month').format('YYYY-MM-DD'), currentMonth.endOf('month').format('YYYY-MM-DD'));
          lastMonthCount += periodsDate;
          monthDate[currentKey] = periodsDate;
          currentMonth.add(1, 'month');
        }
      }
      if (monthLength > 1) {
        currentKey = currentMonth.format('YYYY MMM');
        if (activeMonth[currentKey]) {
          data.push({
            start: lastMonthCount,
            duration: activeMonth[currentKey],
            title: `${currentMonth.format('MMM YYYY')}: ${activeMonth[currentKey]} ${this.getPeriodType(activeMonth[currentKey])}`,
          });
        }
        const periodsDate = this.getWeekdaysByType(currentMonth.startOf('month').format('YYYY-MM-DD'), this.maxDate);
        lastMonthCount += periodsDate;
        monthDate[currentKey] = periodsDate;
      }

      const guides = this.calcGuides(monthDate, lastMonthCount);
      return { data, totalCount: lastMonthCount, guides };
    },
    calcGuides(monthDate, totalCount) {
      const guides = [{
        value: this.getMomentData(totalCount, 0),
        lineColor: '#CCC',
        lineThickness: 1,
        lineAlpha: 1,
      }];
      const monthLength = Object.keys(monthDate).length;
      if (monthLength <= 6) {
        let currentMonth = 0;
        for (const month in monthDate) {
          guides.push({
            lineThickness: 1,
            lineAlpha: 0,
            value: this.getMomentData(currentMonth, monthDate[month]),
            label: this.getMonthLabel(month),
          });
          currentMonth += monthDate[month];
        }
      } else if (monthLength <= 30) {
        let currentMonth = 0;
        for (const month in monthDate) {
          const monthLabel = month.substring(month.length - 3);
          if (monthLabel === 'Jan' || monthLabel === 'Apr' || monthLabel === 'Jul' || monthLabel === 'Oct') {
            guides.push({
              lineThickness: 1,
              lineAlpha: 0,
              value: this.getMomentData(currentMonth, monthDate[month]),
              label: this.getMonthLabel(month),
            });
          }
          currentMonth += monthDate[month];
        }
      } else {
        let currentMonth = 0;
        let count = 0;
        for (const month in monthDate) {
          if (count % 12 === 0) {
            guides.push({
              lineThickness: 1,
              lineAlpha: 0,
              value: this.getMomentData(currentMonth, monthDate[month]),
              label: month.substring(0, 4),
            });
          }
          currentMonth += monthDate[month];
          count++;
        }
      }
      return guides;
    },
    getMomentData(current, added) {
      const value = current + added / 2;
      const momentDate = moment(this.minDate);
      const stringDate = momentDate.add(value, 'day').format('YYYY-MM-DD');
      return window.AmCharts.stringToDate(stringDate, 'YYYY-MM-DD');
    },
    getMonthLabel(month) {
      const monthLabel = month.substring(month.length - 3);
      if (monthLabel === 'Jan') return month.substring(0, 4);
      return monthLabel;
    },
    loadChart() {
      if (this.datasets.length === 0) return null;
      const { data, totalCount, guides } = this.calUsedDate();
      const dataProvider = [{
        category: '',
        segments: data,
      }];
      const graph = {
        fillAlphas: 1,
        lineAlpha: 1,
        lineColor: '#123B69',
        fillColors: '#123B69',
        balloonText: '[[title]]',
        animationPlayed: true,
        borderAlpha: 1,
        fixedColumnWidth: 40,
      };
      const { id } = this;
      if (window.charts[id]) {
        window.charts[id].graph = graph;
        window.charts[id].startDate = this.minDate;
        window.charts[id].dataProvider = dataProvider;
        window.charts[id].valueAxis.maximum = totalCount;
        window.charts[id].valueAxis.guides = guides;
        window.charts[id].validateData();
      } else {
        window.charts[id] = window.AmCharts.makeChart(this.id, {
          hideCredits: true,
          type: 'gantt',
          theme: 'light',
          marginBottom: 40,
          marginLeft: 50,
          marginTop: 0,
          fontFamily: 'Fira Sans',
          period: 'DD',
          dataDateFormat: 'YYYY-MM-DD',
          endField: 'end',
          startDate: this.minDate,
          startField: 'start',
          categoryField: 'category',
          segmentsField: 'segments',
          durationField: 'duration',
          zoomOutButtonRollOverAlpha: 0,
          zoomOutButtonImage: '',
          zoomOutText: '',
          dataProvider,
          graph,
          rotate: true,
          categoryAxis: {
            gridAlpha: 1,
            gridColor: '#CCC',
            axisAlpha: 1,
            axisColor: '#CCC',
            labelsEnabled: false,
          },
          valueAxis: {
            type: 'date',
            gridAlpha: 0,
            fontSize: 13,
            axisAlpha: 0,
            labelsEnabled: false,
            maximum: totalCount,
            minimum: 0,
            guides,
            // guides: [{
            //   value: this.getMomentData(totalCount, 0),
            //   lineColor: '#123B69',
            //   fillColor: '#123B69',
            //   lineThickness: 1,
            //   lineAlpha: 1,
            //   expand: true,
            //   label: 'end',
            // }],
          },
          balloon: {
            fixedPosition: true,
          },
          pathToImages: 'https://cdn.amcharts.com/lib/3/images/',
        });
      }
    },
  },
  template: '<div><h4 class="print-border charttitle chartgantttitle text-right">{{title}}</h4><div class="chart chart-gantt" :id="id" style="height:80px"></div></div>',
});
