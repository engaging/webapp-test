/* global window document analytics Vue $ _ */
const uuidv4 = require('uuid/v4');
const ACTION = require('../store/mutation-type');
const apiDashboard = require('../api/dashboard');

Vue.component('widgets', {
  props: ['user', 'widgetDataInitial', 'widgetLayoutInitial'],
  data() {
    return {
      draggable: true,
      resizeable: true,
      indicesAll: [],
      colorsAll: {},
      widgetData: [],
      widgetData_layout: [],
      trackLoaded: [],
      hasFeed: false,
      hasNews: false,
      volatilityLoading: [],
      strategiesIds: [],
      portfolioLoaded: false,
      strategiesLoaded: false,
    };
  },
  computed: {
    benchmarks() {
      const results = {};
      const { strategies } = this.$store.state;
      for (const idx in strategies) {
        if (strategies[idx].type === 'Benchmark') {
          results[idx] = strategies[idx];
        }
      }
      return results;
    },
    portfolios() {
      return this.$store.state.portfolios;
    },
    strategies() {
      return this.$store.state.strategies;
    },
  },
  methods: {
    widgetUpdatedHandler(e) {
      if (e.method === 'removeWidget') {
        this.removeWidget(e.idx);
        this.saveAll();
      }
    },
    itemMovedHandler(e) {
      switch (e.e) {
        case 'dragend':
          this.saveAll();
          break;
      }
    },
    itemResizedHandler(e) {
      switch (e.e) {
        case 'resizeend':
          for (let j = 0; j < this.widgetData_layout.length; j++) {
            if (this.widgetData_layout[j].i == e.details[0].toString()) {
              //x, y, w, h
              this.widgetData_layout[j].x = e.details[1];
              this.widgetData_layout[j].y = e.details[2];
              this.widgetData_layout[j].w = e.details[3];
              this.widgetData_layout[j].h = e.details[4];
            }
          }
          this.saveAll();
          break;
      }
    },
    widgetModal(type) {
      window.eventBus.$emit('showWidgetModal', type);
    },

    toggleWidget(type) {
      // only for news and feed
      if (this.hasNews && type === 'news' || this.hasFeed && type === 'feed') {
        console.log(`turn off ${type}`);
        const del = this.widgetData.findIndex(o => o.componenttype === type);
        this.removeWidget(del);
        if (type === 'feed') {
          this.hasFeed = false;
        } else {
          this.hasNews = false;
        }
      } else {
        console.log(`turn on ${type}`);
        let newdata = null; //toggle on
        if (type === 'news') {
          newdata = {
            title: 'News',
            size: 4,
            componenttype: 'news',
            showMenu: false,
            h: 2,
            w: 2,
          };
          this.hasNews = true;
        } else {
          newdata = {
            title: 'Feed',
            size: 4,
            componenttype: 'feed',
            showMenu: false,
            h: 2,
            w: 2,

          };
          this.hasFeed = true;
        }
        this.widgetData.push(newdata);
        this.getPositions();
        this.scrollBottom();
        analytics.track('Added Widget to Dashboard', { 'Widget Type': _.startCase(type) });
      }
      this.saveAll();
    },

    scrollBottom() {
      try {
        $('html, body').animate({ scrollTop: $(document).height() }, 'slow');
      } catch (e) {
        console.error(e);
      }
    },
    async volatilityHandler({ volatilityPeriod_small, period, idx }) {
      // check whether need use api to call.
      const fields = `${period}_volatility_track_${volatilityPeriod_small}`;

      // search for already data.
      Vue.set(this.volatilityLoading, idx, true);
      const strategiesParamIds = [];
      const portfoliosParamIds = [];
      const { datasets } = this.widgetData[idx].properties;
      datasets.forEach((o) => {
        if (o.type === 'portfolio' && !this.portfolios[o.id][fields]) {
          portfoliosParamIds.push(o.id);
        }
        if (o.type !== 'portfolio' && !this.strategies[o.id][fields]) {
          strategiesParamIds.push(o.id);
        }
      });
      const vuexActions = [];
      if (portfoliosParamIds.length > 0) {
        vuexActions.push(this.$store.dispatch(ACTION.API_FETCH_PORTFOLIO_VOLATILITY, { ids: portfoliosParamIds, payload: { period, measure: volatilityPeriod_small } }));
      }
      if (strategiesParamIds.length > 0) {
        vuexActions.push(this.$store.dispatch(ACTION.API_FETCH_STRATEGY_VOLATILITY, { ids: strategiesParamIds, payload: { period, measure: volatilityPeriod_small } }));
      }
      await Promise.all(vuexActions);
      const that = this;
      setTimeout(() => {
        Vue.set(that.volatilityLoading, idx, false);
      }, 20);
    },
    editWidgetHandler({ editWidget, idx }) {
      if (editWidget.componenttype === 'track' || editWidget.componenttype === 'volatilitytrack') {
        this.callTrackApis(editWidget, idx, 'track');
      }
      if (editWidget.componenttype === 'xy') {
        Vue.set(this.trackLoaded, idx, false);
        const that = this;
        setTimeout(() => {
          Vue.set(that.trackLoaded, idx, true);
        }, 20);
      }
      Vue.set(this.widgetData, idx, editWidget);
      // this.widgetData[idx] = editWidget;
      this.saveAll();
    },
    addWidgetHandler(editWidget) {
      const type = editWidget.componenttype;
      if (editWidget.componenttype === 'track' || editWidget.componenttype === 'volatilitytrack') {
        this.callTrackApis(editWidget, null, 'track');
      } else {
        this.trackLoaded.push(true);
      }
      this.volatilityLoading.push(false);
      this.widgetData.push(editWidget);
      this.getPositions();
      this.saveAll();
      this.scrollBottom();
      analytics.track('Added Widget to Dashboard', { 'Widget Type': _.startCase(type) });
    },
    async callTrackApis(editWidget, index, field) {
      const portfolioIds = [];
      const strategyIds = [];
      if (index !== null) {
        Vue.set(this.trackLoaded, index, false);
      } else {
        this.trackLoaded.push(false);
      }
      const keyField = `6m_${field}`;
      editWidget.properties.datasets.forEach((data) => {
        if (data.type === 'portfolio' && this.portfolios[data.id]['6m_active'] === 1 && !this.portfolios[data.id][keyField]) {
          portfolioIds.push(data.id);
        }
        if (data.type !== 'portfolio' && this.strategies[data.id]['6m_active'] === 1 && !this.strategies[data.id][keyField]) {
          strategyIds.push(data.id);
        }
      });
      const vuexActions = [];
      if (portfolioIds.length > 0) {
        vuexActions.push(this.$store.dispatch(ACTION.API_FETCH_PORTFOLIO_DETAILS, {
          ids: portfolioIds,
        }));
      }
      if (strategyIds.length > 0) {
        vuexActions.push(this.$store.dispatch(ACTION.API_FETCH_STRATEGY_DETAILS, {
          ids: strategyIds,
        }));
      }
      await Promise.all(vuexActions);
      const that = this;
      setTimeout(() => {
        if (index !== null) {
          Vue.set(that.trackLoaded, index, true);
        } else {
          Vue.set(that.trackLoaded, that.trackLoaded.length - 1, true);
        }
      }, 20);
    },
    removeWidget(index) {
      const type = this.widgetData[index].componenttype;
      if (type === 'news') {
        this.hasNews = false;
      }
      if (type === 'feed') {
        this.hasFeed = false;
      }
      this.trackLoaded.splice(index, 1);
      this.volatilityLoading.splice(index, 1);
      this.widgetData.splice(index, 1);
      this.widgetData_layout.splice(index, 1);
    },

    saveAll() {
      const data = []; // order widgetdata by layout order? or save layout x and y?
      for (let i = 0; i < this.widgetData_layout.length; i++) {
        const block = this.widgetData_layout[i];
        const position = `${block.x},${block.y}`;
        if (this.widgetData[i]) {
          const properties = Object.assign({}, this.widgetData[i], block);
          properties.position = position;
          if (properties.showMenu !== undefined) properties.showMenu = false; // close all menu;
          data.push(properties);
        } else {
          console.error('missing data for widget', i, 'in', this.widgetData.slice());
        }
      }
      apiDashboard.save({ data, mode: 'saveall' }).catch(e => console.error(e));
    },

    sortTable(block, field) {
      if (block.properties.sortBy === field) {
        // flip the order
        if (block.properties.sortDirection === 'asc') {
          block.properties.sortDirection = 'desc';
        } else {
          block.properties.sortDirection = 'asc';
        }
      }

      block.properties.sortBy = field;
    },

    async getColors() {
      const fetechData = await this.$store.dispatch(ACTION.API_FETCH_COLORS, {});
      this.colorsAll = fetechData.benchmarks;
    },
    async loadPortfolios() {
      const payload = {
        metrics: true,
      };
      await this.$store.dispatch(ACTION.API_FETCH_PORTFOLIO, {
        payload,
      });
      this.portfolioLoaded = true;
    },
    async loadStrategies() {
      const payload = {
        ids: this.strategiesIds,
        types: ['Strategy', 'Benchmark', 'Private Strategy'],
        metrics: true,
      };
      await this.$store.dispatch(ACTION.API_FETCH_STRATEGY, { payload });
      this.strategiesLoaded = true;
    },
    async loadFilters() {
      const payload = {
        types: ['Strategy', 'Benchmark', 'Private Strategy'],
      };
      await this.$store.dispatch(ACTION.API_FETCH_FILTER, payload);
    },
    setInitDate() {
      this.widgetData = this.widgetDataInitial;
      for (let i = this.widgetData.length - 1; i >= 0; i--) {
        const w = this.widgetData[i];
        if (w.componenttype === 'news') {
          this.hasNews = true;
        }
        if (w.componenttype === 'feed') {
          this.hasFeed = true;
        }
      }
    },
    async initData() {
      const portfolioIds = {};
      const strategyIds = {};
      for (let i = 0; i < this.widgetData.length; i++) {
        this.volatilityLoading.push(false);
        switch (this.widgetData[i].componenttype) {
          case 'track':
          case 'volatilitytrack':
            this.widgetData[i].properties.datasets.forEach((data, index) => {
              console.log('start', index);
              if (data.type === 'portfolio') {
                portfolioIds[data.id] = true;
              } else {
                strategyIds[data.id] = true;
              }
            });
            this.trackLoaded.push(false);
            break;
          case 'stat':
            if (this.widgetData[i].properties.main && this.widgetData[i].properties.main.type !== 'portolio') {
              this.strategiesIds.push(this.widgetData[i].properties.main.id);
            }
            this.trackLoaded.push(true);
            break;
          case 'bubble':
          case 'default':
          case 'news':
          case 'feed':
            this.trackLoaded.push(true);
            break;
          default:
            this.widgetData[i].properties.datasets.forEach((data) => {
              if (data.type !== 'portfolio') {
                this.strategiesIds.push(data.id);
              }
            });
            this.trackLoaded.push(true);
            break;
        }
      }
      const portfolioKeys = Object.keys(portfolioIds);
      const strategyKeys = Object.keys(strategyIds);
      const vuexActions = [];
      if (this.strategiesIds.length > 0) {
        this.loadStrategies();
      } else {
        this.strategiesLoaded = true;
      }
      if (strategyKeys.length > 0) {
        vuexActions.push(this.$store.dispatch(ACTION.API_FETCH_STRATEGY_DETAILS, {
          ids: strategyKeys,
        }));
      }
      if (portfolioKeys.length > 0) {
        vuexActions.push(this.$store.dispatch(ACTION.API_FETCH_PORTFOLIO_DETAILS, {
          ids: portfolioKeys,
        }));
      }
      await Promise.settleAll(vuexActions);
      for (let i = 0; i < this.trackLoaded.length; i++) {
        if (!this.trackLoaded[i]) Vue.set(this.trackLoaded, i, true);
      }
    },
    getPositions() {
      console.log('get positions');
      let lastx = 0;
      let lasty = 0;
      let previousx = 0;
      let previousy = 0;
      let currentx = 0;
      let currenty = 0;
      const maxcol = 4;
      const widgetData_layout = [];
      let rowHeightMax = 0;
      let bottom = {
        x: 0, y: 0, w: 0, h: 0,
      };

      for (let i = 0; i < this.widgetData.length; i++) {
        const itemLayout = this.widgetData_layout[i];
        const item = this.widgetData[i];
        if (itemLayout) {
          previousx = currentx;
          previousy = currenty;
          if (itemLayout.y > bottom.y) {
            bottom = Object.assign({}, itemLayout);
          }
          if (itemLayout.x + itemLayout.w <= maxcol) {
            currentx = itemLayout.x + itemLayout.w;
          } else {
            currentx = 0;
          }
          currenty = itemLayout.y;
        } else {
          const line = {};
          if (!item.w && !item.h) {
            if (item.size === 4) { //w2 h2
              line.w = 2;
              line.h = 2;
            } else if (item.size === 2) { // w2 h1
              line.w = 2;
              line.h = 1;
            } else { //default size w1 h1
              line.w = 1;
              line.h = 1;
            }
          } else {
            line.w = item.w;
            line.h = item.h;
          }

          if (item.componenttype === 'bubble') { // fixed size for this one
            line.w = 4;
            line.h = 3;
          }

          if (!line.uniqueKey) {
            line.uniqueKey = uuidv4();
          }

          if (rowHeightMax < line.h) {
            rowHeightMax = line.h;
          }

          // Auto calc the positions based on their sizes
          let x = 0;
          if (currenty > previousy || currentx + line.w <= maxcol) {
            x = (currentx + line.w <= maxcol) ? currentx : 0;
          } else if (currenty < previousy) {
            x = (previousx + line.w <= maxcol) ? previousx : 0;
          } else if (currentx > 2 && bottom.x < 2 && currenty + line.h < bottom.y + bottom.h) {
            x = 2;
          }

          let y = currenty; // (x === 0 && i > 0) ? currenty : 0;
          if (y === bottom.y - 1 && x === bottom.x) {
            x += bottom.w;
            if (x + line.w > maxcol) {
              x = bottom.x;
              y = bottom.y + bottom.h;
            }
          }

          if (this.widgetData_layout.length > i) { // actual xy
            x = this.widgetData_layout[i].x;
            y = this.widgetData_layout[i].y;
          } else if (this.widgetLayoutInitial.length > i) { // saved xy
            const coord = this.widgetLayoutInitial[i].split(',');
            x = parseInt(coord[0], 10);
            y = parseInt(coord[1], 10);
          }

          line.x = x;
          line.y = y;
          currentx = x + line.w;
          line.i = i.toString();
          line.type = item.componenttype;

          //widgetData_layout.push(line);
          Vue.set(this.widgetData_layout, i, line);

          if (line.x + line.w === maxcol) {
            currenty += rowHeightMax;
            rowHeightMax = 0;
            //new row.
          }

          lastx = (line.x + line.w + 2) <= maxcol ? line.x + line.w : 0;
          lasty = lastx === 0 ? line.y + line.h : line.y + line.h + 1;
        }
      }
    },
  },
  created() {
    this.setInitDate();
    this.getColors();
    this.loadFilters();
    this.initData();
    this.loadPortfolios();
  },
  mounted() {
    const that = this;
    window.eventBus.$on('widgetUpdated', this.widgetUpdatedHandler);
    window.eventBus.$on('item-moved', this.itemMovedHandler);
    window.eventBus.$on('item-resized', this.itemResizedHandler);
    window.eventBus.$on('addNewWidget', this.addWidgetHandler);
    window.eventBus.$on('editWidgetSave', this.editWidgetHandler);
    window.eventBus.$on('volatility.api', this.volatilityHandler);

    this.getPositions(); // do this after the first ajax/data set is gotten

    $('body').on('click', (e) => {
      if ($('.opened').length) {
        const target = e.toElement || e.target;
        if (!$(target).parents('.opened').length) {
          const widget = that.widgetData[$('.opened').data('var')];
          if (widget && widget.showMenu) {
            widget.showMenu = false;
          }
          if (widget && widget.properties && widget.properties.showColMenu) {
            widget.properties.showColMenu = false;
          }
        }
      }
    });
  },
});
