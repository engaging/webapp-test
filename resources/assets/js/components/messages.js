Vue.component('messages', {
  props: ['user', 'thread', 'notification'],
  data() {
    return {
      showSidebar: 'all', //all, notices, messages,
      thread_messages: [],
    };
  },
  methods: {
    initRichtext() {
      $('.richtext-editor#message-editor').wysiwyg();
    },
    submitMessage() {
      // copy richtext in rawhtml form
      const msg = $('.richtext-editor#message-editor').html();
      $('#message-input').val(msg);
      if (msg != '') {
        $('#message-new-form').submit();
      } else {
        swal('Whoops!', 'Please enter a message.', 'error');
      }
    },
  },

  mounted() {
    if (window.thread_messages) {
      const msgs = window.thread_messages;

      for (let i = 0; i < msgs.length; i++) {
        const m = msgs[i];
        m.body = m.body.replace(/\n/g, '<br>'); //nl2br
        m.closed = i != msgs.length - 1; //only show full text of newest msg
      }
      this.thread_messages = msgs;
    }

    // indicate active in sidebar
    if (this.notification != undefined) {
      $(`.sidebar .notification-${this.notification}`).addClass('active');
    } else if (this.thread != undefined) {
      $(`.sidebar .thread-${this.thread}`).addClass('active');
    }

    // fixed size sticky side
    $(window).resize(() => {
      const windowheight = $(window).height();
      $('.sidebar').height(windowheight - 120);
      $('.sidebar .scroll').height($('.sidebar').height() - 170);
    });
    $(window).trigger('resize');

    $(window).on('scroll', () => {
      const top = $(window).scrollTop();
      if (top > $('.navbar').height()) {
        $('.sidebar').width($('.sidebar').width()).addClass('fixed');
      } else {
        $('.sidebar').removeClass('fixed');
      }
    });
  },
});

