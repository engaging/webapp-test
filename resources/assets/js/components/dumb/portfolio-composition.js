/* global Vue */
Vue.component('portfolio-composition', {
  props: ['activeStrategies', 'allocationModel', 'allocationLabels', 'allocationOptions', 'pipeClass'],
  template: `
    <span>
    {{ activeStrategies }} Active Strategies
    <span v-bind:class="pipeClass">|</span> {{ allocationLabels[allocationModel] }}
    <span style="margin: 0;" v-if="allocationModel !== 'custom' && allocationOptions.leverage_type === 'Fixed Leverage'">
      <span v-bind:class="pipeClass">|</span> LV {{ allocationOptions.fixed_leverage}}%
    </span>
    <span style="margin: 0;" v-if="allocationModel !== 'custom' && allocationOptions.leverage_type === 'Target Volatility'">
      <span v-bind:class="pipeClass"">|</span> VT {{ allocationOptions.target_volatility}}%
    </span>
    <span style="margin: 0;" v-if="['equal','manual', 'custom'].indexOf(allocationModel) < 0 && ((allocationOptions.primary_allocation_constraint && allocationOptions.primary_allocation_constraint !== 'None') || (allocationModel !== 'custom' && allocationOptions.secondary_allocation_constraint && allocationOptions.secondary_allocation_constraint !== 'None'))">
      <span v-bind:class="pipeClass"">|</span> Constraints
    </span>
    </span>
  `,
});
