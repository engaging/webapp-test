/* global window document Vue AmCharts $ _ */
const ACTION = require('../store/mutation-type');
const { assetManagers, metrics } = require('../util/config');

if (window.AmCharts) {
  AmCharts.baseHref = true;
}

const getDefaultData = () => (
  {
    strategy: '', // remove vue warnings
    countActiveByGroup: {},
    groupedIndicesSelected: {},
    groupsDisplay: [],
    period: '5 Year',
    periodKeys: {
      '6 Month': 'data_6m',
      '1 Year': 'data_1y',
      '3 Year': 'data_3y',
      '5 Year': 'data_5y',
      '10 Year': 'data_10y',
    },
    periodKeysForFilters: {
      '10 Year': '10y',
      '5 Year': '5y',
      '3 Year': '3y',
      '1 Year': '1y',
      '6 Month': '6m',
    },
    chartloaded: false,
    view: 'xy',
    views: [
      ['trackrecord', 'Track Record'],
      ['xy', 'X Y Graph'],
    ],
    isInvalid: false,
    dataTableColsCount: 6,
    dataTableCols_raw: [
      'return',
      'volatility',
      'sharpe',
      'sortino',
      'mdd',
    ],
    dataTableCols: _.pick(metrics, ['return', 'volatility', 'sharpe', 'sortino', 'mdd']), // columns showing, ordered to menu appearance
    dotColors: {
      strategy: '#123B69',
      benchmark: '#F7B918',
      'private strategy': '#29B999',
    },
    componentAll: [],
    pinnedColorsAll: {
      color1: '#4a7c59',
      color2: '#68b0ab',
      color3: '#35a03a',
      color4: '#53ad8a',
      color5: '#7aa559',
      color6: '#9c89b8',
      color7: '#705695',
      color8: '#573b8c',
      color9: '#866487',
      color10: '#723f62',
      color11: '#dddddd',
      color12: '#cccccc',
    },
  }
);

Vue.component('sponsorportfolio', {
  props: ['user', 'provider', 'type'],
  mounted() {
    const that = this;
    $('#maxDate').text(`As of ${assetManagers[this.provider.name].maxDate}`);
    window.eventBus.$on('highlightIndex', this.highlightIndex);
    window.eventBus.$on('unhighlightIndex', this.unhighlightIndex);
    $(window).resize(() => {
      that.setScrollPanels();
    });
  },
  data() {
    return getDefaultData();
  },
  created() {
    this.createCssColorRules();
    this.loadOverallPortfolioStats();
    this.setScrollPanels();
  },
  watch: {
    // view() {
    //   this.initCharts();
    //   this.setScrollPanels();
    // },
    chartloaded(val) {
      if (val) {
        const componet = [this.portfolio];
        for (const idx in this.amStrategies) {
          componet.push(this.amStrategies[idx]);
        }
        this.componentAll = componet;
      }
    },
  },
  computed: {
    portfolio() {
      const portfolioType = assetManagers[this.provider.name].type[0];
      for (const idx in this.$store.state.strategies) {
        if (this.$store.state.strategies[idx].type === portfolioType) {
          return this.$store.state.strategies[idx];
        }
      }
      return {};
    },
    amStrategies() {
      const results = {};
      const portfolioType = assetManagers[this.provider.name].type[1];
      for (const idx in this.$store.state.strategies) {
        if (this.$store.state.strategies[idx].type === portfolioType) {
          results[idx] = this.$store.state.strategies[idx];
        }
      }
      return results;
    },
  },
  methods: {
    createCssColorRules() {
      //highlight-benchmark
      const style = document.createElement('style');
      style.type = 'text/css';
      style.innerHTML = `.highlight-benchmark { color: ${this.dotColors.benchmark}; }`;
      style.innerHTML += '\n\r';
      style.innerHTML += `.highlight-private_track { color: ${this.dotColors['private strategy']}; }`;
      document.getElementsByTagName('head')[0].appendChild(style);
    },
    async loadOverallPortfolioStats() {
      const ids = this.provider.ownStrategyIds;
      const { maxDate } = assetManagers[this.provider.name];
      await this.$store.dispatch(ACTION.API_FETCH_STRATEGY_DETAILS, {
        ids,
        payload: {
          maxDate,
        },
      });
      this.chartloaded = true;
    },
    getStrategyType(index) {
      if (index.type === 'Private Strategy') return 'Private Track';
      return $.trim(index.type) || 'Strategy';
    },
    getStrategyTypeForCss(index) {
      let type = this.getStrategyType(index);
      type = type.replace(' ', '_');
      return type;
    },
    setScrollPanels() {
      const that = this;
      if ($('.bottom-portfolio').length) {
        const heightLeft = 550;
        $('.bottom-portfolio .sidebar-filters').height(heightLeft);
      } else {
        setTimeout(() => {
          that.setScrollPanels();
        }, 100);
      }
    },
    setPeriod(period) {
      //console.log(this.portfolio[period + "_active"]);
      const k = `${this.periodKeysForFilters[period]}_active`;
      if (this.portfolio[k] === 0) {
        this.setInvalidPeriod(period);
      } else {
        this.isInvalid = false;
        this.period = period;
      }
    },
    setInvalidPeriod(period) {
      this.period = period;
      this.isInvalid = true;
    },
    setView(view) {
      this.view = view;
      this.sortby = 'shortName';
    },
    highlightIndex(index, fromDom) {
      if (!index.indexinfo) {
        index.indexinfo = index;
      }
      $(`#row-${index.indexinfo.id}`).addClass('pinned-row');
      if (!fromDom) {
        try {
          const data = $(`#row-${index.indexinfo.id}`).length;
          if (data > 0) {
            const top = $(`#row-${index.indexinfo.id}`).parent()[0].offsetTop;
            $('.bottom-portfolio .sidebar-filters').scrollTop(top);
          }
        } catch (e) {
          console.error(e);
        }
      }
      if (index.suspended === 1) {
        $('#hover-correlation').text(`${index.indexinfo.shortName} is suspended.`);
      }
      const idTarget = `g${index.indexinfo.id}`;

      if (this.view === 'trackrecord') {
        $(`.amcharts-graph-${idTarget} .amcharts-graph-stroke`).addClass('highlight-chart');
        $(`.amcharts-graph-${idTarget} .amcharts-graph-stroke`).parent('g').parent('g').appendTo('#chartCtx-wrapper svg');
        $(`.amcharts-graph-${idTarget} .amcharts-graph-stroke:gt(0)`).remove();
        //$(".amcharts-graph-" + idTarget + " .amcharts-graph-stroke:lt(1)").remove();
      }
      if (this.view === 'xy') {
        $(`.amcharts-graph-line.amcharts-graph-${idTarget} .amcharts-graph-bullet`).addClass('highlight-dot');
      }
    },
    unhighlightIndex(index) {
      $(`#row-${index.indexinfo.id}`).removeClass('pinned-row');
      const idTarget = `g${index.indexinfo.id}`;
      if (this.view === 'trackrecord') {
        $(`.amcharts-graph-${idTarget} .amcharts-graph-stroke`).removeClass('highlight-chart');
      }
      if (this.view === 'xy') {
        $(`.amcharts-graph-line.amcharts-graph-${idTarget} .amcharts-graph-bullet`).removeClass('highlight-dot');
      }
    },
  },
});
