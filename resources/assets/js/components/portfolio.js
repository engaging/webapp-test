/* global window document analytics moment Vue AmCharts $ _ */
import DatePicker from './vue2-datepicker';

const ACTION = require('../store/mutation-type');
const { checksInsensitive } = require('../lib/lodash-helper');
const { PERIOD, getPeriodKeys } = require('../util/period');
const {
  basePrefs,
  fundings,
  metrics,
  models,
} = require('../util/config');

if (window.AmCharts) {
  AmCharts.baseHref = true;
}

const getDynamicData = (payload) => {
  const { options = {}, periods = {}, maxDate } = payload;
  const ascPeriodKeys = getPeriodKeys(periods);
  const ascPeriodKeysWithCust = Object.assign({ Cust: PERIOD.CUST }, ascPeriodKeys);
  const descPeriodKeys = getPeriodKeys(periods, 'desc');
  const descKeys = Object.keys(descPeriodKeys);
  let { period } = payload;
  if (!period) {
    period = options.period ? _.invert(ascPeriodKeys)[options.period] : '5 Year';
  }

  return {
    tableCols_performance: descPeriodKeys,
    tableCols_performance_show: descKeys,
    tableCols_performance_show_ordered: descKeys,
    period,
    periodKeysForFilters: ascPeriodKeysWithCust,
    periodDates: {
      from: periods[ascPeriodKeys[period]],
      to: maxDate,
    },
  };
};

const getDefaultData = payload => ({
  ...getDynamicData(payload),
  strategy: '', //remove vue warnings
  strategyDetails: null, //remove vue warnings
  loadingPortfolio: true,
  loadingPie: true,
  loadingCharts: true,
  loadingExport: false,
  benchmarksLeft: 0,
  indicesSuspended: [],
  weighting: 'equal_weighting',
  weightingOptions: models,

  stats: [],
  statsForIndices: {},
  riskFreeMonthly: 2,

  tableCols_benchmarks_show: [
    'return',
    'volatility',
    'sharpe',
    'skewness',
    'var',
    'mdd',
  ],
  tableCols_benchmarksMax: 6,
  tableCols_benchmarksCount: 6,
  tableCols_benchmarks_sortBy: '',
  tableCols_benchmarks_sortOrder: -1,

  showBenchmarkMenu1: -1, /*mini list of columns for benchmarks table*/
  showBenchmarkMenu2: -1, /*list of benchmarks*/
  // this is the array to save in the db
  benchmarksActive: {
    portfolios: [], //{id:id,color:color} from the portfolio table
    benchmarks: [], //ids from the indices table
  },
  benchmarkColors: [
    'color1',
    'color2',
    'color3',
    'color4',
    'color5',
    'color6',
    'color7',
    'color8',
    'color9',
    'color10',
    'color11',
    'color12',
  ],
  colorsAll: {
    color1: '#4a7c59',
    color2: '#68b0ab',
    color3: '#35a03a',
    color4: '#53ad8a',
    color5: '#7aa559',
    color6: '#9c89b8',
    color7: '#705695',
    color8: '#573b8c',
    color9: '#866487',
    color10: '#723f62',
    color11: '#dddddd',
    color12: '#cccccc',
  },
  // this is populated when we know benchmarksactive
  benchmarksActiveData: [],
  benchmarksActiveMax: 4,

  labels: metrics,
  tableRows_performance_show: [
    'return',
    'volatility',
    'sharpe',
    'skewness',
    'var',
    'mdd',
    'worst20d',
    'posWeeks',
  ],
  tableRows_performanceMax: 12,
  showPerformanceMenu: -1,
  tableCols_performanceCount: 5,

  // noticed that assetClass and Currency are already under the title of the row
  // still has col for sorting the weight field is adjusted based on portfolio's weighting
  tableCols_composition: {
    Name: 'shortName',
    'Asset Class': 'assetClass',
    Factor: 'factor',
    Code: 'codeType',
    'Return Type': 'returnType',
    Style: 'style',
    Region: 'region',
    Weight: 'weighting',
    Allocation: 'allocation',
  },
  tableCols_composition_show: [
    'Name', 'Asset Class', 'Factor', 'Code', 'Weight', 'Allocation',
  ],
  tableCols_composition_show_ordered: [
    'Name', 'Asset Class', 'Factor', 'Code', 'Weight', 'Allocation',
  ],
  tableCols_composition_show_always: [
    'Weight', 'Allocation',
  ],
  tableCols_composition_sortBy: 'indexinfo.shortName',
  tableCols_composition_sortOrder: 1,
  showCompositionMenu: -1,
  tableCols_compositionMax: 6,
  tableCols_compositionCount: 6,
  tableCols_compositionHasName: true,

  customDates: {
    from: null,
    to: null,
  },
  datePickerDates: [],
  stickyheader: false,
  assetClassColors: {},
  categoryColors: {},
  widgetConfig: [
    'chart-radar',
    'chart-drawdowntrack',
    'chart-volatilitytrack',
    'chart-correlationscatter',
    'chart-returnbar',
    'chart-weighting',
    'chart-leverage',
    'chart-returnscatter',
    'chart-varcontribution',
    'chart-returncontribution',
    'chart-volatilitycontribution',
  ],
  widgetsAvailable: {
    'chart-weighting': 'Weighting',
    'chart-leverage': 'Leverage',
    'chart-radar': 'Metrics',
    'chart-drawdowntrack': 'Drawdown',
    'chart-volatilitytrack': 'Volatility',
    'chart-correlationscatter': 'Correlation Versus Benchmarks',
    'chart-returnbar': 'Monthly Return Distribution',
    'chart-returnscatter': 'Monthly Return Regression',
    'chart-varcontribution': 'VaR Contribution',
    'chart-returncontribution': 'Return Contribution',
    'chart-volatilitycontribution': 'Volatility Contribution',
  },
  allocationKeys1: { // ['Label', 'unit (opt.)', default value (opt.)]
  },
  allocationOptions: {},
  filteredPortfolios: {},
  changeCheckBox: false,
  loadingQueue: {
    benchmarks: {},
    portfolios: {},
  },
  loadingVolatitltyQueue: {
    '30d': {
      benchmarks: {},
      portfolios: {},
    },
    '60d': {
      benchmarks: {},
      portfolios: {},
    },
    '120d': {
      benchmarks: {},
      portfolios: {},
    },
  },
  volatilityHasDays: {
    '30d': [],
    '60d': [],
    '120d': [],
  },
  volatilityLoadingList: {
    '30d': false,
    '60d': false,
    '120d': false,
  },
  volatilityDay: '30d',
  benchmarksLoaded: false,
  funding: null,
});

Vue.component('portfolio', {
  props: ['user', 'portfolio', 'options', 'periods', 'maxDate'],
  components: { DatePicker },
  mounted() {
    const that = this;
    window.eventBus.$on('volatility.api', this.volatilityHandler);
    window.eventBus.$on('volatility.change', this.changeButtonHandler);
    window.addEventListener('scroll', () => {
      const navheight = 100;
      const scroll = $(window).scrollTop();
      if (scroll >= navheight) {
        that.stickyheader = true;
      } else {
        that.stickyheader = false;
      }
    });

    $('body').on('click', (e) => {
      if ($('.opened').length) {
        const target = e.toElement || e.target;
        if ($(target).parents('.opened').length == 0) {
          that[$('.opened').data('var')] = -1;
          that.showPerformanceMenu = -1;
        }
      }
    });
    try {
      analytics.track('Viewed Portfolio Factsheet', {
        'Portfolio Id': this.portfolio.id,
        'Portfolio Title': this.portfolio.title,
        'Portfolio Slug': this.portfolio.slug,
        'Portfolio Strategies Count': this.portfolio.strategiesCount || 0,
        'Portfolio Model': this.weightingOptions[this.portfolio.weighting],
        'Portfolio Created At': this.portfolio.created_at.split(' ').join('T'),
        'Portfolio Updated At': this.portfolio.updated_at.split(' ').join('T'),
      });
    } catch (e) {
      console.error(e);
    }
  },

  data() {
    return getDefaultData(this);
  },

  created() {
    this.$store.commit(ACTION.API_FETCH_PORTFOLIO, {
      fetchedData: [this.portfolio],
    });
    this.setOrginalPeriod();
    this.setAllocation();
    this.getColors();
    this.setBenchmarkActiveData();
    this.loadIndices();
    this.loadBenchmarks();
    this.loadPortfolios();
  },

  watch: {
    period() {
      if (this.period === 'Cust') {
        if (this.customDates.from) this.periodDates.from = this.customDates.from;
        if (this.customDates.to) this.periodDates.to = this.customDates.to;
        else this.showDatePicker();
      } else {
        this.periodDates.from = this.periods[this.periodKeysForFilters[this.period]];
        this.periodDates.to = this.maxDate;
        this.loadBenchmarkData.run();
      }
    },
    tableCols_benchmarks_show() {
      this.tableCols_benchmarksCount = this.tableCols_benchmarks_show.length;
    },

    tableCols_performance_show() {
      const res = [];
      const that = this;
      Object.keys(this.tableCols_performance).forEach((key) => {
        if (that.tableCols_performance_show.indexOf(key) >= 0) { res.push(key); }
      });
      this.tableCols_performance_show_ordered = res;
      this.tableCols_performanceCount = this.tableCols_performance_show.length;
    },

    tableCols_composition_show() {
      const res = [];
      const that = this;
      Object.keys(this.tableCols_composition).forEach((key) => {
        if (that.tableCols_composition_show.indexOf(key) >= 0) { res.push(key); }
      });
      this.tableCols_composition_show_ordered = res;
      this.tableCols_compositionCount = this.tableCols_composition_show.length;
      this.tableCols_compositionHasName = this.tableCols_composition_show.includes('Name');
    },
    benchmarks(newBenchmarks) {
      if (_.some(this.benchmarksActiveData, o => o !== newBenchmarks[o.id])) {
        this.resetBenchmarkActiveData();
      }
    },
    portfolios(newPortfolios) {
      this.filteredPortfolios = {};
      for (const key in newPortfolios) {
        if (newPortfolios[key].activeStrategiesCount > 0 && newPortfolios[key].id !== this.portfolio.id) {
          this.filteredPortfolios[newPortfolios[key].id] = newPortfolios[key];
        }
      }
      this.resetBenchmarkActiveData();
    },

    'benchmarksActive.portfolios': function () {
      this.showTracksLoading()
      this.loadBenchmarkData.run();
    },
    'benchmarksActive.benchmarks': function () {
      this.showTracksLoading();
      this.loadBenchmarkData.run();
    },
  },
  tasks: t => ({
    loadBenchmarkData: t(function* loadBenchmarkData() {
      const period = this.periodKeysForFilters[this.period];
      const fields = `${period}_active`;
      const payload = {
        volMeasure: this.volatilityDay,
      };

      const { benchmarksParamIds, portfoliosParamIds } = this.needCallApiId({ key: fields, type: 'all' });

      if (benchmarksParamIds.length === 0 && portfoliosParamIds.length === 0) {
        this.resetBenchmarkActiveData();
        this.volatilityLoadingList[payload.volMeasure] = false;
      } else {
        this.callApisForData({
          benchmarksParamIds, portfoliosParamIds, type: 'all', payload,
        });
      }

      for (const volatilityPeriod_small in this.volatilityHasDays) {
        if (volatilityPeriod_small !== this.volatilityDay && this.volatilityHasDays[volatilityPeriod_small].length > 0) {
          this.volatilityHandler({ volatilityPeriod_small });
        }
      }
    }).flow('restart', { delay: 500 }),
  }),
  computed: {
    sortedIndices() {
      return _.orderBy(this.indices, this.tableCols_composition_sortBy, (this.tableCols_composition_sortOrder < 1) ? 'desc' : 'asc');
    },
    benchmarkLeftItems() {
      const totalItems = this.benchmarksActive.portfolios.length + this.benchmarksActive.benchmarks.length;
      return this.benchmarksActiveMax - 1 - totalItems;
    },
    portfolios() {
      return this.$store.state.portfolios;
    },
    portfolioDetails() {
      const result = this.$store.state.portfolios[this.portfolio.id];
      result.color = '';
      return result;
    },
    benchmarks() {
      return this.$store.state.strategies;
    },
    allowExtraTools() {
      if (this.user.user_type === 'admin') return true;
      if (this.user.subscriptions.some(o => ['Standard', 'Premium', 'Premium + Custom Model'].includes(o.name))) return true;
      return false;
    },
    showExtraTools() {
      return this.user.user_type !== 'sponsor' || this.allowExtraTools;
    },
    indices() {
      const data = this.$store.state.portfolioIndices[this.portfolio.id] || [];
      const indices = [];
      for (let x = 0; x < data.length; x++) {
        const index = data[x];
        // weight isnt in indexinfo like the other stats, clone it over
        index.indexinfo.weighting = index.weighting;
        index.indexinfo.equal_weighting = index.equal_weighting;
        let label = index.indexinfo.returnType;
        if (label.length) {
          if (label.toUpperCase() === 'TOTAL RETURN') {
            label = 'TR';
          }
          if (label.toUpperCase() === 'EXCESS RETURN') {
            label = 'ER';
          }
          if (label.toUpperCase() === 'NET RETURN') {
            label = 'NR';
          }
        }
        const codeDot = index.indexinfo.code.length > 10 ? '...' : '';
        index.indexinfo.codeType = `${index.indexinfo.code.slice(0, 10)}${codeDot}`;
        index.indexinfo.weighting = this.portfolioDetails.strategyWeights[index.indexinfo.code] || 0;
        index.indexinfo.allocation = this.portfolioDetails.strategyAllocations[index.indexinfo.code] || 0;
        index.indexinfo.um_weighting = '%';
        if (index.suspended === 1) {
          // suspended strategy. dont use it in calculations
          this.indicesSuspended.push(index);
        } else {
          // otherwise add it to indices list
          indices.push(index);
        }
      }
      return indices;
    },
    asOf() {
      return (this.period === 'Cust' && this.customDates.to) ? this.customDates.to : this.maxDate;
    },
    isCustDisabled() {
      return this.periodDates.from < this.periods.min
        || this.periodDates.to > this.maxDate
        || (this.periodDates.from === this.customDates.from
          && this.periodDates.to === this.customDates.to);
    },
    basePayload() {
      let payload = {
        maxDate: this.maxDate,
        periods: this.periods,
      };
      if (this.period === 'Cust') {
        if (this.periodDates.to === payload.maxDate) {
          payload.periods[PERIOD.CUST] = this.periodDates.from; // add cust period
        } else {
          payload = {
            maxDate: this.periodDates.to, // set cust maxDate
            periods: { // set cust and min periods only
              [PERIOD.CUST]: this.periodDates.from,
              [PERIOD.MIN]: payload.periods[PERIOD.MIN],
            },
          };
        }
      }
      return payload;
    },
    tableOtherColWidth() {
      if (this.tableCols_compositionHasName) return 54 / (this.tableCols_composition_show.length - 1);
      return 94 / this.tableCols_composition_show.length;
    },
  },
  methods: {
    showAlert(type, message) {
      return this.$store.dispatch(ACTION.SHOW_ALERT_MODAL, { type, message });
    },
    checkChoose(field) {
      return (this.tableCols_composition_show.indexOf(field) < 0 && this.tableCols_composition_show.length >= this.tableCols_compositionMax)
        || (this.tableCols_composition_show.indexOf(field) >= 0 && this.tableCols_composition_show.length <= 4);
    },
    showTracksLoading() {
      setTimeout(() => {
        this.changeCheckBox = true;
        this.volatilityLoadingList[this.volatilityDay] = true;
      }, 50);
    },
    showDatePicker() {
      const $datePicker = $('.mx-datepicker-popup');
      if ($datePicker.length && !$datePicker.is(':visible')) {
        const toDate = string => moment(string).toDate();
        const isCust = this.period === 'Cust';
        this.datePickerDates = [
          toDate(isCust ? this.customDates.from || this.periodDates.from : this.periodDates.from),
          toDate(isCust ? this.customDates.to || this.periodDates.to : this.periodDates.to),
        ];
        $('.mx-input').click();
      }
    },
    changeButtonHandler({ newDays, oldDays, idx }) {
      if (oldDays) {
        const index = this.volatilityHasDays[oldDays].findIndex(x => x === idx);
        if (index >= 0) {
          this.volatilityHasDays[oldDays].splice(index, 1);
        }
      }
      if (newDays) {
        this.volatilityHasDays[newDays].push(idx);
      }
      for (const key in this.volatilityHasDays) {
        if (this.volatilityHasDays[key].length > 0) {
          this.volatilityDay = key;
          break;
        }
      }
    },
    needCallApiId({ key, type = 'all', measure = '30d' }) {
      const portfoliosNeedToLoad = this.benchmarksActive.portfolios.length;
      const benchmarksNeedToLoad = this.benchmarksActive.benchmarks.length;
      const loadingQueue = type === 'volatility' ? this.loadingVolatitltyQueue[measure] : this.loadingQueue;
      let benchmarksParamIds = [];
      let portfoliosParamIds = [];
      if (benchmarksNeedToLoad > 0) {
        const ids = this.benchmarksActive.benchmarks;
        benchmarksParamIds = (this.period === 'Cust') ? ids : ids.filter(o => !((this.benchmarks[o] && this.benchmarks[o][key]) || loadingQueue.benchmarks[o]));
      }
      if (portfoliosNeedToLoad > 0) {
        const ids = this.benchmarksActive.portfolios;
        portfoliosParamIds = (this.period === 'Cust') ? ids : ids.filter(o => !((this.filteredPortfolios[o] && this.filteredPortfolios[o][key]) || loadingQueue.portfolios[o]));
      }
      return { benchmarksParamIds, portfoliosParamIds };
    },
    async callApisForData({
      benchmarksParamIds, portfoliosParamIds, type, payload,
    }) {
      const vuexActions = [];
      const loadingQueue = type === 'volatility' ? this.loadingVolatitltyQueue[payload.measure] : this.loadingQueue;
      const strategyAction = type === 'volatility' ? ACTION.API_FETCH_STRATEGY_VOLATILITY : ACTION.API_FETCH_STRATEGY_DETAILS;
      const portfolioAction = type === 'volatility' ? ACTION.API_FETCH_PORTFOLIO_VOLATILITY : ACTION.API_FETCH_PORTFOLIO_DETAILS;
      const measure = type === 'volatility' ? payload.measure : payload.volMeasure;

      payload = { ...this.basePayload, ...payload };

      if (benchmarksParamIds.length > 0) {
        benchmarksParamIds.forEach((x) => {
          loadingQueue.benchmarks[x] = true;
        });
        vuexActions.push(this.$store.dispatch(strategyAction, { ids: benchmarksParamIds, payload }));
      }
      if (portfoliosParamIds.length > 0) {
        portfoliosParamIds.forEach((x) => {
          loadingQueue.portfolios[x] = true;
        });
        vuexActions.push(this.$store.dispatch(portfolioAction, { ids: portfoliosParamIds, payload }));
      }

      await Promise.settleAll(vuexActions);

      portfoliosParamIds.forEach((x) => {
        if (loadingQueue.portfolios[x]) {
          delete loadingQueue.portfolios[x];
        }
      });
      benchmarksParamIds.forEach((x) => {
        if (loadingQueue.benchmarks[x]) {
          delete loadingQueue.benchmarks[x];
        }
      });

      if (Object.keys(loadingQueue.portfolios).length === 0 && Object.keys(loadingQueue.benchmarks).length === 0) {
        this.volatilityLoadingList[measure] = false;
      }
    },
    applyCustomDates() {
      if (this.datePickerDates.length > 1) {
        if (this.period !== 'Cust') this.period = 'Cust';
        this.periodDates.from = moment(this.datePickerDates[0]).format('YYYY-MM-DD');
        this.periodDates.to = moment(this.datePickerDates[1]).format('YYYY-MM-DD');
        this.computeCustomPeriod();
      }
    },
    computeCustomPeriod() {
      if (this.isCustDisabled) return;
      this.customDates.to = this.periodDates.to;
      this.customDates.from = this.periodDates.from;

      const volMeasure = this.volatilityDay;
      const payload = { ...this.basePayload, volMeasure };

      // fetch strategies and portfolios details
      this.showTracksLoading();
      const strategyIds = this.benchmarksActive.benchmarks;
      const portfolioIds = [this.portfolioDetails.id, ...this.benchmarksActive.portfolios];
      Promise.all([
        this.$store.dispatch(ACTION.API_FETCH_STRATEGY_DETAILS, { ids: strategyIds, payload }),
        this.$store.dispatch(ACTION.API_FETCH_PORTFOLIO_DETAILS, { ids: portfolioIds, payload }),
      ]).then(() => {
        // emit load event for standalone components
        setTimeout(() => window.eventBus.$emit('cust-period.load'), 50);
        this.volatilityLoadingList[volMeasure] = false;
        return null;
      }).catch(console.error);

      // fetch volatility tracks for other volatility measures
      for (const volatilityPeriod_small in this.volatilityHasDays) {
        if (volatilityPeriod_small !== this.volatilityDay
        && this.volatilityHasDays[volatilityPeriod_small].length > 0) {
          this.volatilityHandler({ volatilityPeriod_small, reset: true });
        }
      }

      // emit fetch event for standalone components
      setTimeout(() => window.eventBus.$emit('cust-period.fetch'), 50);
    },
    volatilityHandler({ volatilityPeriod_small: measure, reset = false }) {
      // check whether need use api to call.
      const period = this.periodKeysForFilters[this.period];
      const fields = `${period}_volatility_track_${measure}`;

      // search for already data.
      let benchmarksParamIds = [];
      let portfoliosParamIds = [];
      if (reset) {
        benchmarksParamIds = this.benchmarksActive.benchmarks;
        portfoliosParamIds = [this.portfolioDetails.id, ...this.benchmarksActive.portfolios];
      } else {
        ({ benchmarksParamIds, portfoliosParamIds } = this.needCallApiId({ key: fields, type: 'volatility', measure }));

        if (!(this.portfolioDetails[fields]
        || this.loadingVolatitltyQueue[measure].portfolios[this.portfolioDetails.id])) {
          portfoliosParamIds.push(this.portfolioDetails.id);
        }
      }
      this.volatilityLoadingList[measure] = true;

      if (benchmarksParamIds.length === 0 && portfoliosParamIds.length === 0) {
        if (Object.keys(this.loadingVolatitltyQueue[measure].benchmarks).length === 0
          && Object.keys(this.loadingVolatitltyQueue[measure].portfolios).length === 0) {
          this.volatilityLoadingList[measure] = false;
        }
      } else {
        const payload = { period, measure };
        this.callApisForData({
          benchmarksParamIds, portfoliosParamIds, type: 'volatility', payload,
        });
      }
    },
    resetBenchmarkActiveData() {
      const benchmarksActiveData_mod = []; //the updated benchmarksActiveData
      const benchmark_ids = [];
      const portfolios_ids = [];
      const first = this.portfolioDetails;
      benchmarksActiveData_mod.push(first);

      // remove deleted items and color;
      for (let i = 1; i < this.benchmarksActiveData.length; i++) {
        //first always the strategy itself.
        const benchmark = this.benchmarksActiveData[i];
        if (benchmark.shortName !== undefined && benchmark.color !== '') { //was removed from indices benchmarks
          if (this.benchmarksActive.benchmarks.indexOf(benchmark.id) < 0) {
            this.benchmarkColors.push(benchmark.color);
            //its not going to be included in the updated array
          } else {
            benchmarksActiveData_mod.push(this.benchmarks[benchmark.id]); //keep it
            benchmark_ids.push(benchmark.id);
          }
        } else if (benchmark.shortName === undefined && benchmark.color !== '') { //was removed from portfolio benchmarks
          if (this.benchmarksActive.portfolios.indexOf(benchmark.id) < 0) {
            this.benchmarkColors.push(benchmark.color);
          } else {
            benchmarksActiveData_mod.push(this.filteredPortfolios[benchmark.id]);
            portfolios_ids.push(benchmark.id);
          }
        }
      }

      // add new choose items and color;
      for (let i = 0; i < this.benchmarksActive.benchmarks.length; i++) {
        const id = this.benchmarksActive.benchmarks[i];//ADD HERE
        const index = this.benchmarks[id];
        if (benchmark_ids.indexOf(index.id) < 0) {
          index.title = index.shortName;
          index.color = this.benchmarkColors[0];
          this.benchmarkColors.shift();
          benchmarksActiveData_mod.push(index);
        }
      }

      for (let i = 0; i < this.benchmarksActive.portfolios.length; i++) { //portfolios
        const id = this.benchmarksActive.portfolios[i];//ADD HERE
        const index = this.filteredPortfolios[id];
        if (portfolios_ids.indexOf(index.id) < 0) {
          index.color = this.benchmarkColors[0];
          this.benchmarkColors.shift();
          benchmarksActiveData_mod.push(index);
        }
      }
      this.changeCheckBox = false;
      this.benchmarksActiveData = benchmarksActiveData_mod.slice();
    },
    setOrginalPeriod() {
      if (this.portfolio) {
        let flag = false;
        for (const key in this.periodKeysForFilters) {
          if (flag === true || key === this.period) {
            flag = true;
            const value = `${this.periodKeysForFilters[key]}_active`;
            if (this.portfolio[value] === 1) {
              this.period = key;
              break;
            }
          }
        }
      }
    },
    setAllocation() {
      this.weighting = this.portfolio.weighting;
      this.allocationOptions = JSON.parse(this.portfolio.allocationOptions) || {};

      const allocationKeys1 = {
        rebalancing: ['Rebalancing', '', 'Monthly'],
        rebalancing_month: ['Rebalancing Month', '', 'Every Month'],
        rebalancing_date: ['Rebalancing Date', '', 'Last Day'],
        rebalancing_cost: ['Rebalancing cost', '%'],
        replication_cost: ['Replication cost', '% p.a.'],
      };
      if (!['equal', 'manual'].includes(this.weighting)) {
        allocationKeys1.volatility_interval = [
          this.weighting === 'erc' ? 'ERC Interval' : 'Volatility Interval',
        ];
      }
      if (this.allocationOptions.leverage_type && this.allocationOptions.leverage_type !== 'None') {
        switch (this.allocationOptions.leverage_type) {
          case 'Fixed Leverage':
            allocationKeys1.fixed_leverage = ['Fixed Leverage', '%'];
            break;
          case 'Target Volatility':
            allocationKeys1.target_volatility = ['Target Volatility', '%'];
            allocationKeys1.target_interval = ['Target Interval'];
            allocationKeys1.leverage_min = ['Min Leverage', '%', 0];
            allocationKeys1.leverage_max = ['Max Leverage', '%', 200];
            break;
        }
        // allocationKeys1.funding_currency = ['Funding'];
        // if (this.allocationOptions.funding_currency && this.allocationOptions.funding_currency !== 'None') {
        //   allocationKeys1.borrowing_spread = ['Borrowing Spread', '%'];
        //   allocationKeys1.lending_spread = ['Lending Spread', '%'];
        // }
      }
      this.allocationKeys1 = allocationKeys1;
      this.tableCols_performance_show = [];
      for (const periodLabel in this.periodKeysForFilters) {
        const k = this.periodKeysForFilters[periodLabel];
        if (this.portfolio[`${k}_active`] === 1) {
          this.tableCols_performance_show.push(periodLabel);
        }
      }
      this.tableCols_performance_show_ordered = this.tableCols_performance_show.reverse();
    },
    setBenchmarkActiveData() {
      const first = this.portfolioDetails;
      this.benchmarksActiveData.push(first);
    },
    setWidgetConfig(idx, val) {
      this.widgetConfig.splice(idx, 1, val);
    },

    getColors() {
      const that = this;
      this.$store.dispatch(ACTION.API_FETCH_COLORS, {}).then((o) => {
        that.colorsAll = o.benchmarks;
        that.assetClassColors = o.assetClass;
        that.categoryColors = o.factor;
        return null;
      }).catch(e => console.error(e));
    },
    loadPortfolios() {
      this.$store.dispatch(ACTION.API_FETCH_PORTFOLIO, {});
    },

    async loadBenchmarks() {
      const payload = {
        types: ['Benchmark'],
      };
      await this.$store.dispatch(ACTION.API_FETCH_STRATEGY, { payload });
      this.benchmarksLoaded = true;

      if (this.portfolio.weighting === 'custom' && this.portfolio.customRebalancing) {
        const { customAllocation = {} } = JSON.parse(this.portfolio.customRebalancing);
        const sortDate = (acc, cur) => acc > cur ? acc : cur;
        const lastAllocationKey = Object.keys(customAllocation).reduce(sortDate);
        const { funding_currency } = customAllocation[lastAllocationKey];
        if (['USD', 'EUR'].includes(funding_currency)) {
          const code = fundings[funding_currency];
          this.funding = Object.values(this.benchmarks).find(o => o.code === code) || {};
        }
      }
    },
    async loadIndices() {
      await this.$store.dispatch(ACTION.API_FETCH_PORTFOLIO_INDICES, { id: this.portfolio.id });
      this.loadingPortfolio = false;
    },
    setPeriod(period) {
      this.period = period;
    },
    removeBenchmark(benchmark) {
      if (benchmark.shortName !== undefined) {
        // its an index
        const idx = this.benchmarksActive.benchmarks.findIndex(x => x === benchmark.id);
        if (idx >= 0) {
          this.benchmarksActive.benchmarks.splice(idx, 1);
        }
      } else {
        // its a portfolio
        const idx = this.benchmarksActive.portfolios.findIndex(x => x === benchmark.id);
        if (idx >= 0) {
          this.benchmarksActive.portfolios.splice(idx, 1);
        }
      }
    },
    sortBenchmarks(a, b) {
      const sortby = this.tableCols_benchmarks_sortBy;
      if (sortby === 'shortName' || sortby === 'title') {
        if (checksInsensitive(_.lt, a[sortby], b[sortby])) { return -1; }
        if (checksInsensitive(_.gt, a[sortby], b[sortby])) { return 1; }
        return 0;
      }
      return parseFloat(a[sortby]) - parseFloat(b[sortby]);
    },

    changeSortby_benchmark(sortby) {
      // console.log(sortby,this.tableCols_benchmarks_sortBy,this.tableCols_benchmarks_sortOrder)
      if (this.tableCols_benchmarks_sortBy === sortby) {
        this.tableCols_benchmarks_sortOrder = this.tableCols_benchmarks_sortOrder * -1;
      }
      const reverse = this.tableCols_benchmarks_sortOrder;
      this.tableCols_benchmarks_sortBy = sortby;
      // float as string issue + orderby string issue.... sorting in data

      let array = this.benchmarksActiveData.sort(this.sortBenchmarks);
      if (reverse === 1) { array = array.reverse(); }
      this.benchmarksActiveData = array;
    },
    changeSortby_composition(sortby) {
      if (this.tableCols_composition_sortBy === sortby) {
        this.tableCols_composition_sortOrder = this.tableCols_composition_sortOrder * -1;
      }
      this.tableCols_composition_sortBy = sortby;
    },
    exportPdf(to) {
      this.showBenchmarkMenu1 = -1;
      this.showBenchmarkMenu2 = -1;

      const $pdfInput = $('#pdfinput input[name=html]').val('');

      // set right color for leverage graph, <clippath> need in <defs> for linuxpdf.
      const $leverages = $('.chart-leverage .amcharts-graph-fill-negative');
      const leverageInnerHtml = [];
      try {
        if ($leverages.length > 0) {
          $leverages.each(() => {
            const innerHtml = $(this).parent().parent()[0].innerHTML;
            const firstHtml = innerHtml.substr(0, innerHtml.indexOf('<clipPath'));
            const secondHtml = innerHtml.substr(innerHtml.indexOf('<clipPath'));
            const finalHtml = `<defs>${secondHtml}</defs>${firstHtml}`;
            leverageInnerHtml.push({
              orginal: innerHtml,
              replace: finalHtml,
            });
          });
        }
      } catch (e) {
        console.error(e);
      }

      // volatility graph causes issues with this
      $('[stroke-dasharray="0.5"]').removeAttr('stroke-dasharray');

      // maybe put a print.css file in here. dont get everything,
      // the @font-face in bootstrap kills dompdf and scripts make errors

      let prefcss = '';
      const prefs = Object.assign({}, basePrefs, _.pickBy(this.user, _.negate(_.isEmpty)));

      if (prefs.logoImage.length) {
        const imageUrl = `background-image:url(${prefs.logoImage})`;
        $('.print-header .print-border-image').attr('style', imageUrl);
      }

      prefcss += "<style type='text/css'>";
      prefcss += `.navbar-inverse.user { background:${prefs.navbarColor}; color: ${prefs.navbarTextColor}; }\n`;
      prefcss += `.print-border-right { border-color: ${prefs.navbarLineColor}; }\n`;
      prefcss += `h4.print-border { color:${prefs.headerColor}; border-color:${prefs.headerLineColor};}\n`;
      prefcss += '</style>';


      const header = `<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /><link href="${window.location.origin}/css/print.css" rel="stylesheet">${prefcss}</head><body>`;//dont use fintech.css ugh overflow/page break issues
      const footer = '</body></html>';
      let html = document.getElementById('print-container').innerHTML;
      const printheader = document.getElementById('print-header').innerHTML;
      html = html.replace(/<div class="print-header"><\/div>/g, printheader);

      try {
        if (leverageInnerHtml.length > 0) {
          leverageInnerHtml.forEach((o) => {
            html = html.replace(o.orginal, o.replace);
          });
        }
      } catch (e) {
        console.error(e);
      }

      html = header + html + footer;

      // see final html in new tab
      // const myWindow = window.open('');
      // myWindow.document.write(html);

      const $pdfForm = $('#pdfinput');
      $pdfForm.unbind('submit');
      $pdfForm.submit((e) => {
        e.preventDefault();
        switch (to) {
          case 'download':
            $pdfInput.val(html);
            $pdfForm.unbind('submit');
            $pdfForm.submit();
            break;
          default: {
            const action = $pdfForm.prop('action');
            this.loadingExport = true;
            this.$http.post(`${action}?to=${to}`, { html })
              .then(() => {
                this.loadingExport = false;
                return this.showAlert('success', `The PDF has been exported to ${_.startCase(to)}`);
              })
              .catch((e) => {
                console.error(e);
                this.loadingExport = false;
                this.showAlert('danger', '<strong>Whoops!</strong> Something went wrong! Please retry or contact support on <a href="mailto:admin@engagingenterprises.co" class="alert-link">admin@engagingenterprises.co</a>.');
              });
          }
        }
      });
      $('#pdfinput').submit();
    },
  },
});
