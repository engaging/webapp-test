Vue.component('labselector', {
  props: ['user'],

  data() {
    return {
      selectedType:'portfolios',
      selectedTool:'backtesting',
      keyword:''
    } 
  },
  computed: {
    device() {
      if ($(window).width() <= 768) {
        return 'mobile';
      }
      return 'desktop';
    },
    validState() {
        if(this.selectedType&&this.selectedTool)
        {
          return true;
        }

        return false;
    }
  },
  created() {

  },

  watch: {
		
  },

  methods: {
    submit() {
        location.href = '/lab';
    },
    selectType(type){

      this.selectedType = type;

    },
    selectTool(tool){

      this.selectedTool = tool;

    }

  },
});
