const ACTION = require('../../store/mutation-type');

Vue.component('alert-modal', {
  props: [],
  template: `
    <div>
      <div v-if="alertMessage" class="alert alert-overlay fade in" v-bind:class="'alert-' + alertMessage.type">
        <a href="#" class="close" data-dismiss="alert" @click="hideMessage">&times;</a>
        <span v-html="alertMessage.message"></span>
      </div>
    </div>
  `,
  computed: {
    alertMessage() {
      return this.$store.state.alertModal;
    },
  },
  watch: {
    alertMessage() {
    },
  },
  methods: {
    hideMessage() {
      this.$store.dispatch(ACTION.HIDE_ALERT_MODAL);
    },
  },
});
