import GridItem from './GridItem.vue';
import GridLayout from './GridLayout.vue';


const VueGridLayout = {
  GridLayout,
  GridItem,
};

module.exports = VueGridLayout;
