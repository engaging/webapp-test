/* global window document analytics moment Vue AmCharts $ _ */
import DatePicker from './vue2-datepicker';

const ACTION = require('../store/mutation-type');
const { basePrefs } = require('../util/config');
const { PERIOD, getPeriodKeys } = require('../util/period');
const { checksInsensitive } = require('../lib/lodash-helper');
const { getStrategyAnalytics } = require('../util/strategy');
const { metrics } = require('../util/config');

if (window.AmCharts) {
  AmCharts.baseHref = true;
}

const getDynamicData = (payload) => {
  const { options = {}, periods = {}, maxDate } = payload;
  const ascPeriodKeys = getPeriodKeys(periods);
  const ascPeriodKeysWithCust = Object.assign({ Cust: PERIOD.CUST }, ascPeriodKeys);
  const descPeriodKeys = getPeriodKeys(periods, 'desc');
  const descKeys = Object.keys(descPeriodKeys);
  let { period } = payload;
  if (!period) {
    period = options.period ? _.invert(ascPeriodKeys)[options.period] : '5 Year';
  }

  return {
    period,
    periodKeysForFilters: ascPeriodKeysWithCust,
    periodDates: {
      from: periods[ascPeriodKeys[period]],
      to: maxDate,
    },
    tableRows_performanceMax: 12,
    tableCols_performance: descPeriodKeys,
    tableCols_performance_show: descKeys,
    tableCols_performance_show_ordered: descKeys,
  };
};

const getDefaultData = payload => ({
  // note all graphs should work the same way as on portfolio_detail/portfolio.js,
  // except for cases where fieldnames may be different.
  ...getDynamicData(payload),
  infoKeys1: {
    'Index Provider': 'provider',
    'Index Code': 'code',
    'Index Source': 'indexSource',
    'Live Date': 'inceptionDate',
    'History Start Date': 'historyStartDate',
    Currency: 'currency',
    Style: 'style',
  },
  infoKeys2: {
    Factor: 'factor',
    'Asset Class': 'assetClass',
    Region: 'region',
    'Vol Target': 'volTarget',
    'Return Type': 'returnType',
    'Net Return': 'returnCategory',
  },
  customDates: {
    from: null,
    to: null,
  },
  datePickerDates: [],
  loadingExport: false,
  loadingQueue: {
    benchmarks: {},
    portfolios: {},
  },
  loadingVolatitltyQueue: {
    '30d': {
      benchmarks: {},
      portfolios: {},
    },
    '60d': {
      benchmarks: {},
      portfolios: {},
    },
    '120d': {
      benchmarks: {},
      portfolios: {},
    },
  },
  initBenchmarkIds: [],
  initPortfolioIds: [],
  stickyheader: false,
  changeCheckBox: false,
  filteredPortfolios: {},
  labels: metrics,
  tableCols_benchmarks_show: [
    'return',
    'volatility',
    'sharpe',
    'skewness',
    'var',
    'mdd',
  ],
  tableCols_benchmarksMax: 6,
  tableCols_benchmarksCount: 6,
  tableCols_benchmarks_sortBy: '',
  tableCols_benchmarks_sortOrder: -1,

  showBenchmarkMenu1: -1,
  /*mini list of columns for benchmarks table*/
  showBenchmarkMenu2: -1,
  /*list of benchmarks*/
  // this is the array to save in the db
  benchmarksActive: {
    portfolios: [], //{id:id,color:color} from the portfolio table
    benchmarks: [], //ids from the indices table
  },
  benchmarkColors: [
    'color1',
    'color2',
    'color3',
    'color4',
    'color5',
    'color6',
    'color7',
  ],
  // this is populated when we know benchmarksactive
  benchmarksActiveData: [],
  benchmarksActiveMax: 4,

  tableRows_performance_show: [
    'return',
    'volatility',
    'sharpe',
    'skewness',
    'var',
    'mdd',
    'worst20d',
    'posWeeks',
  ],
  tableRows_performanceMax: 12,
  showPerformanceMenu: -1,
  tableCols_performanceCount: 5,
  widgetConfig: [
    'chart-radar',
    'chart-drawdowntrack',
    'chart-volatilitytrack',
    'chart-correlationscatter',
    'chart-returnbar',
    'chart-returnscatter',
  ],
  widgetsAvailable: {
    'chart-radar': 'Metrics',
    'chart-drawdowntrack': 'Drawdown',
    'chart-volatilitytrack': 'Volatility',
    'chart-correlationscatter': 'Correlation Versus Benchmarks',
    'chart-returnbar': 'Monthly Return Distribution',
    'chart-returnscatter': 'Monthly Return Regression',
  },
  num_widgets: 6,
  volatilityHasDays: {
    '30d': [],
    '60d': [],
    '120d': [],
  },
  volatilityLoadingList: {
    '30d': false,
    '60d': false,
    '120d': false,
  },
  volatilityDay: '30d',
  benchmarksLoaded: false,
});

Vue.component('strategy-detail', {
  props: ['user', 'strategy', 'options', 'periods', 'maxDate'],
  components: { DatePicker },
  computed: {
    benchmarks() {
      const benchmarks = {};
      const { strategies } = this.$store.state;
      for (const key in strategies) {
        if (strategies[key].type === 'Benchmark' && strategies[key].id !== this.strategy.id) {
          benchmarks[key] = strategies[key];
        }
      }
      return benchmarks;
    },
    isPremiumPlan() {
      return this.user.user_type === 'admin' || this.user.subscriptions.some(o => ['Premium', 'Premium + Custom Model'].includes(o.name));
    },
    allowExtraTools() {
      if (this.user.user_type === 'admin') return true;
      if (this.user.subscriptions.some(o => ['Standard', 'Premium', 'Premium + Custom Model'].includes(o.name))) return true;
      return false;
    },
    showExtraTools() {
      return this.user.user_type !== 'sponsor' || this.allowExtraTools;
    },
    strategyDetails() {
      const result = this.$store.state.strategies[this.strategy.id];
      result.title = result.shortName;
      result.color = '';
      return result;
    },
    benchmarkLeftItems() {
      const totalItems = this.benchmarksActive.portfolios.length + this.benchmarksActive.benchmarks.length;
      return this.benchmarksActiveMax - 1 - totalItems;
    },
    portfolios() {
      return this.$store.state.portfolios;
    },
    colorsAll() {
      if (this.$store.state.colorsAll.benchmarks) {
        return this.$store.state.colorsAll.benchmarks;
      }
      return {
        color1: '#4a7c59',
        color2: '#68b0ab',
        color3: '#35a03a',
        color4: '#53ad8a',
        color5: '#7aa559',
        color6: '#9c89b8',
        color7: '#705695',
        color8: '#573b8c',
        color9: '#866487',
        color10: '#723f62',
        color11: '#dddddd',
        color12: '#cccccc',
      };
    },

    returnType() {
      switch ((this.strategy.returnType || '').toUpperCase()) {
        case 'TOTAL RETURN':
          return 'TR';
        case 'EXCESS RETURN':
          return 'ER';
        case 'NET RETURN':
          return 'NR';
        default:
          return '';
      }
    },
    returnCategory() {
      if (this.strategy.returnCategory && this.strategy.returnCategory.length) {
        if (this.strategy.returnCategory.toUpperCase() === 'NET') {
          return 'N';
        }
        return '';
      }
      return '';
    },
    asOf() {
      return (this.period === 'Cust' && this.customDates.to) ? this.customDates.to : this.maxDate;
    },
    isCustDisabled() {
      return this.periodDates.from < this.periods.min
        || this.periodDates.to > this.maxDate
        || (this.periodDates.from === this.customDates.from
          && this.periodDates.to === this.customDates.to);
    },
    basePayload() {
      let payload = {
        maxDate: this.maxDate,
        periods: this.periods,
      };
      if (this.period === 'Cust') {
        if (this.periodDates.to === payload.maxDate) {
          payload.periods[PERIOD.CUST] = this.periodDates.from; // add cust period
        } else {
          payload = {
            maxDate: this.periodDates.to, // set cust maxDate
            periods: { // set cust and min periods only
              [PERIOD.CUST]: this.periodDates.from,
              [PERIOD.MIN]: payload.periods[PERIOD.MIN],
            },
          };
        }
      }
      return payload;
    },
  },

  mounted() {
    const that = this;
    analytics.track('Viewed Strategy Factsheet', getStrategyAnalytics(that.strategy));
    window.eventBus.$on('volatility.api', this.volatilityHandler);
    window.eventBus.$on('volatility.change', this.changeButtonHandler);
    window.generatePdf = that.getPdf;
    that.configureWhenPrivate();
    window.addEventListener('scroll', () => {
      const navheight = 100;
      const scroll = $(window).scrollTop();
      if (scroll >= navheight) {
        that.stickyheader = true;
      } else {
        that.stickyheader = false;
      }
    });
    $('body').on('click', (e) => {
      if ($('.opened').length) {
        const target = e.toElement || e.target;
        if ($(target).parents('.opened').length === 0) {
          that[$('.opened').data('var')] = -1;
          that.showPerformanceMenu = -1;
        }
      }
    });

    that.tableCols_performance_show = [];
    for (const periodLabel in that.periodKeysForFilters) {
      const k = that.periodKeysForFilters[periodLabel];
      if (that.strategy[`${k}_active`] == 1) {
        that.tableCols_performance_show.push(periodLabel);
      }
    }
    that.tableCols_performance_show_ordered = that.tableCols_performance_show.reverse();
  },

  data() {
    return getDefaultData(this);
  },

  created() {
    this.uploadDataToVuex();
    this.setOrginalPeriod();
    this.setBenchmarkActiveData();
    this.getColors();
    this.loadBenchmarks();
    this.loadPortfolios();
  },

  watch: {
    period() {
      if (this.period === 'Cust') {
        if (this.customDates.from) this.periodDates.from = this.customDates.from;
        if (this.customDates.to) this.periodDates.to = this.customDates.to;
        else this.showDatePicker();
      } else {
        this.periodDates.from = this.periods[this.periodKeysForFilters[this.period]];
        this.periodDates.to = this.maxDate;
        this.loadBenchmarkData.run();
      }
    },
    tableCols_performance_show() {
      const res = [];
      const that = this;
      Object.keys(this.tableCols_performance).forEach((key) => {
        if (that.tableCols_performance_show.indexOf(key) >= 0) { res.push(key); }
      });
      this.tableCols_performance_show_ordered = res;
      this.tableCols_performanceCount = this.tableCols_performance_show.length;
    },

    tableCols_benchmarks_show() {
      this.tableCols_benchmarksCount = this.tableCols_benchmarks_show.length;
    },

    benchmarks(newBenchmarks) {
      if (_.some(this.benchmarksActiveData, o => o !== newBenchmarks[o.id])) {
        this.resetBenchmarkActiveData();
      }
    },
    portfolios(newPortfolios) {
      this.filteredPortfolios = {};
      for (const key in newPortfolios) {
        if (newPortfolios[key].activeStrategiesCount > 0) {
          this.filteredPortfolios[newPortfolios[key].id] = newPortfolios[key];
        }
      }
      if (_.some(this.benchmarksActiveData, o => o !== newPortfolios[o.id])) {
        this.resetBenchmarkActiveData();
      }
    },

    'benchmarksActive.portfolios': function () {
      this.showTracksLoading();
      this.loadBenchmarkData.run();
    },

    'benchmarksActive.benchmarks': function () {
      this.showTracksLoading();
      this.loadBenchmarkData.run();
    },

  },
  tasks: t => ({
    loadBenchmarkData: t(function* loadBenchmarkData() {
      const period = this.periodKeysForFilters[this.period];
      const fields = `${period}_active`;
      const payload = {
        volMeasure: this.volatilityDay,
      };

      const { benchmarksParamIds, portfoliosParamIds } = this.needCallApiId({ key: fields, type: 'all' });

      if (benchmarksParamIds.length === 0 && portfoliosParamIds.length === 0) {
        this.resetBenchmarkActiveData();
        this.volatilityLoadingList[payload.volMeasure] = false;
      } else {
        this.callApisForData({
          benchmarksParamIds, portfoliosParamIds, type: 'all', payload,
        });
      }
      //call for extra volitality data
      for (const volatilityPeriod_small in this.volatilityHasDays) {
        if (volatilityPeriod_small !== this.volatilityDay && this.volatilityHasDays[volatilityPeriod_small].length > 0) {
          this.volatilityHandler({ volatilityPeriod_small });
        }
      }
    }).flow('restart', { delay: 500 }),
  }),
  methods: {
    showAlert(type, message) {
      return this.$store.dispatch(ACTION.SHOW_ALERT_MODAL, { type, message });
    },
    showAddStrategies() {
      $('#addStrategies').modal('show');
    },
    showTracksLoading() {
      setTimeout(() => {
        this.changeCheckBox = true;
        this.volatilityLoadingList[this.volatilityDay] = true;
      }, 50);
    },
    showDatePicker() {
      const $datePicker = $('.mx-datepicker-popup');
      if ($datePicker.length && !$datePicker.is(':visible')) {
        const toDate = string => moment(string).toDate();
        const isCust = this.period === 'Cust';
        this.datePickerDates = [
          toDate(isCust ? this.customDates.from || this.periodDates.from : this.periodDates.from),
          toDate(isCust ? this.customDates.to || this.periodDates.to : this.periodDates.to),
        ];
        $('.mx-input').click();
      }
    },
    changeButtonHandler({ newDays, oldDays, idx }) {
      if (oldDays) {
        const index = this.volatilityHasDays[oldDays].findIndex(x => x === idx);
        if (index >= 0) {
          this.volatilityHasDays[oldDays].splice(index, 1);
        }
      }
      if (newDays) {
        this.volatilityHasDays[newDays].push(idx);
      }
      for (const key in this.volatilityHasDays) {
        if (this.volatilityHasDays[key].length > 0) {
          this.volatilityDay = key;
          break;
        }
      }
    },
    needCallApiId({ key, type = 'all', measure = '30d' }) {
      const portfoliosNeedToLoad = this.benchmarksActive.portfolios.length;
      const benchmarksNeedToLoad = this.benchmarksActive.benchmarks.length;
      const loadingQueue = type === 'volatility' ? this.loadingVolatitltyQueue[measure] : this.loadingQueue;
      let benchmarksParamIds = [];
      let portfoliosParamIds = [];
      if (benchmarksNeedToLoad > 0) {
        const ids = this.benchmarksActive.benchmarks;
        benchmarksParamIds = (this.period === 'Cust') ? ids : ids.filter(o => !((this.benchmarks[o] && this.benchmarks[o][key]) || loadingQueue.benchmarks[o]));
      }
      if (portfoliosNeedToLoad > 0) {
        const ids = this.benchmarksActive.portfolios;
        portfoliosParamIds = (this.period === 'Cust') ? ids : ids.filter(o => !((this.filteredPortfolios[o] && this.filteredPortfolios[o][key]) || loadingQueue.portfolios[o]));
      }
      return { benchmarksParamIds, portfoliosParamIds };
    },
    async callApisForData({
      benchmarksParamIds, portfoliosParamIds, type, payload,
    }) {
      const vuexActions = [];
      const loadingQueue = type === 'volatility' ? this.loadingVolatitltyQueue[payload.measure] : this.loadingQueue;
      const strategyAction = type === 'volatility' ? ACTION.API_FETCH_STRATEGY_VOLATILITY : ACTION.API_FETCH_STRATEGY_DETAILS;
      const portfolioAction = type === 'volatility' ? ACTION.API_FETCH_PORTFOLIO_VOLATILITY : ACTION.API_FETCH_PORTFOLIO_DETAILS;
      const measure = type === 'volatility' ? payload.measure : payload.volMeasure;

      payload = { ...this.basePayload, ...payload };

      if (benchmarksParamIds.length > 0) {
        benchmarksParamIds.forEach((x) => {
          loadingQueue.benchmarks[x] = true;
        });
        vuexActions.push(this.$store.dispatch(strategyAction, { ids: benchmarksParamIds, payload }));
      }
      if (portfoliosParamIds.length > 0) {
        portfoliosParamIds.forEach((x) => {
          loadingQueue.portfolios[x] = true;
        });
        vuexActions.push(this.$store.dispatch(portfolioAction, { ids: portfoliosParamIds, payload }));
      }

      await Promise.settleAll(vuexActions);

      portfoliosParamIds.forEach((x) => {
        if (loadingQueue.portfolios[x]) {
          delete loadingQueue.portfolios[x];
        }
      });
      benchmarksParamIds.forEach((x) => {
        if (loadingQueue.benchmarks[x]) {
          delete loadingQueue.benchmarks[x];
        }
      });

      if (Object.keys(loadingQueue.portfolios).length === 0 && Object.keys(loadingQueue.benchmarks).length === 0) {
        this.volatilityLoadingList[measure] = false;
      }
    },
    uploadDataToVuex() {
      this.$store.dispatch(ACTION.UPLOAD_STRATEGY_DETAILS, this.strategy);
      if (this.options.summary && this.options.summary.length > 0) {
        this.tableCols_benchmarks_show = this.options.summary;
      }
      if (this.options.measure && this.options.measure.length > 0) {
        this.tableRows_performance_show = this.options.measure;
      }
      if (this.options.otherIndices && this.options.otherIndices.length > 0) {
        this.options.otherIndices.forEach((o) => {
          if (o.code) {
            if (this.options.otherItems && this.options.otherItems.includes(o.code)) {
              this.initBenchmarkIds.push(o.id);
            }
            this.$store.dispatch(ACTION.UPLOAD_STRATEGY_DETAILS, o);
          } else {
            if (this.options.otherItems && this.options.otherItems.includes(o.slug)) {
              this.initBenchmarkIds.push(o.id);
            }
            this.$store.dispatch(ACTION.UPLOAD_PORTFOLIO_DETAILS, o);
          }
        });
      }
    },
    applyCustomDates() {
      if (this.datePickerDates.length > 1) {
        if (this.period !== 'Cust') this.period = 'Cust';
        this.periodDates.from = moment(this.datePickerDates[0]).format('YYYY-MM-DD');
        this.periodDates.to = moment(this.datePickerDates[1]).format('YYYY-MM-DD');
        this.computeCustomPeriod();
      }
    },
    computeCustomPeriod() {
      if (this.isCustDisabled) return;
      this.customDates.to = this.periodDates.to;
      this.customDates.from = this.periodDates.from;

      const volMeasure = this.volatilityDay;
      const payload = { ...this.basePayload, volMeasure };

      // fetch strategies and portfolios details
      this.showTracksLoading();
      const strategyIds = [this.strategyDetails.id, ...this.benchmarksActive.benchmarks];
      const portfolioIds = this.benchmarksActive.portfolios;
      Promise.all([
        this.$store.dispatch(ACTION.API_FETCH_STRATEGY_DETAILS, { ids: strategyIds, payload }),
        this.$store.dispatch(ACTION.API_FETCH_PORTFOLIO_DETAILS, { ids: portfolioIds, payload }),
      ]).then(() => {
        // emit load event for standalone components
        setTimeout(() => window.eventBus.$emit('cust-period.load'), 50);
        this.volatilityLoadingList[volMeasure] = false;
        return null;
      }).catch(console.error);

      // fetch volatility tracks for other volatility measures
      for (const volatilityPeriod_small in this.volatilityHasDays) {
        if (volatilityPeriod_small !== this.volatilityDay
        && this.volatilityHasDays[volatilityPeriod_small].length > 0) {
          this.volatilityHandler({ volatilityPeriod_small, reset: true });
        }
      }

      // emit fetch event for standalone components
      setTimeout(() => window.eventBus.$emit('cust-period.fetch'), 50);
    },
    volatilityHandler({ volatilityPeriod_small: measure, reset = false }) {
      // check whether need use api to call.
      const period = this.periodKeysForFilters[this.period];
      const fields = `${period}_volatility_track_${measure}`;

      // search for already data.
      let benchmarksParamIds = [];
      let portfoliosParamIds = [];
      if (reset) {
        benchmarksParamIds = [this.strategyDetails.id, ...this.benchmarksActive.benchmarks];
        portfoliosParamIds = this.benchmarksActive.portfolios;
      } else {
        ({ benchmarksParamIds, portfoliosParamIds } = this.needCallApiId({ key: fields, type: 'volatility', measure }));

        if (!(this.strategyDetails[fields]
        || this.loadingVolatitltyQueue[measure].benchmarks[this.strategyDetails.id])) {
          benchmarksParamIds.push(this.strategyDetails.id);
        }
      }
      this.volatilityLoadingList[measure] = true;

      if (benchmarksParamIds.length === 0 && portfoliosParamIds.length === 0) {
        if (Object.keys(this.loadingVolatitltyQueue[measure].benchmarks).length === 0
          && Object.keys(this.loadingVolatitltyQueue[measure].portfolios).length === 0) {
          this.volatilityLoadingList[measure] = false;
        }
      } else {
        const payload = { period, measure };
        this.callApisForData({
          benchmarksParamIds, portfoliosParamIds, type: 'volatility', payload,
        });
      }
    },
    setOrginalPeriod() {
      if (this.strategy) {
        let flag = false;
        for (const key in this.periodKeysForFilters) {
          if (flag === true || key === this.period) {
            flag = true;
            const value = `${this.periodKeysForFilters[key]}_active`;
            if (this.strategy[value] === 1) {
              this.period = key;
              break;
            }
          }
        }
      }
    },
    configureWhenPrivate() {
      if (this.strategy.type == 'Private Strategy') {
        this.infoKeys1 = {
          'Live Date': 'inceptionDate',
          'History Start Date': 'historyStartDate',
          Currency: 'currency',
          Style: 'style',
        };
      }
    },
    checkShowData(field) {
      return field === '0000-00-00' ? '-' : field;
    },
    setWidgetConfig(idx, val) {
      this.widgetConfig.splice(idx, 1, val);
    },
    getColors() {
      this.$store.dispatch(ACTION.API_FETCH_COLORS, {});
    },
    setPeriod(period) {
      this.period = period;
    },
    loadPortfolios() {
      this.indicesBasket = [];
      let indexToAdd = {
        id: this.strategy.id,
        shortName: this.strategy.shortName,
      };
      if (!indexToAdd) {
        indexToAdd = '';
      } else {
        this.indicesBasket = [indexToAdd];
      }
      this.$store.dispatch(ACTION.API_FETCH_PORTFOLIO, {}).then(() => {
        if (this.initPortfolioIds.length > 0) {
          this.benchmarksActive.portfolios = this.initPortfolioIds;
        }
        return null;
      }).catch(e => console.error(e));
    },
    loadBenchmarks() {
      const payload = { types: ['Benchmark'] };
      this.$store.dispatch(ACTION.API_FETCH_STRATEGY, { payload }).then(() => {
        this.benchmarksLoaded = true;
        if (this.initBenchmarkIds.length > 0) {
          this.benchmarksActive.benchmarks = this.initBenchmarkIds;
        }
        return null;
      }).catch(e => console.error(e));
    },
    setBenchmarkActiveData() {
      const first = this.strategyDetails;
      this.benchmarksActiveData.push(first);
    },
    resetBenchmarkActiveData() {
      const benchmarksActiveData_mod = []; //the updated benchmarksActiveData
      const benchmark_ids = [];
      const portfolios_ids = [];
      const first = this.strategyDetails;
      benchmarksActiveData_mod.push(first);

      // remove deleted items and color;
      for (let i = 1; i < this.benchmarksActiveData.length; i++) {
        //first always the strategy itself.
        const benchmark = this.benchmarksActiveData[i];
        if (benchmark.shortName !== undefined && benchmark.color !== '') { //was removed from indices benchmarks
          if (this.benchmarksActive.benchmarks.indexOf(benchmark.id) < 0) {
            this.benchmarkColors.push(benchmark.color);
            //its not going to be included in the updated array
          } else {
            benchmarksActiveData_mod.push(this.benchmarks[benchmark.id]); //keep it
            benchmark_ids.push(benchmark.id);
          }
        } else if (benchmark.shortName === undefined && benchmark.color !== '') { //was removed from portfolio benchmarks
          if (this.benchmarksActive.portfolios.indexOf(benchmark.id) < 0) {
            this.benchmarkColors.push(benchmark.color);
          } else {
            benchmarksActiveData_mod.push(this.filteredPortfolios[benchmark.id]);
            portfolios_ids.push(benchmark.id);
          }
        }
      }

      // add new choose items and color;
      for (let i = 0; i < this.benchmarksActive.benchmarks.length; i++) {
        const id = this.benchmarksActive.benchmarks[i];//ADD HERE
        const index = this.benchmarks[id];
        if (benchmark_ids.indexOf(index.id) < 0) {
          index.title = index.shortName;
          index.color = this.benchmarkColors[0];
          this.benchmarkColors.shift();
          benchmarksActiveData_mod.push(index);
        }
      }

      for (let i = 0; i < this.benchmarksActive.portfolios.length; i++) { //portfolios
        const id = this.benchmarksActive.portfolios[i];//ADD HERE
        const index = this.filteredPortfolios[id];
        if (portfolios_ids.indexOf(index.id) < 0) {
          index.color = this.benchmarkColors[0];
          this.benchmarkColors.shift();
          benchmarksActiveData_mod.push(index);
        }
      }
      this.changeCheckBox = false;
      this.benchmarksActiveData = benchmarksActiveData_mod.slice();
    },
    removeBenchmark(benchmark) {
      if (benchmark.shortName !== undefined) {
        // its an index
        const idx = this.benchmarksActive.benchmarks.findIndex(x => x === benchmark.id);
        if (idx >= 0) {
          this.benchmarksActive.benchmarks.splice(idx, 1);
        }
      } else {
        // its a portfolio
        const idx = this.benchmarksActive.portfolios.findIndex(x => x === benchmark.id);
        if (idx >= 0) {
          this.benchmarksActive.portfolios.splice(idx, 1);
        }
      }
    },

    getDomain(url) {
      if (url) {
        const domains = url.split('://');
        if (domains.length > 1) {
          const subdomain = domains[1].split('/')[0];
          if (subdomain.startsWith('www.')) return subdomain.substring(4);
          return subdomain;
        }
      }
      return '-';
    },

    sortBenchmarks(a, b) {
      const sortby = this.tableCols_benchmarks_sortBy;
      if (sortby === 'shortName' || sortby === 'title') {
        if (checksInsensitive(_.lt, a[sortby], b[sortby])) { return -1; }
        if (checksInsensitive(_.gt, a[sortby], b[sortby])) { return 1; }
        return 0;
      }
      return parseFloat(a[sortby]) - parseFloat(b[sortby]);
    },

    changeSortby_benchmark(sortby) {
      // sorting the benchmark table
      if (this.tableCols_benchmarks_sortBy == sortby) {
        this.tableCols_benchmarks_sortOrder = this.tableCols_benchmarks_sortOrder * -1;
      }
      const reverse = this.tableCols_benchmarks_sortOrder;
      this.tableCols_benchmarks_sortBy = sortby;
      // float as string issue + orderby string issue.... sorting in data

      let array = this.benchmarksActiveData.sort(this.sortBenchmarks);
      if (reverse === 1) { array = array.reverse(); }
      this.benchmarksActiveData = array;
    },

    exportPdf(to) {
      this.showBenchmarkMenu1 = -1;
      this.showBenchmarkMenu2 = -1;

      // volatility graph causes issues with this
      $('[stroke-dasharray="0.5"]').removeAttr('stroke-dasharray');

      const $pdfInput = $('#pdfinput input[name=html]').val('');
      let prefcss = '';
      const prefs = Object.assign({}, basePrefs, _.pickBy(this.user, _.negate(_.isEmpty)));

      if (prefs.logoImage.length) {
        const imageUrl = `background-image:url(${prefs.logoImage})`;
        $('.print-header .print-border-image').attr('style', imageUrl);
      }

      prefcss += "<style type='text/css'>";
      prefcss += `.navbar-inverse.user { background:${prefs.navbarColor}; color: ${prefs.navbarTextColor}; }\n`;
      prefcss += `.print-border-right { border-color: ${prefs.navbarLineColor}; }\n`;
      prefcss += `h4.print-border { color:${prefs.headerColor}; border-color:${prefs.headerLineColor};}\n`;
      prefcss += '.print-hide { display: none !important; }\n';
      prefcss += '</style>';

      const header = `<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /><link href="${window.location.origin}/css/print.css" rel="stylesheet">${prefcss}</head><body>`;
      const footer = '</body></html>';
      let html = document.getElementById('print-container').innerHTML;
      const printheader = document.getElementById('print-header').innerHTML;

      html = html.replace(/<div class="print-header"><\/div>/g, printheader);
      html = header + html + footer;

      // var wnd = window.open("about:blank", "", ""); wnd.document.write(html);

      const $pdfForm = $('#pdfinput');
      $pdfForm.unbind('submit');
      $pdfForm.submit((e) => {
        e.preventDefault();
        switch (to) {
          case 'download':
            if (window.env.API_TOKEN) {
              window.document.write(html);
            } else {
              $pdfInput.val(html);
              $pdfForm.unbind('submit');
              $pdfForm.submit();
            }
            break;
          default: {
            const action = $pdfForm.prop('action');
            this.loadingExport = true;
            this.$http.post(`${action}?to=${to}`, { html })
              .then(() => {
                this.loadingExport = false;
                return this.showAlert('success', `The PDF has been exported to ${_.startCase(to)}`);
              })
              .catch((e) => {
                console.error(e);
                this.loadingExport = false;
                this.showAlert('danger', '<strong>Whoops!</strong> Something went wrong! Please retry or contact support on <a href="mailto:admin@engagingenterprises.co" class="alert-link">admin@engagingenterprises.co</a>.');
              });
          }
        }
      });
      $('#pdfinput').submit();
    },
    async exportConstituents() {
      this.loadingExport = true;
      try {
        await this.$http.post(`/api/strategies/${this.strategy.id}/constituents/send`);
        this.showAlert('success', 'The constituents has been exported to Symphony');
      } catch (e) {
        console.error(e);
        this.showAlert('danger', '<strong>Whoops!</strong> Something went wrong! Please retry or contact support on <a href="mailto:admin@engagingenterprises.co" class="alert-link">admin@engagingenterprises.co</a>.');
      }
      this.loadingExport = false;
    },
  },
});
