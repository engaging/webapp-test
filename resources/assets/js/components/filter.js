Vue.filter('percentage', (a) => {
  a = parseFloat(a);
  return `${a.toFixed(2)}%`;
});


Vue.filter('4dp', (a) => {
  a = parseFloat(a);
  return a.toFixed(4);
});

Vue.filter('2dp', (a) => {
  a = parseFloat(a);
  return a.toFixed(2);
});
