import AmSerialChart from "amcharts/AmSerialChart";
import AmStockChart from "amcharts/AmStockChart";
import * as AmCharts from "amcharts"
import * as _ from "lodash";
// import Vue from 'vue/types/vue'

// declare function require(name:string);
// declare module '*.vue' {
//   export default Vue
// }

declare global {
  interface Window {
    Vue: any; 
    charts: {
      [id: string]: any;
    };
    AmCharts: any;
    eventBus: any;
  }
}

type Api = {
  [key: string]: string;  
}

export type apiUrl = Api & {
  BASE: string;
};