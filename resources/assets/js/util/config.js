module.exports = {
  assetClasses: [
    'Commodity',
    'Credit',
    'Equity',
    'Fixed Income',
    'Foreign Exchange',
    'Multi Assets',
  ],
  assetManagers: {
    'Nikko AM': {
      type: ['AM Backtest', 'AM Strategy'],
      maxDate: '2018-02-28',
    },
  },
  basePrefs: {
    logoImage: '',
    navbarColor: '#123b69', // .navbar-inverse.user background
    navbarTextColor: '#ffffff', // .navbar-inverse.user color
    navbarLineColor: '#2b87a8', //
    headerColor: '#123b69', // h4.print-border color
    headerLineColor: '#2b87a8', // h4.print-border border-bottom
    disclaimer: '',
  },
  factors: [
    'Carry',
    'Low Vol',
    'Momentum',
    'Multi',
    'Quality',
    'Size',
    'Value',
    'Volatility',
  ],
  fundings: {
    USD: 'MMUS',
    EUR: 'MMEU',
  },
  metrics: {
    returnCum: 'Return',
    return: 'Return p.a.',
    volatility: 'Volatility',
    sharpe: 'Sharpe',
    skewness: 'Skewness',
    kurtosis: 'Kurtosis',
    calmar: 'Calmar',
    omega: 'Omega',
    sortino: 'Sortino',
    var: 'VaR',
    mvar: 'ModVaR',
    cvar: 'CVaR',
    avg_drawdown: 'Avg DD',
    mdd: 'Max DD',
    worst20d: 'Worst 20d',
    posWeeks: '%Up Week',
    posMonths: '%Up Month',
  },
  models: {
    equal: 'Equal Weighting',
    manual: 'Manual Weighting',
    risk: 'Vol Risk Parity',
    erc: 'ERC Weighting',
    custom: 'Custom Rebalancing',
  },
};
