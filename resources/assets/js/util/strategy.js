/* global _ */
const map = require('lodash/fp/map');
const pipe = require('lodash/fp/pipe');
const filter = require('lodash/fp/filter');
const { checksInsensitiveFactory } = require('../lib/lodash-helper');

const ANALYTICS_KEYS = [
  'code',
  'provider',
  'currency',
  'type',
  'style',
  'factor',
  'assetClass',
  'region',
  'returnType',
];

const getUpper = (object, path) => _.get(object, path, '').toUpperCase();

const shortenReturnProps = (strategy, suffix = '') => {
  strategy = Object.assign({}, strategy);
  switch (getUpper(strategy, 'returnCategory')) {
    case 'NET':
      strategy[`returnCategory${suffix}`] = 'N';
      break;
  }
  switch (getUpper(strategy, 'returnType')) {
    case 'TOTAL RETURN':
      strategy[`returnType${suffix}`] = 'TR';
      break;
    case 'EXCESS RETURN':
      strategy[`returnType${suffix}`] = 'ER';
      break;
    case 'NET RETURN':
      strategy[`returnType${suffix}`] = 'NR';
      break;
  }
  return strategy;
};

const byKeywordFactory = (keyword, portfolios = []) => (strategy) => {
  if (!keyword) return true;

  keyword = keyword.trim();
  const startsWithKeyword = checksInsensitiveFactory(_.startsWith, keyword);
  if (startsWithKeyword(strategy.code)) {
    return true;
  }

  const words = keyword.split(' ');
  let check = true;
  for (const word of words) {
    const includesKeyword = checksInsensitiveFactory(_.includes, word);
    check = check && [
      strategy.shortName,
      strategy.longName,
      strategy.assetClass,
      strategy.factor,
      strategy.region,
      strategy.currency,
    ].some(includesKeyword);
  }
  if (check) return true;

  check = true;
  for (const word of words) {
    const includesKeyword = checksInsensitiveFactory(_.includes, word);
    const strategyIds = pipe(
      filter(o => includesKeyword(o.title)),
      map(o => o.strategyIds),
      _.flatten,
      _.uniq,
    )(portfolios);
    check = check && strategyIds.includes(strategy.id);
  }
  return check;
};

const byRelevanceFactory = keyword => (a, b) => {
  if (!keyword) return 0;

  const startsWithKeyword = checksInsensitiveFactory(_.startsWith, keyword);
  const includesKeyword = checksInsensitiveFactory(_.includes, keyword);

  const checkA = [a.code, a.shortName, a.longName].some(startsWithKeyword)
   || [a.assetClass, a.factor, a.region, a.currency].some(includesKeyword);

  const checkB = [b.code, b.shortName, b.longName].some(startsWithKeyword)
    || [b.assetClass, b.factor, b.region, b.currency].some(includesKeyword);

  if (!checkA && checkB) return 1;
  if (checkA && !checkB) return -1;

  const lowerNameA = a.shortName.toLowerCase();
  const lowerNameB = b.shortName.toLowerCase();

  if (lowerNameA > lowerNameB) return 1;
  if (lowerNameA < lowerNameB) return -1;

  return 0;
};

const getStrategyAnalytics = strategy =>
  _.mapKeys(_.pick(strategy, ANALYTICS_KEYS), (value, key) => `Strategy ${_.startCase(key)}`);

const getStrategiesAnalytics = strategies => _.mapKeys(_.reduce(strategies, (acc, cur) => {
  ANALYTICS_KEYS.forEach((key) => {
    if (!acc[key]) acc[key] = [];
    acc[key].push(cur[key]);
  });
  return acc;
}, {}), (value, key) => {
  if (key.endsWith('s')) {
    key = `${key}es`;
  } else if (key.endsWith('y')) {
    key = `${key.slice(0, -1)}ies`;
  } else {
    key = `${key}s`;
  }
  return `Strategy ${_.startCase(key)}`;
});

module.exports = {
  shortenReturnProps,
  byKeywordFactory,
  byRelevanceFactory,
  getStrategyAnalytics,
  getStrategiesAnalytics,
};
