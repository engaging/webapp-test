/* global _ */
const map = require('lodash/fp/map');
const pipe = require('lodash/fp/pipe');
const filter = require('lodash/fp/filter');
const reduce = require('lodash/fp/reduce');
const orderBy = require('lodash/fp/orderBy');
const toPairs = require('lodash/fp/toPairs');

const PERIOD = {
  CUST: 'custom',
  MIN: 'min',
};

const DATA_PERIODS = ['6m', '1y', '3y', '5y', '10y', 'live', 'custom'];

const getPeriodKey = (period) => {
  let key = _.startCase(period);
  if (key.endsWith(' Y')) {
    key += 'ear';
  } else if (key.endsWith(' M')) {
    key += 'onth';
  }
  return key;
};

/**
 * Get period keys
 * @param  {Object} periods       periods: { period: 'date' }
 * @param  {String} [order='asc'] order: 'asc'|'desc'
 * @return {Object}               period keys: { key: 'period' }
 */
const getPeriodKeys = (periods, order = 'asc') => pipe(
  toPairs,
  map(a => ({ period: a[0], date: a[1] })),
  filter(o => o.period !== PERIOD.MIN && o.period !== PERIOD.CUST),
  orderBy('date', order),
  reduce((a, o) => {
    a[getPeriodKey(o.period)] = o.period;
    return a;
  }, {}),
)(periods);

module.exports = {
  PERIOD,
  DATA_PERIODS,
  getPeriodKey,
  getPeriodKeys,
};
