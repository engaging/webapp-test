/* global _ */
const map = require('lodash/fp/map');
const pipe = require('lodash/fp/pipe');
const split = require('lodash/fp/split');
const { checksInsensitiveFactory } = require('../lib/lodash-helper');

const filterByKeywordFactory = (users = []) => keyword => users.filter(o => [
  o.firstname,
  o.lastname,
  o.name,
  o.email,
  o.country,
  o.company,
].some(checksInsensitiveFactory(_.includes, keyword)));

const filterByKeyword = (keyword, users = []) => {
  if (!keyword) {
    return users;
  }
  if (Number.isFinite(+keyword)) {
    return users.filter(o => o.id === +keyword);
  }
  return pipe(
    _.trim,
    split(' '),
    map(filterByKeywordFactory(users)),
    arrays => _.intersection(...arrays),
  )(keyword);
};

module.exports = {
  filterByKeyword,
};
