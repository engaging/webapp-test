const { round } = require('mathjs');

const required = (name = 'a') => { throw new Error(`Missing ${name} parameter`); };

const getEqualWeight = ({
  zeroCount = required('zeroCount'),
  leverage = required('leverage'),
  lockedSum = required('lockedSum'),
  oldLeverage = leverage,
  oldLockedSum = lockedSum,
} = {}) =>
  round(((leverage - lockedSum) - (oldLeverage - oldLockedSum)) / zeroCount, 2);

const getRatioWeight = ({
  weighting = required('weighting'),
  leverage = required('leverage'),
  lockedSum = required('lockedSum'),
  oldLeverage = leverage,
  oldLockedSum = lockedSum,
} = {}) =>
  round(weighting * (leverage - lockedSum) / (oldLeverage - oldLockedSum), 2);

const getRatioWeightForNewLeverage = ({
  weighting = required('weighting'),
  leverage = required('leverage'),
  lockedSum = required('lockedSum'),
  oldLeverage = required('oldLeverage'),
} = {}) =>
  getRatioWeight({
    weighting,
    leverage,
    lockedSum,
    oldLeverage,
  });

const getRatioWeightForNewLocked = ({
  weighting = required('weighting'),
  leverage = required('leverage'),
  lockedSum = required('lockedSum'),
  oldLockedSum = required('oldLockedSum'),
  oldLeverage = required('oldLeverage'),
} = {}) =>
  getRatioWeight({
    weighting,
    leverage,
    lockedSum,
    oldLockedSum,
    oldLeverage,
  });

const createLine = (allocation = required('allocation'), rebalancingOptions = required('rebalancingOptions'), funding = required('funding')) => {
  const {
    rebalancing_cost = 0,
    replication_cost = 0,
    ...baseOptions
  } = rebalancingOptions;
  const line = { ...baseOptions };
  if (line.funding_weight === undefined) {
    line.funding_weight = 100;
  }
  if (line.custom_weighting !== allocation.custom_weighting) {
    line.custom_weighting = allocation.custom_weighting;
    line.custom_weighting_button = true;
    line.leverage_option_button = true;
  } else {
    line.custom_weighting_button = false;
    line.leverage_option_button = false;
  }
  line.fixed_leverage = round(100 - allocation.fundings, 2);
  line.changeCorrelation = false;
  line.correlation = {};
  line.funding_rate = funding.funding_rate;
  for (const code in allocation.correlation) {
    line.correlation[code] = {};
    for (const subCode in allocation.correlation[code]) {
      line.correlation[code][subCode] = {
        locked: 0,
        value: allocation.correlation[code][subCode],
      };
    }
  }
  const codeCount = Object.keys(allocation.allocation).length;
  for (const code in allocation.allocation) {
    line[code] = {
      locked: 0,
      maxDD: allocation.maxDD[code],
      vol: allocation.vol[code],
      volLocked: 0,
      risk: round(100 / codeCount, 2),
      riskLocked: 0,
      weighting: allocation.allocation[code],
      rebalancing_cost: rebalancing_cost,
      replication_cost: replication_cost,
    };
  }
  return line;
};

const updateLine = (line = required('line'), allocation = required('allocation'), funding, resetRisk) => {
  const listCodes = Object.keys(allocation.correlation);
  if (!line.changeCorrelation) { // reset correlation and vol
    for (const code in allocation.correlation) {
      line.correlation[code] = {};
      line[code].vol = allocation.vol[code];
      line[code].volLocked = 0;
      for (const subCode in allocation.correlation[code]) {
        line.correlation[code][subCode] = {
          locked: 0,
          value: allocation.correlation[code][subCode],
        };
      }
    }
  } else { // only reset unlocked correlation and vol
    for (const code in allocation.correlation) {
      if (line[code].volLocked === 0) {
        line[code].vol = allocation.vol[code];
      }
      for (const subCode in allocation.correlation[code]) {
        if (line.correlation[code][subCode].locked === 0) {
          line.correlation[code][subCode].value = allocation.correlation[code][subCode];
        }
      }
    }
  }
  if (funding) {
    line.funding_rate = funding.funding_rate;
  }
  if (line.custom_weighting !== allocation.custom_weighting) {
    line.custom_weighting = allocation.custom_weighting;
    line.custom_weighting_button = true;
    line.leverage_option_button = true;
  }
  line.fixed_leverage = round(100 - allocation.fundings, 2);
  listCodes.forEach((code) => {
    line[code].weighting = allocation.allocation[code];
    line[code].maxDD = allocation.maxDD[code];
  });
  if (resetRisk) {
    listCodes.forEach((code) => {
      line[code].riskLocked = 0;
      line[code].risk = round(100 / listCodes.length, 2);
    });
  }
  return line;
};

module.exports = {
  getEqualWeight,
  getRatioWeightForNewLeverage,
  getRatioWeightForNewLocked,
  createLine,
  updateLine,
};
