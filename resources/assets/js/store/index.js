/* global Vue */
const Vuex = require('vuex');
const actions = require('./actions');
const mutations = require('./mutations');
const getters = require('./getters');
const { lab } = require('./module/lab');
const { vol } = require('./module/volatility');
const { cor } = require('./module/correlation');
const { regression } = require('./module/regression');
const { pca } = require('./module/pca');

Vue.use(Vuex);

module.exports = new Vuex.Store({
  state: {
    //colors,
    colorsAll: {},
    portfolios: {},
    strategies: {},
    portfolioIndices: {},
    filters: {},
    feed: [],
    news: [],
    alertModal: null,
    periodDates: {},
    hiddenPeriods: {},
  },
  actions,
  mutations,
  getters,
  modules: {
    lab,
    vol,
    cor,
    regression,
    pca,
  },
});
