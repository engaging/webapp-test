/* global _ */
const pipe = require('lodash/fp/pipe');
const ACTION = require('./mutation-type');

module.exports = {
  [ACTION.API_FETCH_PORTFOLIO]: (state, { fetchedData, freeze = false }) => {
    const { portfolios } = state;
    const funcs = [o => Object.assign({}, portfolios[o.id], o)];
    if (freeze) {
      funcs.push(Object.freeze);
    }
    const merge = pipe(...funcs);
    const newPortfolios = fetchedData.reduce((acc, cur) => {
      acc[cur.id] = merge(cur);
      return acc;
    }, {});
    state.portfolios = Object.assign({}, portfolios, newPortfolios);
  },

  [ACTION.API_FETCH_COLORS]: (state, fetchedData) => {
    state.colorsAll = fetchedData;
  },

  [ACTION.API_FETCH_STRATEGY]: (state, { fetchedData, transform, freeze = false }) => {
    const { strategies } = state;
    const funcs = [o => Object.assign({}, strategies[o.id], o)];
    if (_.isFunction(transform)) {
      funcs.push(transform);
    }
    if (freeze) {
      funcs.push(Object.freeze);
    }
    const merge = pipe(...funcs);
    const newStrategies = fetchedData.reduce((acc, cur) => {
      acc[cur.id] = merge(cur);
      return acc;
    }, {});
    state.strategies = Object.assign({}, strategies, newStrategies);
  },

  [ACTION.API_FETCH_PORTFOLIO_DETAILS]: (state, { fetchedData, reset = false, freeze = false }) => {
    const { portfolios } = state;
    const funcs = [o => Object.assign({}, portfolios[o.id], o)];
    if (reset) {
      funcs[0] = o => o;
    }
    if (freeze) {
      funcs.push(Object.freeze);
    }
    const merge = pipe(...funcs);
    const newPortfolios = fetchedData.reduce((acc, cur) => {
      acc[cur.id] = merge(cur);
      return acc;
    }, {});
    state.portfolios = Object.assign({}, portfolios, newPortfolios);
  },

  [ACTION.API_CREATE_PORTFOLIO]: (state, portfolio) => {
    state.portfolios[portfolio.id] = portfolio;
    state.portfolios = Object.assign({}, state.portfolios);
  },
  [ACTION.API_FETCH_STRATEGY_DETAILS]: (state, { fetchedData, freeze = false }) => {
    const benchmarks = state.strategies;
    fetchedData.forEach((o) => {
      const data = Object.assign({}, benchmarks[o.id], o);
      benchmarks[o.id] = freeze ? Object.freeze(data) : data;
    });
    state.strategies = Object.assign({}, benchmarks); // trigger vue object reactivity
  },

  [ACTION.API_FETCH_STRATEGY_VOLATILITY]: (state, { fetchedData, period, measure }) => {
    const field = `${period}_volatility_track_${measure}`;
    fetchedData.forEach((o) => {
      state.strategies[o.id][field] = o[field];
    });
  },

  [ACTION.API_FETCH_PORTFOLIO_VOLATILITY]: (state, { fetchedData, period, measure }) => {
    const field = `${period}_volatility_track_${measure}`;
    fetchedData.forEach((o) => {
      state.portfolios[o.id][field] = o[field];
    });
  },

  [ACTION.API_FETCH_STRATEGIES_CORRELATION]: (state, { period, fetchedData }) => {
    for (const keys in fetchedData) {
      const [id1, id2] = keys.split('-');
      const key = `${period}_correlation`;
      const obj1 = {};
      const obj2 = {};
      obj1[id2] = fetchedData[keys];
      obj2[id1] = fetchedData[keys];
      state.strategies[id1] = Object.assign({}, state.strategies[id1]);
      state.strategies[id1][key] = Object.assign({}, state.strategies[id1][key], obj1);
      state.strategies[id2] = Object.assign({}, state.strategies[id2]);
      state.strategies[id2][key] = Object.assign({}, state.strategies[id2][key], obj2);
    }
  },

  [ACTION.API_FETCH_BENCHMARKS_CORRELATION]: (state, {
    type, id, ids, period, portfolioOnly, fetchedData,
  }) => {
    const key = `${period}_correlation`;
    const mainObj = {};
    let mainResult = fetchedData;
    if (!portfolioOnly) {
      for (const id in fetchedData.i) {
        const obj = {};
        obj[ids[0]] = fetchedData.i[id].x;
        obj[ids[1]] = fetchedData.i[id].y;
        state.strategies[id][key] = Object.assign({}, state.strategies[id][key], obj);
      }
      mainResult = fetchedData.p;
    }
    mainObj[ids[0]] = mainResult.x;
    mainObj[ids[1]] = mainResult.y === undefined ? mainResult.x : mainResult.y;
    if (type === 'strategy') {
      state.strategies[id][key] = Object.assign({}, state.strategies[id][key], mainObj);
    } else {
      state.portfolios[id][key] = Object.assign({}, state.portfolios[id][key], mainObj);
    }
  },

  [ACTION.API_FETCH_FILTER]: (state, fetchedData) => {
    state.filters = fetchedData;
  },

  [ACTION.API_FETCH_FEED]: (state, fetchedData) => {
    state.feed = state.feed.concat(fetchedData.data);
  },

  [ACTION.API_FETCH_FEED_DETAILS]: (state, { fetchedData }) => {
    state.feed = fetchedData;
  },

  [ACTION.API_FETCH_NEWS]: (state, fetchedData) => {
    state.news = state.news.concat(fetchedData);
  },

  [ACTION.API_FETCH_NEWS_DETAILS]: (state, { fetchedData, index }) => {
    state.news[index] = Object.assign(state.news[index], fetchedData);
  },

  [ACTION.UPLOAD_STRATEGY_DETAILS]: (state, strategy) => {
    state.strategies[strategy.id] = strategy;
  },

  [ACTION.UPLOAD_PORTFOLIO_DETAILS]: (state, portfolio) => {
    state.portfolios[portfolio.id] = portfolio;
  },

  [ACTION.SET_TIME_PERIOD]: (state, period) => {
    state.periodDates = period;
  },

  [ACTION.SHOW_ALERT_MODAL]: (state, alertModal) => {
    state.alertModal = alertModal;
  },

  [ACTION.HIDE_ALERT_MODAL]: (state) => {
    state.alertModal = null;
  },

  [ACTION.SHOW_PERIODS]: (state, periods) => {
    if (!Array.isArray(periods)) {
      periods = Object.keys(periods);
    }
    if (!periods.length) {
      periods = ['6m', '1y', '3y', '5y', '10y', 'live'];
    }
    state.hiddenPeriods = periods.reduce((hiddenPeriods, period) => {
      hiddenPeriods[period] = false;
      return hiddenPeriods;
    }, { ...state.hiddenPeriods });
  },

  [ACTION.HIDE_PERIODS]: (state, periods) => {
    if (!Array.isArray(periods)) {
      periods = Object.keys(periods);
    }
    if (!periods.length) {
      periods = ['6m', '1y', '3y', '5y', '10y', 'live'];
    }
    state.hiddenPeriods = periods.reduce((hiddenPeriods, period) => {
      hiddenPeriods[period] = true;
      return hiddenPeriods;
    }, { ...state.hiddenPeriods });
  },

  [ACTION.API_FETCH_PORTFOLIO_INDICES]: (state, { fetchedData, id, freeze = false }) => {
    const { portfolioIndices } = state;
    const data = freeze ? Object.freeze(fetchedData.map(o => Object.freeze(o))) : fetchedData;
    portfolioIndices[id] = data;
    state.portfolioIndices = Object.assign({}, portfolioIndices);
  },
};
