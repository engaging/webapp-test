/* global moment */
const ACTION = require('./mutation-type');
const { VOL_ACTION } = require('./module/volatility');
const { COR_ACTION } = require('./module/correlation');
const apiPortfolios = require('../api/portfolios');
const apiColors = require('../api/colors');
const apiStrategies = require('../api/strategies');
const apiFilters = require('../api/filters');
const apiNews = require('../api/news');
const apiFeed = require('../api/feed');

const message = `
  <strong>Whoops!</strong> Unable to compute using the given options! Please modify options and try again.
  <br>If the problem persists, please <a href="mailto:admin@engagingenterprises.co?subject=Computation%20Issue" class="alert-link">contact us</a> for more detail.
`;

module.exports = {
  /**
   * Fetch portfolios
   *
   * @param  {Object} payload payload options
   *  [metrics=false] {Boolean} attach metrics if true
   *  [shares=false]  {Boolean} attach shares if true
   *  periodKeys      {Array}   override default periodKeys
   * @return {Array}
   */
  async [ACTION.API_FETCH_PORTFOLIO]({ commit }, { payload, freeze = false }) {
    const fetchedData = await apiPortfolios.loadPortfolios(payload).then(o => o.data);
    commit(ACTION.API_FETCH_PORTFOLIO, { fetchedData, freeze });
  },

  async [ACTION.API_CREATE_PORTFOLIO]({ commit }, {
    id,
    indices,
    title,
    mode,
  }) {
    const { portfolio } = await apiPortfolios.putPortfolio(id, {
      mode,
      title,
      indices,
    }).then(o => o.data) || {};
    commit(ACTION.API_CREATE_PORTFOLIO, portfolio);
    return portfolio;
  },

  /**
   * Fetch portfolios details
   *
   * @param  {Array}   ids           portfolios ids
   * @param  {Boolean} [reset=false] reset portfolios if true
   * @param  {Object}  payload       payload options
   *  [shares=false]     {Boolean} attach shares if true
   *  [volMeasure='30d'] {String}  volatility measure: 30d|60d|120d
   *  maxDate            {String}  override default maxDate
   *  periods            {Object}  override default periods
   *  ignoreCache        {Boolean} not use any cache
   *  reCache            {Boolean} not fetch from cache but store the result in cache
   * @param  {Boolean} [showError=false]      show error if true
   * @param  {Boolean} [throwError=showError] throw error if true
   * @return {Array}
   */
  async [ACTION.API_FETCH_PORTFOLIO_DETAILS]({ commit, dispatch }, {
    ids = [],
    reset = false,
    payload,
    showError = false,
    throwError = showError,
  }) {
    const promise = id => apiPortfolios.loadOnePortfolio(id, payload).then(o => o.data);
    const { values: fetchedData, reasons: errors } = await Promise.settleMap(ids, promise)
      .then(Promise.reduceInspections);

    if (showError) {
      const { periods } = payload;
      dispatch(errors.length ? ACTION.HIDE_PERIODS : ACTION.SHOW_PERIODS, { periods });
    }
    if (throwError && errors.length) {
      throw errors[0];
    }

    commit(ACTION.API_FETCH_PORTFOLIO_DETAILS, { fetchedData, reset });
  },

  /**
   * Fetch portfolios details using multiple payloads
   *
   * @param  {Array}   ids            portfolios ids
   * @param  {Boolean} [reset=false]  reset portfolios if true
   * @param  {Boolean} [freeze=false] freeze items if true
   * @param  {Array}   payloads       payloads options
   *  [shares=false]     {Boolean} attach shares if true
   *  [volMeasure='30d'] {String}  volatility measure: 30d|60d|120d
   *  maxDate            {String}  override default maxDate
   *  periods            {Object}  override default periods
   *  ignoreCache        {Boolean} not use any cache
   *  reCache            {Boolean} not fetch from cache but store the result in cache
   * @param  {Boolean} [showError=false]      show error if true
   * @param  {Boolean} [throwError=showError] throw error if true
   * @return {Array}
   */
  async [ACTION.API_FETCH_PORTFOLIO_DETAILS_V2]({ commit, dispatch }, {
    ids = [],
    reset = false,
    freeze = false,
    payloads = [undefined],
    showError = false,
    throwError = showError,
  }) {
    const promise = id => Promise.settleAll(payloads.map(payload =>
      apiPortfolios.loadOnePortfolio(id, payload).then(o => o.data)));

    const { values: fetchedData, reasons: errors } = await Promise.map(ids, promise)
      .map(Promise.reduceInspections)
      .reduce((acc, cur) => {
        if (cur.values.length) acc.values.push(Object.assign(...cur.values));
        if (cur.reasons.length) cur.reasons.forEach(x => acc.reasons.push(x));
        return acc;
      }, { values: [], reasons: [] });

    if (showError) {
      const periods = Object.assign({}, ...payloads.map(o => o && o.periods));
      dispatch(errors.length ? ACTION.HIDE_PERIODS : ACTION.SHOW_PERIODS, { periods });
    }
    if (throwError && errors.length) {
      throw errors[0];
    }

    commit(ACTION.API_FETCH_PORTFOLIO_DETAILS, { fetchedData, reset, freeze });
    if (freeze) {
      const useful = payloads[payloads.length - 1] || { volMeasure: '30d' };
      commit(VOL_ACTION.SET_PORTFOLIOS, {
        fetchedData,
        reset,
        freeze,
        vol: useful.volMeasure,
      });
    }
  },

  async [ACTION.API_FETCH_COLORS]({ commit }, payload) {
    const fetchedData = await apiColors.getColors(payload).then(o => o.data);
    commit(ACTION.API_FETCH_COLORS, fetchedData);
    return fetchedData;
  },

  /**
   * Fetch strategies
   *
   * @param  {Object}  payload         payload options
   *  [full=false]    {Boolean} full static info if true
   *  [metrics=false] {Boolean} attach metrics if true
   *  ids             {Array}   strategies ids
   *  only            {Array}   only strategy codes
   *  ignore          {Array}   ignore strategy codes
   *  providers       {Array}   providers names
   *  assetClasses    {Array}   asset classes
   *  factors         {Array}   factors
   *  currencies      {Array}   currencies
   *  regions         {Array}   regions
   *  styles          {Array}   styles
   *  returnTypes     {Array}   return types
   *  types           {Array}   types: Strategy|Benchmark|Private Strategy
   *  keyword         {String}  keyword
   *  from            {String}  updated from date
   *  to              {String}  updated to date
   *  periodKeys      {Array}   override default periodKeys
   *  ignoreCache     {Boolean} not use any cache (only for Private Strategy and custom periods)
   *  reCache         {Boolean} not fetch from cache but store the result in cache
   * @param  {Function} transform      transform items if provided
   * @param  {Boolean}  [freeze=false] freeze items if true
   * @return {Array}
   */
  async [ACTION.API_FETCH_STRATEGY]({ commit }, { payload, transform, freeze = false }) {
    const fetchedData = await apiStrategies.loadStrategies(payload).then(o => o.data);
    commit(ACTION.API_FETCH_STRATEGY, { fetchedData, transform, freeze });
    return fetchedData;
  },

  /**
   * Fetch strategies details
   *
   * @param  {Array}  ids     strategies ids
   * @param  {Object} payload payload options
   *  [volMeasure='30d'] {String} volatility measure: 30d|60d|120d
   *  maxDate            {String} override default maxDate
   *  periods            {Object} override default periods
   *  ignoreCache        {Boolean} not use any cache
   *  reCache            {Boolean} not fetch from cache but store the result in cache
   * @return {Array}
   */
  async [ACTION.API_FETCH_STRATEGY_DETAILS]({ commit }, {
    ids = [],
    payload,
    freeze = false,
  }) {
    const promise = id => apiStrategies.loadOneStrategy(id, payload).then(o => o.data);
    const { values: fetchedData, reasons: errors } = await Promise.settleMap(ids, promise).then(Promise.reduceInspections);
    if (errors.length) {
      console.error(errors);
    }
    commit(ACTION.API_FETCH_STRATEGY_DETAILS, { fetchedData, freeze });
  },

  /**
   * Fetch strategies details
   *
   * @param  {Array}  ids     strategies ids
   * @param  {Array}   payloads      payloads options
   *  [volMeasure='30d'] {String} volatility measure: 30d|60d|120d
   *  maxDate            {String} override default maxDate
   *  periods            {Object} override default periods
   *  ignoreCache        {Boolean} not use any cache
   *  reCache            {Boolean} not fetch from cache but store the result in cache
   * @return {Array}
   */
  async [ACTION.API_FETCH_STRATEGY_DETAILS_v2]({ commit }, {
    ids = [],
    payloads,
    freeze = false,
  }) {
    const promise = id => Promise.settleAll(payloads.map(payload =>
      apiStrategies.loadOneStrategy(id, payload).then(o => o.data)));

    const { values: fetchedData, reasons: errors } = await Promise.map(ids, promise)
      .map(Promise.reduceInspections)
      .reduce((acc, cur) => {
        if (cur.values.length) acc.values.push(Object.assign(...cur.values));
        if (cur.reasons.length) cur.reasons.forEach(x => acc.reasons.push(x));
        return acc;
      }, { values: [], reasons: [] });

    if (errors.length) {
      console.error(errors);
    }
    commit(ACTION.API_FETCH_STRATEGY_DETAILS, { fetchedData, freeze });
  },

  /**
   * Load volatility tracks for given strategies ids, period and measure
   *
   * @param  {Array}  ids     strategies ids
   * @param  {Object} payload payload options
   *  [period='5y']   {String} volatility period
   *  [measure='30d'] {String} volatility measure: 30d|60d|120d
   *  maxDate         {String} override default maxDate
   *  periods         {Object} override default periods
   * @return {Array}          volatility tracks
   */
  async [ACTION.API_FETCH_STRATEGY_VOLATILITY]({ commit }, {
    ids = [],
    payload = {},
  }) {
    const { period = '5y', measure = '30d' } = payload;
    const promise = id => apiStrategies.loadOneVolatility(id, payload).then(o => ({
      id, [`${period}_volatility_track_${measure}`]: o.data,
    }));
    const { values: fetchedData, reasons: errors } = await Promise.settleMap(ids, promise).then(Promise.reduceInspections);
    if (errors.length) {
      console.error(errors);
    }
    commit(ACTION.API_FETCH_STRATEGY_VOLATILITY, { fetchedData, period, measure });
  },

  /**
   * Fetch volatility tracks for given portfolios ids, period and measure
   *
   * @param  {Array}  ids     portfolios ids
   * @param  {Object} payload payload options
   *  [period='5y']   {String} volatility period
   *  [measure='30d'] {String} volatility measure: 30d|60d|120d
   *  maxDate         {String} override default maxDate
   *  periods         {Object} override default periods
   * @return {Array}          volatility tracks
   */
  async [ACTION.API_FETCH_PORTFOLIO_VOLATILITY]({ commit }, {
    ids = [],
    payload = {},
    freeze = false,
  }) {
    const { period = '5y', measure = '30d' } = payload;
    const promise = id => apiPortfolios.loadOneVolatility(id, payload).then(o => ({
      id, [`${period}_volatility_track_${measure}`]: o.data,
    }));
    const { values: fetchedData, reasons: errors } = await Promise.settleMap(ids, promise).then(Promise.reduceInspections);
    if (errors.length) {
      console.error(errors);
    }
    if (freeze) {
      commit(VOL_ACTION.SET_PORTFOLIOS, { fetchedData, vol: measure, freeze });
    } else {
      commit(ACTION.API_FETCH_PORTFOLIO_VOLATILITY, { fetchedData, period, measure });
    }
  },


  /**
   * Fetch portfolio indices for given portfolio id
   *
   * @param  {Number} id      portfolio id
   * @param  {Object} payload payload options
   *  [metrics=false] {Boolean} attach metrics if true
   *  periodKeys      {Array}   override default periodKeys
   * @return {Array}          portfolio indices
   */
  async [ACTION.API_FETCH_PORTFOLIO_INDICES]({ commit }, { id, payload, freeze = false }) {
    const fetchedData = await apiPortfolios.loadPortfolioIndices(id, payload).then(o => o.data);
    commit(ACTION.API_FETCH_PORTFOLIO_INDICES, { fetchedData, id, freeze });
  },

  /**
   * Fetch strategies correlation
   *
   * @param  {Object} payload payload options
   *  ids           {Array}  strategies ids
   *  [period='5y'] {String} volatility period
   *  maxDate       {String} override default maxDate
   *  periods       {Object} override default periods
   * @return {Object}         strategies correlation
   */
  async [ACTION.API_FETCH_STRATEGIES_CORRELATION]({ commit }, { payload, reset = false, freeze = false }) {
    const fetchedData = await apiStrategies.loadStrategiesCorrelation(payload).then(o => o.data).catch(() => {});
    const { period = '5y' } = payload;
    commit(COR_ACTION.LOAD_STRATEGIES, { period, fetchedData, reset, freeze });
  },

  /**
   * Fetch benchmarks correlation for given strategy or portfolio id and period
   *
   * @param  {Number} id      strategy or portfolio id
   * @param  {String} type    type: strategy|portfolio
   * @param  {Object} payload payload options
   *  ids                   {Array}   benchmarks ids
   *  [period='5y']         {String}  volatility period
   *  [portfolioOnly=false] {Boolean} ignore portfolio constituents if true
   *  maxDate               {String}  override default maxDate
   *  periods               {Object}  override default periods
   * @return {Object}         benchmarks correlation
   */
  async [ACTION.API_FETCH_BENCHMARKS_CORRELATION]({ commit }, {
    id,
    type,
    payload = {},
    reset = false,
    freeze = false,
  }) {
    const { ids = [], period = '5y', portfolioOnly = false } = payload;
    const apiHandler = (type === 'strategy') ? apiStrategies : apiPortfolios;
    const fetchedData = await apiHandler.loadOneCorrelation(id, payload).then(o => o.data).catch(() => {});
    if (freeze) {
      commit(COR_ACTION.LOAD_PORTFOLIOS, {
        type, id, ids, period, fetchedData, reset, freeze,
      });
    } else {
      commit(ACTION.API_FETCH_BENCHMARKS_CORRELATION, {
        type, id, ids, period, portfolioOnly, fetchedData,
      });
    }
  },

  /**
   * Fetch strategies filters
   *
   * @param  {Object} payload payload options
   * @return {Object}         strategies filters
   */
  async [ACTION.API_FETCH_FILTER]({ commit }, payload) {
    let fetchedData;
    if (payload) {
      fetchedData = await apiStrategies.loadFilters(payload).then(o => o.data);
    } else {
      fetchedData = await apiFilters.getFilters().then(o => o.data);
    }
    commit(ACTION.API_FETCH_FILTER, fetchedData);
  },

  async [ACTION.API_FETCH_FEED]({ commit }, payload) {
    const fetchedData = await apiFeed.searchFeed(payload).then(o => o.data);
    commit(ACTION.API_FETCH_FEED, fetchedData);
  },

  async [ACTION.API_FETCH_FEED_DETAILS]({ commit }, { id, method, payload }) {
    let fetchedData = null;
    switch (method) {
      case 'put':
        fetchedData = await apiFeed.putFeedById(id, payload).then(o => o.data.data);
        break;
      case 'delete':
        fetchedData = await apiFeed.deleteFeedById(id).then(o => o.data.data);
        break;
      case 'post':
        fetchedData = await apiFeed.postFeedById(id, payload).then(o => o.data.data);
        break;
    }
    commit(ACTION.API_FETCH_FEED_DETAILS, { fetchedData, method });
  },

  async [ACTION.API_FETCH_NEWS]({ commit }) {
    const fetchedData = await apiNews.getNews().then(o => o.data);
    fetchedData.forEach((item) => {
      item.type = 'FacNews';
      const d = moment(item.addedAt, 'YYYYMMDDHHmmss');
      item.created_at = d.format('YYYY-MM-DD HH:mm:ss');
    });
    commit(ACTION.API_FETCH_NEWS, fetchedData);
  },

  async [ACTION.API_FETCH_NEWS_DETAILS]({ commit }, { id, index }) {
    const fetchedData = await apiNews.getNewsById(id).then(o => o.data);
    commit(ACTION.API_FETCH_NEWS_DETAILS, { fetchedData, index });
  },

  [ACTION.UPLOAD_STRATEGY_DETAILS]({ commit }, strategy) {
    commit(ACTION.UPLOAD_STRATEGY_DETAILS, strategy);
  },

  [ACTION.UPLOAD_PORTFOLIO_DETAILS]({ commit }, portfolio) {
    commit(ACTION.UPLOAD_PORTFOLIO_DETAILS, portfolio);
  },

  [ACTION.HIDE_ALERT_MODAL]({ commit }) {
    commit(ACTION.HIDE_ALERT_MODAL);
  },

  [ACTION.SHOW_ALERT_MODAL]({ commit }, alertModal) {
    commit(ACTION.SHOW_ALERT_MODAL, alertModal);
  },

  [ACTION.SHOW_PERIODS]({ commit, dispatch }, { periods }) {
    commit(ACTION.SHOW_PERIODS, periods);
    dispatch(ACTION.HIDE_ALERT_MODAL);
  },

  [ACTION.HIDE_PERIODS]({ commit, dispatch }, { periods }) {
    commit(ACTION.HIDE_PERIODS, periods);
    dispatch(ACTION.SHOW_COMPUTE_ERROR_ALERT_MODAL);
  },

  [ACTION.SHOW_COMPUTE_ERROR_ALERT_MODAL]({ dispatch }) {
    dispatch(ACTION.SHOW_ALERT_MODAL, { type: 'danger', message });
  },

};
