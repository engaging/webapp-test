/* global window document Vue */
const LAB_ACTION = {
  SET_TOOL: 'LAB_SET_TOOL',
  SET_CHOICE: 'LAB_SET_CHOICE',
  SET_VIEW: 'LAB_SET_VIEW',
  SET_PERIOD: 'LAB_SET_PERIOD',
  SET_LOADING: 'LAB_SET_LOADING',
  SET_ITEM: 'LAB_SET_ITEM',
};

const state = {
  tool: 'backtesting',
  view: '',
  backtesting: {},
  pca: {},
  regression: {},
  common: {},
  loading: false,
  analyseItem: {},
};

const actions = {
  [LAB_ACTION.SET_TOOL]({ commit }, tool) {
    commit(LAB_ACTION.SET_TOOL, tool);
  },
  [LAB_ACTION.SET_VIEW]({ commit }, view) {
    commit(LAB_ACTION.SET_VIEW, view);
  },
  [LAB_ACTION.SET_PERIOD]({ commit }, period) {
    commit(LAB_ACTION.SET_PERIOD, period);
  },
  [LAB_ACTION.SET_CHOICE]({ commit }, choice) {
    commit(LAB_ACTION.SET_CHOICE, choice);
  },
  [LAB_ACTION.SET_LOADING]({ commit }, loading) {
    commit(LAB_ACTION.SET_LOADING, loading);
  },
  [LAB_ACTION.SET_ITEM]({ commit }, item) {
    commit(LAB_ACTION.SET_LOADING, item);
  },
};

const mutations = {
  [LAB_ACTION.SET_TOOL](state, tool) {
    state.tool = tool;
  },
  [LAB_ACTION.SET_VIEW](state, view) {
    state.view = view;
  },
  [LAB_ACTION.SET_PERIOD](state, period) {
    state.common = period;
  },
  [LAB_ACTION.SET_ITEM](state, item) {
    state.analyseItem = item;
  },
  [LAB_ACTION.SET_CHOICE](state, { field = '', choice }) {
    if (field === '') {
      state[state.tool] = choice;
    } else {
      Vue.set(state[state.tool], field, choice);
    }
  },
  [LAB_ACTION.SET_LOADING](state, loading) {
    state.loading = loading;
  },
};

const getters = {
  labChoice: state => state[state.tool],
  // labbacktesting: state => state.backtesting,
  // labpca: state => state.pca,
  // labregression: state => state.regression,
};

module.exports = {
  LAB_ACTION,
  lab: {
    state,
    actions,
    mutations,
    getters,
  },
};
