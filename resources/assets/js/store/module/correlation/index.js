/* global window document */
const pipe = require('lodash/fp/pipe');

const COR_ACTION = {
  LOAD_STRATEGIES: 'COR_LOAD_STRATEGIES',
  LOAD_PORTFOLIOS: 'COR_LOAD_PORTFOLIOS',
  CLEAR: 'COR_CLEAR',
};

const state = {
  strategies: {}, //correlation between strategies,
  portfolios: {}, //correaltion between portfolios and others,
};

const actions = {
  [COR_ACTION.LOAD_STRATEGIES]({ commit }, tool) {
    commit(COR_ACTION.LOAD_STRATEGIES, tool);
  },
  [COR_ACTION.LOAD_PORTFOLIOS]({ commit }, view) {
    commit(COR_ACTION.LOAD_PORTFOLIOS, view);
  },
};

const mutations = {
  [COR_ACTION.LOAD_STRATEGIES](state, {
    period = '5y',
    fetchedData,
    // reset = false,
    freeze = false,
  }) {
    const funcs = [o => Object.assign({}, state.strategies[period], o)];
    if (freeze) {
      funcs.push(Object.freeze);
    }
    const merge = pipe(...funcs);
    const newData = merge(fetchedData);
    state.strategies = Object.assign({}, state.strategies, { [period]: newData });
  },
  [COR_ACTION.CLEAR](state, {
    key = 'all',
    field = 'strategies',
  }) {
    if (key === 'all') {
      state[field] = {};
    } else {
      state[field] = Object.assign({}, state[field], { [key]: {} });
    }
  },
  [COR_ACTION.LOAD_PORTFOLIOS](state, {
    type,
    id,
    ids,
    period,
    fetchedData,
    freeze = false,
  }) {
    if (fetchedData.i === undefined) {
      return null;
    }
    const items = {};
    for (const stringId in fetchedData.i) {
      const s_id = +stringId;
      const key0 = s_id < ids[0] ? `${s_id}-${ids[0]}` : `${ids[0]}-${s_id}`;
      items[key0] = fetchedData.i[stringId].x;
      if (ids[0] !== ids[1]) {
        const key1 = s_id < ids[1] ? `${s_id}-${ids[1]}` : `${ids[1]}-${s_id}`;
        items[key1] = fetchedData.i[stringId].y;
      }
    }
    const mainKey0 = id < ids[0] ? `${id}-${ids[0]}` : `${ids[0]}-${id}`;
    const mainKey1 = id < ids[1] ? `${id}-${ids[1]}` : `${ids[1]}-${id}`;
    if (type === 'portfolio') {
      const periodData = Object.freeze(Object.assign({}, state.portfolios[period], {
        [mainKey0]: fetchedData.p.x,
        [mainKey1]: fetchedData.p.y || fetchedData.p.x,
      }));
      state.portfolios = Object.assign({}, state.portfolios, { [period]: periodData });
    } else {
      items[mainKey0] = fetchedData.p.x;
      items[mainKey1] = fetchedData.p.y || fetchedData.p.x;
    }
    const funcs = [
      o => Object.assign({}, state.strategies[period], o),
    ];
    if (freeze) {
      funcs.push(Object.freeze);
    }
    const merge = pipe(...funcs);
    const newData = merge(items);
    state.strategies = Object.assign({}, state.strategies, { [period]: newData });
  },
};

const getters = {
};

module.exports = {
  COR_ACTION,
  cor: {
    state,
    actions,
    mutations,
    getters,
  },
};
