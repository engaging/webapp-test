/* global window document _ */
const pipe = require('lodash/fp/pipe');
const pick = require('lodash/fp/pick');
const { DATA_PERIODS, PERIOD } = require('../../../util/period');

const VOL_ACTION = {
  SET_STRATEGIES: 'VOL_SET_STRATEGIES',
  SET_PORTFOLIOS: 'VOL_SET_PORTFOLIOS',
  CLEAR: 'VOL_CLEAR',
};

const state = {
  strategies: {},
  portfolios: {},
};

const actions = {
  [VOL_ACTION.SET_STRATEGIES]({ commit }, tool) {
    commit(VOL_ACTION.SET_STRATEGIES, tool);
  },
  [VOL_ACTION.SET_PORTFOLIOS]({ commit }, view) {
    commit(VOL_ACTION.SET_PORTFOLIOS, view);
  },
  [VOL_ACTION.CLEAR]({ commit }, view) {
    commit(VOL_ACTION.CLEAR, view);
  },
};

const mutations = {
  [VOL_ACTION.CLEAR](stat, {
    type = 'portfolios',
    period = PERIOD.CUST,
    measure = ['30d', '60d', '120d'],
    id,
  }) {
    const deleteKeys = measure.map(vol => `${period}_volatility_track_${vol}`);
    const data = state[type][id];
    if (data) {
      const newData = Object.freeze({ [id]: _.omit(data, deleteKeys) });
      state[type] = Object.assign({}, state[type], newData);
    }
  },
  // [VOL_ACTION.SET_STRATEGIES](state, {
  //   fetchedData,
  //   reset = false,
  //   freeze = false,
  //   vol = '30d',
  // }) {
  //   const { strategies } = state;
  //   const keywords = DATA_PERIODS.map(x => `${x}_volatility_track_${vol}`);
  //   keywords.push('id');
  //   const funcs = [
  //     pick(keywords),
  //     o => Object.assign({}, strategies[o.id], o),
  //   ];
  //   if (reset) {
  //     funcs[1] = o => o;
  //   }
  //   if (freeze) {
  //     funcs.push(Object.freeze);
  //   }
  //   const merge = pipe(...funcs);
  //   const newStrategies = fetchedData.reduce((acc, cur) => {
  //     acc[cur.id] = merge(cur);
  //     return acc;
  //   }, {});
  //   state.strategies = Object.assign({}, strategies, newStrategies);
  // },
  [VOL_ACTION.SET_PORTFOLIOS](state, {
    fetchedData,
    reset = false,
    freeze = false,
    vol = '30d',
  }) {
    const { portfolios } = state;
    const keywords = DATA_PERIODS.map(x => `${x}_volatility_track_${vol}`);
    keywords.push('id');
    const funcs = [
      pick(keywords),
      o => Object.assign({}, portfolios[o.id], o),
    ];
    if (reset) {
      funcs[1] = o => o;
    }
    if (freeze) {
      funcs.push(Object.freeze);
    }
    const merge = pipe(...funcs);
    const newPortfolios = fetchedData.reduce((acc, cur) => {
      acc[cur.id] = merge(cur);
      return acc;
    }, {});
    state.portfolios = Object.assign({}, portfolios, newPortfolios);
  },
};

const getters = {
};

module.exports = {
  VOL_ACTION,
  vol: {
    state,
    actions,
    mutations,
    getters,
  },
};
