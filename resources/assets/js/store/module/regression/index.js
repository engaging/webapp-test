const ACTION = require('../../mutation-type');
const apiRegression = require('../../../api/regression');

const RG_ACTION = {
  LOAD_FACTORS: 'RG_LOAD_FACTORS',
  LOAD_HEATMAP: 'RG_LOAD_HEATMAP',
  CLEAR: 'RG_CLEAR',
};

const state = {
  summary: {},
  main: {},
  dates: {},
  units: {},
  decimals: {},
  total: {},
  trackDates: {},
  trackAlpha: {},
};

const actions = {
  [RG_ACTION.CLEAR]({ commit }, results) {
    commit(RG_ACTION.CLEAR, results);
  },
  async [RG_ACTION.LOAD_FACTORS]({ commit, dispatch }, {
    id,
    type = 'portfolio',
    payload,
  }) {
    const { period = '5y' } = payload;
    try {
      const { data } = await apiRegression.loadOneRegression(id, type, payload);
      commit(RG_ACTION.LOAD_FACTORS, { data, period });
      dispatch(ACTION.SHOW_PERIODS, { periods: [period] });
    } catch (e) {
      console.error(e);
      dispatch(ACTION.HIDE_PERIODS, { periods: [period] });
    }
  },
  async [RG_ACTION.LOAD_HEATMAP]({ commit, dispatch }, {
    id,
    type = 'portfolio',
    clearError = false,
    payload,
  }) {
    const { period = '5y' } = payload;
    try {
      const { data } = await apiRegression.loadOneHeatmap(id, type, payload);
      commit(RG_ACTION.LOAD_HEATMAP, { data, period });
      if (clearError) {
        dispatch(ACTION.SHOW_PERIODS, { periods: [period] });
      }
    } catch (e) {
      console.error(e);
      dispatch(ACTION.HIDE_PERIODS, { periods: [period] });
    }
  },
};

const mutations = {
  [RG_ACTION.CLEAR](state, { initData = false }) {
    state.summary = {};
    state.main = {};
    state.dates = {};
    state.units = {};
    state.decimals = {};
    state.trackDates = {};
    state.trackAlpha = {};
    if (initData) {
      state.total = {};
    }
  },
  [RG_ACTION.LOAD_HEATMAP](state, { data, period }) {
    state.total[period] = Object.freeze(data);
  },
  [RG_ACTION.LOAD_FACTORS](state, { data, period }) {
    const {
      summary,
      dates,
      units,
      decimals,
      trackDates,
      trackAlpha,
      ...main
    } = data;
    state.summary = Object.assign({}, state.summary, {
      [period]: Object.freeze(summary),
    });
    state.dates = Object.assign({}, state.dates, {
      [period]: Object.freeze(dates),
    });
    state.trackDates = Object.assign({}, state.trackDates, {
      [period]: Object.freeze(trackDates),
    });
    state.trackAlpha = Object.assign({}, state.trackAlpha, {
      [period]: Object.freeze(trackAlpha),
    });
    state.units = units;
    state.decimals = decimals;
    state.main = Object.assign({}, state.main, {
      [period]: Object.freeze(main),
    });
  },
};

const getters = {
};

module.exports = {
  RG_ACTION,
  regression: {
    state,
    actions,
    mutations,
    getters,
  },
};
