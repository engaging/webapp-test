const ACTION = require('../../mutation-type');
const apiPca = require('../../../api/pca');

const PCA_ACTION = {
  LOAD_PCAS: 'PCA_LOAD_PCAS',
  CLEAR: 'PCA_CLEAR',
};

const state = {
  summary: {},
  dates: {},
  units: {},
  portfolio: {},
  main: {}, // take all other date,
  datesCount: {},
};

const actions = {
  [PCA_ACTION.CLEAR]({ commit }, results) {
    commit(PCA_ACTION.CLEAR, results);
  },
  async [PCA_ACTION.LOAD_PCAS]({ commit, dispatch }, {
    id,
    payload,
  }) {
    const { period = '5y' } = payload;
    try {
      const { data = {} } = await apiPca.loadOnePca(id, payload);
      commit(PCA_ACTION.LOAD_PCAS, { data, period });
      dispatch(ACTION.SHOW_PERIODS, { periods: [period] });
    } catch (e) {
      console.error(e);
      dispatch(ACTION.HIDE_PERIODS, { periods: [period] });
    }
  },
};

const mutations = {
  [PCA_ACTION.CLEAR](state) {
    state.summary = {};
    state.main = {};
    state.dates = {};
    state.units = {};
    state.portfolio = {};
  },
  [PCA_ACTION.LOAD_PCAS](state, { data, period }) {
    const {
      summary,
      dates,
      units,
      portfolio,
      ...main
    } = data;
    state.summary = Object.assign({}, state.summary, {
      [period]: Object.freeze(summary),
    });
    state.dates = Object.assign({}, state.dates, {
      [period]: Object.freeze(dates),
    });
    state.datesCount = Object.assign({}, state.datesCount, {
      [period]: dates.length,
    });
    state.units = units;
    state.portfolio = Object.assign({}, state.portfolio, {
      [period]: Object.freeze(portfolio),
    });
    state.main = Object.assign({}, state.main, {
      [period]: Object.freeze(main),
    });
  },
};

const getters = {
};

module.exports = {
  PCA_ACTION,
  pca: {
    state,
    actions,
    mutations,
    getters,
  },
};
