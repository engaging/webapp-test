var base = require('../../spark/kiosk/users');

Vue.component('spark-kiosk-users', {
    mixins: [base],
    
    created(){
	    this.getUsers();
    }
});
