var base = require('../../spark/settings/payment-method-stripe');

Vue.component('spark-payment-method-stripe', {
    mixins: [base]
});
