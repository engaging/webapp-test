var base = require('../../spark/settings/payment-method-braintree');

Vue.component('spark-payment-method-braintree', {
    mixins: [base]
});
