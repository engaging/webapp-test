var base = require('../../../spark/settings/security/disable-two-factor-auth');

Vue.component('spark-disable-two-factor-auth', {
    mixins: [base]
});
