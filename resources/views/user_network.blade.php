@extends('spark::layouts.app') 
@section('title', '| Network') 
@section('scripts') 
@stop 
@section('content')

<usernetwork :user="user" inline-template>
  <div class="container network">
    <div class="row">
      <div class="sidebar-filters col-sm-3" style="padding:0" v-if="user.user_type!='sponsor'">
        <div class="sidebar" style="max-height: none;">

          <div class="col-xs-12">
            <h4 class="label hidden-xs"></h4>
            <div class="visible-xs">
              <div @click="toggleSidebar" class="btn btn-default form-control toggle-sidebar-filters">
                <h4 class="label">PARTNERS
                  <i v-show="showFilters == false" class="fa fa-caret-down"></i>
                  <i v-show="showFilters == true" class="fa fa-caret-up"></i>
                </h4>
              </div>
            </div>
          </div>

          <div v-show="showFilters" class="scrolling" style="max-height: none">

            <div v-for="provider in [].concat(user.community_member ? [user.community_member] : [], assetManagerData, providerData)">
              <div class="clearfix provider-item">
                <div class="col-xs-9">
                  <a v-bind:href="'/sponsor/' + provider.id">
                    <strong class="upper">@{{provider.name}}</strong>
                  </a>
                  <br>
                  <span class="text-gray upper">@{{provider.type}}</span>
                </div>
                <div class="col-xs-3">
                  <a v-bind:href="'/sponsor/' + provider.id" class="btn btn-outline">View</a>
                </div>
              </div>
              <hr class="dotted">
            </div>
          </div>
        </div>
      </div>
      <div class="sidebar-filters col-sm-3" style="padding:0" v-if="user.user_type=='sponsor'">
        <div class="sidebar" style="max-height: none;">

          <div class="col-xs-12">
            <h4 class="label">Welcome back, @{{ user.community_member.name }}</h4>
          </div>

          <div class="scrolling" style="max-height: none">
            {{--
            <div v-for="provider in [user.community_member].concat(assetManagerData, providerData)">--}}
              <div v-for="provider in [user.community_member]">
                <div class="clearfix provider-item">
                  <div class="col-xs-9">
                    <a v-bind:href="'/sponsor/' + provider.id">
                      <strong class="upper">@{{provider.name}}</strong>
                    </a>
                    <br>
                    <span class="text-gray upper">@{{provider.type}}</span>
                  </div>
                  <div class="col-xs-3">
                    <a v-bind:href="'/sponsor/' + provider.id" class="btn btn-outline">View</a>
                  </div>
                </div>
                <hr class="dotted">
              </div>

              <connection-requests :communitymemberid="user.community_member.id" :isowner='true' :canedit="true" :isadmin="false" inline-template>
                <div>
                  <div v-if="requests.pending.total>0">
                    <div class="col-xs-12">
                      <p>You have @{{ requests.pending.total }} pending connection request.</p>
                      <a class="btn btn-primary" href="/settings#/connections">Action requests</a>
                    </div>
                  </div>
                </div>
              </connection-requests>


            </div>
          </div>
        </div>
        <div class="main col-md-9">
          {{-- Feed here --}}
          <feed :user="user" mode="feed" inline-template>
            <div class="feed-component-wrapper">
              <div class="col-xs-12">
                <h3>MY FEED</h3>
              </div>

              <div class="feed-nav clearfix pad-v">

                <div class="col-sm-8 col-xs-12">
                  <div class="typemenu">
                    <button v-for="v in feedtypes" v-bind:class="{'btn-primary': (feedActive===v), 'btn-factiva': v==='News' }" @click="feedActive=v"
                      class="btn btn-outline-blue lg">
                      @{{v}}
                      <span v-if="v==='News'">
                        <img style="height: 22px; margin-left: 6px;" src="/img/factiva.png" />
                      </span>
                    </button>
                  </div>
                </div>

                <div class="visible-xs col-xs-12 spacer-div"></div>

                <div class="col-sm-4 col-xs-12">
                  <input name="text" type="search" class="search-input form-control" placeholder="Keyword Search" v-model="keywordActive">
                </div>


              </div>

              <hr class="dotted">

              <div class="feed" v-show="!currentFacNews">
                <div v-show="loading==true">
                  <div class="loader-inner ball-pulse">
                    <div></div>
                    <div></div>
                    <div></div>
                  </div>
                </div>
                <div v-if="loading===false && pagination.total === 0 && feedActive === 'Updates'" class="alert alert-info">
                  <p>No updates have been posted here yet.</p>
                </div>
                <div v-show="loading==false" v-for="(item, idx) in feedData">
                  @include('feed.feed_item_stream')
                </div>
                @include('feed.feed_edit_modal')

              </div>
              @include('feed.feed_edit_fac_modal')
              <div v-if="currentFacNews">
                @include('feed.feed_fac_item_stream')
              </div>

            </div>

          </feed>

          @include('spark::nav.footer')

        </div>
      </div>
    </div>
</usernetwork>



@endsection