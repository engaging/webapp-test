@extends('spark::layouts.app')
@section('title', "| ".$portfolio->title)
@push('css')
<link href="/css/ion.rangeSlider.css" rel="stylesheet">
<link href="/css/ion.rangeSlider.skinHTML5.css" rel="stylesheet">
@endpush
@section('scripts')
<script src="/js/amcharts/amcharts.js"></script>
<script src="/js/amcharts/serial.js"></script>
<script src="/js/amcharts/radar.js"></script>
<script src="/js/amcharts/pie.js"></script>
<script src="/js/amcharts/xy.js"></script>
<script src="/js/amcharts/themes/light.js"></script>
@stop
@section('content')

<?php
  if (!isset($componentOptions)) {
    $componentOptions = [];
  }
  $componentOptions = json_encode((object)$componentOptions);
  if (!isset($templateOptions)) {
    $templateOptions = [];
  }
  if (!isset($templateOptions['chartOptions'])) {
    $templateOptions['chartOptions'] = [];
    for ($i = 0; $i < 6; $i++) {
      $templateOptions['chartOptions'][] = [
        'order' => $i,
        'options' => 'null',
      ];
    }
  }
?>

<portfolio
  :user="user"
  :portfolio='{!! str_ireplace("'", "&apos;", $portfolio) !!}'
  :options='{!! str_ireplace("'", "&apos;", $componentOptions) !!}'
  :periods={{ json_encode($periods) }}
  max-date={{ $maxDate }}
  inline-template>
<div>
  <div id="stickyheader" v-bind:class="{ 'affixed':stickyheader==true} ">
    <div class="clearfix bg-gray">
      <div class="container">
        <div class="row">
          <div class="col-sm-5 col-xs-12">
            <h4 class="upper ">@{{portfolio.title}}</h4>

            <div v-if="loadingPortfolio === false">
              <strong class="text-gray upper">
                <portfolio-composition :active-strategies="indices.length + '/' + (indices.length + indicesSuspended.length)" :allocation-model="weighting"
                  :allocation-labels="weightingOptions" :allocation-options="allocationOptions" pipe-class="pipe2" />
              </strong>
            </div>
          </div>
          <div class="col-sm-4 col-xs-12">
            <form id="pdfinput" target="_blank" rel="noopener noreferrer" action="/portfolio/{{$portfolio->slug}}/pdf" method="post">
              <input type="hidden" name="html"> {{ csrf_field() }}
            </form>
            
            <div class="dropdown" style="margin-right: 1em; display: inline-block;">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                  <button class="btn btn-outline-blue lg">
                    Analyse &amp; Design
                  </button>
              </a>
              <ul class="dropdown-menu" role="menu">
                  <li>
                    <div>
                      <a href="/analyse/portfolio/{{$portfolio->slug}}" class="btn btn-dropdown-menu lg">
                        <img src="/img/icons/icon-analysis.png" class="icon icon-md">&nbsp;&nbsp;Portfolio Analysis
                      </a>
                    </div>
                  </li>
                  <li v-if="showExtraTools" class="tooltip-container">
                    <div>
                      <a :class="{'disabled': !allowExtraTools}" href="/analyse/pca/portfolio/{{$portfolio->slug}}" class="btn btn-dropdown-menu lg">
                        <img src="/img/icons/icon-pca.png" class="icon icon-md">&nbsp;&nbsp;PCA
                      </a>
                    </div>
                    <div v-if="!allowExtraTools" class="tooltip-popover">
                      This function is for Premium User only. Please contact <a href="mailto:admin@engagingenterprises.co?subject=Upgrade%20to%20Premium%20Request">engagingenterprises</a> to upgrade your account.
                    </div>
                  </li>
                  <li v-if="showExtraTools" class="tooltip-container">
                    <div>
                      <a :class="{'disabled': !allowExtraTools}" href="/analyse/regression/portfolio/{{$portfolio->slug}}" class="btn btn-dropdown-menu lg">
                        <img src="/img/icons/icon-regression.png" class="icon icon-md">&nbsp;&nbsp;Regression
                      </a>
                    </div>
                    <div v-if="!allowExtraTools" class="tooltip-popover">
                      This function is for Premium User only. Please contact <a href="mailto:admin@engagingenterprises.co?subject=Upgrade%20to%20Premium%20Request">engagingenterprises</a> to upgrade your account.
                    </div>
                  </li>
              </ul>
            </div>

            <div class="dropdown" style="margin-right: 1em; display: inline-block;">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                    <button class="btn btn-outline-blue lg" :class="{ 'btn-caret-loading': loadingExport }">
                      <i class="fa fa-download fa-sm"></i>&nbsp;&nbsp;Export&nbsp;
                      <i class="fa fa-spinner fa-spin" v-if="loadingExport"></i>
                      <span class="caret caret-hidden" v-if="!loadingExport"></span>
                    </button>
                </a>
                <ul class="dropdown-menu" role="menu" v-if="!loadingExport">
                    <li>
                        <div>
                            <a download target="_blank" href="/portfolios/{{$portfolio->slug}}/export" class="btn btn-dropdown-menu lg">
                                <img src="/img/icons/excel.png" class="icon icon-md">&nbsp;&nbsp;Excel
                            </a>
                        </div>
                    </li>
                    <li>
                        <button @click="exportPdf('download')" class="btn btn-dropdown-menu lg">
                          <img src="/img/icons/pdf.png" class="icon icon-md">&nbsp;&nbsp;PDF
                        </button>
                    </li>
                    <li class="tooltip-container">
                        <button @click="exportPdf('symphony')" class="btn btn-dropdown-menu lg" :disabled="!user.hasSymphonyAccount">
                          <img src="/img/icons/symphony.png" class="icon icon-md">&nbsp;&nbsp;Symphony
                        </button>
                        <div v-if="!user.hasSymphonyAccount" class="tooltip-popover">
                          Link your Symphony account from <a href="/settings#/symphony">Your Settings</a> to use this service
                        </div>
                    </li>
                </ul>
            </div>

            <div class="visible-xs spacer-div"></div>
          </div>
          <div class="col-sm-3 col-xs-12">
            @include('partials.periodmenu2',array('class'=>'gray'))
          </div>
        </div>
      </div>
    </div>
  </div>

  <div id="print-header" class="clearfix print-header print-only">
    <div class="nav-blue navbar-inverse user" style="padding:10px 25px;">
      <div style="width:22%;float:left;" class="print-border-right">
        <div class="print-border-image" style="background-image:url({{Request::root()}}/img/design/logo.png);">
        </div>
      </div>
      <div style="width:40%;float:left;padding-left:27px;" class="pad">
        <h4 class="upper">@{{portfolio.title}}</h4>
        <strong class="upper">
          <portfolio-composition :active-strategies="indices.length + '/' + (indices.length + indicesSuspended.length)" :allocation-model="weighting"
            :allocation-labels="weightingOptions" :allocation-options="allocationOptions" pipe-class="pipe2" />
        </strong>
      </div>
      <div style="width:12%;float:left;text-align:right;" class="pad">
        <br>
      </div>
      <div style="width:22%;float:left;text-align:right;" class="pad">
        <div><strong>Start Date: @{{periodDates.from}}</strong></div>
        <div><strong>&nbsp;&nbsp;&nbsp;&nbsp;End Date: @{{periodDates.to}}</strong></div>
      </div>
    </div>
  </div>


  <div id="print-container">

    <div class="print-header"></div>

    <div class="container">
      <div class="col-lg-8 col-lg-offset-2">

        <div v-show="loadingPortfolio === true">
          <div class="loader-inner ball-pulse">
            <div></div>
            <div></div>
            <div></div>
          </div>
        </div>

        {{-- pie graphs --}}
        <div class="row pad-top" style="padding-top: 2px;">
          <div class="col-sm-6 col-xs-12 pie-large">
            <chart-pie :datasets="portfolio['assetClassWeights']" middletitle="ASSET CLASS" main="portfolio" legend="true" legendtop="350"
              radius="150" innerradius="80" idname="assetclass" :colors="assetClassColors"></chart-pie>

          </div>
          <div class="col-sm-6 col-xs-12 pie-large">
            <chart-pie :datasets="portfolio['factorWeights']" middletitle="FACTOR" main="portfolio" legend="true" legendtop="350" radius="150"
              innerradius="80" idname="factor" :colors="categoryColors"></chart-pie>

          </div>
        </div>



        {{-- benchmark stats --}}
        <div class="clearfix pad-md benchmarks" style="padding-top: 0px; padding-bottom: 10px;">
          <h4 class="print-border">
            PERFORMANCE SUMMARY
            <div class="tableoption pull-right" v-bind:class="{'opened':showBenchmarkMenu1==1}" data-var="showBenchmarkMenu1">
              <button @click="showBenchmarkMenu1=showBenchmarkMenu1*-1" class="btn">
                <span class="icon icon-cog"></span>
              </button>
              <div class="menu" v-show="showBenchmarkMenu1==1">
                <p>Add performance metrics (@{{tableCols_benchmarksMax - tableCols_benchmarks_show.length}} left)</p>
                <label v-for="(v,k) in labels" class="checkbox-style">
                  <input v-model="tableCols_benchmarks_show" v-bind:disabled="tableCols_benchmarks_show.indexOf(k) < 0 && tableCols_benchmarks_show.length >= tableCols_benchmarksMax"
                    type="checkbox" v-bind:value="k">
                  <i></i>
                  @{{v}}
                </label>
              </div>
            </div>
          </h4>

          <div class="datatable" style="position: relative">
            <div class="loading-layer" v-show="changeCheckBox">
                <div class="abs-loader">
                  <div class="loader-inner ball-pulse">
                    <div></div>
                    <div></div>
                    <div></div>
                  </div>
                </div>
            </div>
            <div class="data-row-parent">
              <div class="heading nopadding data-row">
                <div style="width:25%" class="namecol">
                  <button class="btn" @click="changeSortby_benchmark('title')">
                    Name
                  </button>
                </div>
                <div v-bind:style="{width: (74/tableCols_benchmarksCount)+'%'}" v-for="(field, idx) in tableCols_benchmarks_show" v-bind:class="{'highlight':periodKeysForFilters[period]+'_'+field ==tableCols_benchmarks_sortBy ,'bg-gray':idx%2==0}">
                  <button class="btn" @click="changeSortby_benchmark(periodKeysForFilters[period]+'_'+field)">
                    @{{labels[field]}}
                    <span v-show="tableCols_benchmarks_sortBy!=(periodKeysForFilters[period]+'_'+field)">
                      <i class="fa fa-sort"></i>
                    </span>
                    <span v-show="tableCols_benchmarks_sortBy==(periodKeysForFilters[period]+'_'+field)">
                      <i v-bind:class="{ 'fa-sort-up':(tableCols_benchmarks_sortOrder>0),'fa-sort-down':(tableCols_benchmarks_sortOrder<0) }" class="fa"></i>
                    </span>
                  </button>
                </div>
              </div>
            </div>

            <div class="clearfix">

              <div v-for="benchmark in benchmarksActiveData" class="data-row-parent">
                <div class="data-row">
                  <div style="width:25%">
                    {{-- the no-color benchmark is the portfolio that owns this page. cant remove it --}}
                    <div class="btn option" v-bind:class="{'text-white':benchmark.color!=''}" v-bind:style="{'background-color':colorsAll[benchmark.color]}">
                      <a v-show="(benchmark.color!='')" @click="removeBenchmark(benchmark)" class="pull-right">
                        <i class="fa fa-remove"></i>
                      </a>
                      <strong>@{{benchmark.title}}</strong>

                      {{--
                      <span v-show="benchmark.shortName!=undefined">
                        <br> @{{benchmark.assetClass}} | @{{benchmark.currency}} @{{benchmark.returnCategory}} @{{benchmark.returnType}}
                        @{{benchmark.volTarget}}
                      </span>--}}

                    </div>

                  </div>
                  <div v-bind:style="{width: (74/tableCols_benchmarksCount)+'%'}" class="text-center" v-for="(field, idx) in tableCols_benchmarks_show"
                    v-bind:class="{'highlight':periodKeysForFilters[period]+'_'+field ==tableCols_benchmarks_sortBy ,'bg-gray':idx%2==0}">
                    @{{ benchmark[periodKeysForFilters[period] + '_' + field]| round_4dp | unit(benchmark['um_' + field]) }}
                  </div>
                </div>
              </div>

              {{-- benchmark menu --}}
              <div class="data-row print-hide">
                <div style="width:25%">

                  <div class="tableoption pad-top" v-bind:class="{'opened':showBenchmarkMenu2==1}" data-var="showBenchmarkMenu2">
                    <button @click="showBenchmarkMenu2=showBenchmarkMenu2*-1" class="btn btn-gray form-control">
                      Compare
                      <i class="fa fa-plus pull-right"></i>
                    </button>
                    <div class="menu" v-show="showBenchmarkMenu2==1">
                      <p>Add benchmarks (@{{benchmarkLeftItems}} left)</p>

                      <strong>POPULAR BENCHMARKS</strong>
                      <label class="checkbox-style" v-for="option in benchmarks">
                        <input type="checkbox" v-bind:disabled="benchmarksActive.benchmarks.indexOf(option.id)<0 && benchmarkLeftItems<=0"
                          v-model="benchmarksActive.benchmarks" v-bind:value="option.id">
                        <i></i>
                        @{{ option.shortName }} (@{{ option.code }} )
                      </label>
                      <hr class="dotted white">
                      <strong>YOUR PORTFOLIOS</strong>
                      <label class="checkbox-style" v-for="option in filteredPortfolios">
                        <input type="checkbox" v-bind:disabled="benchmarksActive.portfolios.indexOf(option.id)<0 && benchmarkLeftItems<=0"
                          v-model="benchmarksActive.portfolios" v-bind:value="option.id">
                        <i></i>
                        @{{ option.title }}
                      </label>
                    </div>
                  </div>
                </div>
                <div style="width:25%">
                    <h5 v-if="benchmarksActiveMax-benchmarksActiveData.length>0" class="text-gray pad-top">(@{{benchmarksActiveMax-benchmarksActiveData.length}} left)</h5>
                </div>
              </div>

            </div>
          </div>


        </div>

        <div class="pos-rel">

          <div class="track animatelines" style="position: relative">

            <h4 class="print-border">
              TRACK RECORD
              <span class="pull-right date-disclaimer">As of @{{ asOf }}</span>
            </h4>

            <chart-track :datasets="benchmarksActiveData" :main="portfolio" :period="periodKeysForFilters[period]" title="" type="portfolio"
              :colors="colorsAll" :loading="changeCheckBox"></chart-track>

          </div>

        </div>

      </div>
    </div>

    <div class="print-header"></div>


    <div class="clearfix bg-gray pad-md lowercharts" transition="fade">

      <div class="clearfix bg-gray pad-md lowercharts" transition="fade">
        <div class="container">

          <div class="row">
            @include('partials.strategies.all_charts', ['pos' => 0, 'chart' => $templateOptions['chartOptions'][0], 'type'=>'portfolio'])
            @include('partials.strategies.all_charts', ['pos' => 1, 'chart' => $templateOptions['chartOptions'][1], 'type'=>'portfolio'])
          </div>
          <div class="row pad-top">
            @include('partials.strategies.all_charts', ['pos' => 2, 'chart' => $templateOptions['chartOptions'][2], 'type'=>'portfolio'])
            @include('partials.strategies.all_charts', ['pos' => 3, 'chart' => $templateOptions['chartOptions'][3], 'type'=>'portfolio'])
          </div>
          <div class="row pad-top">
            @include('partials.strategies.all_charts', ['pos' => 4, 'chart' => $templateOptions['chartOptions'][4], 'type'=>'portfolio'])
            @include('partials.strategies.all_charts', ['pos' => 5, 'chart' => $templateOptions['chartOptions'][5], 'type'=>'portfolio'])
          </div>

        </div>
      </div>
    </div>



    <div class="print-header"></div>


    <div class="container">
      <div class="row">
        <div class="col-lg-10 col-lg-offset-1">
          <div class="performance pad-md">
            <h4 class="print-border">
              MONTHLY RETURN (%)
            </h4>
            <div class="table-responsive">
              <table cellspacing="0" class="table pad-md">
                <thead>
                  <tr>
                    <td></td>
                    <td class="bg-gray">
                      Year
                    </td>
                    <td>
                      Jan
                    </td>
                    <td>
                      Feb
                    </td>
                    <td>
                      Mar
                    </td>
                    <td>
                      Apr
                    </td>
                    <td>
                      May
                    </td>
                    <td>
                      Jun
                    </td>
                    <td>
                      Jul
                    </td>
                    <td>
                      Aug
                    </td>
                    <td>
                      Sep
                    </td>
                    <td>
                      Oct
                    </td>
                    <td>
                      Nov
                    </td>
                    <td>
                      Dec
                    </td>
                  </tr>
                </thead>

                @foreach ($monthlyReturns as $year => $monthly_values)

                <tr>
                  <td>{{ $year }}</td>
                  <td class="bg-gray <?= (isset($monthly_values[0])&&$monthly_values[0]<0) ? " text-red ": " " ?>">
                    <?= (isset($monthly_values[0])) ? $monthly_values[0] : '-' ?>
                  </td>
                  @for ($i = 1; $i<=12; $i++)
                    <td class="<?= (isset($monthly_values[$i])&&$monthly_values[$i]<0) ? " text-red": " " ?>">
                        <?= (isset($monthly_values[$i])) ? $monthly_values[$i] : '-' ?>
                    </td>
                  @endfor
                </tr>
                @endforeach
              </table>
            </div>
          </div>
        </div>
        <div class="col-lg-8 col-lg-offset-2">
          <div class="performance">
            <h4 class="print-border">
              RISK METRICS
              <span class="date-disclaimer">As of @{{ maxDate }}</span>
              <div class="tableoption pull-right" v-bind:class="{'opened':showPerformanceMenu==1}">
                <button @click="showPerformanceMenu=showPerformanceMenu*-1" class="btn">
                  <span class="icon icon-cog"></span>
                </button>
                <div class="menu" v-show="showPerformanceMenu==1">
                  <p>Select time period</p>
                  <label v-for="(v,k) in tableCols_performance" v-if="portfolio[v + '_active'] > 0" class="checkbox-style">
                    <input type="checkbox" v-model="tableCols_performance_show" v-bind:value="k">
                    <i></i>
                    @{{k}}
                  </label>
                  <p>Add performance metrics (@{{tableRows_performanceMax - tableRows_performance_show.length}} left)</p>
                  <label v-for="(v,k) in labels" class="checkbox-style">
                    <input v-model="tableRows_performance_show" v-bind:disabled="tableRows_performance_show.indexOf(k) < 0 && tableRows_performance_show.length >= tableRows_performanceMax"
                      type="checkbox" v-bind:value="k">
                    <i></i>
                    @{{v}}
                  </label>
                </div>
              </div>
            </h4>
            <div class="datatable">
              <div class="data-row-parent">
                <div class="clearfix heading nopadding">
                  <div style="width:25%" class="namecol">
                    <span class="btn-noclick text-left">Measure</span>
                  </div>
                  <div v-bind:style="{width: (74/tableCols_performanceCount)+'%'}" v-for="(field, idx) in tableCols_performance_show" v-bind:class="{'bg-gray':idx%2==0}">
                    <span class="btn-noclick">@{{field}}</span>
                  </div>
                </div>
              </div>

              <div class="clearfix">
                <div v-for="measure in tableRows_performance_show" class="data-row-parent">
                  <div class="data-row">
                    <div style="width:25%" class="namerow">@{{ labels[measure] }}</div>
                    <div v-bind:style="{width: (74/tableCols_performanceCount)+'%'}" class="text-center" v-for="(fieldPeriod, idx) in tableCols_performance_show"
                      v-bind:class="{'bg-gray':idx%2==0}">
                      @{{ portfolio[tableCols_performance[fieldPeriod] + '_' + measure] | round_4dp | unit(portfolio['um_' + measure]) }}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="print-header"></div>

    <div class="container">
      <div class="row">

        <div class="col-lg-8 col-lg-offset-2">
          <div class="row strategyinfo pad-md" v-if="allocationOptions">



            <div class="col-xs-12">
              <h4 class="print-border">
                ALLOCATION MODEL
              </h4>
            </div>
            <div class="col-xs-12 col-sm-6">
              <div class="row">
                <div class="col-xs-6 text-gray">Model</div>
                <div class="col-xs-6">
                  <strong class="datatable" style="text-transform: capitalize;">@{{ weightingOptions[weighting] }}</strong>
                </div>
              </div>
              <div class="row" v-for="(labels, key) in allocationKeys1" v-if="weighting !== 'custom' && (allocationOptions[key] || labels.length > 2)">
                <div class="col-xs-6 text-gray">@{{labels[0]}}</div>
                <div class="col-xs-6">
                  <strong class="datatable">@{{ (allocationOptions[key] || labels[2]) + (labels.length > 1 ? labels[1] : '') }}</strong>
                </div>
              </div>
            </div>
            <div v-if="weighting !== 'custom'" class="col-xs-12 col-sm-6">
              <div>
                <hr class="visible-xs" />
                <div class="row">
                  <div class="col-xs-12">
                    <strong>Primary Allocation Constraint</strong>
                  </div>
                </div>
                <div v-if="['equal','manual'].indexOf(weighting) < 0 && allocationOptions.primary_allocation_constraint && allocationOptions.primary_allocation_constraint !== 'None'">
                  <div class="row">
                    <div style="border-bottom: 1px solid #eee;" class="col-xs-6">@{{ allocationOptions.primary_allocation_constraint }}</div>
                    <div style="border-bottom: 1px solid #eee;" class="col-xs-3">Min</div>
                    <div style="border-bottom: 1px solid #eee;" class="col-xs-3">Max</div>
                  </div>
                  <div class="row" v-for="(field, key) in (allocationOptions.allocationConstraints.primary.allocationsMax.length !== 0) ? allocationOptions.allocationConstraints.primary.allocationsMax : allocationOptions.allocationConstraints.primary.allocationsMin">
                    <div class="col-xs-6 text-gray">@{{key}}</div>
                    <div class="col-xs-3">
                      <strong>@{{ (allocationOptions.allocationConstraints.primary.allocationsMin[key] || '0') + '%' }}</strong>
                    </div>
                    <div class="col-xs-3">
                      <strong>@{{ (allocationOptions.allocationConstraints.primary.allocationsMax[key] || '100') + '%' }}</strong>
                    </div>
                  </div>
                </div>
                <div v-else>None</div>
              </div>
              <div>
                <hr />
                <div class="row">
                  <div class="col-xs-12">
                    <strong>Secondary Allocation Constraint</strong>
                  </div>
                </div>
                <div v-if="['equal','manual'].indexOf(weighting) < 0 && allocationOptions.secondary_allocation_constraint && allocationOptions.secondary_allocation_constraint !== 'None'">
                  <div class="row">
                    <div style="border-bottom: 1px solid #eee;" class="col-xs-6">@{{ allocationOptions.secondary_allocation_constraint }}</div>
                    <div style="border-bottom: 1px solid #eee;" class="col-xs-3">Min</div>
                    <div style="border-bottom: 1px solid #eee;" class="col-xs-3">Max</div>
                  </div>
                  <div class="row" v-for="(field, key) in (allocationOptions.allocationConstraints.secondary.allocationsMax !== 0) ? allocationOptions.allocationConstraints.secondary.allocationsMax : allocationOptions.allocationConstraints.secondary.allocationsMin">
                    <div class="col-xs-6 text-gray">@{{key}}</div>
                    <div class="col-xs-3">
                      <strong>@{{ (allocationOptions.allocationConstraints.secondary.allocationsMin[key] || '0') + '%' }}</strong>
                    </div>
                    <div class="col-xs-3">
                      <strong>@{{ (allocationOptions.allocationConstraints.secondary.allocationsMax[key] || '100') + '%' }}</strong>
                    </div>
                  </div>
                </div>
                <div v-else>None</div>
              </div>
            </div>
          </div>
          <div class="print-only" style="padding-bottom: 40px; padding-top: 40px;"></div>
          <div class="composition pad-md">
            @if (!$registerModal )
            <h4 class="print-border">
              COMPOSITION
              <span class="date-disclaimer">As of @{{ asOf }}</span>
              <div class="tableoption pull-right" v-bind:class="{'opened':showCompositionMenu==1}" data-var="showCompositionMenu">
                <button @click="showCompositionMenu=showCompositionMenu*-1" class="btn">
                  <span class="icon icon-cog"></span>
                </button>
                <div class="menu" v-show="showCompositionMenu==1">
                  <p>Select categories (@{{tableCols_compositionMax - tableCols_composition_show.length}} left)</p>
                  <div v-for="(v,k) in tableCols_composition">
                    <div v-if="!tableCols_composition_show_always.includes(k)">
                      <label class="checkbox-style">
                        <input type="checkbox" v-model="tableCols_composition_show" v-bind:value="k" v-bind:disabled="checkChoose(k)">
                        <i></i>
                        @{{k}}
                      </label>
                    </div>
                  </div>
                </div>
              </div>
            </h4>

            <!-- strategy table -->
            <div class="datatable">
              <div class="data-row-parent">
                <div class="clearfix heading nopadding">
                  <div style="width: 4%;"></div>
                  <div
                    v-for="(header, showedIdx) in tableCols_composition_show_ordered"
                    :class="{ 'highlight': (tableCols_composition_sortBy === 'indexinfo.'+tableCols_composition[header]) && header !== 'Name' }"
                    :style="{ width: (header !== 'Name') ? tableOtherColWidth + '%' : '40%' }">
                    <button
                      :class="{ 'text-left': showedIdx === 0 }"
                      class="btn"
                      @click="changeSortby_composition('indexinfo.'+tableCols_composition[header])">
                      @{{header}}
                      <span v-show="tableCols_composition_sortBy!='indexinfo.'+tableCols_composition[header]">
                        <i class="fa fa-sort"></i>
                      </span>
                      <span v-show="tableCols_composition_sortBy=='indexinfo.'+tableCols_composition[header]">
                        <i :class="{
		  								    	'fa-sort-up':(tableCols_composition_sortOrder>0),'fa-sort-down':(tableCols_composition_sortOrder<0)
		  								    	}" class="fa"></i>
                      </span>
                    </button>
                  </div>
                </div>
              </div>

              <div class="clearfix bg-gray">
                <div v-for="(index, idx) in sortedIndices" class="data-row-parent">
                  <div class="data-row">
                    <div style="width: 4%;">
                      <span class="text-gray">@{{idx+1}}</span>
                    </div>
                    <div
                      v-for="(header, showedIdx)  in tableCols_composition_show_ordered"
                      :class="{ 'highlight': (tableCols_composition_sortBy === 'indexinfo.'+tableCols_composition[header]) && header !== 'Name', 'text-center': showedIdx !== 0, 'text-left': showedIdx === 0 }"
                      :style="{ width: (header !== 'Name') ? tableOtherColWidth + '%' : '40%' }">
                      <div v-if="header === 'Name'">
                        <a :href="'/strategy/' + index.indexinfo.code">
                          @{{index.indexinfo[tableCols_composition[header]]}}
                        </a>
                        <p class="text-gray">
                          @{{ index.indexinfo.assetClass }} | @{{ index.indexinfo.currency }} @{{ index.indexinfo.returnCategory }}@{{ index.indexinfo.returnType_short
                          }} @{{ index.indexinfo.volTarget }}
                        </p>
                      </div>
                      <div v-else>
                        @{{ index.indexinfo[tableCols_composition[header]] }}@{{ index.indexinfo['um_' + tableCols_composition[header]] }}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- portfolio and funding table -->
            <div class="datatable">
              {{-- <div class="data-row-parent">
                <div class="clearfix heading nopadding">
                  <div style="width: 4%;"></div>
                  <div :style="{ width: (94 - 2 * tableOtherColWidth) + '%' }">
                    <span class="btn-noclick text-left">
                      Name
                    </span>
                  </div>
                  <div :style="{ width: tableOtherColWidth +'%' }">
                    <span class="btn-noclick">
                      Weight
                    </span>
                  </div>
                  <div :style="{ width: tableOtherColWidth +'%' }">
                    <span class="btn-noclick">
                      Allocation
                    </span>
                  </div>
                </div>
              </div> --}}

              <div class="clearfix">
                <div class="data-row-parent" style="background: #F7F8FA;">
                  <div class="data-row">
                    <div style="width: 4%;"></div>
                    <div
                      :style="{ width: (94 - tableOtherColWidth * 2) +'%' }"
                      class="text-left">
                      <div>
                        <a href="/portfolios/{{ $portfolio->slug }}">
                          Portfolio
                        </a>
                        <p class="text-gray" style="white-space:nowrap;">
                          <portfolio-composition :active-strategies="indices.length" :allocation-model="weighting" :allocation-labels="weightingOptions"
                            :allocation-options="allocationOptions" pipe-class="pipe2" />
                        </p>
                      </div>
                    </div>
                    <div
                      :style="{ width: tableOtherColWidth +'%' }"
                      class="text-center">
                      <div>
                        @{{ portfolioDetails.portfolioWeights }}%
                      </div>
                    </div>
                    <div
                      :style="{ width: tableOtherColWidth +'%' }"
                      class="text-center">
                      <div>
                        @{{ portfolioDetails.portfolioAllocations }}
                      </div>
                    </div>
                  </div>
                </div>
                <div class="data-row-parent" style="background: #F7F8FA;">
                  <div
                    v-if="weighting === 'custom'"
                    class="data-row">
                    <div style="width: 4%;"></div>
                    <div
                      :style="{ width: (94 - tableOtherColWidth * 2) +'%' }"
                      class="text-left">
                      <div v-if="funding">
                        <a :href="'/strategy/' + funding.code">
                          Funding
                        </a>
                        <p class="text-gray">
                          Cash | @{{ funding.currency }} | @{{ funding.code }}
                        </p>
                      </div>
                      <div v-else>
                        <a>
                            Funding
                          </a>
                          <p class="text-gray">
                            Cash | Manual
                          </p>
                      </div>
                    </div>
                    <div
                      :style="{ width: tableOtherColWidth +'%' }"
                      class="text-center">
                      <div>
                        @{{ portfolioDetails.fundingWeights }}%
                      </div>
                    </div>
                    <div
                      :style="{ width: tableOtherColWidth +'%' }"
                      class="text-center">
                      <div>
                        @{{ portfolioDetails.fundingAllocations }}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            @endif


          </div>
        </div>
      </div>
    </div>


    <div class="footer">
      <div class="container disclaimer pad-top" v-bind:class="user.disclaimer && 'print-hide'">
        {!!$disclaimer!!}
      </div>
      <div class="container disclaimer pad-top print-only">
        @{{user.disclaimer}}
      </div>
    </div>
  </div>

  @if ($registerModal )
  <div id="registermodal_fixed">
    <div class="container bg-gray text-center">
      <h4 class="text-navy">
        <strong>LOG IN NOW TO ACCESS MORE USEFUL FEATURES</strong>
      </h4>
      <p></p>
      <div class="clearfix">
        <a href="/request" class="btn btn-primary">REGISTER</a>
        <a data-toggle="modal" data-target=".login-modal" href="#" class="btn btn-outline-blue">LOG IN</a>
      </div>
    </div>
  </div>
  @endif
  @include('spark::nav.footer')
</div>
</portfolio>
@endsection
