<div class="clearfix feed-item" v-bind:class="{'grayin':idx==0 && item.new }" v-if="item.type=='Updates'">
  <div class="col-xs-4 col-sm-2" v-if="item.provider">
    <a v-bind:href="'/sponsor/' + item.provider.id" class="provider-img">
      <img :src="item.provider.image" class="img-responsive" alt>
    </a>
  </div>
  <div class="col-sm-10 col-xs-8">
    <div class="row">
      <div class="col-xs-6">
        <a v-bind:href="'/sponsor/' + item.provider.id">
          <strong class="upper">@{{item.provider.name}}</strong>
        </a>
      </div>
      <div class="col-xs-6 text-right">
        <span class="text-gray">@{{item.created_at | toDate}}</span>
        <button v-if="item.canedit" type="button" style="padding:0 10px" @click="editPost(item)" class="btn text-navy pull-right">
          <i class="fa fa-pencil"></i>
        </button>
      </div>
    </div>
    <div class="clearfix title-row">
      <div class="col-xs-1" v-if="item.type=='News'">
        <img src="/img/design/icon-news.png" alt="News" class="img-responsive">
      </div>
      <div v-bind:class="{ 'col-xs-11':item.type=='News','col-xs-12':item.type!='News' }">
        <a :href="item.url">
          <h4>
            <strong class="upper">@{{item.title}}</strong>
          </h4>
        </a>
      </div>
    </div>
    <p>
      <span v-html="item.description.slice(0,400)"></span>
      <span v-if="item.description.length>400">...</span>
    </p>

    <div v-if="item.image && item.image.length">
      <a :href="item.url">
        <img :src="item.image" :alt="item.title" class="img-responsive">
      </a>
    </div>
  </div>
</div>
<div class="clearfix feed-item" v-bind:class="{'grayin':idx==0 && item.new }" v-if="item.type=='News'||item.type=='FacNews'">
  <div class="hidden-xs col-sm-2">
    <a :href="item.url" class="provider-img" v-if="item.type=='News'">
      <img :src="item.logo" class="img-responsive" alt>
    </a>
    <a @click="showFacNews(item.id)" class="provider-img" v-if="item.type=='FacNews'" v-html="item.logo"></a>
  </div>
  <div class="col-sm-10 col-xs-12">
    <div class="row">
      <div class="visible-xs col-xs-6 col-xs-offset-3 col-sm-offset-0">
        <a :href="item.url" class="provider-img" v-if="item.type=='News'">
          <img :src="item.logo" class="img-responsive" alt>
        </a>
        <a @click="showFacNews(item.id)" class="provider-img" v-if="item.type=='FacNews'" v-html="item.logo"></a>
        <hr />
      </div>
      <div class="col-sm-6 col-xs-12 text-xs-center">
        <a :href="item.url" v-if="item.type=='News'">
          <strong class="upper">@{{item.source}}</strong>
        </a>
        <a @click="showFacNews(item.id)" v-if="item.type=='FacNews'">
          <strong class="upper">@{{item.source}}</strong>
        </a>
      </div>
      <div class="col-sm-6 col-xs-12 text-right  text-xs-center">
        <span class="text-gray">@{{item.created_at | toDate}}</span>
        <button v-if="item.canedit && item.type==='News'" type="button" style="padding:0 10px" @click="editPost(item)" class="btn text-navy pull-right">
          <i class="fa fa-pencil"></i>
        </button>
        <button v-if="user.user_type==='admin' && item.type==='FacNews'" type="button" style="padding:0 10px" @click="editFacPost(item)"
          class="btn text-navy pull-right">
          <i class="fa fa-pencil"></i>
        </button>
      </div>
    </div>
    <div class="clearfix title-row">
      <div class="col-xs-1 hidden-xs" v-if="item.type=='News'||item.type=='FacNews'">
        <img src="/img/design/icon-news.png" alt="News" class="img-responsive">
      </div>
      <div v-bind:class="{ 'col-xs-12 col-sm-11':item.type=='News'||item.type=='FacNews','col-xs-12':item.type!='News' }">
        <a :href="item.url" v-if="item.type=='News'">
          <h4>
            <strong class="upper" v-if="item.type=='News'">@{{item.title}}</strong>
          </h4>
        </a>
        <a @click="showFacNews(item.id)" v-if="item.type=='FacNews'">
          <h4>
            <strong class="upper" v-if="item.type=='FacNews'" v-html="item.title"></strong>
          </h4>
        </a>
      </div>
    </div>
    <p>
      <span v-html="item.description.slice(0,400)"></span>
      <span v-if="item.description.length>400">...</span>
    </p>

    <div v-if="item.image && item.image.length && item.type=='News'">
      <a :href="item.url">
        <img :src="item.image" :alt="item.title" class="img-responsive">
      </a>
    </div>
    <div v-if="item.image && item.type=='FacNews'">
      <a @click="showFacNews(item.id)">
        <img :src="item.image" :alt="item.title" class="img-responsive">
      </a>
    </div>
  </div>
</div>
<hr class="dotted">
