{{--  FEED MODAL --}}
<div class="modal fade editmodal" id="edit_updates" tabindex="-1" role="dialog" v-if="itemBeingEdited">
	<div v-show="canedit || (itemBeingEdited && itemBeingEdited.canedit)"  class="modal-dialog network" role="document">
		<div class="modal-content">
			<div class="modal-header" v-if="itemBeingEdited.type == 'Updates'">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" v-show="itemBeingEdited==null">Add New Update</h4>
				<h4 class="modal-title" v-show="itemBeingEdited!=null">Edit your Update</h4>
			</div>
			<div class="modal-header" v-if="itemBeingEdited.type == 'News'">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" v-show="itemBeingEdited==null">Add New News Item</h4>
				<h4 class="modal-title" v-show="itemBeingEdited!=null">Edit News Item</h4>
			</div>
			<div class="modal-body">
				<div class="modal-top">

					<div class="clearfix" v-if="itemBeingEdited!=null && itemBeingEdited.published == 1" style="margin-bottom: 30px">
						<small>Originally posted on @{{itemBeingEdited.created_at}}</small>
					</div>

					<div class="row">
						<div class="clearfix">

							<div class="col-sm-2" v-if="itemBeingEdited.type != 'News'">
								<a v-if="itemBeingEdited==null && provider" :href="'/sponsor/' + provider.id" class="provider-img"><img :src="provider.image"  class="img-responsive" alt></a>
								<a v-if="itemBeingEdited!=null && itemBeingEdited.provider" :href="'/sponsor/' + itemBeingEdited.provider.id" class="provider-img"><img :src="itemBeingEdited.provider.image"  class="img-responsive" alt></a>
							</div>
							<div class="col-sm-2" v-if="itemBeingEdited.type == 'News'">
								<a class="provider-img"><img :src="itemBeingEdited.logo"  class="img-responsive" alt></a>
							</div>
							<div class="col-sm-10">
								<div class="form-group">
									<label class="label">TITLE</label>
									<input type="text" placeholder="Title" class="form-control" v-model="newPostTitle">
								</div>
								<div class="form-group" v-if="itemBeingEdited.type == 'News'">
									<label class="label">SOURCE</label>
									<input type="text" placeholder="Source" class="form-control" v-model="newPostSource">
								</div>
								<div class="form-group">
									<label class="label">DESCRIPTION</label>

									{{-- <textarea placeholder="Description" class="form-control" v-model="newPostDescription"></textarea> --}}

									{{-- test richtext --}}
									@include('partials.richtext',array('divid'=>'description-editor'))
									<div class="richtext-editor" id="description-editor" v-html="newPostDescription"></div>
									{{-- endtest --}}

								</div>

								<div class="form-group" v-if="itemBeingEdited.type == 'News'">
									<label class="label">FULL ARTICLE</label>

									@include('partials.richtext',array('divid'=>'full_article-editor'))
									<div class="richtext-editor" id="full_article-editor" v-html="full_article"></div>

								</div>

								<div class="bg-gray">
									<div class="filedrop">
											<div v-show="newPostImage">
												<img :src="newPostImage" class="img-responsive" style="width: 100%">
												<a v-show="newPostImage" class="btn btn-outline" @click="newPostImage=false">Remove</a>
											</div>
											<div v-show="!newPostImage || newPostImage.length==0">
												<label>
													<h4>DROP IMAGE HERE OR CLICK TO UPLOAD</h4>
													<p>JPG, PNG or GIF file. Max file size 2MB.</p>
													<input type="file" @change="imagePreview" class="form-control">
												</label>
											</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="clearfix" style="margin-top: 30px;">
							<div class="col-sm-3" v-show='itemBeingEdited!=null'>
								<button class="btn btn-warning form-control submit pull-right" @click="removePost()">DELETE</button>
							</div>
							<div class="col-sm-6  pull-right">
								<button class="btn btn-primary form-control submit pull-right" @click="sharePost()">SAVE</button>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>
