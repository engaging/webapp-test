{{--  FEED MODAL --}}
<div class="modal fade editmodal" id="edit_fac_updates" tabindex="-1" role="dialog" v-if="itemBeingEdited">
	<div   class="modal-dialog network" role="document">
		<div class="modal-content">
			<div class="modal-header" v-if="itemBeingEdited.type == 'Updates'">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" v-show="itemBeingEdited==null">Add New Update</h4>
				<h4 class="modal-title" v-show="itemBeingEdited!=null">Edit your Update</h4>
			</div>
			<div class="modal-header" >
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" v-show="itemBeingEdited==null">Add New News Item</h4>
				<h4 class="modal-title" v-show="itemBeingEdited!=null">Edit News Item</h4>
			</div>
			<div class="modal-body">
				<div class="modal-top">

					<div class="clearfix" v-if="itemBeingEdited!=null && itemBeingEdited.published == 1" style="margin-bottom: 30px">
						<small>Originally posted on @{{itemBeingEdited.created_at}}</small>
					</div>

					<div class="row">
						<div class="clearfix">
							
			
							<div class="col-sm-2" >
								<a class="provider-img" v-html="itemBeingEdited.logo"></a>
							</div>
							<div class="col-sm-10">
								<div class="form-group">
									<label class="label">TITLE</label>
									<div v-html="itemBeingEdited.title"></div>
								</div>
								<div class="form-group" >
									<label class="label">SOURCE</label>
									<p>@{{itemBeingEdited.source}}</p>
								</div>
								<div class="form-group">
									<label class="label">DESCRIPTION</label>
									<div v-html="itemBeingEdited.description"></div>


								</div>
								
								<div class="form-group" >
									<label class="label">FULL ARTICLE</label>

									<div v-html="itemBeingEdited.full_article"></div>

								</div>

								<div class="bg-gray">
									<div class="filedrop">
											<div v-show="newPostImage">
												<img :src="newPostImage" class="img-responsive" style="width: 100%">
												<a v-show="newPostImage" class="btn btn-outline" @click="newPostImage=false">Remove</a>
											</div>
											<div v-show="!newPostImage || newPostImage.length==0">
												<label>
													<h4>DROP IMAGE HERE OR CLICK TO UPLOAD</h4>
													<p>JPG, PNG or GIF file. Max file size 2MB.</p>
													<input type="file" @change="imagePreview" class="form-control">
												</label>
											</div>
									</div>
								</div>



							</div>
						</div>
					</div>
					<div class="row">
						<div class="clearfix" style="margin-top: 30px;">

							<div class="col-sm-6  pull-right">
								<button class="btn btn-primary form-control submit pull-right" @click="saveFacPhoto()">SAVE</button>
							</div>
						</div>
					</div>
					
				</div>
			</div>
		</div>
	</div>
</div>
