<div class="clearfix feed-item" v-bind:class="{'grayin': currentFacNews.new }" v-if="currentFacNews.type=='News'||currentFacNews.type=='FacNews'">
  <div class="col-sm-12">
    <a @click="closeFacNew()" class="back_btn">Back to News Feed</a>
  </div>
  <div class="hidden-xs col-sm-2">
    <div v-html="currentFacNews.logo"></div>
  </div>
  <div v-bind:class="{ 'col-sm-10 col-xs-12':currentFacNews.type=='News','col-sm-10 col-xs-12':currentFacNews.type=='FacNews' }">
    <div class="row">
      <div class="visible-xs col-xs-6 col-xs-offset-3 col-sm-offset-0">
        <div v-html="currentFacNews.logo"></div>
        <hr />
      </div>
      <div class="col-sm-6 col-xs-12 text-xs-center">
        <div>
          <span class="text-gray">@{{currentFacNews.created_at}}</span>
        </div>

      </div>
      <div class="col-sm-6 col-xs-12 text-right  text-xs-center">

        <button v-if="user.user_type==='admin' && currentFacNews.type==='FacNews'" type="button" style="padding:0 10px" @click="editFacPost(currentFacNews)"
          class="btn text-navy pull-right">
          <i class="fa fa-pencil"></i>
        </button>
      </div>
    </div>
    <div class="clearfix title-row">
      <a :href="currentFacNews.url" v-if="currentFacNews.type=='News'">
        <strong class="upper">@{{currentFacNews.source}}</strong>
      </a>
      <a v-if="currentFacNews.type=='FacNews'">
        <strong class="upper">@{{currentFacNews.source}}</strong>
      </a>
      <div v-bind:class="{ 'col-xs-12 col-sm-12':currentFacNews.type=='News'||currentFacNews.type=='FacNews','col-xs-12':currentFacNews.type!='News' }">
        <a :href="currentFacNews.url" v-if="currentFacNews.type=='News'">
          <h4>
            <strong class="upper" v-if="currentFacNews.type=='News'">@{{currentFacNews.title}}</strong>
            <strong class="upper" v-if="currentFacNews.type=='FacNews'" v-html="currentFacNews.title"></strong>
          </h4>
        </a>
        <a v-if="currentFacNews.type=='FacNews'">
          <h4>
            <strong class="upper" v-if="currentFacNews.type=='News'">@{{currentFacNews.title}}</strong>
            <strong class="upper" v-if="currentFacNews.type=='FacNews'" v-html="currentFacNews.title"></strong>
          </h4>
        </a>
      </div>
    </div>
  </div>
  <div class="col-sm-12">
    <hr class="dotted">
    <div v-if="currentFacNews.image ">
      <a :href="currentFacNews.url">
        <img :src="currentFacNews.image" :alt="currentFacNews.title" class="img-responsive">
      </a>
    </div>
    <hr class="dotted" v-if="currentFacNews.image">
    <div v-html="currentFacNews.article"></div>


    <p>&nbsp;</p>
    <div class="clearfix back">
      <a @click="closeFacNew()" class="btn btn-outline-blue lg">Back to News Feed</a>
    </div>
  </div>
</div>
<hr class="dotted">