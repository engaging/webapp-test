@extends('spark::layouts.app') 
@section('title', '| ' . $item->title ) 
@section('scripts')
<link rel="stylesheet" type="text/css" href="css/cropper.css" />
@stop
@section('content')

<feed :user="user" :provider='{!! str_ireplace("'", "&apos;", $item->provider) !!}' :items='[{!! str_ireplace("'", "&apos;", $item) !!}]'  mode="article" 
  @if ($canedit> 0) 
  :canedit="true" 
  @else 
  :canedit="false" 
  @endif 
  inline-template>
  <div>
    <div class="container">
      <div class="row feed-item">
        <div class="col-sm-12 main">


          <div v-if="saving">
            <div class="loader-inner ball-pulse">
              <div></div>
              <div></div>
              <div></div>
            </div>
          </div>

          <div v-if="!saving" class="clearfix row">

            <div class="col-sm-8 col-sm-offset-2" v-if="item.type == 'Updates'">
              <div class="author row">

                <div class="col-sm-12">
                  <a href="/community" class="back_btn">Back to News Feed</a>
                </div>
                <div class="col-sm-2">
                  <a v-bind:href="'/sponsor/' + item.provider.id" class="provider-img">
                    <img :src="item.provider.image" class="img-responsive" alt>
                  </a>
                </div>
                <div class="col-sm-10">
                  <button v-if="canedit" type="button" style="padding:0 10px" @click="editPost(item)" class="btn text-navy pull-right">
                    <i class="fa fa-pencil text-gray"></i>
                  </button>
                  <span class="text-gray">@{{item.created_at}}</span>
                  <br>
                  <a v-bind:href="'/sponsor/' + item.provider.id">
                    <strong class="upper">@{{item.provider.name}}</strong>
                  </a>
                  <h4 class="text-upper text-navy" style="font-weight:normal">@{{item.title}}</h4>
                </div>

              </div>

              <hr class="dotted">

              <div v-if="item.image && item.image.length">
                <img :src="item.image" :alt="item.title" class="img-responsive">
              </div>
              <p v-html="item.description"></p>


              <div class="clearfix back">
                <a href="/community" class="btn btn-outline-blue lg">Back to News Feed</a>
              </div>
            </div>

            <div class="col-sm-8 col-sm-offset-2" v-if="item.type == 'News'">

              <div class="author row">

                <div class="col-sm-12">
                  <a href="/community" class="back_btn">Back to News Feed</a>
                </div>

                <div class="col-sm-2">
                  <div class="provider-img">
                    <img :src="item.logo" class="img-responsive" alt>
                  </div>
                </div>
                <div class="col-sm-10">
                  <button v-if="canedit" type="button" style="padding:0 10px" @click="editPost(item)" class="btn text-navy pull-right">
                    <i class="fa fa-pencil text-gray"></i>
                  </button>
                  <span class="text-gray">@{{item.created_at}}</span>
                  <br>
                  <a v-bind:href="item.url">
                    <strong class="upper">@{{item.source}}</strong>
                  </a>
                  <h4 class="text-upper text-navy" style="font-weight:normal">@{{item.title}}</h4>
                </div>

              </div>

              <hr class="dotted">

              <div v-if="item.image && item.image.length">
                <img :src="item.image" :alt="item.title" class="img-responsive">
              </div>

              <hr class="dotted" v-if="item.image && item.image.length">

              <div v-html="item.full_article"></div>

              <div class="clearfix back">
                <a href="/community" class="btn btn-outline-blue lg">Back to News Feed</a>
              </div>
            </div>


          </div>

        </div>
      </div>
    </div>
    @include('feed.feed_edit_modal')
  </div>

</feed>

@include('spark::nav.footer') 
@endsection