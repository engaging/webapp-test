<div class="clearfix feed-item"  v-bind:class="{'grayin':idx==0 && item.new }">
	<div class="col-xs-1">
		<a v-bind:href="'/sponsor/' + item.provider.id" class="provider-img"><img :src="item.provider.image"  class="img-responsive" alt></a>
	</div>
	<div class="col-xs-11">
		<div class="row">
			<div class="col-xs-6">
				<a v-bind:href="'/sponsor/' + item.provider.id"><strong class="upper">@{{item.provider.name}}</strong></a>
			</div>
			<div class="col-xs-6 text-right">
				<span class="text-gray">@{{item.date}}</span>
				<button v-if="item.canedit" type="button" style="padding:0 10px" @click="editPost(item)" class="btn text-gray pull-right"><i class="fa fa-pencil"></i></button>
			</div>
		</div>
		<div class="clearfix title-row">
			<div class="col-xs-1" v-if="item.type=='News'">
				<img src="/img/design/icon-news.png" alt="News" class="img-responsive">
			</div>
			<div v-bind:class="{ 'col-xs-11':item.type=='News','col-xs-12':item.type!='News' }" >
				<a :href="item.url"><h4 class="upper" v-html="item.title"></h4></a>
			</div>
		</div>
		<p>
      <span v-html="item.description.slice(0,400)"></span>
      <span v-if="item.description.length>400">...</span>
    </p>

		<div v-if="item.image && item.image.length">
			<a :href="item.url"><img :src="item.image" :alt="item.title" class="img-responsive"></a>
		</div>
	</div>
</div>
<hr class="dotted">
