@extends('spark::layouts.app')
@section('title', '| Analysing Portfolios')
@push('css')
<link href="/css/bootstrap-slider.min.css" rel="stylesheet">
<link href="/css/ion.rangeSlider.css" rel="stylesheet">
<link href="/css/ion.rangeSlider.skinHTML5.css" rel="stylesheet">
@endpush
@push('js')

<script src="/js/bootstrap-slider.min.js"></script>
@endpush
@section('scripts')
<script src="/js/amcharts/amcharts.js"></script>
<script src="/js/amcharts/serial.js"></script>
<script src="/js/amcharts/amstock.js"></script>
<script src="/js/amcharts/radar.js"></script>
<script src="/js/amcharts/pie.js"></script>
<script src="/js/amcharts/xy.js"></script>
<script src="/js/amcharts/gantt.js"></script>
<script src="/js/amcharts/themes/light.js"></script>
@stop
@section('content')

<lab-container
  :user="user"
  init-selected={{$mode}}
  :init-analysing='{{$analysing}}'
  :init-periods={{ json_encode($periods) }}
  init-max-date={{ $maxDate }}
></lab-container>

  @include('spark::nav.footer')

  @endsection
