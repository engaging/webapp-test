@extends('spark::layouts.app')
@section('title', '| Dashboard')
@section('scripts')
<script src="/js/amcharts/amcharts.js"></script>
<script src="/js/amcharts/serial.js"></script>
<script src="/js/amcharts/pie.js"></script>
<script src="/js/amcharts/themes/light.js"></script>
<script src="/js/amcharts/xy.js"></script>
<script src="/js/amcharts/radar.js"></script>

<script src="test-d3/bubbles_files/CustomTooltip.js"></script>
<script src="test-d3/bubbles_files/d3.min.js"></script>
<script src="test-d3/bubbles_files/d3.csv.min.js"></script>
<script src="test-d3/bubbles_files/d3.layout.min.js"></script>
<script src="test-d3/bubbles_files/d3.geom.min.js"></script>
<script type="text/javascript" src="test-d3/bubbles_files/vis.js"></script>

@stop
@push('css')
<link href="/css/bootstrap-slider.min.css" rel="stylesheet">
@endpush
@push('js')
<script src="/js/bootstrap-slider.min.js"></script>
@endpush
@section('footer-scripts')
@stop
<style type="text/css">
  body {
    background: #F0F1F3 !important;
  }
</style>
@section('content')

<widgets :user="user" :widget-data-initial='{!! str_ireplace("'", "&apos;", $widgetData) !!}'
	:widget-layout-initial='{!! str_ireplace("'", "&apos;", $widgetLayoutInitial) !!}' inline-template>
  <div class="container">

    <div class="clearfix widget-grid small-grid">
      <grid-layout :layout.sync="widgetData_layout" :col-num="4" :row-height="130" :is-draggable="draggable" :is-resizable="resizeable"
        :vertical-compact="true" :margin="[25, 25]" :use-css-transforms="true">

        <grid-item v-for="(block, idx) in widgetData_layout" :x.sync="block.x" :y.sync="block.y" :orginal-w.sync="block.w" :orginal-h.sync="block.h" :key="block.uniqueKey"
          :i="block.i" v-if="widgetData[idx] && widgetData[idx]['componenttype']" v-bind:class="['height'+ block.h,'width'+block.w,'type-'+widgetData[idx]['componenttype']]">
          <div v-if="widgetData[idx]" class="grid-item-inner">
            <div v-if="block.type==='default'" class="default-menu">
              <div class="clearfix">
                <div class="row">
                  <div class="col-xs-4 text-gray">
                    <div class="text-center" style="font-size: 120px;line-height: 50px;margin-top: 20px;">+</div>
                    <h4>ADD A WIDGET</h4>
                  </div>
                  <div class="col-xs-4">
                    <button v-bind:class="{'selected':hasNews}" @click="toggleWidget('news')" class="btn bg-white form-control">
                      <img src="/img/design/icon-widget-news.png">
                      <span>News</span>
                    </button>
                  </div>
                  <div class="col-xs-4">
                    <button v-bind:class="{'selected':hasFeed}" @click="toggleWidget('feed')" class="btn bg-white form-control">
                      <img src="/img/design/icon-feed.png">
                      <span>Feed</span>
                    </button>
                  </div>

                  <div class="col-xs-4">
                    <button @click="widgetModal('charts')" class="btn bg-white form-control">
                      <img src="/img/design/icon-widget-chart.png">
                      <span>Chart</span>
                    </button>
                  </div>
                  <div class="col-xs-4">
                    <button @click="widgetModal('table')" class="btn bg-white form-control">
                      <img src="/img/design/icon-widget-table.png">
                      <span>Watchlist</span>
                    </button>
                  </div>
                  <div class="col-xs-4">
                    <button @click="widgetModal('stat')" class="btn bg-white form-control">
                      <img src="/img/design/icon-widget-stat.png">
                      <span>Data</span>
                    </button>
                  </div>
                </div>
              </div>
            </div>
            <widget-block :widget-data="widgetData" :idx="idx" :user="user" :block="block" :portfolio-loaded="portfolioLoaded" :strategies-loaded="strategiesLoaded"
              :period="widgetData[idx]['period']" :properties="widgetData[idx]['properties']" :track-loaded="trackLoaded" :volatility-loading="volatilityLoading[idx]" inline-template>
              <div v-if="block.type!='default' && widgetData[idx]!=undefined" class="clearfix item" :class="'block-'+widgetData[idx]['componenttype']">
                {{-- Menu bar --}}
                <div class="clearfix widget-menu">
                  {{-- TOP selectperiod : visible for width=2 and nonfeed, nonnews --}}
                  <div v-if="widgetData[idx]['componenttype']!='feed' && widgetData[idx]['componenttype']!='news'" class="periodselect-top">

                    <div class="periodmenu-dropdown gray">
                      <div class="dropdown">
                        <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                          @{{widgetData[idx]['period']}}
                          <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                          <li v-for="(v,k) in periodKeysForFilters">
                            <a @click="widgetData[idx]['period']=v">@{{v}}</a>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>

                  <div>
                    <div class="tableoption float pull-right" v-bind:class="{'opened':widgetData[idx]['showMenu']}" :data-var="idx" v-if="showEditMenu(widgetData[idx]['componenttype'])">
                      <button @click="showMenu(widgetData[idx])" class="btn">
                        <img src="/img/design/icon-menu.png" width="20" height="15" alt>
                      </button>
                      <div class="menu" v-show="widgetData[idx]['showMenu']">

                        <a v-if="widgetData[idx]['properties'] && widgetData[idx]['properties']['main'] && widgetData[idx]['componenttype']=='stat' && widgetData[idx]['properties']['main']['type'] != 'portfolio'"
                          target="_blank" rel="noopener noreferrer" :href="'/strategy/'+widgetData[idx]['properties']['main']['id']">
                          <i style="margin:4px;" class="fa fa-external-link"></i> Open Factsheet
                        </a>

                        <a v-if="widgetData[idx]['properties'] && widgetData[idx]['properties']['main'] && widgetData[idx]['componenttype']=='stat' && widgetData[idx]['properties']['main']['type'] == 'portfolio'"
                          target="_blank" rel="noopener noreferrer" :href="'/portfolios/'+widgetData[idx]['properties']['main']['id']">
                          <i style="margin:4px;" class="fa fa-external-link"></i> Open Factsheet
                        </a>

                        <a @click="editWidgetModal(idx)" v-if="allowEdit(widgetData[idx]['componenttype'])">
                          <i style="margin:4px;" class="fa fa-pencil"></i> Edit
                        </a>

                        <a class="hidden-xs" @click="showTable=false" v-if="widgetData[idx]['componenttype'] == 'bubble'">
                          <i style="margin:4px;" class="fa fa-bar-chart"></i> Chart
                        </a>
                        <a @click="showTable=true" v-if="widgetData[idx]['componenttype'] == 'bubble'">
                          <i style="margin:4px;" class="fa fa-table"></i> Table
                        </a>


                        <a @click="removeWidget(idx)" v-if="allowDelete(widgetData[idx]['componenttype'])">
                          <img src="/img/design/icon-remove-white.png" alt> Remove
                        </a>
                      </div>
                    </div>
                  </div>
                  <div>
                    <div class="move" v-if="allowMoveAndResize(widgetData[idx]['componenttype'])">
                      <img class="vue-grid-handle" width="20" height="20" src="/img/design/icon-move.png" style="margin:8px;cursor:pointer;" alt="Move">
                    </div>
                  </div>
                </div>

                {{-- main content/graph --}}

                <div v-if="widgetData[idx]['componenttype']=='bubble'">
                    @include('partials.marketmonitor')
                </div>

                <div v-if="widgetData[idx]['componenttype']=='news'">
                  <h4 class="charttitle">@{{widgetData[idx]['title']}}</h4>
                  <div class="scrolling">
                    <feed :user="user" mode="news" inline-template>
                      <div v-show="loading==false">
                        <div class="news-item" v-for="item in feedData">
                          <div class="clearfix">
                            <div class="col-xs-12">
                              <strong>@{{item.created_at}}</strong>
                              <br>
                              <a :href="item.url || '/community'" target="_blank" rel="noopener noreferrer" v-html="item.title"></a>
                              <p>
                                <span v-html="item.description.slice(0,400)"></span>
                                <span v-if="item.description.length>400">...</span>
                              </p>
                            </div>
                            <hr class="dotted">
                          </div>
                        </div>

                      </div>
                    </feed>
                  </div>
                </div>

                <div v-if="widgetData[idx]['componenttype']=='feed'">
                  <h4 class="charttitle">@{{widgetData[idx]['title']}}</h4>
                  <div class="scrolling">
                    <feed :user="user" mode="updates" inline-template>
                      <div v-show="loading==false">
                        <div class="feed-item" v-for="item in feedData">
                          <div class="clearfix">
                            <div class="col-xs-12">
                              <small>
                                {{--  <a :href="'/sponsor/' + item.provider.id" class="upper">@{{item.provider.name}}</a>  --}}
                              </small>
                              <br>
                              <strong>@{{item.created_at}}</strong>
                              <br>
                              <a :href="item.url">@{{item.title}}</a>
                              <p>
                                <span v-html="item.description.slice(0,400)"></span>
                                <span v-if="item.description.length>400">...</span>
                              </p>
                            </div>
                            <hr class="dotted">
                          </div>
                        </div>
                      </div>
                    </feed>
                  </div>
                </div>

                <chart-track v-if="widgetData[idx]['componenttype']=='track'  && totalLoaded" :title="widgetData[idx]['title']" :period="widgetData[idx]['period']"
                :colors="colorsAll" :idname="idx+'chart'" :datasets="datasets" :main="main"></chart-track>

                <chart-xy v-if="widgetData[idx]['componenttype']=='xy'  && totalLoaded" :datasets="datasets" :main="main" :period="widgetData[idx]['period']"
                :title="widgetData[idx]['title']" :idname="idx+'chart'" :colors="colorsAll" :is-subgraphy="true"></chart-xy>

                <chart-volatilitytrack v-if="widgetData[idx]['componenttype']=='volatilitytrack'  && totalLoaded" :title="widgetData[idx]['title']" :period="widgetData[idx]['period']"
                :datasets="datasets"  :loading-list="{}" :loading="volatilityLoading" :colors="colorsAll" :idname="idx+'chart'" :idx="idx"></chart-volatilitytrack>

                <comparison-table v-if="widgetData[idx]['componenttype']=='comparisonTable'" :data-loaded="totalLoaded" :widget-data="widgetData" :idx="idx"
                :period="period" >
                </comparison-table>


                <monitor-table v-if="widgetData[idx]['componenttype']=='monitor'" :data-loaded="totalLoaded" :widget-data="widgetData" :idx="idx"
                :period="period" :user="user">
                </monitor-table>

                <chart-portfoliobar v-if="widgetData[idx]['componenttype']=='portfolioBar' && totalLoaded" :title="widgetData[idx]['title']" :idname="idx+'chart'"
                :data-loaded="totalLoaded" :widget-data="widgetData" :idx="idx" :period="period">
                </chart-portfoliobar>

                <div v-if="widgetData[idx]['componenttype']=='stat' && totalLoaded">
                  <h4 class="charttitle">@{{widgetData[idx]['title']}}</h4>
                  <div class="stat-large">
                    <chart-data :data-loaded="totalLoaded" :widget-data="widgetData" :idx="idx"  :period="widgetData[idx]['period']">
                    </chart-data>
                  </div>
                </div>
                <div class="abs-loader" v-if="!totalLoaded">
                  <div class="loader-inner ball-pulse">
                    <div></div>
                    <div></div>
                    <div></div>
                  </div>
                </div>
              </div>
            </widget-block>
          </div>
        </grid-item>
      </grid-layout>
    </div>
    <widget-modal :portfolio-loaded="portfolioLoaded"></widget-modal>
  </div>
</widgets>

@include('spark::nav.footer')
@endsection
