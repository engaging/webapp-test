@extends('spark::layouts.app')

@section('title', '| Plans')

@section('content')

<style type="text/css">
    .datatable .heading>div {
        height: 60px;
    }
    .datatable .data-row > div {
        height: 100px;
        padding: 15px;
    }
    .datatable svg {
        vertical-align: top;
        margin-top: -5px;
    }
    .data-row-parent {
        background: transparent;
    }
    .data-row-parent.last:after {
        display: none;
    }
    .datatable {
        margin: 50px auto;
        position: relative;
    }
    .datatable:after {
        content: "";
        height: 110%;
        width: 34%;
        display: block;
        background: #fff;
        box-shadow: 0 0 30px #aaa;
        position: absolute;
        top: -30px;
        right: 0;
    }
    .datatable .btn {
        margin-top: 15px;
    }
    .datatable .overlay * {
        position: relative;
        z-index: 2;
    }
    .datatable .data-row-parent:after {
        z-index: 5;
    }
</style>

<div class="banner-top pull-top" style="background-image:url({!!$content->banner!!})">
    <div class="banner-mobile visible-xs">
        <img src="{!!$content->mobileBanner!!}" class="img-responsive">
    </div>
    <div class="caption">
        <h1>EXAMPLE PENDING PAGE</h1>
    </div>
</div>


<div class="clearfix bg-darkgray pad-md">
    <div class="container marketing">
        
        <div class="clearfix"> 
            <div class="col-sm-8 col-sm-offset-2 text-center">
                <h3 class="text-navy upper">Lorem ipsum dolor sit amet, consectetur adipiscing elit</h3>
                <p>Morbi odio ipsum, consequat ac feugiat quis, sollicitudin ut augue. Sed vitae tempor velit. Fusce maximus ex a diam tincidunt dapibus. Donec iaculis augue aliquet porta fermentum. Maecenas ac semper risus. Fusce interdum varius urna nec fringilla. Pellentesque vitae pretium risus, ut malesuada nulla. Vivamus ac mattis lectus. Donec sed neque quis lorem pulvinar lobortis et quis odio. Morbi ut est sit amet massa molestie posuere tempor nec elit. Duis ullamcorper mauris eros, vel dignissim lectus cursus sit amet.</p>
            </div>
        </div>

        

    </div>
</div>

@include('spark::nav.footer')


@endsection
