@extends('spark::layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Your account is already activated</div>

                <div class="panel-body">
                    <h5>Please login directly to your engagingenterprises account. If you cannot login anymore, try to reset your password or contact our support on <a href="mailto:admin@engagingenterprises.co?subject=Support%20Request:%20Cannot%20login%20anymore">admin@engagingenterprises.co</a>.</h5>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
