@extends('spark::layouts.app')
@section('title', '| My Portfolios')
@section('scripts')
<script src="/js/amcharts/amcharts.js"></script>
<script src="/js/amcharts/pie.js"></script>
<script src="/js/amcharts/themes/light.js"></script>
@stop
@section('content')
<portfolios :user="user" inline-template>
  <div class="container">

    <!-- Application Dashboard -->

    <div id="print-header" class="clearfix print-header print-only">
      <div class="nav-blue navbar-inverse user" style="padding:10px 25px;">
        <div style="width:22%;float:left;" class="print-border-right">
          <div class="print-border-image" style="background-image:url({{Request::root()}}/img/design/logo.png);">
          </div>
        </div>
        <div style="width:48%;float:left;padding-left:27px;" class="pad">
          <h3>MY PORTFOLIOS</h3>
        </div>
        <div style="width:10%;float:left;" class="pad">
        </div>
        <div style="width:18%;float:left;" class="pad">
          <strong>Time: @{{periodKeysForFilters[period]}}</strong>
          <br>
          <strong>As of {{ $maxDate }}</strong>

        </div>
      </div>
    </div>
    <div id="print-container">

      <div class="print-header"></div>
      <div class="row">
        <div class="col-xs-12">

          <div class="clearfix">


            <div class="chartfilter col-sm-12">
              <div class="text-right">

                <form id="pdfinput" target="_blank" rel="noopener noreferrer" action="/portfolios/pdf" method="post">
                  <input type="hidden" name="html"> {{ csrf_field() }}
                </form>



              </div>
            </div>
          </div>

          {{-- Navigation --}}
          <div class="row portfolio-tab-nav print-hide">

            <div class="col-lg-3 col-sm-4 pull-right hidden-xs">
              @include('partials.periodmenu',array('class'=>'gray pull-right'))
            </div>

            <div class="col-lg-3 col-sm-4 col-xs-6">
              <button @click="showList=1" v-bind:class="{'btn-default':showList!=1,'btn-primary':showList==1}" class="btn form-control"
                style="height: 50px">
                <img class="hidden-xs" src="/img/design/icon-graph.png" alt style="margin-right: 10px;"> My Portfolios
              </button>
            </div>
            <div class="col-lg-3 col-sm-4" v-if="allowSharing">
              <button @click="showList=2" v-bind:class="{'btn-default':showList!=2,'btn-primary':showList==2}" class="btn form-control"
                style="height: 50px">
                <img class="hidden-xs" src="/img/design/icon-shared.png" alt style="margin-right: 10px;" v-show="showList!=2">
                <img class="hidden-xs" src="/img/design/icon-shared-white.png" alt style="margin-right: 10px;" v-show="showList==2"> Shared with me
              </button>
            </div>

            <div class="col-lg-3 col-sm-4 col-xs-6 hidden-xs">
              <button @click="showList=3" v-bind:class="{'btn-default':showList!=3,'btn-primary':showList==3}" class="btn form-control "
                style="height: 50px">
                <img class="hidden-xs" src="/img/design/icon-upload.png" alt style="margin-right: 10px;" v-show="showList!=3">
                <img class="hidden-xs" src="/img/design/icon-upload-white.png" alt style="margin-right: 10px;" v-show="showList==3"> Upload Private Track
              </button>
            </div>

            <div class="col-lg-3 col-sm-4 col-xs-6 visible-xs">
              <button @click="showList=3" v-bind:class="{'btn-default':showList!=3,'btn-primary':showList==3}" class="btn form-control "
                style="height: 50px">
                <img class="hidden-xs" src="/img/design/icon-upload.png" alt style="margin-right: 10px;" v-show="showList!=3">
                <img class="hidden-xs" src="/img/design/icon-upload-white.png" alt style="margin-right: 10px;" v-show="showList==3"> Upload
              </button>
            </div>
          </div>

          {{--  <div v-if="portfoliosLoaded && portfolios.length == 0">
            You haven't created any portfolios yet.
          </div>  --}}

          <div v-show="(portfoliosLoaded == false && showList==1) || (indicesLoaded == false && showList==3)" class="container" style="position:relative">
            <div class="loader-inner ball-pulse" style="position: absolute;top:-100px;z-index:2;width:100%;">
              <div></div>
              <div></div>
              <div></div>
            </div>
          </div>


          {{-- TAB 1 --}}
          <div v-show="showList==1" class="row portfolios portfolios-main">
            {{-- Portfolios you own --}}
            <div class="col-xs-12">
              <div class="clearfix options print-hide">
                <div class="tableoption pull-right" v-bind:class="{'opened':showBenchmarkMenu1==1}" data-var="showBenchmarkMenu1">
                  <button @click="showBenchmarkMenu1=showBenchmarkMenu1*-1" class="btn">
                    <span class="icon icon-cog"></span>
                  </button>
                  <div class="menu" v-show="showBenchmarkMenu1==1">
                    <p>Add performance metrics (@{{dataTableColsMax - tableCols_benchmarks_show.length}} left)</p>
                    <label v-for="(v,k) in dataTableCols" class="checkbox-style">
                      <input v-model="tableCols_benchmarks_show" type="checkbox" v-bind:value="k" v-bind:disabled='tableCols_benchmarks_show.indexOf(k) < 0 && tableCols_benchmarks_show.length >= dataTableColsMax'>
                      <i></i>
                      @{{v}}
                    </label>
                  </div>
                </div>
                <div class="pull-left visible-xs">
                  @include('partials.periodmenu',array('class'=>'gray pull-right'))
                </div>

                <div class="dropdown pull-right" style="margin-bottom: 10px; margin-right: 1em; display: inline-block;">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                        <button class="btn btn-outline-blue lg" :class="{ 'btn-caret-loading': loadingExport }">
                          <i class="fa fa-download fa-sm"></i>&nbsp;&nbsp;Export&nbsp;
                          <i class="fa fa-spinner fa-spin" v-if="loadingExport"></i>
                          <span class="caret caret-hidden" v-if="!loadingExport"></span>
                        </button>
                    </a>
                    <ul class="dropdown-menu" role="menu" v-if="!loadingExport">
                        <li>
                            <button @click="exportPdf('download')" class="btn btn-dropdown-menu lg">
                              <img src="/img/icons/pdf.png" class="icon icon-md">&nbsp;&nbsp;PDF
                            </button>
                        </li>
                        <li class="tooltip-container">
                            <button @click="exportPdf('symphony')" class="btn btn-dropdown-menu lg" :disabled="!user.hasSymphonyAccount">
                              <img src="/img/icons/symphony.png" class="icon icon-md">&nbsp;&nbsp;Symphony
                            </button>
                            <div v-if="!user.hasSymphonyAccount" class="tooltip-popover">
                              Link your Symphony account from <a href="/settings#/symphony">Your Settings</a> to use this service
                            </div>
                        </li>
                    </ul>
                </div>
              </div>

              <div class="clearfix" v-if="device == 'mobile'">
                <div class="data-row-parent data-row-parent-mobile" v-for="portfolio in portfolios">
                  <div class="data-row row">
                    <div class="col-xs-12">
                      <div class="clearfix">
                        <div class="row">
                          <div class="col-xs-8">
                            <a v-bind:href="'/portfolios/' + portfolio.slug" class="title">
                              <strong>@{{ portfolio.title }}</strong>
                            </a>
                            <span style="font-size:12px;">
                              @{{portfolio.strategiesCount}} Strategies
                              <br> @{{weightingOptions[portfolio.weighting]}}
                            </span>
                            <br v-if="allowSharing">
                            <small v-if="allowSharing" @click="openSharePortfolio(portfolio)" v-if="portfolio.shares > 0" class="text-gray nowrap">
                              <img src="/img/design/icon-shared2.png" width="20" style="margin-right:5px;"> Sharing
                            </small>
                          </div>
                          <div class="col-xs-4">
                            <div class="tableoption pull-right" v-bind:class="{'opened':showPortfolioMenu==portfolio.id}" data-var="showPortfolioMenu">
                              <button class="btn text-blue" @click="openPortfolioMenu(portfolio.id)">
                                <i class="fa fa-list"></i>
                              </button>
                              <div class="menu" v-show="showPortfolioMenu == portfolio.id">
                                <a @click="openEditPortfolio(portfolio)">Edit or Delete</a>
                                <a @click="openDuplicatePortfolio(portfolio)">Duplicate</a>
                                <a v-bind:href="'/analyse/portfolio/' + portfolio.slug">Analyse &amp; Design</a>
                                <a v-if="allowSharing" @click="openSharePortfolio(portfolio)">Share</a>
                                <a v-bind:href="'/portfolios/' + portfolio.slug">Open Factsheet</a>
                              </div>
                            </div>
                          </div>



                          <div class="col-xs-12">
                            <div v-for="(field, idx) in tableCols_benchmarks_show" v-bind:class="{'highlight':(sortby==periodKeysForFilters[period] + '_'+field),'bg-gray':idx%2==0}"
                              class="row data-row-kpi">
                              <div class="text-left col-xs-6" v-if="portfolio[periodKeysForFilters[period]+'_active'] == 1">
                                @{{dataTableCols[field]}}
                              </div>
                              <div class="text-left col-xs-6" v-if="portfolio[periodKeysForFilters[period]+'_active'] == 1">
                                @{{ portfolio[periodKeysForFilters[period] + '_' + field] | round_4dp | unit(portfolio['um_' + field]) }}
                              </div>
                              <div class="text-left col-xs-6" v-if="portfolio[periodKeysForFilters[period]+'_active'] == 0">
                                n/a
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>



                  </div>
                </div>
              </div>
            </div>

            <div class="datatable main" v-if="device != 'mobile'">
              <div class="clearfix heading nopadding">
                <div style="width:23%" class="namecol">
                  <button class="btn" @click="changeSortby('title')">
                    Name
                    <span v-show="sortby!='title'">
                      <i class="fa fa-sort"></i>
                    </span>
                    <span v-show="sortby=='title'">
                      <i v-bind:class="{
                                            'fa-sort-up':(sortorder>0),'fa-sort-down':(sortorder<0)
                                            }" class="fa"></i>
                    </span>
                  </button>
                </div>
                <div style="width:15%">
                  <div class="dropdown-fake-container text-center">
                    <a class="current" @click="groupTypesShow=groupTypesShow*-1">
                      @{{groupTypeNames[groupType]}}
                      <span class="fa fa-angle-down" v-if="groupTypesShow==-1"></span>
                      <span class="fa fa-angle-up" v-if="groupTypesShow==1"></span>
                    </a>
                    <div class="dropdown-fake" v-show="groupTypesShow==1">
                      <a v-for='(v,k) in groupTypeNames' @click="groupType=k">@{{v}}</a>
                    </div>
                    {{--
                    <small>Allocation by</small>
                    <select class="form-control" v-model="groupType">
                      <option value='asset'>Asset Class</option>
                      <option value='type'>Asset Type</option>
                    </select> --}}
                  </div>
                </div>
                <div v-bind:style="{width: (60/dataTableColsCount)+'%'}" v-for="(field, idx) in tableCols_benchmarks_show" v-bind:class="{'highlight':(sortby==periodKeysForFilters[period] + '_'+field),'bg-gray':idx%2==0}">
                  <button @click="changeSortby(periodKeysForFilters[period] + '_' + field)" class="btn">
                    @{{dataTableCols[field]}}
                    <span v-show="sortby!=periodKeysForFilters[period] + '_'+ field">
                      <i class="fa fa-sort pull-right"></i>
                    </span>
                    <span v-show="sortby==periodKeysForFilters[period] + '_'+ field">
                      <i v-bind:class="{
                                            'fa-sort-up':(sortorder>0),'fa-sort-down':(sortorder<0)
                                            }" class="fa pull-right"></i>
                    </span>
                  </button>
                </div>
              </div>

              <div class="clearfix">
                <div class="data-row-parent" v-for="portfolio in portfolios" :key="portfolio.id">
                  <div class="data-row">
                    <div style="width:23%">

                      <div class="clearfix">
                        <div class="col-xs-8">
                          <a v-bind:href="'/portfolios/' + portfolio.slug" class="title">
                            <strong>@{{ portfolio.title }}</strong>
                          </a>
                          <span style="font-size:12px;">
                            @{{portfolio.strategiesCount}} Strategies
                            <br> @{{weightingOptions[portfolio.weighting]}}
                          </span>
                          <br v-if="allowSharing">
                          <small v-if="allowSharing" @click="openSharePortfolio(portfolio)" v-if="portfolio.shares > 0" class="text-gray nowrap">
                            <img src="/img/design/icon-shared2.png" width="20" style="margin-right:5px;"> Sharing
                          </small>
                        </div>
                        <div class="col-xs-4">
                          <div class="tableoption pull-right" v-bind:class="{'opened':showPortfolioMenu==portfolio.id}" data-var="showPortfolioMenu">
                            <button class="btn text-blue" @click="openPortfolioMenu(portfolio.id)">
                              <i class="fa fa-list"></i>
                            </button>
                            <div class="menu" v-show="showPortfolioMenu == portfolio.id">
                              <a @click="openEditPortfolio(portfolio)">Edit or Delete</a>
                              <a @click="openDuplicatePortfolio(portfolio)">Duplicate</a>
                              <a v-bind:href="'/analyse/portfolio/' + portfolio.slug">Analyse &amp; Design</a>
                              <a v-if="allowSharing" @click="openSharePortfolio(portfolio)">Share</a>
                              <a v-bind:href="'/portfolios/' + portfolio.slug">Open Factsheet</a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div style="width:15%">
                      {{-- graph --}}

                      <div class="pie-small asset" v-show="groupType=='asset'">
                        <chart-pie :datasets="portfolio['assetClassWeights']" middletitle="" main="portfolio" legend="false"
                        radius="50" innerradius="20" :idname="'asset_'+portfolio.id" :colors="assetClassColors"></chart-pie>
                      </div>

                      <div class="pie-small asset" v-show="groupType=='type'">
                          <chart-pie :datasets="portfolio['factorWeights']" middletitle="" main="portfolio" legend="false"
                          radius="50" innerradius="20" :idname="'category_'+portfolio.id" :colors="categoryColors"></chart-pie>
                      </div>

                    </div>

                    <div v-for="(field, idx) in tableCols_benchmarks_show" v-bind:style="{width: (60/dataTableColsCount)+'%'}" v-bind:class="{'highlight':(sortby==periodKeysForFilters[period] + '_'+field),'bg-gray':idx%2==0}"
                      class="text-center col">
                      <span v-if="portfolio[periodKeysForFilters[period]+'_active'] == 1">
                        @{{ portfolio[periodKeysForFilters[period] + '_' + field] | round_4dp | unit(portfolio['um_' + field]) }}
                      </span>
                      <span v-if="portfolio[periodKeysForFilters[period]+'_active'] == 0">
                        n/a
                      </span>
                    </div>

                  </div>
                </div>
              </div>


            </div>
          </div>


          {{-- TAB 2 --}}
                      {{-- shared Portfolios --}}
          {{--  <div v-show="showList==2" class="row portfolios portfolios-share" v-if="allowSharing">

            <div class="col-xs-12">

              <div class="clearfix options">
                <div class="tableoption pull-right" v-bind:class="{'opened':showBenchmarkMenu1==1}" data-var="showBenchmarkMenu1">
                  <button @click="showBenchmarkMenu1=showBenchmarkMenu1*-1" class="btn">
                    <span class="icon icon-cog"></span>
                  </button>
                  <div class="menu" v-show="showBenchmarkMenu1==1">
                    <p>Select performance metrics</p>
                    <label v-for="(v,k) in dataTableCols" class="checkbox-style">
                      <input v-model="tableCols_benchmarks_show" type="checkbox" v-bind:value="k">
                      <i></i>
                      @{{v}}
                    </label>
                  </div>
                </div>
              </div>

              <div class="datatable main" v-if="device != 'mobile'">
                <div class="clearfix heading nopadding">
                  <div style="width:23%" class="namecol">
                    <button class="btn" @click="changeSortby('title')">
                      Name
                      <span v-show="sortby!='title'">
                        <i class="fa fa-sort"></i>
                      </span>
                      <span v-show="sortby=='title'">
                        <i v-bind:class="{
                                            'fa-sort-up':(sortorder>0),'fa-sort-down':(sortorder<0)
                                            }" class="fa"></i>
                      </span>
                    </button>
                  </div>
                  <div style="width:15%">
                    <div class="dropdown-fake-container text-center">
                      <a class="current" @click="groupTypesShow=groupTypesShow*-1">
                        @{{groupTypeNames[groupType]}}
                        <span class="fa fa-angle-down" v-if="groupTypesShow==-1"></span>
                        <span class="fa fa-angle-up" v-if="groupTypesShow==1"></span>
                      </a>
                      <div class="dropdown-fake" v-show="groupTypesShow==1">
                        <a v-for='(i,v) in groupTypeNames' @click="groupType=i">@{{v}}</a>
                      </div>
                    </div>
                  </div>
                  <div v-bind:style="{width: (60/dataTableColsCount)+'%'}" v-for="(field, idx) in tableCols_benchmarks_show" v-bind:class="{'highlight':(sortby==periodKeysForFilters[period] + '_'+field),'bg-gray':idx%2==0}">
                    <button @click="changeSortby(periodKeysForFilters[period] + '_' + field)" class="btn">
                      @{{dataTableCols[field]}}
                      <span v-show="sortby!=periodKeysForFilters[period] + '_'+ field">
                        <i class="fa fa-sort pull-right"></i>
                      </span>
                      <span v-show="sortby==periodKeysForFilters[period] + '_'+ field">
                        <i v-bind:class="{
                                            'fa-sort-up':(sortorder>0),'fa-sort-down':(sortorder<0)
                                            }" class="fa pull-right"></i>
                      </span>
                    </button>
                  </div>
                </div>

                <div class="clearfix" v-if="allowSharing">
                  <div class="data-row-parent" v-for="portfolio in portfoliosShared" :key="portfolio.id">
                    <div class="data-row">
                      <div style="width:23%">

                        <div class="clearfix">

                            <a v-bind:href="'/portfolios/' + portfolio.slug + '?u=' + portfolio.share.user_id + '&e=' + portfolio.share.to_email + '&share=' + portfolio.share.share_code"
                              class="title">
                              <strong> @{{portfolio.title}} </strong>
                            </a>

                            <button @click="copySharedPortfolio(portfolio.id)" class="pull-right btn btn-primary" style="font-size: 11px;width: 100px;white-space: normal;">
                              Create a copy for my account
                            </button>

                            <span style="font-size:12px;">
                              @{{portfolio.strategiesCount}} Strategies
                              <br> @{{weightingOptions[portfolio.weighting]}}
                            </span>
                            <br>
                            <small class="nowrap">Shared on @{{portfolio.share.created_at}}</small>



                        </div>
                        <div style="width:15%">


                          <div class="pie-small asset" v-show="groupType=='asset'">
                              <chart-pie :datasets="portfolio['assetClassBreakdown']" middletitle="" main="portfolio" legend="false"
                              radius="50" innerradius="20" :idname="'share_asset_'+portfolio.id" :colors="assetClassColors"></chart-pie>
                          </div>

                          <div class="pie-small asset" v-show="groupType=='type'">
                              <chart-pie :datasets="portfolio['factorBreakdown']" middletitle="" main="portfolio" legend="false"
                              radius="50" innerradius="20" :idname="'share_category_'+portfolio.id" :colors="categoryColors"></chart-pie>
                          </div>

                        </div>

                        <div v-for="(field, idx) in tableCols_benchmarks_show" v-bind:style="{width: (60/dataTableColsCount)+'%'}" v-bind:class="{'highlight':(sortby==periodKeysForFilters[period] + '_'+field),'bg-gray':idx%2==0}"
                          class="text-center col">
                          <span v-if="portfolio[periodKeysForFilters[period]+'_active'] == 1">
                            @{{ portfolio[periodKeysForFilters[period] + '_' + field] | round_4dp | unit(portfolio['um_' + field]) }}
                          </span>
                          <span v-if="portfolio[periodKeysForFilters[period]+'_active'] == 0">
                            n/a
                          </span>
                        </div>

                      </div>
                    </div>
                  </div>

                </div>
              </div>

            </div>
          </div>  --}}

          {{-- TAB 3 --}}
          <div v-show="showList==3" class="row portfolios portfolios-upload">

            <div class="col-xs-12">

              <div class="clearfix options">

                <div class="tableoption pull-right" v-bind:class="{'opened':showBenchmarkMenu1==1}" data-var="showBenchmarkMenu1">
                  <button @click="showBenchmarkMenu1=showBenchmarkMenu1*-1" class="btn">
                    <span class="icon icon-cog"></span>
                  </button>
                  <div class="menu" v-show="showBenchmarkMenu1==1">
                    <p>Select performance metrics</p>
                    <label v-for="(v,k) in dataTableCols" class="checkbox-style">
                      <input v-model="tableCols_benchmarks_show" type="checkbox" v-bind:value="k">
                      <i></i>
                      @{{v}}
                    </label>
                  </div>
                </div>

                <button class="btn btn-info pull-right lg" data-toggle="modal" style="margin-bottom:10px" @click="openEditStrategy()">
                  <i class="fa fa-plus"></i>
                  <strong>Upload</strong>
                </button>

                <div class="visible-xs pull-left">
                  @include('partials.periodmenu',array('class'=>'gray pull-right'))
                </div>
              </div>

              <div class="clearfix" v-if="device == 'mobile'">
                <div class="data-row-parent data-row-parent-mobile" v-for="strategy in indices">
                  <div class="data-row row">
                    <div class="col-xs-12">
                      <div class="clearfix">
                        <div class="row">

                          <div class="col-xs-8">
                            <a v-bind:href="'/strategy/' + strategy.code" class="title">
                              <strong>@{{ strategy.shortName }}</strong>
                            </a>
                            <div style="width:15%" class="text-center">
                              <span>
                                @{{ strategy.lastDate || '-' }}
                              </span>
                            </div>
                          </div>

                          <div class="col-xs-4">
                            <div class="tableoption pull-right" v-bind:class="{'opened':showStrategyMenu==strategy.id}" data-var="showStrategyMenu">
                              <button class="btn text-blue" @click="openStrategyMenu(strategy.id)">
                                <i class="fa fa-list"></i>
                              </button>
                              <div class="menu" v-show="showStrategyMenu == strategy.id">
                                <a v-if="strategy.is_editable" @click="openEditStrategy(strategy)">
                                  Edit or Delete
                                </a>
                                <a :href="'/strategy/' + strategy.code">
                                  Open Factsheet
                                </a>
                              </div>
                            </div>
                          </div>

                          <div class="col-xs-12">
                            <div v-for="(field, idx) in tableCols_benchmarks_show" v-bind:class="{'highlight':(sortby==periodKeysForFilters[period] + '_'+field),'bg-gray':idx%2==0}"
                              class="row data-row-kpi">
                              <div class="text-left col-xs-6" v-if="strategy[periodKeysForFilters[period]+'_active'] == 1">
                                @{{dataTableCols[field]}}
                              </div>
                              <div class="text-left col-xs-6" v-if="strategy[periodKeysForFilters[period]+'_active'] == 1 && strategy.is_suspended == 0">
                                @{{ strategy[periodKeysForFilters[period] + '_' + field] | round_4dp | unit(strategy['um_' + field]) }}
                              </div>
                              <div class="text-left col-xs-6" v-if="strategy[periodKeysForFilters[period]+'_active'] == 0 || strategy.is_suspended == 1">
                                n/a
                              </div>
                            </div>
                          </div>

                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="datatable main" v-if="device != 'mobile'">
                <div class="clearfix heading nopadding">
                  <div style="width:23%" class="namecol">
                    <button class="btn" @click="changeSortbyStrategy('shortName')">
                      Name
                      <span v-show="sortby!='shortName'">
                        <i class="fa fa-sort"></i>
                      </span>
                      <span v-show="sortby=='shortName'">
                        <i v-bind:class="{
                                          'fa-sort-up':(sortorder>0),'fa-sort-down':(sortorder<0)
                                          }" class="fa"></i>
                      </span>
                    </button>
                  </div>
                  <div style="width:15%">
                    <button class="btn" @click="changeSortbyStrategy('lastDate')">
                      Last date
                      <span v-show="sortby!='lastDate'">
                        <i class="fa fa-sort"></i>
                      </span>
                      <span v-show="sortby=='lastDate'">
                        <i v-bind:class="{
                                          'fa-sort-up':(sortorder>0),'fa-sort-down':(sortorder<0)
                                          }" class="fa"></i>
                      </span>
                    </button>
                  </div>
                  <div v-bind:style="{width: (60/dataTableColsCount)+'%'}" v-for="(field, idx) in tableCols_benchmarks_show" v-bind:class="{'highlight':(sortby==periodKeysForFilters[period] + '_'+field),'bg-gray':idx%2==0}">
                    <button @click="changeSortbyStrategy(periodKeysForFilters[period] + '_' + field)" class="btn">
                      @{{dataTableCols[field]}}
                      <span v-show="sortby!=periodKeysForFilters[period] + '_'+ field">
                        <i class="fa fa-sort pull-right"></i>
                      </span>
                      <span v-show="sortby==periodKeysForFilters[period] + '_'+ field">
                        <i v-bind:class="{
                                          'fa-sort-up':(sortorder>0),'fa-sort-down':(sortorder<0)
                                          }" class="fa pull-right"></i>
                      </span>
                    </button>
                  </div>
                </div>

                <div class="clearfix">
                  <div class="data-row-parent" v-for="strategy in indices">
                    <div class="data-row">
                      <div style="width:23%">

                        <div class="clearfix">
                          <div class="col-xs-8">
                            <a v-bind:href="'/strategy/' + strategy.code" class="title">
                              <strong>@{{ strategy.shortName }}</strong>
                            </a>
                          </div>
                          <div class="col-xs-4">
                            <div class="tableoption pull-right" v-bind:class="{'opened':showStrategyMenu==strategy.id}" data-var="showStrategyMenu">
                              <button class="btn text-blue" @click="openStrategyMenu(strategy.id)">
                                <i class="fa fa-list"></i>
                              </button>
                              <div class="menu" v-show="showStrategyMenu == strategy.id">
                                <a v-if="strategy.is_editable" @click="openEditStrategy(strategy)">
                                  Edit or Delete
                                </a>
                                <a :href="'/strategy/' + strategy.code">
                                  Open Factsheet
                                </a>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div style="width:15%" class="text-center">
                        <span>
                          @{{ strategy.lastDate || '-' }}
                        </span>
                      </div>

                      <div v-for="(field, idx) in tableCols_benchmarks_show" v-bind:style="{width: (60/dataTableColsCount)+'%'}" v-bind:class="{'highlight':(sortby==periodKeysForFilters[period] + '_'+field),'bg-gray':idx%2==0}"
                        class="text-center col">
                        <span v-if="strategy[periodKeysForFilters[period]+'_active'] == 1 && strategy.is_suspended == 0">
                          @{{ strategy[periodKeysForFilters[period] + '_' + field] | round_4dp | unit(strategy['um_' + field]) }}
                        </span>
                        <span v-if="strategy[periodKeysForFilters[period]+'_active'] == 0 || strategy.is_suspended == 1">
                          n/a
                        </span>
                      </div>

                    </div>
                  </div>
                </div>

              </div>
            </div>
          </div>
        </div>


        <div class="footer  print-only">
          <div class="container disclaimer pad-top">
            {!!$disclaimer!!}
          </div>
        </div>

      </div>
    </div>

    {{--  <div v-if="allowSharing" class="modal fade" id="sharePortfolio" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <h4 class="modal-title">Share @{{sharingTitle}}</h4>
          </div>

          <div class="modal-body" v-show="shareLoading==1">
            <div class="loader-inner ball-pulse">
              <div></div>
              <div></div>
              <div></div>
            </div>
          </div>
          <div class="modal-body text-center" v-show="shareLoading==2">
            <p>@{{shareResult}} share notification(s) have been sent.</p>
            <p v-show="shareResult==0">Please check that you have entered valid email addresses.</p>
            <button class="btn btn-primary" @click="shareLoading=0">OK</button>
          </div>
          <div class="modal-body" v-show="shareLoading==0">
            <small>Email Addresses (comma separated list)</small>
            <div class="input clearfix" style="border:1px solid #aaa;padding: 5px;border-radius: 3px;">
              <div class="col-xs-9" style="padding: 3px;">
                <input name="shareEmails" v-model="shareEmails" class="form-control" style="border:0;box-shadow:none;">
              </div>
              <div class="col-xs-3" style="padding: 3px;">
                <button @click="addShare()" class="btn btn-primary form-control">SHARE</button>
              </div>
            </div>
            <div v-show="shareCurrent.length>0">
              <hr>
              <strong>CURRENTLY SHARED WITH</strong>
              <ul class="text-navy" style="margin-top: 10px">
                <h4 class="text-center text-gray" v-show="shareCurrent.length==0 || shareCurrent==null">Loading...</h4>
                <li v-for="share in shareCurrent" style="min-height: 30px;">
                  <strong>@{{share.to_email}}</strong>
                  <span v-if="share.to_user" style="font-size: 11px">engagingenterprises User</span>
                  <button @click="removeShare(share.id)" class="btn btn-outline pull-right" style="padding: 0 10px">remove</button>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>  --}}
    <edit-portfolio></edit-portfolio>
    <private-strategies><private-strategies>
  </div>
</portfolios>

@include('spark::nav.footer')
@endsection
