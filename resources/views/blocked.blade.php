@extends('spark::layouts.app')

@section('content')
<home :user="user" inline-template>
    <div class="container">
        <!-- Application Dashboard -->
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="clearfix pad-md"> 
                    {!! $content->inactiveUserText !!}
                </div>
            </div>
        </div>
    </div>
    @include('spark::nav.footer')
    
</home>
@endsection
