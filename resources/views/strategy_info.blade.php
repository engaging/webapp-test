@extends('spark::layouts.app')
@section('title', "| ".$index->longName)
@push('css')
<link href="/css/ion.rangeSlider.css" rel="stylesheet">
<link href="/css/ion.rangeSlider.skinHTML5.css" rel="stylesheet">
@endpush
@section('scripts')
<script src="/js/amcharts/amcharts.js"></script>
<script src="/js/amcharts/serial.js"></script>
<script src="/js/amcharts/radar.js"></script>
<script src="/js/amcharts/pie.js"></script>
<script src="/js/amcharts/xy.js"></script>
<script src="/js/amcharts/themes/light.js"></script>
@stop
@section('content')

<?php
  if (!isset($componentOptions)) {
    $componentOptions = [];
  }
  $componentOptions = json_encode((object)$componentOptions);
  if (!isset($templateOptions)) {
    $templateOptions = [];
  }
  if (!isset($templateOptions['chartOptions'])) {
    $templateOptions['chartOptions'] = [];
    for ($i = 0; $i < 6; $i++) {
      $templateOptions['chartOptions'][] = [
        'order' => $i,
        'options' => 'null',
      ];
    }
  }
?>

<strategy-detail
  :user='{!! isset($templateOptions->user) ? str_ireplace("'", "&apos;", $templateOptions->user) : "user" !!}'
  :strategy='{!! str_ireplace("'", "&apos;", $index) !!}'
  :options='{!! str_ireplace("'", "&apos;", $componentOptions) !!}'
  :periods={{ json_encode($periods) }}
  max-date={{ $maxDate }}
  inline-template>
  <div>
    <div id="stickyheader" v-bind:class="{ 'affixed':stickyheader==true} ">
      <div class="clearfix bg-gray ">
        <div class="container ">
          <div class="row ">
            <div class="col-sm-4 text-xs-center ">
              <h4 class="upper ">@{{strategy.shortName}}</h4>

              <strong class="text-gray upper ">
                @{{ strategy.assetClass }} | @{{ strategy.currency }} @{{ returnCategory }}@{{ returnType }} @{{ strategy.volTarget }}
              </strong>
            </div>
            <div class="col-sm-5 text-xs-center">
                <form id="pdfinput" target="_blank" rel="noopener noreferrer" action="/strategy/{{$index->code}}/pdf" method="post" hidden>
                  <input type="hidden" name="html"> {{ csrf_field() }}
                </form>

                <div v-if="showExtraTools" class="dropdown" style="margin-right: 1em; display: inline-block;">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                      <button class="btn btn-outline-blue lg">
                        Analyse &amp; Design
                      </button>
                  </a>
                  <ul class="dropdown-menu" role="menu">
                    <li class="tooltip-container">
                      <div>
                        <a :class="{'disabled': !allowExtraTools}" href="/analyse/regression/strategy/{{$index->code}}" class="btn btn-dropdown-menu lg">
                          <img src="/img/icons/icon-regression.png" class="icon icon-md">&nbsp;&nbsp;Regression
                        </a>
                      </div>
                      <div v-if="!allowExtraTools" class="tooltip-popover">
                        This function is for Premium User only. Please contact <a href="mailto:admin@engagingenterprises.co?subject=Upgrade%20to%20Premium%20Request">engagingenterprises</a> to upgrade your account.
                      </div>
                    </li>
                  </ul>
                </div>
                <button v-if="strategyDetails.code !== 'VIX'" @click="showAddStrategies" class="btn btn-outline-blue lg" style="margin-right: 1em;">Add to Portfolio</button>
                <div class="dropdown" style="margin-right: 1em; display: inline-block;">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                        <button class="btn btn-outline-blue lg" :class="{ 'btn-caret-loading': loadingExport }">
                          <i class="fa fa-download fa-sm"></i>&nbsp;&nbsp;Export&nbsp;
                          <i class="fa fa-spinner fa-spin" v-if="loadingExport"></i>
                          <span class="caret caret-hidden" v-if="!loadingExport"></span>
                        </button>
                    </a>
                    <ul class="dropdown-menu" role="menu" v-if="!loadingExport">
                        <li v-if="isPremiumPlan">
                            <div>
                                <a download target="_blank" href="/strategy/{{$index->code}}/export" class="btn btn-dropdown-menu lg">
                                    <img src="/img/icons/excel.png" class="icon icon-md">&nbsp;&nbsp;Excel
                                </a>
                            </div>
                        </li>
                        <li>
                            <button @click="exportPdf('download')" class="btn btn-dropdown-menu lg">
                              <img src="/img/icons/pdf.png" class="icon icon-md">&nbsp;&nbsp;PDF
                            </button>
                        </li>
                        <li class="tooltip-container">
                            <button @click="exportPdf('symphony')" class="btn btn-dropdown-menu lg" :disabled="!user.hasSymphonyAccount">
                              <img src="/img/icons/symphony.png" class="icon icon-md">&nbsp;&nbsp;Symphony
                            </button>
                            <div v-if="!user.hasSymphonyAccount" class="tooltip-popover">
                              Link your Symphony account from <a href="/settings#/symphony">Your Settings</a> to use this service
                            </div>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="visible-xs col-xs-12 spacer-div"></div>

            <div class="col-sm-3 col-xs-6 col-xs-offset-3 col-sm-offset-0">
              @include('partials.periodmenu2',array('class'=>'gray'))
            </div>
          </div>
        </div>
      </div>
    </div>


    <div id="print-header" class="clearfix print-header print-only">
      <div class="nav-blue navbar-inverse user" style="padding:10px 25px;">
        <div style="width:22%;float:left;" class="print-border-right">
          <div class="print-border-image" style="background-image:url({{Request::root()}}/img/design/logo.png);">
          </div>
        </div>
        <div style="width:40%;float:left;padding-left:27px" class="pad">
          <h4 class="upper">@{{strategy.shortName}}</h4>
          <strong class="upper">
            @{{strategy.assetClass}}
            <span>|</span> @{{strategy.currency}}
          </strong>
        </div>
        <div style="width:12%;float:left;text-align:right;" class="pad">
          <br>
        </div>
        <div style="width:22%;float:left;text-align:right;" class="pad">
          <div><strong>Start Date: @{{periodDates.from}}</strong></div>
          <div><strong>&nbsp;&nbsp;&nbsp;&nbsp;End Date: @{{periodDates.to}}</strong></div>
        </div>
      </div>
    </div>



    <div id="print-container">

      <div class="print-header"></div>

      <div class="container pad-md" :class="'num_widgets_' + num_widgets">
        <div class="col-sm-8 col-sm-offset-2">

          {{--
          <div class="loading" v-show="statsLoaded == false">
            LOADING...
          </div> --}}
          <h3 v-if="strategy.is_suspended==1" style='color: red'>NOT UPDATED</h3>
          <p class="text-justify" v-html="strategy.description"></p>

          <div class="row strategyinfo pad-md">
            <div class="col-xs-12">
              <div class="row">
                <div class="col-sm-3 col-xs-5 text-gray">Full Name</div>
                <div class="col-sm-9 col-xs-7">
                  <strong class="datatable">@{{strategy['longName']}}</strong>
                </div>
              </div>
            </div>
            <hr />
            <div class="col-sm-6 col-xs-12">
              <div class="row" v-for="(field, key) in infoKeys1">
                <div class="col-sm-6 col-xs-5 text-gray">@{{key}}</div>
                <div class="col-sm-6 col-xs-7">
                  <strong class="datatable">@{{ checkShowData(strategy[field]) }}</strong>
                </div>
              </div>
            </div>
            <div class="col-sm-6 col-xs-12">
              <div class="row" v-for="(field, key) in infoKeys2">
                <div class="col-sm-6 col-xs-5 text-gray">@{{key}}</div>
                <div class="col-sm-6 col-xs-7">
                  <strong class="datatable">@{{ strategy[field]==0 ? '-' : strategy[field] }}</strong>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-6 col-xs-5 text-gray">Source</div>
                <div class="col-sm-6 col-xs-7">
                  <strong class="datatable">
                    <a v-bind:href="strategy['source']" target="_blank" rel="noopener noreferrer">@{{getDomain(strategy['source'])}}</a>
                  </strong>
                </div>
              </div>
            </div>
          </div>

          <div class="benchmarks pad-md">
            <h4 class="print-border">
              PERFORMANCE SUMMARY
              <div class="tableoption pull-right" v-bind:class="{'opened':showBenchmarkMenu1==1}" data-var="showBenchmarkMenu1">
                <button @click="showBenchmarkMenu1=showBenchmarkMenu1*-1" class="btn">
                  <span class="icon icon-cog"></span>
                </button>
                <div class="menu" v-show="showBenchmarkMenu1==1">
                  <p>Add performance metrics (@{{tableCols_benchmarksMax - tableCols_benchmarks_show.length}} left)</p>
                  <label v-for="(v,k) in labels" class="checkbox-style">
                    <input v-model="tableCols_benchmarks_show" v-bind:disabled="tableCols_benchmarks_show.indexOf(k) < 0 && tableCols_benchmarks_show.length >= tableCols_benchmarksMax"
                      type="checkbox" v-bind:value="k">
                    <i></i>
                    @{{v}}
                  </label>
                </div>
              </div>
            </h4>

            <div class="datatable" style="position: relative">
              <div class="loading-layer" v-show="changeCheckBox">
                  <div class="abs-loader">
                    <div class="loader-inner ball-pulse">
                      <div></div>
                      <div></div>
                      <div></div>
                    </div>
                  </div>
              </div>
              <div class="data-row-parent">
                <div class="data-row heading nopadding">
                  <div style="width:25%" class="namecol">
                    <button class="btn" @click="changeSortby_benchmark('title')">
                      Name
                    </button>
                  </div>
                  <div v-bind:style="{width: (74/tableCols_benchmarksCount)+'%'}" v-for="(field, idx) in tableCols_benchmarks_show" v-bind:class="{'highlight':periodKeysForFilters[period]+'_'+field ==tableCols_benchmarks_sortBy ,'bg-gray':idx%2==0}">
                    <button class="btn" @click="changeSortby_benchmark(periodKeysForFilters[period]+'_'+field)">
                      @{{labels[field] }}

                      <span v-show="tableCols_benchmarks_sortBy!=(periodKeysForFilters[period]+'_'+field)">
                        <i class="fa fa-sort"></i>
                      </span>
                      <span v-show="tableCols_benchmarks_sortBy==(periodKeysForFilters[period]+'_'+field)">
                        <i v-bind:class="{
                                          'fa-sort-up':(tableCols_benchmarks_sortOrder>0),'fa-sort-down':(tableCols_benchmarks_sortOrder<0)
                                          }" class="fa"></i>
                      </span>

                    </button>
                  </div>
                </div>
              </div>

              <div class="clearfix">

                <div v-for="(benchmark, idx) in benchmarksActiveData" v-bind:key="idx" class="data-row-parent">
                  <div class="data-row">
                    <div style="width:25%">
                      <div class="btn option" v-bind:class="{'text-white':benchmark.color!=''}" v-bind:style="{'background-color':colorsAll[benchmark.color]}">
                        <a v-show="(benchmark.color!='')" @click="removeBenchmark(benchmark)" class="pull-right">
                          <i class="fa fa-remove"></i>
                        </a>
                        <strong v-show="benchmark.code!=strategy.code">@{{benchmark.title}} <span v-if="benchmark[periodKeysForFilters[period] + '_active'] === 0"> (no data for @{{periodKeysForFilters[period]}})</span></strong>

                        <strong v-show="benchmark.code==strategy.code">@{{benchmark.title}}</strong>
                      </div>

                    </div>
                    <div v-bind:style="{width: (74/tableCols_benchmarksCount)+'%'}" class="text-center" v-for="(field, idxInLoop) in tableCols_benchmarks_show"
                      v-bind:class="{'highlight':periodKeysForFilters[period]+'_'+field ==tableCols_benchmarks_sortBy ,'bg-gray':idxInLoop%2==0}">
                      @{{ benchmark[periodKeysForFilters[period] + '_' + field] | round_4dp | unit(benchmark['um_' + field]) }}
                    </div>
                  </div>
                </div>

                {{-- benchmark menu --}}
                <div class="data-row print-hide">
                  <div style="width:25%">

                    <div class="tableoption pad-top" v-bind:class="{'opened':showBenchmarkMenu2==1}" data-var="showBenchmarkMenu2">
                      <button @click="showBenchmarkMenu2=showBenchmarkMenu2*-1" class="btn btn-gray form-control">
                        Compare
                        <i class="fa fa-plus pull-right"></i>
                      </button>
                      <div class="menu" v-show="showBenchmarkMenu2==1">
                        <p>Add benchmarks (@{{benchmarkLeftItems}} left)</p>

                        <strong>POPULAR BENCHMARKS</strong>
                        <label class="checkbox-style" v-for="option in benchmarks">
                          <input type="checkbox" v-bind:disabled="benchmarksActive.benchmarks.indexOf(option.id)<0 && benchmarkLeftItems<= 0"
                            v-model="benchmarksActive.benchmarks" v-bind:value="option.id">
                          <i></i>
                          @{{ option.shortName }} (@{{option.code}})
                        </label>
                        <hr class="dotted white">
                        <strong>YOUR PORTFOLIOS</strong>
                        <label class="checkbox-style" v-for="option in filteredPortfolios">
                          <input type="checkbox" v-bind:disabled="benchmarksActive.portfolios.indexOf(option.id)<0 && benchmarkLeftItems<=0"
                            v-model="benchmarksActive.portfolios" v-bind:value="option.id">
                          <i></i>
                          @{{ option.title }}
                        </label>
                      </div>
                    </div>
                  </div>
                  <div style="width:25%">
                    <h5 v-if="benchmarksActiveMax-benchmarksActiveData.length>0" class="text-gray pad-top">(@{{benchmarksActiveMax-benchmarksActiveData.length}} left)</h5>
                  </div>
                </div>

              </div>
            </div>
          </div>

          <div class="track animatelines" v-if="strategy.is_suspended!==1" style="position:relative">
            <h4 class="print-border">
              TRACK RECORD
              <span class="pull-right date-disclaimer">As of @{{ asOf }}</span>
            </h4>

            <chart-track :datasets="benchmarksActiveData" :main="strategy" :period="periodKeysForFilters[period]" title="" type="strategy"
              :colors="colorsAll" :loading="changeCheckBox"></chart-track>
          </div>

        </div>
      </div>

      <div class="print-header"></div>


      <div class="clearfix bg-gray pad-md lowercharts lowercharts-1" transition="fade">
        <div class="container">

          <div class="row">
            @include('partials.strategies.all_charts', ['pos' => 0, 'chart' => $templateOptions['chartOptions'][0], 'type'=>'strategy'])
            @include('partials.strategies.all_charts', ['pos' => 1, 'chart' => $templateOptions['chartOptions'][1], 'type'=>'strategy'])
          </div>
          <div class="row pad-top">
            @include('partials.strategies.all_charts', ['pos' => 2,'chart' => $templateOptions['chartOptions'][2], 'type'=>'strategy'])
            @include('partials.strategies.all_charts', ['pos' => 3,'chart' => $templateOptions['chartOptions'][3], 'type'=>'strategy'])
          </div>
          <div v-if="num_widgets === 6" class="row pad-top">
            @include('partials.strategies.all_charts', ['pos' => 4, 'chart' => $templateOptions['chartOptions'][4], 'type'=>'strategy'])
            @include('partials.strategies.all_charts', ['pos' => 5, 'chart' => $templateOptions['chartOptions'][5], 'type'=>'strategy'])
          </div>

        </div>
      </div>

      <div class="print-header" v-if="num_widgets > 6"></div>

      <div v-if="num_widgets > 6" class="clearfix bg-gray pad-md lowercharts lowercharts-2" transition="fade">
        <div class="container">

          <div class="row pad-top">
            @include('partials.strategies.all_charts', ['pos' => 4, 'chart' => $templateOptions['chartOptions'][4], 'type'=>'strategy'])
            @include('partials.strategies.all_charts', ['pos' => 5, 'chart' => $templateOptions['chartOptions'][5], 'type'=>'strategy'])
          </div>

          {{-- <div v-if="num_widgets === 8" class="row pad-top">
            @include('partials.strategies.all_charts', ['pos' => 6, 'order' => 1, 'type'=>'strategy'])
            @include('partials.strategies.all_charts', ['pos' => 7, 'order' => 1, 'type'=>'strategy'])
          </div> --}}

        </div>
      </div>

      <div class="print-header"></div>


      <div class="container">
        <div class="col-sm-10 col-sm-offset-1 col-xs-12">
          <div class="performance pad-md" style="padding-bottom: 0;">

            <h4 class="print-border">
              MONTHLY RETURN (%)
            </h4>
            <div class="table-responsive">
              <table cellspacing="0" class="table pad-md">
                <thead>
                  <tr>
                    <td></td>
                    <td class="bg-gray">
                      Year
                    </td>
                    <td>
                      Jan
                    </td>
                    <td>
                      Feb
                    </td>
                    <td>
                      Mar
                    </td>
                    <td>
                      Apr
                    </td>
                    <td>
                      May
                    </td>
                    <td>
                      Jun
                    </td>
                    <td>
                      Jul
                    </td>
                    <td>
                      Aug
                    </td>
                    <td>
                      Sep
                    </td>
                    <td>
                      Oct
                    </td>
                    <td>
                      Nov
                    </td>
                    <td>
                      Dec
                    </td>
                  </tr>
                </thead>

                @foreach ($monthlyReturns as $year => $monthly_values)
                <tr>
                  <td>{{ $year }}</td>
                  <td class="bg-gray <?= (isset($monthly_values[0])&&$monthly_values[0]<0) ? " text-red ": " " ?>">
                    <?= (isset($monthly_values[0])) ? $monthly_values[0] : '-' ?>
                  </td>
                  @for ($i = 1; $i<=12; $i++)
                  <td class="<?= (isset($monthly_values[$i])&&$monthly_values[$i]<0) ? " text-red ": " " ?>">
                    <?= (isset($monthly_values[$i])) ? $monthly_values[$i] : '-' ?>
                  </td>
                  @endfor
                </tr>
                @endforeach
              </table>
            </div>
          </div>
        </div>
        <div class="col-sm-8 col-sm-offset-2 col-xs-12">
          <div class="performance ">
            <h4 class="print-border">
              RISK METRICS
              <span class="date-disclaimer">As of @{{ maxDate }}</span>
              <div class="tableoption pull-right" v-bind:class="{'opened':showPerformanceMenu==1}">
                <button @click="showPerformanceMenu=showPerformanceMenu*-1" class="btn">
                  <span class="icon icon-cog"></span>
                </button>
                <div class="menu" v-show="showPerformanceMenu==1">
                  <p>Select time periods</p>
                  <label v-for="(v,k) in tableCols_performance" class="checkbox-style" v-if="strategy[v + '_active'] > 0">
                    <input type="checkbox" v-model="tableCols_performance_show" v-bind:value="k">
                    <i></i>
                    @{{k}}
                  </label>
                  <p>Add performance metrics (@{{tableRows_performanceMax - tableRows_performance_show.length}} left)</p>
                  <label v-for="(v,k) in labels" class="checkbox-style">
                    <input v-model="tableRows_performance_show" v-bind:disabled="tableRows_performance_show.indexOf(k) < 0 && tableRows_performance_show.length >= tableRows_performanceMax"
                      type="checkbox" v-bind:value="k">
                    <i></i>
                    @{{v}}
                  </label>
                </div>
              </div>
            </h4>

            <div class="datatable">
              <div class="data-row-parent">
                <div class="clearfix heading nopadding">
                  <div style="width:25%" class="namecol">
                    <span class="btn-noclick text-left">Measure</span>
                  </div>
                  <div v-bind:style="{width: (74/tableCols_performanceCount)+'%'}" v-for="(field, idx) in tableCols_performance_show_ordered"
                    v-bind:class="{'bg-gray':idx%2==0}">
                    <span class="btn-noclick">@{{field}}</span>

                  </div>
                </div>
              </div>

              <div class="clearfix">
                <div v-for="measure in tableRows_performance_show" class="data-row-parent">
                  <div class="data-row">
                    <div style="width:25%" class="namerow">
                      @{{ labels[measure] }}
                    </div>
                    <div v-bind:style="{width: (74/tableCols_performanceCount)+'%'}" class="text-center" v-for="(fieldPeriod, idx) in tableCols_performance_show"
                      v-bind:class="{'bg-gray':idx%2==0}">
                      @{{ strategy[tableCols_performance[fieldPeriod] + '_' + measure] | round_4dp | unit(strategy['um_' + measure]) }}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <?php if (count($related) > 0) { ?>
          <div class="clearfix related pad-top">
            <h4>
              RELATED STRATEGIES
            </h4>
            @foreach ($related as $relatedStrategy)
            <a class="btn btn-outline" href="/strategy/{{ $relatedStrategy->code }}">{{ $relatedStrategy->longName }}</a>
            @endforeach
          </div>
          <?php } ?>
        </div>
      </div>



      <div class="footer">
        <div class="container disclaimer pad-top" v-bind:class="user.disclaimer && 'print-hide' ">
          {!!$disclaimer!!}
        </div>
        <div v-if="strategy.disclaimer" v-bind:class="user.disclaimer && 'print-hide'">
          <div class="print-header"></div>
          <div class="container disclaimer text-break-line">
            @{{strategy.disclaimer}}
          </div>
        </div>
        <div v-if="user.disclaimer" class="container disclaimer text-break-line print-only">
          @{{user.disclaimer}}
        </div>
      </div>
    </div>

    <add-strategies :indices-basket="indicesBasket" :period="periodKeysForFilters[period]" :user="user" close-btn-label="Continue viewing strategy"></add-strategies>

    @include('spark::nav.footer')
  </div>
</strategy-detail>

@endsection
