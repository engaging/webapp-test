<!-- Modal -->
<div class="modal fade" id="lab_selector" tabindex="-1" role="dialog" aria-labelledby="lab_selector">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-body">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<labselector :user="user" inline-template>
					<div class="row">
						<div class="col-sm-6">
							<h4 class="modal-title text-center" id="myModalLabel">Choose</h4>
							<hr>
							<div class="row form-group">
								<div class="col-sm-4">
									<button class="btn btn-block" v-bind:class="{'btn-primary':selectedType=='portfolios','btn-outline-grey':selectedType!='portfolios'}" @click="selectType('portfolios')">Portfolios</button>
								</div>
								<div class="col-sm-4">
									<button class="btn btn-block" v-bind:class="{'btn-primary':selectedType=='benchmarks','btn-outline-grey':selectedType!='benchmarks'}" @click="selectType('benchmarks')">Benchmarks</button>
								</div>
								<div class="col-sm-4">
									<button class="btn btn-block" v-bind:class="{'btn-primary':selectedType=='strategies','btn-outline-grey':selectedType!='strategies'}" @click="selectType('strategies')">Strategies</button>
								</div>
							</div>
							<div class="form-group">
								<input type="text" class="form-control search-input" placeholder="Keyword Search" v-model="keyword" />
								<div class="text-center">
									<a href="javascript:void(0);return false;" class="" @click="keyword=''"><b>Clear</b></a>
								</div>
							</div>
							<div class="strategy-list ">
								<div data-var="showTableOption" class="tableoption float pull-right">
									<button class="btn"><span class="icon icon-cog"></span></button> 
								</div> 
								<div class="datatable">
									<div class="clearfix heading">
										<div class="text-center" style="width: 5%;">
											<span class="text-gray"></span>
										</div> 
										<div class="namecol" style="width: 35%;">
											<button class="btn">Name</button></div> 
											<div class="" style="width: 15%;">
												<button class="btn">6m Return p.a.</button>
											</div>
										</div> 
										<div class="scrolling">
											<div class="data-row-parent">
												<div class="data-row bg-gray">
													<div class="text-center" style="width: 5%;">
														<span class="text-gray">
															<input type="checkbox">
														</span>
													</div> 
													<div style="width: 35%;">
														<h5 class="list-group-item-heading">
															<a href="/strategy/ABGSRP38" target="_blank" rel="noopener noreferrer">US Value</a>
														</h5> 
														<span class="text-gray">16 strategies</span>
													</div>  
													<div class="text-center" style="width: 15%; ">
														14.84%
													</div>
												</div>
											</div> 
											<div class="data-row-parent">
												<div class="data-row bg-gray">
													<div class="text-center" style="width: 5%;">
														<span class="text-gray">
															<input type="checkbox">
														</span>
													</div> 
													<div style="width: 35%;">
														<h5 class="list-group-item-heading">
															<a href="/strategy/ABGSRP38" target="_blank" rel="noopener noreferrer">US Value</a>
														</h5> 
														<span class="text-gray">16 strategies</span>
													</div>  
													<div class="text-center" style="width: 15%; ">
														14.84%
													</div>
												</div>
											</div> 
											<div class="data-row-parent">
												<div class="data-row bg-gray">
													<div class="text-center" style="width: 5%;">
														<span class="text-gray">
															<input type="checkbox">
														</span>
													</div> 
													<div style="width: 35%;">
														<h5 class="list-group-item-heading">
															<a href="/strategy/ABGSRP38" target="_blank" rel="noopener noreferrer">US Value</a>
														</h5> 
														<span class="text-gray">16 strategies</span>
													</div>  
													<div class="text-center" style="width: 15%; ">
														14.84%
													</div>
												</div>
											</div> 


										</div>
									</div>
								</div>

							</div>
							<div class="col-sm-6">
								<h4 class="modal-title text-center" id="myModalLabel">Analysis Tool</h4>
								<hr>
								<div class="analysis-tools col-sm-12">
									<div class="form-group">
										<button class="btn btn-block text-left" v-bind:class="{'btn-primary':selectedTool=='backtesting','btn-outline-blue':selectedTool!='backtesting'}" @click="selectTool('backtesting')">
										<i class="ico ico-backtesting " v-bind:class="{'active':selectedTool=='backtesting'}"></i><b>Backtesting</b><p>Advaned computation portfolio construction engine for taliored simulations</p></button>
									</div>
									<div class="form-group">
										<button class="btn btn-block text-left" v-bind:class="{'btn-primary':selectedTool=='pca','btn-outline-blue':selectedTool!='pca'}" @click="selectTool('pca')">
											<i class="ico ico-pca" v-bind:class="{'active':selectedTool=='pca'}"></i><b>PCA</b><p>Advaned computation portfolio construction engine for taliored simulations</p></button></button>
									</div>
									<div class="form-group">
										<button class="btn btn-block text-left" v-bind:class="{'btn-primary':selectedTool=='regression','btn-outline-blue':selectedTool!='regression'}" @click="selectTool('regression')">
											<i class="ico ico-factor" v-bind:class="{'active':selectedTool=='regression'}"></i><b>Factor Angalysis</b><p>Advaned computation portfolio construction engine for taliored simulations</p></button></button>
									</div>
								</div>

								<div class="col-sm-6 col-sm-offset-3"><button class="btn btn-primary btn-lg btn-block" :disabled="!validState" @click="submit()">Analyse Now</button></div>
							</div>
						</div>
					</labselector>
					</div>
				</div>
			</div>
		</div>

