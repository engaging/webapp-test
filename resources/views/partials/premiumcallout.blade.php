<div class="premium-callout {{$class}}">
	@if ($class=='blue')
		<h3>Upgrade to Premium</h3>
	@endif
		


	<div class="clearfix">
		<div class="col-sm-6">
			<div class="row">
				<div class="col1"><img src="/img/design/icon-check-lg.png" alt></div>
				<div class="col2">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi sed lob</div>
			</div>
			<div class="row">
				<div class="col1"><img src="/img/design/icon-check-lg.png" alt></div>
				<div class="col2">Sed placerat nisl pharetra, aliquam est molestie, tempor odio. Etiam sit  </div>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="row">
				<div class="col1"><img src="/img/design/icon-check-lg.png" alt></div>
				<div class="col2">Aenean cursus non augue id pr mi quis enim finibus, a elementum eros variu</div>
			</div>
			<div class="row">
				<div class="col1"><img src="/img/design/icon-check-lg.png" alt></div>
				<div class="col2">laoreet dignissim neque. Aenean blandit egestas orci, eu posuere erat cursus ac. Nullam risus neque, lobortis quis posu</div>
			</div>
		</div>
	</div>

	<div class="clearfix text-center">
		<a href="/premium" class="btn btn-lg btn-primary"><strong>UPGRADE NOW</strong></a>
	</div>
</div>
