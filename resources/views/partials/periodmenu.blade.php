{{-- <div class="periodmenu gray">
	<button v-for="(v,k) in periodKeysForFilters" v-if="(!(strategy||portfolio) || (strategy||portfolio)[v + '_active'])" v-bind:class="{'btn-default': (period==k) }" @click="setPeriod(k)" class="btn periodkey" href="#">@{{v}}</button>
</div>
<div class="periodmenu {{$class}}">
	<button v-for="(v,k) in periodKeysForFilters" v-bind:class="{'btn-default': (period==k),'insufficient-data': !(!(strategy||portfolio) || (strategy||portfolio)[v + '_active']) }" @click="setPeriod(k)" class="btn periodkey" href="#">@{{v}}</button>
</div>
--}}

<div class="periodmenu-dropdown {{$class}}">
	<span>Time Period</span>
	<div class="dropdown">
	  <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
	    @{{periodKeysForFilters[period]}}
	    <span class="caret"></span>
	  </button>
	  <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
	    <li v-for="(v,k) in periodKeysForFilters">
	    	<a v-if="(!(strategy||portfolio) || (strategy||portfolio)[v + '_active'])" @click="setPeriod(k)">@{{v}}</a>
	    	<a v-if="!(!(strategy||portfolio) || (strategy||portfolio)[v + '_active']) && {{(!isset($inactive) || $inactive === 'disabled')  ? 'true' : 'false'}}" class="disabled">@{{v}}</a>
	    	<a v-if="!(!(strategy||portfolio) || (strategy||portfolio)[v + '_active']) && {{(isset($inactive) && $inactive === 'clickable') ? 'true' : 'false'}}" class="disabled" @click="setPeriod(k)">@{{v}}</a>
	    </li>
	  </ul>
	</div>
</div>
