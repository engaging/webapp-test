{{-- <div class="periodmenu gray">
	<button v-for="(v,k) in periodKeysForFilters" v-if="(!(strategy||portfolio) || (strategy||portfolio)[v + '_active'])" v-bind:class="{'btn-default': (period==k) }" @click="setPeriod(k)" class="btn periodkey" href="#">@{{v}}</button>
</div>
<div class="periodmenu {{$class}}">
	<button v-for="(v,k) in periodKeysForFilters" v-bind:class="{'btn-default': (period==k),'insufficient-data': !(!(strategy||portfolio) || (strategy||portfolio)[v + '_active']) }" @click="setPeriod(k)" class="btn periodkey" href="#">@{{v}}</button>
</div> --}}

<div class="periodmenu-dropdown periodmenu-dropdown-custdates pull-right {{$class}}">
	<div class="row">
  	<div class="col-sm-7">
      <div class="periodmenu-label">
        Time Period
      </div>
    </div>
    <div class="col-sm-5">
    	<div class="dropdown">
    	  <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
    	    @{{periodKeysForFilters[period]}}
    	    <span class="caret"></span>
    	  </button>
    	  <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
    	    <li v-for="(v,k) in periodKeysForFilters">
    	    	<a v-if="k === 'Cust'" @click="setPeriod(k)">@{{v}}</a>
    	    	<a v-if="k !== 'Cust' && (!(strategy||portfolio) || (strategy||portfolio)[v + '_active'] !== 0)" @click="setPeriod(k)">@{{v}}</a>
    	    	<a v-if="k !== 'Cust' && !(!(strategy||portfolio) || (strategy||portfolio)[v + '_active'] !== 0) && {{(!isset($inactive) || $inactive === 'disabled')  ? 'true' : 'false'}}" class="disabled">@{{v}}</a>
    	    	<a v-if="k !== 'Cust' && !(!(strategy||portfolio) || (strategy||portfolio)[v + '_active'] !== 0) && {{(isset($inactive) && $inactive === 'clickable') ? 'true' : 'false'}}" class="disabled" @click="setPeriod(k)">@{{v}}</a>
    	    </li>
    	  </ul>
    	</div>
    </div>
	</div>
  <div class="row no-gutter">
    <div class="col-sm-12">
      {{--<div class="form-group" style="margin: 0;">
        <input type="date" class="form-control" v-model="periodDates.from" :min="periods.min" :max="periodDates.to" :disabled="period !== 'Cust'">
      </div>
      <div class="form-group" style="margin: 0; margin-top: 1px;">
        <input type="date" class="form-control" v-model="periodDates.to" :min="periodDates.from" :max="maxDate" :disabled="period !== 'Cust'">
      </div>--}}

  	  <date-picker
  	  	v-model="datePickerDates"
  	  	:lang="'en'"
  	  	:shortcuts="false"
  	  	:not-before="periods.min"
  	  	:not-after="maxDate"
  	  	:range-separator="'to'"
  	  	:input-class="'mx-input hidden'"
  	  	:confirm="true"
  	  	:confirm-text="'Update'"
  	  	:cancel="true"
  	  	:show-current="true"
  	  	:editable="true"
  	  	date-picker-type="slider"
  	  	range
        @confirm="applyCustomDates"
  	  ></date-picker>
	    <div class="row display-dates no-gutter cursor-pointer unselectable" @click="showDatePicker">
	        <div class="col-sm-5">
		        @{{ periodDates.from }}
	        </div>
	        <div class="col-sm-2 to-date-separator">
		        to
	        </div>
	        <div class="pull-right">
		        @{{ periodDates.to }}
	        </div>
	    </div>
    </div>
  </div>
</div>
