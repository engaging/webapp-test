<div class="strategy-list row panel bg-gray text-center hidden-xs" :class="{ 'pinned-chart': showPinnedGraphics }" style="padding-top:15px;" v-if="pinnedIndices.length <= 0" transition="fade">
	<div class="row no-pins">
	Click or <span class="inline-pin"><svg-icon :color-all="pinnedColorsAll" :pinned-index="-1" :colors="pinnedColorOrder"></svg-icon></span> to compare up to @{{pinnedIndicesMax}} strategies
	</div>
</div>
<div class="strategy-list row strategy-list-pinned panel bg-gray" v-show="pinnedIndices.length > 0" transition="fade">
	<div class="pinned-radar-chart bg-gray" :class="{ 'pinned-chart': showPinnedGraphics }" transition="fade">	
		<div>
			<div class="row pad-top">
        <div class="col-sm-5 col-xs-12">
          <h4 class="label" transition="fade">@{{pinnedIndices.length}} SELECTED <span v-if="pinnedIndices.length == 1">STRATEGY</span><span v-else>STRATEGIES</span> <span>(@{{pinnedIndicesMax - pinnedIndices.length}} LEFT)</span></h4>
        </div>
        <div class="col-sm-2 text-center"> 
          <a class="btn btn-white toggleGraphics" @click="showPinnedGraphics = !showPinnedGraphics"><i class="fa fa-angle-up" v-if="!showPinnedGraphics"></i><i class="fa fa-angle-down" v-else></i> <span v-if="!showPinnedGraphics">Graphic View</span><span v-else>Hide Graphics</span></a>
        </div>
        <div class="col-sm-5 col-xs-12">
          <a class="clear-all-pinned" @click="unpinAll">Clear All</a>
        </div>
		  </div>
      <chart-radar
            v-if="pinnedIndices.length > 0 && showPinnedGraphics"
		       :datasets="pinnedIndices"
		       :main="strategy?strategy:portfolio"
		       :period="periodKeysForFilters[period]"
           :idname="'radar-pinned'"
           :colors="pinnedColorOrder"
           :pinned-colors="pinnedColorsAll"
           :loading="true"
           :menu="true"
           :preference="preference.tools"
           :set-preference="setPreference"
		       title=""
           type="pinned"
       ></chart-radar>
      <div class="pinned-legend" v-if="showPinnedGraphics">
        <div class="tag" v-for="(color, pinnedId) in pinnedColorOrder" :key="pinnedId" :style="setPinnedStyle(pinnedId)">
          @{{ getPinnedName(pinnedId)}}
          <i class="fa fa-times" @click="unpinStringIndex(pinnedId)"></i>
          </div>
      </div>
		</div>
	</div>
  <div class="datatable">

    <div class="clearfix heading">
      <div style="width:5%" class="text-center">
      </div>
      <div style="width:34%" class="namecol">
        <button class="btn" @click="changeSortby('shortName')">
          Name
          <span v-show="sortby!='shortName'">
            <i class="fa fa-sort"></i>
          </span>
          <span v-show="sortby=='shortName'">
            <i v-bind:class="{
        				    	'fa-sort-up':(sortorder>0),'fa-sort-down':(sortorder<0)
        				    	}" class="fa"></i>
          </span>
        </button>
      </div>
      <div style="width:9%">
      </div>
      <div v-bind:style="{width: (47/dataTableColsCount)+'%'}" v-for="(header, field) in labels" v-show="dataTableCols_raw.indexOf(field)>=0"
        v-bind:class="{'highlight':(sortby==periodKeysForFilters[period] + '_'+field)}">
        <button @click="changeSortby(periodKeysForFilters[period] + '_' + field)" class="btn">
          @{{header}}
          <span v-show="sortby!=periodKeysForFilters[period] + '_'+ field">
            <i class="fa fa-sort pull-right"></i>
          </span>
          <span v-show="sortby==periodKeysForFilters[period] + '_'+ field">
            <i v-bind:class="{
        				    	'fa-sort-up':(sortorder>0),'fa-sort-down':(sortorder<0)
        				    	}" class="fa pull-right"></i>
          </span>
        </button>
      </div>
    </div>

    <div v-for="index in pinnedIndices" class="data-row-parent" :key="index.id">
      @include('partials.strategies.discover-item-pinned')
    </div>
  </div>
</div>