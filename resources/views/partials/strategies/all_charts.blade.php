<div class="col-sm-6 col-xs-12">

   <div class="dropdown graph-options print-hide pull-right">
     <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu{{$pos}}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
       <i class="fa fa-ellipsis-v"></i>
     </button>

     <ul class="dropdown-menu" aria-labelledby="dropdownMenu{{$pos}}">
       <li v-for="(val, key) in widgetsAvailable"><a v-bind:class="{'active':widgetConfig[{{$pos}}]===key}" @click="setWidgetConfig({{$pos}},key)">@{{ val }}</a></li>
     </ul>
   </div>


   <chart-radar
      v-if="widgetConfig[{{$chart['order']}}]==='chart-radar'"
       :datasets="benchmarksActiveData"
       :main="strategy?strategy:portfolio"
       :period="periodKeysForFilters[period]"
       :colors="colorsAll"
       :idname="'radar-{{ $pos }}'"
       :loading="changeCheckBox"
       title="METRICS"
       type="{{$type}}"
   ></chart-radar>
   <chart-drawdowntrack
      v-if="widgetConfig[{{$chart['order']}}]==='chart-drawdowntrack'"
       :datasets="benchmarksActiveData"
       :main="strategy?strategy:portfolio"
       :period="periodKeysForFilters[period]"
       :colors="colorsAll"
       :idname="'radar-{{ $pos }}'"
       :loading="changeCheckBox"
       title="DRAWDOWN"
       type="{{$type}}"
   ></chart-drawdowntrack>
   <chart-volatilitytrack
      v-if="widgetConfig[{{$chart['order']}}]==='chart-volatilitytrack'"
       :datasets="benchmarksActiveData"
       :period="periodKeysForFilters[period]"
       :colors="colorsAll"
       :idname="'radar-{{ $pos }}'"
       :loading-list="volatilityLoadingList"
       :pdfoptions='widgetConfig[{{$chart["order"]}}]==="chart-volatilitytrack" ? {!! $chart["options"] !!} : null'
       title="VOLATILITY"
       :idx="{{$pos}}"
       :measure="volatilityDay"
       type="{{$type}}"
   ></chart-volatilitytrack>
   <chart-returnbar
      v-if="widgetConfig[{{$chart['order']}}]==='chart-returnbar'"
       :main="strategyDetails?strategyDetails:portfolioDetails"
       :period="periodKeysForFilters[period]"
       :idname="'radar-{{ $pos }}'"
       title="MONTHLY RETURN DISTRIBUTION"
       type="{{$type}}"
   ></chart-returnbar>
   <chart-returnscatter
      v-if="widgetConfig[{{$chart['order']}}]==='chart-returnscatter'"
       :main="strategyDetails?strategyDetails:portfolioDetails"
       :period="periodKeysForFilters[period]"
       :idname="'radar-{{ $pos }}'"
       :base-payload="basePayload"
       :pdfoptions='widgetConfig[{{$chart["order"]}}]==="chart-returnscatter" ? {!! $chart["options"] !!} : null'
       title="MONTHLY RETURN REGRESSION"
       type="{{$type}}"
   ></chart-returnscatter>
   <chart-correlationscatter
      v-if="widgetConfig[{{$chart['order']}}]==='chart-correlationscatter' && benchmarksLoaded"
       :benchmarks="benchmarks"
       :main="strategyDetails?strategyDetails:portfolioDetails"
       :period="periodKeysForFilters[period]"
       :idname="'radar-{{ $pos }}'"
       :base-payload="basePayload"
       :is-subgraphy="true"
       :pdfoptions='widgetConfig[{{$chart["order"]}}]==="chart-correlationscatter" ? {!! $chart["options"] !!} : null'
       title="CORRELATION"
       type="{{$type}}"
   ></chart-correlationscatter>
   <chart-contribution
      v-if="widgetConfig[{{$chart['order']}}]==='chart-varcontribution'"
      :main="portfolioDetails"
      :indices="indices"
      :period="periodKeysForFilters[period]"
      :idname="'radar-{{ $pos }}'"
      title="VaR CONTRIBUTION"
      type="var"
   ></chart-contribution>
   <chart-contribution
   v-if="widgetConfig[{{$chart['order']}}]==='chart-returncontribution'"
   :main="portfolioDetails"
   :indices="indices"
   :period="periodKeysForFilters[period]"
   :idname="'radar-{{ $pos }}'"
   title="RETURN CONTRIBUTION"
   type="return"
  ></chart-contribution>
  <chart-contribution
  v-if="widgetConfig[{{$chart['order']}}]==='chart-volatilitycontribution'"
  :main="portfolioDetails"
  :indices="indices"
  :period="periodKeysForFilters[period]"
  :idname="'radar-{{ $pos }}'"
  title="VOLATILITY CONTRIBUTION"
  type="volatility"
  ></chart-contribution>
  <chart-weighting
  v-if="widgetConfig[{{$chart['order']}}]==='chart-weighting'"
  :main="portfolioDetails"
  :indices="indices"
  :period="periodKeysForFilters[period]"
  :idname="'radar-{{ $pos }}'"
  title="WEIGHTING"
  type="{{$type}}"
  ></chart-weighting>
  <chart-leverage
  v-if="widgetConfig[{{$chart['order']}}]==='chart-leverage'"
  :main="portfolioDetails"
  :indices="indices"
  :period="periodKeysForFilters[period]"
  :idname="'radar-{{ $pos }}'"
  title="LEVERAGE"
  type="{{$type}}"
  ></chart-leverage>

    {{--  <div class="warning print-hide" v-if="widgetConfig[{{$pos}}]=='chart-correlationscatter' && monthlyXYBenchmark_1[periodKeysForFilters[period] + '_active'] == 0 || monthlyXYBenchmark_2[periodKeysForFilters[period] + '_active'] == 0">
       <div class="row">
           <div class="col-sm-8 col-sm-offset-2">
               <p>Sorry, your selected benchmarks have insufficient data for the time period you have selected. Please select a different benchmark, portfolio, or time period.</p>
           </div>
       </div>
   </div>  --}}
</div>
