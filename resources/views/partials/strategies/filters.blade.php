<form v-show="filtersLoaded == true" transition="fade">

  <div class="form-group" v-if="hideFilters.indexOf('keywords')==-1" style="position:relative;">
     <input type="text" placeholder="Keyword Search" class="form-control" :class="{ 'search-input': keywords.length === 0 }" v-model="keywords">
     <div class="remove-search-input" v-if="keywords.length > 0" @click="keywords=''"></div>
  </div>

  <div :class="['form-group', {inactive:false}]"  v-if="hideFilters.indexOf('return')==-1">
     <label class="top">@{{ slider2Text }}<span>@{{ slider2Value }}</span>
     </label>
     <div class="slider-parent" v-on:mousedown="clearSearchTimeout()">
     	<input id="ex2" type="text" class="span2" value="" data-provide="slider" v-bind:data-slider-min="sliderRange2[0]" v-bind:data-slider-max="sliderRange2[1]" v-bind:data-slider-step="slider2Step" v-bind:data-slider-value="'['+sliderRange2+']'" v-model="slider2" data-slider-tooltip="hide"/>
     </div>
  </div>

  <div v-bind:class="['form-group', {inactive:false}]" v-if="hideFilters.indexOf('volatility')==-1">
    <label class="top">@{{ slider1Text }}<span>@{{ slider1Value }}</span>
    </label>
    <div class="slider-parent" v-on:mousedown="clearSearchTimeout()">
       <input id="ex1" type="text" class="span2" value="" data-provide="slider" v-bind:data-slider-min="sliderRange1[0]" v-bind:data-slider-max="sliderRange1[1]" v-bind:data-slider-step="slider1Step" v-bind:data-slider-value="'['+sliderRange1+']'" v-model="slider1" data-slider-tooltip="hide"/>
    </div>
  </div>

  <div :class="['form-group', {inactive:false}]"  v-if="hideFilters.indexOf('sharpe')==-1">
     <label class="top">Sharpe <span>@{{ slider3Value }}</span>
     </label>
     <div class="slider-parent" v-on:mousedown="clearSearchTimeout()">
     	<input id="ex3" type="text" class="span2" value="" data-provide="slider" v-bind:data-slider-min="sliderRange3[0]" v-bind:data-slider-max="sliderRange3[1]" data-slider-step="0.05" v-bind:data-slider-value="'['+sliderRange3+']'" v-model="slider3" data-slider-tooltip="hide"/>
     </div>
  </div>

  <div :class="['border-bottom form-group', {inactive:FactorCheckboxInactive}]"  v-if="hideFilters.indexOf('factor')==-1">
  	<div>
        <label class="top" @click="toggle('FactorCheckboxInactive')">Factor
        	<span class="selected" v-if="factors.length>0">(@{{factors.length}} selected)</span>
	        <a transition="fade" v-show="editing==true" >
	        	<span v-show="FactorCheckboxInactive==true"><i class="fa fa-caret-up"></i></span>
	        	<span v-show="FactorCheckboxInactive!=true"><i class="fa fa-caret-down"></i></span>
	        </a>
        </label>
        <div class="filter-container" v-show="FactorCheckboxInactive!=true">
	        <div class="checkbox" v-for="factor in filters['factor']">
            <label class="checkbox-style">
              <input type="checkbox" v-model="factors" v-bind:value="factor"><i></i> @{{ factor }}
            </label>
			    </div>
		    </div>
	    </div>
  </div>

  <div :class="['border-bottom form-group', {inactive:AssetClassCheckboxInactive}]"  v-if="hideFilters.indexOf('assetClass')==-1">
     <label class="top" @click="toggle('AssetClassCheckboxInactive')">Asset Class
     	<span class="selected" v-if="assetClasses.length>0">(@{{assetClasses.length}} selected)</span>
     <a transition="fade"  v-show="editing==true" >
     	<span v-show="AssetClassCheckboxInactive==true"><i class="fa fa-caret-up"></i></span>
     	<span v-show="AssetClassCheckboxInactive!=true"><i class="fa fa-caret-down"></i></span>
     </a>
     </label>
     <div class="filter-container" v-show="AssetClassCheckboxInactive!=true">
        <div class="checkbox" v-for="assetClass in filters['assetClass']">
		    <label class="checkbox-style">
		      <input type="checkbox" v-model="assetClasses" v-bind:value="assetClass"><i></i>
		       @{{ assetClass }}
		    </label>
		</div>
	</div>
  </div>


  <div :class="['border-bottom form-group', {inactive:RegionCheckboxInactive}]"  v-if="hideFilters.indexOf('region')==-1">
     <label class="top" @click="toggle('RegionCheckboxInactive')">Region
     <span class="selected" v-if="regions.length>0">(@{{regions.length}} selected)</span>
     <a transition="fade" v-show="editing==true">
     	<span v-show="RegionCheckboxInactive==true"><i class="fa fa-caret-up"></i></span>
     	<span v-show="RegionCheckboxInactive!=true"><i class="fa fa-caret-down"></i></span>
     </a>
     </label>
     <div class="filter-container" v-show="RegionCheckboxInactive!=true">
        <div class="checkbox" v-for="region in filters['region']">
		    <label class="checkbox-style">
		      <input type="checkbox" v-model="regions" v-bind:value="region"><i></i> @{{ region }}
		    </label>
		</div>
	</div>
  </div>

  <div :class="['border-bottom form-group', {inactive:CurrencyCheckboxInactive}]" v-if="hideFilters.indexOf('currency')==-1">
     <label class="top" @click="toggle('CurrencyCheckboxInactive')">Currency
     	<span class="selected" v-if="currencies.length>0">(@{{currencies.length}} selected)</span>
     <a transition="fade" v-show="editing==true">
     	<span v-show="CurrencyCheckboxInactive==true"><i class="fa fa-caret-up"></i></span>
     	<span v-show="CurrencyCheckboxInactive!=true"><i class="fa fa-caret-down"></i></span>
     </a>
     </label>
     <div class="filter-container" v-show="CurrencyCheckboxInactive!=true">
        <div class="checkbox" v-for="currency in filters['currency']">
		    <label class="checkbox-style">
		      <input type="checkbox" v-model="currencies" v-bind:value="currency"><i></i> @{{ currency }}
		    </label>
		</div>
	</div>
  </div>

  <div :class="['border-bottom form-group', {inactive:StyleCheckboxInactive}]" v-if="hideFilters.indexOf('style')==-1">
  	<div>
        <label class="top" @click="toggle('StyleCheckboxInactive')">Style
        	<span class="selected" v-if="styles.length>0">(@{{styles.length}} selected)</span>
			<a transition="fade" v-show="editing==true">
        	<span v-show="StyleCheckboxInactive==true"><i class="fa fa-caret-up"></i></span>
        	<span v-show="StyleCheckboxInactive!=true"><i class="fa fa-caret-down"></i></span>
        </a>
        </label>
        <div class="filter-container" v-show="StyleCheckboxInactive!=true">
	        <div class="checkbox" v-for="style in filters['style']">
			    <label class="checkbox-style">
			      <input type="checkbox" v-model="styles" v-bind:value="style"><i></i> @{{ style }}
			    </label>
			</div>
		</div>
	</div>
  </div>

  <div :class="['border-bottom form-group', {inactive:TypeCheckboxInactive}]" v-if="hideFilters.indexOf('type')==-1">
  	<div>
        <label class="top" @click="toggle('TypeCheckboxInactive')">Type
        	<span class="selected" v-if="types.length>0">(@{{types.length}} selected)</span>
			<a transition="fade" v-show="editing==true">
        	<span v-show="TypeCheckboxInactive==true"><i class="fa fa-caret-up"></i></span>
        	<span v-show="TypeCheckboxInactive!=true"><i class="fa fa-caret-down"></i></span>
        </a>
        </label>
        <div class="filter-container" v-show="TypeCheckboxInactive!=true">
	        <div class="checkbox" v-for="type in filters['type']">
			    <label class="checkbox-style">
			      <input type="checkbox" v-model="types" v-bind:value="type"><i></i> @{{ type }}
			    </label>
			</div>
		</div>
	</div>
  </div>

  <div :class="['border-bottom form-group', {inactive:ReturnTypeCheckboxInactive}]" v-if="hideFilters.indexOf('returnType')==-1">
     <label class="top" @click="toggle('ReturnTypeCheckboxInactive')">Return
     	<span class="selected" v-if="returnTypes.length>0">(@{{returnTypes.length}} selected)</span>
		<a transition="fade" v-show="editing==true">
     	<span v-show="ReturnTypeCheckboxInactive==true"><i class="fa fa-caret-up"></i></span>
     	<span v-show="ReturnTypeCheckboxInactive!=true"><i class="fa fa-caret-down"></i></span>
     </a>
     </label>
     <div class="filter-container" v-show="ReturnTypeCheckboxInactive!=true">
        <div class="checkbox" v-for="returnType in filters['returnType']">
		    <label class="checkbox-style">
		      <input type="checkbox" v-model="returnTypes" v-bind:value="returnType"><i></i> @{{ returnType }}
		    </label>
		</div>
	</div>
  </div>

 <div :class="['border-bottom form-group', {inactive:HistoryCheckboxInactive}]"  v-if="hideFilters.indexOf('history')==-1">
  	<div>
          <label class="top" @click="toggle('HistoryCheckboxInactive')">History Start Date
          	<span class="selected" v-if="history.length>0">(1 selected)</span>
          <a transition="fade" v-show="editing==true">
          	<span v-show="HistoryCheckboxInactive==true"><i class="fa fa-caret-up"></i></span>
          	<span v-show="HistoryCheckboxInactive!=true"><i class="fa fa-caret-down"></i></span>
          </a>
          </label>
          <div class="filter-container" v-show="HistoryCheckboxInactive!=true">
  	        <div class="checkbox" v-for="historyItem in filters['history']">
  			    <label class="checkbox-style">
  			      <input type="checkbox" v-model="history" v-bind:value="historyItem"><i></i> @{{ historyItem }}
  			    </label>
  			</div>
  		</div>
  	</div>
    </div>
</form>
