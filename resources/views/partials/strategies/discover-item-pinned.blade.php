<div class="pinColumn">
  <a @click="unpinIndex(index)" class="btn pin">
    <svg-icon :color-all="pinnedColorsAll" :pinned-index="index.id" :colors="pinnedColorOrder"></svg-icon>
  </a>
</div>
<div class="data-row">
  <div style="width:5%" class="text-center">
    <span class="text-gray">@{{(indices.indexOf(index) === -1) ? '-' : (indices.indexOf(index)+1)}}</span>
  </div>
  <div style="width:35%">
    <h5 class="list-group-item-heading">
      <a v-bind:href="'/strategy/' + index.code" target="_blank" rel="noopener noreferrer">@{{ index.shortName }}</a>
    </h5>
    <span class="text-gray">@{{ index.assetClass }} | @{{ index.currency }} @{{ index.returnCategoryFormat }}@{{ index.returnTypeFormat }} @{{ index.volTarget
      }}</span>
    <span v-if="index.type != 'Strategy'">|
      <span :class="'highlight-' + getStrategyTypeForCss(index).toLowerCase()">@{{ getStrategyType(index) }}</span>
    </span>
  </div>
  <div style="width:8%">
    <div class="clearfix" style="white-space:nowrap">
      <a v-if="!index.is_suspended" class="btn btn-outline-blue" @click="saveStrategies([index])">Add</a>
      <a v-if="index.is_suspended" class="btn btn-outline-blue">Not updated</a>
    </div>
  </div>
  <div class="text-center" v-bind:style="{width: (47/dataTableColsCount)+'%'}" v-for="(header, field) in labels" v-show="dataTableCols_raw.indexOf(field)>=0"
    v-bind:class="{'highlight':(sortby==periodKeysForFilters[period] + '_' + field)}">
    @{{ index[periodKeysForFilters[period] + '_' + field] | round_4dp | unit(index['um_' + field]) }}
  </div>
</div>
