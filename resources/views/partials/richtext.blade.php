{{-- test richtext ed --}}
<div class="btn-toolbar editor-toolbar" data-role="editor-toolbar" data-target="#{{$divid}}">
	<div class="btn-group">
	    <a class="btn" data-edit="bold" title="Bold (Ctrl/Cmd+B)"><i class="fa fa-bold"></i></a>
	    <a class="btn" data-edit="italic" title="Italic (Ctrl/Cmd+I)"><i class="fa fa-italic"></i></a>
	    {{-- <a class="btn" data-edit="strikethrough" title="Strikethrough"><i class="fa fa-strikethrough"></i></a> --}}
	    <a class="btn" data-edit="underline" title="Underline (Ctrl/Cmd+U)"><i class="fa fa-underline"></i></a>
	</div>
	<div class="btn-group">
	    <a class="btn dropdown-toggle" data-toggle="dropdown" title="Font Size"><i class="fa fa-text-height"></i>&nbsp;<b class="caret"></b></a>
	      <ul class="dropdown-menu">
	      <li><a data-edit="fontSize 5"><font size="5">Large</font></a></li>
	      <li><a data-edit="fontSize 3"><font size="3">Normal</font></a></li>
	      <li><a data-edit="fontSize 1"><font size="1">Small</font></a></li>
	      </ul>
	</div>
	<div class="btn-group">
	    <a class="btn" data-edit="insertunorderedlist" title="Bullet list"><i class="fa fa-list-ul"></i></a>
	    <a class="btn" data-edit="insertorderedlist" title="Number list"><i class="fa fa-list-ol"></i></a>
	</div>
	<div class="btn-group">
	  <a class="btn dropdown-toggle" data-toggle="dropdown" title="Hyperlink"><i class="fa fa-link"></i></a>
	    <div class="dropdown-menu input-append">
		    <input class="span2" placeholder="URL" type="text" data-edit="createLink"/>
		    <button type="button" class="btn" type="button">Add</button>
		</div>
		<a class="btn" data-edit="unlink" title="Remove Hyperlink"><i class="fa fa-unlink"></i></a>
	</div>
</div>
