<h4 class="charttitle">@{{widgetData[idx]['title']}} (As of {{ $maxDate }})</h4>

{{-- menu --}}

<market-monitor
		:widgetid="'widget'+idx"
		:widget="widgetData[idx]"
		:user="user"
		:period="period"
		:idname="idx"
		:show-table="showTable" inline-template>
  <div class="marketmonitor">
		<div class="clearfix monitor-options">
			<div class="col-sm-4 pull-right hidden-xs">
				<button class="btn btn-outline lg form-control" :class="{'btn-primary':showFilters}" @click="showFilters=!showFilters">
					Filters <span v-if="countActiveFilters>0">(@{{ countActiveFilters }} active)</span>
				</button>
			</div>

			<div class="col-sm-4 pull-right hidden-xs">
				<div class="bubble-filter" v-show="marketData.length && !showTable">
          <div class="dropdown">
            <a class="btn dropdown-toggle select-button" type="button" id="group-everything-by" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              @{{ groupByText }}
              <span class="caret select-caret"></span>
            </a>
            <ul class="dropdown-menu select-option" aria-labelledby="group-everything-by">
              <li>
                <a class="dropdown-item" @click="chooseItem('asset')">Group by Asset Class</a>
              </li>
              <li>
                <a class="dropdown-item" @click="chooseItem('factor')">Group by Factor</a>
              </li>
            </ul>
          </div>
				</div>
			</div>
		</div>

		<div class="table sidebar-filters-monitor clearfix hidden-xs" v-bind:class="{'closed':!showFilters}">
			<div class="col-sm-12 sidebar-filters panel">
	        	@include('partials.strategies.filters')
	    </div>
		</div>



		<div class="charts clearfix hidden-xs" v-bind:class="{'closed':showTable}">
      <div id="vis" v-show="marketData.length" class="vis-holder"></div>
      <div id="vis_empty" v-show="marketData.length==0" class="vis-holder">
        <h3 class="text-center text-navy">Loading data...</h3>
      </div>
      <div class="tooltip_for_market_monitor" id="my_tooltip" style="width: 240px; display: none;"></div>
    </div>



		{{-- table --}}



		<div class="table clearfix " v-bind:class="{'closed':!showTable}">
			<div class="table-responsive visible-xs">

				<table class="table table-striped">
					<thead>
						<tr>
							<th></th>
							<th v-bind:style="{width: (90/strategyAssets.length)+'%'}" v-for="asset in strategyAssets" v-bind:class="{'highlight':widget.properties.sortBy==asset}">
								<button class="btn" @click="sortTable(asset)">
									@{{asset}}
								</button>
							</th>
						</tr>
					</thead>
					<tbody>
						<tr v-for="factor in strategyFactors" class="data-row-parent text-center">
							<td style="width: 10%">
								<span class="text-navy">@{{factor}}</span>
							</td>
							<td v-bind:style="{width: (90/strategyAssets.length)+'%'}" v-for="asset in strategyAssets" v-bind:class="{'highlight':widget.properties.sortBy==asset}">
								<span v-if="tableData"
									:class="{'neg-return': ( tableData[asset][factor]!='n/a' && tableData[asset][factor] < 0 ), 'pos-return': ( tableData[asset][factor]!='n/a' && tableData[asset][factor] > 0 ) }"
								>@{{tableData[asset][factor]!='n/a'?tableData[asset][factor] + "%":'-'}}</span>
							</td>
						</tr>
					</tbody>

				</table>

			</div>
			<div class="datatable hidden-xs">

				<div class="clearfix heading">
					<div style="width: 10%">
						{{-- name --}}
					</div>
					<div v-bind:style="{width: (90/strategyAssets.length)+'%'}" v-for="asset in strategyAssets" v-bind:class="{'highlight':widget.properties.sortBy==asset}">
						<button class="btn" @click="sortTable(asset)">
							@{{asset}}
						</button>
					</div>
				</div>
				<div class="scrolling" style="height:320px">
					<div v-for="factor in strategyFactors" class="data-row-parent text-center">
						<div class="data-row">
							<div style="width: 10%">
								<span class="text-navy">@{{factor}}</span>
							</div>
							<div v-bind:style="{width: (90/strategyAssets.length)+'%'}" v-for="asset in strategyAssets" v-bind:class="{'highlight':widget.properties.sortBy==asset}">
								<span v-if="tableData"
									:class="{'neg-return': ( tableData[asset][factor]!='n/a' && tableData[asset][factor] < 0 ), 'pos-return': ( tableData[asset][factor]!='n/a' && tableData[asset][factor] > 0 ) }"
								>@{{tableData[asset][factor]!='n/a'?tableData[asset][factor] + "%":'-'}}</span>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>

    <div class="popover fade right" id="popover-test">
    	<div class="arrow"></div>
    	<div class="popover-inner">
    	  <div class="popover-content">
        </div>
      </div>
    </div>

    <div v-show="dataLoaded === false" class="abs-loader">
      <div class="loader-inner ball-pulse"> 
        <div></div>
        <div></div>
        <div></div> 
      </div>
    </div> 

	</div>
</market-monitor>
