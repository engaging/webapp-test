@extends('spark::layouts.app')
@section('title', '| Index Sponsor')
@section('scripts')
<link rel="stylesheet" type="text/css" href="css/cropper.css" />
<script src="/js/amcharts/amcharts.js"></script>
<script src="/js/amcharts/serial.js"></script>
<script src="/js/amcharts/pie.js"></script>
<script src="/js/amcharts/themes/light.js"></script>
<script src="/js/amcharts/xy.js"></script>
<script src="/js/amcharts/radar.js"></script>
@stop
@section('content')

<userprovider :user="user" @if ($canedit> 0) :canedit="true" @else :canedit="false" @endif :provider_data='{!! str_ireplace("'", "&apos;", $provider) !!}' inline-template>
  <div class="provider-wrapping" v-if="provider_loaded">
    <div class="provider">
      <div class="banner-top pull-top text-center" :style="{backgroundImage:'url('+provider.bannerImage+')'}">
        <div class="provider-img">
          <div class="provider-img-wrapper">
            <button v-if="canedit" data-toggle="modal" data-target="#edit_top" class="btn btn-edit-hover btn-edit-logo btn-outline" @click="activeEditTab='logo'"
              href>
              <i class="fa fa-pencil"></i> Update Logo
            </button>
            <img :src="provider.image" style="max-height:180px;max-width: 100%;">
          </div>
        </div>
        <button v-if="canedit" data-toggle="modal" data-target="#edit_top" class="btn btn-edit-hover btn-edit-cover-image btn-outline"
          @click="activeEditTab='cover-image'" href>
          <i class="fa fa-pencil"></i> Update Cover Image
        </button>
      </div>
      <div class="clearfix bg-gray top">
        <div class="container">
          <div class="row">
            <div class="col-sm-10">
              <h3 class="upper">@{{provider.name}}</h3>
              <p class="text-gray upper">
                <strong>@{{provider.type}}</strong>
              </p>
              <p v-if="provider.website">
                <strong>
                  <a :href="provider.website" target="_blank" rel="noopener noreferrer">Website
                    <i class="fa fa-external-link"></i>
                  </a>
                </strong>
              </p>
            </div>

            <div v-if="canedit" class="col-sm-2">
              <div>
                <button v-if="canedit" @click="activeEditTab='home'" data-toggle="modal" data-target="#edit_top" class="btn btn-outline edit">
                  <strong>Edit</strong>
                </button>
              </div>
              {{--
              <div>
                <button v-if="canedit" data-toggle="modal" data-target="#edit_connections" class="btn btn-outline edit">
                  <strong>Connection Settings</strong>
                </button>
              </div> --}}
            </div>
          </div>
          <p class="text-justify" v-html="provider.description"></p>
        </div>
      </div>



      <div class="clearfix pad-md">
        <div class="container">
          <div class="clearfix section-title pad-bottom" style="margin-top: -7px;">
            <div class="float pull-right" style="width: 8%; min-width: 65px;">
              <select class="form-control btn btn-default" v-model="featuresOpt.period">
                <option v-for="(v,k) in periodKeysForFilters" :value="k">@{{v}}</option>
              </select>
            </div>

            <div class="float pull-right" style="width: 12%; min-width: 144px; padding-right: 24px;">
              <select class="form-control btn btn-default" v-model="featuresOpt.metric">
            		<option v-for="(v,k) in labels" :value="k">@{{v}}</option>
            	</select>
            </div>

            <div class="float pull-right" style="width: 13%; min-width: 175px; padding-right: 24px;">
              <select class="form-control btn btn-default" v-model="featuresOpt.assetClass">
            		<option v-for="v in assetClasses" :value="v">@{{v}}</option>
            	</select>
            </div>

            <div class="float pull-right" style="padding-top: 7px; padding-right: 24px;">
              <strong>As of {{ $maxDate }}</strong>
            </div>

            <div style="padding-top: 7px;">
              <h4 v-show="featuresOpt.assetClass === 'All Asset Classes'" class="upper">Top @{{provider.name}} strategies by @{{labels[featuresOpt.metric]}}</h4>
              <h4 v-show="featuresOpt.assetClass !== 'All Asset Classes'" class="upper">Top @{{provider.name}} strategies by @{{labels[featuresOpt.metric]}} for @{{featuresOpt.assetClass}}</h4>
            </div>

            <div v-show="!features.length && featuresOpt.loading">
              <div class="loader-inner ball-pulse">
                <div></div>
                <div></div>
                <div></div>
              </div>
            </div>
          </div>

          <div class="row">

            <div v-for="(feature, idx) in features" class="col-sm-6">
              <div class="feature-graph">
                <div class="clearfix title-row">
                  <div class="col-sm-12">
                    <h4>
                      <a :href="'/strategy/' + feature.main.code" target="_blank" rel="noopener noreferrer">@{{ feature.main.shortName }}</a>
                    </h4>
                  </div>
                </div>

                <chart-track :datasets="feature.datasets" :main="feature.main"
                  :period="feature.period" :idname="'feature_' + idx" type="strategy"
                  :colors="colorsAll" :loading="featuresOpt.loading" title=""
                ></chart-track>

                <div class="clearfix portfolio-stats widget-portfolio-stats">
                  <div>{{-- DO NOT DELETE, WORKAROUND FOR CSS ISSUE --}}</div>
                  <div class="col bg-green">
                    {{-- bg-red if negative, bg-green if posisitve. return only --}}
                    <span>Return p.a.</span>
                    <span class="stat">
                      @{{ feature.main[feature.period + '_' + 'return'] | round_4dp | unit(feature.main['um_' + 'return']) }}
                    </span>
                  </div>
                  <div class="col">
                    <span>Volatility</span>
                    <span class="stat">
                      @{{ feature.main[feature.period + '_' + 'volatility'] | round_4dp | unit(feature.main['um_' + 'volatility']) }}
                    </span>
                  </div>
                  <div class="col">
                    <span>Avg DD</span>
                    <span class="stat">
                      @{{ feature.main[feature.period + '_' + 'avg_drawdown'] | round_4dp | unit(feature.main['um_' + 'avg_drawdown']) }}
                    </span>
                  </div>
                  <div class="col">
                    <span>Max DD</span>
                    <span class="stat">
                      @{{ feature.main[feature.period + '_' + 'mdd'] | round_4dp | unit(feature.main['um_' + 'mdd']) }}
                    </span>
                  </div>
                  <div class="col">
                    <span>Sharpe</span>
                    <span class="stat">
                      @{{ feature.main[feature.period + '_' + 'sharpe'] | round_4dp | unit(feature.main['um_' + 'sharpe']) }}
                    </span>
                  </div>
                  <div class="col">
                    <span>Skewness</span>
                    <span class="stat">
                      @{{ feature.main[feature.period + '_' + 'skewness'] | round_4dp | unit(feature.main['um_' + 'skewness']) }}
                    </span>
                  </div>
                </div>
              </div>
            </div>


          </div>
        </div>
      </div>

      <div class="clearfix pad-md bg-lightgray" style="padding-bottom: 30px;">
        <div class="container">
          <div class="clearfix section-title pad-bottom">

            <h4>DATA ROOM</h4>
            <button v-if="canedit" data-toggle="modal" data-target="#edit_documents" @click="resetEditing(1)" class="btn btn-outline edit">
              <i class="fa fa-plus"></i>
              <strong>Add New</strong>
            </button>

          </div>
          <div class="row">
            {{-- private/notprivate , download and/or view website --}}
            <div class="col-sm-6" v-for="(item, idx) in documents">

              <div class="document" v-bind:class="{'grayin':idx==0 && item.new }">
                <span class="date text-gray">@{{item.date}}</span>

                <button v-if="canedit" @click="editDocument(idx)" style="padding:0 10px" class="btn pull-right text-navy">
                  <i class="fa fa-pencil"></i>
                </button>

                <div class="row">
                  <div class="col-xs-12">
                    <span class="title">@{{item.title}}</span>
                  </div>

                </div>
                <p v-html="item.description"></p>
                <div class="clearfix links">
                  <a v-if="item.link && item.link.length" target="_blank" rel="noopener noreferrer" :href="item.link">
                    <img src="/img/design/icon-download.png" alt="Download"> Download
                  </a>
                  <span class="spacer" v-if="item.website && item.website.length && item.link && item.link.length">|</span>
                  <a v-if="item.website && item.website.length" target="_blank" rel="noopener noreferrer" :href="item.website">
                    <img src="/img/design/icon-website.png" alt="Website"> Visit Website
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>


      {{-- TOP 10 STRATEGIES --}}
      <div class="clearfix bg-gray pad-md">
        <div class="container">
          <div class="clearfix section-title pad-bottom" style="margin-top: -7px;">

            <div class="tableoption float pull-right" v-bind:class="{'opened':showTableOption==1}" data-var="showTableOption">
              <button @click="showTableOption=showTableOption*-1" class="btn">
                <span class="icon icon-cog"></span>
              </button>
              <div class="menu" v-show="showTableOption==1">
                <p>Add performance metrics (@{{dataTableColsMax - dataTableCols_raw.length}} left)</p>
                <label v-for="(v,k) in labels" class="checkbox-style">
                  <input type="checkbox" v-model="dataTableCols_raw" :value="k" :disabled='k === metric || (dataTableCols_raw.indexOf(k) < 0 && dataTableCols_raw.length >= dataTableColsMax)'>
                  <i></i>
                  @{{v}}
                </label>
              </div>
            </div>

            <div class="float pull-right" style="width: 8%; min-width: 65px;">
              <select class="form-control" v-model="period">
                <option v-for="(v,k) in periodKeysForFilters" :value="k">@{{v}}</option>
              </select>
            </div>

            <div class="float pull-right" style="width: 12%; min-width: 144px; padding-right: 24px;">
              <select class="form-control" v-model="metric">
            		<option v-for="(v,k) in labels" :value="k">@{{v}}</option>
            	</select>
            </div>

            <div class="float pull-right" style="padding-top: 7px; padding-right: 24px;">
              <strong>As of {{ $maxDate }}</strong>
            </div>

            <div style="padding-top: 7px;">
              <h4 class="upper">Top @{{topSize}} @{{provider.name}} strategies by @{{labels[metric]}}</h4>
            </div>

            <div v-show="!indices.length">
              <div class="loader-inner ball-pulse">
                <div></div>
                <div></div>
                <div></div>
              </div>
            </div>
          </div>

          <div class="datatable provider-hidden-scroll">
            <div class="clearfix heading">
              <div style="width:5%" class="text-center">
                <span class="text-gray"></span>
              </div>
              <div style="width:35%" class="namecol">
                <button class="btn" @click="changeSortby('shortName')">
                  Name
                  <span v-show="sortby!='shortName'">
                    <i class="fa fa-sort"></i>
                  </span>
                  <span v-show="sortby=='shortName'">
                    <i v-bind:class="{'fa-sort-up':(sortorder>0),'fa-sort-down':(sortorder<0)}" class="fa"></i>
                  </span>
                </button>
              </div>
              <div style="width:10%"></div>
              <div :style="{width: (60/(dataTableColsCount+1))+'%'}" :class="{'highlight':(sortby==periodKeys[period] + '_'+metric)}">
                <button @click="changeSortby(periodKeys[period] + '_' + metric)" class="btn">
                  @{{labels[metric]}}
                  <span v-show="sortby!=periodKeys[period] + '_'+ metric">
                    <i class="fa fa-sort pull-right"></i>
                  </span>
                  <span v-show="sortby==periodKeys[period] + '_'+ metric">
                    <i v-bind:class="{'fa-sort-up':(sortorder>0),'fa-sort-down':(sortorder<0)}" class="fa pull-right"></i>
                  </span>
                </button>
              </div>
              <div :style="{width: (60/(dataTableColsCount+1))+'%'}" v-for="(header, field) in labels" v-show="field !== metric && dataTableCols_raw.indexOf(field) > -1"
                :class="{'highlight':(sortby==periodKeys[period] + '_'+field)}">
                <button @click="changeSortby(periodKeys[period] + '_' + field)" class="btn">
                  @{{header}}
                  <span v-show="sortby!=periodKeys[period] + '_'+ field">
                    <i class="fa fa-sort pull-right"></i>
                  </span>
                  <span v-show="sortby==periodKeys[period] + '_'+ field">
                    <i v-bind:class="{'fa-sort-up':(sortorder>0),'fa-sort-down':(sortorder<0)}" class="fa pull-right"></i>
                  </span>
                </button>
              </div>
            </div>

            <div class="scrolling">
              <div v-for="(index, idx) in indices" class="data-row-parent">
                <div class="data-row">
                  <div style="width:5%" class="text-center">
                    <span class="text-gray">@{{idx+1}}</span>
                  </div>
                  <div style="width:35%">
                    <h5 class="list-group-item-heading">
                      <a :href="'/strategy/' + index.code" target="_blank" rel="noopener noreferrer">@{{ index.shortName }}</a>
                    </h5>
                    <span class="text-gray">@{{ index.assetClass }} | @{{ index.currency }} @{{ index.returnCategory }}@{{ index.returnType }} @{{index.volTarget }}</span>
                  </div>

                  <div style="width:10%">
                    <div class="clearfix" style="white-space:nowrap">
                      <a v-if="index.is_suspended == 0" class="btn-outline-blue btn" @click="saveOneStrategy(index.id)">Add</a>
                      <a v-if="index.is_suspended == 1" class="btn-outline-blue btn">Not updated</a>
                    </div>
                  </div>

                  <div :style="{width: (60/(dataTableColsCount+1))+'%'}" class="text-center" :class="{'highlight':(sortby==periodKeys[period] + '_' + metric)}">
                    @{{ index[periodKeys[period] + '_' + metric] | round_4dp | unit(index['um_' + metric]) }}
                  </div>
                  <div :style="{width: (60/(dataTableColsCount+1))+'%'}" class="text-center" v-for="(header, field) in labels" v-show="field !== metric && dataTableCols_raw.indexOf(field) > -1"
                    :class="{'highlight':(sortby==periodKeys[period] + '_' + field)}">
                    @{{ index[periodKeys[period] + '_' + field] | round_4dp | unit(index['um_' + field]) }}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <feed :provider="provider" :canedit="canedit" mode="updates" inline-template>
        <div class="clearfix pad-md">
          <div class="container">
            <div class="clearfix section-title pad-bottom">
              <h4>UPDATES</h4>
              <button v-if="canedit" class="btn btn-outline edit" @click="resetEditing(1)" data-toggle="modal" data-target="#edit_updates">
                <i class="fa fa-plus"></i>
                <strong>Add New</strong>
              </button>
            </div>
            {{-- instead of editing here, since these have detail pages w/ all the text you should edit there --}}

            <div class="feed">
              <div v-show="loading==true">
                <div class="loader-inner ball-pulse">
                  <div></div>
                  <div></div>
                  <div></div>
                </div>
              </div>
              <div v-if="loading==false && pagination.total == 0" class="alert alert-info">
                <p>No updates have been posted here yet.</p>
              </div>
              <div v-for="(item, idx) in feedData">
                <div v-if="item.type==='Updates'">
                  @include('feed.feed_item')
                </div>
              </div>
            </div>
          </div>

          @include('feed.feed_edit_modal')

        </div>
      </feed>

      {{-- DISCLAIMER --}}
      <div v-if="provider.disclaimer" class="clearfix pad-md bg-lightgray">
        <div class="container">
          <p class="text-justify" v-html="provider.disclaimer"></p>
        </div>
      </div>

    </div>

    @if ( $canedit > 0 ) {{-- FEED MODAL --}}
    <div class="modal fade editmodal" id="edit_updates" tabindex="-1" role="dialog">
      <div class="modal-dialog network" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <h4 class="modal-title" v-show="editingFeed==null">Add New Update</h4>
            <h4 class="modal-title" v-show="editingFeed!=null">Edit your Update</h4>
          </div>
          <div class="modal-body">
            <div class="top">

              <div class="clearfix" v-show="editingFeed!=null" style="margin-bottom: 30px">
                <small>Originally posted on @{{feedData[editingFeed]?feedData[editingFeed].date:""}}</small>
              </div>


              <div class="clearfix">
                <div class="col-sm-2">
                  <a href="/sponsor/1" class="provider-img">
                    <img :src="provider.image" class="img-responsive" alt>
                  </a>
                </div>
                <div class="col-sm-10">
                  <div class="form-group">
                    <label class="label">TITLE</label>
                    <input type="text" placeholder="Title" class="form-control" v-model="newPostTitle">
                  </div>
                  <div class="clearfix" style="margin-top: 30px;">
                    <h4 class="label">EDIT DESCRIPTION</h4>
                    {{--
                    <textarea class="form-control" v-model="newTopDescription"></textarea> --}} {{-- test richtext --}}
                    @include('partials.richtext',array('divid'=>'description-editor'))
                    <div class="richtext-editor" id="description-editor" style="background:#fff;" contenteditable v-html="newTopDescription"></div>
                    {{-- endtest --}}

                  </div>

                  <div class="bg-gray">
                    <div class="filedrop">
                      <div v-show="newPostImage">
                        <img :src="newPostImage" class="img-responsive">
                        <a v-show="newPostImage" class="btn btn-outline" @click="newPostImage=null">Remove</a>
                      </div>
                      <div v-show="!newPostImage || newPostImage.length==0">
                        <label>
                          <h4>DROP IMAGE HERE OR CLICK TO UPLOAD</h4>
                          <p>JPG, PNG or GIF file. Max file size 2MB.</p>
                          <input type="file" @change="imagePreview" class="form-control">
                        </label>
                      </div>
                    </div>
                  </div>

                  {{--
                  <br>
                  <div class="clearfix">
                    <div class="tableoption" style="max-width:none" v-bind:class="{'opened':showTagOptions==1}">
                      <button @click="showTagOptions=showTagOptions*-1" class="btn btn-default" style="width:100%">
                        Tags (@{{tagsActive.length}})
                        <span class="caret"></span>
                      </button>
                      <div class="menu" v-show="showTagOptions==1">
                        <label v-for="k in tags" class="checkbox-style">
                          <input type="checkbox" v-model="tagsActive" v-bind:value="k">
                          <i></i>
                          @{{k}}
                        </label>
                      </div>
                    </div>
                  </div> --}} {{--

                  <hr class="dotted">
                  <div class="clearfix">
                    <label class="label">SHARE WITH</label>
                    <div class="bg-gray clearfix">
                      <div class="col-xs-6" v-for="(v,k) in shareOptions">
                        <label class="checkbox-style" style="padding:5px">
                          <input type="checkbox" v-bind:value="k" v-model="shareOptionsActive">
                          <i></i>
                          @{{v}}
                        </label>
                      </div>
                    </div>
                  </div>--}}


                  <div class="clearfix" style="margin-top: 30px;">
                    <div class="col-sm-6 col-sm-offset-3">
                      <button class="btn btn-primary form-control submit" @click="sharePost()">SAVE</button>
                    </div>
                  </div>

                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    {{-- DOCUMENTS MODAL --}}
    <div v-if="canedit" class="modal fade editmodal" id="edit_documents" tabindex="-1" role="dialog">
      <div class="modal-dialog network" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <h4 class="modal-title" v-show="editingDocument==null">Add New Document</h4>
            <h4 class="modal-title" v-show="editingDocument!=null">Edit Document</h4>

          </div>
          <div class="modal-body">
            <div class="clearfix" v-show="editingFeed!=null" style="margin-bottom: 30px">
              <small>Originally posted on @{{documents[editingDocument]?documents[editingDocument].date:""}}</small>
            </div>

            <div class="form-group">
              <label class="label">TITLE</label>
              <input type="text" class="form-control" v-model="newDocumentTitle" placeholder="Title">
            </div>
            <div class="form-group">
              <label class="label">DESCRIPTION</label>
              @include('partials.richtext',array('divid'=>'document-editor'))
              <div class="richtext-editor" id="document-editor" style="background:#fff;" contenteditable v-html="newDocumentDescription"></div>
            </div>
            <div class="form-group">
              <label class="label">LINK</label>
              <input type="text" class="form-control" v-model="newDocumentWebsite" placeholder="Website (optional)">
            </div>

            <div class="form-group">
              <label class="label">ATTACHMENT</label>
              <div class="bg-gray">


                <div class="filedrop">
                    <div v-show="documentSaving===true">
                        <div class="loader-inner ball-pulse">
                          <div></div>
                          <div></div>
                          <div></div>
                        </div>
                    </div>
                  <div v-show="newDocumentFile ">
                    <span>
                      <i class="fa fa-file"></i> @{{newDocumentFilename}}</span>
                    <a class="btn btn-outline" @click="newDocumentFile=null; newDocumentFilename=null">Remove</a>
                  </div>

                  <div v-show="!newDocumentFile || newDocumentFile.length==0">
                    <label>
                      <h4>DROP FILE HERE OR CLICK TO UPLOAD</h4>
                      <p>PDF file. Max file size 2MB.</p>
                      <input type="file" class="form-control" @change="filePreview">
                    </label>
                  </div>
                </div>
              </div>
            </div>

            {{--
            <hr class="dotted">
            <div class="clearfix">
              <label class="label">SHARE WITH</label>
              <div class="bg-gray clearfix">
                <div class="col-xs-6" v-for="(v,k) in shareOptions">
                  <label class="checkbox-style" style="padding:5px">
                    <input type="checkbox" v-bind:value="k" v-model="shareOptionsActive">
                    <i></i>
                    @{{v}}
                  </label>
                </div>
              </div>
            </div>--}} {{--
            <div class="clearfix form-group" style="margin-top: 30px">
              <div class="col-sm-6">
                <br> Only Connected
              </div>
              <div class="col-sm-6">
                <div class="switch-custom" style="margin-top: 15px; float: right;">
                  <label>
                    <input type="radio" v-model="newDocumentLocked" value="1">
                    <i>Yes</i>
                  </label>
                  <label>
                    <input type="radio" v-model="newDocumentLocked" value="0">
                    <i>No</i>
                  </label>
                </div>
              </div>
            </div>

            --}}
            <div class="clearfix" style="margin-top: 30px">
              <div class="col-sm-3" v-show='editingDocument!=null'>
                <button class="btn btn-warning form-control submit pull-right" @click="removeDocument()">DELETE</button>
              </div>
              <div class="col-sm-6 col-sm-offset-3">
                <button class="btn btn-primary form-control submit" @click="saveDocument()" v-bind:disabled="documentSaving===true">SAVE</button>
              </div>
            </div>

          </div>
        </div>
      </div>
    </div>

    <div v-if="canedit" class="modal fade editmodal" id="edit_features" tabindex="-1" role="dialog">
      <div class="modal-dialog network" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <h4 class="modal-title">Edit Featured Strategies</h4>
          </div>

          <div class="modal-body" v-show="newFeatureStep==1">
            <h4 class="label">YOUR STRATEGIES</h4>
            <div class="form-group">
              <input type="text" placeholder="Keyword Search" class="search-input form-control" v-model="indicesSearch">
            </div>


            <div class="datatable">
              <div class="data-row-parent">
                <div class="clearfix heading data-row">
                  <div style="width:50%" class="namecol">
                    {{-- Name --}}
                  </div>
                </div>
              </div>

              <div class="clearfix bg-gray" style="max-height: 300px;overflow:auto;min-height: 30px;">
                <div v-for="index in indicesAll" class="data-row-parent">
                  <div class="data-row item" v-bind:class="{'hidden': indicesSearch.length && index.shortName.toLowerCase().indexOf(indicesSearch.toLowerCase())<0,'selected':newFeatureStrategy!=null && newFeatureStrategy.id==index.id}"
                    @click="newFeatureStrategy=index">
                    <div style="width:50%;padding-left: 15px">
                      <p style="margin-bottom: 0;">@{{index.shortName}}</p>
                      <span class="text-gray">@{{ index.assetClass }} | @{{ index.currency }} @{{ index.returnCategory }}@{{ index.returnType }}
                        @{{ index.volTarget }}</span>
                    </div>
                  </div>
                </div>

              </div>
            </div>


            <div class="clearfix" style="margin-top: 30px;">
              <div class="col-sm-6 col-sm-offset-3">
                <button class="btn btn-primary form-control submit" v-bind:disabled="!newFeatureStrategy" @click="newFeatureStep=2">NEXT</button>
              </div>
            </div>
          </div>
          <div class="modal-body" v-show="newFeatureStep==2">
            <h4 class="label">YOUR STRATEGIES</h4>

            <button class="btn btn-info" @click="newFeatureStep=1" style="width:100%;margin:10px 0; text-align: left;">
              <p style="margin-bottom: 0;">@{{newFeatureStrategy?newFeatureStrategy.shortName:""}}</p>
              <span class="text-gray">@{{ newFeatureStrategy?newFeatureStrategy.assetClass:"" }} | @{{ newFeatureStrategy?newFeatureStrategy.currency:""}}
                @{{ newFeatureStrategy?newFeatureStrategy.returnCategory:"" }}@{{ newFeatureStrategy?newFeatureStrategy.returnType:""}}
                @{{ newFeatureStrategy?newFeatureStrategy.volTarget:"" }}</span>
            </button>

            <div class="clearfix">
              <select class="form-control bg-gray" v-model="newFeaturePeriod" style="border: 1px solid #ddd;">
                <option value="">Choose Default Time Period</option>
                <option v-for="(v,k) in periodKeysForFilters" v-bind:value="v">@{{k}}</option>
              </select>
            </div>

            <div class="clearfix graphselect bg-gray">
              <div v-for="graph in newFeatureGraphOptions" class="col-sm-3">
                <div class="clearfix text-center item" v-bind:class="{'selected':graph.name==newFeatureGraph}" @click="newFeatureGraph=graph.name">
                  <img :src="graph.image" class="img-responsive">
                  <span>@{{graph.name}}</span>
                </div>
              </div>
            </div>

            <div class="clearfix" style="margin-top: 30px;" v-show="newFeatureStrategy!=null">
              <div class="col-sm-3">
                <button class="btn text-gray" @click="newFeatureStep=1">&lt; BACK</button>
              </div>
              <div class="col-sm-6">
                <button class="btn btn-primary form-control submit" v-bind:disabled="this.newFeaturePeriod.length==0 || !newFeatureGraph || newFeatureGraph.length==0"
                  @click="saveFeature()">ADD</button>
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>



    <div v-if="canedit" class="modal fade editmodal" id="edit_connections" tabindex="-1" role="dialog" data-backdrop="static"
      data-keyboard="false">
      <div class="modal-dialog network" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <h4 class="modal-title">Connection Settings</h4>
          </div>
          <div class="modal-body">
            @include('settings.connections',array('size'=>'small'))
          </div>
        </div>
      </div>
    </div>

    {{-- main banner and info edit --}}
    <div v-if="canedit" class="modal fade editmodal" id="edit_top" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
      <div class="modal-dialog network" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <h4 class="modal-title">Edit Your Page</h4>
          </div>
          <div class="modal-body" v-show="loading">
            <p>Saving... please wait.</p>
          </div>
          <div class="modal-body" v-show="loading==false">


            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
              <li role="presentation" :class="{'active': activeEditTab == 'home' }">
                <a @click="activeEditTab='home'" href="#edit-description" role="tab" data-toggle="tab">Description</a>
              </li>
              <li role="presentation" :class="{'active': activeEditTab == 'logo' }">
                <a href="#edit-logo" @click="activeEditTab='logo'" role="tab" data-toggle="tab">Logo</a>
              </li>
              <li role="presentation" :class="{'active': activeEditTab == 'cover-image' }">
                <a href="#edit-cover-image" @click="activeEditTab='cover-image'" role="tab" data-toggle="tab">Cover Image</a>
              </li>
              <li role="presentation" :class="{'active': activeEditTab == 'disclaimer' }">
                <a href="#edit-disclaimer" @click="activeEditTab='disclaimer'" role="tab" data-toggle="tab">Disclaimer</a>
              </li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
              <div role="tabpanel" :class="{'active': activeEditTab == 'home' }" class="tab-pane" id="edit-description">
                <div class="clearfix form-row">
                  <h4 class="label">NAME</h4>
                  <input class="form-control" v-model="newName" />
                </div>

                <div class="clearfix form-row">
                  <h4 class="label">WEBSITE</h4>
                  <input class="form-control" v-model="newWebsite" />
                </div>

                <div class="clearfix form-row">
                  <h4 class="label">DESCRIPTION</h4>
                  @include('partials.richtext',array('divid'=>'topdescription-editor'))
                  <div class="richtext-editor" id="topdescription-editor" style="background:#fff;" contenteditable v-html="newTopDescription"></div>
                </div>
              </div>
              <div role="tabpanel" :class="{'active': activeEditTab == 'logo' }" class="tab-pane" id="edit-logo">
                <div class="clearfix form-row">

                  <h4 class="label">LOGO</h4>



                  <div class="row" style="margin-top: 15px;" v-if="logoPreviewImage.length">
                    <div class="col-sm-6 col-sm-3">
                      <div style="border-radius: 4px;border: 1px solid #444;">
                        <img :src="logoPreviewImage" class="logo-preview img-responsive" />
                      </div>
                    </div>
                  </div>

                  <div class="clearfix">
                    <div class="filedrop">
                      <label>
                        <h4>DROP IMAGE HERE OR CLICK TO UPLOAD A NEW LOGO</h4>
                        <p>JPG, PNG or GIF file. Max file size 2MB.</p>
                        <input type="file" @change="logoPreview" class="form-control">
                      </label>
                    </div>
                  </div>

                </div>
              </div>
              <div v-if="activeEditTab" role="tabpanel" :class="{'active': activeEditTab == 'cover-image' }" class="tab-pane" id="edit-cover-image">
                <div class="clearfix form-row">
                  <h4 class="label">COVER PHOTO</h4>
                  <div v-if="newTopImage && newTopImage.length" style="border-radius: 4px;border: 1px solid #444;">

                    <div style="width: 100%;">
                      <!-- assign ref property to access the underlying functions -->
                      <!-- all cropperjs options should be in kebab-case instead of camelCase -->
                      <!-- see http://vuejs.org/guide/components.html#camelCase-vs-kebab-case -->
                      <!-- :cropend="previewBg" -->
                      <vue-cropper ref="cropper" :aspect-ratio="1400/285" :guides="true" :view-mode="0" drag-mode="crop" :auto-crop-area="1" :background="true"
                        :rotatable="false" :min-container-width="566" :min-canvas-width="566" :crop-box-resizable="false" :src="newTopImage" :cropend="imageCropped">
                      </vue-cropper>
                    </div>

                  </div>
                  <div v-else>

                    <div style="border-radius: 4px;border: 1px solid #444;" v-if="provider.bannerImageOriginal.length">

                      <div style="width: 100%;">
                        <!-- assign ref property to access the underlying functions -->
                        <!-- all cropperjs options should be in kebab-case instead of camelCase -->
                        <!-- see http://vuejs.org/guide/components.html#camelCase-vs-kebab-case -->
                        <!-- :cropend="previewBg" -->
                        <vue-cropper ref="cropper" :aspect-ratio="1400/285" :guides="true" :view-mode="0" drag-mode="move" :auto-crop-area="1" :background="true"
                          :rotatable="false" :min-container-width="566" :min-canvas-width="566" :min-container-height="116"
                          :min-canvas-height="116" :crop-box-resizable="false" :src="provider.bannerImageOriginal" :cropend="imageCropped">
                        </vue-cropper>
                      </div>
                    </div>

                  </div>


                  <span class="text-gray">Recommended dimensions: 1400px wide by 285px high</span>



                  <div class="clearfix">
                    <div class="filedrop">
                      <label>
                        <h4>DROP IMAGE HERE OR CLICK TO UPLOAD A NEW COVER IMAGE</h4>
                        <p>JPG, PNG or GIF file. Max file size 2MB.</p>
                        <input type="file" @change="bannerPreview" class="form-control">
                      </label>
                    </div>
                  </div>
                </div>
              </div>
              <div role="tabpanel" :class="{'active': activeEditTab == 'disclaimer' }" class="tab-pane" id="edit-disclaimer">
                <div class="clearfix form-row">
                  <h4 class="label">DISCLAIMER</h4>
                  @include('partials.richtext',array('divid'=>'disclaimer-editor'))
                  <div class="richtext-editor" id="disclaimer-editor" style="background:#fff;" contenteditable v-html="newDisclaimer"></div>
                </div>
              </div>
            </div>


            <div class="clearfix form-row">
              <div class="col-sm-6">
                <button class="btn btn-default form-control" data-dismiss="modal" aria-label="Close">DISCARD CHANGES</button>
              </div>
              <div class="col-sm-6">
                <button class="pull-right btn btn-primary form-control submit" @click="saveTop()">SAVE</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>


    @endif

    <div class="modal fade editmodal" id="prompt_connect" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
      <div class="modal-dialog network" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <h4 class="modal-title">Data Room Restricted Access</h4>
          </div>
          <div class="modal-body" v-if="provider.pending">
            <p>You have sent a connection request to @{{ provider.name }}. Access will be granted to you when your connection
              request has been granted.</p>
          </div>
          <div class="modal-body" v-if="!provider.pending">
            <p>In order to access files in the data room you must be connected.</p>
            <connect :provider="provider" :user="user" inline-template>
              <div>
                <div v-if="!user.community_member || user.community_member.id != provider.id">
                  <div class="connect-buttons">
                    <button v-if="!provider.pending && !provider.following" @click="provider.pending=true" class="btn btn-outline-blue lg" transition="fade">
                      Connect
                    </button>
                    <a v-if="!provider.pending && provider.following" class="btn btn-primary" :href="'/inbox/create?to=' + provider.id" transition="fade">
                      Contact @{{ provider.name }}
                    </a>
                    <button v-if="provider.pending" class="btn btn-info" transition="fade">
                      Pending
                    </button>
                  </div>
                </div>
                <div v-if="user.community_member && user.community_member.id == provider.id">
                  <div class="connect-buttons">
                    <p>This is an example of what potential investors will see upon clicking restricted files in your data room.</p>
                    <button v-if="!provider.pending && !provider.following" @click="provider.pending=true" class="btn btn-outline-blue lg" transition="fade">
                      Connect
                    </button>
                    <a v-if="!provider.pending && provider.following" class="btn btn-primary" :href="'/inbox/create?to=' + provider.id" transition="fade">
                      Contact @{{ provider.name }}
                    </a>
                    <button v-if="provider.pending" class="btn btn-info" transition="fade">
                      Pending
                    </button>
                  </div>
                </div>
              </div>
            </connect>
          </div>
        </div>
      </div>
    </div>
    <add-strategies :indices-basket="[addingStrategy]" :period="periodKeysForFilters[period]" :user="user"></add-strategies>
  </div>
</userprovider>

@include('spark::nav.footer')

@endsection
