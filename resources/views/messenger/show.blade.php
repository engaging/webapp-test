@extends('spark::layouts.app')

@section('content')
    <script type="text/javascript">
        window.thread_messages = {!! $thread->messages !!};
    </script>
    <messages :user="user" thread="{{$thread->id}}" inline-template>

        <div class=" inbox">
        	<div class="container">
        		<div class="sidebar-filters col-sm-4" style="padding:0">
        
        				@include('messenger.partials.sidebar')
        
        		</div>
        		<div class="col-sm-8 right">
                    <div class="main">


                    <?php
                        // getting the participant who is not you
                        $recipient = '';
                        foreach (explode(',',$thread->participantsString()) as $key => $value) {
                            if ( Auth::user()->name != trim($value) ){
                                $recipient = trim($value);
                            }
                        }
                    ?>
        			{{-- <h3 class="text-navy">{{ $thread->subject }}</h3> --}}
                    <div class="clearfix">
                        <div class="col-xs-12"><h4 class="text-navy upper">Discussion with {{$thread->participantsString()}}</h4></div>
                    </div>

                    <hr class="dotted">
                    <div class="thread-messages">
                        <div v-for="message in thread_messages" >
                        <div class="message" v-bind:class="{'closed':message.closed, 'color1':message.user_id==user.id, 'color2':message.user_id!=user.id }">
                            <div class="clearfix author text-gray" @click="message.closed=!message.closed">
                                <strong v-if="message.user_id==user.id">You</strong>
                                <strong v-if="message.user_id!=user.id">
                                    {{-- {{ $recipient }} --}}
                                    @{{user.name}}
                                </strong>
                                | 
                                Posted @{{ message.created_at }}
                            </div>
                            <div class="clearfix text">
                                <div class="col-xs-12" v-show="message.closed" @click="message.closed=!message.closed">
                                    <p>@{{ message.body.slice(0,300)}}</p>
                                </div>
                                <div class="col-xs-12" v-show="!message.closed">
                                    <p v-html="message.body"></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                    {{-- @each('messenger.partials.messages', $thread->messages, 'message') --}}
                    <div class="spacer"></div>
                    <div class="bg-gray clearfix message-form">
                        <div class="col-xs-12">@include('messenger.partials.form-message')</div>
        			</div>

        			@include('spark::nav.footer')
                    </div>
        		</div>
        	</div>
        </div>
        
    </messages>
@stop

