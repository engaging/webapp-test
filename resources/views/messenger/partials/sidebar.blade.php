<div class="sidebar" style="max-height: none;">

	<div class="panel-heading">
		<!--<a class="btn btn-outline pull-right" href="/inbox/create">Create a message</a>-->


		<h4>INBOX</h4>
		{{--<div class="row text-center" style="padding: 15px 0">
			<div class="col-xs-4">
				<button class="btn form-control" v-bind:class="{'btn-primary':showSidebar=='all','btn-outline':showSidebar!='all'}" @click="showSidebar='all'">All</button>
			</div>
			<div class="col-xs-4">
				<button class="btn form-control" v-bind:class="{'btn-primary':showSidebar=='notices','btn-outline':showSidebar!='notices'}" @click="showSidebar='notices'">Notices</button>
			</div>
			<div class="col-xs-4">
				<button class="btn form-control" v-bind:class="{'btn-primary':showSidebar=='messages','btn-outline':showSidebar!='messages'}" @click="showSidebar='messages'">Messages</button>
			</div>
		</div>--}}
		<input class="form-control search-input" type="text" placeholder="Keyword Search" style="border:0;" />
	</div>

	@include('messenger.partials.flash')


	<div class="scroll all" v-show="showSidebar=='all'" transition="fade">
		@each('messenger.partials.notification', $notifications, 'notification', 'messenger.partials.no-threads')
		@each('messenger.partials.thread', $threads, 'thread', 'messenger.partials.no-threads')
	</div>


	<div class="scroll notifications" v-show="showSidebar=='notices'" transition="fade">
		@each('messenger.partials.notification', $notifications, 'notification', 'messenger.partials.no-threads')
	</div>

	<div class="scroll messages" v-show="showSidebar=='messages'" transition="fade">
		@each('messenger.partials.thread', $threads, 'thread', 'messenger.partials.no-threads')
	</div>

</div>
