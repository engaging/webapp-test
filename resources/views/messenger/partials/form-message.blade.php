 {{-- test richtext --}}
@include('partials.richtext',array('divid'=>'message-editor'))
<div class="richtext-editor" id="message-editor" style="background:#fff;" @click="initRichtext()">{{ old('message') }}</div>
{{-- endtest --}}


<div class="form-group">
    <button type="submit" class="btn btn-lg btn-primary pull-right" @click="submitMessage()"><strong>SEND</strong></button>
</div>



<form id="message-new-form" class="hidden" action="{{ route('messages.update', $thread->id) }}" method="post">
    {{ method_field('put') }}
    {{ csrf_field() }}
    <!-- Message Form Input -->
    <div class="form-group">
        <textarea name="message" id="message-input" class="form-control" placeholder="Reply...">{{ old('message') }}</textarea>
    </div>

   {{--  <h5>Send to</h5>
    @if($users->count() > 0)
       <div class="checkbox">
            @foreach($users as $user)
            <div class="clearfix">
                <label title="{{ $user->name }}" class="checkbox-style">
                    <input type="checkbox" name="recipients[]" value="{{ $user->id }}"><i></i>
                    {!!$user->name!!}
                </label>
            </div>
            @endforeach
        </div>
    @endif --}}
</form>
