<?php $alertclass = $thread->isUnread(Auth::id()) ? 'fa fa-circle' : ''; ?>

<?php $class = 'thread-'.$thread->id; ?>

<div class="clearfix message-item {{$class}}">
    <div class="col-xs-3">
        <div class="provider-img"><img src="//www.gravatar.com/avatar/{{ md5($thread->creator()->email) }} ?s=64"
             alt="{{ $thread->creator()->name }}" class="img-responsive" alt></div>
    </div>
    <div class="col-xs-9">
        <i class="{{$alertclass}} text-green pull-right"></i>

        <small>{{ $thread->created_at->diffForHumans() }}</small><br>
        
        <a href="{{ route('messages.show', $thread->id) }}">
        <strong>Discussion with {{ $thread->creator()->name }} & {{ $thread->participantsString(Auth::id()) }}</strong>
        </a>
            {{-- ({{ $thread->userUnreadMessagesCount(Auth::id()) }} unread)<br> --}}
        <a href="{{ route('messages.show', $thread->id) }}">
            <p>{{ substr( strip_tags($thread->latestMessage->body),0,50) }}</p>
        </a>

            {{-- <small><strong>Creator:</strong> {{ $thread->creator()->name }}</small><br>
            <small><strong>Participants:</strong> {{ $thread->participantsString(Auth::id()) }}</small> --}}
        </a>
    </div>
</div>
<hr class="dotted">
