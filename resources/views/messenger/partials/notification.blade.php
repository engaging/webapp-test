<?php $alertclass = $notification->read == 0 ? 'fa fa-circle' : ''; ?>
<?php $class = 'notification-'.$notification->id; ?>

<div class="clearfix message-item {{ $class }}">
    <div class="col-xs-3">
         <div class="provider-img">
         <img src="/img/design/logo.png" style="background:#333;"
             alt="{{ $notification->user->name }}" class="img-responsive" alt>
         </div>

    </div>
    <div class="col-xs-9">
        <i class="{{$alertclass}} text-green pull-right"></i>

    	<small>{{ $notification->created_at->diffForHumans() }}</small><br>

         <a href="{{ route('notification.show', $notification->id) }}"><strong>{!! $notification->body !!}</strong></a>

        {{-- <div class="clearfix text-right"><a class='btn btn-primary btn-xs' href="{{ $notification->action_url }}">{{ $notification->action_text }}</a></div> --}}
    </div>
</div>
<hr class="dotted">
