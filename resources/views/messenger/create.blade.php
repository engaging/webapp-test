@extends('spark::layouts.app')

@section('title', '| Inbox')
<style type="text/css">
  .sidebar .scrolling {
      margin: 5px 0;
  }
</style>
@section('content')

<messages :user="user" inline-template>

<div class=" inbox">
    <div class="container">
        <div class="sidebar-filters col-sm-4" style="padding:0">

                @include('messenger.partials.sidebar')

        </div>
        <div class="col-sm-8 right">

            <div class="clearfix main">

                <h1>Send a new message to {{ $recipient->name }}</h1>
                <form action="{{ route('messages.store') }}" method="post">
                    {{ csrf_field() }}
                    <!-- Subject Form Input -->
                    {{-- <div class="form-group">
                        <label class="control-label">Subject</label>
                        <input type="text" class="form-control" name="subject" placeholder="Subject"
                               value="{{ old('subject') }}">
                    </div> --}}

                    <!-- Message Form Input -->
                    <div class="form-group">
                        <label class="control-label">Message</label>
                        <textarea name="message" class="form-control">{{ old('message') }}</textarea>
                    </div>
                    
                    <input type="hidden" name="recipients" value="{{ $recipient->id }}" />
                    
                    <!-- Submit Form Input -->
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary btn-lg pull-right"><strong>SEND</strong></button>
                    </div>
                </form>
            
            </div>

            @include('spark::nav.footer')

        </div>
    </div>
</div>

</messages>

@endsection
