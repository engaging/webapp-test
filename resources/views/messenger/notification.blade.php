@extends('spark::layouts.app')

@section('content')

    <messages :user="user" notification="{{$thread->id}}" inline-template>

        <div class=" inbox">
        	<div class="container">
        		<div class="sidebar-filters col-sm-4" style="padding:0">
        
        				@include('messenger.partials.sidebar')
        
        		</div>
        		<div class="right col-sm-8">
        
        			<div class="main">

                        <div class="clearfix">
                            <div class="col-xs-12"><h4 class="text-navy upper">{!! $thread->body !!}</h4></div>
                        </div>
                        
                        <hr class="dotted">

                        <div class="message color1">
                            <div class="clearfix author text-gray">
                                
                                Posted {{ $thread->created_at }}
                            </div>
                            <div class="clearfix text">
                                <div class="col-xs-12">
                                     <p>{!! $thread->body !!}</p>

                                    <div class="clearfix text-center">
                                        <a class='btn btn-primary' href="{{ $thread->action_url }}"><strong>{{ $thread->action_text }}</strong></a>
                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>

        			
        			@include('spark::nav.footer')
                    </div>
                    
        		</div>
        	</div>
        </div>
        
    </messages>
@stop

