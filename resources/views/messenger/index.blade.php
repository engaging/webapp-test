@extends('spark::layouts.app')

@section('title', '| Inbox')
<style type="text/css">
	.sidebar .scrolling {
		margin: 5px 0;
	}
</style>
@section('content')


<messages :user="user" inline-template>

<div class=" inbox">
	<div class="container">
		<div class="sidebar-filters col-sm-4" style="padding:0">

				@include('messenger.partials.sidebar')

		</div>
		<div class="col-sm-8 right">
			<div class="main">
				<div class="clearfix empty-inbox">
	        		<h4 class="pad-md hidden-xs">
	        			<i class="fa fa-caret-left"></i> Please click on any of your messages on the left.
	        		</h4>
	        		<h4 class="pad-md visible-xs">
	        			<i class="fa fa-caret-left"></i> Please click on any of your messages above.
	        		</h4>
	        	</div>
				@include('spark::nav.footer')
			</div>
		</div>
	</div>
</div>

</messages>

@endsection
