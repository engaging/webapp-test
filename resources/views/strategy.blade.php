@extends('spark::layouts.app')
@section('title', "| Discover Strategies")
@push('css')
<link href="/css/bootstrap-slider.min.css" rel="stylesheet">
@endpush
@push('js')
<script src="/js/bootstrap-slider.min.js"></script>
@endpush
@section('scripts')
<script src="/js/amcharts/amcharts.js"></script>
<script src="/js/amcharts/radar.js"></script>
<script src="/js/amcharts/xy.js"></script>
<script src="/js/amcharts/themes/light.js"></script>
@stop
@section('content')
<strategy :user="user" :request-filters='{!! str_ireplace("' ", "&apos; ", $requestFilters) !!}'
  cut-off-date='{!! str_ireplace(" '", "&apos;", $maxDate) !!}' inline-template>
  <div>
    <div class="container">
      <!-- Application Dashboard -->

      <div class="row">
        <div class="col-sm-3 col-xs-12">
          <div v-show="showFilters == false" @click="toggleSidebar" class="btn btn-default form-control toggle-sidebar-filters">
            <h4 class="label">FILTER STRATEGIES
              <i class="fa fa-caret-down"></i>
            </h4>
          </div>
          <div v-if="showFilters == false && countActiveFilters > 0" @click="resetFilters()" class="btn btn-default form-control toggle-filters">
            <h4 class="label">
              REMOVE @{{countActiveFilters}} ACTIVE FILTERS
            </h4>
          </div>
        </div>
      </div>

      <div class="row pull-bottom">
        <div v-bind:class="{ 'closed':showFilters != true, 'hidden-xs':showFilters != true }" class="col-sm-3 sidebar-filters panel">

          <div class="clearfix strategy-filter">
            <div class="panel-heading">

              <div @click="toggleSidebar" class="btn btn-default form-control toggle-sidebar-filters">
                <h4 class="label">
                  FILTER STRATEGIES
                  <i class="fa fa-caret-up"></i>
                </h4>
              </div>
              <div class="row">
                <div class="col-xs-10 col-xs-offset-1" style="padding:3px;">
                  <button v-if="countActiveFilters > 0" @click="resetFilters()" class="btn btn-outline form-control toggle-filters" style="font-size: 12px;">
                    Remove @{{countActiveFilters}} Active Filters
                  </button>
                </div>
              </div>



            </div>
            <div class="panel-body">

              <div v-show="filtersLoaded == false" transition="fade" class="clearfix">
                <div class="loader-inner ball-pulse">
                  <div></div>
                  <div></div>
                  <div></div>
                </div>
              </div>
              @include('partials.strategies.filters')

            </div>
          </div>
        </div>

        <div class="col-sm-12 main col-xs-12" v-bind:class="{ 'col-sm-9': (showFilters==true) }">
          <div v-show="indices.length === 0 && (!indicesLoaded || !statsLoaded || loadingNewData)" transition="fade" class="clearfix">
            <div class="loader-inner ball-pulse">
              <div></div>
              <div></div>
              <div></div>
            </div>
          </div>

          <div transition="fade" class="row panel panel-default" id="strategy">
            <div class="panel-body">

              <div class="row">
                <div class="chartfilter pull-right chartfilter-perod-drop col-xs-12" v-bind:class="{'col-sm-4':(!showFilters),'col-sm-6':showFilters}">
                  <div class="row">
                    <div class="col-sm-12 text-right col-xs-12 periodmenu-dropdown-wrapper">
                      @include('partials.periodmenu',array('class'=>'wide'))
                    </div>
                  </div>
                </div>
              </div>

              <div v-show="indices.length === 0 && indicesLoaded && statsLoaded && !loadingNewData">
                <div class="no-strategies-error">
                  <h3 class="text-center pad-top">No strategies were found for your filters.
                    <br>Try entering more general search terms.</h3>
                </div>
              </div>

              <div v-show="indices.length > 0">
                <div id="chartCtx-wrapper">

                  <div v-show="loadingNewData && indices.length > 0" transition="fade" class="clearfix loader-overlay">
                    <div class="loader-inner ball-pulse">
                      <div></div>
                      <div></div>
                      <div></div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="chartfilter col-xs-12" v-bind:class="{'col-sm-8 pull-right mobile-row':(!showFilters),'col-sm-12':showFilters}">
                      <div class="row">
                        <div class="btn-group col-sm-6 col-xs-6 pull-right" role="group">
                          <div class="btn-group" role="group">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              X Axis (@{{labels[xAxis]}})
                              <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu">
                              <li v-for="(label, key) in labels">
                                <a @click="setXAxis(key)">@{{label}}</a>
                              </li>
                            </ul>
                          </div>
                        </div>

                        <div class="btn-group col-sm-6 col-xs-6" role="group">
                          <div class="btn-group" role="group">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              Y Axis (@{{labels[yAxis]}})
                              <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu">
                              <li v-for="(label, key) in labels">
                                <a @click="setYAxis(key)">@{{label}}</a>
                              </li>
                            </ul>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div style="clear: both"></div>

                  <hr />

                  <div class="clearfix pos-rel hidden-xs">
                    <strong class="pull-right date-label">As of {{ $maxDate }}</strong>
                    <h4 class="text-center upper">@{{period}} Strategy Metrics</h4>
                  </div>
                  <div class="clearfix pos-rel visible-xs text-center">
                    <h4 class="text-center upper">@{{period}} Strategy Metrics</h4>
                    <strong class="date-label-static">As of {{ $maxDate }}</strong>
                  </div>
                  <div id="chartdiv" style="width: 100%; height: 500px;"></div>
                  <a v-if="showAllButton && !loadingNewData" class="button" id="show-all-button" style="position: absolute;top: 225px;right: 5%;"
                    @click="resetSldierFilters()">
                    <img width="19" height="19" class="amcharts-zoom-out-image" src="https://cdn.amcharts.com/lib/3/images/lens.svg">
                    <span>Show all</span>
                  </a>
                </div>

                @include('partials.strategies.comparison-tool')

                <div class="strategy-list row">

                  <div class="row pad-top">
                    <div class="col-sm-7 col-xs-12">
                      <h4 class="label" transition="fade">DISPLAYING @{{ indices.length | thousand_comma }} STRATEGIES</h4>
                    </div>
                    <div class="col-sm-5 col-xs-12">
                      <a class="btn btn-primary saveall form-control" @click="createPortfolio()">
                        <i class="fa fa-plus"></i>
                        <span>Create Portfolio</span>
                      </a>
                    </div>
                  </div>

                  <div class="tableoption float pull-right" v-bind:class="{'opened':showTableOption==1}" data-var="showTableOption">
                    <button @click="showTableOption=showTableOption*-1" class="btn">
                      <span class="icon icon-cog"></span>
                    </button>
                    <div class="menu" v-show="showTableOption==1">
                      <p>Add performance metrics (@{{dataTableColsMax - dataTableCols_raw.length}} left)</p>
                      <label v-for="(v,k) in labels" class="checkbox-style">
                        <input type="checkbox" v-model="dataTableCols_raw" v-bind:value="k" v-bind:disabled='dataTableCols_raw.indexOf(k) < 0 && dataTableCols_raw.length >= dataTableColsMax'>
                        <i></i>
                        @{{v}}
                      </label>
                    </div>
                  </div>

                  <div class="datatable">
                    <div class="clearfix heading">
                      <div style="width:5%" class="text-center">
                        <span class="text-gray"></span>
                      </div>
                      <div style="width:35%" class="namecol">
                        <button class="btn" @click="changeSortby('shortName')">
                          Name
                          <span v-show="sortby!='shortName'">
                            <i class="fa fa-sort"></i>
                          </span>
                          <span v-show="sortby=='shortName'">
                            <i v-bind:class="{
                            'fa-sort-up':(sortorder>0),'fa-sort-down':(sortorder<0)
                            }" class="fa"></i>
                          </span>
                        </button>
                      </div>
                      <div style="width:8%"></div>
                      <div v-bind:style="{width: (47/dataTableColsCount)+'%'}" v-for="(header, field) in labels" v-show="dataTableCols_raw.indexOf(field)>=0"
                        v-bind:class="{'highlight':(sortby==periodKeysForFilters[period] + '_'+field)}">
                        <button @click="changeSortby(periodKeysForFilters[period] + '_' + field)" class="btn">
                          @{{header}}
                          <span v-show="sortby!=periodKeysForFilters[period] + '_'+ field">
                            <i class="fa fa-sort pull-right"></i>
                          </span>
                          <span v-show="sortby==periodKeysForFilters[period] + '_'+ field">
                            <i v-bind:class="{
                            'fa-sort-up':(sortorder>0),'fa-sort-down':(sortorder<0)
                            }" class="fa pull-right"></i>
                          </span>
                        </button>
                      </div>
                    </div>

                    <div class="scrolling">
                      <div v-for="(index,idx) in getDisplayIndices(indices)" class="data-row-parent" :key="index.id">
                        @include('partials.strategies.discover-item')
                      </div>
                      <button class="btn btn-primary btn-show-more" v-if="onDisplay < indices.length" @click="showMore()">Show more</button>
                    </div>
                  </div>
                </div>
              </div>

            </div>
          </div>

          @include('spark::nav.footer')
        </div>
      </div>
    </div>

    <add-strategies :indices-basket="indicesBasket" :period="periodKeysForFilters[period]" :user="user"></add-strategies>
    <create-portfolio :indices="indices" :indices-basket="indicesBasket" :period="periodKeysForFilters[period]"></create-portfolio>
  </div>
</strategy>


@endsection
