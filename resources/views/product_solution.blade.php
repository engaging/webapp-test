@extends('spark::layouts.app') 

@push('css')
    <link href="/css/slick.css" rel="stylesheet">
    <link href="/css/slick-theme.css" rel="stylesheet">
@endpush

@section('content')

<marketing inline-template>
  {{-- non login homepage template --}}
  <div>
  <div class="banner-top pull-top " style="background-image:url({!!$content->banner!!})">
    <div class="banner-mobile visible-xs">
      <img src="{!!$content->mobileBanner!!}" class="img-responsive">
    </div>
    <div class="caption">
      {!! $content->body !!}
    </div>
  </div>

  <div class="clearfix">
    <div class="container-fluid">
	  @for ($i = 0; $i < count($content->productsMatrix); $i++)
      <div class="row product-callout">
	      <div class="container"><div class="row">
		      @if($i % 2 == 1)
		      <div class="col-md-6 col-md-push-6 col-sm-4 col-sm-push-8">
			  @else
		      <div class="col-md-6 col-sm-4">
			  @endif
			  	<div class="img-container" style="background: url({!! $content->productsMatrix[$i]->image !!}) no-repeat center center / cover;">
			  	</div>
			  	<img src="{!! $content->productsMatrix[$i]->image !!}" alt class="img-responsive visible-xs">
		      </div>
		      @if($i % 2 == 1)
		      <div class="col-md-6 col-md-pull-6 col-sm-8 col-sm-pull-4 text-container">
			  @else
		      <div class="col-md-6 col-sm-8 text-container">
			  @endif
			      <div class="heading-container">
				      <h2>{!! $content->productsMatrix[$i]->title !!}</h2>
				      <h3>{!! $content->productsMatrix[$i]->subtitle !!}</h3>
			      </div>
			      {!! $content->productsMatrix[$i]->body !!}
		      </div>
	      </div></div>
      </div>
	  @endfor
    </div>
  </div>


  <div class="clearfix pad-md demo-callout" id="particles-js">
    <div class="container">
      <div class="row marketing ">
        <div class="col-sm-6">
          <div class="clearfix text-white">
            {!! $content->demoCallout !!}
          </div>
        </div>
        <div class="col-sm-6">
          <div class="clearfix text-right request-btn">
		  	<a href="/request" class="btn btn-white">Request a demo</a>
          </div>
        </div>

      </div>
    </div>
  </div>

  @include('spark::nav.footer')
</div>
</marketing>

@endsection

@push('js')
    <script src="/js/slick.min.js"></script>
    <script src="/js/particles.min.js"></script>
    <script>
		$('document').ready(function(){
			particlesJS.load('particles-js', 'js/particles.json');
		});  
	</script>
@endpush