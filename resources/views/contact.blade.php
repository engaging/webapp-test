@extends('spark::layouts.app')

@section('title', '| Contact Us')

@section('content')

<div class="banner-top pull-top" style="background-image:url({!!$content->banner!!})">
    <div class="banner-mobile visible-xs">
        <img src="{!!$content->mobileBanner!!}" class="img-responsive">
    </div>
    <div class="caption">
        <h1>Contact Us</h1>
    </div>
</div>


<div class="clearfix bg-gray pad-md">
    <div class="container">
        @if (isset($message) && $message!='')
            <h3 class="text-center text-navy" style="padding-bottom: 15px;">{!! $message !!}</h3>
        @endif

        <div class="row marketing">
            <div class="col-md-5 richtext">
                {!! $content->body !!}
            </div>
            <div class="col-md-7">
                
                <div class="row">
                    <div class="col-sm-12">
                        <form method="post">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="action" value="contact">
                            <div class="form-group">
                                <input type="text" class="form-control" name="name" placeholder="Name" required>
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-control" name="email" placeholder="Email Address" required>
                            </div>
                            <div class="form-group">
                                <textarea class="form-control" name="message" placeholder="Message" required></textarea>
                            </div>
                            <button class="btn btn-blue pull-right" value="submit">SUBMIT</button>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

@include('spark::nav.footer')


@endsection
