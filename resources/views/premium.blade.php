@extends('spark::layouts.app')

@section('title', '| Plans')

@section('content')

<style type="text/css">
    .datatable .heading>div {
        height: 60px;
    }
    .datatable .data-row > div {
        height: 100px;
        padding: 15px;
    }
    .datatable svg {
        vertical-align: top;
        margin-top: -5px;
    }
    .data-row-parent {
        background: transparent;
    }
    .data-row-parent.last:after {
        display: none;
    }
    .datatable {
        margin: 50px auto;
        position: relative;
    }
    .datatable:after {
        content: "";
        height: 110%;
        width: 34%;
        display: block;
        background: #fff;
        box-shadow: 0 0 30px #aaa;
        position: absolute;
        top: -30px;
        right: 0;
    }
    .datatable .btn {
        margin-top: 15px;
    }
    .datatable .overlay * {
        position: relative;
        z-index: 2;
    }
    .datatable .data-row-parent:after {
        z-index: 5;
    }
</style>

<div class="banner-top pull-top" style="background-image:url({!!$content->banner!!})">
    <div class="banner-mobile visible-xs">
        <img src="{!!$content->mobileBanner!!}" class="img-responsive">
    </div>
    <div class="caption">
        <h1>Plans</h1>
    </div>
</div>


<div class="clearfix bg-darkgray pad-md">
    <div class="container marketing">
        
        <div class="clearfix"> 
            <div class="col-sm-8 col-sm-offset-2 text-center">
                <h3 class="text-navy upper">Lorem ipsum dolor sit amet, consectetur adipiscing elit</h3>
                <p>Morbi odio ipsum, consequat ac feugiat quis, sollicitudin ut augue. Sed vitae tempor velit. Fusce maximus ex a diam tincidunt dapibus. Donec iaculis augue aliquet porta fermentum. Maecenas ac semper risus. Fusce interdum varius urna nec fringilla. Pellentesque vitae pretium risus, ut malesuada nulla. Vivamus ac mattis lectus. Donec sed neque quis lorem pulvinar lobortis et quis odio. Morbi ut est sit amet massa molestie posuere tempor nec elit. Duis ullamcorper mauris eros, vel dignissim lectus cursus sit amet.</p>
            </div>
        </div>

        <div class="clearfix">
            <div class="col-sm-10 col-sm-offset-1">

                <div class="datatable">
                    <div  class="data-row-parent">
                        <div class="clearfix heading nopadding text-center">
                            <div style="width:33%">
                            </div>
                            <div style="width:33%" class="bg-gray">
                                <h4 class="upper text-navy">FREE</h4>
                            </div>
                            <div style="width:33%" class="overlay">
                                <h4 class="upper text-navy">@include('partials.premiumsvg',array('color'=>'#123b69')) PREMIUM</h4>
                            </div>
                        </div>
                    </div>

                    <div class="clearfix">

                        <div class="data-row-parent">
                            <div class="data-row">
                                <div style="width:33%">
                                    <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam congue mi feugiat magna sollicitudin, Nulla fa</span>
                                </div>
                                <div style="width:33%" class="text-center bg-gray">
                                    <img src="/img/design/icon-check-lg.png" alt>
                                </div>
                                <div style="width:33%" class="text-center overlay">
                                    <img src="/img/design/icon-check-lg.png" alt>
                                </div>
                            </div>
                        </div>

                        <div class="data-row-parent">
                            <div class="data-row">
                                <div style="width:33%">
                                    <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam</span>
                                </div>
                                <div style="width:33%" class="text-center bg-gray">
                                    <img src="/img/design/icon-check-lg.png" alt>
                                </div>
                                <div style="width:33%" class="text-center overlay">
                                    <img src="/img/design/icon-check-lg.png" alt>
                                </div>
                            </div>
                        </div>

                        <div class="data-row-parent">
                            <div class="data-row">
                                <div style="width:33%">
                                     <span>Aliquam congue mi feugiat magna sollicitudin, sed posuere nunc pretium. </span>
                                </div>
                                <div style="width:33%" class="text-center bg-gray">

                                </div>
                                <div style="width:33%" class="text-center overlay">
                                    <img src="/img/design/icon-check-lg.png" alt>
                                </div>
                            </div>
                        </div>

                        <div class="data-row-parent">
                            <div class="data-row">
                                <div style="width:33%">
                                    <span>agna sollicitudin, sed posuere nunc pretium. Nullam at commodo mi, non dignissim nisl. Nulla fa</span>
                                </div>
                                <div style="width:33%" class="text-center bg-gray">

                                </div>
                                <div style="width:33%;" class="text-center overlay">
                                    <img src="/img/design/icon-check-lg.png" alt>
                                </div>
                            </div>
                        </div>

                        <div class="data-row-parent last">
                            <div class="data-row">
                                <div style="width:33%">
                                </div>
                                <div style="width:33%" class="text-center bg-gray">
                                    <h4>FREE</h4>
                                </div>
                                <div style="width:33%" class="text-center overlay">
                                    <strong>
                                        <span class="h2 text-navy">$90</span> PER MONTH
                                    </strong>
                                    <br>
                                    <a class="btn btn-primary" href="/premium-requested">UPGRADE NOW</a>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>


        <div class="clearfix bullets"> 
            <div class="col-sm-8 col-sm-offset-2">
                <div class="row">
                    <div class="clearfix">
                        <div class="pincol">
                            <img src="/img/design/icon-pin.png" alt>
                        </div>
                        <div class="text">
                            <p>dio ipsum, consequat ac feugiat quis, sollicitudin ut augue. Sed vitae tempor velit. Fusce maximus ex a diam tincidunt dapibus. Donec iaculis augue aliquet porta fermentum. M</p>
                        </div>
                    </div>

                     <div class="clearfix">
                        <div class="pincol">
                            <img src="/img/design/icon-pin.png" alt>
                        </div>
                        <div class="text">
                            <p>Morbi odio ipsum, consequat ac feugiat quis, sollicitudin ut augue. Sed vitae tempor velit. Fusce maximus ex a diam tincidunt dapibus. Donec iaculis augue aliquet porta</p>
                        </div>
                    </div>

                </div>

            </div>
        </div>

    </div>
</div>

@include('spark::nav.footer')


@endsection
