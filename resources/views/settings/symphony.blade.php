<symphony-account :user="user" inline-template>
    <div class="panel panel-default">
        <div class="panel-heading">Symphony Account</div>

        <div class="panel-body">
            <!-- Alert Message -->
            <div class="alert alert-success" v-if="['verify', 'linked'].includes(status)">
                @{{messages[status]}}
            </div>

            <!-- Link Form -->
            <form class="form-horizontal" role="form" v-if="['unlinked', 'linked'].includes(status)">
                <!-- E-Mail Address -->
                <div class="form-group" :class="{'has-error': form.errors.has('email')}">
                    <label class="col-md-4 control-label">E-Mail Address</label>

                    <div class="col-md-6">
                        <input type="email" class="form-control" name="email" v-model="form.email" :disabled="status === 'linked'">

                        <span class="help-block" v-show="form.errors.has('email')">
                            @{{ form.errors.get('email') }}
                        </span>
                    </div>
                </div>

                <!-- Link / Unlink Buttons -->
                <div class="form-group">
                    <div class="col-md-offset-4 col-md-6">
                        <button type="submit" class="btn btn-primary"
                                @click.prevent="linkAccount"
                                :disabled="form.busy"
                                v-if="status === 'unlinked'">
                            Link
                        </button>
                        <button class="btn btn-default"
                                @click.prevent="unlinkAccount"
                                v-if="status === 'linked'">
                            Unlink
                        </button>
                    </div>
                    <div v-show="form.busy" transition="fade" class="clearfix loader-overlay">
                        <div class="loader-inner ball-pulse">
                            <div></div>
                            <div></div>
                            <div></div>
                        </div>
                    </div>
                </div>
            </form>

            <!-- Verify Form -->
            <form class="form-horizontal" role="form" v-if="status === 'verify'">
                <!-- Verification Code -->
                <div class="form-group" :class="{'has-error': verifyForm.errors.has('code')}">
                    <label class="col-md-4 control-label">Verification Code</label>

                    <div class="col-md-6">
                        <input type="text" class="form-control" name="code" v-model="verifyForm.code">

                        <span class="help-block" v-show="verifyForm.errors.has('code')">
                            @{{ verifyForm.errors.get('code') }}
                        </span>
                    </div>
                </div>

                <!-- Verify / Cancel Buttons -->
                <div class="form-group">
                    <div class="col-md-offset-4 col-md-1">
                        <button type="submit" class="btn btn-primary"
                                @click.prevent="verifyAccount"
                                :disabled="verifyForm.busy">
                            Verify
                        </button>
                    </div>
                    <div class="col-md-1">
                        <button class="btn btn-default"
                                style="margin-left: 15px;"
                                @click.prevent="unlinkAccount">
                            Cancel
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</symphony-account>
