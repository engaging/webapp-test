<connection-requests
    v-if="user.community_member"
    :communitymemberid="user.community_member.id" 
    :isowner='true'
    :canedit='true'
    :isadmin='false'
    inline-template>
    <div class="panel panel-default">
        <a name="connection-requests" id="connection-requests"></a>
        {{-- <div class="panel-heading">
            Connection Requests
        </div> --}}
        <div class="panel-body">
            <div class="clearfix" v-if="connectionRequestsLoaded == false">
                <div class="col-sm-12">
                    LOADING
                </div>
            </div>
            <div class="clearfix" v-show="connectionRequestsLoaded">

                <div class="row" style="margin-bottom: 15px;">
                    <div class="col-sm-3"><button v-bind:class="{'btn-primary':showPanel==1}" class="btn btn-outline-blue form-control" @click="showPanel=1">Pending (@{{ requests.pending.total }})</button></div>
                    <div class="col-sm-3"><button v-bind:class="{'btn-primary':showPanel==2}" class="btn btn-outline-blue form-control" @click="showPanel=2">Accepted (@{{ requests.accepted.total }})</button></div>
                    <div class="col-sm-3"><button v-bind:class="{'btn-primary':showPanel==3}" class="btn btn-outline-blue form-control" @click="showPanel=3">Archived (@{{ requests.rejected.total }})</button></div>
                </div>
                
                    <!-- Tab panes -->
                <div class="clearfix">
                    <div class="scrolling bg-gray" id="pending" v-show="showPanel==1">
                        <div class="clearfix">

                            <div class="col-sm-12" v-if="requests.pending.total == 0">
                                <h3 class="pad-md text-navy text-center">You have no pending connection requests.</h3>
                            </div>

                            <div class="clearfix pad-sm" v-for="request in requests.pending.data">
                                <div class="col-sm-1 text-center">
                                    <label class="checkbox-style text-center">
                                        <input type="checkbox" v-model="userChecked" v-bind:value="request.id"><i></i>
                                    </label>
                                </div>
                                <div class="<?php echo ($size=='small'?'col-sm-5':'col-sm-7'); ?>">
                                    
                                    <div class="title"><span>@{{ request.requestingUser.name }}</span></div>
                                    <small class="text-gray">Requested to connect @{{ request.created_at }}</small>
                                </div>
                                <div class="<?php echo ($size=='small'?'col-sm-3':'col-sm-2'); ?>">
                                     <button class="btn btn-outline-blue lg"  @click="acceptRequest(request)">
                                        Accept
                                    </button>
                                </div>
                                <div class="<?php echo ($size=='small'?'col-sm-3':'col-sm-2'); ?>">
                                    <button class="btn btn-outline lg" @click="rejectRequest(request)">
                                        Reject
                                    </button>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="scrolling bg-gray" id="accepted" v-show="showPanel==2">
                        <div class="clearfix">
                            <div class="col-sm-12" v-if="requests.accepted.total == 0">
                                <h3 class="pad-md text-navy text-center">You have no accepted connection requests.</h3>
                            </div>

                            <div class="clearfix pad-sm" v-for="request in requests.accepted.data">
                                <div class="col-sm-1 text-center">
                                    <label class="checkbox-style text-center">
                                        <input type="checkbox" v-model="userChecked" v-bind:value="request.id"><i></i>
                                    </label>
                                </div>
                                <div class="<?php echo ($size=='small'?'col-sm-5':'col-sm-7'); ?>">
                                    <div class="title"><span>@{{ request.requestingUser.name }}</span></div>
                                    <small class="text-gray">Requested to connect @{{ request.created_at }}</small>
                                </div>
                                
                                <div class="pull-right <?php echo ($size=='small'?'col-sm-6':'col-sm-4'); ?>">
                                     <button class="btn btn-outline lg" @click="rejectRequest(request)">
                                       Disconnect
                                    </button>
                                </div>
                            </div>
                           
                        </div>
                    </div>
                    <div class="scrolling bg-gray" id="rejected" v-show="showPanel==3">
                        <div class="clearfix">
                            <div class="col-sm-12" v-if="requests.rejected.total == 0">
                                <h3 class="pad-md text-navy text-center">You have no archived connection requests.</h3>
                            </div>

                            <div class="clearfix pad-sm" v-for="request in requests.rejected.data">
                                <div class="col-sm-1 text-center">
                                    <label class="checkbox-style text-center">
                                        <input type="checkbox"><i></i>
                                    </label>
                                </div>
                                <div class="col-sm-7">
                                    <div class="title"><span>@{{ request.requestingUser.name }}</span></div>
                                    <small class="text-gray">Requested to connect @{{ request.created_at }}</small>
                                </div>
                                <div class="col-sm-2">
                                    <button @click="acceptRequest(request)" class="btn btn-outline-blue lg">Accept</button>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>


                <div class="clearfix">
                    <div class="col-sm-8 col-sm-offset-2 text-center pad-top">
                        <button v-if="userChecked.length>0" class="btn btn-primary">Delete @{{userChecked.length}} connections</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</connection-requests>
