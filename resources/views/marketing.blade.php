@extends('spark::layouts.app')

@push('css')
    <link href="/css/slick.css" rel="stylesheet">
    <link href="/css/slick-theme.css" rel="stylesheet">
@endpush

@section('content')

<marketing inline-template>
  {{-- non login homepage template --}}
  <div>
  <div class="banner-top pull-top tall" style="background-image:url({!!$content->banner!!})">
    <div class="banner-mobile visible-xs">
      <img src="{!!$content->mobileBanner!!}" class="img-responsive">
    </div>
    <div class="caption">
      {!! $content->body !!}
      <a href="/request" class="btn btn-white">Request a demo</a>
    </div>
  </div>

  @if ($content->trustedLogos)
  <div class="clearfix bg-gray">
    <div class="container">
      <div class="row pad-md marketing">
	      <div class="col-sm-12 text-center">
		      <h4>Trusted by</h4>
	      </div>
        <div class="col-sm-12 flex-items">
        @foreach ($content->trustedLogos as $logo)
          <img src="{!! $logo !!}" alt>
        @endforeach
        </div>
      </div>
    </div>
  </div>
  @endif

  <div class="particles-js-outer">
	  <div id="particles-js" class="home"></div>
	  <div class="pjs-wrapper">
		  <div class="container pad-md">
		    <div class="row marketing feature-blocks">
		      @for ($i = 0; $i < count($content->featuresMatrix); $i++)
		      @if ($i == 3)
		      <div class="col-sm-4 col-sm-offset-2">
		      @else
		      <div class="col-sm-4">
		      @endif
		        <div class="row">
			        <div class="col-sm-12">
				        <div class="feature-block" style="background-image: url({!! $content->featuresMatrix[$i]->backgroundImage !!});">
					      <div class="mobile-background visible-xs" style="background-image: url({!! $content->featuresMatrix[$i]->backgroundImageMobile !!});"></div>
				          <div class="icon-container">
				            <img src="{!! $content->featuresMatrix[$i]->image !!}" class="img-responsive" alt>
				          </div>
				          <div class="text-content">
				            <h4 class="upper">{!! $content->featuresMatrix[$i]->title !!}</h4>
				            <p>{!! $content->featuresMatrix[$i]->body !!}</p>
				          </div>
				        </div>
			        </div>
		        </div>
		      </div>
		      @endfor
		    </div>
		  </div>
      @if ($content->quoteList)
		  <div class="container pad-md">
		    <div class="row marketing">
			    <div class="col-sm-12">
				    <div class="testimonials-slider">
					    @foreach( $content->quoteList as $quote)
					    <div class="testimonial">
						    <blockquote>{!! $quote->quote !!}</blockquote>
						    <div class="caption row">
							    <div class="col-xs-6 col-sm-3 col-sm-offset-3 logo">
								    <img src="{!! $quote->logo !!}" alt class="img-responsive">
							    </div>
							    <div class="col-xs-6 col-sm-3 person">
								    <strong>{!! $quote->person !!}</strong>
								    <span class="title">{!! $quote->title !!}</span>
							    </div>
						    </div>
					    </div>
					    @endforeach
				    </div>
			    </div>
		    </div>
		  </div>
      @endif
	  </div>
  </div>
  <div class="container ">
    <div class="row marketing">
      <div class="col-sm-6 pad-top-lg caption">
        <img src="{!! $content->floatImage !!}" alt class="img-responsive visible-xs">
        {!! $content->floatText !!}
      </div>
    </div>
  </div>

  <div class="clearfix bg-gray">
    <div class="container">
      <div class="row pad-md marketing">
        <div class="col-sm-6">
        @foreach ($content->middleBody as $col)
          {!! $col->body !!}
        @endforeach
        </div>
      <div class="col-sm-6 pull-up-marketing hidden-xs">
        <img src="{!! $content->floatImage !!}" alt class="img-responsive" style="margin: 0;">
      </div>
      </div>
    </div>
  </div>

  {{--<div class="clearfix pad-md marketing-bottom" style="background-size:cover;background-position:bottom;background-image:url({!! $content->lowerBodyImage !!});">
    <div class="banner-mobile visible-xs">
      <img src="{!! $content->lowerBodyImage !!}" alt class="img-responsive">
    </div>
    <div class="container">
      <div class="row marketing text-white">
        <div class="col-sm-6">
          <div class="clearfix text-center">
            {!! $content->lowerBody !!}
          </div>
          <div class="bullets">
            @foreach ($content->lowerBodyList as $pin)
            <div class="clearfix">
              <div class="pincol">
                <img src="/img/design/icon-pin-white.png" alt>
              </div>
              <div class="text">
                <p>{!! $pin->body !!}</p>
              </div>
            </div>
            @endforeach
          </div>
        </div>

      </div>
    </div>
  </div>--}}


  <div class="clearfix pad-md marketing-bottom style2" style="background-size:cover;background-position:bottom;background-image:url({!! $content->thirdBlockImage !!});">
    <div class="banner-mobile visible-xs" style="padding-left: 15px; padding-right: 15px;">
      <img src="{!! $content->thirdBlockImageMobile !!}" alt class="img-responsive" style="width: 100%;">
    </div>
    <div class="container">
      <div class="row marketing ">
        <div class="col-sm-offset-6 col-sm-6">
          <div class="clearfix text-left">
            {!! $content->thirdBlock !!}
          </div>
        </div>

      </div>
    </div>
  </div>

  @include('spark::nav.footer')
</div>
</marketing>

@endsection

@push('js')
    <script src="/js/slick.min.js"></script>
    <script src="/js/particles.min.js"></script>
    <script>
		$('document').ready(function(){
			$('.testimonials-slider').slick({
				dots: true,
				autoplay: true,
				autoplaySpeed: 3000,
			})
			particlesJS.load('particles-js', 'js/particles.json');
		});
	</script>
@endpush
