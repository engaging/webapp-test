@extends('spark::layouts.app')

@section('content')
<home :user="user" inline-template>
    <div>
      <div class="container">
          <!-- Application Dashboard -->
          <div class="row">
              <div class="col-md-8 col-md-offset-2">
                  <div class="clearfix pad-md"> <h2>Thank you for requesting access.</h2><p>engagingenterprises will review your application and notify you by email if your account is approved.</p></div>
              </div>
          </div>
      </div>
      @include('spark::nav.footer')
  </div>
    
</home>
@endsection


{{-- <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Spark</title>
    <link href='/css/google-font-opensans-300-400-600.css' rel='stylesheet' type='text/css'>
	
    <link href="/css/app.css" rel="stylesheet">
    <style>
        body, html {
            background: url('/img/spark-bg.png');
            background-repeat: repeat;
            background-size: 300px 200px;
            height: 100%;
            margin: 0;
        }

        .full-height {
            min-height: 100%;
        }

        .flex-column {
            display: flex;
            flex-direction: column;
        }

        .flex-fill {
            flex: 1;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }


        .text-center {
            text-align: center;
        }

        .links {
            padding: 1em;
            text-align: right;
        }

        .links a {
            text-decoration: none;
        }

        .links button {
            background-color: #3097D1;
            border: 0;
            border-radius: 4px;
            color: white;
            cursor: pointer;
            font-family: 'Open Sans';
            font-size: 14px;
            font-weight: 600;
            padding: 15px;
            text-transform: uppercase;
            width: 100px;
        }
    </style>
</head>
<body>
    <div class="full-height flex-column">
        <nav class="links">
			<div class="logo pull-left">
				fintech
			</div>
            <a href="/register">
                <button>
                    Request Access
                </button>
            </a>

            <a href="/login" style="margin-right: 15px;">
                    Login
            </a>  
            <a href="/contact" style="margin-right: 15px;">
                    Contact Us
            </a>        
		</nav>

        <div class="flex-fill flex-center">
            <h2>Thank you for requesting access. <br>engagingenterprises will review your application and notify you by email if your account is approved.</h2>
        </div>
    </div>
</body>
</html>
 --}}
