@extends('spark::layouts.app')

@section('content')


<home v-if="user" :user="user" inline-template>
    {{-- logged in user homepage --}}

    <div class="container">
        <!-- Application Dashboard -->
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Welcome to engagingenterprisess</div>

                    <div class="panel-body">
                        Welcome, @{{ user.email }}
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('spark::nav.footer')
</home>


@endsection
