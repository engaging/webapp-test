# base image for PHP version 7.2
FROM php:7.2-apache
MAINTAINER engagingenterprises
USER root
RUN php -v

# environment variables
ENV DEBIAN_FRONTEND noninteractive
ENV PHANTOMJS_VERSION 2.1.1-linux-x86_64

# install all the system dependencies
# install apt-utils for workaround of missing ubuntu package
# install gpg to install nodejs and yarn from source
# install zip and unzip to install composer vendors
# install git to install dependencies from git repositories
# install wkhtmltopdf dependencies to convert html to pdf
RUN apt-get update && apt-get install -y \
  apt-utils \
  gnupg \
  zip \
  unzip \
  git \
  libfontconfig1 \
  zlib1g-dev \
  libfreetype6 \
  libxrender1 \
  libxext6 \
  libx11-6 \
  libssl1.0-dev

# install wkhtmltopdf from source to convert html to pdf
# install wkhtmltopdf 0.12.3 because the quality is better than 0.12.4
RUN curl -L -o wkhtmltopdf.tar.xz https://github.com/wkhtmltopdf/wkhtmltopdf/releases/download/0.12.3/wkhtmltox-0.12.3_linux-generic-amd64.tar.xz
RUN tar -xf wkhtmltopdf.tar.xz
RUN mv wkhtmltox/bin/wkhtmltopdf /usr/local/bin/wkhtmltopdf
RUN chmod +x /usr/local/bin/wkhtmltopdf
RUN wkhtmltopdf -V

# install nodejs and yarn from source to build JS front-end distribution
RUN curl -sL https://deb.nodesource.com/setup_8.x | bash -
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
RUN echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
RUN apt-get update && apt-get install -y yarn
RUN echo -n "node "; node -v
RUN echo -n "yarn "; yarn -v

# install phantomjs for pdf generation
RUN curl -L -o phantomjs.tar.bz2 https://bitbucket.org/ariya/phantomjs/downloads/phantomjs-$PHANTOMJS_VERSION.tar.bz2
RUN tar xvjf phantomjs.tar.bz2
RUN mv phantomjs-$PHANTOMJS_VERSION/bin/phantomjs /usr/local/bin/phantomjs
RUN chmod +x /usr/local/bin/phantomjs
RUN phantomjs -v

# install PHP composer to install PHP back-end vendors
RUN curl -sS https://getcomposer.org/installer | php -- --filename=composer --install-dir=/usr/local/bin

# install APCu Object Cache Backend PHP extension
RUN pear config-set php_ini /usr/local/etc/php/php.ini  && \
    pecl config-set php_ini /usr/local/etc/php/php.ini   && \
    pecl install apcu

# install other PHP extensions
# install pdo_mysql to connect to MySQL database
# install zip dependency for PHPExcel vendor
RUN docker-php-ext-install pdo_mysql zip

# configure apache2
RUN rm -rf /etc/apache2/sites-enabled/000-default.conf
ADD apache/000-default.conf /etc/apache2/sites-available/
RUN ln -s /etc/apache2/sites-available/000-default.conf /etc/apache2/sites-enabled/
RUN a2enmod rewrite     && \
    a2enmod expires     && \
    a2enmod mime        && \
    a2enmod filter      && \
    a2enmod deflate     && \
    a2enmod proxy_http  && \
    a2enmod headers     && \
    a2enmod php7

# change folders ownerships
RUN chown -R www-data:www-data /var/www && \
    chown -R www-data:www-data /var/log/apache2/ && \
    rm -Rvf /var/www/html/*

# add project to /var/www/html
ADD . /var/www/html

# change /var/www/html folder ownership
RUN chown -R www-data:www-data /var/www/html

# change working directory to /var/www/html
WORKDIR /var/www/html

# switch to www-data user
USER www-data

# copy .env.example to .env
RUN cp /var/www/html/.env.example /var/www/html/.env

# install JS front-end packages using yarn
ADD package.json /tmp/package.json
ADD yarn.lock /tmp/yarn.lock
RUN cd /tmp && yarn --frozen-lockfile
RUN mkdir -p /var/www/html && cd /var/www/html && rm -rf node_modules && ln -sf /tmp/node_modules

# install PHP back-end vendors using composer
# https://getcomposer.org/doc/faqs/how-to-install-untrusted-packages-safely.md
# https://adamcod.es/2013/03/07/composer-install-vs-composer-update.html
RUN composer install

# patch PHP back-end vendors
# patch laravel unfixed issues https://github.com/laravel/framework/issues/20248
RUN mv \
  vendor-patch/laravel/framework/src/Illuminate/Database/Eloquent/Builder.php \
  vendor/laravel/framework/src/Illuminate/Database/Eloquent/Builder.php
RUN mv \
  vendor-patch/laravel/framework/src/Illuminate/Database/Eloquent/SoftDeletingScope.php \
  vendor/laravel/framework/src/Illuminate/Database/Eloquent/SoftDeletingScope.php
RUN mv \
  vendor-patch/laravel/framework/src/Illuminate/Database/Query/Builder.php \
  vendor/laravel/framework/src/Illuminate/Database/Query/Builder.php


# build JS front-end distribution using yarn
RUN yarn build

# change Laravel folders ownerships
RUN chgrp -R www-data /var/www/html/storage /var/www/html/bootstrap/cache  && \
    chmod -R ug+rwx /var/www/html/storage /var/www/html/bootstrap/cache

# create mandatory Laravel folders
RUN mkdir -p /var/www/html/storage/framework          && \
    mkdir -p /var/www/html/storage/framework/sessions && \
    mkdir -p /var/www/html/storage/framework/views    && \
    mkdir -p /var/www/html/storage/meta               && \
    mkdir -p /var/www/html/storage/cache              && \
    mkdir -p /var/www/html/public/uploads/

# cleanup
RUN rm /var/www/html/.env

# switch to root user
USER root
