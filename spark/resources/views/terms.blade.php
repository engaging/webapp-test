@extends('spark::layouts.app')

@section('content')
<div class="container">
    <!-- Application Dashboard -->
    <div class="row pad-top">
        <div class="col-md-8 col-md-offset-2 marketing">
            <div class="richtext">
                    {{-- {!! $terms !!} --}}

                {!! $content->body !!}

            </div>
        </div>
    </div>
</div>
	@include('spark::nav.footer')

@endsection
