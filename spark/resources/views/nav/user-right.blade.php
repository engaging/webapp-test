<!-- Right Side Of Navbar -->
<li>
	<a href="/dashboard" class="{{strpos( Route::getCurrentRoute()->getPath(), 'dashboard')===0?'active':''}}">
	Dashboard
	</a>
</li>
<li>
	<a href="/community" class="{{strpos( Route::getCurrentRoute()->getPath(), 'community')===0?'active':''}}{{strpos( Route::getCurrentRoute()->getPath(), 'sponsor')===0?'active':''}}">
	Community
	</a>
</li>
<li>
	<a href="/strategy" class="{{strpos( Route::getCurrentRoute()->getPath(), 'strategy')===0?'active':''}}">
	Discover Strategies
	</a>
</li>
<li>
	<a href="/analyse/" class="{{strpos( Route::getCurrentRoute()->getPath(), 'analyse')===0?'active':''}}">
  Analyse & Design
	</a>
</li>
<li>
	<a href="/portfolios" class="{{strpos( Route::getCurrentRoute()->getPath(), 'portfolios')===0?'active':''}}">
  My Lab
	</a> 
</li>
