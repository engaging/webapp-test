<nav class="navbar navbar-inverse guest">
    <div class="container">
        <div class="navbar-header">
            <!-- Collapsed Hamburger -->
            {{-- <div class="hamburger">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#spark-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div> --}}

            <!-- Branding Image -->
            @include('spark::nav.brand')
        </div>

        <div class="clearfix">
            <!-- Right Side Of Navbar -->
            <ul class="nav navbar-nav navbar-right">
	            <li>
	            <button class="hamburger hamburger--collapse visible-xs" type="button">
				  <span class="hamburger-box">
				    <span class="hamburger-inner"></span>
				  </span>
				</button> 
	            </li>
                <li><a href="/product-solution" class="navbar-link hidden-xs">Product & Solution</a></li>
                <li><a href="/contact" class="navbar-link hidden-xs">Contact Us</a></li>
                <li>
                    <a href="/request" class="btn btn-blue">Register</a>
                </li>
                <li><a href="#" data-toggle="modal" data-target=".login-modal" class="btn btn-outline-blue">Login</a></li>
            </ul>
        </div>
        <div class="clearfix mobile-nav-wrapper">
			<ul class="nav navbar-nav mobile-nav">
                <li><a href="/product-solution" class="navbar-link">Product & Solution</a></li>
                <li><a href="/contact" class="navbar-link">Contact Us</a></li>
			</ul>
        </div>
    </div>
</nav>


<div class="modal fade login-modal" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Log In</h4>
        </div>
        <div class="modal-body">
            @include('spark::auth.login-form')
        </div>
    </div>
  </div>
</div>
