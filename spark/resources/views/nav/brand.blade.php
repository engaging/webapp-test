<a class="navbar-brand" href="/home">
    <img src="/img/design/logo.png" alt="engagingenterprises" class="hidden-xs" style="max-width: 275px;">
    <img src="/img/design/logo.png" alt="engagingenterprises" class="visible-xs" style="max-width: 200px;">
</a>
@if (session('stg') == 'stg')
	<span class="label label-danger">STAGING</span>
@endif
