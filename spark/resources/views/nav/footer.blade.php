<div class="footer container">
	<div class="row">


		<?php

			if (Request::is('community/*') || Request::is('community')) {
				?>
				<div class="col-xs-6">
					<p class="visual-credits"><strong>Visual credits: iStockphoto</strong></p>
					<p class="terms-link"><a href="/terms"><strong>Terms and conditions</strong></a> | <a href="/privacy"><strong>Privacy policy</strong></a> &copy; engagingenterprises <?=date("Y")?></p>
				</div>
				<div class="col-xs-6 text-right">
					<p class="factiva-copyright"><img src="/img/factiva.png" style="width: 100px;" class="pull-right" /></p>
					<p class="factset-copyright"><a href="/"><strong>Copyright &copy; <?=date("Y")?> Dow Jones &amp; Company, Inc. All rights reserved.</strong></a></p>
				</div>
				<?php
			} else if (Request::is('/') || Request::is('home') ||Request::is('product-solution')) {
				?>
				<div class="col-sm-6 fact-credits">
	              <strong>Powered by</strong>
	              <img src="/img/factiva.png" class="img-responsive" />
	              <img src="/img/factset.png" class="img-responsive" />
				</div>
				<div class="col-sm-6 text-right terms-text">
					<p class="terms-link"><a href="/terms"><strong>Terms and conditions</strong></a> | <a href="/privacy"><strong>Privacy policy</strong></a> <br class="visible-xs">&copy; engagingenterprises <?=date("Y")?></p>
					<p class="factset-copyright"><strong>&copy; Powered by FactSet, Copyright <?=date("Y")?> FactSet UK Ltd. <br class="visible-xs">All rights reserved.</strong></p></div>
				<?php
			} else {
				?>
				<div class="col-sm-12 text-right terms-text">
					<p class="terms-link"><a href="/terms"><strong>Terms and conditions</strong></a> | <a href="/privacy"><strong>Privacy policy</strong></a> <br class="visible-xs">&copy; engagingenterprises <?=date("Y")?></p>
					<p class="factset-copyright"><strong>&copy; Powered by FactSet, Copyright <?=date("Y")?> FactSet UK Ltd. <br class="visible-xs">All rights reserved.</strong></p></div>
				<?php
			}

		?>
	</div>
</div>
