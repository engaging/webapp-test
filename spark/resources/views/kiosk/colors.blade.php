<spark-kiosk-colors :user="user" inline-template>
  <div>
    <!-- Loading Indicator -->
    <div class="row" v-if="loading">
      <div class="col-md-12">
        <div class="panel panel-default">
          <div class="panel-body">
            <i class="fa fa-btn fa-spinner fa-spin"></i>Loading
          </div>
        </div>
      </div>
    </div>

    <!-- User Profile -->
    <div v-if=" ! loading">
      <div class="row">
        <div class="col-md-12">
          <div class="panel panel-default">
            <div class="panel-heading">
              <!-- User Name -->
              <div class="pull-left">
                <div class="btn-table-align">
                  Update Colour Scheme
                </div>
              </div>



              <div class="clearfix"></div>
            </div>

            <div class="panel-body">
              <form class="form-horizontal" role="form">
                <div class="row">
                  <div class="col-sm-12">

                    <h3>Benchmarks 1 - 12</h3>


                    <div class="form-group" v-for="idx in numColors" :class="{'has-error': colorsForm.errors.has('color' + idx)}" v-if="idx>0">
                      <label class="col-md-4 control-label">Benchmark @{{ idx }}</label>

                      <div class="col-md-6">
                        <input class="form-control" required type="color" :name="'color'+idx" rows="7" v-model="colorsForm['color'+idx]" style="font-family: monospace;"
                        />

                        <span class="help-block" v-show="colorsForm.errors.has('color'+idx)">
                          @{{ colorsForm.errors.get('color'+idx) }}
                        </span>
                      </div>
                      <div class="col-md-2">
                        <div style="width: 50px; height: 20px; display: block" v-bind:style="{backgroundColor: colorsForm['color'+idx] }">

                        </div>
                      </div>
                    </div>

                  </div>
                  <div class="col-sm-12">
                    <hr />
                  </div>
                  <div class="col-sm-12">

                    <h3>Asset Classes</h3>

                    <div class="form-group" v-for="(item, key) in colorsForm['assetClass']">
                      <label class="col-md-4 control-label">@{{ key }}</label>

                      <div class="col-md-6">
                        <input class="form-control" required type="color" :name="item" rows="7" v-model="colorsForm['assetClass'][key]" style="font-family: monospace;"
                        />
                      </div>
                      <div class="col-md-2">
                        <div style="width: 50px; height: 20px; display: block" v-bind:style="{backgroundColor: colorsForm['assetClass'][key] }">

                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="col-sm-12">
                    <hr />
                  </div>
                  <div class="col-sm-12">

                    <h3>Factors @{{ colorsForm['factor'].length }}</h3>

                    <div class="form-group" v-for="(item, key) in colorsForm['factor']">
                      <label class="col-md-4 control-label">@{{ key }}</label>

                      <div class="col-md-6">
                        <input class="form-control" required type="color" :name="item" rows="7" v-model="colorsForm['factor'][key]" style="font-family: monospace;"
                        />
                      </div>
                      <div class="col-md-2">
                        <div style="width: 50px; height: 20px; display: block" v-bind:style="{backgroundColor: colorsForm['factor'][key] }">

                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="form-group">
                  <div class="col-md-offset-4 col-md-6">
                    <button type="submit" class="btn btn-primary" @click.prevent="update" :disabled="colorsForm.busy">

                      Save
                    </button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</spark-kiosk-colors>