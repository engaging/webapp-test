<spark-kiosk-users :user="user" inline-template>
  <div>
    <div>
      <div v-show="showing === 'search'">
        <!-- Searching -->
        <div class="panel panel-default" v-if="searching">
          <div class="panel-heading">Users</div>

          <div class="panel-body">
            <i class="fa fa-btn fa-spinner fa-spin"></i>Loading
          </div>
        </div>

        <!-- User Search Results -->
        <div class="panel panel-default" v-if="!searching">
          <div class="panel-heading">Users
            <a class="pull-right" @click="getUsers()">
              <i class="fa fa-refresh fa-xl"></i>
            </a>
            <a class="pull-right" style="padding-right: 15px;" @click="exportUsers('xls')">
              <i class="fa fa-download fa-xl"></i>
            </a>
            <a class="pull-right" style="padding-right: 15px;" @click="showRegister('xls')">
              <i class="fa fa-user-plus fa-xl"></i>
            </a>

            <!-- Search Field Panel -->
            <div class="row" style="padding-top: 20px;">
              <div class='form-group'>
                <div class="col-sm-12">
                  <input id="kiosk-users-search" class="form-control" name="keyword"
                    placeholder="Keyword" v-model="searchForm.keyword" autocomplete="off" autofocus>
                </div>
              </div>
            </div>
          </div>

          <div class="panel-body">
            <table class="table table-striped m-b-none table-condensed">
              <thead class="">
                <th></th>
                <th class="sortable">
                  <a @click="toggleSort(['firstname', 'lastname'])">
                    Name
                    <i class="fa" :class="{'fa-caret-up': sort.length === 2 && dir == 'asc', 'fa-caret-down': sort.length === 2 && dir == 'desc'}"></i>
                  </a>
                </th>
                <th class="sortable">
                  <a @click="toggleSort('company')">
                    Company
                    <i class="fa" :class="{'fa-caret-up': sort == 'company' && dir == 'asc', 'fa-caret-down': sort == 'company' && dir == 'desc'}"></i>
                  </a>
                </th>
                <th class="sortable">
                  <a @click="toggleSort('email')">
                    E-Mail
                    <i class="fa" :class="{'fa-caret-up': sort == 'email' && dir == 'asc', 'fa-caret-down': sort == 'email' && dir == 'desc'}"></i>
                  </a>
                </th>
                <th class="sortable">
                  <a @click="toggleSort('subscriptions')">
                    Plans
                    <i class="fa" :class="{'fa-caret-up': sort == 'subscriptions' && dir == 'asc', 'fa-caret-down': sort == 'subscriptions' && dir == 'desc'}"></i>
                  </a>
                </th>
                <th class="sortable" style="min-width: 30px;">
                  <a @click="toggleSort('portfoliosCount')">
                    P
                    <i class="fa" :class="{'fa-caret-up': sort == 'portfoliosCount' && dir == 'asc', 'fa-caret-down': sort == 'portfoliosCount' && dir == 'desc'}"></i>
                  </a>
                </th>
                <th class="sortable text-center">
                  <a @click="toggleSort(['approved', 'blocked', 'approval_sent'])">
                    Approval
                    <i class="fa" :class="{'fa-caret-up': sort.length === 3 && dir == 'asc', 'fa-caret-down': sort.length === 3 && dir == 'desc'}"></i>
                  </a>
                </th>
                <th></th>
                <th></th>
              </thead>

              <tbody>
                <tr v-for="searchUser in searchResults" :class="{'danger': searchUser.blocked, 'success': !searchUser.blocked && searchUser.approved}">
                  <!-- Profile Photo -->
                  <td>
                    <div class="btn-table-align">
                      <i v-if="searchUser.blocked" class="fa fa-lock"></i>
                      <i v-if="!searchUser.blocked && searchUser.approved" class="fa fa-check"></i>
                      <i v-if="!searchUser.blocked && !searchUser.approved && searchUser.approval_sent" class="fa fa-clock-o"></i>
                      <i v-if="!searchUser.blocked && !searchUser.approved && !searchUser.approval_sent" class="fa fa-question-circle"></i>
                    </div>
                  </td>

                  <!-- Name -->
                  <td>
                    <div class="btn-table-align">
                      @{{ searchUser.fullname }}
                    </div>
                  </td>

                  <!-- Company -->
                  <td>
                    <div class="btn-table-align">
                      @{{ searchUser.company }}
                    </div>
                  </td>

                  <!-- E-Mail Address -->
                  <td>
                    <div class="btn-table-align" v-html="(searchUser.email && searchUser.email.length > 30) ? searchUser.email.split('@').join('@<br>') : searchUser.email">
                    </div>
                  </td>

                  <!-- Plans -->
                  <td>
                    <div class="btn-table-align" v-html="searchUser.subscriptions.length ? `<strong>${searchUser.subscriptions.map(o => o.name).join('<br>')}</strong>` : 'Free'">
                    </div>
                  </td>

                  <!-- Portfolios -->
                  <td>
                    <div class="btn-table-align">
                      @{{ searchUser.portfoliosCount }}
                    </div>
                  </td>

                  <!-- Approval -->
                  <td class="text-center">
                    <div v-if="!searchUser.blocked">
                      <a class="btn btn-default" v-show="!searchUser.approved && !searchUser.approval_sent" @click="sendApproval(searchUser)">
                        Send Approval
                      </a>

                      <a class="btn btn-default" v-show="!searchUser.approved && searchUser.approval_sent" @click="sendApproval(searchUser)">
                        Re-send Approval
                      </a>
                      <span v-show="!searchUser.approved && searchUser.approval_sent">
                        <small>Sent @{{ dateFormat(searchUser.approval_sent) }}</small>
                      </span>

                      <span v-show="searchUser.approved">
                        Approved
                      </span>
                    </div>

                    <div class="btn-table-align" v-if="searchUser.blocked">
                      Blocked
                    </div>
                  </td>

                  <td>
                    <!-- View User Profile -->
                    <button class="btn btn-default" @click="showUserProfile(searchUser)">
                      <i class="fa fa-search"></i>
                    </button>
                  </td>
                  <td>
                    <!-- Show User Menu -->
                    <div class="dropdown">
                        <a href="#" class="btn btn-default dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            <i class="fa fa-navicon"></i>
                        </a>
                        <ul class="dropdown-menu" role="menu">
                            <!-- User management -->
                            <li class="dropdown-header">User management</li>

                            <li>
                                <a @click="showUserProfile(searchUser)">
                                    <i class="fa fa-fw fa-btn fa-search"></i>Profile
                                </a>
                            </li>

                            <li>
                                <a @click="removeConfirm(searchUser)">
                                    <i class="fa fa-fw fa-btn fa-remove"></i>Delete
                                </a>
                            </li>

                            <li>
                                <a @click="toggleBlocked(searchUser)">
                                    <div v-show="searchUser.blocked">
                                        <i class="fa fa-fw fa-btn fa-unlock"></i>Unblock
                                    </div>
                                    <div v-show="!searchUser.blocked">
                                        <i class="fa fa-fw fa-btn fa-lock"></i>Block
                                    </div>
                                </a>
                            </li>

                            <li>
                                <a @click="impersonate(searchUser)">
                                    <i class="fa fa-fw fa-btn fa-user-secret"></i>Impersonate
                                </a>
                            </li>

                            <li>
                                <a @click="showCopyPortfolioModal(searchUser)">
                                    <i class="fa fa-fw fa-btn fa-copy"></i>Copy portfolio
                                </a>
                            </li>

                            <li class="divider"></li>

                            <!-- Rights management -->
                            <li class="dropdown-header">Rights management</li>

                            <li>
                                <a @click="createInvestorRight(searchUser)">
                                    <i class="fa fa-fw fa-btn fa-unlock-alt"></i>Create Investor right
                                </a>
                            </li>

                            <li class="divider"></li>

                            <!-- Plans management -->
                            <li class="dropdown-header">Plans management</li>

                            <li v-for="plan in plans" :class="{ 'disable-links': !checkPlanId(searchUser, plan) && !plan.active }">
                                <a v-if="checkPlanId(searchUser, plan)" @click="cancelPlan(searchUser, plan)">
                                    <i class="fa fa-fw fa-btn fa-minus"></i>Cancel @{{plan.name}} plan
                                </a>
                                <a v-else-if="searchUser.subscriptions.length" @click="changePlan(searchUser, plan)">
                                    <i class="fa fa-fw fa-btn fa-retweet"></i>Change to @{{plan.name}} plan
                                </a>
                                <a v-else @click="subscribePlan(searchUser, plan)">
                                    <i class="fa fa-fw fa-btn fa-plus"></i>Subscribe @{{plan.name}} plan
                                </a>
                            </li>
                        </ul>
                    </div>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>

    <div>
      <!-- User Profile Detail -->
      <div v-show="showing === 'profile'">
        @include('spark::kiosk.profile')
      </div>
    </div>

    <div>
      <!-- User Register -->
      <div v-show="showing === 'register'">
        @include('spark::kiosk.register')
      </div>
    </div>

    <div>
      <!-- Copy Portfolio Modal -->
      <div class="modal fade" id="modal-copy-portfolio" tabindex="-1" role="dialog">
          <div class="modal-dialog">
              <div class="modal-content">
                  <div class="modal-header">
                      <button type="button " class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                      <h4 class="modal-title">
                          Copy portfolio to @{{ modalUser.fullname }} account
                      </h4>
                  </div>

                  <div class="modal-body">
                      <table class="table table-borderless m-b-none">
                          <thead>
                              <th>Name</th>
                              <th>Composition</th>
                              <th>Model</th>
                              <th></th>
                          </thead>

                          <tbody>
                              <tr v-for="portfolio in portfolios">
                                  <!-- Name -->
                                  <td>
                                      <div class="btn-table-align">
                                        @{{ portfolio.title }}
                                      </div>
                                  </td>

                                  <!-- Composition -->
                                  <td>
                                      <div class="btn-table-align">
                                        @{{ portfolio.strategiesCount }} Strategies
                                      </div>
                                  </td>

                                  <!-- Model -->
                                  <td>
                                      <div class="btn-table-align">
                                        @{{ models[portfolio.weighting] }}
                                      </div>
                                  </td>

                                  <!-- Copy Button -->
                                  <td>
                                      <button class="btn btn-primary" @click="copyPortfolioToUser(modalUser, portfolio)">
                                          <i class="fa fa-copy"></i>
                                      </button>
                                  </td>
                              </tr>
                          </tbody>
                      </table>
                  </div>

                  <!-- Modal Actions -->
                  <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  </div>
              </div>
          </div>
      </div>
    </div>
  </div>
</spark-kiosk-users>
