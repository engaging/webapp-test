<spark-kiosk-profile :user="user" :plans="plans" inline-template>
    <div>
        <!-- Loading Indicator -->
        <div class="row" v-if="loading">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <i class="fa fa-btn fa-spinner fa-spin"></i>Loading
                    </div>
                </div>
            </div>
        </div>

        <!-- User Profile -->
        <div v-if="!loading && profile">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <!-- User Name -->
                            <div class="pull-left">
                                <div class="btn-table-align">
                                    <i class="fa fa-btn fa-close" style="cursor: pointer;" @click="showSearch" title="Close profile"></i>
                                    @{{ profile.firstname ? (profile.firstname + ' ' + profile.lastname) : profile.name }}
                                </div>
                            </div>

                            <!-- Profile Actions -->
                            <div class="pull-right" style="padding-top: 2px;">

                                <button class="btn btn-warning" @click="removeConfirm(profile)">
                                    <span><i class="fa fa-remove"></i>&nbsp;&nbsp;Delete</span>
                                </button>

                                <button class="btn btn-default" @click="toggleBlocked(profile)">
                                    <span v-show="profile.blocked"><i class="fa fa-unlock"></i>&nbsp;&nbsp;Unblock</span>
                                    <span v-show="!profile.blocked"><i class="fa fa-lock"></i>&nbsp;&nbsp;Block</span>
                                </button>

                                <div class="btn-group" role="group">
                                  <button v-show="mode === 'view'" class="btn btn-default" @click="mode = 'edit'">
                                      <span><i class="fa fa-edit"></i>&nbsp;&nbsp;Edit</span>
                                  </button>

                                  <button v-show="mode === 'edit'" class="btn btn-primary" @click="saveUser">
                                      <span><i class="fa fa-save"></i>&nbsp;&nbsp;Save</span>
                                  </button>

                                  <button v-show="mode === 'edit'" class="btn btn-default" @click="getUserProfile(profile.id)">
                                      <span><i class="fa fa-undo"></i>&nbsp;&nbsp;Cancel</span>
                                  </button>
                                </div>

                                <div class="btn-group" role="group">
                                    <!-- Apply Discount -->
                                    <button class="btn btn-default" v-if="spark.usesStripe && profile.stripe_id" @click="addDiscount(profile)">
                                        <i class="fa fa-gift"></i>
                                    </button>


                                    <!-- Impersonate Button -->
                                    <button class="btn btn-default" @click="impersonate(profile)" :disabled="user.id === profile.id">
                                        <i class="fa fa-user-secret"></i>&nbsp;&nbsp;Impersonate
                                    </button>
                                </div>
                            </div>

                            <div class="clearfix"></div>
                        </div>

                        <div class="panel-body">
                            <div class="row">
                                <!-- Profile Photo -->
                                <div class="col-md-3 text-center">
                                    <img :src="profile.photo_url" class="spark-profile-photo-xl">
                                </div>

                                <div class="col-md-9">
                                    <!-- Email Address -->
                                    <p>
                                        <strong>Email Address:</strong> <a href="'mailto:' + profile.email">@{{ profile.email }}</a>
                                    </p>

                                    <!-- Joined Date -->
                                    <p>
                                        <strong>Joined:</strong> @{{ profile.created_at | datetime }}
                                    </p>

                                    <!-- Status -->
                                    <p>
                                        <strong>Status:</strong>
                                        <span v-show="profile.blocked">
                                          Blocked
                                        </span>
                                        <span v-show="!profile.blocked && profile.approved">
                                          Approved
                                        </span>
                                        <span v-show="!profile.blocked && !profile.approved && !profile.approval_sent">
                                          Pending Approval
                                        </span>
                                        <span v-show="!profile.blocked && !profile.approved && profile.approval_sent">
                                          Approval Sent
                                        </span>
                                    </p>

                                    <!-- Plans -->
                                    <p>
                                        <strong>Plans:</strong> @{{ profile.subscriptions.length ? profile.subscriptions.map(o => o.name).join(', ') : 'Free' }}
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="clearfix pad-md">
                        <div class="container-inner">
                            <h4>Personal information</h4>

                            {{-- name --}}
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label>First Name</label>
                                        <input type="name" class="form-control" name="firstname"  required v-model="profile.firstname" autofocus :disabled="mode !== 'edit'">
                                    </div>
                                     <div class="col-sm-6">
                                        <label>Last Name</label>

                                        <input type="name" class="form-control" name="lastname" required v-model="profile.lastname" autofocus :disabled="mode !== 'edit'">
                                    </div>
                                </div>
                            </div>

                            {{-- company --}}
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label for="company">Company</label>
                                        <input type="text" class="form-control" name="company" v-model="profile.company" :disabled="mode !== 'edit'">
                                    </div>
                                    <div class="col-sm-6">
                                        <label>Job Title</label>
                                        <input type="text" class="form-control" name="title" v-model="profile.title" :disabled="mode !== 'edit'">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label>Your Sector</label>
                                        <select class="form-control" name="sector" v-model="profile.sector" :disabled="mode !== 'edit'">
                                            <option value="" selected disabled hidden></option>
                                            <option value="Asset management">Asset management</option>
                                            <option value="Bank">Bank</option>
                                            <option value="Consulting">Consulting</option>
                                            <option value="Corporate">Corporate</option>
                                            <option value="Family office">Family office</option>
                                            <option value="Insurance company">Insurance company</option>
                                            <option value="Pension fund">Pension fund</option>
                                            <option value="Private bank">Private bank</option>
                                            <option value="Sovereign wealth fund">Sovereign wealth fund</option>
                                            <option value="Other">Other</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="clearfix pad-md">
                        <div class="container-inner">
                            <h4>Contact Information & Login</h4>

                            <!-- E-Mail Address -->
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label>Work Email</label>
                                        <input type="email" class="form-control" name="email" v-model="profile.email" :disabled="mode !== 'edit'">
                                    </div>
                                    <div class="col-sm-6">
                                        <label>Supplementary Email</label>
                                        <input type="text" class="form-control" placeholder="Optional" name="secondary_email" v-model="profile.secondary_email" :disabled="mode !== 'edit'">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label>Telephone</label>
                                        <input type="text" class="form-control" name="telephone"  v-model="profile.telephone" :disabled="mode !== 'edit'">
                                    </div>
                                    <div class="col-sm-6">
                                        <label>Country</label>
                                        <?php
                    												$topCountries = [
                    													'AU' => 'Australia',
                    													'AT' => 'Austria',
                    													'BE' => 'Belgium',
                    													'CA' => 'Canada',
                    													'CN' => 'China',
                    													'DK' => 'Denmark',
                    													'FI' => 'Finland',
                    													'FR' => 'France',
                    													'DE' => 'Germany',
                    													'HK' => 'Hong Kong',
                    													'IT' => 'Italy',
                    													'JP' => 'Japan',
                    													'KR' => 'Korea, Republic of',
                    													'LU' => 'Luxembourg',
                    													'NL' => 'Netherlands',
                    													'NZ' => 'New Zealand',
                    													'NO' => 'Norway',
                    													'PT' => 'Portugal',
                    													'SG' => 'Singapore',
                    													'ES' => 'Spain',
                    													'SE' => 'Sweden',
                    													'CH' => 'Switzerland',
                    													'TW' => 'Taiwan',
                    													'GB' => 'United Kingdom',
                    													'US' => 'United States',
                    												];
                    										?>
                                        <select class="form-control" name="country" v-model="profile.country" :disabled="mode !== 'edit'">
                                            <option value="" selected disabled hidden></option>
                                            <optgroup label="Frequent">
                                              @foreach ($topCountries as $key => $country)
                                              <option value="{{ $key }}">{{ $country }}</option>
                                              @endforeach
                                            </optgroup>
                                            <option disabled>&nbsp;</option>
                                            <optgroup label="All">
                                              @foreach (app(Laravel\Spark\Repositories\Geography\CountryRepository::class)->all() as $key => $country)
                                              <option value="{{ $key }}">{{ $country }}</option>
                                              @endforeach
                                            </optgroup>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="clearfix pad-md">
                        <div class="container-inner">
                            <h4>White Labelling</h4>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label>Header Border Color</label>
                                        <input type="color" class="form-control" name="headerLineColor" v-model="profile.headerLineColor">
                                    </div>
                                    <div class="col-sm-6">
                                        <label>Header Text Color</label>
                                        <input type="color" class="form-control" name="headerColor" v-model="profile.headerColor" >
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label>Top Bar Background Color</label>
                                        <input type="color" class="form-control" name="navbarColor" v-model="profile.navbarColor">
                                    </div>
                                    <div class="col-sm-6">
                                        <label>Top Bar Text Color</label>
                                        <input type="color" class="form-control" name="navbarTextColor" v-model="profile.navbarTextColor" >
                                    </div>
                                    <div class="col-sm-6">
                                      <label>Top Bar Border Color</label>
                                      <input type="color" class="form-control" name="navbarLineColor" v-model="profile.navbarLineColor" >
                                  </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label v-if="profile.logoImage && profile.logoImage.length > 3">Current Logo</label>
                                        <img v-if="profile.logoImage && profile.logoImage.length > 3" :src="profile.logoImage" class="img-responsive" />

                                        <div v-if="!image">
                                            <label>Select an image</label>
                                            <input type="file" @change="onFileChange">
                                        </div>
                                        <div v-else>
                                            <img class="img-responsive" :src="image" />
                                            <button class="btn btn-primary" @click="removeImage">Remove image</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <label>Disclaimer</label>
                                        <textarea class="form-control" name="disclaimer" v-model="profile.disclaimer"></textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <button class="btn btn-default" @click="saveUser">Save white labelling</button>
                                        <button class="btn btn-default" @click="revertWhitelabel">Revert white labelling</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Teams -->
            <div class="row" v-if="spark.usesTeams && profile.owned_teams.length > 0">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Teams
                        </div>

                        <div class="panel-body">
                            <table class="table table-borderless m-b-none">
                                <thead>
                                    <th></th>
                                    <th>Name</th>
                                    <th>Subscription</th>
                                </thead>

                                <tbody>
                                    <tr v-for="team in profile.owned_teams">
                                        <!-- Photo -->
                                        <td>
                                            <img :src="team.photo_url" class="spark-team-photo">
                                        </td>

                                        <!-- Team Name -->
                                        <td>
                                            <div class="btn-table-align">
                                                @{{ team.name }}
                                            </div>
                                        </td>

                                        <!-- Subscription -->
                                        <td>
                                            <div class="btn-table-align">
                                                <span v-if="activePlan(team)">
                                                    <a :href="customerUrlOnBillingProvider(team)" target="_blank" rel="noopener noreferrer">
                                                        @{{ activePlan(team).name }} (@{{ activePlan(team).interval | capitalize }})
                                                    </a>
                                                </span>

                                                <span v-else>
                                                    None
                                                </span>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Apply Discount Modal -->
        <div>
            @include('spark::kiosk.modals.add-discount')
        </div>
    </div>
</spark-kiosk-profile>
