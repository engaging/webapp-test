<spark-kiosk-database :user="user" inline-template>
  <div>
    <div>
      <div class="panel panel-default">
        <div class="panel-heading">Database</div>
        <div class="panel-body">
          <div>
            <ul class="nav nav-tabs" role="tablist">
              <li role="presentation" class="active">
                <a href="#cut-off-date" role="tab" data-toggle="tab">Cut-off date</a>
              </li>
            </ul>
            <div class="tab-content">
              <div role="tabpanel" class="tab-pane active" id="cut-off-date">
                <br>
                <div class="row">
                  <div class="col-xs-4">
                    <div class="form-group">
                      <label>Cut-off date</label>
                      <input type="date" class="form-control" v-model="cutOffDate">
                    </div>
                  </div>
                  <div class="col-xs-4 text-center" style="padding-top: 5px;">
                    <br>
                    <button @click="updateCutOffDate()" class="btn btn-default">
                      Update database
                    </button>
                  </div>
                  <div class="col-xs-4 text-center" style="padding-top: 5px;">
                    <br>
                    <button @click="generateTracks(true)" class="btn btn-default">
                      Run datafeed + tracks
                    </button>
                  </div>
                </div>
                <div class="row">
                  <div class="col-xs-4 text-center" style="padding-top: 5px;">
                    <br>
                    <button @click="generateTracks()" class="btn btn-primary">
                      Generate tracks
                    </button>
                  </div>
                  <div class="col-xs-4 text-center" style="padding-top: 5px;">
                    <br>
                    <button @click="generateDiscover()" class="btn btn-primary">
                      Generate discover
                    </button>
                  </div>
                  <div class="col-xs-4 text-center" style="padding-top: 5px;">
                    <br>
                    <button @click="generateMylab()" class="btn btn-primary">
                      Generate mylab + reports
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</spark-kiosk-database>
