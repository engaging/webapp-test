<spark-kiosk-register inline-template>
  <div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <!-- User Name -->
                    <div class="pull-left">
                        <div class="btn-table-align">
                            <i class="fa fa-btn fa-close" style="cursor: pointer;" @click="showSearch" title="Close registration"></i>
                            Register a new user
                        </div>
                    </div>
                    <?php
                      $content = (object)['body' => ''];
                    ?>
                    @include('spark::auth.register-common-form-kiosk')
                </div>
            </div>
        </div>
    </div>
  </div>
</spark-kiosk-register>
