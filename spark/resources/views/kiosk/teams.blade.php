<spark-kiosk-teams :user="user" inline-template>
    <div>
        <!-- Teams -->
        <div>
            <div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                      Teams
                      <a class="pull-right" @click="addTeam">
                        <i class="fa fa-plus-square fa-xl"></i>
                      </a>
                    </div>

                    <div class="panel-body">
                        <table class="table table-borderless m-b-none">
                            <thead>
                                <th>Name</th>
                                <th>Type</th>
                                <th>Members</th>
                                <th></th>
                                <th></th>
                            </thead>

                            <tbody>
                                <tr v-for="team in teams">
                                    <!-- Name -->
                                    <td>
                                        <div class="btn-table-align">
                                          @{{ team.name }}
                                        </div>
                                    </td>

                                    <!-- Type -->
                                    <td>
                                        <div class="btn-table-align">
                                          @{{ team.community_member ? team.community_member.type : 'Investor' }}
                                        </div>
                                    </td>

                                    <!-- Members -->
                                    <td>
                                        <div class="btn-table-align">
                                          @{{ team.users.length }}
                                        </div>
                                    </td>

                                    <!-- Edit Button -->
                                    <td>
                                        <button class="btn btn-primary" @click="editTeam(team)">
                                            <i class="fa fa-pencil"></i>
                                        </button>
                                    </td>

                                    <!-- Delete Button -->
                                    <td>
                                        <button :disabled="team.users.length > 0" class="btn btn-danger-outline" @click="approveTeamDelete(team)">
                                            <i class="fa fa-times"></i>
                                        </button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <!-- Create Team Modal -->
            <div class="modal fade" id="modal-create-team" tabindex="-1" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button " class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                            <h4 class="modal-title">
                                Add Team
                            </h4>
                        </div>

                        <div class="modal-body">
                            <!-- Create Team Form -->
                            <form class="form-horizontal" role="form">
                                <!-- Name -->
                                <div class="form-group" :class="{'has-error': createTeamForm.errors.has('name')}">
                                    <label class="col-md-4 control-label">Name</label>

                                    <div class="col-md-6">
                                        <input type="text" class="form-control" name="name" v-model="createTeamForm.name" autocomplete="off">

                                        <span class="help-block" v-show="createTeamForm.errors.has('name')">
                                            @{{ createTeamForm.errors.get('name') }}
                                        </span>
                                    </div>
                                </div>
                                <!-- Type -->
                                <div class="form-group" :class="{'has-error': createTeamForm.errors.has('type')}">
                                    <label class="col-md-4 control-label">Type</label>

                                    <div class="col-md-6">
                                        <select class="form-control" style="border: 1px solid #ccd0d2;" name="type" v-model="createTeamForm.type">
                                            <option value="">Investor</option>
                                            <option value="Asset Manager">Asset Manager (create a community)</option>
                                            <option value="Index Sponsor">Index Sponsor (create a community)</option>
                                        </select>

                                        <span class="help-block" v-show="createTeamForm.errors.has('type')">
                                            @{{ createTeamForm.errors.get('type') }}
                                        </span>
                                    </div>
                                </div>
                            </form>
                        </div>

                        <!-- Modal Actions -->
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-primary" @click="createTeam">Create</button>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Update Team Modal -->
            <div class="modal fade" id="modal-update-team" tabindex="-1" role="dialog">
                <div class="modal-dialog" v-if="updatingTeam">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button " class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                            <h4 class="modal-title">
                                Edit @{{ updatingTeam.name }} team
                            </h4>
                        </div>

                        <div class="modal-body">
                            <!-- Team Members -->
                            <strong>Team Members</strong>
                            <table class="table table-borderless m-b-none">
                                <tbody>
                                    <tr v-for="user in updatingTeam.users">
                                        <!-- Name -->
                                        <td>
                                            <div class="btn-table-align">
                                              @{{ user.fullname }}
                                            </div>
                                        </td>

                                        <!-- Company -->
                                        <td>
                                            <div class="btn-table-align">
                                              @{{ user.company }}
                                            </div>
                                        </td>

                                        <!-- E-Mail -->
                                        <td>
                                            <div class="btn-table-align" v-html="(user.email && user.email.length > 30) ? user.email.split('@').join('@<br>') : user.email">
                                            </div>
                                        </td>

                                        <!-- Edit Button -->
                                        <td>
                                            <button class="btn btn-primary" @click="deleteTeamUser(user)">
                                                <i class="fa fa-user-times"></i>
                                            </button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <br><br>
                            <!-- Update Team Form -->
                            <form class="form-horizontal" role="form">
                                <!-- Add Member -->
                                <strong>Add Member</strong>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Search User By Keyword</label>

                                    <div class="col-md-6">
                                        <input type="text" class="form-control" name="keyword" v-model="keyword" placeholder="Name, Company or E-Mail" autocomplete="off">
                                    </div>
                                </div>
                            </form>
                            <table class="table table-borderless m-b-none">
                                <tbody>
                                    <tr v-for="user in findUsers">
                                        <!-- Name -->
                                        <td>
                                            <div class="btn-table-align">
                                              @{{ user.fullname }}
                                            </div>
                                        </td>

                                        <!-- Company -->
                                        <td>
                                            <div class="btn-table-align">
                                              @{{ user.company }}
                                            </div>
                                        </td>

                                        <!-- E-Mail -->
                                        <td>
                                            <div class="btn-table-align" v-html="(user.email && user.email.length > 30) ? user.email.split('@').join('@<br>') : user.email">
                                            </div>
                                        </td>

                                        <!-- Edit Button -->
                                        <td>
                                            <button class="btn btn-primary" @click="createTeamUser(user)">
                                                <i class="fa fa-user-plus"></i>
                                            </button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        <!-- Modal Actions -->
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Delete Team Modal -->
            <div class="modal fade" id="modal-delete-team" tabindex="-1" role="dialog">
                <div class="modal-dialog" v-if="deletingTeam">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button " class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                            <h4 class="modal-title">
                                Delete @{{ deletingTeam.name }} team
                            </h4>
                        </div>

                        <div class="modal-body">
                            Are you sure you want to delete this team? This action cannot be undone.
                        </div>

                        <!-- Modal Actions -->
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">No, Go Back</button>

                            <button type="button" class="btn btn-danger" @click="deleteTeam">
                                Yes, Delete It
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</spark-kiosk-teams>
