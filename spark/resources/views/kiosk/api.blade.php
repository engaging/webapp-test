<spark-kiosk-api :user="user" inline-template>
    <div>
        <!-- API Keys -->
        <div>
            <div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                      API Keys
                      <a class="pull-right" @click="addApiKey()">
                        <i class="fa fa-plus-square fa-xl"></i>
                      </a>
                    </div>

                    <div class="panel-body">
                        <table class="table table-borderless m-b-none">
                            <thead>
                                <th>Name</th>
                                <th>Company</th>
                                <th>Scope</th>
                                <th></th>
                                <th></th>
                            </thead>

                            <tbody>
                                <tr v-for="right in rights">
                                    <!-- Name -->
                                    <td>
                                        <div class="btn-table-align">
                                          @{{ right.user.fullname }}
                                        </div>
                                    </td>

                                    <!-- Company -->
                                    <td>
                                        <div class="btn-table-align">
                                          @{{ right.user.company }}
                                        </div>
                                    </td>

                                    <!-- Scope -->
                                    <td>
                                        <div class="btn-table-align">
                                          @{{ right.api_scope }}
                                        </div>
                                    </td>

                                    <!-- Edit Button -->
                                    <td>
                                        <button class="btn btn-primary" @click="editApiKey(right)">
                                            <i class="fa fa-pencil"></i>
                                        </button>
                                    </td>

                                    <!-- Delete Button -->
                                    <td>
                                        <button class="btn btn-danger-outline" @click="approveApiKeyDelete(right)">
                                            <i class="fa fa-times"></i>
                                        </button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <!-- Create API Key Modal -->
            <div class="modal fade" id="modal-create-api-key" tabindex="-1" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button " class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                            <h4 class="modal-title">
                                Add API Key
                            </h4>
                        </div>

                        <div class="modal-body">
                            <!-- Create API Key Form -->
			    <form class="form-horizontal" role="form">
                                <!--  Create API radio button -->
                                <div class="form-group" >
                                  <label class="col-md-4 control-label">Auto Create API</label>
                                    <input type="radio" id="true" :value="true" v-model="autoCreateApi">
                                    <label for="True">True</label>
                                    <input type="radio" id="false" :value="false" v-model="autoCreateApi">
                                    <label for="False">False</label>
                                  <br>
                                </div>

                                <!-- API Key -->
                                <div class="form-group" :class="{'has-error': createApiKeyForm.errors.has('api_key')}">
                                    <label class="col-md-4 control-label">API Key</label>

                                    <div class="col-md-6">
                                        <input type="text" class="form-control" name="api_key" v-model="createApiKeyForm.api_key" autocomplete="off" :disabled="autoCreateApi">

                                        <span class="help-block" v-show="createApiKeyForm.errors.has('api_key')">
                                            @{{ createApiKeyForm.errors.get('api_key') }}
                                        </span>
                                    </div>
                                </div>
                                <!-- API Secret -->
                                <div class="form-group" :class="{'has-error': createApiKeyForm.errors.has('api_secret')}">
                                    <label class="col-md-4 control-label">API Secret</label>

                                    <div class="col-md-6">
                                        <input type="password" class="form-control" name="api_secret" v-model="createApiKeyForm.api_secret" autocomplete="off">

                                        <span class="help-block" v-show="createApiKeyForm.errors.has('api_secret')">
                                            @{{ createApiKeyForm.errors.get('api_secret') }}
                                        </span>
                                    </div>
                                </div>
                                <!-- API Secret Confirmation -->
                                <div class="form-group" :class="{'has-error': createApiKeyForm.errors.has('api_secret_confirmation')}">
                                    <label class="col-md-4 control-label">Confirm API Secret</label>

                                    <div class="col-md-6">
                                        <input type="password" class="form-control" name="api_secret_confirmation" v-model="createApiKeyForm.api_secret_confirmation" autocomplete="off">

                                        <span class="help-block" v-show="createApiKeyForm.errors.has('api_secret_confirmation')">
                                            @{{ createApiKeyForm.errors.get('api_secret_confirmation') }}
                                        </span>
                                    </div>
                                </div>
                                <!-- API Scope -->
                                <div class="form-group" :class="{'has-error': createApiKeyForm.errors.has('api_scope')}">
                                    <label class="col-md-4 control-label">Scope</label>

                                    <div class="col-md-6">
                                        <input type="text" class="form-control" name="api_scope" v-model="createApiKeyForm.api_scope">

                                        <span class="help-block" v-show="createApiKeyForm.errors.has('api_scope')">
                                            @{{ createApiKeyForm.errors.get('api_scope') }}
                                        </span>
                                    </div>
                                </div>
                                <!-- Search User By Keyword -->
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Search User By Keyword</label>

                                    <div class="col-md-6">
                                        <input type="text" class="form-control" name="keyword" v-model="keyword" placeholder="Name, Company or E-Mail" autocomplete="off">
                                    </div>
                                </div>
                            </form>
                            <table class="table table-borderless m-b-none">
                                <thead>
                                    <th>Name</th>
                                    <th>Company</th>
                                    <th>E-Mail</th>
                                    <th></th>
                                </thead>

                                <tbody>
                                    <tr v-for="user in findUsers">
                                        <!-- Name -->
                                        <td>
                                            <div class="btn-table-align">
                                              @{{ user.fullname }}
                                            </div>
                                        </td>

                                        <!-- Company -->
                                        <td>
                                            <div class="btn-table-align">
                                              @{{ user.company }}
                                            </div>
                                        </td>

                                        <!-- E-Mail -->
                                        <td>
                                            <div class="btn-table-align" v-html="(user.email && user.email.length > 30) ? user.email.split('@').join('@<br>') : user.email">
                                            </div>
                                        </td>

                                        <!-- Edit Button -->
                                        <td>
                                            <button class="btn btn-primary" @click="createApiKey(user)">
                                                <i class="fa fa-plus"></i>
                                            </button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        <!-- Modal Actions -->
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Update API Key Modal -->
            <div class="modal fade" id="modal-update-api-key" tabindex="-1" role="dialog">
                <div class="modal-dialog" v-if="updatingApiKey">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button " class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                            <h4 class="modal-title">
                                Edit API Key (@{{ updatingApiKey.user.fullname }}@{{ updatingApiKey.user.company ? (' - ' + updatingApiKey.user.company) : '' }})
                            </h4>
                        </div>

                        <div class="modal-body">
                            <!-- Update API Key Form -->
                            <form class="form-horizontal" role="form">
                                <!-- API Secret -->
                                <div class="form-group" :class="{'has-error': updateApiKeyForm.errors.has('api_secret')}">
                                    <label class="col-md-4 control-label">API Secret</label>

                                    <div class="col-md-6">
                                        <input type="password" class="form-control" name="api_secret" v-model="updateApiKeyForm.api_secret" autocomplete="off">

                                        <span class="help-block" v-show="updateApiKeyForm.errors.has('api_secret')">
                                            @{{ updateApiKeyForm.errors.get('api_secret') }}
                                        </span>
                                    </div>
                                </div>
                                <!-- API Secret Confirmation -->
                                <div class="form-group" :class="{'has-error': updateApiKeyForm.errors.has('api_secret_confirmation')}">
                                    <label class="col-md-4 control-label">Confirm API Secret</label>

                                    <div class="col-md-6">
                                        <input type="password" class="form-control" name="api_secret_confirmation" v-model="updateApiKeyForm.api_secret_confirmation" autocomplete="off">

                                        <span class="help-block" v-show="updateApiKeyForm.errors.has('api_secret_confirmation')">
                                            @{{ updateApiKeyForm.errors.get('api_secret_confirmation') }}
                                        </span>
                                    </div>
                                </div>
                                <!-- API Scope -->
                                <div class="form-group" :class="{'has-error': updateApiKeyForm.errors.has('api_scope')}">
                                    <label class="col-md-4 control-label">Scope</label>

                                    <div class="col-md-6">
                                        <input type="text" class="form-control" name="api_scope" v-model="updateApiKeyForm.api_scope">

                                        <span class="help-block" v-show="updateApiKeyForm.errors.has('api_scope')">
                                            @{{ updateApiKeyForm.errors.get('api_scope') }}
                                        </span>
                                    </div>
                                </div>
                            </form>
                        </div>

                        <!-- Modal Actions -->
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                            <button type="button" class="btn btn-primary" @click="updateApiKey" :disabled="updateApiKeyForm.busy">
                                Update
                            </button>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Delete API Key Modal -->
            <div class="modal fade" id="modal-delete-api-key" tabindex="-1" role="dialog">
                <div class="modal-dialog" v-if="deletingApiKey">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button " class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                            <h4 class="modal-title">
                                Delete API Key (@{{ deletingApiKey.user.fullname }}@{{ deletingApiKey.user.company ? (' - ' + deletingApiKey.user.company) : '' }})
                            </h4>
                        </div>

                        <div class="modal-body">
                            Are you sure you want to delete this API Key? If deleted, API requests
                            using this API Key will no longer be accepted.
                        </div>

                        <!-- Modal Actions -->
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">No, Go Back</button>

                            <button type="button" class="btn btn-danger" @click="deleteApiKey" :disabled="deleteApiKeyForm.busy">
                                Yes, Delete It
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</spark-kiosk-api>
