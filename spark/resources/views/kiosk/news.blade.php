<spark-kiosk-news :user="user" inline-template>
  <div>
    <div class="panel panel-default clearfix factiva-block">

      <div class="panel-heading">News
        <div class="filter-bar row" v-if="stage=='list'">
          <div class='col-sm-4'>
            <div class="form-group">
              <small>Source</small>
              <select class="form-control" v-model="filter.source">
                <option value="">None</option>
                <option v-for="source in sources" :value="source">@{{source}}</option>
              </select>
            </div>
          </div>
          <div class='col-sm-4'>
            <div class="form-group">
              <small>Start date</small>
              <input type="date" class="form-control" v-model="filter.from">
            </div>
          </div>
          <div class='col-sm-4'>
            <div class="form-group">
              <small>End date</small>
              <input type="date" class="form-control" v-model="filter.to">
            </div>
          </div>
          <div class="col-sm-9">
            <input class="form-control" placeholder="Keyword" v-model="filter.keyword">
          </div>
          <div class="col-sm-3">
            <button class="btn btn-primary btn-block" @click="searchNews()">Search</button>
          </div>
        </div>

        <div class="detail-bar text-right" v-if="stage=='detail'">
          <button class="btn btn-default" @click="showList()">Close</button>
          <button v-if="currentItem.prev !== null" class="btn btn-default" @click="showDetail(currentItem.prev)">
            <span><i class="fa fa-arrow-left"></i>&nbsp;&nbsp;Previous</span>
          </button>
          <button v-if="currentItem.next !== null" class="btn btn-primary" @click="showDetail(currentItem.next)">
            <span><i class="fa fa-arrow-right"></i>&nbsp;&nbsp;Next</span>
          </button>
          <button class="btn btn-warning" style="margin-left: 15px;" @click="deleteItem()">
            <span><i class="fa fa-remove"></i>&nbsp;&nbsp;Delete</span>
          </button>
          <button class="btn btn-danger" @click="unpublishItem()">
            <span><i class="fa fa-undo"></i>&nbsp;&nbsp;Unpublish</span>
          </button>
          <button class="btn btn-primary" @click="publishItem()">
            <span><i class="fa fa-send"></i>&nbsp;&nbsp;Publish</span>
          </button>
        </div>
      </div>

      <div v-if="loading">
        <div class="loader-inner ball-pulse">
          <div></div>
          <div></div>
          <div></div>
        </div>
      </div>

      <div class="filter_result" v-if="!loading && stage === 'list' && listItems.length">
        <div class="form-inline hidden">
          <div class="form-group">
            <select class="form-control" v-model="bulk.status">
              <option value="" selected>-- Bulk Status --</option>
              <option v-for="status in status" :value="status.key">@{{status.label}}</option>
            </select>
            <button class="btn btn-secondary" @click="bulkStatus()">Bulk Edit</button>
          </div>
        </div>

        <table class="table table-striped">
          <thead>
            <tr>
              <th class="hidden">
                <input type='checkbox' value="1" v-model="select_all_btn" />
              </th>
              <th @click="sortBy('source')">Source</th>
              <th @click="sortBy('addedAt')">Date</th>
              <th @click="sortBy('status')">Published</th>
              <th @click="sortBy('read')">Read</th>
            </tr>
          </thead>
          <tbody>
            <tr v-for="(filter_item, filter_idx) in listItems" v-bind:class="{ success: filter_item.clicked }" :id="filter_item.id">
              <td class="hidden">
                <input type='checkbox' value="1" v-model="filter_item.selected" />
              </td>
              <td>
                <a href="javascript:void(0);" @click="showDetail(filter_idx)">@{{filter_item.source}}</a>
              </td>
              <td>@{{filter_item.addedAt | toDate}}</td>
              <td>
                <span class="label" v-bind:class="{'label-success':filter_item.status!='unpublished','label-danger':filter_item.status=='unpublished'}">@{{filter_item.status}}</span>
              </td>
              <td>
                <span class="label" v-bind:class="{'label-success':filter_item.read,'label-danger':!filter_item.read}">@{{filter_item.read}}</span>
              </td>
            </tr>
          </tbody>
        </table>
      </div>

      <div class="panel-body" v-if="!loading && stage === 'detail' && items.length">
        <feed :user="user" :items="items" mode="preview" inline-template>
          <div class="feed-component-wrapper">
            <div v-for="(item, idx) in items">
              <div class="clearfix feed-item" v-bind:class="{'grayin':idx==0 && item.new }">
                <div class="col-xs-2">
                  <div class="provider-img" v-html="item.logo"></div>
                </div>
                <div class="col-xs-10">
                  <div class="row">
                    <div class="col-xs-6">
                      <a href="item.url">
                        <strong class="upper">@{{item.source}}</strong>
                      </a>
                    </div>
                    <div class="col-xs-6 text-right">
                      <span class="text-gray">@{{item.addedAt | toDate}}</span>
                    </div>
                  </div>
                  <div class="clearfix title-row">
                    <div class="col-xs-1" v-if="item.type=='News'">
                      <img src="/img/design/icon-news.png" alt="News" class="img-responsive">
                    </div>
                    <div v-bind:class="{ 'col-xs-11':item.type=='News','col-xs-12':item.type!='News' }">
                      <a :href="item.url">
                        <h4>
                          <strong class="upper" v-html="item.title"></strong>
                        </h4>
                      </a>
                    </div>
                  </div>
                  <p>
                    <span v-html="item.description.slice(0,400)"></span>
                    <span v-if="item.description.length>400">...</span>
                  </p>
                  <hr />
                  <div>
                    <h4>Full Article:</h4>
                    <div v-if="!item.article">
                      <div class="loader-inner ball-pulse">
                        <div></div>
                        <div></div>
                        <div></div>
                      </div>
                    </div>
                    <div v-if="item.article" v-html="item.article"></div>
                  </div>

                  <div v-if="item.image && item.image.length">
                    <a :href="item.url">
                      <img :src="item.image" :alt="item.title" class="img-responsive">
                    </a>
                  </div>
                </div>
              </div>
            </div>
            @include('feed.feed_edit_modal')
          </div>
        </feed>
      </div>
    </div>
  </div>
</spark-kiosk-news>
