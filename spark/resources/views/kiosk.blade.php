@extends('spark::layouts.app')
@section('scripts')
<script src="/js/mousetrap.min.js"></script>
<script src="/js/chart.min.js"></script>

@if (Spark::billsUsingStripe())
  <script src="/js/stripe.js"></script>
@else
  <script src="/js/braintree.js"></script>
@endif

@endsection
@section('content')


<spark-kiosk :user="user" inline-template>
  <div class="container">
    <div class="row">
      <!-- Tabs -->
      <div class="col-md-3">
        <div class="panel panel-default panel-flush">
          <div class="panel-heading">
            Kiosk
          </div>

          <div class="panel-body">
            <div class="spark-settings-tabs">
              <ul class="nav spark-settings-stacked-tabs" role="tablist">

                <!-- Database Link -->
                <li role="presentation" class="active">
                  <a href="#database" aria-controls="database" role="tab" data-toggle="tab">
                    <i class="fa fa-fw fa-btn fa-database"></i>Database
                  </a>
                </li>

                <!-- Upload Link -->
                {{--<li role="presentation">
                  <a href="#upload" aria-controls="upload" role="tab" data-toggle="tab">
                    <i class="fa fa-fw fa-btn fa-upload"></i>Upload
                  </a>
                </li>--}}

                <!-- Colors Link -->
                <li role="presentation">
                  <a href="#colors" aria-controls="colors" role="tab" data-toggle="tab">
                    <i class="fa fa-fw fa-btn fa-paint-brush"></i>Colors
                  </a>
                </li>

                <!-- Announcements Link -->
                <li role="presentation">
                  <a href="#announcements" aria-controls="announcements" role="tab" data-toggle="tab">
                    <i class="fa fa-fw fa-btn fa-bullhorn"></i>Announcements
                  </a>
                </li>

                <!-- Metrics Link -->
                <li role="presentation">
                  <a href="#metrics" aria-controls="metrics" role="tab" data-toggle="tab">
                    <i class="fa fa-fw fa-btn fa-bar-chart"></i>Metrics
                  </a>
                </li>

                <!-- Users Link -->
                <li role="presentation">
                  <a href="#users" aria-controls="users" role="tab" data-toggle="tab">
                    <i class="fa fa-fw fa-btn fa-user"></i>Users
                  </a>
                </li>

                <!-- Teams Link -->
                <li role="presentation">
                    <a href="#teams" aria-controls="teams" role="tab" data-toggle="tab">
                        <i class="fa fa-fw fa-btn fa-users"></i>Teams
                    </a>
                </li>

                <!-- News Link -->
                <li role="presentation">
                  <a href="#news" aria-controls="news" role="tab" data-toggle="tab">
                    <i class="fa fa-fw fa-btn fa-newspaper-o"></i>News
                  </a>
                </li>

                <!-- API Link -->
                <li role="presentation">
                  <a href="#api" aria-controls="api" role="tab" data-toggle="tab">
                    <i class="fa fa-fw fa-btn fa-cubes"></i>API
                  </a>
                </li>

              </ul>
            </div>
          </div>
        </div>
      </div>

      <!-- Tab Panels -->
      <div class="col-md-9">
        <div class="tab-content">
          <!-- Database -->
          <div role="tabpanel" class="tab-pane active" id="database">
            @include('spark::kiosk.database')
          </div>

          <!-- Colors -->
          <div role="tabpanel" class="tab-pane" id="colors">
            @include('spark::kiosk.colors')
          </div>

          <!-- Announcements -->
          <div role="tabpanel" class="tab-pane" id="announcements">
            @include('spark::kiosk.announcements')
          </div>

          <!-- Metrics -->
          <div role="tabpanel" class="tab-pane" id="metrics">
            @include('spark::kiosk.metrics')
          </div>

          <!-- Users -->
          <div role="tabpanel" class="tab-pane" id="users">
            @include('spark::kiosk.users')
          </div>

          <!-- Teams -->
          <div role="tabpanel" class="tab-pane" id="teams">
            @include('spark::kiosk.teams')
          </div>

          <!-- News -->
          <div role="tabpanel" class="tab-pane" id="news">
            @include('spark::kiosk.news')
          </div>

          <!-- API -->
          <div role="tabpanel" class="tab-pane" id="api">
            @include('spark::kiosk.api')
          </div>
        </div>
      </div>
    </div>
  </div>
</spark-kiosk>
@endsection
