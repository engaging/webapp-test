<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Meta Information -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--[if IE]>
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <![endif]-->
    <!--[if lt IE 9]>
      <div>
        Sorry, versions of Internet Explorer lower than 9 are not supported.
        Please update your browser.
      </div>
    <![endif]-->

    <!-- Base Url, Favicon and Title -->
    <base href="{{ url('/') }}">
    <link rel="icon" href="/favicon.ico">
    <title>engagingenterprises @yield('title', '')</title>

    <!-- Fonts -->
    @if (app('request')->input('pdf', '') == '')
    <link href='/css/google-font-firasabs-400-500-700.css' rel='stylesheet' type='text/css'>
    <link href='/css/font-awesome.min.css' rel='stylesheet' type='text/css'>
    @endif

    <!-- CSS -->
    @stack('css')
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link href="/css/sweetalert.css" rel="stylesheet">
    <link href="/css/responsive.css" rel="stylesheet">
    <style>
      [v-cloak] {
        display: none;
      }
    </style>

    <!-- JavaScript -->
    <!-- Spark Globals -->
    <script>
      window.Spark = <?php echo json_encode(array_merge(
        Spark::scriptVariables(), []
      )); ?>
    </script>

    <!-- Env Globals -->
    <script type="text/javascript">
      window.env = {
        API_BASE_URL: "{{ env('API_BASE_URL') }}",
        APP_DEBUG: {{ env('APP_DEBUG') ?: 0 }} === 1,
        {{-- UPLOAD_BUCKET: "{{ env('UPLOAD_BUCKET') ?: 'engagingenterprises-uploads-public' }}", --}}
        {{-- UPLOAD_FOLDER: "{{ env('UPLOAD_FOLDER') ?: 'uploads-dev' }}", --}}
      };
      @if ($devKey = env('API_DEV_KEY'))
        window.env.API_DEV_KEY = "{{ $devKey }}";
      @endif
      @if (isset($templateOptions) && isset($templateOptions['apiToken']))
        window.env.API_TOKEN = "{{ $templateOptions['apiToken'] }}";
      @endif
    </script>

    <!-- Analytics -->
    <script type="text/javascript">
      !function(){var analytics=window.analytics=window.analytics||[];if(!analytics.initialize)if(analytics.invoked)window.console&&console.error&&console.error("Segment snippet included twice.");else{analytics.invoked=!0;analytics.methods=["trackSubmit","trackClick","trackLink","trackForm","pageview","identify","reset","group","track","ready","alias","debug","page","once","off","on"];analytics.factory=function(t){return function(){var e=Array.prototype.slice.call(arguments);e.unshift(t);analytics.push(e);return analytics}};for(var t=0;t<analytics.methods.length;t++){var e=analytics.methods[t];analytics[e]=analytics.factory(e)}analytics.load=function(t){var e=document.createElement("script");e.type="text/javascript";e.async=!0;e.src=("https:"===document.location.protocol?"https://":"http://")+"cdn.segment.com/analytics.js/v1/"+t+"/analytics.min.js";var n=document.getElementsByTagName("script")[0];n.parentNode.insertBefore(e,n)};analytics.SNIPPET_VERSION="4.0.0";
      if (window.location.hostname.toLowerCase() === 'engagingenterprises.co') {
        analytics.load('8FAI9glWy0tuY43LGqwWBD4sjYmzgRr1');
      }
      }}();
    </script>

    <!-- Injected Scripts -->
    @yield('scripts', '')
</head>
<body>
    <div id="spark-app" v-cloak>
        <!-- Bootstrap Alert Messages -->
        @foreach (['success', 'info', 'warning', 'danger'] as $alertType)
          @if ($alertMessage = Session::get($alertType))
            <div class="alert alert-overlay alert-{{ $alertType }} fade in">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                {!! $alertMessage !!}
            </div>
          @endif
        @endforeach
        <alert-modal></alert-modal>

        <!-- Navigation -->
        @if (Auth::check())
            @include('spark::nav.user')
        @else
            @include('spark::nav.guest')
        @endif

        <!-- Main Content -->
        <div id="main-content">
          @yield('content')
        </div>

        <!-- Application Level Modals -->
        @if (Auth::check())
            @include('spark::modals.notifications')
            @include('spark::modals.support')
            @include('spark::modals.session-expired')
        @endif

        {{-- If not premium user --}}
        {{--  <div class="modal fade" id="premium_upgrade_modal">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">
                    @include('partials.premiumsvg',array('color'=>'#123B69'))
                    Upgrade to Premium
                </h4>
              </div>
              <div class="modal-body">
                @include('partials.premiumcallout',array('class'=>''))
              </div>
            </div>
          </div>
        </div>  --}}

    </div>
    <!-- JavaScript -->
    <script src="/js/vendors~main.js"></script>
    <script src="/js/main.js"></script>
    <script src="/js/sweetalert.min.js"></script>
    <script src="/js/bootstrap-wysiwyg.js"></script>

    <!-- Contenteditable pasted as plain text -->
    <script type="text/javascript">
      $('div[contenteditable]').on('paste', function(e) {
        try {
          e.preventDefault();
          var text = e.originalEvent.clipboardData.getData('text/plain');
          var temp = document.createElement('div');
          temp.innerHTML = text;
          document.execCommand('insertHTML', false, temp.textContent);
        } catch(e) {
          console.error(e);
        }
      });
      $('.hamburger').click(function(e){
	    e.preventDefault();
	    $(this).toggleClass('is-active');
	  	$('.mobile-nav-wrapper').slideToggle();    
      });
    </script>

    <!-- Analytics reset on logout -->
    <script type="text/javascript">
      $('a[href="/logout"]').on('click', function(e) {
        try {
          window.analytics && window.analytics.reset();
          window.mixpanel && window.mixpanel.reset();
          e.preventDefault();
          window.location = this.href;
        } catch(e) {
          console.error(e);
        }
      });
    </script>

    @stack('js')
    @yield('footer-scripts', '')
</body>
</html>
