@extends('spark::layouts.app')

@section('scripts')
    @if (Spark::billsUsingStripe())
        <script src="/js/stripe.js"></script>
    @else
        <script src="/js/braintree.js"></script>
    @endif
@endsection

@section('content')
<spark-settings :user="user" :teams="teams" inline-template>
    <div class="spark-screen container">
        <div class="row">
            <!-- Tabs -->
            <div class="col-md-3">
                <div class="panel panel-default panel-flush">
                    <div class="panel-heading">
                        Settings
                    </div>

                    <div class="panel-body">
                        <div class="spark-settings-tabs">
                            <ul class="nav spark-settings-stacked-tabs" role="tablist">
                                <!-- Profile Link -->
                                <li role="presentation">
                                    <a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">
                                        <i class="fa fa-fw fa-btn fa-edit"></i>Your Profile
                                    </a>
                                </li>

                                <!-- Security Link -->
                                <li role="presentation">
                                    <a href="#security" aria-controls="security" role="tab" data-toggle="tab">
                                        <i class="fa fa-fw fa-btn fa-lock"></i>Your Password
                                    </a>
                                </li>

                                <!-- Symphony Link -->
                                <li role="presentation">
                                    <a href="#symphony" aria-controls="symphony" role="tab" data-toggle="tab">
                                        <i class="fa fa-fw fa-btn fa-link"></i>Symphony
                                    </a>
                                </li>

                                @if (Spark::usesTeams())
                                    <!-- Connections Link -->
                                    {{--<li role="presentation">
                                        <a href="#connections" aria-controls="profile" role="tab" data-toggle="tab">
                                            <i class="fa fa-fw fa-btn fa-plug"></i>Your Connections
                                        </a>
                                    </li>--}}

                                    <!-- Teams Link -->
                                    <li v-if="user.user_type == 'admin'" role="presentation">
                                        <a href="#teams" aria-controls="teams" role="tab" data-toggle="tab">
                                            <i class="fa fa-fw fa-btn fa-users"></i>Teams
                                        </a>
                                    </li>
                                @endif

                                <!-- API Link -->
                                @if (Spark::usesApi())
                                    <li role="presentation" v-if="user.user_type == 'admin'">
                                        <a href="#api" aria-controls="api" role="tab" data-toggle="tab">
                                            <i class="fa fa-fw fa-btn fa-cubes"></i>API
                                        </a>
                                    </li>
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>

                <!-- Billing Tabs -->
                @if (Spark::canBillCustomers())
                    <div class="panel panel-default panel-flush">
                        <div class="panel-heading">
                            Billing
                        </div>

                        <div class="panel-body">
                            <div class="spark-settings-tabs">
                                <ul class="nav spark-settings-stacked-tabs" role="tablist">
                                    @if (Spark::hasPaidPlans())
                                        <!-- Subscription Link -->
                                        <li role="presentation">
                                            <a href="#subscription" aria-controls="subscription" role="tab" data-toggle="tab">
                                                <i class="fa fa-fw fa-btn fa-shopping-bag"></i>Subscription
                                            </a>
                                        </li>
                                    @endif

                                    <!-- Payment Method Link -->
                                    <li role="presentation">
                                        <a href="#payment-method" aria-controls="payment-method" role="tab" data-toggle="tab">
                                            <i class="fa fa-fw fa-btn fa-credit-card"></i>Payment Method
                                        </a>
                                    </li>

                                    <!-- Invoices Link -->
                                    <li role="presentation">
                                        <a href="#invoices" aria-controls="invoices" role="tab" data-toggle="tab">
                                            <i class="fa fa-fw fa-btn fa-history"></i>Invoices
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                @endif
            </div>

            <!-- Tab Panels -->
            <div class="col-md-9">
                <div class="tab-content">
                    <!-- Profile -->
                    <div role="tabpanel" class="tab-pane active" id="profile">
                        @include('spark::settings.profile')
                    </div>

                    <!-- Security -->
                    <div role="tabpanel" class="tab-pane" id="security">
                        @include('spark::settings.security')
                    </div>

                    <!-- Symphony -->
                    <div role="tabpanel" class="tab-pane" id="symphony">
                        @include('settings.symphony')
                    </div>

                    <!-- Teams -->
                    @if (Spark::usesTeams())
                        <!-- Connections -->
                        <div role="tabpanel" class="tab-pane" id="connections">
                            @include('settings.connections', array('size'=>''))
                        </div>
                        <!-- Teams -->
                        <div role="tabpanel" class="tab-pane" id="teams">
                            @include('spark::settings.teams')
                        </div>
                    @endif

                    <!-- API -->
                    @if (Spark::usesApi())
                        <div role="tabpanel" class="tab-pane" id="api">
                            @include('spark::settings.api')
                        </div>
                    @endif

                    <!-- Billing Tab Panes -->
                    @if (Spark::canBillCustomers())
                        @if (Spark::hasPaidPlans())
                            <!-- Subscription -->
                            <div role="tabpanel" class="tab-pane" id="subscription">
                                <div v-if="user">
                                    @include('spark::settings.subscription')
                                </div>
                            </div>
                        @endif

                        <!-- Payment Method -->
                        <div role="tabpanel" class="tab-pane" id="payment-method">
                            <div v-if="user">
                                @include('spark::settings.payment-method')
                            </div>
                        </div>

                        <!-- Invoices -->
                        <div role="tabpanel" class="tab-pane" id="invoices">
                            @include('spark::settings.invoices')
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</spark-settings>
@endsection
