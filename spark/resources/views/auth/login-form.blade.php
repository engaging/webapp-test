@include('spark::shared.errors')

<form class="form-horizontal" role="form" method="POST" action="/login">
    {{ csrf_field() }}

    <!-- E-Mail Address -->
    <div class="form-group">
        <div class="col-md-12">
            <label class="control-label">E-Mail Address</label>
            <input type="email" class="form-control" name="email" value="{{ old('email') }}" autofocus>
        </div>
    </div>

    <!-- Password -->
    <div class="form-group">
        <div class="col-md-12">
            <label class="control-label">Password</label>
            <input type="password" class="form-control" name="password">
        </div>
    </div>

    <!-- Remember Me -->
    <div class="form-group">
        <div class="col-xs-6">
            <div class="checkbox">
                <label class="checkbox-style">
                    <input type="checkbox" name="remember">
                    <i></i> Remember Me
                </label>
            </div>
        </div>
        <div class="col-xs-6">
            <a class="btn btn-link" href="{{ url('/password/reset') }}">Forgot Your Password?</a>
        </div>
    </div>



    <!-- Login Button -->
    <div class="form-group clearfix">
        <div class="col-xs-12 text-center">
            <button type="submit" class="btn btn-blue btn-lg">
                LOG IN
            </button>
        </div>
    </div>
</form>
