@extends('spark::layouts.app')

@section('content')

	<div class="banner-top pull-top" style="background-image:url({!!$content->banner!!})">
	    <div class="banner-mobile visible-xs">
	        <img src="{!!$content->mobileBanner!!}" class="img-responsive">
	    </div>
	    <div class="caption">
	        <h1>Account Registration</h1>
	    </div>
	</div>




	@if (Spark::billsUsingStripe())
	    @include('spark::auth.register-stripe')

	    @section('scripts')
		    <script src="/js/stripe.js"></script>
		@endsection

	@else
		@section('scripts')
		    <script src="/js/braintree.js"></script>
		@endsection

	    @include('spark::auth.register-braintree')
	@endif



	@include('spark::nav.footer')


@endsection

