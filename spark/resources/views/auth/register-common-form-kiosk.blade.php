<form class="form-horizontal" role="form" method="post">
  {{ csrf_field() }}

  <div class="container pad-md">
    {!!$content->body!!}
  </div>

  <div class="clearfix pad-md">
    <div class="container">

      <h4>Personal information</h4>


      {{-- name --}}
      <div class="form-group" :class="{'has-error': registerForm.errors.has('firstname') || registerForm.errors.has('lastname')}">
        <div class="col-sm-4">
          <label>First Name*</label>
          <input type="name" class="form-control" name="firstname" required v-model="registerForm.firstname" autofocus>
          <span class="help-block" v-show="registerForm.errors.has('firstname')">
            @{{ registerForm.errors.get('firstname') }}
          </span>
        </div>
        <div class="col-sm-4">
          <label>Last Name*</label>

          <input type="name" class="form-control" name="lastname" required v-model="registerForm.lastname" autofocus>

          <span class="help-block" v-show="registerForm.errors.has('lastname')">
            @{{ registerForm.errors.get('lastname') }}
          </span>
        </div>
      </div>

      {{-- company --}}
      <div class="form-group" :class="{'has-error': registerForm.errors.has('company') || registerForm.errors.has('title') }">
        <div class="col-sm-4">
          <label for="company">Company</label>
          <input type="text" class="form-control" name="company" v-model="registerForm.company" required>

          <span class="help-block" v-show="registerForm.errors.has('company')">
            @{{ registerForm.errors.get('company') }}
          </span>
        </div>
        <div class="col-sm-4">
          <label>Job Title</label>
          <input type="text" class="form-control" name="title" v-model="registerForm.title" required>

          <span class="help-block" v-show="registerForm.errors.has('title')">
            @{{ registerForm.errors.get('title') }}
          </span>
        </div>
      </div>


      <div class="form-group" :class="{'has-error': registerForm.errors.has('sector')}">
        <div class="col-sm-4">
          <label>Your Sector*</label>
          <select class="form-control" name="sector" v-model="registerForm.sector">
            <option value="" selected disabled hidden></option>
            <option value="Asset management">Asset management</option>
            <option value="Bank">Bank</option>
            <option value="Consulting">Consulting</option>
            <option value="Corporate">Corporate</option>
            <option value="Family office">Family office</option>
            <option value="Insurance company">Insurance company</option>
            <option value="Pension fund">Pension fund</option>
            <option value="Private bank">Private bank</option>
            <option value="Sovereign wealth fund">Sovereign wealth fund</option>
            <option value="Other">Other</option>
          </select>
          <span class="help-block" v-show="registerForm.errors.has('sector')">
            @{{ registerForm.errors.get('sector') }}
          </span>
        </div>
      </div>
    </div>

  </div>

  <div class="clearfix bg-gray pad-md">
    <div class="container">
      <h4>Contact Information & Login</h4>

      <!-- E-Mail Address -->
      <div class="form-group" :class="{'has-error': registerForm.errors.has('email') || registerForm.errors.has('secondary_email')}">

        <div class="col-sm-4">
          <label>Work Email*</label>
          <input type="email" class="form-control" name="email" v-model="registerForm.email">
          <span class="help-block" v-show="registerForm.errors.has('email')">
            @{{ registerForm.errors.get('email') }}
          </span>
        </div>
        <div class="col-sm-4">
          <label>Supplementary Email</label>
          <input type="text" class="form-control" placeholder="Optional" name="secondary_email" v-model="registerForm.secondary_email">
        </div>
      </div>

      <!-- <div class="form-group" :class="{'has-error': registerForm.errors.has('password')}">
               <div class="col-sm-4">
                    <label>Password*</label>
                    <input type="password" class="form-control" placeholder="Not in DB yet">
                </div>
                <div class="col-sm-4">
                    <label>Confirm Password*</label>
                    <input type="password" class="form-control" placeholder="Not in DB yet">
                </div>
            </div> -->

      <div class="form-group" :class="{'has-error': registerForm.errors.has('telephone') || registerForm.errors.has('country') }">
        <div class="col-sm-4">
          <label>Telephone*</label>
          <input type="text" class="form-control" name="telephone" v-model="registerForm.telephone">
          <span class="help-block" v-show="registerForm.errors.has('telephone')">
            @{{ registerForm.errors.get('telephone') }}
          </span>
        </div>
        <div class="col-sm-4">
          <label>Country*</label>
          <?php
												$topCountries = [
													'AU' => 'Australia',
													'AT' => 'Austria',
													'BE' => 'Belgium',
													'CA' => 'Canada',
													'CN' => 'China',
													'DK' => 'Denmark',
													'FI' => 'Finland',
													'FR' => 'France',
													'DE' => 'Germany',
													'HK' => 'Hong Kong',
													'IT' => 'Italy',
													'JP' => 'Japan',
													'KR' => 'Korea, Republic of',
													'LU' => 'Luxembourg',
													'NL' => 'Netherlands',
													'NZ' => 'New Zealand',
													'NO' => 'Norway',
													'PT' => 'Portugal',
													'SG' => 'Singapore',
													'ES' => 'Spain',
													'SE' => 'Sweden',
													'CH' => 'Switzerland',
													'TW' => 'Taiwan',
													'GB' => 'United Kingdom',
													'US' => 'United States',
												];
										?>
            <select class="form-control" name="country" v-model="registerForm.country">
              <option value="" selected disabled hidden></option>
              <optgroup label="Frequent">
                @foreach ($topCountries as $key => $country)
                <option value="{{ $key }}">{{ $country }}</option>
                @endforeach
              </optgroup>
              <option disabled>&nbsp;</option>
              <optgroup label="All">
                @foreach (app(Laravel\Spark\Repositories\Geography\CountryRepository::class)->all() as $key => $country)
                <option value="{{ $key }}">{{ $country }}</option>
                @endforeach
              </optgroup>
            </select>
            <span class="help-block" v-show="registerForm.errors.has('country')">
              @{{ registerForm.errors.get('country') }}
            </span>
        </div>
      </div>

      <!-- Team Name -->
      @if (Spark::usesTeams())
      <div class="form-group hidden" :class="{'has-error': registerForm.errors.has('team')}" v-if=" ! invitation">
        <div class="col-sm-4">
          <label>Team Name</label>
          <input type="text" class="form-control" name="team" v-model="registerForm.team">
          <span class="help-block" v-show="registerForm.errors.has('team')">
            @{{ registerForm.errors.get('team') }}
          </span>
        </div>
      </div>
      @endif

    </div>
  </div>



  <div class="container pad-md">
    <!-- Terms And Conditions -->
    <div v-if=" ! selectedPlan || selectedPlan.price == 0">


      <div class="form-group hidden" :class="{'has-error': registerForm.errors.has('terms')}">
        <div class="col-md-12">

          <div class="checkbox">
            <label class="checkbox-style">
              <input type="checkbox" name="terms" v-model="registerForm.terms" required>
              <i></i>
              I have read and agree to the general
              <a href="/terms" target="_blank" rel="noopener noreferrer">Terms and conditions</a> and
              <a href="/privacy" target="_blank" rel="noopener noreferrer">Privacy policy</a> and I expressly acknowledge and confirm that I am a Professional Investor as defined herein.
            </label>
          </div>

        </div>
        <span class="help-block" v-show="registerForm.errors.has('terms')">
          @{{ registerForm.errors.get('terms') }}
        </span>
      </div>
    </div>
  </div>

  <div class="container">
    <div class="form-group pad-top">
      <div class="row">
        <div class="col-md-2 col-md-offset-3">
          <button class="btn btn-blue btn-lg form-control" @click.prevent="register" :disabled="registerForm.busy">
            <span v-if="registerForm.busy">
              <i class="fa fa-btn fa-spinner fa-spin"></i>Requesting Access
            </span>

            <span v-else>
              Request Access
            </span>
          </button>
        </div>
      </div>
    </div>
  </div>


</form>
