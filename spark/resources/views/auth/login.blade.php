@extends('spark::layouts.app')

@section('content')
<div class="banner-top pull-top" style="background-image:url({!!$content->banner!!})">
    <div class="banner-mobile visible-xs">
        <img src="{!!$content->mobileBanner!!}" class="img-responsive">
    </div>
    <div class="caption">
        <h1>Log in to engagingenterprises</h1>
    </div>
</div>
<div class="container">
    <div class="row pad-md">
        <div class="col-md-8 col-md-offset-2">
            {!!$content->body!!}
        </div>
    </div>
</div>

<div class="clearfix bg-gray marketing">
    <div class="container">
        <div class="row pad-md">
            <div class="col-md-8 col-md-offset-2">
                {{-- @include('spark::shared.errors')

                <form class="form-horizontal" role="form" method="POST" action="/login">
                    {{ csrf_field() }}

                    <!-- E-Mail Address -->
                    <div class="form-group">
                        <label class="col-md-4 control-label">E-Mail Address</label>

                        <div class="col-md-6">
                            <input type="email" class="form-control" name="email" value="{{ old('email') }}" autofocus>
                        </div>
                    </div>

                    <!-- Password -->
                    <div class="form-group">
                        <label class="col-md-4 control-label">Password</label>

                        <div class="col-md-6">
                            <input type="password" class="form-control" name="password">
                        </div>
                    </div>

                    <!-- Remember Me -->
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="remember">
                                    Remember Me
                                </label>
                            </div>
                        </div>
                    </div>

                    <!-- Login Button -->
                    <div class="form-group">
                        <div class="col-md-8 col-md-offset-4">
                            <button type="submit" class="btn btn-primary">
                                <i class="fa m-r-xs fa-sign-in"></i>Login
                            </button>

                            <a class="btn btn-link" href="{{ url('/password/reset') }}">Forgot Your Password?</a>
                        </div>
                    </div>
                </form> --}}
                @include('spark::auth.login-form')

            </div>
        </div>
    </div>
</div>


    @include('spark::nav.footer')


@endsection
