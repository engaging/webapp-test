

module.exports = {
    props: ['user'],


    /**
     * Load mixins for the component.
     */
    mixins: [require('./../mixins/tab-state')],


    /**
     * Prepare the component.
     */
    mounted() {
        this.usePushStateForTabs('.spark-settings-tabs');
    },


    created() {
        /**
         * Handle the Spark tab changed event.
         */
        window.eventBus.$on('sparkHashChanged', this.sparkHashChangedHandler)
    },

    methods: {
      sparkHashChangedHandler(hash) {
        if (hash == 'users') {
            setTimeout(() => {
                $('#kiosk-users-search').focus();
            }, 150);
        }

        return true;
      }

    }
};
