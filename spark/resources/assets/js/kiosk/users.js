const ACTION = require('../../../../../resources/assets/js/store/mutation-type');
const apiPortfolios = require('../../../../../resources/assets/js/api/portfolios');
const { models } = require('../../../../../resources/assets/js/util/config');
const { filterByKeyword } = require('../../../../../resources/assets/js/util/user');
const { iterateInsensitiveFactory } = require('../../../../../resources/assets/js/lib/lodash-helper');

module.exports = {
    props: ['user'],

    /**
     * The component's data.
     */
    data() {
      return {
        models,
        plans: [],
        portfolios: [],

        searchForm: new SparkForm({
          keyword: ''
        }),

        searching: false,
        searchResults: [],
        users: [],

        showing: 'search', // 'search', 'profile', 'register'
        sort: 'name',
        dir: 'asc',

        modalUser: {},
      };
    },

    /**
     * The component has been created by Vue.
     */
    created() {
      this.getPlans();
      this.getPortfolios();

      /**
       * Show the search results and hide the user profile.
       */
      window.eventBus.$on('showSearch', this.showSearchHandler)

        /**
       * Handle the Spark tab changed event.
       */
      window.eventBus.$on('sparkHashChanged', this.sparkHashChangedHandler)
    },

    watch: {
      users() {
        this.searchUsers();
      },
      'searchForm.keyword'() {
        this.searchUsers();
      },
    },

    methods: {
        showAlert(type, message) {
          return this.$store.dispatch(ACTION.SHOW_ALERT_MODAL, { type, message });
        },

        showSearchHandler(user) {
          if (user && !(user instanceof Event)) {
            const userIdx = this.users.findIndex(o => o.id === user.id);
            if (userIdx > -1) { // existing user
              if (user.deleted_at) {
                this.users.splice(userIdx); // delete existing user
              } else {
                this.users.splice(userIdx, 1, user); // replace existing user
              }
            } else { // new user
              this.users.push(user); // add new user
            }
          }
          this.navigateToSearch();
        },

        sparkHashChangedHandler(hash, parameters) {
          if (hash !== 'users') {
            return true;
          }

          if (parameters && parameters.length > 0) {
            this.loadProfile({ id: parameters[0] });
          } else {
            this.showSearch();
          }

          return true;
        },
        /**
         * Get all of the available subscription plans.
         */
        async getPlans() {
          try {
            const { data: plans } = await this.$http.get('/spark/plans') || {};
            this.plans = plans;
          } catch (e) {
            console.error(e);
            this.showAlert('danger', 'Failed to load plans.');
          }
        },

        async getPortfolios() {
          try {
            const { data: portfolios } = await apiPortfolios.loadPortfolios() || {};
            this.portfolios = portfolios;
          } catch (e) {
            console.error(e);
            this.showAlert('danger', 'Failed to load your portfolios.');
          }
        },

        dateFormat(d){
          return moment.unix(d).fromNow(true) + ' ago';
        },


        /**
         * Get all users.
         */
        async getUsers() {
          this.searching = true;
          try {
            const { data: users } = await this.$http.post('/spark/kiosk/users/search') || {};
            this.users = users;
          } catch (e) {
            console.error(e);
            this.showAlert('danger', 'Failed to load users.');
          }
          this.searching = false;
        },

        exportUsers(to = 'xls') {
          this.searching = false;
          this.searchForm.sort = this.sort;
          this.searchForm.dir = this.dir;
          this.searchForm.exportFormat = to;

          let sortQueryString;
          if (Array.isArray(this.sort)) {
            sortQueryString = this.sort.map(sort => `sort[]=${sort}`).join('&');
          } else {
            sortQueryString = `sort=${this.sort}`;
          }

          const data = JSON.stringify(this.searchForm);
          window.location.href = `/spark/kiosk/users/search?keyword=${this.searchForm.keyword}&${
            sortQueryString}&dir=${this.searchForm.dir}&exportFormat=${this.searchForm.exportFormat}`;
        },

        searchUsers() {
          let users = this.users;

          if (this.searchForm.keyword) {
            users = filterByKeyword(this.searchForm.keyword, users);
          }

          let iterate;
          if (Array.isArray(this.sort)) {
            iterate = this.sort.map(iterateInsensitiveFactory);
          } else if (this.sort !== 'portfoliosCount') {
            iterate = iterateInsensitiveFactory(this.sort);
          } else {
            iterate = this.sort;
          }
          this.searchResults = _.orderBy(users, iterate, this.dir);
        },

        toggleSort(field) {
          if (_.isEqual(this.sort, field)){
            if (this.dir === 'asc'){
              this.dir = 'desc';
            } else {
              this.dir = 'asc';
            }
          } else {
            this.sort = field;
            this.dir = 'asc';
          }
          this.searchUsers();
        },

        sendApproval(user){
          swal(
            {
              title: `Are you sure to send approval to ${user.fullname}?`,
              text: 'This action cannot be undone.',
              type: 'warning',
              showCancelButton: true,
              confirmButtonClass: 'btn-danger',
              confirmButtonText: 'Yes, send it',
            },
            async (isConfirm) => {
              if (isConfirm) {
                try {
                  await this.$http.get(`api/admin/users/${user.id}/send-approval`);
                  this.showAlert('success', `Successfully sent approval to ${user.fullname}.`);
                  await this.getUsers().catch(console.error);
                } catch (e) {
                  console.error(e);
                  this.showAlert('danger', `Failed to send approval to ${user.fullname}.`);
                }
              }
            },
          );
        },

        /**
         * Show the search results and update the browser history.
         */
        navigateToSearch() {
          history.pushState(null, null, '#/users');

          this.showSearch();
        },


        /**
         * Show the search results.
         */
        showSearch() {
          this.showing = 'search';

          Vue.nextTick(function () {
              $('#kiosk-users-search').focus();
          });
        },


        /**
         * Show the user profile for the given user.
         */
        showUserProfile(user) {
          history.pushState(null, null, `#/users/${user.id}`);

          this.loadProfile(user);
        },


        /**
         * Load the user profile for the given user.
         */
        loadProfile(user) {
          window.eventBus.$emit('showUserProfile', user.id);

          this.showing = 'profile';
        },

        /**
         * Impersonate the given user.
         */
        impersonate(user) {
          window.location = `/spark/kiosk/users/impersonate/${user.id}`;
        },

        /**
         * Show the search results.
         */
        showRegister() {
          this.showing = 'register';
        },

        async toggleBlocked(user){
          try {
            const { data } = await this.$http.get(`/spark/kiosk/users/${user.id}/toggleblocked`);
            user.blocked = data.user.blocked;
            this.showAlert('success', `Successfully ${user.blocked ? 'blocked' : 'unblocked' } ${user.fullname}.`);
          } catch(e) {
            console.error(e);
            this.showAlert('danger', `Failed to ${user.blocked ? 'unblock' : 'block' } ${user.fullname}.`);
          }
        },

        async removeConfirm(user){
          swal(
            {
              title: `Are you sure to delete ${user.fullname}?`,
              text: 'This action cannot be undone.',
              type: 'warning',
              showCancelButton: true,
              confirmButtonClass: 'btn-danger',
              confirmButtonText: 'Yes, delete it',
            },
            async (isConfirm) => {
              if (isConfirm) {
                try {
                  await this.$http.get(`/spark/kiosk/users/${user.id}/remove`);
                  this.searchResults.splice(this.searchResults.indexOf(user));
                  this.showAlert('success', `Successfully deleted ${user.fullname}.`);
                } catch (e) {
                  console.error(e);
                  this.showAlert('danger', `Failed to delete ${user.fullname}.`);
                }
              }
            },
          );
        },

        async createInvestorRight(user) {
          try {
            await this.$http.post(`/api/admin/users/${user.id}/right`);
            this.showAlert('success', `Investor right successfully created for ${user.fullname}.`);
          } catch(e) {
            if (e.status === 409) {
              this.showAlert('warning', `Right already exists for ${user.fullname
                }. Contact support to amend right.`);
            } else {
              console.error(e);
              this.showAlert('danger', `Failed to create Investor right for ${user.fullname}.`);
            }
          }
        },

        async subscribePlan(user, plan) {
          try {
            const { data: subscriptions } = await this.$http.post(`/api/admin/users/${user.id}/subscription`, {
              plan: plan.id,
            });
            user.subscriptions = subscriptions;
            this.showAlert('success', `${plan.name} plan successfully subscribed for ${user.fullname}.`);
          } catch(e) {
            console.error(e);
            this.showAlert('danger', `Failed to subscribe ${plan.name} plan for ${user.fullname}.`);
          }
        },

        async changePlan(user, plan) {
          try {
            const { data: subscriptions } = await this.$http.put(`/api/admin/users/${user.id}/subscription`, {
              plan: plan.id,
            });
            user.subscriptions = subscriptions;
            this.showAlert('success', `Subscription successfully changed to ${plan.name} plan for ${user.fullname}.`);
          } catch(e) {
            console.error(e);
            this.showAlert('danger', `Failed to change subscription to ${plan.name} plan for ${user.fullname}.`);
          }
        },

        async cancelPlan(user, plan) {
          try {
            const { data: subscriptions } = await this.$http.delete(`/api/admin/users/${user.id}/subscription`);
            user.subscriptions = subscriptions;
            this.showAlert('success', `${plan.name} plan successfully canceled for ${user.fullname}.`);
          } catch(e) {
            console.error(e);
            this.showAlert('danger', `Failed to cancel ${plan.name} plan for ${user.fullname}.`);
          }
        },

        checkPlanId(searchUser, plan) {
          return searchUser.subscriptions.some(o => o.stripe_plan === plan.id);
        },

        showCopyPortfolioModal(user) {
          this.modalUser = user;
          $('#modal-copy-portfolio').modal('show');
        },

        async copyPortfolioToUser(user, portfolio) {
          try {
            await this.$http.get(`/api/admin/users/${user.id}/portfolios/${portfolio.id}/copy`);
            this.showAlert('success', `${portfolio.title} portfolio successfully copied to ${user.fullname} account.`);
            $('#modal-copy-portfolio').modal('hide');
          } catch(e) {
            console.error(e);
            this.showAlert('danger', `Failed to copy ${portfolio.title} portfolio to ${user.fullname} account.`);
          }
        },
    },
};
