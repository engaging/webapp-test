const ACTION = require('../../../../../resources/assets/js/store/mutation-type');
const { basePrefs } = require('../../../../../resources/assets/js/util/config');

const COLOR_KEYS = [
  'navbarColor',
  'navbarTextColor',
  'navbarLineColor',
  'headerColor',
  'headerLineColor',
];

module.exports = {
    props: ['user', 'plans'],

    /**
     * The component's data.
     */
    data() {
      return {
        loading: false,
        profile: null,
        revenue: 0,
        image: '',
        mode: 'view',
      };
    },


    /**
     * Prepare the component.
     */
    mounted() {
      Mousetrap.bind('esc', e => this.showSearch());
    },


    created() {
      /**
       * Watch the current profile user for changes.
       */
      window.eventBus.$on('showUserProfile', this.showUserProfileHandler)
    },

    watch: {
      profile() {
        if (!this.profile) return;
        COLOR_KEYS.forEach(key => {
          if (_.isEmpty(this.profile[key])) {
            this.profile[key] = basePrefs[key];
          }
        });
      },
    },

    methods: {
        showAlert(type, message) {
          return this.$store.dispatch(ACTION.SHOW_ALERT_MODAL, { type, message });
        },

        showUserProfileHandler(id) {
          this.getUserProfile(id);
        },

        /**
         * Get the profile user.
         */
        async getUserProfile(id) {
          this.loading = true;
          try {
            const { data } = await this.$http.get(`/spark/kiosk/users/${id}/profile`) || {};
            this.profile = data.user;
            this.revenue = data.revenue;
            this.mode = 'view';
          } catch (e) {
            console.error(e);
            this.showAlert('danger', 'Failed to load user profile.');
          }
          this.loading = false;
        },

        /**
         * Impersonate the given user.
         */
        impersonate(user) {
          window.location = `/spark/kiosk/users/impersonate/${user.id}`;
        },

        onFileChange(e) {
          var files = e.target.files || e.dataTransfer.files;
          if (!files.length) return;
          this.createImage(files[0]);
        },
        createImage(file) {
          var image = new Image();
          var reader = new FileReader();
          var vm = this;

          reader.onload = (e) => {
            vm.image = e.target.result;
          };
          reader.readAsDataURL(file);
        },
        removeImage(e) {
          this.image = '';
        },
        async revertWhitelabel(){
          this.removeImage();
          this.loading = true;
          try {
            const { data } = await this.$http.put(`/spark/kiosk/users/${this.profile.id}/profile`, {
              revertWhitelabel: true,
            }) || {};
            this.profile = data.user;
            this.revenue = data.revenue;
            this.showAlert('success', 'Successfully reverted white labelling.');
          } catch (e) {
            console.error(e);
            this.showAlert('danger', 'Failed to revert white labelling.');
          }
          this.loading = false;
        },
        async saveUser(){
          const profile = Object.assign({}, this.profile);
          if (this.image && this.image.length) {
            profile.base64_image = this.image;
            this.removeImage();
          }

          COLOR_KEYS.forEach(key => {
            if (profile[key] === basePrefs[key]) {
              profile[key] = '';
            }
          });

          this.loading = true;
          try {
            const { data } = await this.$http.put(`/spark/kiosk/users/${this.profile.id}/profile`, profile) || {};
            this.profile = data.user;
            this.revenue = data.revenue;
            this.mode = 'view';
            this.showAlert('success', 'Successfully saved user profile.');
          } catch (e) {
            console.error(e);
            this.showAlert('danger', 'Failed to save user profile.');
          }
          this.loading = false;
        },

        async toggleBlocked(user){
          this.loading = true;
          try {
            const { data } = await this.$http.get(`/spark/kiosk/users/${user.id}/toggleblocked`) || {};
            user.blocked = data.user.blocked;
            this.showAlert('success', `Successfully ${user.blocked ? 'blocked' : 'unblocked' } user.`);
          } catch (e) {
            console.error(e);
            this.showAlert('danger', `Failed to ${user.blocked ? 'unblock' : 'block' } user.`);
          }
          this.loading = false;
        },

        removeConfirm(user){
          swal(
            {
              title: `Are you sure to delete ${user.fullname}?`,
              text: 'This action cannot be undone.',
              type: 'warning',
              showCancelButton: true,
              confirmButtonClass: 'btn-danger',
              confirmButtonText: 'Yes, delete it',
            },
            async (isConfirm) => {
              if (isConfirm) {
                this.loading = true;
                try {
                  await this.$http.get(`/spark/kiosk/users/${user.id}/remove`);
                  this.profile.deleted_at = true;
                  this.showSearch();
                  this.showAlert('success', `Successfully deleted ${user.fullname}.`);
                } catch (e) {
                  console.error(e);
                  this.showAlert('danger', 'Failed to delete user.');
                }
                this.loading = false;
              }
            },
          );
        },

        /**
         * Show the discount modal for the given user.
         */
        addDiscount(user) {
          window.eventBus.$emit('addDiscount', user);
        },

        /**
         * Get the plan the user is actively subscribed to.
         */
        activePlan(billable) {
          if (this.activeSubscription(billable)) {
            const activeSubscription = this.activeSubscription(billable);
            return _.find(this.plans, (plan) => plan.id === activeSubscription.provider_plan);
          }
        },

        /**
         * Get the active, valid subscription for the user.
         */
        activeSubscription(billable) {
          const subscription = this.subscription(billable);
          if (!subscription || (subscription.ends_at && moment.utc().isAfter(moment.utc(subscription.ends_at)))) {
            return;
          }
          return subscription;
        },

        /**
         * Get the active subscription instance.
         */
        subscription(billable) {
          if (!billable) return;

          const subscription = _.find(billable.subscriptions, subscription => subscription.name === 'default');
          if (typeof subscription !== 'undefined') {
            return subscription;
          }
        },

        /**
         * Get the customer URL on the billing provider's website.
         */
        customerUrlOnBillingProvider(billable) {
          if (!billable) return;

          if (this.spark.usesStripe) {
            return `https://dashboard.stripe.com/customers/${billable.stripe_id}`;
          } else {
            const domain = (Spark.env !== 'production') ? 'sandbox.' : '';
            return `https://${domain}braintreegateway.com/merchants/${Spark.braintreeMerchantId}/customers/${billable.braintree_id}`;
          }
        },

        /**
         * Show the search results and hide the user profile.
         */
        showSearch() {
          window.eventBus.$emit('showSearch', this.profile);
          this.profile = null;
        }
    }
};
