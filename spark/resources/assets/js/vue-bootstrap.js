/*
 * Load Vue & Vue-Resource.
 *
 * Vue is the JavaScript framework used by Spark.
 */
if (window.Vue === undefined) {
    window.Vue = require('vue');
    window.Vue.config.devtools = window.env.APP_DEBUG;
    window.eventBus = new Vue();
}

const VueResource = require('vue-resource');
Vue.use(VueResource);

// /**
//  * Load Vue HTTP Interceptors.
//  */
const { addHeaders } = require('./interceptors')
Vue.http.interceptors.push((request, next) => {
  addHeaders(request, next);
});

/**
 * Load Vuency
 */
const Vuency = require('vuency');
Vue.use(Vuency);

/**
 * Load Vue Global Mixin.
 */
Vue.mixin(require('./mixin'));

/**
 * Define the Vue filters.
 */
require('./filters');

/**
 * Load the Spark form utilities.
 */
require('./forms/bootstrap');
