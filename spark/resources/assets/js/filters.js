
/**
 * Format the given date.
 */
Vue.filter('date', (value) => {
  return moment.utc(value).local().format('MMMM Do, YYYY');
});

Vue.filter('toDate', (value) => {
  const d = moment(value, 'YYYY-MM-DD HH:mm:ss');
  return d.format('YYYY-MM-DD');
});

Vue.filter('concat', (...values) => {
  return values.filter(_.isString).join('');
});

Vue.filter('showAsRange', (value, decimals) => {
  const values = value.split(',');
  return parseFloat(values[0]).toFixed(decimals)
    + ' - ' + parseFloat(values[1]).toFixed(decimals);
});

Vue.filter('thousand_comma', (value) => {
  const str = String(value);
  return str.replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
});

Vue.filter('round_4dp', (value) => {
  value = parseFloat(value);
  return _.isFinite(value) ? value.toFixed(2) : '-';
});

Vue.filter('unit', (value, unit = '') => {
  return _.isFinite(parseFloat(value)) ? (value + unit) : value;
});

/**
 * Format the given date as a timestamp.
 */
Vue.filter('datetime', (value) => {
  return moment.utc(value).local().format('MMMM Do, YYYY h:mm A');
});

/**
 * Format the given date into a relative time.
 */
Vue.filter('relative', (value) => {
  moment.locale('en', {
    relativeTime : {
      future: "in %s",
      past:   "%s",
      s:  "1s",
      m:  "m",
      mm: "%dm",
      h:  "1h",
      hh: "%dh",
      d:  "1d",
      dd: "%dd",
      M:  "1m",
      MM: "%dm",
      y:  "1y",
      yy: "%dy",
    },
  });
  return moment.utc(value).local().fromNow();
});

/**
 * Vue1 built-in filters (for legacy).
 */

 /**
 * Filter filter for arrays
 *
 * @param {String|Array<String>|Function} ...sortKeys
 * @param {Number} [order]
 */
  // Vue.filter('orderBy', (arr, sortKeys, order) => _.orderBy(arr, sortKeys, (order < 0) ? 'desc' : 'asc'));

  /**
   * 'abc' => 'Abc'
   */
  Vue.filter('capitalize', (value) => {
    return value[0].toUpperCase() + value.slice(1)
  });

  /**
   * 12345 => $12,345.00
   *
   * @param {String} sign
   * @param {Number} decimals Decimal places
   */
  Vue.filter('currency', (value, currency, decimals) => {

    const digitsRE = /(\d{3})(?=\d)/g
    value = parseFloat(value)
    if (!isFinite(value) || (!value && value !== 0)) return ''
    currency = currency != null ? currency : '$'
    decimals = decimals != null ? decimals : 2
    var stringified = Math.abs(value).toFixed(decimals)
    var _int = decimals
      ? stringified.slice(0, -1 - decimals)
      : stringified
    var i = _int.length % 3
    var head = i > 0
      ? (_int.slice(0, i) + (_int.length > 3 ? ',' : ''))
      : ''
    var _float = decimals
      ? stringified.slice(-1 - decimals)
      : ''
    var sign = value < 0 ? '-' : ''
    return sign + currency + head +
      _int.slice(i).replace(digitsRE, '$1,') +
      _float
  });