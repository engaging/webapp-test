/*
 * Load various JavaScript modules that assist Spark.
 */
window.URI = require('urijs');
window._ = require('lodash');
window.moment = require('moment');
window.Cookies = require('js-cookie');
window.Promise = require('../../../../resources/assets/js/lib/bluebird-extended');

/*
 * Alias removed lodash functions for legacy code.
 */
_.pluck = _.map;
_.contains = _.includes;

/*
 * Enable bluebird warnings in DEBUG mode.
 */
Promise.config({
  warnings: env.APP_DEBUG,
});

/*
 * Load jQuery and Bootstrap jQuery, used for front-end interaction.
 */
if (window.$ === undefined || window.jQuery === undefined) {
    window.$ = window.jQuery = require('jquery');
}

/*
 * Placeholder for charts.
 */
window.charts = {};

require('bootstrap/dist/js/npm');

/**
 * Load Vue if this application is using Vue as its framework.
 */
if ($('#spark-app').length > 0) {
    require('./vue-bootstrap');
}
