

module.exports = {
    props: [
        'user', 'teams', 'currentTeam',
        'hasUnreadNotifications', 'hasUnreadAnnouncements'
    ],


    methods: {
         /**
          * Show the user's notifications.
          */
         showNotifications() {
          window.eventBus.$emit('showNotifications');
        },


        /**
         * Show the customer support e-mail form.
         */
        showSupportForm() {
          window.eventBus.$emit('showSupportForm');
        }
    }
};
