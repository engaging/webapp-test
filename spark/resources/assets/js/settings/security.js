

module.exports = {
    props: ['user'],


    /**
     * The component's data.
     */
    data() {
        return {
            twoFactorResetCode: null
        };
    },


    created() {
        /**
         * Display the received two-factor authentication code.
         */
        window.eventBus.$on('receivedTwoFactorResetCode', this.receivedTwoFactorResetCodeHandler);
    },
    methods: {
      receivedTwoFactorResetCodeHandler(code) {
        this.twoFactorResetCode = code;

        $('#modal-show-two-factor-reset-code').modal('show');
      }
    }
};
