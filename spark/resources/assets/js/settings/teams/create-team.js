

module.exports = {
    /**
     * The component's data.
     */
    data() {
        return {
            form: new SparkForm({
                name: ''
            })
        };
    },


    created() {
        /**
         * Handle the "activatedTab" event.
         */
        window.eventBus.$on('activatedTab', this.activatedTabHandler);
    },


    methods: {
      activatedTabHandler(tab) {
        if (tab === 'teams') {
            Vue.nextTick(() => {
                $('#create-team-name').focus();
            });
        }

        return true;
      }, 
        /**
         * Create a new team.
         */
        create() {
            Spark.post('/settings/teams', this.form)
                .then(() => {
                    this.form.name = '';

                    window.eventBus.$emit('updateUser');
                    window.eventBus.$emit('updateTeams');
                });
        }
    }
};
