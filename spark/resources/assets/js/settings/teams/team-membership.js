

module.exports = {
    props: ['user', 'team'],

    /**
     * The component's data.
     */
    data() {
        return {
            invitations: []
        };
    },


    /**
     * The component has been created by Vue.
     */
    created() {
        this.getInvitations();
        /**
         * Update the team's invitations.
         */
        window.eventBus.$on('updateInvitations', this.updateInvitationsHandler);
    },


    methods: {
        updateInvitationsHandler() {
            this.getInvitations();
        },
        /**
         * Get all of the invitations for the team.
         */
        getInvitations() {
            this.$http.get(`/settings/teams/${this.team.id}/invitations`)
                .then(response => {
                    this.invitations = response.data;
                });
        }
    }
};
