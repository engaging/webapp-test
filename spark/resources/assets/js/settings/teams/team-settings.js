module.exports = {
    props: ['user', 'teamId'],


    /**
     * Load mixins for the component.
     */
    mixins: [require('./../../mixins/tab-state')],


    /**
     * The component's data.
     */
    data() {
        return {
            billableType: 'team',
            team: null
        };
    },


    /**
     * The component has been created by Vue.
     */
    created() {
        this.getTeam();
    },


    /**
     * Prepare the component.
     */
    mounted() {
        this.usePushStateForTabs('.spark-settings-tabs');
        /**
         * Update the team being managed.
         */
        window.eventBus.$on('updateTeam', this.updateTeamHandler);
    },

    methods: {
        updateTeamHandler() {
            this.getTeam();
        },
        /**
         * Get the team being managed.
         */
        getTeam() {
            this.$http.get(`/teams/${this.teamId}`)
                .then(response => {
                    this.team = response.data;
                });
        }
    }
};
