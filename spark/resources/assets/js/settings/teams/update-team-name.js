

module.exports = {
    props: ['user', 'team'],

    /**
     * The component's data.
     */
    data() {
        return {
            form: new SparkForm({
                name: ''
            })
        };
    },


    /**
     * Prepare the component.
     */
    mounted() {
        this.form.name = this.team.name;
    },


    methods: {
        /**
         * Update the team name.
         */
        update() {
            Spark.put(`/settings/teams/${this.team.id}/name`, this.form)
                .then(() => {
                    window.eventBus.$emit('updateTeam');
                    window.eventBus.$emit('updateTeams');
                });
        }
    }
};
