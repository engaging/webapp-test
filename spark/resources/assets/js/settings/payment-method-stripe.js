

module.exports = {
    props: ['user', 'team', 'billableType'],


    /**
     * Load mixins for the component.
     */
    mixins: [
        require('./../mixins/discounts')
    ],


    /**
     * The componetn's data.
     */
    data() {
        return {
            currentDiscount: null,
            loadingCurrentDiscount: false
        };
    },


    /**
     * Prepare the component.
     */
    mounted() {
        this.getCurrentDiscountForBillable(this.billableType, this.billable);
    },


    created() {
        /**
         * Update the discount for the current user.
         */
        window.eventBus.$on('updateDiscount', this.updateDiscountHandler);
    },

    methods: {
      updateDiscountHandler() {
        this.getCurrentDiscountForBillable(this.billableType, this.billable);

        return true;
      }
    }
};
