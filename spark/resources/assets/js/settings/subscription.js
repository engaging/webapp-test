

module.exports = {
    props: ['user', 'team', 'billableType'],

    /**
     * Load mixins for the component.
     */
    mixins: [
        require('./../mixins/plans'),
        require('./../mixins/subscriptions')
    ],


    /**
     * The component's data.
     */
    data() {
        return {
            plans: []
        };
    },


    /**
     * Prepare the component.
     */
    mounted() {
        this.getPlans();
    },


    created() {
        /**
         * Show the details for the given plan.
         */
        window.eventBus.$on('showPlanDetails', this.showPlanDetailsHandler);
    },


    methods: {
        showPlanDetailsHandler(plan) {
            this.showPlanDetails(plan);
        },
        /**
         * Get the active plans for the application.
         */
        getPlans() {
            this.$http.get(this.urlForPlans)
                .then(response => {
                    this.plans = response.data;
                });
        }
    },


    computed: {
        /**
         * Get the URL for retrieving the application's plans.
         */
        urlForPlans() {
            return this.billingUser ? '/spark/plans' : '/spark/team-plans';
        }
    }
};
