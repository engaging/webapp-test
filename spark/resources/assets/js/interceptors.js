const addHeaders = (request, next) => {
  if (request.url.startsWith(env.API_BASE_URL)) {
    const token = env.API_DEV_KEY || `${Cookies.get('spark_token')}.${Cookies.get('XSRF-TOKEN')}`;
    request.headers.set('Authorization', `Bearer ${token}`);
  } else if (env.API_TOKEN) {
    request.headers.set('Authorization', `Bearer ${env.API_TOKEN}`);
  } else {
    request.headers.set('X-XSRF-TOKEN', Cookies.get('XSRF-TOKEN'));
  }

  next(res => {
    switch (res.status) {
      case 402:
        window.location = '/settings#/subscription';
        break;
    }
  })
}


module.exports = {

  /**
   * Intercept the outgoing requests.
   *
   * Set common headers on the request.
   */
  addHeaders,
  /**
   * Intercept the incoming responses.
   *
   * Handle any unexpected HTTP errors and pop up modals, etc.
   */
  response(response) {
    switch (response.status) {
      case 4011:
        // Analytics reset on logout
        try {
          window.analytics && window.analytics.reset();
          window.mixpanel && window.mixpanel.reset();
        } catch (e) {
          console.error(e);
        }
        // Logout
        Vue.http.get('/logout');
        $('#modal-session-expired').modal('show');
        break;

      case 402:
        window.location = '/settings#/subscription';
        break;
    }
    return response;
  },

};
