<?php

namespace Laravel\Spark\Http\Controllers;

use Parsedown;
use Laravel\Spark\Spark;
use GuzzleHttp\Client as GuzzleHttpClient;
use GuzzleHttp\Exception\RequestException;

class TermsController extends Controller
{
    /**
     * Show the terms of service for the application.
     *
     * @return Response
     */
    public function show()
    {
        $content = (object) [
          'title' => "Terms and Conditions",
          'body' => "
            <h1>Terms and Conditions</h1>
            <p>
              Engaging (“engagingenterprises”) is providing on this Website (currently located at engagingenterprises.co) electronic access to documents and information regarding financial indices. The Website and any content and services available therein shall be defined as the “Services”. The Services are available to registered users only and the access to the Services is restricted to Professional Investors as defined herein.
            </p>
            <p>
              These Terms and Conditions are a binding agreement between You and engagingenterprises whereby engagingenterprises agrees to provide to the User, and the User agrees to license and use, the Services, in accordance with these Terms and Conditions.
            </p>
            <p>
              By using or accessing the Services, You acknowledge that You have read, understood and agree to abide by the terms contained herein. Your acceptance takes effect immediately upon first registration on the Website. Please note that engagingenterprises has the right to amend and update these Terms and Conditions at any time. engagingenterprises shall notify you of these amendments by email. You acknowledge that your continued use of the Services after any such amendment constitutes acceptance of such revised Terms and Conditions.
            </p>
            <p>
              By using the Services, You acknowledge your agreement with and understanding of the Terms and Conditions. If You do not adhere to these Terms and Conditions for any reason, you acknowledge that your sole and exclusive remedy is to terminate your use of the Services. You understand that You will not use, or allow the use of, the Website in contravention of any laws, regulations or rules of any regulatory authorities to which you, as the user, are subject.
            </p>
            <p>
              Breach of these Terms and Conditions will immediately, without any notice or other action, terminate your right to access the Services and you will be fully liable for any loss, damages, liabilities and other causes arising directly or indirectly from or relating to your breach.
            </p>
            <h4>Professional Investors</h4>
            <p>
              The access to the Services is restricted to persons who are Eligible Counterparties or Professional Clients for the purposes of the rules of the Financial Conduct Authority and who have professional experience in matters relating to complex investments. If you are not eligible to access the Services, please refrain from using the Services.
            </p>
            <h4>US Supplement</h4>
            <p>
              Major U.S. Institutional Investor Certification
            </p>
            <p>
              Access to the Services is only available to \"Major U.S. Institutional Investor\" (MII) as defined in Rule 15a-6(b)(4) of the Securities Exchange Act. By accessing the Services you are deemed to have certified that you meet the definition of a MII as defined.
            </p>
            <h4>Informational Purposes only - No offer or solicitation – No advice</h4>
            <p>
              The Services provided are for information purposes only and do not constitute an offer, recommendation or solicitation for the purchase or sale of any particular financial instrument or any securities.
            </p>
            <p>
              The Services are not intended for distribution to, or use by, any person or entity in any jurisdiction or country where such distribution or use would be contrary to local law.
            </p>
            <p>
              You acknowledge that engagingenterprises does not provide legal advice or tax advice or investment advice nor does engagingenterprises give any investment recommendation or financial analysis.
            </p>
            <h4>Limited license to use</h4>
            <p>
              engagingenterprises hereby grants to you a non-transferable, non-exclusive license to use, for your internal purposes only, the Website and collectively the Services. Unauthorized use of the Website and/or access to the Services included is strictly prohibited.
            </p>
            <p>
              You represent that you shall not distribute or disclose to any other external party, or allow any other external party to copy or use any information contained in or derived from the Website or the Services without engagingenterprises prior written consent.
            </p>
            <h4>Intellectual Property</h4>
            <p>
              Certain data and information accessible on the Website is the intellectual property of the relevant information services provider or Index Sponsors that provide such data to engagingenterprises. The data is protected by copyright and other intellectual laws and all ownership rights remain with the information service provider or Index sponsors or engagingenterprises, as the case may be.
            </p>
            <p>
              You may only use the data and information retrieved from the Website for your own personal purposes while accessing the Services. You shall not copy, distribute or redistribute the data or information retrieved from our Website available in any manner to any third party without engagingenterprises prior written consent.
            </p>
            <h4>Performance and Statistic Information</h4>
            <p>
              The performance and statistic information related to financial indices provided on the Website are for illustrative and information purposes only. They consist of calculations that are derived from proprietary models based upon well-recognized financial principles and from data that has been obtained from sources believed reliable.
            </p>
            <p>
              However engagingenterprises and any external information service provider, disclaim any responsibility for the accuracy of estimates and models used in the calculations, any errors or omissions in computing or disseminating any performance and statistic information.
            </p>
            <p>
              You hall bear all risk from any use of the Services and You are responsible for validating the integrity of any information.
            </p>
            <p>
              The performance and statistic information based on different models or assumptions may affect the calculations and provide different results. Please contact <a href=\"mailto:admin@engagingenterprises.co\">admin@engagingenterprises.co</a> for further details on the models and calculations implemented by engagingenterprises.
            </p>
            <p>
              Any performance or statistic information provided do not take into account commissions, fees or possible taxation and are not related to any financial contract. They do not represent projections or expected performance of any index or financial instrument.
            </p>
            <h4>Accuracy of Information and Limitation of Liability</h4>
            <p>
              engagingenterprises is providing access to documents and information regarding financial Indices. engagingenterprises is not related, nor associated to any of these Indices. Please refer to the corresponding Index Sponsor or Index Calculation Agent for further information.
            </p>
            <p>
              The Services have been produced using information taken from various sources, including external providers, we consider reliable. However, engagingenterprises and any external provider, do not warrant any accuracy, completeness of this information and expressly disclaim liability for errors or omissions in the Services.
            </p>
            <p>
              The Services are provided 'as is' and 'as available'. We are not providing any warranties and representations regarding the Services. Nothing herein shall be interpreted as limiting or reducing engagingenterprises obligations and responsibilities to You in accordance with applicable laws and regulation.
            </p>
            <div>
              engagingenterprises or any external information provider shall not be liable for any for loss or damages, direct or indirect, from:
            </div>
            <ul>
              <li>
                any access of or inability to access the Services;
              </li>
              <li>
                any inaccuracy or incompleteness in, or delays, interruptions, errors or omissions in the delivery of the information or data provided;
              </li>
              <li>
                any decision made or action taken by you or any third party in reliance upon the Services.
              </li>
            </ul>
            <h4>Governing Law</h4>
            <p>
              Your access to and use of the Website, and the Terms and Conditions shall by governed by the laws of Hong Kong Special administrative Region.
            </p>
            <h4>Dow Jones Terms and Conditions</h4>
            <p>
              engagingenterprises, at our discretion, provide you with services including, but not restricted to, news and information services from Dow Jones. You agree to comply with the conditions imposed on your use of the services, as set out in <a href=\"https://global.factiva.com/FactivaLogin/tou/default.aspx?fcpil=en\">Dow Jones Terms and Conditions</a> as amended from time to time.
            </p>
            <h4>FACTSET Terms and Conditions</h4>
            <p>
              FACTSET AND SUPPLIERS DO NOT WARRANT THAT THE PROVISION OF SERVICES WILL BE UNINTERRUPTED, ERROR FREE, TIMELY, COMPLETE OR ACCURATE, NOR DO FACTSET OR SUPPLIERS MAKE ANY WARRANTIES, EXPRESS OR IMPLIED, AS TO THE RESULTS TO BE OBTAINED FROM THE USE OF THE SERVICE AND THERE ARE NO EXPRESS OR IMPLIED WARRANTIES OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE OR USE. YOU ACKNOWLEDGE THAT NOTHING IN THE SERVICE CONSTITUTES INVESTMENT ADVICE OF ANY KIND AND THAT THE SERVICE IS PROVIDED FOR INFORMATIONAL PURPOSES ONLY. YOU EXPRESSLY AGREE THAT YOUR USE OF THE SERVICE IS AT YOUR OWN RISK. ACCORDINGLY, FACTSET AND SUPPLIERS WILL NOT IN ANY WAY BE LIABLE FOR INACCURACIES, ERRORS, OMISSIONS, DELAYS, DAMAGES, CLAIMS, LIABILITIES OR LOSSES, REGARDLESS OF CAUSE, IN OR ARISING FROM THE USE OF THE SERVICE. YOU MAY NOT USE ANY PART OF THE SERVICE (E.G., INDEX VALUES) TO CREATE A FINANCIAL INSTRUMENT OR INDEX, A FINANCIAL INSTRUMENT BASED ON THAT INDEX, OR ANY MUNICIPAL BOND INDEX TO CREATE OR DERIVE PRICES OF BONDS.
            </p>
            <h4>Barclays Terms and Conditions</h4>
            <p>
              THIS WEBSITE CONTAINS INDICATIVE SUMMARIES AND INFORMATION RELATING TO ONE OR MORE BARCLAYS INDICES. INDICES ARE UNMANAGED AND CANNOT BE INVESTED IN DIRECTLY. THE DEVELOPMENT OR CREATION OF ANY TRANSACTION THAT USES, IS BASED ON, OR IS DEVELOPED IN CONNECTION WITH, ANY INDEX IS PROHIBITED WITHOUT THE PRIOR WRITTEN CONSENT OF BARCLAYS. BARCLAYS DOES NOT SPONSOR, ENDORSE, SELL OR PROMOTE ANY TRANSACTION AND MAKES NO REPRESENTATION REGARDING THE ADVISABILITY OF INVESTING IN ANY SUCH TRANSACTION. YOU SHALL NOT ACCESS THE BARCLAYS INDICES WITHOUT HAVING PREVIOUSLY READ AND UNDERSTOOD THE FOLLOWING DISCLAIMERS.
            </p>
            <p>
              This website shall not constitute an offer (or the solicitation of an offer) to enter into any transaction (Transaction) including, without limitation, an underwriting commitment, an offer of financing, an offer to sell, or the solicitation of an offer to buy or invest in an Index through securities or other products which at any time may be related or linked to, based on, developed in connection with, or may otherwise use or track an Index. The conclusion of any Transaction with Barclays is subject to Barclays' internal approvals and formal agreement, none of which is (or shall be deemed to be) given in, or contemplated by, this website.
            </p>
            <p>
              Any Transaction, may involve a high degree of risk, including without limitation market risk and other risks inherent in investing in securities, commodities, currencies, derivatives and other financial instruments. The value of, and income of, any Transaction may decline and loss of the original amount invested can occur. Barclays makes no representation or warranty, express or implied, to any investor in any Index or any Transaction or any member of the public regarding the advisability of investing in any Index or any Transaction or the ability of any Index, to track the performance of any market or underlying assets or data. In determining, composing or calculating an Index, Barclays has no obligation to take into consideration the needs of the purchasers or traders of any Transaction.
            </p>
            <p>
              This website does not (nor does it purport to) disclose all the risks and other significant issues relating to any Index or any Transaction. Barclays does not (and shall not be deemed to) provide, and has not provided, any investment advice or recommendation to you in relation to any Index or any Transaction. You may not rely on any communication (written or oral) from Barclays as investment advice or as a recommendation relating to any Index or any Transaction. Accordingly, Barclays is under no obligation to, and shall not, determine the suitability for you of any Index or any Transaction. You must determine, on your own behalf or through independent professional advice, the merits, terms and conditions and risks of any Index or any Transaction. You must also satisfy yourself that you are capable of assuming, and assume, the risks of any such Transaction. Barclays accepts no liability whatsoever for any losses arising from the use of this website or its content or reliance on the information contained herein, even if Barclays knew of the possibility of those losses.
            </p>
            <p>
              Barclays does not guarantee the accuracy or completeness of information which is contained in this website and which is stated to have been obtained from or is based upon trade and statistical services or other third party sources. Any data on past performance, modelling, scenario analysis or back-testing contained herein is no indication as to future performance. No representation is made as to the reasonableness of the assumptions made within or the accuracy or completeness of any modelling, scenario analysis or backtesting. All opinions and estimates are given as of the date hereof and are subject to change. The value of any investment may fluctuate as a result of market changes. The information in this website is not intended to predict actual results and no assurances are given with respect thereto.
            </p>
            <p>
              No assurances are given with respect to the future performance of any Index and past performance is no indication as to future performance. Past performance may be simulated past performance (including backtesting) which may involve the use of proxy or substitute Index constituents or Index methodology adjustments where necessary. Back-tested performance data benefits from hindsight and knowledge of factors that may have favorably affected the performance and cannot account for all financial risk that may affect the actual performance of an index. It is in Barclays interest to demonstrate favorable pre-inception index performance. The actual performance of an index may vary significantly from pre-inception index performance. You should not rely on hypothetical index performance information for any purpose.
            </p>
            <p>
              None of Barclays, any of its affiliates or subsidiaries nor any of its directors, officers, employees, representatives, delegates or agents shall have any responsibility to any person (whether as a result of negligence or otherwise) for any determination made or anything done (or omitted to be determined or done) in respect of an Index or publication of the levels of the Index and any use to which any person may put the Index or the levels of the Index. In addition, although Barclays reserves the right to make adjustments to correct previously incorrectly published information, including but not limited to the levels of the Index, Barclays is under no obligation to do so and Barclays shall have no liability in respect of any errors or omissions.
            </p>
            <p>
              BARCLAYS DOES NOT GUARANTEE AND SHALL HAVE NO LIABILITY TO THE COUNTERPARTIES, SUBSCRIBERS, PURCHASERS OR TRADERS, AS THE CASE MAY BE, OF ANY TRANSACTION OR TO THIRD PARTIES FOR THE QUALITY, ACCURACY AND/OR COMPLETENESS OF THE INDICES, OR ANY DATA INCLUDED THEREIN OR FOR INTERRUPTIONS IN THE DELIVERY OF THE INDICES. BARCLAYS MAKES NO EXPRESS OR IMPLIED WARRANTIES, AND HEREBY EXPRESSLY DISCLAIMS ALL WARRANTIES OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE OR USE WITH RESPECT TO THE INDICES, OR ANY DATA INCLUDED THEREIN. WITHOUT LIMITING ANY OF THE FOREGOING, IN NO EVENT SHALL BARCLAYS HAVE ANY LIABILITY FOR ANY SPECIAL, PUNITIVE, INDIRECT, OR CONSEQUENTIAL DAMAGES (INCLUDING LOST PROFITS), EVEN IF NOTIFIED OF THE POSSIBILITY OF SUCH DAMAGES SAVE TO THE EXTENT THAT SUCH EXCLUSION OF LIABILITY IS PROHIBITED BY LAW.
            </p>
            <p>
              Barclays does not provide tax advice and nothing contained herein should be construed to be tax advice. Please be advised that any discussion of U.S. tax matters contained herein (including any attachments)  is not intended or written to be used, and cannot be used, by you for the purpose of avoiding U.S. tax-related penalties. You should seek advice based on your particular circumstances from an independent tax advisor.
            </p>
            <p>
              Barclays Bank PLC is registered in England No. 1026167. Registered Office: 1 Churchill Place, London E14 5HP. Copyright Barclays Bank PLC, 2017 (all rights reserved). This website is confidential, and no part of it may be reproduced, distributed or transmitted without the prior written permission of Barclays. Barclays Bank PLC is authorised by the Prudential Regulation Authority and regulated by the Financial Conduct Authority and the Prudential Regulation Authority.
            </p>
            <div>
              BY EXPRESSING YOUR AGREEMENT BELOW YOU MAKE THE FOLLOWING AFFIRMATIONS:
            </div>
            <ul>
              <li>
                I UNDERSTAND THAT (A) THE INFORMATION THAT FOLLOWS IS INTENDED FOR INSTITUTIONAL OR PROFESSIONAL INVESTORS ONLY AND IS ONLY INTENDED TO BE AVAILABLE TO PERSONS THAT ARE INSTITUTIONAL OR PROFESSIONAL INVESTORS, AND (B) THIS WEBSITE IS NOT (AND DOES NOT CONTAIN) AN OFFER OR SOLICITATION OF AN OFFER TO ENTER INTO ANY TRANSACTION RELATING TO AN INDEX.
              </li>
              <li>
                I HAVE READ AND UNDERSTOOD AND I ACCEPT THIS DISCLAIMER AND ACCORDINGLY I CONFIRM THAT:
                <br>
                (1) I AM AN INSTITUTIONAL OR PROFESSIONAL INVESTOR AND (2) I UNDERSTAND THAT BARCLAYS (INCLUDING ITS AFFILIATES) WILL RELY ON THE VERACITY AND ACCURACY OF THESE AFFIRMATIONS.
              </li>
            </ul>
          ",
        ];

        return view('spark::terms')->with([
            'content'=> $content
        ]);
    }
    public function showCookies()
    {
        $content = (object) [
          'title' => "Cookies Policy",
          'body' => "
            <h1>Cookies Policy</h1>
            <p>
              Transparency is important to us, This Cookies Policy describes how we use cookies and other similar technology on our website.
            </p>
            <h4>What are cookies?</h4>
            <p>
              Cookies are small text files that are placed and stored on your device when you visit a website. Cookies are used in order to improve the security of a website and the general user experience. Cookies are either: session specific, which means that they are deleted from your device once the session and browser are closed; or permanent, meaning that they will remain resident on your device until they are removed.
            </p>
            <h4>What cookies do we use on our website?</h4>
            <p>
              We are using both session and permanent cookies that can be grouped according to their functionalities:
            </p>
            <ul>
              <li>
                Preference cookies allow the website to remember a user’s preferences in relation to the website display or functionalities.
              </li>
              <li>
                Process cookies allow the website to function and deliver the services, such as navigation, access to log-in areas.
              </li>
              <li>
                Advertising cookies allow adverts presented on the website or delivered to users to be targeted to a specific population. Our website may also display some advertisement from third party partners of engagingenterprises. These advertisements may deploy their own separate cookies onto your device controlled by such third party. Please refer to the cookies and privacy policy of the third party advertisers.
              </li>
              <li>
                Security cookies allow to authenticate users, prevent fraudulent access and protect user data from unauthorized parties.
              </li>
              <li>
                Performance cookies allow to collect information about how the user engage with the website and its functionalities. This information is normally used on an aggregate or anonymous basis (such that it does not identify any individual users) for various business purposes, where permissible under applicable laws and regulations.
              </li>
            </ul>
            <h4>Your choice with respect to cookies</h4>
            <p>
              Most browsers accept cookies by default. However, if you do not wish to consent to cookies being stored on your device, you can block all or certain cookies by changing your browser settings. If you decide to block cookies you may find an alteration of your browsing experience and may not be able to use all functionalities of the website.
            </p>
            <p>
              If you have any question or specific request, please contact: admin@engagingenterprises.co
            </p>
          ",
        ];

        return view('spark::terms')->with([
            'content'=> $content
        ]);
    }
    public function showPrivacy()
    {
        $content = (object) [
          'title' => "Privacy Policy",
          'body' => "
            <h1><strong>Privacy Policy</strong></h1>
            <p>
              Your privacy is important to us. This Privacy Policy describes how we collect, protect, use, and share your personal information when you visit or use our Website.
            </p>
            <h4>Protection of your information.</h4>
            <p>
              We maintain physical and electronic safeguards that comply with applicable legal standards to secure the confidentiality of your information, including personal information from unauthorized access and use, alteration and destruction.
            </p>
            <h4>Collection and use of your information for appropriate purposes only.</h4>
            <ul>
              <li>
                The types of personal information we collect from you when you register to our website include your name, email address, company name, mailing address, telephone number(s), user name and password. We may also collect payment information if needed.
              </li>
              <li>
                In addition to the personal information, we may collect certain information about your use of our Services, such as the IP address of your device, the type of operating system and browser you use, and information about the site you came from, your usage of our Services. Please refer to our <a href=\"/cookies\">Cookies Policy</a> for more details. We may associate the usage and other information we collect online with personal information to further improve our Services and as permitted or required by law.
              </li>
              <li>
                We may use the information we collect from you to allow us to further evaluate, improve and promote our business; and to comply with applicable laws and regulations. We may also use data that we collect on an aggregate or anonymous basis (such that it does not identify any individual clients) for various business purposes, where permissible under applicable laws and regulations.
              </li>
            </ul>
            <h4>Sharing of personal information with third parties as required or permitted by law.</h4>
            <ul>
              <li>
                We may share information with regulatory and law enforcement authorities if necessary to comply with legal requirements. We may share information with third parties if needed to protect against fraud or to enforce our <a href=\"/terms\">Terms and Conditions</a>.
              </li>
              <li>
                We may enter into agreements with external parties to provide services necessary for our activity. Under these agreements, these companies may receive your information, but they must safeguard this information, and for a very limited purpose.
              </li>
              <li>
                We can share your personal information with a third party upon your prior consent to do so.
              </li>
            </ul>
            <p>
              You can access and amend <em>your information</em> in “My Account” on our Website<em>.</em>
            </p>
            <p>
              If you have any question or specific request, please contact: admin@engagingenterprises.co
            </p>
          ",
        ];

        return view('spark::terms')->with([
            'content'=> $content
        ]);
    }
}
