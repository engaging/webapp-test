<?php

namespace Laravel\Spark\Http\Controllers\Auth;

use GuzzleHttp\Client as GuzzleHttpClient;
use Laravel\Spark\Spark;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Laravel\Spark\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Laravel\Spark\Contracts\Interactions\Settings\Security\VerifyTwoFactorAuthToken as Verify;
use App\Services\Analytics\AnalyticsService;
use Throwable;
use Helper;
use Log;

class LoginController extends Controller
{
    use AuthenticatesUsers, ThrottlesLogins {
        AuthenticatesUsers::login as traitLogin;
    }

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new login controller instance.
     *
     * @return void
     */
    public function __construct(AnalyticsService $analytics)
    {
        $this->middleware('guest')->except('logout');
        $this->analytics = $analytics;
    }

    /**
     * Show the application login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        $content = (object) [
          'body' => "",
          'inactiveUserText' => "
            <h2>Your account is currently inactive.</h2>
            <p>
              Please contact us on <a href=\"mailto:admin@engagingenterprises.co\">admin@engagingenterprises.co</a> for further information.
            </p>
          ",
          'banner' => "/img/design/home_banner.jpg",
          'mobileBanner' => "/img/design/home_banner_short_m.jpg",
        ];
        return view('spark::auth.login')->with([
            'content'=> $content
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function login(Request $request)
    {
        if ($request->has('remember')) {
            $request->session()->put('spark:auth-remember', $request->remember);

            $request->merge(['remember' => '']);
        }
        $result = $this->traitLogin($request);
        if (env('ANALYTICS_KEY') && !$result->getSession()->get('errors')) {
          try {
            $user = Auth::user();
            $traits = [
              'createdAt' => $user->created_at->format('Y-m-d\TH:i:s') . '.000Z',
              'username' => $user->email,
              'lastName' => Helper::capitalize($user->lastname),
              'firstName' => Helper::capitalize($user->firstname),
              'name' => $user->fullname,
              'email' => strtolower($user->email),
              'phone' => $user->telephone,
              'country' => $user->country,
              'title' => Helper::startCase($user->title),
              'company' => Helper::startCase($user->company),
              'industry' => Helper::startCase($user->sector),
              'User Phone' => $user->telephone,
              'User Country Code' => $user->country,
              'User Title' => Helper::startCase($user->title),
              'User Company Name' => Helper::startCase($user->company),
              'User Company Industry' => Helper::startCase($user->sector),
              'User Type' => Helper::startCase($user->user_type),
              'User Plans' => $user->subscriptions ? $user->subscriptions->pluck('name')->toArray() : [],
              'User Portfolios Count' => $user->portfoliosCount ?: 0,
            ];
            if ($communityMember = $user->communityMember) {
              $traits['website'] = $communityMember->website;
              $traits['User Community Type'] = Helper::startCase($communityMember->type);
              $traits['User Community Name'] = Helper::startCase($communityMember->name);
              $traits['User Website'] = $communityMember->website;
            }
            $context = [
              'ip' => $request->ip(),
              'userAgent' => $request->header('User-Agent'),
            ];
            $this->analytics->identify([
              'userId' => $user->id,
              'traits' => $traits,
              'context' => $context,
            ]);
          } catch(Throwable $e) {
            Log::error($e);
          }
        }
        return $result;
    }

    /**
     * Handle a successful authentication attempt.
     *
     * @param  Request  $request
     * @param  \Illuminate\Contracts\Auth\Authenticatable  $user
     * @return Response
     */
    public function authenticated(Request $request, $user)
    {
        if (Spark::usesTwoFactorAuth() && $user->uses_two_factor_auth) {
            return $this->redirectForTwoFactorAuth($request, $user);
        }

        return redirect()->intended($this->redirectPath());
    }

    /**
     * Redirect the user for two-factor authentication.
     *
     * @param  Request  $request
     * @param  \Illuminate\Contracts\Auth\Authenticatable  $user
     * @return Response
     */
    protected function redirectForTwoFactorAuth(Request $request, $user)
    {
        Auth::logout();

        // Before we redirect the user to the two-factor token verification screen we will
        // store this user's ID and "remember me" choice in the session so that we will
        // be able to get it back out and log in the correct user after verification.
        $request->session()->put([
            'spark:auth:id' => $user->id,
            'spark:auth:remember' => $request->remember,
        ]);

        return redirect('login/token');
    }

    /**
     * Show the two-factor authentication token form.
     *
     * @param  Request  $request
     * @return Response
     */
    public function showTokenForm(Request $request)
    {
        return $request->session()->has('spark:auth:id')
                        ? view('spark::auth.token') : redirect('login');
    }

    /**
     * Verify the given authentication token.
     *
     * @param  Request  $request
     * @return Response
     */
    public function verifyToken(Request $request)
    {
        $this->validate($request, ['token' => 'required']);

        // If there is no authentication ID stored in the session, it means that the user
        // hasn't made it through the login screen so we'll just redirect them back to
        // the login view. They must have hit the route manually via a specific URL.
        if (! $request->session()->has('spark:auth:id')) {
            return redirect('login');
        }

        $user = Spark::user()->findOrFail(
            $request->session()->pull('spark:auth:id')
        );

        // Next, we'll verify the actual token with our two-factor authentication service
        // to see if the token is valid. If it is, we can login the user and send them
        // to their intended location within the protected part of this application.
        if (Spark::interact(Verify::class, [$user, $request->token])) {
            Auth::login($user, $request->session()->pull(
                'spark:auth:remember', false
            ));

            return redirect()->intended($this->redirectPath());
        } else {
            return back();
        }
    }

    /**
     * Log the user out of the application.
     *
     * @return \Illuminate\Http\Response
     */
    public function logout()
    {
        Auth::guard($this->getGuard())->logout();

        session()->flush();

        return redirect(
            property_exists($this, 'redirectAfterLogout')
                    ? $this->redirectAfterLogout : '/'
        );
    }
}
