<?php

namespace Laravel\Spark\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Laravel\Spark\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Foundation\Validation\ValidatesRequests;

class PasswordController extends Controller
{
    use ResetsPasswords;

    /**
     * Create a new password controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');

        $this->middleware('throttle:3,1')->only('sendResetLinkEmail', 'reset');
    }

    /**
     * Get the password reset validation rules.
     *
     * @return array
     */
    protected function getResetValidationRules()
    {
        return [
            'token' => 'required',
            'email' => 'required|email',
            'password' => 'required|password',
            'password_confirmation' => 'required|same:password',
        ];
    }

    /**
     * Get the password reset validation messages.
     *
     * @return array
     */
    protected function getResetValidationMessages()
    {
        return [
          'password.password' => "
            The password must be at least 8 characters in length
            and
            must contain at least 3 of the following 4 types of characters:
            lower case letters (i.e. a-z) ;
            upper case letters (i.e. A-Z) ;
            numbers (i.e. 0-9) ;
            special characters (e.g. -=[]\;,./~!@#$%^&*()_+{}|:<>?).
          ",
        ];
    }

    /**
     * Display the form to request a password reset link.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLinkRequestForm()
    {
        return view('spark::auth.passwords.email');
    }

    /**
     * Display the password reset view for the given token.
     *
     * If no token is present, display the link request form.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string|null  $token
     * @return \Illuminate\Http\Response
     */
    public function showResetForm(Request $request, $token = null)
    {
        if (is_null($token)) {
            return $this->showLinkRequestForm();
        }

        return view('spark::auth.passwords.reset')
                ->with(['token' => $token, 'email' => $request->email]);
    }
}
