<?php

namespace Laravel\Spark\Http\Controllers\Settings\Security;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Laravel\Spark\Http\Controllers\Controller;
use Validator;

class PasswordController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Update the user's password.
     *
     * @param  Request  $request
     * @return Response
     */
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
          'current_password' => 'required',
          'password' => 'required|password',
          'password_confirmation' => 'required|same:password',
        ], [
          'password.password' => "
            The password must be at least 8 characters in length
            and
            must contain at least 3 of the following 4 types of characters:
            lower case letters (i.e. a-z) ;
            upper case letters (i.e. A-Z) ;
            numbers (i.e. 0-9) ;
            special characters (e.g. -=[]\;,./~!@#$%^&*()_+{}|:<>?).
          ",
        ]);
        if ($validator->fails()) {
          return response()->json($validator->errors(), 422);
        }

        if (! Hash::check($request->current_password, $request->user()->password)) {
            return response()->json([
                'current_password' => ['The given password does not match our records.']
            ], 422);
        }

        $request->user()->forceFill([
            'password' => bcrypt($request->password)
        ])->save();
    }
}
