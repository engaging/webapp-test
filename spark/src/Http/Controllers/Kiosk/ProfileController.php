<?php

namespace Laravel\Spark\Http\Controllers\Kiosk;

use Laravel\Spark\Spark;
use Illuminate\Http\Request;
use Laravel\Spark\Http\Controllers\Controller;
use Laravel\Spark\Contracts\Repositories\UserRepository;
use Laravel\Spark\Contracts\Repositories\PerformanceIndicatorsRepository;
use App\Services\Storage\StorageService;
use Throwable;
use Log;

class ProfileController extends Controller
{

    const PROFILE_KEYS = ['firstname', 'lastname', 'company', 'title', 'sector', 'email', 'secondary_email', 'telephone', 'country'];
    const WHITELABEL_KEYS = ['disclaimer', 'navbarTextColor', 'navbarColor', 'navbarLineColor', 'headerColor', 'headerLineColor'];

    /**
     * The performance indicators repository instance.
     *
     * @var PerformanceIndicatorsRepository
     */
    protected $indicators;
    protected $storage;

    /**
     * Create a new controller instance.
     *
     * @param  PerformanceIndicatorsRepository  $indicators
     * @return void
     */
    public function __construct(PerformanceIndicatorsRepository $indicators, StorageService $storage)
    {
        $this->indicators = $indicators;
        $this->storage = $storage;

        $this->middleware('auth');
        $this->middleware('dev');
    }

    /**
     * Get the user to be displayed on the user profile screen.
     *
     * @param  Request  $request
     * @param  string  $id
     * @return Response
     */
    public function show(Request $request, $id)
    {
        $user = Spark::call(UserRepository::class.'@find', [$id]);

        return response()->json([
            'user' => $user,
            'revenue' => $this->indicators->totalRevenueForUser($user),
        ]);
    }

    public function base64ToFile($data, $folder, $id){
        $key = $folder . '/' . $id;
        try {
            return $this->storage->uploadDataURL($data, $key);
        } catch(Throwable $e) {
            Log::error($e, [
              'key' => $key,
            ]);
        }
        return false;
    }

    public function update(Request $request, $id)
    {
        $user = Spark::call(UserRepository::class.'@find', [$id]);


        $revertWhitelabel = $request->get('revertWhitelabel', '');
        if(!empty($revertWhitelabel)){
            foreach(self::WHITELABEL_KEYS as $key){
                $user->$key = "";
            }
            $user->logoImage = '';
            $user->save();

            return response()->json([
                'user' => $user,
                'revenue' => $this->indicators->totalRevenueForUser($user),
            ]);
        }


        foreach(array_merge(self::PROFILE_KEYS, self::WHITELABEL_KEYS) as $key){
            $value = $request->get($key, '');
            $user->$key = $value;
        }

        $base64_image = $request->get('base64_image', '');
        if(!empty($base64_image)){
            $image = $this->base64ToFile($base64_image, 'whitelabel-logos',$id);
            if($image !== false){
                $user->logoImage = $image;
            }
        }

        $user->save();

        return response()->json([
            'user' => $user,
            'revenue' => $this->indicators->totalRevenueForUser($user),
        ]);
    }

    public function toggleblocked(Request $request, $id)
    {
        $user = Spark::call(UserRepository::class.'@find', [$id]);
        if($user->blocked == 0){
            $user->blocked = time();
        } else {
            $user->blocked = 0;
        }

        $user->save();

        return response()->json([
            'user' => $user,
            'revenue' => $this->indicators->totalRevenueForUser($user),
        ]);
    }

    public function remove(Request $request, $id)
    {
        $user = Spark::call(UserRepository::class.'@find', [$id]);
        $user->forceDelete();

        return response()->json([
            'success' => true,
        ]);
    }
}
