<?php

namespace Laravel\Spark\Http\Controllers\Kiosk;

use Laravel\Spark\Spark;
use Illuminate\Http\Request;
use Laravel\Spark\Http\Controllers\Controller;
use Laravel\Spark\Contracts\Repositories\UserRepository;
use Maatwebsite\Excel\Facades\Excel;
use App\User;
use Helper;

class SearchController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('dev');
    }

    /**
     * Get the users based on the incoming search query.
     *
     * @param  Request  $request
     * @return Response
     */
    public function performBasicSearch(Request $request)
    {
        $query = User::where('id', '>', 0)->with('subscriptions');

        // If a user to exclude was passed to the repository, we will exclude their User
        // ID from the list. Typically we don't want to show the current user in the
        // search results and only want to display the other users from the query.
        $excludeUser = $request->user();
        if ($excludeUser) {
          // $query->where('id', '<>', $excludeUser->id);
        }

        $keyword = $request->input('keyword', '');
        if (!empty($keyword)) {
          $keyword = '%' . $keyword . '%';
          $query->where(function($groupClause) use($keyword) {
            $groupClause->where('firstname', 'like', $keyword)
              ->orWhere('lastname', 'like', $keyword)
              ->orWhere('name', 'like', $keyword)
              ->orWhere('email', 'like', $keyword)
              ->orWhere('country', 'like', $keyword)
              ->orWhere('company', 'like', $keyword);
          });
        }

        $dir = $request->input('dir', 'asc');
        $sort = $request->input('sort');
        if (!$sort || (is_string($sort) && in_array($sort, [
          'subscriptions',
          'portfoliosCount',
        ]))) {
          $dir = 'asc';
          $sort = ['firstname', 'lastname'];
        }
        if (is_string($sort)) {
          $query->orderBy($sort, $dir);
        } else {
          foreach ($sort as $value) {
            $query->orderBy($value, $dir);
          }
        }

        $exportFormat = $request->input('exportFormat', '');
        switch ($exportFormat) {
          case 'xls': {
            $data = $query->get()->toArray();
            $data = array_map(function($o) {
              return [
                'name' => $o['name'] ?: $o['firstname'] . ' ' . $o['lastname'],
                'email' => $o['email'],
                'firstname' => $o['firstname'],
                'lastname' => $o['lastname'],
                'telephone' => $o['telephone'],
                'company' => $o['company'],
                'title' => $o['title'],
                'sector' => $o['sector'],
                'secondary_email' => $o['secondary_email'],
                'country' => $o['country'],
                'approved' => $o['approved'] > 0 ? 'Yes' : 'No',
                'approvalSent' => $o['approval_sent'] > 0 ? 'Yes' : 'No',
                'created_at' => $o['created_at'],
                'blocked' => $o['blocked'] > 0 ? 'Yes' : 'No',
                'plans' => !empty($o['subscriptions']) ? join(Helper::map($o['subscriptions'], 'name'), ', ') : 'Free',
              ];
            }, $data);

            return Excel::create('User Export', function($excel) use($data) {
                $excel->sheet('Users', function($sheet) use($data) {
                    $sheet->fromArray($data);
                });
            })->export('xls');
          }
        }
        return $query->get();
    }
}
