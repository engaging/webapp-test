<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pca extends Model
{

    protected $primaryKey = 'id';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      'options',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];
    
    public function user(){
      return $this->belongsTo('App\User');
    }

    public function portfolio(){
      return $this->belongsTo('App\Portfolio');
    }

}
