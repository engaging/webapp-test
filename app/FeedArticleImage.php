<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class FeedArticleImage extends Model
{

    protected $table = 'feed_article_images';
    protected $guarded = ['id'];
}
