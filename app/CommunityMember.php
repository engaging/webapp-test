<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Indexinfo;
use Illuminate\Support\Facades\Auth;
use Laravel\Spark\Spark;
use App\Connection;
use App\ConnectionRequest;

class CommunityMember extends Model
{

    use SoftDeletes;

    protected $dates = ['deleted_at'];
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'community_members';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      'name',
      'type',
    ];

    protected $appends = [
      'pending',
      'following',
    ];

    public function team(){
      return $this->belongsTo('App\Team');
    }

    public function strategies(){
      return $this->hasMany('App\Indexinfo', 'provider', 'name');
    }

    public function connectionRequests($status = ''){

        $query = $this->hasMany("App\ConnectionRequest");
        if(!empty($status)){
            $query->where('status', '=', $status);
        }

        return $query;
    }

    public function getPendingAttribute(){
        $user = Auth::user();
        return $user->hasConnectionRequestWithCommunityMember($this, 'pending');
    }

    public function getFollowingAttribute(){
        $user = Auth::user();
        return $user->isConnectedWithCommunityMember($this);
    }

    public function canEdit(User $user){
        $isMember = $this->isMember($user);
        return $isMember;//in case we need roles later.
    }

    public function getOwnStrategies($provideName = '') {
      return Indexinfo::whereIn('provider', [$provideName])->pluck('id')->toArray();
    }

    public function isMember(User $user){
        if ($user->hasTeams()) {
            if($this->team_id == $user->currentTeam->id){
                return true;
            }
        }
        if($user->isAdmin()){
            return true;
        }
        return false;
    }

    public function users(){
        if($team = Team::find($this->team_id)){
            return $team->users();
        }
        return User::where('id', '=', $this->team_id);//@TODO - this shouldnt be allowed.
    }
}
