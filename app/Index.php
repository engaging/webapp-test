<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Index extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'fund_data';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'indexName',
        'close'
    ];

    // /**
    //  * Get the user that performed the activity.
    //  */
    // public function user()
    // {
    //     return $this->belongsTo(User::class, 'user_id');
    // }
}
