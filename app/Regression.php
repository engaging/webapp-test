<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Regression extends Model
{

    protected $primaryKey = 'id';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      'options',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];

    public function user(){
      return $this->hasOne('App\User');
    }

    public function indexinfo(){
      return $this->hasOne('App\Indexinfo');
    }

    public function portfolio(){
      return $this->hasOne('App\Portfolio');
    }

}
