<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use PasswordPolicy\PolicyBuilder;
use PasswordPolicy;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        PasswordPolicy::define('default', function (PolicyBuilder $builder) {
          $builder
            // must be at least 8 characters in length
            ->minLength(8)
            // must contain at least 3 of the following 4 types of characters:
            ->minPassingRules(3, function (PolicyBuilder $builder) {
              $builder
                // - lower case letters (i.e. a-z)
                ->lowerCase(1)
                // - upper case letters (i.e. A-Z)
                ->upperCase(1)
                // - numbers (i.e. 0-9)
                ->digits(1)
                // - special characters (e.g. -=[]\;,./~!@#$%^&*()_+{}|:<>?)
                ->contains(str_split('-=[]\;,./~!@#$%^&*()_+{}|:<>?'));
            });
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(
          'App\Services\Lab\BacktestingService',
          'App\Services\Lab\BacktestingServiceImpl'
        );
        $this->app->singleton(
          'App\Services\Storage\StorageService',
          'App\Services\Storage\StorageServiceImpl'
        );
        $this->app->singleton(
          'App\Services\Right\RightService',
          'App\Services\Right\RightServiceImpl'
        );
        $this->app->singleton(
          'App\Services\Portfolio\PortfolioService',
          'App\Services\Portfolio\PortfolioServiceImpl'
        );
        $this->app->singleton(
          'App\Services\Analytics\AnalyticsService',
          'App\Services\Analytics\AnalyticsServiceImpl'
        );
        $this->app->singleton(
          'App\Services\Lab\RegressionService',
          'App\Services\Lab\RegressionServiceImpl'
        );
        $this->app->singleton(
          'App\Services\Lab\PCAService',
          'App\Services\Lab\PCAServiceImpl'
        );
        $this->app->singleton(
          'App\Services\Export\ExcelExportService',
          'App\Services\Export\ExcelExportServiceImpl'
        );
    }
}
