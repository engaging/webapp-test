<?php

namespace App\Providers;

use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Laravel\Spark\Spark;
use Laravel\Spark\Providers\AppServiceProvider as ServiceProvider;
use App\User;

class SparkServiceProvider extends ServiceProvider
{
    /**
     * Your application and company details.
     *
     * @var array
     */
    protected $details = [
        'vendor' => 'Engaging',
        'product' => '',
        'street' => '3908 Two Exchange Square, 8 Connaught Place',
        'location' => 'Central, Hong Kong',
        'phone' => '',
    ];

    /**
     * The address where customer support e-mails should be sent.
     *
     * @var string
     */
    protected $sendSupportEmailsTo = 'admin@engagingenterprises.co';

    /**
     * All of the application developer e-mail addresses.
     *
     * @var array
     */
    protected $developers = [
      'admin@engagingenterprises.co',
      'admin@engagingenterprises.co',
      'admin@engagingenterprises.co',
      'admin@engagingenterprises.co',
    ];

    /**
     * Indicates if the application will expose an API.
     *
     * @var bool
     */
    protected $usesApi = true;


    /**
     * Finish configuring Spark for the application.
     *
     * @return void
     */
    public function booted()
    {
        Spark::useStripe()->noCardUpFront(); // ->trialDays(10);

        // Spark::freePlan();

        Spark::plan('Standard', 'standard')
            ->price(0)
            ->features([
              'Regression analysis (Factor analysis)',
              'Principle component analysis (PCA)',
            ]);

        Spark::plan('Premium', 'premium')
            ->price(0)
            ->features([
              'Regression analysis (Factor analysis)',
              'Principle component analysis (PCA)',
              'Advanced Excel export for strategy and portfolio tracks',
            ]);

        Spark::plan('Premium + Custom Model', 'premiumCustomModel')
            ->price(0)
            ->features([
              'Regression analysis (Factor analysis)',
              'Principle component analysis (PCA)',
              'Advanced Excel export for strategy and portfolio tracks',
              'Custom Rebalancing Model for Backtesting',
            ]);

        Spark::validateUsersWith(function () {
            return [
                'firstname' => 'required|max:255',
                'lastname' => 'required|max:255',
                'company' => '',
                'title' => '',
                'sector' => 'required',
                'email' => 'required|email|max:255|unique:users',
                'secondary_email' => '',
                'telephone' => 'required|max:20',
                'country' => 'required',
                'terms' => 'required|accepted',
            ];
        });

		    User::saving(function ($user) {
           $user->resetApprovalToken();
           return true;
       	});


        User::created(function ($user) {
           if (!Auth::check()) {
             $user->sendWelcomeEmail();
           }
           return true;
        });

    		Spark::createUsersWith(function ($request) {
    		    $user = Spark::user();

    		    $data = $request->all();

    		    $user->forceFill([
    		        'firstname' => $data['firstname'],
    		        'lastname' => $data['lastname'],
    		        'email' => $data['email'],
    		        'telephone' => !empty($data['telephone']) ? $data['telephone'] : '',
    		        'company' => !empty($data['company']) ? $data['company'] : '',
    		        'title' => !empty($data['title']) ? $data['title'] : '',
                    'sector' => !empty($data['sector']) ? $data['sector'] : '',
                    'secondary_email' => !empty($data['secondary_email']) ? $data['secondary_email'] : '',
                    'country' => !empty($data['country']) ? $data['country'] : '',
    		        'password' => '',
    		        'approved' => 0,
    		        'last_read_announcements_at' => Carbon::now(),
    		        'trial_ends_at' => Carbon::now()->addDays(Spark::trialDays()),
    		    ])->save();

    		    return $user;
    		});

    		Spark::swap('UserRepository@search', function ($query, $excludeUser = null) {
    		    $search = Spark::user()->with('subscriptions');

    		    // If a user to exclude was passed to the repository, we will exclude their User
    		    // ID from the list. Typically we don't want to show the current user in the
    		    // search results and only want to display the other users from the query.
    		    if ($excludeUser) {
    		        $search->where('id', '<>', $excludeUser->id);
    		    }

    		    $response = $search->where(function ($search) use ($query) {
    		        $search->where('email', 'like', $query)
    		               ->orWhere('name', 'like', $query);
    		    })->get();

    		    return $response;

    		});
    }
}
