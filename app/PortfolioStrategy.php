<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PortfolioStrategy extends Model
{

    public function indexinfo(){
    	return $this->belongsTo('App\Indexinfo');
    }

    public function portfolio(){
    	return $this->belongsTo('App\Portfolio');
    }
}
