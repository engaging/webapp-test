<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FavouriteItem extends Model
{
    public function indexinfo(){
    	return $this->belongsTo('App\Indexinfo', 'index_id');
    }

    public function favouriteSet(){
    	return $this->belongsTo('App\FavouriteSet', 'favourite_sets_id');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }
}
