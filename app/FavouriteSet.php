<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FavouriteSet extends Model
{
    
    public function user(){
        return $this->belongsTo('App\User');
    }

    public function favouriteItems(){
    	return $this->hasMany('App\FavouriteItem', 'favourite_sets_id')->with(['indexinfo']);
    }

    public function afterSave(){
    	$count = $this->hasMany('App\FavouriteItem', 'favourite_sets_id')->count();
    	$this->strategyCount = $count;
    	$this->save();
    }
}
