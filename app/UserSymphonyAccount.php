<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserSymphonyAccount extends Model
{

    protected $primaryKey = 'id';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      'account_id',
      'account_email',
      'approval_code',
      'approved',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
      'account_id',
      'approval_code',
    ];

}
