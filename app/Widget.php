<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

class Widget extends Model
{

    public $table = "widgets";

    public function user(){
      return $this->belongsTo('App\User');
    }
    
}
