<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserRight extends Model
{

    public $timestamps = true;

    protected $primaryKey = 'id';

    protected $appends = ['hasApiKey'];

    protected $hidden = ['api_key', 'api_secret'];

    protected $fillable = [
      'community_my_page',
      'community_am_page',
      'community_is_page',
      'strategy_restriction',
      'api_key',
      'api_secret',
      'api_scope',
    ];

    public function user(){
      return $this->belongsTo('App\User');
    }

    public function getHasApiKeyAttribute(){
      return !empty($this->api_key);
    }

}
