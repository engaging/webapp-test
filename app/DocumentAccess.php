<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DocumentAccess extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'documents_access';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'document_id', 'group'
    ];
}
