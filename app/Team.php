<?php

namespace App;

use Laravel\Spark\Team as SparkTeam;

class Team extends SparkTeam
{

    public function communityMember()
    {
        return $this->hasOne('App\CommunityMember');
    }

}
