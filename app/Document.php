<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use DB;

class Document extends Model
{
    use SoftDeletes;
    protected $appends = ['shareWith', 'date'];
    protected $guarded = ['deleted_at', 'id'];
    protected $dates = ['deleted_at'];
    
    public function getShareWithAttribute(){
        $query = DB::table('documents_access')->where('document_id', '=', $this->id)->select('group');
        if($query->count() == 0){
            return ['all'];
        }
        return $query->pluck('group');
    }
    
    public function getDateAttribute(){
        return $this->created_at->format('d F Y');
    }
    
    public function getWebsiteAttribute($v){
        if(!empty($v)){
            return $this->addhttp($v);
        }
        return $v;
    }
    
    function addhttp($url) {
        if (!preg_match("~^(?:f|ht)tps?://~i", $url)) {
            $url = "http://" . $url;
        }
        return $url;
    }
}

