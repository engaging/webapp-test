<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Auth;

class RollbarHelper {

  function getPerson() {
    if ($user = Auth::user()) {
      return [
        'id' => $user->id, // required - value is a string
        'username' => $user->fullname, // optional - value is a string
        'email' => $user->email, // optional - value is a string
      ];
    }
    return null;
  }

}
