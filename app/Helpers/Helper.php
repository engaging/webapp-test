<?php

namespace App\Helpers;

use Exception;
use DateTime;
use DateInterval;

class Helper {

  /**
   * Public constants
   */

  const DAY_DIFF = 25569; // days between January 1, 1900 and 1970 (including 19 leap years)
  const SEC_IN_DAY = 86400; // seconds in a day

  /**
   * Public functions
   */

  /**
   * Dump data in json string and exit
   * To be used in association with DJSON Chrome Plugin (JSON Viewer & Formatter)
   * @link https://chrome.google.com/webstore/detail/djson-json-viewer-formatt/chaeijjekipecdajnijdldjjipaegdjc
   * @param any $data the data to dump
   */
  static function json_dump($data) {
    header('Content-type: application/json');
    echo !is_string($data) ? json_encode($data) : $data;
    exit;
  }

  /**
   * Debug data with json dump enabled by query string
   * Disable debugging by setting APP_DEBUG env to false
   * @param string $queryString the query string which enables data debugging
   * @param any    $data        the data to dump when debugging is enabled
   */
  static function debug($queryString, $data) {
    if (env('APP_DEBUG') === true && isset($_GET[$queryString])) {
      self::json_dump($data);
    }
  }

  /**
   * Converts the first character of each word to upper case and the remaining to lower case
   * @param  String $str
   * @return String
   */
  static function capitalize($str) {
    if (is_string($str)) {
      return ucwords(strtolower($str));
    }
    return null;
  }

  /**
   * Converts a string to start case
   * @param  String $str
   * @return String
   */
  static function startCase($str) {
    if (is_string($str)) {
      $chars = [' '];
      $str = str_replace('-', ' ', $str);
      $str = str_replace('_', ' ', $str);
      foreach (str_split($str) as $char) {
        if (ctype_lower($char) && end($chars) === ' ') {
          $chars[] = ' ';
          $chars[] = strtoupper($char);
        } else if (ctype_upper($char) && !ctype_upper(end($chars))) {
          $chars[] = ' ';
          $chars[] = $char;
        } else {
          $chars[] = $char;
        }
      }
      $str = join('', $chars);
      $str = preg_replace('!\s+!', ' ', $str);
      return trim($str);
    }
    return null;
  }

  /**
   * Converts a string to title case
   * @param  String $str
   * @return String
   */
  static function titleCase($str) {
    if (is_string($str)) {
      return self::startCase(strtolower($str));
    }
    return null;
  }

  /**
   * Get a value from a key in an array or an object
   * Params passed by value to avoid: "Indirect modification of overloaded property has no effect"
   * @param  arr/obj $arrOrObj the array or the object
   * @param  string  $key      the key to get
   * @param  any     $def      the default value (optional, null by default)
   * @return any               the value if set, the default value if unset
   */
  static function get($arrOrObj, $key, $def = null) {
    if (is_array($arrOrObj)) {
      return isset($arrOrObj[$key]) ? $arrOrObj[$key] : $def;
    } else if (is_object($arrOrObj)) {
      return isset($arrOrObj->{$key}) ? $arrOrObj->{$key} : $def;
    } else {
      return $def;
    }
  }

  /**
   * Set a value to a key in an array or an object
   * @param arr/obj $arrOrObj the array or the object
   * @param string  $key      the key to set
   * @param any     $val      the value
   */
  static function set(&$arrOrObj, $key, $val) {
    if (is_array($arrOrObj)) {
      $arrOrObj[$key] = $val;
    } else if (is_object($arrOrObj)) {
      $arrOrObj->{$key} = $val;
    }
  }

  /**
   * Finds whether a variable is a sequential array
   * @param  mixed   $var the variable being evaluated
   * @return boolean
   */
  static function is_seque_array($var) {
    return (is_array($var) && array_values($var) === $var);
  }

  /**
   * Finds whether a variable is an associative array
   * @param  mixed   $var the variable being evaluated
   * @return boolean
   */
  static function is_assoc_array($var) {
    return (is_array($var) && array_values($var) !== $var);
  }

  /**
   * Unset all null values from an array or an object
   * @param arr/obj $arrOrObj the array or the object
   */
  static function unsetNullValues(&$arrOrObj) {
    $isObject = is_object($arrOrObj);
    // if object convert to an array
    $array = $isObject ? (array) $arrOrObj : $arrOrObj;
    // filter null values in the array
    $array = array_filter($array, function ($val) {
      return !is_null($val);
    });
    // if object convert back to an object
    $arrOrObj = $isObject ? (object) $array : $array;
  }

  /**
   * Unset all empty arrays from an array or an object
   * @param arr/obj $arrOrObj the array or the object
   */
  static function unsetEmptyArrays(&$arrOrObj) {
    $isObject = is_object($arrOrObj);
    // if object convert to an array
    $array = $isObject ? (array) $arrOrObj : $arrOrObj;
    // filter empty arrays in the array
    $array = array_filter($array, function ($val) {
      return !is_array($val) || !empty($val);
    });
    // if object convert back to an object
    $arrOrObj = $isObject ? (object) $array : $array;
  }

  /**
   * Map array items keys to an array of values
   * @param  array  $arr  the array source
   * @param  string $key  the key to be map
   * @param  str/fn $func the function to apply to values (optional)
   * @return array        new array instance
   */
  static function map($arr, $key, $func = null) {
    $getKey = function($o) use($key, $func) {
      $val = self::get($o, $key);
      return $func ? $func($val) : $val;
    };
    return array_values(array_map($getKey, $arr));
  }

  /**
   * Group array items by keys
   * @param  array  $arr  the array source
   * @param  string $key  the key to be group
   * @param  str/fn $func the function to apply to keys (optional)
   * @return array        new array instance
   */
  static function keyBy($arr, $key, $func = null) {
    $array = [];
    foreach ($arr as $item) {
      $val = self::get($item, $key);
      if ($func) $val = $func($val);
      $array[$val] = $item;
    }
    return $array;
  }

  /**
   * Apply a function to array or object keys
   * If the value is an array, the function is map to the array items
   * @param  str/fn  $func     the function to apply
   * @param  arr/obj $arrOrObj the array or the object
   * @param  array   $keys     the keys to be applied
   */
  static function func2keys($func, &$arrOrObj, $keys) {
    foreach($keys as $idx => $key){
      $val = self::get($arrOrObj, $key);
      if (!$val) continue;
      if (is_array($val)) {
        self::set($arrOrObj, $key, array_map($func, $val));
      } else {
        self::set($arrOrObj, $key, $func($val));
      }
    }
  }

  /**
   * Return a number from a string, no matter what it may contain
   * Has support for negative values
   * @param  string $string
   * @return integer
   */
  static function str2int($string){
    return (int) preg_replace('/[^\-\d]*(\-?\d*).*/', '$1', $string);
  }

  /**
  * Convert an array to an object (deep cast)
  * @param  array  $array the array to convert
  * @return object        new stdClass() instance
  */
  static function arr2obj($array){
    $json = json_encode($array); // convert the array to a json string
    $object = json_decode($json); // convert the json string to a stdClass()
    return $object;
  }

  /**
  * Convert an object to an array (deep cast)
  * @param  object $object the object to convert
  * @return array          new array instance
  */
  static function obj2arr($object){
    $json = json_encode($object); // convert the object to a json string
    $array = json_decode($json, true); // convert the json string to an array
    return $array;
  }

  /**
  * Convert a mixed type to a DateTime object
  * Support DateTime, int and string types
  * @param  mixed    $mixed  the mixed type to convert
  * @param  string   $format the format if string type
  *  !Y-m-d format for YYYY-MM-DD (default)
  *  Y-m-d H:i:s format for YYYY-MM-DD HH:mm:ss
  *  Y-m-dTH:i:s format for YYYY-MM-DDTHH:mm:ss
  * @return DateTime         new DateTime instance
  */
  static function toDate($mixed, $format = '!Y-m-d') {
    return ($mixed instanceof DateTime) ? clone($mixed) :
      (is_int($mixed) ? (new DateTime())->setTimestamp($mixed) :
      (is_string($mixed) ? DateTime::createFromFormat($format, $mixed) : null));
  }

  static function excel2time($exceldate, $timepart = '00:00:00'){
   $timestamp = (intval($exceldate) - self::DAY_DIFF) * self::SEC_IN_DAY; // convert to timestamp
   return $timepart === '00:00:00' ? $timestamp // return the timestamp or
    : strtotime(strftime("%F {$timepart}", $timestamp)); // add time part
  }

  static function time2excel($timestamp){
    $timestamp = strtotime(date('Y-m-d', $timestamp)); // remove the time part
    return intval(($timestamp / self::SEC_IN_DAY) + self::DAY_DIFF); // convert to excel date
  }

  static function excel2date($exceldate, $timepart = '00:00:00'){
    return self::toDate(self::excel2time($exceldate, $timepart));
  }

  static function date2excel(DateTime $date){
    return self::time2excel($date->getTimestamp());
  }

  static function ISOString2sql($ISOString, $defaultTime = '00:00:00'){
    $separator = 'T'; // T separator
    if (strpos($ISOString, $separator) === false) {
      $separator = ' '; // space separator
    };
    $parts = explode($separator, trim($ISOString));
    $date = $parts[0];
    if (count($parts) > 1) {
      $time = $parts[1];
    }
    $time = isset($time) ? explode('.', $time)[0] : $defaultTime;
    return "$date $time";
  }

  static function dateString2sql($dateString, $defaultTime = '00:00:00'){
    if (!is_numeric($dateString)) {
      return self::ISOString2sql($dateString, $defaultTime);
    }
    if (strlen($dateString) === 8) {
      $dateString += implode(explode(':', $defaultTime));
    }
    return intval($dateString);
  }

  static function isWeekendTime($timestamp) {
    return intval(strftime('%u', $timestamp)) > 5; // is weekend if day > Friday
  }

  static function isWeekendDate(DateTime $date) {
    return $date->format('N') > 5; // is weekend if day > Friday
  }

  static function countWeekendDaysBetweenTimes($startTime, $endTime) {
    $count = 0;
    $startTime = strtotime(date('Y-m-d', $startTime));
    $endTime = strtotime(date('Y-m-d', $endTime));
    for ($time = $startTime; $time <= $endTime; $time += self::SEC_IN_DAY) {
      if (self::isWeekendTime($time)) {
        $count++;
      }
    }
    return $count;
  }

  static function countWeekendDaysBetweenDates($startDate, $endDate) {
    $count = 0;
    $startDate = clone($startDate);
    $startDate->setTime(0, 0, 0);
    $endDate = clone($endDate);
    $endDate->setTime(0, 0, 0);
    $oneDay = new DateInterval('P1D');
    for ($date = $startDate; $date <= $endDate; $date->add($oneDay)) {
      if (self::isWeekendDate($date)) {
        $count++;
      }
    }
    return $count;
  }

  static function appendDates(array &$track, $toDate, $withValue = 'last', $weekend = false) {
    list($date, $value) = end($track);
    if ($withValue !== 'last') {
      $value = $withValue;
    }
    $date = self::excel2date($date);
    $toDate = self::toDate($toDate);
    $toDate->setTime(0, 0, 0);
    $oneDay = new DateInterval('P1D');
    for ($date->add($oneDay); $date < $toDate; $date->add($oneDay)) {
      if (!$weekend && self::isWeekendDate($date)) continue;
      $track[] = [self::date2excel($date), floatval($value)];
    }
  }

  static function fillMissingDates(array &$track, $toDate = null, $withValue = 'last', $weekend = false) {
    $newTrack = [];
    foreach ($track as $idx => list($date, $value)) {
      $currDate = self::excel2date($date);
      if ($idx > 0 && $currDate->diff($prevDate)->days > 1) {
        self::appendDates($newTrack, $currDate, $withValue, $weekend);
      }
      $newTrack[] = [intval($date), floatval($value)];
      $prevDate = $currDate;
    }
    if ($toDate) {
      self::appendDates($newTrack, $toDate, $withValue, $weekend);
    }
    $track = $newTrack;
  }

  static function fillMissingDatesAndValue(array &$dates, array &$values) {
    $newDates = [];
    $newValues = [];
    $lackValue = [];
    $prevDate = null;
    $currDate = null;
    $oneDay = new DateInterval('P1D');
    foreach ($dates as $idx => $date) {
      $currDate = self::excel2date($date);
      if ($idx > 0 && $currDate->diff($prevDate)->days > 1) {
        $activeDays = [];
        for ($prevDate->add($oneDay); $prevDate < $currDate; $prevDate->add($oneDay)) {
          if (self::isWeekendDate($prevDate)) continue;
          $excelDate = self::date2excel($prevDate);
          $newDates[] = $excelDate;
          $activeDays[] = $excelDate;
        }
        if (count($activeDays) > 0) {
          $prevValue = $values[$idx - 1];
          if (is_array($prevValue)) {
            $diffValue = [];
            foreach ($prevValue as $cellIdx => $prevValueCell) {
              $diffValue[] = ($values[$idx][$cellIdx] - $prevValueCell) / (count($activeDays) + 1);
            }
            foreach ($activeDays as $activeIdx => $activeDay) {
              $valueArray = [];
              foreach($prevValue as $cellIdx => $prevValueCell) {
                $valueArray[] = $prevValueCell + ($activeIdx + 1) * $diffValue[$cellIdx];
              }
              $newValues[] = $valueArray;
            }
          } else {
            $diffValue = ($values[$idx] - $prevValue) / (count($activeDays) + 1);
            foreach ($activeDays as $activeIdx => $activeDay) {
              $newValues[] = ($prevValue + ($activeIdx + 1) * $diffValue);
            }
          }
        }
      }
      $prevDate = $currDate;
      $newValues[] = $values[$idx];
      $newDates[] = $date;
    }
    $values = $newValues;
    $dates = $newDates;
  }

  /**
   * Return the earlier weekday timestamp
   * @param  integer $timestamp
   * @return integer
   */
  static function shiftTimeToWeekday($timestamp) {
    $u = strftime('%u', $timestamp);
    if ($u === '6') { // if Saturday
      $timestamp = $timestamp - (self::SEC_IN_DAY); // subtract 1 day
    }
    if ($u === '7') { // if Sunday
      $timestamp = $timestamp - (self::SEC_IN_DAY * 2); // subtract 2 days
    }
    return $timestamp;
  }

  /**
   * Return the earlier weekday date
   * @param  DateTime $date
   * @return DateTime       new instance, original parameter is unchanged
   */
  static function shiftDateToWeekday(DateTime $date) {
    $date = clone($date);
    if (self::isWeekendDate($date)) {
      return $date->modify('previous friday');
    }
    return $date;
  }

  /**
   * Return the date of the given day of the month
   * @param  DateTime $date
   * @param  str/int  $day  "first" or "last" or integer between 1 and 31
   * @return DateTime       new instance, original parameter is unchanged
   */
  static function shiftDateToDayOfMonth(DateTime $date, $day){
    $date = clone($date);
    if ($day === 'first' || $day === 'last') {
      return $date->modify($day . ' day of this month');
    } else if ($day >= 1 && $day <= 31) {
      $lastDay = clone($date->modify('last day of this month'));
      $date->modify('first day of this month');
      $date->modify($day - 1 . ' day');
      return ($date < $lastDay) ? $date : $lastDay;
    } else {
      throw new Exception('The day should be "first" or "last" or between 1 and 31');
    }
  }

  /**
   * Correctly calculates end of months when we shift to a shorter or longer month
   * Workaround for http://php.net/manual/en/datetime.add.php#example-2489
   *
   * Makes the assumption that shifting from the 28th Feb +1 month is 31st March
   * Makes the assumption that shifting from the 28th Feb -1 month is 31st Jan
   * Makes the assumption that shifting from the 29,30,31 Jan +1 month is 28th (or 29th) Feb
   *
   * @param  integer $timestamp
   * @param  integer $months    positive or negative
   * @return integer
   */
  static function shiftTimeMonths(int $timestamp, int $months) {
    $date = (new DateTime())->setTimestamp($timestamp);
    return self::shiftDateMonths($date, $months)->getTimestamp();
  }

  /**
   * Correctly calculates end of months when we shift to a shorter or longer month
   * Workaround for http://php.net/manual/en/datetime.add.php#example-2489
   *
   * Makes the assumption that shifting from the 28th Feb +1 month is 31st March
   * Makes the assumption that shifting from the 28th Feb -1 month is 31st Jan
   * Makes the assumption that shifting from the 29,30,31 Jan +1 month is 28th (or 29th) Feb
   *
   * @param  DateTime $date
   * @param  integer  $months positive or negative
   * @return DateTime         new instance, original parameter is unchanged
   */
  static function shiftDateMonths(DateTime $date, int $months){
    $dateA = clone($date);
    $dateB = clone($date);
    $plusMonths = clone($dateA->modify($months . ' Month'));
    //check whether reversing the month addition gives us the original day back
    if($dateB != $dateA->modify($months*-1 . ' Month')){
        $result = $plusMonths->modify('last day of last month');
    } elseif($date == $dateB->modify('last day of this month')){
        $result =  $plusMonths->modify('last day of this month');
    } else {
        $result = $plusMonths;
    }
    return $result;
  }

  /**
   * Generate a random string, using a cryptographically secure
   * pseudorandom number generator (random_int)
   *
   * For PHP 7, random_int is a PHP core function
   * For PHP 5.x, depends on https://github.com/paragonie/random_compat
   *
   * @param  integer $length   how many characters to return, 10 by default
   * @param  string  $keyspace a string of all possible characters to select from
   * @return string
   */
  static function random_str($length = 10, $keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ') {
    $str = '';
    $max = mb_strlen($keyspace, '8bit') - 1;
    for ($i = 0; $i < $length; ++$i) {
      $str .= $keyspace[random_int(0, $max)];
    }
    return $str;
  }

  /**
   * Trim unicode characters of a string
   * @param  string $string the string to trim
   * @return string         new trimmed string
   */
  static function trimUnicode($string) {
    $pattern = '/^[\pZ\p{Cc}\x{feff}]+|[\pZ\p{Cc}\x{feff}]+$/ux';
    $replacement = '';
    return preg_replace($pattern, $replacement, $string);
  }

  /**
   * Normalize the end of line characters of a string
   * @param  string  $string         the string to normalize
   * @param  string  $format         the eol format (optional)
   *                                  - "\n" = Unix format (LF) / default
   *                                  - "\r\n" = Windows format (CR/LF)
   *                                  - "\r" = Old Mac format (CR)
   * @param  integer $maxConsecutive the max consecutive eol allowed (optional)
   *                                  -  -1 = Infinite / default
   *                                  - >-1 = Finite integer applied
   * @return string                  new normalized string
   */
  static function normalizeEol($string, $format = "\n", $maxConsecutive = -1) {
    // Filter end of line characters to be normalized
    $convertEols = array_filter([
      "\n", // Unix format (LF)
      "\r\n", // Windows format (CR/LF)
      "\r", // Old Mac format (CR)
    ], function($f) use($format) {
      return $f !== $format;
    });
    // Convert all end of lines to the specified format
    foreach ($convertEols as $eol) {
      $string = str_replace($eol, $format, $string);
    }
    // Don't allow out-of-control consecutive blank lines
    if ($maxConsecutive > -1) {
      $pattern = '/' . $format . '{' . ($maxConsecutive + 1) . ',}/';
      $replacement = str_repeat($format, $maxConsecutive);
      $string = preg_replace($pattern, $replacement, $string);
    }
    // Trim unicode characters
    $string = self::trimUnicode($string);
    return $string;
  }

}
