<?php

namespace App\Helpers;

use App\User;
use App\Indexinfo;
use Helper;

class StrategyHelper {

  static function getBaseQuery(User $user, array $filters = []) {
    $owner = Helper::get($filters, 'owner');
    if (!$owner || !$user->isAdmin()) {
      $owner = Helper::get($user->currentTeam, 'id');
    }

    $ids = Helper::get($filters, 'ids', []);
    if (!empty($ids)) {
      $query = Indexinfo::whereIn('id', $ids);
    } else {
      $query = Indexinfo::where('id', '>', 0);
    }

    $only = Helper::get($filters, 'only', []);
    if (!empty($only)) {
      $query->whereIn('code', $only);
    }

    $ignore = Helper::get($filters, 'ignore', []);
    if (!empty($ignore)) {
      $query->whereNotIn('code', $ignore);
    }

    $from = Helper::get($filters, 'from');
    if ($from) {
      $query->where('updated_at', '>=', Helper::dateString2sql($from));
    }

    $to = Helper::get($filters, 'to');
    if ($to) {
      $query->where('updated_at', '<=', Helper::dateString2sql($to, '23:59:59'));
    }

    $providers = Helper::get($filters, 'providers', []);
    if (!empty($providers)) {
      $query->whereIn('provider', $providers);
    }

    $assetClasses = Helper::get($filters, 'assetClasses', []);
    if (!empty($assetClasses)) {
      $query->whereIn('assetClass', $assetClasses);
    }

    $factors = Helper::get($filters, 'factors', []);
    if (!empty($factors)) {
      $query->whereIn('factor', $factors);
    }

    $currencies = Helper::get($filters, 'currencies', []);
    if (!empty($currencies)) {
      $query->whereIn('currency', $currencies);
    }

    $regions = Helper::get($filters, 'regions', []);
    if (!empty($regions)) {
      $query->whereIn('region', $regions);
    }

    $styles = Helper::get($filters, 'styles', []);
    if (!empty($styles)) {
      $query->whereIn('style', $styles);
    }

    $returnTypes = Helper::get($filters, 'returnTypes', []);
    if (!empty($returnTypes)) {
      $query->whereIn('returnType', $returnTypes);
    }

    $types = Helper::get($filters, 'types', []);
    if (empty($types)) {
      $types = ['Strategy', 'Benchmark', 'Private Strategy', 'Pure Factor'];
    }

    $query->whereIn('type', $types);
    if (in_array('Private Strategy', $types)) {
      $query->where(function($groupClause) use($owner) {
        $groupClause->whereNull('owner')
          ->orWhere('owner', $owner);
      });
    } else {
      $query->whereNull('owner');
    }

    $keyword = Helper::get($filters, 'keyword', '');
    if (!empty($keyword)) {
      $keyword = '%' . $keyword . '%';
      $query->where(function($groupClause) use($keyword) {
        $groupClause->where('code', 'like', $keyword)
          ->orWhere('shortName', 'like', $keyword)
          ->orWhere('longName', 'like', $keyword);
      });
    }

    return $query;
  }

}
