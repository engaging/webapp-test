<?php

namespace App;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Input;
use App\Setting;
use Exception;
use Cache;
use App;
use DB;

class Portfolio extends Model
{
	use Sluggable;
    use SoftDeletes;
    public $ids = [];
    public $totalTimeOnQueries = 0;
    public $minDate;
    public $maxDate;
    public $latestUpdateToStrategy;
    public $cachedResults;
    public $cachedResultsByIdx;
    public $cachedResultsOrdered;
    public $tenYearsAgo = false;

    protected $backtesting;

    public function __construct(array $attributes = array()){
        $this->backtesting = App::make('App\Services\Lab\BacktestingService');
        parent::__construct($attributes);
    }

    public function sluggable(){
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    protected $guarded = ['id'];

    public $appends = ['portfolioStrategies', 'activePortfolioStrategies', 'strategyIds', 'activeStrategyIds', 'activeStrategyCodes', 'strategiesCount', 'activeStrategiesCount'];

    protected $hidden = ['portfolioStrategies', 'activePortfolioStrategies', 'activeStrategyIds', 'activeStrategyCodes'];

    public function attachAllFavourites(){
        $favourites = Auth::user()->favouriteIndices();
        foreach ($favourites->take(100)->get() as $favourite) {
            $PortfolioStrategy = new PortfolioStrategy;
            $PortfolioStrategy->indexinfo_id = $favourite->index_id;
            $PortfolioStrategy->portfolio_id = $this->id;
            $PortfolioStrategy->user_id = Auth::user()->id;
            //$PortfolioStrategy->equal_weighting = $this->getEqualWeighting($portfolio, $PortfolioStrategy);
            $PortfolioStrategy->weighting = 0;
            $PortfolioStrategy->save();
        }
        return;
    }

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function pca(){
        return $this->hasOne('App\Pca');
    }

    public function regression(){
        return $this->hasOne('App\Regression');
    }

    public function getPortfolioStrategiesAttribute(){
      return $this->hasMany('App\PortfolioStrategy')
        ->with(['indexinfo' => function($query) {
          $query->select('id', 'code', 'is_suspended');
        }])
        ->whereHas('indexinfo', function($query) {
          $query->whereNotNull('id');
        })
        ->get();
    }

    public function getStrategyIdsAttribute(){
      return $this->portfolioStrategies
        ->pluck('indexinfo.id')
        ->all();
    }

    public function getActivePortfolioStrategiesAttribute(){
      $byActive = function($o) {
        return !$o->suspended && !$o->indexinfo->is_suspended;
      };
      return collect($this->portfolioStrategies
        ->filter($byActive)
        ->all());
    }

    public function getActiveStrategyIdsAttribute(){
      return $this->activePortfolioStrategies
        ->pluck('indexinfo.id')
        ->all();
    }

    public function getActiveStrategyCodesAttribute(){
      return $this->activePortfolioStrategies
        ->pluck('indexinfo.code')
        ->all();
    }

    public function getStrategiesCountAttribute(){
      return count($this->strategyIds);
    }

    public function getActiveStrategiesCountAttribute(){
      return count($this->activeStrategyIds);
    }

    public function getStrategies(){
      return Indexinfo::whereIn('id', $this->strategyIds)->get();
    }

    public function getActiveStrategies(){
      return Indexinfo::whereIn('id', $this->activeStrategyIds)->get();
    }

    public function getPortfolioStrategiesWithIndexinfo(){
      return $this->hasMany('App\PortfolioStrategy')
        ->with(['indexinfo'])
        ->whereHas('indexinfo', function($query) {
          $query->whereNotNull('id');
        })
        ->get();
    }

    public function duplicate($title, $slug){
        $original = $this;
        $copy = $original->replicate();
        $copy->title = $title;
        $copy->slug = $slug;
        $copy->save();
        $copy->slug = $slug . "-" . $copy->id;
        $copy->save();
        $strategies = PortfolioStrategy::where('portfolio_id', '=', $original->id)->get();

        foreach ($strategies as $key => $s) {
            $draft_strategy = $s->replicate();
            $draft_strategy->portfolio_id = $copy->id;
            $draft_strategy->save();
        }
        $removedAny = $copy->setSuspendedStrategiesAsZeroWeight();
        if($removedAny > 0){
            $copy->calculateWeightings("*");
        }
        return $copy;
    }

    public function getDraft($resetStrategies = false, $forceThrough = false){
        if (!empty($this->draft_of)) {
          throw new Exception('Cannot create a draft from a draft');
        }
        $original = $this;

        $draft = Portfolio::where(
            'draft_of', '=', $original->id
        );
        // make new if it doesnt exists
        if ( count($draft->get())==0 ){
            $draft = $original->replicate();
            $draft->draft_of = $original->id;
        }
        // else update the draft to last saved original
        else {
            $draft = $draft->first();
            foreach($original->attributes as $key => $value){
                if ( $draft->$key && $key != 'id' && $key != 'slug' && $key != 'draft_of'){
                    $draft->$key = $value;
                }
            }
        }
        $draft->save();

        // match the strategies...
        if($resetStrategies == true){
            $deleted = DB::delete('delete from portfolio_strategies WHERE portfolio_id = ' . $draft->id);
            $strategies = PortfolioStrategy::where('portfolio_id', '=', $original->id)->get();

            foreach ($strategies as $key => $s) {
                $draft_strategy = $s->replicate();
                $draft_strategy->portfolio_id = $draft->id;
                $draft_strategy->save();
            }
            $removedAny = $draft->setSuspendedStrategiesAsZeroWeight();

            if($removedAny > 0){
                $draft->calculateWeightings("*");
            }
        }

        return $draft;
    }

    public function getNonDraftHash(){
        if(!empty($this->draft_of)){
            $portfolio = Portfolio::where(
                'id', '=', $this->draft_of
            )->first();
            if(!$portfolio){
                return 'recompile';
            }
            return $portfolio->hash;
        }

        if($this->draft_of == 0){
            $portfolio = Portfolio::where(
                'id', '=', $this->id
            )->first();
            return $portfolio->hash;//reload so not comparing to in-memory hash
        }

        return false;
    }

    public function latestUpdateToStrategy(){

        if(empty($this->latestUpdateToStrategy)){
            $row = $this->hasMany('App\PortfolioStrategy')
                ->selectRaw("MAX(updated_at) as ts")->first();
            $this->latestUpdateToStrategy = $row->ts;


        }

        if(empty($this->latestUpdateToStrategy)){
            $this->latestUpdateToStrategy . 'n/a';
        }

        return $this->latestUpdateToStrategy;
    }

    public function getHash(){
        if(empty($this->maxDate)){
            $this->maxDate = Setting::where('key', '=', 'maxDate')->first()->value;
        }
        $hash = $this->weighting . "-" . md5(implode(",", $this->strategyIds)) . "-" .$this->latestUpdateToStrategy();
        $hash .= "[" . $this->maxDate . "]";
        return $hash;
    }

    public function setRecomputeHash(){
        $this->hash = $this->getHash();
    }

    public function requiresRegeneration(){
        if($this->hash == 'recompute'){
            return true;
        }
        if($this->hash != $this->getNonDraftHash()){
            return true;
        }
        return false;
    }

    public function getTenYearsAgo($timestamp = false){
        if(empty($this->maxDate)){
            $this->maxDate = Setting::where('key', '=', 'maxDate')->first()->value;
        }

        if($this->tenYearsAgo == false){
            $timespan = strtotime("-10 years", $this->maxDate);
            $u = strftime("%u", $timespan);
            $dateFrom = strftime("%F 00:00:00", $timespan);
            //if this is a sun or a sat, go back one
            if($u == "6"){
                $timespan = $timespan - (24*60*60);
                $dateFrom = strftime("%F 00:00:00", $timespan);
            }
            if($u == "7"){
                $timespan = $timespan - (24*60*60*2);
                $dateFrom = strftime("%F 00:00:00", $timespan);
            }

            $this->tenYearsAgo = strtotime($dateFrom);
        }

        $ago = $this->tenYearsAgo;

        if($timestamp == false){
            //$ago = $ago - (24 * 60 * 60);
            //$ago = $ago - (24*60*60*365);
            return strftime("%F 00:00:00", $ago);
        } else {
            return $ago;//strtotime(strftime("%F 00:00:00", $ago) . " -1 day");
        }

        //the old.

        if($timestamp == false){
            //$ago = $ago - (24 * 60 * 60);
            $ago = strtotime("-11 years", $this->maxDate);
            return strftime("%Y-12-31 00:00:00", $ago);
        } else {
            $ago = strtotime("-10 years", $this->maxDate);
            return strtotime(strftime("%Y-01-01 00:00:00", $ago) . " -1 day");
        }

    }

    public function getStrategySelectFields(){
        $selections = [
            'region', 'currency', 'shortName', 'code', 'factor', 'longName',
            'indexSource', 'style', 'volTarget', 'provider', 'assetClass', 'id',
            'is_suspended', 'returnType', 'returnCategory', 'type',
        ];
        $selections = implode(", ", $selections);
        return DB::raw($selections);
    }

    public function getSelectFields(){
        $selections = [
            'region', 'currency', 'shortName', 'code', 'factor', 'longName',
						'indexSource', 'style', 'volTarget', 'provider', 'assetClass', 'id',
						'is_suspended',
        ];
        $selections = implode(", ", $selections);
        return DB::raw($selections);
    }

    public function sd_square($x, $mean) { return pow($x - $mean,2); }

    public function sd($array) {
        // Function to calculate square of value - mean
        return sqrt(array_sum(array_map(array($this, "sd_square"), $array, array_fill(0,count($array), (array_sum($array) / count($array)) ) ) ) / (count($array)-1) );
    }

    public function date_compare($a, $b){
        $t1 = strtotime($a['date']);
        $t2 = strtotime($b['date']);
        return $t1 - $t2;
    }

    public function getValidTime($year, $month, $day, $iteration = 0) {
	      $day = $day - $iteration;
	      if (checkdate($month, $day, $year)) {
	        return strftime('%F 00:00:00', strtotime($year . '-' . $month . '-' . $day));
	      }
	      $iteration = 1;
	      return $this->getValidTime($year, $month, $day, $iteration);
    }

    public function cacheBreakdowns($period = '6m'){

        $weightings = ['equal_weighting', 'weighting'];
        $weightingsCategories = ['assetClass', 'factor'];
        $breakdown = [];
        foreach($weightingsCategories as $weightingsCategory){
            foreach($weightings as $weighting){
                $breakdown[$weightingsCategory][$weighting] = DB::table('portfolio_strategies')
                    ->select('indexinfo.' . $weightingsCategory . ' AS title', DB::raw('SUM(' . $weighting . ') as amount'))
                    ->join('indexinfo', 'indexinfo.id', '=', 'portfolio_strategies.indexinfo_id')
                    ->where([
                        'portfolio_strategies.suspended' => 0,
                        'portfolio_strategies.portfolio_id' => $this->id,
                    ])
                    ->groupBy('indexinfo.' . $weightingsCategory)
                    ->get();
            }
        }

        foreach($breakdown as $breakdownGroup => $data){
            $this->update([$breakdownGroup . 'Breakdown' => json_encode($data)]);
        }
    }

    public function setSuspendedStrategiesAsZeroWeight(){
        $items = $this->hasMany('App\PortfolioStrategy')->where([
            'suspended' => 0,
        ])->join('indexinfo', function($join) {
            $join
                ->on('indexinfo.id', '=', 'portfolio_strategies.indexinfo_id')
                ->where('indexinfo.is_suspended', '=', 1);
        })->select('portfolio_strategies.*')->get();
        $count = 0;
        foreach($items as $item){
            $item->suspended = 1;
            $item->save();
            $count++;
        }
        return $count;
    }

    public function calculateWeightings($period = '6m'){
      $count = $this->activeStrategiesCount;
    	if ($count > 0) {
    		$equalSplit = 100 / $count;
    	} else {
        $equalSplit = 0;
    	}

        // equal weight: 100% split among all active strategies, suspended = 0
    	$collection = $this->hasMany('App\PortfolioStrategy');
      $collection->update(array('equal_weighting' => $equalSplit));

      $collection2 = $this->hasMany('App\PortfolioStrategy')->where([ 'suspended'=>1 ]);
      $collection2->update(array('equal_weighting' => '0'));

      // manual weight: keep anything locked the same, dist the remaining % equal among the rest excluding suspended;
      //this means manually set stuff needs to be locked automatically?

      $manualSetTotal = $this->hasMany('App\PortfolioStrategy')->where([
          'suspended' => 0,
          'locked' => 1
      ])->sum('weighting');

      $remainingTotal = 100-$manualSetTotal;
      $autoSet = $this->hasMany('App\PortfolioStrategy')->where([
          'suspended' => 0,
          'locked' => 0
      ]);
      $autoSetCount = $autoSet->count();
      if($autoSetCount == 0){
          $autoWeight = 0;
          $autoSet->update(array('weighting' => $autoWeight));
      } else {
          $autoWeight = $remainingTotal/$autoSetCount;
          $autoSet->update(array('weighting' => $autoWeight));
      }
      $this->cacheBreakdowns($period);

      $this->calCustomWeighting();
      //
    }

    public function calCustomWeighting() {
      $customRebalancing = json_decode($this->customRebalancing, true);
      if (empty($customRebalancing)) {
        return null;
      }
      $customBaseOptions = $customRebalancing['customBaseOptions'];
      $customAllocation = $customRebalancing['customAllocation'];
      if (count($customAllocation) === 0) {
        return null;
      }
      $strategyChanged = false;
      $correlation = array_values($customAllocation)[0]['correlation'];
      if (count($correlation) !== count($this->activeStrategyCodes)) {
        $strategyChanged = true;
      } else {
        foreach($this->activeStrategyCodes as $code) {
          if (empty($correlation[$code])) {
            $strategyChanged = true;
          }
        }
      }
      if ($strategyChanged) {
        $maxDate = $this->backtesting->getMaxDate($this);
        $periods = $this->backtesting->getPeriods($this, $maxDate);

        $customBaseOptions = array_merge([
          'rebalancing' => 'Monthly',
          'rebalancing_month' => 'Every Month',
          'rebalancing_date' => 'Last Day',
        ], $customBaseOptions);

        $portfolio = $this->replicate();
        $allocationOptions = json_decode($portfolio->allocationOptions); 
        if (empty($allocationOptions)) {
          $allocationOptions = (object)[];
        }
        $allocationOptions->rebalancing = $customBaseOptions['rebalancing'];
        $allocationOptions->rebalancing_month = $customBaseOptions['rebalancing_month'];
        $allocationOptions->rebalancing_date = $customBaseOptions['rebalancing_date'];
        $portfolio->allocationOptions = json_encode($allocationOptions);
        $portfolio->weighting = 'equal';
        $portfolio->id = $this->id;
        $options = $this->backtesting->getPortfolio($portfolio, [
          'maxDate' => $maxDate,
          'periods' => [
            'min' => $periods['min'],
          ],
          'optionsOnly' => true,
        ]);

        $listRebalancing= [];
        $listRebalancingDates = [];
        foreach($options['listRebalancingDates'] as $date) {
          $listRebalancingDates[] = $date;
        }
        $allocationOptions = [
          'maxDate' => $maxDate,
          'periods' => [
            $periods['min'],
          ],
          'listCodes' => $this->activeStrategyCodes,
          'listRebalancingDates' => $listRebalancingDates,
          'rebalancingList' => [],
          'initOption' => $customBaseOptions,
          'init' => true,
        ];
        $fundingOptions = [
          'maxDate' => $maxDate,
          'periods' => [
            $periods['min'],
          ],
          'listCodes' => $this->activeStrategyCodes,
          'listRebalancingDates' => $listRebalancingDates,
          'funding_currency' => [$customBaseOptions['funding_currency']],
          'init' => true,
        ];
        $weightings = $this->backtesting->getAllocation($allocationOptions);
        $fundings = $this->backtesting->getFunding($fundingOptions);
        foreach($weightings as $date => $allocation) {
          $listRebalancing[$date] = $customBaseOptions;
          unset($listRebalancing[$date]['rebalancing']);
          unset($listRebalancing[$date]['rebalancing_month']);
          unset($listRebalancing[$date]['rebalancing_date']);
          unset($listRebalancing[$date]['rebalancing_cost']);
          unset($listRebalancing[$date]['rebalancing']);
          if (!isset($listRebalancing[$date]['funding_weight'])) {
            $listRebalancing[$date]['funding_weight'] = 100;
          }
          if ($listRebalancing[$date]['custom_weighting'] !== $allocation['custom_weighting']) {
            $listRebalancing[$date]['custom_weighting'] = $allocation['custom_weighting'];
            $listRebalancing[$date]['custom_weighting_button'] = true;
            $listRebalancing[$date]['leverage_option_button'] = true;
          } else {
            $listRebalancing[$date]['custom_weighting_button'] = false;
            $listRebalancing[$date]['leverage_option_button'] = false;
          }
          $listRebalancing[$date]['fixed_leverage'] = round(100 - $allocation['fundings'], 2);
          $listRebalancing[$date]['changeCorrelation'] = false;
          $listRebalancing[$date]['funding_rate'] = isset($fundings[$date]) ? $fundings[$date]['funding_rate'] : 0;
          $listRebalancing[$date]['correlation'] = [];
          $codeLength = count($this->activeStrategyCodes);
          foreach($this->activeStrategyCodes as $code) {
            $listRebalancing[$date][$code] = [
              'vol' => $allocation['vol'][$code],
              'risk' => round(100 / $codeLength, 2),
              'weighting' => $allocation['allocation'][$code],
              'maxDD' => $allocation['maxDD'][$code],
              'rebalancing_cost' => $customBaseOptions['rebalancing_cost'],
              'replication_cost' => $customBaseOptions['replication_cost'],
              'volLocked' => 0,
              'riskLocked' => 0,
              'locked' => 0,
            ];
            $listRebalancing[$date]['correlation'][$code] = [];
            foreach($this->activeStrategyCodes as $subCode) {
              $listRebalancing[$date]['correlation'][$code][$subCode] = [
                'locked' => 0,
                'value' => $allocation['correlation'][$code][$subCode],
              ];
            }
          }
        }
        $this->customRebalancing = json_encode([
          'customBaseOptions' => $customBaseOptions,
          'customAllocation' => $listRebalancing,
        ]);
        $this->save();
      }
    }

    public function saveWithDraft(Portfolio $draft){
        $original = $this;
        // portfolio row cols
        foreach($draft->attributes as $key => $value){

            if ($key != 'id' && $key != 'slug' && $key != 'draft_of'){
                $original->$key = $value;
            }
        }
        $original->save();

        // match the strategies...
        $deleted = DB::delete('delete from portfolio_strategies WHERE portfolio_id = ' . $original->id);
        $strategies = PortfolioStrategy::where('portfolio_id', '=', $draft->id)->get();
        foreach ($strategies as $key => $s) {
            $strategy = $s->replicate();
            $strategy->portfolio_id = $original->id;
            $strategy->save();
        }
    }

    public function saveAllocation($allocation){
        $cleaned = [];
        foreach($allocation as $key => $value){
            if(!empty($value)){
                $cleaned[$key] = $value;
            }
        }
        if(!empty($cleaned)){
            $this->allocationOptions = json_encode($cleaned);
            return $this->save();
        }
    }

}
