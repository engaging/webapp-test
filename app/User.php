<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;
use Laravel\Spark\Spark;
use Laravel\Spark\User as SparkUser;
use Laravel\Spark\CanJoinTeams;
use Laravel\Spark\Notification;
use Cmgmyr\Messenger\Models\Message;
use Cmgmyr\Messenger\Models\Participant;
use Cmgmyr\Messenger\Models\Thread;
use App\Connection;
use App\ConnectionRequest;
use App\CommunityMember;
use Helper;
use Mail;

class User extends SparkUser
{

    protected $primaryKey = 'id';

    /**
     * Get the unique identifier for the user.
     *
     * @return mixed
     */
    public function getAuthIdentifier()
    {
        return $this->id;
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
    ];

    protected $with=['preference'];

    use SoftDeletes;
    use CanJoinTeams;

    protected $dates = ['deleted_at'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'authy_id',
        'country_code',
        'phone',
        'card_brand',
        'card_last_four',
        'card_country',
        'billing_address',
        'billing_address_line_2',
        'billing_city',
        'billing_zip',
        'billing_country',
        'extra_billing_information',
        'approval_token',
        'approval_token_salt',
        'right',
        'symphonyAccount',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'trial_ends_at' => 'date',
        'uses_two_factor_auth' => 'boolean',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    public $appends = [
      'fullname',
      'portfoliosCount',
      'unread_count',
      'user_type',
      'community_member',
      'portfolios_list',
      'hasSymphonyAccount',
    ];

    public function getFullnameAttribute(){
      return Helper::capitalize($this->name ?: $this->firstname.' '.$this->lastname);
    }

    public function getUserTypeAttribute(){
      if (!empty($_GET['mimic_user_type'])) {
        return $_GET['mimic_user_type'];
      }
      if ($this->isAdmin()) {
        return 'admin';
      }
      if ($this->isSponsor()) {
        return 'sponsor';
      }
      return 'investor';
    }

    public function getCommunityMemberAttribute(){
      if ($this->isAdmin()) {
        return CommunityMember::where('name', 'engagingenterprises')->first();
      }
      if ($this->hasTeams()) {
        $teamIds = $this->teams->pluck('id')->toArray();
        return CommunityMember::whereIn('team_id', $teamIds)->first();
      }
      return null;
    }

    public function getHasSymphonyAccountAttribute(){
      return $this->symphonyAccount && $this->symphonyAccount->approved;
    }

    /**
     * Get the user subscriptions.
     */
    public function subscriptions()
    {
        return $this->hasMany('Laravel\Cashier\Subscription');
    }

    /**
     * Get the user right record.
     */
    public function right()
    {
        return $this->hasOne('App\UserRight');
    }

    /**
     * Get the user widget record.
     */
    public function widget()
    {
        return $this->hasOne('App\Widget');
    }

    /**
     * Get the user preference record.
     */
    public function preference()
    {
        return $this->hasOne('App\UserPreferences');
    }

    /**
     * Get the user symphony account record.
     */
    public function symphonyAccount()
    {
        return $this->hasOne('App\UserSymphonyAccount');
    }

    /**
     * Get the user regressions record.
     */
    public function regressions()
    {
        return $this->hasMany('App\Regression');
    }

    public function sendWelcomeEmail(){
        $user = $this;
        Mail::send('emails.user.welcome', ['user' => $user], function ($m) use ($user) {
            $m->from('admin@engagingenterprises.co', 'engagingenterprises')
            	->bcc('admin@engagingenterprises.co', 'engagingenterprises')
				->to($user->email, $user->fullname)
				->subject('Thank you for your interest in engagingenterprises');
        });
    }

    public function sendActivateEmail(){
        $user = $this;
        $user->approval_sent = time();
        $user->save();
        Mail::send('emails.user.activate', ['user' => $user], function ($m) use ($user) {
            $m->from('admin@engagingenterprises.co', 'engagingenterprises')
            	->bcc('admin@engagingenterprises.co', 'engagingenterprises')
				->to($user->email, $user->fullname)
				->subject('Welcome to engagingenterprises');
        });
    }

    public function sendPasswordEmail($password){
        $user = $this;
        $user->approval_sent = time();
        $user->save();
        $params = ['user' => $user, 'password' => $password];
        Mail::send('emails.user.password', $params, function ($m) use ($user) {
            $m->from('admin@engagingenterprises.co', 'engagingenterprises')
              ->bcc('admin@engagingenterprises.co', 'engagingenterprises')
        ->to($user->email, $user->fullname)
        ->subject('Welcome to engagingenterprises');
        });
    }

    public function canEditCommunityMember($communityMember){
        return true;
    }

    public function resetApprovalToken(){
        $this->generateApprovalToken();
    }

    public function getSalt(){
        if(empty($this->approval_token_salt)){
            $this->approval_token_salt = Str::random(40);
        }
        return $this->approval_token_salt;
    }

    public function generateApprovalToken(){
        $token = hash_hmac('sha256', $this->getSalt(), $this->email);
        if($this->approval_token != $token){
            $this->approval_token = $token;
            //$this->save();
        }
    }

    public function getApprovalTokenAttribute($value){
        if(empty($value) && $this->approved === false){
            $this->generateApprovalToken();
        }
        return $value;
    }

    public function portfolios(){
        return $this->hasMany('App\Portfolio');
    }

    public function getUnreadCountAttribute(){
        $unread = 0;
        $threads = 0; // TODO fix: (int)Thread::forUserWithNewMessages($this->id)->get()->count();

        $notifications = (int)Notification::where('user_id', '=', $this->id)->where('read', '=', 0)->count();
        $unread = $threads+$notifications;

        return $unread;
    }

    public function getPortfoliosListAttribute(){
        return $this->portfolios()->where(
                'draft_of', '=', null
            )->select(['id','title', 'slug'])->get();
    }

    public function getPortfoliosCountAttribute(){
        return $this->portfolios()->where(
                'draft_of', '=', null
            )->count();
    }

    public function favourites(){
        return $this->hasMany('App\FavouriteSet');
    }

    public function favouriteIndices(){
        return $this->hasMany('App\FavouriteItem')->with(['indexinfo' => function($query){
            $selections = [
                'shortName', 'assetClass', 'id', 'is_suspended', 'factor', 'volTarget', 'currency'
            ];
            $query->select($selections);
        }]);
    }


    public function favouriteIndicesIds(){
        return $this->hasMany('App\FavouriteItem')->pluck('index_id');
    }

    public function requestConnectionToCommunityMember($communityMember){
        if($this->hasConnectionRequestWithCommunityMember($communityMember)){
            $connectionRequest = $this->getConnectionRequestWithCommunityMember($communityMember, 'pending');

            return false;
        }
        $connectionRequest = ConnectionRequest::create([
            'requester_id' => $this->id,
            'community_member_id' => $communityMember->id,
            'status' => "pending"
        ]);

        $connectionRequest->sendAsNotification();
        //send a message to the recipient.

        return true;
    }

    public function hasConnectionRequestWithCommunityMember($communityMember, $status = ''){
        $query = ConnectionRequest::where('requester_id', '=', $this->id)
            ->where('community_member_id', '=', $communityMember->id);

        if(!empty($status)){
            $query->where('status', '=', $status);
        }
        // if($status == 'accepted'){
        //     dd($this->id, $communityMember->id, $query->get()->count(), $status);
        // }


        return ($query->count() > 0);
    }

    public function getConnectionRequestWithCommunityMember($communityMember, $status = ''){
        $query = ConnectionRequest::where('requester_id', '=', $this->id)
            ->where('community_member_id', '=', $communityMember->id);

        if(!empty($status)){
            $query->where('status', '=', $status);
        }

        return $query->first();
    }

    public function isConnectedWithCommunityMember($communityMember){
        if($this->hasConnectionRequestWithCommunityMember($communityMember, 'accepted')){
            return true;
        }
        return false;
    }

    public function canCommunicateWith($otherUser){
        $type = $this->user_type;

        if($type == 'admin'){
            return true;
        }

        if($type == 'investor'){
            if($communityMember = $otherUser->community_member){
                return $this->isConnectedWithCommunityMember($communityMember);
            }
            return false;
        }

        if($type == 'sponsor'){
            if($communityMember = $this->community_member){
                return $otherUser->isConnectedWithCommunityMember($communityMember);
            }
            return false;
        }
    }

    public function isAdmin(){
        if(Spark::developer($this->email)){
            return true;
        }
        return false;
    }

    public function isSponsor(){
        if(!$this->isAdmin() && $this->hasTeams()){
            $communityMembers = CommunityMember::whereIn('team_id', $this->teams->pluck('id')->toArray());
            if($communityMembers->count() > 0){
                return true;
            }
        }
        return false;
    }

    public function isInvestor(){
        if (!$this->isAdmin() && !$this->isSponsor()) {
            return true;
        }
        return false;
    }

}
