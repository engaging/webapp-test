<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class FeedArticleTag extends Model
{

    protected $table = 'feed_article_tags';
    protected $guarded = ['id'];
}
