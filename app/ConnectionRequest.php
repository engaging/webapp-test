<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cmgmyr\Messenger\Models\Message;
use Cmgmyr\Messenger\Models\Participant;
use Cmgmyr\Messenger\Models\Thread;
use DB;
use Carbon\Carbon;
use Laravel\Spark\Notification;
use Ramsey\Uuid\Uuid;

class ConnectionRequest extends Model
{
        //
    protected $fillable = ['requester_id', 'status', 'community_member_id'];
    protected $appends = ['requestingUser'];

    public function communityMember(){
		return $this->belongsTo('App\CommunityMember', 'community_member_id');
	}

	public function requestedBy(){
		return $this->belongsTo('App\User', 'requester_id');
	}

	public function getRequestingUserAttribute(){
		$user = $this->requestedBy()->first();
		return $user;
	}
	
	public function sendAsNotification(){
	    
	    $teamUserIds = $this->communityMember->users()->pluck('id')->toArray();
	    foreach($teamUserIds as $userId){
	        Notification::create(
    	        [
    	            'id' => Uuid::uuid4(),
    	            'user_id' =>  $userId,
    	            'body' => $this->requestingUser->name . ' has requested to connect with you.',
    	            'action_text' => "Respond",
    	            'action_url' => '/settings#connections'
    	        ]
    	    );
	    }
	    
        
        return true;
	}
	
	public function sendAsMessage(){
	    
	    $teamUserIds = $this->communityMember->users()->pluck('id')->toArray();
	    $allUsers = array_merge([$this->requester_id], $teamUserIds);
	    
	    $query = Thread::whereHas('participants', function ($q) use ($allUsers) {
            $q->whereIn('user_id', $allUsers)
                ->select(DB::raw('DISTINCT(thread_id)'))
                ->groupBy('thread_id')
                ->havingRaw('COUNT(thread_id)=' . count($allUsers));
        });
	    
	    if(!$thread =$query->first()){
	        $thread = Thread::create(
                [
                    'subject' => "Connection Request"
                ]
            );
	    }
	    
        // Message
        Message::create(
            [
                'thread_id' => $thread->id,
                'user_id'   => $this->requester_id,
                'body'      => "I would like to connect with you.",
            ]
        );

        // Sender
        Participant::create(
            [
                'thread_id' => $thread->id,
                'user_id'   => $this->requester_id,
                'last_read' => new Carbon,
            ]
        );

        // Recipients
        foreach($teamUserIds as $userId){
            $thread->addParticipant($userId);
        }
        
        return $thread;
	}
}
