<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Connection extends Model
{
    
	public function connectedUser(){
		return $this->belongsTo('App\User', 'user_id');
	}

}