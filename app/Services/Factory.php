<?php

namespace App\Services;

interface Factory {

  static function make(string $name);

}
