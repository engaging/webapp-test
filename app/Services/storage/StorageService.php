<?php

namespace App\Services\Storage;

interface StorageService {

  function uploadFile($file, $key);
  function uploadDataURL($dataURL, $key);
  function getSignedUrl($operation, $params, $expires = '+15 minutes');

}
