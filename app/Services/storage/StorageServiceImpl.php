<?php

namespace App\Services\Storage;
use Illuminate\Support\Facades\Storage;
use App\Services\Constant;
use AWS;
use Aws\Command;

class StorageServiceImpl implements StorageService, Constant {

  /**
   * Private variables
   */

  protected $s3Client;

  /**
   * Public constructor
   */

  public function __construct(){
    $this->s3Client = AWS::createClient('s3', [
      'region' => 'us-east-1',
    ]);
  }

  /**
   * Public functions
   */

  function uploadFile($file, $key) {
    $disk = env('UPLOAD_BUCKET') ? 's3-upload' : 'public';
    $folder = ($disk !== 'public') ? env('UPLOAD_FOLDER', 'uploads-dev') . '/' : '';
    $storage = Storage::disk($disk);

    $key = $folder . $key;
    $storage->put($key, file_get_contents($file->getRealPath()));
    $url = $storage->url($key);
    if ($disk === 'public') {
      $url = asset($url);
    } else {
      $url = Str_replace('s3.amazonaws.com/', '', $url);
    }
    return $url;
  }

  function uploadDataURL($dataURL, $key) {
    $disk = env('UPLOAD_BUCKET') ? 's3-upload' : 'public';
    $folder = ($disk !== 'public') ? env('UPLOAD_FOLDER', 'uploads-dev') . '/' : '';
    $storage = Storage::disk($disk);

    $data = $this->decodeDataURL($dataURL);
    $key = $folder . $key . '.' . $data['ext'];
    $storage->put($key, $data['bytes']);
    $url = $storage->url($key);
    if ($disk === 'public') {
      $url = asset($url);
    } else {
      $url = Str_replace('s3.amazonaws.com/', '', $url);
    }
    return $url;
  }

  function getSignedUrl($operation, $params, $expires = '+15 minutes') {
    $command = $this->s3Client->getCommand($operation, $params);
    $request = $this->s3Client->createPresignedRequest($command, $expires);
    $url = $request->getUri()->__toString();
    $url = Str_replace('s3.amazonaws.com/', '', $url);
    return $url;
  }

  /**
   * Private functions
   */

  protected function decodeDataURL($dataURL) {
    // separate desc and data components
    list($desc, $data) = explode(',', $dataURL);
    // convert base64/URLEncoded data component to raw binary data held in a string
    $isBase64 = strpos($desc, 'base64') !== false;
    $bytes = $isBase64 ? base64_decode($data) : rawurldecode($data);
    // separate out the MIME component
    $type = explode(';', explode(':', $desc)[1])[0];
    // separate out the file extension
    $ext = explode('/', $type)[1];
    return [
      'bytes' => $bytes, // raw binary data held in a string
      'type' => $type, // MIME type
      'ext' => $ext, // file extension
    ];
  }

}
