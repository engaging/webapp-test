<?php

namespace App\Services\Bot;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use GuzzleHttp\Client as GuzzleHttpClient;
use App\Services\Constant;
use Exception;
use Helper;
use AWS;

class BotSymphonyServiceImpl implements BotService, Constant {

  public function __construct(){
    $this->lambdaClient = AWS::createClient('lambda', [
      'region' => 'eu-west-1',
    ]);
  }

  /**
   * Public functions
   */

  function sendMessage(string $message, array $options = []) {
    $options = array_merge([
      'attachment' => null,
    ], $options);

    $account = Auth::user()->symphonyAccount;
    if (!$account || !$account->approved) {
      throw new Exception('No Symphony account linked');
    }

    $body = [
      'email' => $account->account_email,
      'message' => $message,
    ];

    if ($attachment = $options['attachment']) {
      $body['bucket'] = env('BUCKET_SYMPHONY');
      $body['key'] = $attachment['key'];
      Storage::disk('s3-symphony')->put($body['key'], $attachment['file']);
    }

    $client = new GuzzleHttpClient();
    $client->post(env('API_SYMPHONY').'/v1/messages/send', [
      'json' => $body,
    ]);
  }

  function sendConstituents(string $code, array $options = []) {
    $user = Auth::user();
    $account = $user->symphonyAccount;
    if (!$account || !$account->approved) {
      throw new Exception('No Symphony account linked');
    }

    $name = Helper::capitalize($user->firstname) ?: Helper::capitalize($user->name);
    $message = "Hi $name. Thank you for requesting constituents for the strategy <b>$code</b>. Your constituents file is attached below.";

    $options = array_merge([
      'pastDays' => 1,
      'message' => $message,
      'destinations' => [
        $account->account_email => [
          'listCodes' => [$code],
          'pastDays' => 1,
        ],
      ],
    ], $options);

    $this->lambdaClient->invoke([
      'FunctionName' => 'datafeed-prod-symphony-file-send',
      'Payload' => json_encode(['config' => $options]),
    ]);
  }

}
