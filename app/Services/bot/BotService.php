<?php

namespace App\Services\Bot;

interface BotService {

  function sendMessage(string $message, array $options = []);
  function sendConstituents(string $code, array $options = []);

}
