<?php

namespace App\Services\Bot;

use App\Services\Factory;
use Exception;

class BotFactory implements Factory {

  static function make(string $name): BotService {
    switch ($name) {
      case 'symphony':
        return new BotSymphonyServiceImpl();
      default:
        throw new Exception('No service associated with ' . $name . ' name');
    }
  }

}
