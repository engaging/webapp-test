<?php

namespace App\Services;

interface Constant {

  const NONE = 'None';
  const SEC_IN_DAY = 86400; // seconds in a day
  const ZERO_DATE = '0000-00-00'; // zero date sql value
  const MIN_DATE = '2000-01-04'; // hard min date (disable by setting to self::ZERO_DATE)
  const REG_MIN_DATE = '2008-04-25'; // regression min date (disable by setting to self::ZERO_DATE)
  const DEC_IN = 4; // float precision decimals for input
  const DEC_OUT = 2; // float precision decimals for output
  const PERIOD_MAP = [
    '6m' => '6 Month',
    '1y' => '1 Year',
    '3y' => '3 Year',
    '5y' => '5 Year',
    '10y' => '10 Year',
    'live' => 'Live',
  ];
  const METRIC_MAP = [ // 'sourceKey' => [factor, 'targetKey', 'label']
    'returnCum'        => [100, 'returnCum'   , 'Return'     ], // Return       %
    'return'           => [100, 'return'      , 'Return p.a.'], // Return p.a.  %
    'volatility'       => [100, 'volatility'  , 'Volatility' ], // Volatility   %
    'sharpe'           => [  1, 'sharpe'      , 'Sharpe'     ], // Sharpe
    'skew'             => [  1, 'skewness'    , 'Skewness'   ], // Skewness
    'kurtosis'         => [  1, 'kurtosis'    , 'Kurtosis'   ], // Kurtosis
    'calmar'           => [  1, 'calmar'      , 'Calmar'     ], // Calmar
    'omega'            => [  1, 'omega'       , 'Omega'      ], // Omega
    'sortino'          => [  1, 'sortino'     , 'Sortino'    ], // Sortino
    'var'              => [100, 'var'         , 'VaR'        ], // VaR          %
    'mvar'             => [100, 'mvar'        , 'ModVaR'     ], // ModVaR       %
    'cvar'             => [100, 'cvar'        , 'CVaR'       ], // CVaR         %
    'avgDD'            => [100, 'avg_drawdown', 'Avg DD'     ], // Avg DD       %
    'maxDD'            => [100, 'mdd'         , 'Max DD'     ], // Max DD       %
    'stressTest'       => [100, 'worst20d'    , 'Worst 20d'  ], // Worst 20d    %
    'percentPosWeeks'  => [100, 'posWeeks'    , '%Up Week'   ], // %Up Week     %
    'percentPosMonths' => [100, 'posMonths'   , '%Up Month'  ], // %Up Month    %
  ];
  const DEFAULT_ALLOCATION = [
    // general options
    'rebalancing' => 'Monthly', // ['Monthly', 'Quarterly', 'Semi-annual', 'Annual']
    'rebalancing_month' => 'Every Month', // ['Every Month', ...]
    'rebalancing_date' => 'Last Day', // ['Last Day', '1', ..., '30']
    'rebalancing_cost' => null, // number
    'replication_cost' => null, // number
    'leverage_type' => self::NONE, // ['NONE', 'Target Volatility', 'Fixed Leverage']
    'target_volatility' => null, // number
    'target_interval' => '30d', // ['30d', '60d', '120d', '250d', '500d', '750d', '1250d']
    'fixed_leverage' => null,  // number
    'leverage_min' => null, // number
    'leverage_max' => null, // number
    'funding_currency' => self::NONE, // ['None', USD', 'EUR']
    'borrowing_spread' => null, // number
    'lending_spread' => null, // number
    // specific options
    'volatility_interval' => '30d', // ['30d', '60d', '120d', '250d', '500d', '750d', '1250d']
    'allocationConstraints' => [
      'primary' => [
        'type' => self::NONE,
        // 'type' => self::STRATEGY,
        // 'allocationsMin' => [
        //   self::STRATEGY => 20,
        // ],
        // 'allocationsMax' => [
        //   self::STRATEGY => 40,
        // ],
      ],
      'secondary' => [
        'type' => self::NONE,
        // 'type' => self::ASSET_CLASS,
        // 'allocationsMin' => [
        //   self::ASSET_CLASS_COMMODITY => 10,
        //   self::ASSET_CLASS_CREDIT => 20,
        //   self::ASSET_CLASS_EQUITY => 30,
        //   self::ASSET_CLASS_FIXED_INCOME => 40,
        //   self::ASSET_CLASS_FOREIGN_EXCHANGE => 50,
        //   self::ASSET_CLASS_MULTI_ASSETS => 60,
        // ],
        // 'allocationsMax' => [
        //   self::ASSET_CLASS_COMMODITY => 10,
        //   self::ASSET_CLASS_CREDIT => 20,
        //   self::ASSET_CLASS_EQUITY => 30,
        //   self::ASSET_CLASS_FIXED_INCOME => 40,
        //   self::ASSET_CLASS_FOREIGN_EXCHANGE => 50,
        //   self::ASSET_CLASS_MULTI_ASSETS => 60,
        // ],
        // 'type' => self::FACTOR,
        // 'allocationsMin' => [
        //   self::FACTOR_CARRY => 10,
        //   self::FACTOR_VOLATILITY => 20,
        //   self::FACTOR_MOMENTUM => 30,
        //   self::FACTOR_VALUE => 40,
        //   self::FACTOR_LOW_VOL => 50,
        //   self::FACTOR_QUALITY => 60,
        //   self::FACTOR_SIZE => 70,
        //   self::FACTOR_MULTI_FACTOR => 80,
        // ],
        // 'allocationsMax' => [
        //   self::FACTOR_CARRY => 10,
        //   self::FACTOR_VOLATILITY => 20,
        //   self::FACTOR_MOMENTUM => 30,
        //   self::FACTOR_VALUE => 40,
        //   self::FACTOR_LOW_VOL => 50,
        //   self::FACTOR_QUALITY => 60,
        //   self::FACTOR_SIZE => 70,
        //   self::FACTOR_MULTI_FACTOR => 80,
        // ],
      ],
    ],
  ];

}
