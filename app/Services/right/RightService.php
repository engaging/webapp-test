<?php

namespace App\Services\Right;
use App\User;
use App\CommunityMember;

interface RightService {

  function createInvestorRight(User $user, bool $upsert = false);
  function createSponsorRight(User $user, CommunityMember $community, bool $upsert = false);
  function setPasswordAndCreateInvestorRight(User $user, string $password);

  function hasCommunityMemberRight(User $user, CommunityMember $community);
  function hasStrategyRight(User $user, &$index, $scope = 'read');
  function hasPortfolioRight(User $user, &$portfolio);
  function filterAuthorizedStrategies(User $user, array &$indices, $scope = 'read');
  function getAuthorizedStrategies(User $user, array $options = []);

}
