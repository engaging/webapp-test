<?php

namespace App\Services\Right;
use App\Services\Constant;
use App\User;
use App\UserRight;
use App\CommunityMember;
use App\Helpers\StrategyHelper;
use Throwable;
use Exception;
use Helper;
use Log;
use function Tarsana\Functional\{ curry, filter };

class RightServiceImpl implements RightService, Constant {

  const SCOPE_KEY_MAP = [
    'read' => 'strategy_restriction',
    'export' => 'strategy_export_restriction',
  ];
  const STRATEGY_EXPORT_RESTRICTION = [
    'provider' => [
      'type' => 'exclude',
      'list' => ['Platinum Rocks'],
    ],
    'type' => [
      'type' => 'exclude',
      'list' => ['Pure Factor', 'Benchmark'],
    ],
  ];
  const RIGHT_ALREADY_CREATED = 'User right already created. To update use the upsert mode.';

  /**
   * Public functions
   */

  function createInvestorRight(User $user, bool $upsert = false){
    $right = $user->right()->first();
    if (!$right) {
      $right = new UserRight();
    } else if (!$upsert) {
      throw new Exception(self::RIGHT_ALREADY_CREATED);
    }

    $right->community_my_page = 1; // default
    $right->community_am_page = 1; // default
    $right->community_is_page = 1; // default
    $right->strategy_restriction = 'null'; // default
    $right->strategy_export_restriction = json_encode(self::STRATEGY_EXPORT_RESTRICTION);
    $user->right()->save($right);
  }

  function createSponsorRight(User $user, CommunityMember $community, bool $upsert = false){
    $right = $user->right()->first();
    if (!$right) {
      $right = new UserRight();
    } else if (!$upsert) {
      throw new Exception(self::RIGHT_ALREADY_CREATED);
    }

    switch ($community->type) {
      case 'Asset Manager':
        $right->community_my_page = 1; // default
        $right->community_am_page = 0; // disallow other asset manager pages
        $right->community_is_page = 0; // disallow index sponsor pages
        $right->strategy_restriction = 'null'; // default
        $right->strategy_export_restriction = json_encode(self::STRATEGY_EXPORT_RESTRICTION);
        break;

      case 'Index Sponsor':
        $right->community_my_page = 1; // default
        $right->community_am_page = 0; // disallow asset manager pages
        $right->community_is_page = 0; // disallow other index sponsor pages
        $strategy_restriction = [
          'provider' => [
            'type' => 'include',
            'list' => [
              $community->name, // allow only strategies from this index sponsor
            ],
          ],
        ];
        $right->strategy_restriction = json_encode($strategy_restriction);
        $right->strategy_export_restriction = json_encode(array_merge(self::STRATEGY_EXPORT_RESTRICTION, $strategy_restriction));
        break;
    }
    $user->right()->save($right);
  }

  function setPasswordAndCreateInvestorRight(User $user, string $password){
    try {
      $this->createInvestorRight($user);
    } catch (Throwable $e) {
      if ($e->getMessage() === self::RIGHT_ALREADY_CREATED) {
        Log::warning('Skipped right creation because it already exists, setting password now.');
      } else {
        throw $e;
      }
    }

    $user->forceFill([
      'password' => bcrypt($password),
      'approved' => 1,
    ])->save();
  }

  function hasCommunityMemberRight(User $user, CommunityMember $community){
    if ($user->isAdmin()) return true;
    if (!$right = $user->right) return false;

    if ($right->community_is_page === 1 && $community->type === 'Index Sponsor') {
      return true;
    }
    if ($right->community_am_page === 1 && $community->type === 'Asset Manager') {
      return false; // asset managers viewed by admin only
    }
    if ($right->community_my_page === 1 && $community->isMember($user)) {
      return $community->type !== 'Asset Manager'; // asset managers viewed by admin only
    }
    return false;
  }

  function hasStrategyRight(User $user, &$index, $scope = 'read'){
    if (!$user->isAdmin()) // even admins need to check private strategy ownership
    if (!$right = $user->right) return false;

    $strategyRestriction = $this->getStrategyRestriction($user, $scope);
    return $this->checkStrategyRight($user, $strategyRestriction, $index);
  }

  function hasPortfolioRight(User $user, &$portfolio) {
    // even admins need to check portfolio ownership
    return ($portfolio->user_id === $user->id);
  }

  function filterAuthorizedStrategies(User $user, array &$indices, $scope = 'read'){
    if (!$user->isAdmin()) // even admins need to check private strategy ownership
    if (!$right = $user->right) return [];

    if (empty($indices)) return [];
    $strategyRestriction = $this->getStrategyRestriction($user, $scope);
    $checkStrategyRight = curry(function(User $user, $strategyRestriction, &$index) {
      return $this->checkStrategyRight($user, $strategyRestriction, $index);
    });
    $filterAuthorizedStrategies = filter($checkStrategyRight($user, $strategyRestriction));
    return $filterAuthorizedStrategies($indices);
  }

  function getAuthorizedStrategies(User $user, array $options = []){
    $indices = StrategyHelper::getBaseQuery($user, $options)->get()->all();;
    return collect($this->filterAuthorizedStrategies($user, $indices));
  }

  /**
   * Private functions
   */

   protected function getStrategyRestriction(User $user, $scope = 'read'){
     if (!isset($user->right)) return [];
     $restrictionKey = self::SCOPE_KEY_MAP[$scope];
     return json_decode($user->right->$restrictionKey, true) ?: [];
   }

    protected function checkRestriction($value, $restriction){
      switch ($restriction['type']) {
        case 'include':
          return in_array($value, $restriction['list']);
        case 'exclude':
          return !in_array($value, $restriction['list']);
      }
    }

   protected function checkStrategyRight(User $user, $strategyRestriction, &$index){
     $hasAccess = true;
     $strategyType = Helper::get($index, 'type');

     switch ($strategyType) {
       case 'Strategy':
         if (!$user->isAdmin() && !empty($strategyRestriction)) {
           foreach ($strategyRestriction as $checkedKey => $restriction) {
              $value = Helper::get($index, $checkedKey);
              $hasAccess = $hasAccess && $this->checkRestriction($value, $restriction);
           }
         }
         break;

       case 'Benchmark':
       case 'Pure Factor':
          if (!$user->isAdmin() && !empty($strategyRestriction)) {
            $checkedKey = 'type';
            if ($restriction = Helper::get($strategyRestriction, $checkedKey)) {
              $value = Helper::get($index, $checkedKey);
              $hasAccess = $hasAccess && $this->checkRestriction($value, $restriction);
            }
          }
          if ($strategyType === 'Pure Factor') {
            $hasAccess = $hasAccess && ($user->isAdmin() || $user->subscriptions->contains(function($i, $o) {
              return in_array($o->name, ['Standard', 'Premium', 'Premium + Custom Model']);
            }));
          }
          break;

       case 'Private Strategy':
         $owner = Helper::get($index, 'owner');
         $hasAccess = $hasAccess && ($owner === Helper::get($user->currentTeam, 'id'));
         break;
     }
     return $hasAccess;
   }

}
