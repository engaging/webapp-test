<?php

namespace App\Services\Lab;
use App\Portfolio;

interface PCAService {

  function calcul(Portfolio $portfolio, array $options = []);

}
