<?php

namespace App\Services\Lab;

interface RegressionService {

  function calcul($indexOrPortfolio, array $options = []);
  function getRegression($indexOrPortfolio, array $options = []);
  function getHeatmap($indexOrPortfolio, array $options = []);

}
