<?php

namespace App\Services\Lab;
use Illuminate\Support\Facades\Auth;
use App\Services\Right\RightService;
use App\Services\Constant;
use App\PortfolioStrategy;
use App\Indexinfo;
use App\Portfolio;
use App\Setting;
use App\Team;
use Throwable;
use Exception;
use Helper;
use Mail;
use Log;
use DB;
use function Tarsana\Functional\{ __, pipe, map, filter };

class BacktestingServiceImpl extends AbstractLabService implements BacktestingService, Constant {

  /**
   * Public constructor
   */

  public function __construct(RightService $right){
    $this->right = $right;
  }

  /**
   * Public functions
   */

  function calcul(array $options) {
    // check calcul options
    $this->checkInput($options);

    // get calcul input
    $input = $this->getBacktestingInput($options);

    // invoke calcul lambda
    $output = $this->invokeCalculLambda($input); // to array

    // handle lambda error
    $this->checkOutput($output);

    return $output;
  }

  function getStrategy(Indexinfo $index, array $options = []) {
      $index = clone($index);
      $options = array_merge([
        'maxDate' => null,
        'periods' => null,
        'period' => '*',
        'volMeasure' => '30d',
        'metricsPrefix' => '',
        'metricsOnly' => false,
        'fullTracks' => false,
        'noFactor' => false,
        'noLive' => false,
        'noMin' => true,
        'cache' => null, // options: ignoreCache or reCache
      ], $options);

      // check user right
      if (!$this->right->hasStrategyRight(Auth::user(), $index)) {
        throw new Exception('Restricted access');
      }

      // get maxDate and periods
      $maxDate = $this->getMaxDate($index, $options['maxDate']);
      $periods = $this->getPeriods($index, $maxDate, $options['periods'], $options['noLive']);

      // calcul strategy
      $assetCal = $this->calcul([
        'listCodes' => [$index->code],
        'listStartDates' => array_values($periods),
        'endDate' => $maxDate,
        'cache' => $options['cache'],
      ]);

      // set skipped periods to inactive and skip mapping
      if (!empty($assetCal['skipped'])) {
        $skipFrom = min($assetCal['skipped']);
        $skipPeriods = array_slice($periods, $skipFrom);
        foreach ($skipPeriods as $periodKey => $minDate) {
          if ($periodKey !== 'min' || $options['noMin'] !== true) {
            $index[$periodKey . '_active'] = 0;
          }
        }
        $periods = array_slice($periods, 0, $skipFrom);
      }

      // map metrics units
      foreach (self::METRIC_MAP as $sourceKey => list($factor, $targetKey)) {
        if ($options['noFactor'] === true) $factor = 1;
        $index[self::UNIT_PREFIX . $targetKey] = self::UNIT_MAP[abs($factor)];
      }

      // add fields per periods
      $i = 0;
      $dec = ($options['noFactor'] === true) ? self::DEC_IN : self::DEC_OUT;
      foreach ($periods as $periodKey => $minDate) {
        if ($options['noMin'] === false || $periodKey !== 'min') {
          $suffix = '_' . $i;
          $prefix = $periodKey . '_';
          $dataPrefix = $options['metricsPrefix'] . $prefix;

          // set active period
          $index[$prefix . 'active'] = 1;

          // map metrics fields
          foreach (self::METRIC_MAP as $sourceKey => list($factor, $targetKey)) {
            if ($options['noFactor'] === true) $factor = 1;
            $index[$dataPrefix . $targetKey] = round($factor * $assetCal[$sourceKey . $suffix], $dec);
          }

          // map starts and ends dates
          $dates = $assetCal['timeseriesDates' . $suffix];
          $index[$prefix . 'starts'] = strftime('%F %T', Helper::excel2time(reset($dates)));
          $index[$prefix . 'ends'] = strftime('%F %T', Helper::excel2time(end($dates)));

          if (in_array($options['period'], ['*', $periodKey]) && $options['metricsOnly'] === false) {
            // map timeseries to tracks
            $dateTrack = [];
            $equityTrack = [];
            $drawdownTrack = [];
            $volatilityTrack = [];
            $sample = $this->getSample($minDate, $maxDate, $options['fullTracks']);
            $this->fillTracks($assetCal, $dates, $dateTrack, $equityTrack, $drawdownTrack, $volatilityTrack, $sample, $suffix, $options['volMeasure'], $options['noFactor']);
            $index[$prefix . 'date_track'] = $dateTrack;
            $index[$prefix . 'track'] = $equityTrack;
            $index[$prefix . 'drawdown_track'] = $drawdownTrack;
            $index[$prefix . 'volatility_track_' . $options['volMeasure']] = $volatilityTrack;

            // map monthly returns per years
            $years = [];
            $this->fillMonthlyReturns($assetCal, $suffix, $years, $options['noFactor']);
            $index[$prefix . 'monthly_returns'] = $years;
          }
        }
        // increment i
        $i++;
      }

      Helper::debug('object', $index);
      return $index;
  }

  function getAllocation(array $options = []) {
    Helper::debug('options', $options);
    $calculOpt = $this->getAllocationOption($options);
    $this->checkInput($calculOpt);
    $input = $this->getAllocationInput($calculOpt);
    $jsonInput = json_encode($input);
    $jsonOutput = $this->invokeLambda([
      'FunctionName' => env('SLS_ALOC_FUNC'),
      'Payload' => $jsonInput,
    ]);
    $output = json_decode($jsonOutput, true);
    $results = [];
    foreach ($output['rebalancingDates'] as $dateIdx => $date) {
      $formatDate = strftime('%F', Helper::excel2time($date));
      $allocation = [];
      $cor = [];
      $vol = [];
      $maxDD = [];
      $allocationModel = 'equal';
      switch ($output['allocationModel'][$dateIdx]) {
        case -1: $allocationModel = 'manual'; break;
        case 0:  $allocationModel = 'equal';  break; // Fixed Equity
        case 1:  $allocationModel = 'risk';  break; // Vol Risk Parity
        case 2:  $allocationModel = 'erc';  break; // Equal Risk Contribution
      }
      foreach ($options['listCodes'] as $idx => $code) {
        $allocation[$code] = round($output['allocation'][$dateIdx][$idx] * 100, self::DEC_OUT);
        if (count($output['vol'][$dateIdx]) === 0) {
          $vol[$code] = 0;
          $maxDD[$code] = 0;
          $cor[$code] = [];
          foreach ($options['listCodes'] as $subIdx => $subCode) {
            if ($subCode === $code) {
              $cor[$code][$subCode] = 1;
            } else {
              $cor[$code][$subCode] = 0;
            }
          }
        } else {
          $vol[$code] = round($output['vol'][$dateIdx][$idx] * 100, self::DEC_OUT);
          $maxDD[$code] = round($output['maxDD'][$dateIdx][$idx] * 100, self::DEC_OUT);
          $cor[$code] = [];
          foreach ($options['listCodes'] as $subIdx => $subCode) {
            $cor[$code][$subCode] = round($output['correl'][$dateIdx][$idx][$subIdx], self::DEC_OUT);
          }
        }
      }
      $results[$formatDate] = [
        'allocation' => $allocation,
        'correlation' => $cor,
        'fundings' => round($output['fundings'][$dateIdx] * 100, self::DEC_OUT),
        'vol' => $vol,
        'maxDD' => $maxDD,
        'custom_weighting' => $allocationModel,
      ];
    }
    if (isset($options['warning']) && $options['warning'] === true) {
      $results['warningCodes'] = $output['warningCodes'];
    }
    return $results;
  }
  function getFunding(array $options = []) {
    Helper::debug('options', $options);
    $calculOpt = $this->getFundingOption($options);
    $this->checkInput($calculOpt);
    $input = $this->getAllocationInput($calculOpt);
    $jsonInput = json_encode($input);
    $jsonOutput = $this->invokeLambda([
      'FunctionName' => env('SLS_FUND_FUNC'),
      'Payload' => $jsonInput,
    ]);
    $output = json_decode($jsonOutput, true);
    $results = [];
    foreach ($output['rebalancingDates'] as $dateIdx => $date) {
      $formatDate = strftime('%F', Helper::excel2time($date));
      $results[$formatDate] = [
        'funding_rate' => round($output['fundingRates'][$dateIdx] * 100, self::DEC_OUT),
      ];
    }
    return $results;
  }

  function getPortfolio(Portfolio $portfolio, array $options = []) {
      $portfolio = clone($portfolio);
      $options = array_merge([
        'maxDate' => null,
        'periods' => null,
        'period' => '*',
        'volMeasure' => '30d',
        'rebalancingWeights' => 'last', // options: last or all
        'metricsOnly' => false,
        'optionsOnly' => false,
        'fullTracks' => false,
        'noFactor' => false,
        'noLive' => false,
        'noMin' => true,
        'cache' => null, // options: ignoreCache or reCache
      ], $options);

      // check user right
      if (!$this->right->hasPortfolioRight(Auth::user(), $portfolio)) {
        throw new Exception('Restricted access');
      }

      // get active strategies and portfolioStrategies
      [$strategies, $portfolioStrategies] = $this->getActiveStrategiesAndPortfolioStrategies($portfolio);

      // get maxDate and periods
      $maxDate = $this->getMaxDate($portfolio, $options['maxDate']);
      $periods = $this->getPeriods($strategies, $maxDate, $options['periods'], $options['noLive']);

      // get portfolio options
      $calculOpt = $this->getPortfolioOptions($portfolio, $portfolioStrategies, $strategies, $periods, $maxDate);
      // Helper::json_dump($calculOpt);
      $calculOpt['cache'] = $options['cache'];
      if ($options['optionsOnly'] === true) {
        return $calculOpt;
      }

      // calcul portfolio
      $portfolioCal = $this->calcul($calculOpt);
      // dd($portfolioCal);

      // set skipped periods to inactive and skip mapping
      if (!empty($portfolioCal['skipped'])) {
        $skipFrom = min($portfolioCal['skipped']);
        $skipPeriods = array_slice($periods, $skipFrom);
        foreach ($skipPeriods as $periodKey => $minDate) {
          if ($periodKey !== 'min' || $options['noMin'] !== true) {
            $portfolio[$periodKey . '_active'] = 0;
          }
        }
        $periods = array_slice($periods, 0, $skipFrom);
      }

      // map metrics units
      foreach (self::METRIC_MAP as $sourceKey => list($factor, $targetKey)) {
        if ($options['noFactor'] === true) $factor = 1;
        $portfolio[self::UNIT_PREFIX . $targetKey] = self::UNIT_MAP[abs($factor)];
      }

      // add fields per periods
      $i = 0;
      $fac = ($options['noFactor'] === true) ? 1 : 100;
      $dec = ($options['noFactor'] === true) ? self::DEC_IN : self::DEC_OUT;
      $lastIdx = count($periods) - 1;
      if ($options['noMin'] === true && array_keys($periods)[count($periods) - 1] === 'min') {
        $lastIdx--;
      }
      foreach ($periods as $periodKey => $minDate) {
        if ($options['noMin'] === false || $periodKey !== 'min') {
          $suffix = '_' . $i;
          $prefix = $periodKey . '_';

          if ($i === $lastIdx) {
            // map rebalancing dates, allocations and weights
            $rebalancingDates = [];
            $portfolioAllocations = [];
            $portfolioWeights = [];
            $fundingAllocations = [];
            $fundingWeights = [];
            $strategyAllocations = [];
            $strategyWeights = [];
            $borrowingWeights = [];
            $lendingWeights = [];
            $assetClassWeights = [];
            $factorWeights = [];
            $listCodes = $calculOpt['listCodes'];
            $strategyCount = count($listCodes);
            $rebalancingWeights = $portfolioCal['weights' . $suffix];
            $rebalancingCount = count($rebalancingWeights);
            $j = 0;
            $addItem = function(&$src, $val) { $src[] = $val; };
            if ($options['rebalancingWeights'] === 'last') {
              $j = $rebalancingCount - 1;
              $addItem = function(&$src, $val) { $src = $val; };
            }
            for (; $j < $rebalancingCount; $j++) {
              $weights = $rebalancingWeights[$j];
              $weightsCount = count($weights);
              $sum = 0;
              foreach ($weights as $idx => $val) {
                if ($idx > 0 && $idx <= $strategyCount) {
                  $sum += $val;
                }
              }
              $addItem($portfolioAllocations, round($sum * $fac, $dec));
              $addItem($portfolioWeights, round(1 * $fac, $dec));
              $fundingVal = $portfolioCal['fundingWeights' . $suffix][$j];
              $addItem($fundingAllocations, round($fundingVal * $fac, $dec));
              $addItem($fundingWeights, round($fundingVal * $fac / $sum, $dec));
              foreach ($weights as $idx => $val) {
                if ($idx === 0) {
                  $addItem($rebalancingDates, strftime('%F', Helper::excel2time($val)));
                } else if ($idx <= $strategyCount) {
                  $code = $listCodes[$idx - 1];
                  $addItem($strategyAllocations[$code], round($val * $fac, $dec));
                  $addItem($strategyWeights[$code], round($val * $fac / $sum, $dec));
                } else if ($idx === $weightsCount - 2) {
                  $addItem($borrowingWeights, round($val * $fac, $dec));
                } else {
                  $addItem($lendingWeights, round($val * $fac, $dec));
                }
              }
            }
            $weightsAssetClasses = end($portfolioCal['weightsAssetClasses' . $suffix]);
            foreach ($weightsAssetClasses as $idx => $val) {
              if ($idx > 0 && $val > 0) $assetClassWeights[] = [
                'title' => self::ASSET_CLASSES[$idx - 1],
                'amount' => round($val * $fac, $dec),
              ];
            }
            $weightsFactors = end($portfolioCal['weightsFactors' . $suffix]);
            foreach ($weightsFactors as $idx => $val) {
              if ($idx > 0 && $val > 0) $factorWeights[] = [
                'title' => self::FACTORS[$idx - 1],
                'amount' => round($val * $fac, $dec),
              ];
            }
            $portfolio['listCodes'] = $listCodes;
            $portfolio['rebalancingDates'] = $rebalancingDates;
            $portfolio['portfolioAllocations'] = $portfolioAllocations;
            $portfolio['portfolioWeights'] = $portfolioWeights;
            $portfolio['fundingAllocations'] = $fundingAllocations;
            $portfolio['fundingWeights'] = $fundingWeights;
            $portfolio['strategyAllocations'] = $strategyAllocations;
            $portfolio['strategyWeights'] = $strategyWeights;
            $portfolio['borrowingWeights'] = $borrowingWeights;
            $portfolio['lendingWeights'] = $lendingWeights;
            $portfolio['assetClassWeights'] = $assetClassWeights;
            $portfolio['factorWeights'] = $factorWeights;
          }

          // set active period
          $portfolio[$prefix . 'active'] = 1;

          // map metrics fields
          foreach (self::METRIC_MAP as $sourceKey => list($factor, $targetKey)) {
            if ($options['noFactor'] === true) $factor = 1;
            $portfolio[$prefix . $targetKey] = round($factor * $portfolioCal[$sourceKey . $suffix], $dec);
          }

          // map starts and ends dates
          $dates = $portfolioCal['timeseriesDates' . $suffix];
          $portfolio[$prefix . 'starts'] = strftime('%F %T', Helper::excel2time(reset($dates)));
          $portfolio[$prefix . 'ends'] = strftime('%F %T', Helper::excel2time(end($dates)));

          if (in_array($options['period'], ['*', $periodKey]) && $options['metricsOnly'] === false) {
            // map timeseries to tracks
            $dateTrack = [];
            $equityTrack = [];
            $drawdownTrack = [];
            $volatilityTrack = [];
            $sample = $this->getSample($minDate, $maxDate, $options['fullTracks']);
            $this->fillTracks($portfolioCal, $dates, $dateTrack, $equityTrack, $drawdownTrack, $volatilityTrack, $sample, $suffix, $options['volMeasure'], $options['noFactor']);
            $portfolio[$prefix . 'date_track'] = $dateTrack;
            $portfolio[$prefix . 'track'] = $equityTrack;
            $portfolio[$prefix . 'drawdown_track'] = $drawdownTrack;
            $portfolio[$prefix . 'volatility_track_' . $options['volMeasure']] = $volatilityTrack;

            // map monthly returns per years
            $years = [];
            $this->fillMonthlyReturns($portfolioCal, $suffix, $years, $options['noFactor']);
            $portfolio[$prefix . 'monthly_returns'] = $years;

            // contribution graph
            $listCodes = $calculOpt['listCodes'];
            $volatility = [];
            $return = [];
            $var = [];
            $incrementalVolatility = $portfolioCal['incrementalVolatility' . $suffix];
            if ($baseVolatility = $portfolioCal['volatility' . $suffix]) {
              foreach ($incrementalVolatility as $idx => $val) {
                $volatility[$listCodes[$idx]] = round($val * $fac / $baseVolatility, $dec);
              }
            }
            $incrementalReturn = $portfolioCal['incrementalReturn' . $suffix];
            foreach ($incrementalReturn as $idx => $val) {
              $return[$listCodes[$idx]] = round($val * $fac, $dec);
            }
            $incrementalVar = $portfolioCal['incrementalVar' . $suffix];
            if ($baseVar = $portfolioCal['var' . $suffix]) {
              foreach ($incrementalVar as $idx => $val) {
                $var[$listCodes[$idx]] = round($val * $fac / $baseVar, $dec);
              }
            }
            $portfolio[$prefix . 'incremental_volatility'] = $volatility;
            $portfolio[$prefix . 'incremental_return'] = $return;
            $portfolio[$prefix . 'incremental_var'] = $var;

            // weights allocation
            $years = [];
            $leverage = [];
            $this->fillDailyWeights($listCodes, $portfolioCal, $suffix, $years, $leverage, $options['noFactor']);
            $portfolio[$prefix . 'daily_weights'] = $years;
            $portfolio[$prefix . 'daily_leverage'] = $leverage;
          }
        }
        // increment i
        $i++;
      }
      Helper::debug('object', $portfolio);
      return $portfolio;
  }

  function fillVolatilityTrack($indexOrPortfolio, array &$track, array $options = []) {
    // default options
    $options = array_merge([
      'maxDate' => null,
      'periods' => null,
      'period' => '5y',
      'measure' => '30d',
      'fullTrack' => false,
      'noLive' => false,
      'field' => null,
    ], $options);

    // cast the first parameter into index or portfolio
    if ($indexOrPortfolio instanceof Indexinfo) {
      $index = $indexOrPortfolio;
      $strategies = [$index];
    } else if ($indexOrPortfolio instanceof Portfolio) {
      $portfolio = $indexOrPortfolio;
      [$strategies, $portfolioStrategies] = $this->getActiveStrategiesAndPortfolioStrategies($portfolio);
    } else {
      throw new Exception('First parameter should be an instance of Indexinfo or Portfolio');
    }

    // get maxDate and periods
    $maxDate = $this->getMaxDate($indexOrPortfolio, $options['maxDate']);
    $periods = $this->getPeriods($strategies, $maxDate, $options['periods'], $options['noLive']);

    // keep only the specified period
    $period = $options['period'];
    $periods = [
      $period => $periods[$period],
      'min' => $periods['min'],
    ];
    $listStartDates = array_values($periods);

    // prepare options input for calcul-cpp extension
    if (isset($index)) {
      $calculOpt = [
        'listCodes' => [$index->code],
        'listStartDates' => $listStartDates,
        'endDate' => $maxDate,
      ];
    } else {
      $calculOpt = $this->getPortfolioOptions($portfolio, $portfolioStrategies, $strategies, $periods, $maxDate);
    }

    // set the volatilityInterval
    $measure = Helper::str2int($options['measure']);
    $calculOpt['volatilityInterval'] = [$measure];

    // calcul volatility track
    $outputCal = $this->calcul($calculOpt);

    // check skipped periods
    if (count($outputCal['skipped']) === count($periods)) {
      throw new Exception('Not sufficient data for requested period');
    }

    // map date timeseries to dates
    $dates = $outputCal['timeseriesDates_0'];

    // map volatility timeseries to track
    $sample = $this->getSample(end($listStartDates), $maxDate, $options['fullTrack']);
    $this->fillVolTrack($outputCal, $dates, $track, $sample, '_0', $options['measure'], $options['field']);
  }

  function getCorrelationWithBenchmarks($indexOrPortfolio, array $benchmarkCodes, array $options = []) {
    $options = array_merge([
      'maxDate' => null,
      'periods' => null,
      'period' => '5y',
      'noLive' => false,
    ], $options);

    // cast the first parameter into index or portfolio
    if ($indexOrPortfolio instanceof Indexinfo) {
      $index = $indexOrPortfolio;
      $strategies = [$index];
    } else if ($indexOrPortfolio instanceof Portfolio) {
      $portfolio = $indexOrPortfolio;
      [$strategies, $portfolioStrategies] = $this->getActiveStrategiesAndPortfolioStrategies($portfolio);
    } else {
      throw new Exception('First parameter should be an instance of Indexinfo or Portfolio');
    }

    // get maxDate and periods
    $maxDate = $this->getMaxDate($indexOrPortfolio, $options['maxDate']);
    $periods = $this->getPeriods($strategies, $maxDate, $options['periods'], $options['noLive']);

    // keep only the specified period
    $period = $options['period'];
    $periods = [
      $period => $periods[$period],
      'min' => $periods['min'],
    ];

    // prepare options input for calcul-cpp extension
    if (isset($index)) {
      $calculOpt = [
        'listCodes' => Helper::map($strategies, 'code'),
        'listStartDates' => array_values($periods),
        'endDate' => $maxDate,
      ];
    } else {
      $calculOpt = $this->getPortfolioOptions($portfolio, $portfolioStrategies, $strategies, $periods, $maxDate);
    }
    $calculOpt['listBenchmarks'] = $benchmarkCodes;

    // calcul correlation
    $outputCal = $this->calcul($calculOpt);

    // map correlation between portfolio/strategies and benchmarks
    $correls = $outputCal['correlation_0'];
    return $this->mapCorrelationWithBenchmarks($correls, $strategies, $benchmarkCodes);
  }

  function getCorrelationBetweenStrategies(array &$strategies, array $options = []) {
    $options = array_merge([
      'maxDate' => null,
      'periods' => null,
      'period' => '5y',
      'noLive' => false,
    ], $options);

    // get maxDate and periods
    $maxDate = $this->getMaxDate(null, $options['maxDate']);
    $periods = $this->getPeriods($strategies, $maxDate, $options['periods'], $options['noLive']);

    // keep only the specified period
    $period = $options['period'];
    $periods = [
      $period => $periods[$period],
      'min' => $periods['min'],
    ];

    // calcul correlation
    $outputCal = $this->calcul([
      'listCodes' => Helper::map($strategies, 'code'),
      'listStartDates' => array_values($periods),
      'endDate' => $maxDate,
    ]);

    // map correlation between strategies
    $correls = $outputCal['correlation_0'];
    return $this->mapCorrelationBetweenStrategies($correls, $strategies);
  }

  function generateStrategiesMetrics(string $maxDate) {
    // PURE FACTORS
    $result = $this->getClient('s3')->getObject([
      'Bucket' => 'engagingenterprises-configs',
      'Key' => 'pure-factors-config.json',
    ]);
    $inputPure = json_decode($result['Body']);

    // DISCOVER GENERATION
    // get non private strategies codes
    $listCodes = Indexinfo::whereNull('owner')->pluck('code')->toArray();
    if (empty($listCodes)) {
      return;
    }

    // get calcul input
    $periods = $this->getDefaultPeriods($maxDate);
    $input = [
      // 'queryLimit' => 200,
      // 'dynamoDelay' => 1000,
      'listStartDates' => $periods,
      'endDate' => $maxDate,
      'listCodes' => $listCodes,
      'riskFreeRateTicker' => self::TICKERS['RISK_FREE_RATE'],
      'inputPure' => $inputPure,
    ];
    Helper::func2keys(pipe('strtotime', 'Helper::time2excel'), $input, self::DATE_KEYS);

    // generate discover input
    $jsonInput = json_encode($input);
    $jobName = explode(':', env('JOB_DISC_GEN_DEF'))[0] . '_' . env('APP_ENV') . date('_Ymd_His');
    $this->getClient('s3')->putObject([
      'Bucket' => 'engagingenterprises-batch-files',
      'Key' => $jobName . '.json',
      'Body' => $jsonInput,
    ]);

    // generate discover cache
    $this->getClient('batch')->submitJob([
      'jobDefinition' => env('JOB_DISC_GEN_DEF'),
      'jobName' => $jobName,
      'jobQueue' => env('JOB_QUEUE'),
      'containerOverrides' => [
        'environment' => [
          [ 'name' => 'SLS_STAGE', 'value' => env('JOB_STAGE') ],
          [ 'name' => 'DISCOVER_JSON', 'value' => $jobName . '.json' ],
        ],
      ],
    ]);
  }

  function generatePrivateStrategiesMetrics(string $maxDate, array $teamIds, string $invocationType = 'RequestResponse') {
    if (empty($teamIds)) {
      return [];
    }

    // get calcul input
    $periods = $this->getDefaultPeriods($maxDate);
    $input = [
      // 'queryLimit' => 200,
      // 'dynamoDelay' => 1000,
      'listStartDates' => $periods,
      'endDate' => $maxDate,
      'listCodes' => [], // to be filled in later
      'riskFreeRateTicker' => self::TICKERS['RISK_FREE_RATE'],
      'mappingCodes' => [], // to be filled in later
    ];
    Helper::func2keys(pipe('strtotime', 'Helper::time2excel'), $input, self::DATE_KEYS);

    // get private strategies grouped by team id
    $strategiesByTeamId = Indexinfo::whereIn('owner', $teamIds)
      ->get()->groupBy('owner');

    /// complete calcul input
    foreach($strategiesByTeamId as $teamId => $strategies) {
      // get private strategies codes
      $listCodes = $strategies->pluck('code')->toArray();
      if (empty($listCodes)) {
        continue;
      }

      // add codes to calcul input
      $input['listCodes'] = array_merge($input['listCodes'], $listCodes);
      $input['mappingCodes'][strval($teamId)] = $listCodes;
    }

    // generate private discover cache
    $jsonInput = json_encode($input);
    $jsonOutputs[] = $this->invokeLambda([
      'FunctionName' => env('SLS_PRIVE_GEN_FUNC'),
      'InvocationType' => $invocationType,
      'Payload' => $jsonInput,
    ]);

    // return output for RequestResponse invocation type
    if ($invocationType === 'RequestResponse') {
      $output = json_decode($jsonOutput, true); // to array
      $this->checkOutput($output); // handle lambda error
      return $output;
    }
  }

  function suspendStrategiesNotUpdated(string $maxDate = null): array {
    // if maxDate shift it to weekday
    if ($maxDate) {
      $maxDate = Helper::shiftTimeToWeekday(strtotime($maxDate));
    }

    // get deleted strategies codes
    $deletedCodes = Indexinfo::onlyTrashed()->pluck('code')->toArray();

    // get previous suspended strategies codes
    $prevSuspendedCodes = Indexinfo::where('is_suspended', 1)->pluck('code')->toArray();

    // get all strategies metrics
    $params = [
      'FunctionName' => env('SLS_DISC_FUNC'),
      'Payload' => json_encode(['team' => 'summary']), // add private discover gapData
    ];
    $jsonOutput = $this->invokeLambda($params);
    $output = json_decode($jsonOutput, true); // to array
    $this->checkOutput($output); // handle lambda error

    // extract lastDates, suspended and unsuspended codes
    $lastDates = [];
    $suspendeds = [];
    $unsuspendedCodes = [];
    $startDateErrors = [];
    foreach (Helper::get($output, 'gapData', []) as $code => $gap) {
      // fill lastDates array
      if ($headDate = Helper::get($gap, 'headDate')) {
        $headDate = strftime('%F', Helper::excel2time($headDate));
      }
      $lastDate = Helper::get($gap, 'lastDate');
      $lastDate = ($lastDate) ? Helper::excel2time($lastDate) : $maxDate;
      $lastDate = ($lastDate) ? strftime('%F', $lastDate) : null;
      $lastDates[$code] = $lastDate;

      // fill suspendeds and unsuspendedCodes arrays
      $is_suspended = 0;
      if ($gap['largestGap'] >= 20) {
        $is_suspended = 1;
        $gap['new'] = !in_array($code, $prevSuspendedCodes);
        $gap['code'] = $code;
        $gap['headDate'] = $headDate;
        $gap['lastDate'] = $lastDate;
        $suspendeds[$code] = $gap;
      } else if (in_array($code, $prevSuspendedCodes)) {
        $unsuspendedCodes[] = $code;
      }

      if ($strategy = Indexinfo::where('code', $code)->first()) {
        // fill startDateErrors array
        if ($trackStartDate = $headDate) {
          if ($strategy->historyStartDate < $trackStartDate) {
            $startDateError = [
              'code' => $code,
              'trackStartDate' => $trackStartDate,
              'historyStartDate' => $strategy->historyStartDate,
            ];
            if ($strategy->inceptionDate < $trackStartDate) {
              $startDateError['inceptionDate'] = $strategy->inceptionDate;
            }
            $startDateErrors[] = $startDateError;
          }
        }

        // update strategy lastDate and is_suspended without updating timestamps
        $strategy->lastDate = $lastDate;
        $strategy->is_suspended = $is_suspended;
        $strategy->timestamps = false;
        $strategy->save();
      }
    }

    // private strategies never suspended
    // $privateStrategies = Indexinfo::whereNotNull('owner')->get();
    // foreach ($privateStrategies as $strategy) {
    //   $isOutdated = ($maxDate - strtotime($strategy->lastDate)) >= (self::SEC_IN_DAY * 35);
    //   if ($isOutdated && !$strategy->is_suspended) {
    //     // suspend strategy without updating timestamps
    //     DB::table('indexinfo')->where('code', $strategy->code)->update([
    //       'is_suspended' => 1,
    //     ]);
    //   }
    //   if (!$isOutdated && $strategy->is_suspended) {
    //     // unsuspend strategy without updating timestamps
    //     DB::table('indexinfo')->where('code', $strategy->code)->update([
    //       'is_suspended' => 0,
    //     ]);
    //   }
    // }

    // recompute portfolio weights for soft deleted and suspended strategies
    $portfolioIds = [];
    $disabledCodes = array_unique(array_merge($deletedCodes, array_keys($suspendeds)));
    foreach($disabledCodes as $code) {
      if ($strategy = Indexinfo::withTrashed()->where('code', $code)->first()) {
        $portfolioStrategies = PortfolioStrategy::where('indexinfo_id', $strategy->id)->get();
        foreach($portfolioStrategies as $portfolioStrategy) {
          $portfolioIds[] = $portfolioStrategy->portfolio_id;
          $portfolioStrategy->delete();
        }
      }
    }
    $portfolioIds = array_unique($portfolioIds);
    $portfolios = Portfolio::whereIn('id', $portfolioIds)->get();
    foreach($portfolios as $portfolio) {
      $portfolio->calculateWeightings('*');
    }

    // send lastDates by email
    array_multisort($lastDates, SORT_DESC);
    Mail::raw(json_encode($lastDates), function($m) {
      $m->from('admin@engagingenterprises.co', 'engagingenterprises')
        ->to('admin@engagingenterprises.co')
        ->cc(['admin@engagingenterprises.co', 'admin@engagingenterprises.co', 'admin@engagingenterprises.co', 'admin@engagingenterprises.co', 'admin@engagingenterprises.co'])
        ->subject(ucfirst(env('APP_ENV')) . ' - Last Contribution Dates');
    });

    // send report by email
    $report = [
      'errors' => Helper::get($output, 'errors', []),
      'deleteds' => $deletedCodes,
      'suspendeds' => array_values($suspendeds),
      'unsuspendeds' => $unsuspendedCodes,
      'startDateErrors' => $startDateErrors,
    ];
    Mail::raw(json_encode($report), function($m) {
      $m->from('admin@engagingenterprises.co', 'engagingenterprises')
        ->to('admin@engagingenterprises.co')
        ->cc(['admin@engagingenterprises.co', 'admin@engagingenterprises.co', 'admin@engagingenterprises.co', 'admin@engagingenterprises.co', 'admin@engagingenterprises.co'])
        ->subject(ucfirst(env('APP_ENV')) . ' - Discover Generation Report');
    });

    return $report;
  }

  function getStrategiesMetrics(array &$indices, array $options = []) {
    $options = array_merge([
      'maxDate' => null,
      'periods' => null,
      'periodKeys' => [],
      'prefix' => '',
      'activeOnly' => false,
      'unauthorized' => false,
      'noPrivate' => false,
      'noFactor' => false,
      'cache' => null, // options: ignoreCache or reCache
    ], $options);

    $user = Auth::user();
    if ($options['unauthorized'] !== true) {
      $indices = $this->right->filterAuthorizedStrategies($user, $indices);
    }
    $periodKeys = $options['periodKeys'];
    if (empty($indices) || empty($periodKeys)) {
      return $indices;
    }

    // map indices to indicesByCode
    $indicesByCode = Helper::keyBy($indices, 'code', 'trim'); // key by code
    $output = [];
    $otherIndicesByCode = [];
    $period = (count($periodKeys) === 1) ? $periodKeys[0] : '*';
    $custPeriods = $options['maxDate'] || $options['periods'];
    if ($custPeriods !== true) {
      // if default periods, get output from discover cache
      $params = [
        'FunctionName' => env('SLS_DISC_FUNC'),
        'Payload' => [], // filled later
      ];
      if ($period !== '*') {
        $params['Payload']['single'] = $period;
      }
      if ($options['noPrivate'] !== true) {
        $params['Payload']['team'] = Helper::get($user->currentTeam, 'id');
      }
      if (!empty($params['Payload'])) {
        $params['Payload'] = json_encode($params['Payload']);
      } else {
        unset($params['Payload']);
      }
      $jsonOutput = $this->invokeLambda($params);
      $output = json_decode($jsonOutput, true); // to array
      $this->checkOutput($output); // handle lambda error

    } else {
      // if custom periods, fetch and compute each strategies
      $codes = array_keys($indicesByCode);
      $query = Indexinfo::where('id', '>', 0);
      $query->whereIn('code', $codes);
      $otherIndices = $query->get(); // ->toArray();
      foreach ($otherIndices as $index) {
        try {
          $otherIndicesByCode[$index->code] = $this->getStrategy($index, [
            'maxDate' => $options['maxDate'],
            'periods' => $options['periods'],
            'period' => $period,
            'metricsOnly' => true,
            'noFactor' => true,
            'noLive' => true,
            'cache' => $options['cache'],
          ]);
        } catch(Throwable $e) {
          Log::error($e);
        }
      }
    }

    // map otherIndicesByCode to output
    foreach ($otherIndicesByCode as $code => $index) {
      foreach ($periodKeys as $period) {
        if ($index->{$period . '_active'} === 1) {
          $output[$period][$code] = ['code' => $code];
          foreach (self::METRIC_MAP as $sourceKey => list($factor, $targetKey)) {
            $output[$period][$code][$sourceKey] = $index->{$period . '_' . $targetKey};
          }
        }
      }
    }

    // map output to indicesByCode
    $prefix = $options['prefix'];
    $checkPeriod = ($options['activeOnly'] === true) ? end($periodKeys) : null;
    foreach ($periodKeys as $period) {
      $listMetrics = Helper::get($output, $period);
      foreach ($indicesByCode as $code => $index) {
        $metrics = Helper::get($listMetrics, $code);
        if (!$metrics || count($metrics) < count(self::METRIC_MAP)) {
          $indicesByCode[$code][$period . '_active'] = 0;
          if ($period === $checkPeriod) {
            $hasActivePeriod = false;
            foreach ($periodKeys as $key) {
              if ($indicesByCode[$code][$key . '_active'] === 1) {
                $hasActivePeriod = true;
                break;
              }
            }
            if ($hasActivePeriod !== true) {
              unset($indicesByCode[$code]);
            }
          }
        } else {
          $indicesByCode[$code][$period . '_active'] = 1;
          foreach (self::METRIC_MAP as $sourceKey => list($factor, $targetKey, $discoverKey)) {
            if ($options['noFactor'] === true) $factor = 1;
            if (!isset($indicesByCode[$code][self::UNIT_PREFIX . $targetKey])) {
              $indicesByCode[$code][self::UNIT_PREFIX . $targetKey] = self::UNIT_MAP[abs($factor)];
            }
            $targetKey = $prefix . $period . '_' . $targetKey;
            $indicesByCode[$code][$targetKey] = round($factor * $metrics[$sourceKey], self::DEC_OUT);
          }
        }
      }
    }
    return array_values($indicesByCode);
  }

  function getStrategiesMetricsAggregates(array &$listWithCodes, string $period, callable $aggregateFunc) {
    if (empty($listWithCodes)) {
      return [];
    }
    $measures = Helper::map(self::METRIC_MAP, 1); // map 'targetKey' of [factor, 'targetKey']
    $indices = Indexinfo::where('id', '>', 0)->selectRaw('code')->get()->toArray();
    $indices = $this->getStrategiesMetrics($indices, [
      'periodKeys' => [$period],
      'activeOnly' => true,
      'unauthorized' => true,
      'noPrivate' => true,
    ]);

    foreach ($listWithCodes as $idx => $arrWithCodes) {
      $codes = $arrWithCodes['codes'];
      if (!is_array($codes)) {
        $codes = explode(',', $codes);
      }
      $indices2 = array_filter($indices, function($o) use($codes) {
        return in_array(trim($o['code']), $codes);
      });
      unset($listWithCodes[$idx]['codes']);
      $indices2Count = count($indices2);
      foreach ($measures as $measure) {
        $listWithCodes[$idx]['number_of_strategies'] = $indices2Count;
        if ($indices2Count > 0) {
          $values = Helper::map($indices2, $period . '_' . $measure);
          $listWithCodes[$idx][$measure] = $aggregateFunc($values);
        } else {
          $listWithCodes[$idx][$measure] = 0;
        }
      }
    }
    return $listWithCodes;
  }

  function generatePortfoliosMetrics(array $userIds, string $invocationType = 'RequestResponse') {
    if (empty($userIds)) {
      return [];
    }

    // get portfolios grouped by user id
    $portfoliosByUserIdChunks = Portfolio::whereIn('user_id', $userIds)
      ->where(function($groupClause) {
        $groupClause->whereNull('draft_of')
          ->orWhere('draft_of', 0);
      })
      ->get()->groupBy('user_id')->chunk(3);

    // generate mylab cache per portfolios chunk
    $date = date('_Ymd_His');
    $jsonOutputs = [];
    foreach($portfoliosByUserIdChunks as $chunkIdx => $portfoliosByUserId) {

      // get calcul inputs
      $inputs = [];
      foreach ($portfoliosByUserId as $userId => $portfolios) {

        // compute calcul inputs
        foreach ($portfolios as $portfolio) {
          try {
            // get active strategies and portfolioStrategies
            [$strategies, $portfolioStrategies] = $this->getActiveStrategiesAndPortfolioStrategies($portfolio);
            if (!$strategies) continue;

            // get maxDate and periods
            $maxDate = $this->getMaxDate($portfolio);
            $periods = $this->getPeriods($strategies, $maxDate, null, true);

            // get portfolio options
            $options = $this->getPortfolioOptions($portfolio, $portfolioStrategies, $strategies, $periods, $maxDate);

            // check calcul options
            $this->checkInput($options);

            // get calcul input
            $input = $this->getBacktestingInput($options);

            // map listStartDates by period
            $listStartDates = [];
            $periodKeys = array_keys($periods);
            foreach ($input['listStartDates'] as $idx => $startDate) {
              $listStartDates[$periodKeys[$idx]] = $startDate;
            }
            $input['listStartDates'] = $listStartDates;

            // add input to calcul inputs
            $portfolioId = $portfolio['id'];
            $inputs[strval($userId)][strval($portfolioId)] = $input;
          } catch(Throwable $e) {
            Log::error($e, [
              'userId' => $userId,
              'portfolioId' => $portfolio->id,
              'portfolioSlug' => $portfolio->slug,
            ]);
          }
        }
      }

      // generate mylab cache
      $jsonInput = json_encode($inputs);
      $filename = 'generate_mylab_' . env('APP_ENV') . $date . '_chunk' . str_pad($chunkIdx + 1, 2, '0', STR_PAD_LEFT) . '.json';
      $this->getClient('s3')->putObject([
        'Bucket' => 'engagingenterprises-batch-files',
        'Key' => $filename,
        'Body' => $jsonInput,
      ]);

      $jsonOutputs[] = $this->invokeLambda([
        'FunctionName' => env('SLS_MYLAB_GEN_FUNC'),
        'InvocationType' => $invocationType,
        'Payload' => json_encode([
          'Key' => $filename,
        ]),
      ]);
    }

    // return output for RequestResponse invocation type
    if ($invocationType === 'RequestResponse') {
      $outputs = [];
      foreach ($jsonOutputs as $jsonOutput) {
        $output = json_decode($jsonOutput, true); // to array
        $this->checkOutput($output); // handle lambda error
        $outputs[] = $output;
      }
      return $outputs;
    }
  }

  function getPortfoliosMetrics(array &$portfolios, array $options = []) {
    $options = array_merge([
      'userId' => 0,
      'periodKeys' => [],
      'activeOnly' => false,
      'noFactor' => false,
    ], $options);

    $periodKeys = $options['periodKeys'];
    if (empty($portfolios) || empty($periodKeys)) {
      return $portfolios;
    }

    // get mylab cache
    $userId = strval($options['userId']);
    $input = ['users' => [$userId]];
    $jsonInput = json_encode($input);
    $jsonOutput = $this->invokeLambda([
      'FunctionName' => env('SLS_MYLAB_FUNC'),
      'Payload' => $jsonInput,
    ]);
    $output = json_decode($jsonOutput, true); // to array
    $this->checkOutput($output); // handle lambda error

    // add metrics to portfolios
    $portfoliosById = Helper::keyBy($portfolios, 'id'); // key by id
    foreach ($portfoliosById as $portfolioId => $portfolio) {
      $listMetrics = Helper::get($output[$userId], strval($portfolioId), []);
      // map last rebalancing date weights
      $portfoliosById[$portfolioId]['strategyWeights'] = Helper::get($listMetrics, 'strategyWeights', []);
      $portfoliosById[$portfolioId]['assetClassWeights'] = Helper::get($listMetrics, 'assetClassWeights', []);
      $portfoliosById[$portfolioId]['factorWeights'] = Helper::get($listMetrics, 'factorWeights', []);
      // map metrics units
      foreach (self::METRIC_MAP as $sourceKey => list($factor, $targetKey)) {
        if ($options['noFactor'] === true) $factor = 1;
        $portfoliosById[$portfolioId][self::UNIT_PREFIX . $targetKey] = self::UNIT_MAP[abs($factor)];
      }
      // map metrics fields
      $checkPeriod = ($options['activeOnly'] === true) ? end($periodKeys) : null;
      foreach ($periodKeys as $period) {
        $metrics = Helper::get($listMetrics, $period);
        if (!$metrics || count($metrics) < count(self::METRIC_MAP)) {
          $portfoliosById[$portfolioId][$period . '_active'] = 0;
          if ($period === $checkPeriod) {
            $hasActivePeriod = false;
            foreach ($periodKeys as $key) {
              if ($portfoliosById[$portfolioId][$key . '_active'] === 1) {
                $hasActivePeriod = true;
                break;
              }
            }
            if ($hasActivePeriod !== true) {
              unset($portfoliosById[$portfolioId]);
            }
          }
        } else {
          $portfoliosById[$portfolioId][$period . '_active'] = 1;
          foreach (self::METRIC_MAP as $sourceKey => list($factor, $targetKey)) {
            $portfoliosById[$portfolioId][$period . '_' . $targetKey] = round($factor * $metrics[$sourceKey], self::DEC_OUT);
          }
        }
      }
    }
    return array_values($portfoliosById);
  }

  function feedRawData() {
    $jobName = explode(':', env('JOB_TRACK_GEN_DEF'))[0] . '_' . env('APP_ENV') . date('_Ymd_His');

    // feed raw data only (datafeed)
    $this->getClient('batch')->submitJob([
      'jobDefinition' => env('JOB_TRACK_GEN_DEF'),
      'jobName' => $jobName,
      'jobQueue' => env('JOB_QUEUE'),
      'containerOverrides' => [
        'environment' => [
          [ 'name' => 'SLS_STAGE', 'value' => env('JOB_STAGE') ],
          [ 'name' => 'DATA_FEED', 'value' => 'true' ],
        ],
      ],
    ]);
  }

  function updateCutOffDate(string $maxDate = null): string {
    // update max date setting
    $time = env('JOB_MAX_DATE') ?: '-1 day';
    $maxDateSetting = Setting::where('key', 'maxDate')->first();
    if ($maxDate) {
      $maxDateSetting->value = $maxDate;
    } else {
      $timestamp = strtotime($time);
      $weekendDays = Helper::countWeekendDaysBetweenTimes($timestamp, time());
      if ($weekendDays) {
        $timestamp = strtotime($time . ' -' . $weekendDays . ' days');
      }
      $maxDateSetting->value = strftime('%F', Helper::shiftTimeToWeekday($timestamp));
    }
    $maxDateSetting->save();

    return $maxDateSetting->value;
  }

  function generateTrackData(string $maxDate, bool $datafeed = false){
    // get all strategies
    $codes = Indexinfo::where('datafeed', true)->pluck('code')->all();
    if (!$codes) return;

    // get track gen input
    $input = [
      // 'queryLimit' => 200,
      // 'dynamoDelay' => 1000,
      // 'startDate' => strftime('%F', strtotime('-10 years', strtotime($maxDate))),
      'endDate' => $maxDate,
      'listCodes' => array_merge(
        $codes,
        array_values(self::TICKERS)
      ),
    ];

    // generate track gen input
    $jsonInput = json_encode($input);
    $jobName = explode(':', env('JOB_TRACK_GEN_DEF'))[0] . '_' . env('APP_ENV') . date('_Ymd_His');
    $this->getClient('s3')->putObject([
      'Bucket' => 'engagingenterprises-batch-files',
      'Key' => $jobName . '.json',
      'Body' => $jsonInput,
    ]);

    // generate track data with or without datafeed
    $datafeed = ($datafeed === true) ? 'true' : 'false';
    $this->getClient('batch')->submitJob([
      'jobDefinition' => env('JOB_TRACK_GEN_DEF'),
      'jobName' => $jobName,
      'jobQueue' => env('JOB_QUEUE'),
      'containerOverrides' => [
        'environment' => [
          [ 'name' => 'SLS_STAGE', 'value' => env('JOB_STAGE') ],
          [ 'name' => 'DATA_FEED', 'value' => $datafeed ],
          [ 'name' => 'TRACK_JSON', 'value' => $jobName . '.json' ],
        ],
      ],
    ]);
  }

  function downloadTrackData(array $codes, string $maxDate = null): array {
    $TableName = env('DYN_TRACK_DATA_TABLE');
    $mapKeys = map(function($code) {
      return ['code' => ['S' => trim($code)]];
    });
    $mapPutRequests = map(function($Item) {
      return ['PutRequest' => ['Item' => $Item]];
    });

    $results = [];
    $codesChunks = array_chunk($codes, 100);
    foreach ($codesChunks as $codes) {
      $params = [
        'RequestItems' => [
          $TableName => ['Keys' => $mapKeys($codes)],
        ],
        'ReturnConsumedCapacity' => 'TOTAL',
      ];
      $result = $this->getClient('dynamodb')->batchGetItem($params);
      $items = $result['Responses'][$TableName];
      foreach ($items as $idx => $item) {
        $code = $item['code']['S'];
        $track = json_decode($item['track']['S']);
        Helper::fillMissingDates($track, $maxDate);
        $results[$code] = $track;
      }
    }
    return $results;
  }

  function uploadTrackData(string $code, array &$track, string $currency = '') {
    $params = [
      'Item' => [
        'code' => ['S' => trim($code)],
        'track' => ['S' => json_encode($track)],
        'original_file' => ['S' => 'Private Strategy'],
      ],
      'ReturnConsumedCapacity' => 'TOTAL',
      'TableName' => env('DYN_TRACK_DATA_TABLE'),
    ];
    if ($currency) {
      $params['Item']['currency'] = trim($currency);
    }
    return $this->getClient('dynamodb')->putItem($params);
  }

  function deleteTrackData(string $code) {
    // check if it is a private strategy owned by the team
    $teamId = Helper::get(Auth::user()->currentTeam, 'id');
    $query = Indexinfo::where('id', '>', 0);
    $query->where('type', '=', 'Private Strategy');
    $query->where('owner', '=', $teamId);
    $query->where('code', '=', $code);
    $privateStrategy = $query->first();
    if (!$privateStrategy) {
      throw new Exception('Private strategy not found');
    }

    // delete the private strategy track data if existing
    $params = [
      'Key' => [
        'code' => ['S' => trim($code)],
      ],
      'ReturnConsumedCapacity' => 'TOTAL',
      'TableName' => env('DYN_TRACK_DATA_TABLE'),
    ];
    $result = $this->getClient('dynamodb')->getItem($params);
    if (!empty($result['Item'])) {
      $result = $this->getClient('dynamodb')->deleteItem($params);
    }

    // delete the private strategy from team portfolios and favourites
    if ($team = Team::find($teamId)) {
      foreach ($team->users() as $user) {
        $inUserPortfolios = false;
        $portfolios = Portfolio::where('user_id', $user->id)->get();
        foreach ($portfolios as $portfolio) {
          $index = $portfolio->hasMany('App\PortfolioStrategy')
            ->where('indexinfo_id', '=', $privateStrategy->id)
            ->first();

          if ($index) {
            $index->forceDelete();
          }

          $fave = null;
          // if is a favourites draft
          if ($portfolio->draft_of === 0) {
              //means it's a favourites. should remove faves too here.
              $fave = Auth::user()->hasMany('App\FavouriteItem')
              ->where('index_id', '=', $privateStrategy->id)
              ->first();

              if ($fave) {
                $fave->forceDelete();
              }
          }

          if ($index || $fave) {
            $inUserPortfolios = true;
            $portfolio->calculateWeightings("*");
          }
        }
        // re-generate portfolios metrics if in user portfolios
        if ($inUserPortfolios) {
          $this->generatePortfoliosMetrics([$user->id], 'Event');
        }
      }
    }

    // delete the private strategy indexinfo
    $privateStrategy->forceDelete();

    return $result;
  }

  /**
   * Private functions
   */

  protected function checkInput(array $input = null) {
    if (!$input) {
      throw new Exception('Input is NULL');
    }
    if (empty($input['listCodes'])) {
      throw new Exception('Input listCodes is empty');
    }
  }

  protected function checkOutput(array $output = null) {
    if (!$output) {
      throw new Exception('Cannot decode json output');
    }

    $errorMessage = !empty($output['errorMessage']) ? $output['errorMessage']
      : (!empty($output['error']) ? $output['error'] : '');

    if ($errorMessage) {
      $errorType = !empty($output['errorType']) ? $output['errorType'].': ' : '';
      $stackTrace = !empty($output['stackTrace']) ? ' in '.$output['stackTrace'][0] : '';
      $message = $errorType . $errorMessage . $stackTrace;

      if ($errorType || stripos($errorMessage, 'error')) {
        throw new Exception($message);
      }
    }
  }

  protected function invokeCalculLambda(array $input, bool $toArray = true) {
    Helper::debug('input', $input);
    $jsonInput = json_encode($input);
    $jsonOutput = $this->invokeLambda([
      'FunctionName' => env('SLS_CALC_FUNC'),
      'Payload' => $jsonInput,
    ]);
    $output = json_decode($jsonOutput, $toArray);
    Helper::debug('output', $output);
    return $output;
  }

  protected function mapCorrelationWithBenchmarks(array &$correls, array &$strategies, array &$benchmarkCodes) {
    $matrix = [];
    $portfolioCorrel = [];
    $fields = Helper::map($strategies, 'id');
    $benchCnt = count($benchmarkCodes);
    $totalCnt = count($correls) - $benchCnt; // ignoring benchmarks correlations with strategies
    for ($i=0; $i < $totalCnt; $i++) {
      $correl = $correls[$i];
      $k = array_search(1, $correl);
      $correlCnt = count($correl);
      $noBenchCnt = $correlCnt - $benchCnt;
      $isPortfolioCorrel = ($i === $totalCnt - 1);
      for ($j=$noBenchCnt; $j < $correlCnt; $j++) {
        $l = $j - $noBenchCnt;
        $ascii = $l + ($l < 3 ? 120 : 94);
        if ($isPortfolioCorrel) {
          $portfolioCorrel[chr($ascii)] = round($correl[$j], self::DEC_OUT);
        } else {
          $matrix[$fields[$k]][chr($ascii)] = round($correl[$j], self::DEC_OUT);
        }
      }
    }
    return ['i' => $matrix, 'p' => $portfolioCorrel];
  }

  protected function mapCorrelationBetweenStrategies(array &$correls, array &$strategies) {
    $matrix = [];
    $fields = Helper::map($strategies, 'id');
    $totalCnt = count($correls) - 1; // ignoring portfolio correlation with strategies
    for ($i=0; $i < $totalCnt; $i++) {
      $correl = $correls[$i];
      $k = array_search(1, $correl);
      $correlCnt = count($correl) - 1; // ignoring strategy correlation with portfolio
      for ($j=0; $j < $correlCnt; $j++) {
        if ($j === $k) continue;
        $field1 = $fields[$k] . '-' . $fields[$j];
        $field2 = $fields[$j] . '-' . $fields[$k];
        $exists = array_key_exists($field1, $matrix) || array_key_exists($field2, $matrix);
        if ($exists) continue;
        $matrix[$field1] = round($correl[$j], self::DEC_OUT);
      }
    }
    return $matrix;
  }

  protected function fillTracks(array &$outputCal, array &$dates, array &$dateTrack, array &$equityTrack, array &$drawdownTrack, array &$volatilityTrack, string $sample = 'daily', string $suffix = '_0', string $volMeasure = '30d', bool $noFactor = false) {
    $volMeasure = Helper::str2int($volMeasure);
    $factor = ($noFactor === true) ? 1 : 100;
    $decimal = ($noFactor === true) ? self::DEC_IN : self::DEC_OUT;
    $firstIdx = 0;
    $lastIdx = count($dates) - 1;
    $currDate = null;
    $prevDate = null;
    foreach ($dates as $idx => $date) {
      $prevDate = $currDate;
      $currDate = Helper::excel2time($date); // current date
      switch ($sample) {
        case 'weekly':
          $day = date('N', $currDate); // current weekday (1 Mon - 7 Sun)
          if ($idx === $firstIdx) {
            $sampleDay = date('N', Helper::excel2time($dates[$lastIdx])); // sample day (1 Mon - 7 Sun)
          }
          if ($day !== $sampleDay) continue 2; // skipping no sample day
          break;
      }
      // $equity = $outputCal['timeseriesEquity' . $suffix][$idx]; // equity value
      $var = $outputCal['timeseriesEquityVariation' . $suffix][$idx]; // equity variation
      $equity = 100 + (100 * $var); // equity value on base 100
      $drawdown = $outputCal['timeseriesRollingDrawDowns' . $suffix][$idx]; // drawdown
      $dateTrack[] = strftime('%F', $currDate); // fill date track
      $equityTrack[] = round($equity, $decimal); // fill equity track
      $drawdownTrack[] = round($drawdown * $factor, $decimal); // fill drawdown track
      if ($idx > $volMeasure && $prevDate) { // skip days before measure
        $volatility = $outputCal['timeseriesRollingVolatility' . $suffix][$idx][0]; // volatility
        $volatilityTrack[] = [strftime('%F', $prevDate), round($volatility * $factor, $decimal)]; // fill volatility track
      }
    }
  }

  protected function fillVolTrack(array &$outputCal, array &$dates, array &$track, string $sample = 'daily', string $suffix = '_0', string $measure = '30d', string $field = null) {
    $measure = Helper::str2int($measure);
    $firstIdx = $measure;
    $lastIdx = count($dates) - 1;
    $currDate = null;
    $prevDate = null;
    foreach ($dates as $idx => $date) {
      if ($idx < $firstIdx) continue; // skip days before measure
      $prevDate = $currDate;
      $currDate = Helper::excel2time($date); // current date
      switch ($sample) {
        case 'weekly':
          $day = date('N', $currDate); // current weekday (1 Mon - 7 Sun)
          if ($idx === $firstIdx) {
            $sampleDay = date('N', Helper::excel2time($dates[$lastIdx])); // sample day (1 Mon - 7 Sun)
          }
          if ($day !== $sampleDay) continue 2; // skipping no sample day
          break;
      }
      if ($prevDate) {
        $val = $outputCal['timeseriesRollingVolatility' . $suffix][$idx][0]; // track value
        if (!empty($field)) {
          if (empty($track[$prevDate])) { // if the current date is missing, add it to the track
            $track[$prevDate] = ['date' => strftime('%F', $prevDate)];
          }
          $track[$prevDate][$field] = round($val * 100, self::DEC_OUT); // fill track with factor 100
        } else {
          $track[] = [strftime('%F', $prevDate), round($val * 100, self::DEC_OUT)]; // fill volatility track with factor 100
        }
      }
    }
  }

  protected function fillMonthlyReturns(array &$outputCal, string $suffix, array &$years, bool $noFactor = false) {
    $factor = ($noFactor === true) ? 1 : 100;
    $decimal = ($noFactor === true) ? self::DEC_IN : self::DEC_OUT;
    $yearIdx = 0;
    $yearlyRtnVals = $outputCal['yearlyRtn' . $suffix];
    $monthlyRtnVals = $outputCal['monthlyRtn' . $suffix];
    $monthlyRtnDates = $outputCal['monthlyRtnDates' . $suffix];
    array_shift($monthlyRtnDates); // remove first date
    foreach ($monthlyRtnDates as $monthIdx => $date) {
      $unixtimestamp = Helper::excel2time($date);
      $year = strftime('%Y', $unixtimestamp);
      if (!isset($years[$year])) {
        $years[$year] = (object)[]; // workaround to output string keys (https://stackoverflow.com/questions/3445953/how-can-i-force-php-to-use-strings-for-array-keys)
        $years[$year]->{'0'} = number_format($yearlyRtnVals[$yearIdx++] * $factor, $decimal);
      }
      $month = strval(intval(strftime('%m', $unixtimestamp)));
      $val = $monthlyRtnVals[$monthIdx];
      $years[$year]->{$month} = number_format($val * $factor, $decimal);
    }
  }

  protected function fillDailyWeights(array &$listCodes, array &$outputCal, string $suffix, array &$years, array &$leverage, bool $noFactor = false) {
    $factor = ($noFactor === true) ? 1 : 100;
    $decimal = ($noFactor === true) ? self::DEC_IN : self::DEC_OUT;
    $monthlyWeightVals = $outputCal['weights' . $suffix];
    foreach ($monthlyWeightVals as $monthIdx => $list) {
      $date = strftime('%F', Helper::excel2time($list[0]));
      $monthlyWeights = [];
      foreach ($listCodes as $idx => $val) {
        $monthlyWeights[$val] = round($list[$idx + 1] * $factor, $decimal);
      }
      $years[$date] = $monthlyWeights;

      $count = count($listCodes) + 1;
      $winLeverage = round($list[$count] * $factor, $decimal);
      $loseLeverage = round($list[$count + 1] * $factor, $decimal);
      $monthlyLeverage = [$winLeverage, $loseLeverage];
      $leverage[$date] = $monthlyLeverage;
    }
  }

  protected function getFundingOption(array $options) {
    $init = !empty($options['init']) && ($options['init'] === true);
    $listStartDates = $options['periods'];
    $endDate = $options['maxDate'];
    $listRebalancingDates = $options['listRebalancingDates'];
    $funding_currency = $options['funding_currency'];
    $rateTickers = [];
    foreach($listRebalancingDates as $idx => $date) {
      $fundingOption = $init ? $funding_currency[0] : $funding_currency[$idx];
      $rateTicker = '';
      switch ($fundingOption) {
        case 'USD': $rateTicker = self::TICKERS['USD_RATE']; break;
        case 'EUR': $rateTicker = self::TICKERS['EUR_RATE']; break;
        default: $rateTicker = 'None';
      }
      $rateTickers[] = $rateTicker;
    }
    return [
      'listStartDates' => $listStartDates,
      'endDate' => $endDate,
      'listRebalancingDates' => $listRebalancingDates,
      'rateTickers' => $rateTickers,
      'listCodes' => $options['listCodes'],
    ];
  }

  protected function getAllocationOption(array $options)  {
    // lendingTicker, lendingRateAdd not mapping
    $initAllocation = $options['init'] === true;
    $listCodes = $options['listCodes'];
    $listStartDates = $options['periods'];
    $endDate = $options['maxDate'];
    $listRebalancingDates = $options['listRebalancingDates'];
    $rebalancingList = $options['rebalancingList'];
    $allocationModels = [];
    $varIntervals = [];
    $scalingTypes = [];
    $leverageOrVols = [];
    $volatilityTargetIntervals = [];
    $targetVolMinLeverages = [];
    $targetVolMaxLeverages = [];
    $manualWeights = [];
    $varMods = [];
    $manualCorrels = [];
    $manualVols = [];
    $manualBudgets = [];
    foreach($listRebalancingDates as $idx => $date) {
      $allocationOption = [];
      if ($initAllocation) {
        $allocationOption = $options['initOption'];
      } else {
        $allocationOption = $rebalancingList[$date];
      }
      $allocationModel = 0;
      $manualWeight = null;
      $varInterval = Helper::str2int(self::DEFAULT_ALLOCATION['volatility_interval']);
      $varMods[] = 0;
      switch ($allocationOption['custom_weighting']) {
        case 'manual': {
          $allocationModel = -1;
          $manualWeight = [];
          foreach($listCodes as $code) {
            $manualWeight[] = round($allocationOption[$code]['weighting'] / 100, self::DEC_IN);
          }
          $manualWeights[] = $manualWeight;
          break;
        }
        case 'equal': {
          $allocationModel = 0;
          break; // Fixed Equity
        }
        case 'risk': {
          $allocationModel = 1;
          if (!$initAllocation) {
            $this->setManualAllocation($listCodes, $idx, $allocationOption, $varMods, $manualVols, $manualCorrels, $manualBudgets);
          }
          break; // Vol Risk Parity
        }
        case 'erc': {
          $allocationModel = 2;
          if (!$initAllocation && $allocationOption['changeCorrelation'] === true) {
            $this->setManualAllocation($listCodes, $idx, $allocationOption, $varMods, $manualVols, $manualCorrels, $manualBudgets);
          }
          break; // Equal Risk Contribution
        }
      }
      if (isset($allocationOption['volatility_interval'])) {
        $varInterval = Helper::str2int($allocationOption['volatility_interval']);
      }

      $scalingType = -1;
      $leverageOrVol = 1;
      $targetVolMinLeverage = 0;
      $targetVolMaxLeverage = 2;
      $volatilityTargetInterval = Helper::str2int(self::DEFAULT_ALLOCATION['target_interval']);

      switch ($allocationOption['leverage_type']) {
        case 'Fixed Leverage': {
          $scalingType = 1;
          $leverageOrVol = round($allocationOption['fixed_leverage'] / 100, self::DEC_IN);
          break;
        }
        case 'Target Volatility': {
          $scalingType = 2;
          $leverageOrVol = round($allocationOption['target_volatility'] / 100, self::DEC_IN);
          $targetVolMinLeverage = round($allocationOption['leverage_min'] / 100, self::DEC_IN) ?: 0; // if not provided 0 (0%)
          $targetVolMaxLeverage = round($allocationOption['leverage_max'] / 100, self::DEC_IN) ?: 2; // if not provided 2 (200%)
          $volatilityTargetInterval = Helper::str2int($allocationOption['target_interval'] ?: self::DEFAULT_ALLOCATION['target_interval']);
          break;
        }
      }
      $allocationModels[] = $allocationModel;
      $varIntervals[] = $varInterval;
      $scalingTypes[] = $scalingType;
      $leverageOrVols[] = $leverageOrVol;
      $volatilityTargetIntervals[] = $volatilityTargetInterval;
      $targetVolMinLeverages[] = $targetVolMinLeverage;
      $targetVolMaxLeverages[] = $targetVolMaxLeverage;
    }
    $result = [
      'listCodes' => $listCodes,
      'listStartDates' => $listStartDates,
      'endDate' => $endDate,
      'listRebalancingDates' => $listRebalancingDates,
      'allocationModel' => $allocationModels,
      'varInterval' => $varIntervals,
      'scalingType' => $scalingTypes,
      'leverageOrVol' => $leverageOrVols,
      'volatilityTargetInterval' => $volatilityTargetIntervals,
      'targetVolMinLeverage' => $targetVolMinLeverages,
      'targetVolMaxLeverage' => $targetVolMaxLeverages,
      'manualWeights' => $manualWeights,
      'varMod' => $varMods,
      'manualCorrel' => $manualCorrels,
      'manualVol' => $manualVols,
      'manualBudget' => $manualBudgets,
    ];
    return $result;
  }

  protected function setManualAllocation(array $listCodes = [], int $idx, array &$allocationOption,array &$varMods, array &$manualVols, array &$manualCorrels, array &$manualBudgets) {
    $manualVol = [];
    $manualBudget = [];
    $manualCorrel = [];
    foreach($listCodes as $code) {
      $manualVol[] = round($allocationOption[$code]['vol'] / 100, self::DEC_IN);
      $manualBudget[] = round($allocationOption[$code]['risk'] / 100, self::DEC_IN);
      $manualCorrelOneline = [];
      foreach($listCodes as $subCode) {
        $manualCorrelOneline[] = round($allocationOption['correlation'][$code][$subCode]['value'], self::DEC_IN);
      }
      $manualCorrel[] = $manualCorrelOneline;
    }
    $varMods[$idx] = 1;
    $manualVols[] = $manualVol;
    $manualBudgets[] = $manualBudget;
    $manualCorrels[] = $manualCorrel;
  }

  protected function getAllocationInput(array $options) {
    Helper::func2keys(pipe('strtotime', 'Helper::time2excel'), $options, self::DATE_KEYS);
    Helper::unsetNullValues($options);
    Helper::unsetEmptyArrays($options);
    return $options;
  }

}
