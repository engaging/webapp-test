<?php

namespace App\Services\Lab;

use Illuminate\Support\Facades\Auth;
use App\Services\Constant;
use App\Portfolio;
use Exception;
use Helper;
use function Tarsana\Functional\{ pipe };

class PCAServiceImpl extends AbstractLabService implements PCAService, Constant {

  /**
   * Public constants
   */

   const METRICS_MAP = [// 'sourceKey' => [factor, 'targetKey', 'targetCat']
     // principle
     'varPropBeforeEx'       => [100, 'varProp'     , 'principle'], // Variance Proportion     %
     // strategy
     'coeffBeforeEx'         => [1,   'cor'         , 'strategy' ], // Correlation
     'coeffAdjustedBeforeEx' => [100, 'coeff'       , 'strategy' ], // Coefficient             %
     'varDecomposeBeforeEx'  => [100, 'varDecompose', 'strategy' ], // Variance Decomposition  %
     'stgyVarDecBeforeEx'    => [100, 'varAttr'     , 'strategy' ], // Variance Attribution    %
     // portfolio
     'benchCoeff'            => [1,   'cor'         , 'portfolio'], // Correlation
     'benchVarProp'          => [100, 'varDecompose', 'portfolio'], // Variance Decomposition  %
     'benchVarDec'           => [100, 'varAttr'     , 'portfolio'], // Variance Attribution    %
   ];

  /**
   * Public functions
   */

  function calcul(Portfolio $portfolio, array $options = []) {
    // default options
    $options = array_merge([
      'maxDate' => null,
      'periods' => null,
      'period' => '5y',
      'varThreshold' => null, // in %
      'covType' => 'Covariance', // ['Covariance', 'Correlation']
      'movingWindow' => 'None', // ['None', 'Yearly', 'Quarterly', 'Monthly']
      'modelInterval' => 250, // in days
      'returnInterval' => 'Daily', // ['Daily', 'Weekly', 'Monthly', 'Quarterly', 'Yearly']
      'inSample' => false, // false for out of sample, true for in sample
      'equalWeights' => false, // false for free weights, true for equal weights
      'scalingType' => 'No leverage', // ['No leverage', 'Constant leverage vol target']
      'targetVol' => null, // in %
      'listFilters' => [],
      'filter' => 'None', // ['None', 'Positive Return', 'Negative Return', '+ 1 Std.Dev', '+ 2 Std.Dev', '- 1 Std.Dev', '- 2 Std.Dev', '+- 1 Std.Dev', '+- 2 Std.Dev']
      'metricsOnly' => false,
      'noFactor' => false,
    ], $options);

    // get listCodes
    $portfolio = clone($portfolio);
    [$strategies, $portfolioStrategies] = $this->getActiveStrategiesAndPortfolioStrategies($portfolio);
    $options['listCodes'] = Helper::map($strategies, 'code');

    // get maxDate and periods
    $maxDate = $this->getMaxDate($portfolio, $options['maxDate']);
    $periods = $this->getPeriods($strategies, $maxDate, $options['periods']);

    // keep only the specified period
    $period = $options['period'];
    $options['periods'] = [
      $period => $periods[$period],
    ];

    // get PCA payload
    $options['maxDate'] = $maxDate;
    $payload = $this->getPCAPayload($options);

    // add portfolio options
    $portfolioMaxDate = $this->getMaxDate($portfolio);
    $portfolioPeriods = $this->getPeriods($strategies, $portfolioMaxDate);
    $portfolioPeriods = ['min' => $portfolioPeriods['min']];
    $portfolioOpts = $this->getPortfolioOptions($portfolio, $portfolioStrategies, $strategies, $portfolioPeriods, $portfolioMaxDate);
    $payload['portfolio'] = $this->getBacktestingInput($portfolioOpts);

    // invoke regression lambda
    $result = $this->invokePCALambda($payload);

    // map output
    $output = [];
    $unitsKey = 'units';
    $datesKey = 'dates';
    $pcasKey = 'pcas';
    $pcaTrackValid = 'track_active';
    $summaryKey = 'summary';
    $portfolioKey = 'portfolio';
    $undeterminedKey = 'undetermined';
    $principleCountKey = 'pcCount';
    $principlePrefix = 'pc';
    foreach ($options['listCodes'] as $code) {
      $output[$code] = [];
    }
    $output[$unitsKey] = [];
    $output[$summaryKey] = [];
    $output[$portfolioKey] = [];
    $output[$summaryKey][$principleCountKey] = 0;
    $output['listCodes'] = $options['listCodes'];

    // map metrics units
    foreach (self::METRICS_MAP as $sourceKey => list($factor, $targetKey)) {
      if ($options['noFactor'] === true) $factor = 1;
      $output[$unitsKey][$targetKey] = self::UNIT_MAP[abs($factor)];
    }

    // map metrics fields
    $dec = ($options['noFactor'] === true) ? self::DEC_IN : self::DEC_OUT;
    foreach (self::METRICS_MAP as $sourceKey => list($factor, $targetKey, $targetCat)) {
      if ($options['noFactor'] === true) $factor = 1;
      foreach ($result[$sourceKey] as $idx => $value) {
        switch ($targetCat) {
          case 'principle':
            $principleKey = $principlePrefix . ($idx + 1);
            if (!isset($output[$principleKey])) {
              $output[$principleKey] = [];
              $output[$summaryKey][$principleCountKey]++;
            }
            $output[$principleKey][$targetKey] = round($factor * $value, $dec);
            break;
          case 'strategy':
            $strategyKey = $options['listCodes'][$idx];
            foreach ($value as $itemIdx => $itemVal) {
              $principleKey = $principlePrefix . ($itemIdx + 1);
              $output[$strategyKey][$targetKey][$principleKey] = round($factor * $itemVal, $dec);
            }
            break;
          case 'portfolio':
            $hasUndetermined = count($value) > $output[$summaryKey][$principleCountKey];
            foreach ($value as $itemIdx => $itemVal) {
              if ($hasUndetermined) {
                $principleKey = ($itemIdx > 0) ? $principlePrefix . $itemIdx : $undeterminedKey;
              } else {
                $principleKey = $principlePrefix . ($itemIdx + 1);
              }
              $output[$portfolioKey][$targetKey][$principleKey] = round($factor * $itemVal, $dec);
            }
            break;
        }
      }
    }

    // map timeseries to tracks
    $output[$datesKey] = [];
    foreach ($result['timeseriesDates'] as $index => $date) {
      $output[$datesKey][] = strftime('%F', Helper::excel2time($date));

      if ($options['metricsOnly'] === false) {
        foreach ($result['timeseriesPCA'][$index] as $idx => $value) {
          $principleKey = $principlePrefix . ($idx + 1);
          if (!$index) {
            $output[$principleKey][$pcasKey] = [];
          }
          $output[$principleKey][$pcasKey][] = round($value, self::DEC_OUT);
        }
      }
    }
    foreach ($result['timeseriesPCAValid'] as $idx => $value) {
      $principleKey = $principlePrefix . ($idx + 1);
      $output[$principleKey][$pcaTrackValid] = $value;
    }
    return $output;
  }

  /**
   * Private functions
   */

  protected function invokePCALambda(array $payload, bool $toArray = true) {
    $result = $this->invokeLambda([
      'FunctionName' => env('SLS_PCA_FUNC'),
      'Payload' => json_encode($payload),
    ]);
    return json_decode($result, $toArray);
  }

  protected function getPCAPayload(array $options) {
    if ($varThreshold = $options['varThreshold']) {
      $varThreshold = round($varThreshold / 100, self::DEC_IN);
    }

    switch ($options['covType']) {
      case 'Covariance':  $covType = 0; break;
      case 'Correlation': $covType = 1; break;
      default:            $covType = 0;
    }

    switch ($options['movingWindow']) {
      case 'None':      $movingWindow = -1; break;
      case 'Yearly':    $movingWindow = 1; break;
      case 'Quarterly': $movingWindow = 2; break;
      case 'Monthly':   $movingWindow = 3; break;
      default:          $movingWindow = -1;
    }

    switch ($options['returnInterval']) {
      case 'Daily':     $returnInterval = 0; break;
      case 'Weekly':    $returnInterval = 1; break;
      case 'Monthly':   $returnInterval = 2; break;
      case 'Quarterly': $returnInterval = 3; break;
      case 'Yearly':    $returnInterval = 4; break;
      default:          $returnInterval = 0;
    }

    $inSample = ($options['inSample'] === true) ? 1 : 0;
    $equalWeights = ($options['equalWeights'] === true) ? 1 : 0;

    switch ($options['scalingType']) {
      case 'No leverage':                  $scalingType = 0; break;
      case 'Constant leverage vol target': $scalingType = 1; break;
      default:                             $scalingType = 0;
    }

    switch ($options['scalingType']) {
      // Constant leverage vol target
      case '1': $targetVol = round($options['targetVol'] / 100, self::DEC_IN);
      // No leverage
      default:  $targetVol = null;
    }

    switch ($options['filter']) {
      case 'None':            $filter = 0; break;
      case 'Positive Return': $filter = 1; break;
      case 'Negative Return': $filter = 2; break;
      case '+ 1 Std.Dev':     $filter = 3; break;
      case '+ 2 Std.Dev':     $filter = 4; break;
      case '- 1 Std.Dev':     $filter = 5; break;
      case '- 2 Std.Dev':     $filter = 6; break;
      case '+- 1 Std.Dev':    $filter = 7; break;
      case '+- 2 Std.Dev':    $filter = 8; break;
      default:                $filter = 0;
    }

    $payload = [
      'listCodes' => Helper::get($options, 'listCodes'),
      'listStartDates' => array_values($options['periods']),
      'endDate' => $options['maxDate'],
      'varThreshold' => $varThreshold,
      'covType' => $covType,
      'movingWindow' => $movingWindow,
      'modelInterval' => intval(Helper::get($options, 'modelInterval', 250)),
      'inSample' => $inSample,
      'equalWeights' => $equalWeights,
      'scalingType' => $scalingType,
      'targetVol' => $targetVol,
      'returnInterval' => $returnInterval,
      'listFilters' => $options['listFilters'],
      'filter' => $filter,
    ];

    // trim some input keys
    Helper::func2keys('trim', $payload, self::CODE_KEYS);

    // convert string date to excel dates and clean keys
    Helper::func2keys(pipe('strtotime', 'Helper::time2excel'), $payload, self::DATE_KEYS);

    return ['pca' => $payload];
  }

}
