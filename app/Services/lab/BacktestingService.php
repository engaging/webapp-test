<?php

namespace App\Services\Lab;
use App\Indexinfo;
use App\Portfolio;

interface BacktestingService {

  function calcul(array $options);
  function getMaxDate($indexOrPortfolio = null, string $maxDate = null): string ;
  function getPeriods(&$indexOrPortfolioOrIndices, string $maxDate = null, array $periods = null, bool $noLive = false): array;
  function getStrategy(Indexinfo $index, array $options = []);
  function getPortfolio(Portfolio $portfolio, array $options = []);
  function getAllocation(array $options = []);
  function getFunding(array $options = []);
  function fillVolatilityTrack($indexOrPortfolio, array &$track, array $options = []);
  function getCorrelationWithBenchmarks($indexOrPortfolio, array $benchmarkCodes, array $options = []);
  function getCorrelationBetweenStrategies(array &$strategies, array $options = []);

  function generateStrategiesMetrics(string $maxDate);
  function generatePrivateStrategiesMetrics(string $maxDate, array $teamIds, string $invocationType = 'RequestResponse');
  function suspendStrategiesNotUpdated(string $maxDate = null): array;
  function getStrategiesMetrics(array &$indices, array $options = []);
  function getStrategiesMetricsAggregates(array &$listWithCodes, string $period, callable $aggregateFunc);

  function generatePortfoliosMetrics(array $userIds, string $invocationType = 'RequestResponse');
  function getPortfoliosMetrics(array &$portfolios, array $options = []);

  function feedRawData();
  function updateCutOffDate(string $maxDate = null): string;
  function generateTrackData(string $maxDate, bool $datafeed = false);
  function downloadTrackData(array $codes, string $maxDate = null): array;
  function uploadTrackData(string $code, array &$track, string $currency = '');
  function deleteTrackData(string $code);

}
