<?php

namespace App\Services\Lab;
use App\Services\Constant;
use App\Indexinfo;
use App\Portfolio;
use App\Setting;
use Exception;
use Helper;
use AWS;
use function Tarsana\Functional\{ __, curry, pipe, map, filter };

abstract class AbstractLabService implements Constant {

  /**
   * Public constants
   */

  const FACTOR = 'Factor';
  const STRATEGY = 'Strategy';
  const ASSET_CLASS = 'Asset Class';
  const ASSET_CLASS_COMMODITY = 'Commodity';
  const ASSET_CLASS_CREDIT = 'Credit';
  const ASSET_CLASS_EQUITY = 'Equity';
  const ASSET_CLASS_FIXED_INCOME = 'Fixed Income';
  const ASSET_CLASS_FOREIGN_EXCHANGE = 'Foreign Exchange';
  const ASSET_CLASS_MULTI_ASSETS = 'Multi Assets';
  const FACTOR_CARRY = 'Carry';
  const FACTOR_VOLATILITY = 'Volatility';
  const FACTOR_MOMENTUM = 'Momentum';
  const FACTOR_VALUE = 'Value';
  const FACTOR_LOW_VOL = 'Low Vol';
  const FACTOR_QUALITY = 'Quality';
  const FACTOR_SIZE = 'Size';
  const FACTOR_MULTI_FACTOR = 'Multi';
  const STRATEGY_WEIGHTINGS = ['equal', 'manual'];
  const ROLLING_VOLATILITY_INTERVALS = [30, 60, 120]; // in days used for timeseriesRollingVolatility
  const ASSET_CLASSES = [ // ordered list
    self::ASSET_CLASS_EQUITY,
    self::ASSET_CLASS_FIXED_INCOME,
    self::ASSET_CLASS_FOREIGN_EXCHANGE,
    self::ASSET_CLASS_CREDIT,
    self::ASSET_CLASS_COMMODITY,
    self::ASSET_CLASS_MULTI_ASSETS,
  ];
  const FACTORS = [ // ordered list
    self::FACTOR_CARRY,
    self::FACTOR_VOLATILITY,
    self::FACTOR_MOMENTUM,
    self::FACTOR_MULTI_FACTOR,
    self::FACTOR_VALUE,
    self::FACTOR_LOW_VOL,
    self::FACTOR_QUALITY,
    self::FACTOR_SIZE,
  ];
  const TICKERS = [
    'RISK_FREE_RATE' => 'USGG3M', // RISK FREE rates
    'USD_RATE' => 'FEDL01', // BOR and LEN rates
    'EUR_RATE' => 'EONIA', // BOR and LEN rates
  ];
  const CODE_KEYS = [
    'listCodes',
    'listfintech',
    'listFilters',
    'listBenchmarks',
  ];
  const DATE_KEYS = [
    'endDate',
    'listStartDates',
    'listRebalancingDates',
  ];
  const UNIT_MAP = [
    1 => '',
    100 => '%',
  ];
  const UNIT_PREFIX = 'um_'; // Unit of Measure (Abbrev. U/M or UOM)

  /**
   * Private variables
   */

  private $clients = [];

  /**
   * Public functions
   */

  function getMaxDate($indexOrPortfolio = null, string $maxDate = null): string {
    if (!$maxDate) {
      $maxDate = Helper::get($indexOrPortfolio, 'maxDate');
    }
    return $this->defaultMaxDate($maxDate);
  }

  function getPeriods(&$indexOrPortfolioOrIndices, string $maxDate = null, array $periods = null, bool $noLive = false): array {
    if (empty($periods)) {
      $indices = [];
      if (Helper::is_seque_array($indexOrPortfolioOrIndices)) { // if seque array
        $indices = $indexOrPortfolioOrIndices;
      } else if ($indexOrPortfolioOrIndices instanceof Indexinfo) { // if strategy
        $indices = [$indexOrPortfolioOrIndices];
      } else if ($indexOrPortfolioOrIndices instanceof Portfolio) { // if portfolio
        [$indices] = $this->getActiveStrategiesAndPortfolioStrategies($indexOrPortfolioOrIndices);
      } else {
        throw new Exception('First parameter should be a sequential array or an instance of Indexinfo or Portfolio');
      }
      $periods = $this->getAllPeriods($indices, $maxDate, $noLive);
    }

    // sort periods per desc order
    array_multisort($periods, SORT_DESC);

    return $periods;
  }

  /**
   * Private functions
   */

  protected function getClient(string $name, string $region = 'eu-west-1') {
    $key = $name . '_' . $region;
    if (!isset($this->clients[$key])) {
      $this->clients[$key] = AWS::createClient($name, [
        'region' => $region,
      ]);
    }
    return $this->clients[$key];
  }

  protected function invokeLambda(array $params) {
    $result = $this->getClient('lambda')->invoke($params);
    $statusCode = $result['StatusCode'];
    // if the HTTP status code is not in the 200 range, the request failed
    if ($statusCode < 200 || $statusCode > 299) {
      throw new Exception($result['FunctionError']);
    }
    return $result['Payload']->__toString();
  }

  protected function getBacktestingInput(array $options) {
    // trim some input keys
    Helper::func2keys('trim', $options, self::CODE_KEYS);
    Helper::debug('options', $options);

    // default missing or empty options
    if (empty($options['riskFreeRateTicker'])) {
      $options['riskFreeRateTicker'] = self::TICKERS['RISK_FREE_RATE'];
    }

    // default portfolio options
    if (isset($options['portfolio']) && $options['portfolio'] === true) {

      // default allocation model to Fixed Equity
      if (!isset($options['allocationModel'])) {
        $options['allocationModel'] = 0;
      }

      // default volatility interval to rolling volatility intervals
      if (empty($options['volatilityInterval'])) {
        $options['volatilityInterval'] = self::ROLLING_VOLATILITY_INTERVALS;
      }

      // default var interval depending on allocation model
      if (empty($options['varInterval'])) {
        switch ($options['allocationModel']) {
          case 0:   // Fixed Equity
          case 1:   // Vol Risk Parity
          case 2: { // Equal Risk Contribution
            $options['varInterval'] = Helper::str2int(self::DEFAULT_ALLOCATION['volatility_interval']);
            break;
          }
        }
      }

      // default rebalancing dates to monthly last day
      if (empty($options['listRebalancingDates'])) {
        $startDate = end($options['listStartDates']);
        $endDate = $options['endDate'];
        $options['listRebalancingDates'] = $this->getRebalancingDates($startDate, $endDate);
      }

      // default strategy allocation constraints to equal for Fixed Equity model
      if ($options['allocationModel'] === 0
      && (empty($options['listMinAllocation']) || empty($options['listMaxAllocation']))) {
        $options['listMinAllocation'] = [];
        $options['listMaxAllocation'] = [];
        $defaultAllocation = round(1 / count($options['listCodes']), self::DEC_IN);
        foreach ($options['listCodes'] as $code) {
          $options['listMinAllocation'][] = $defaultAllocation;
          $options['listMaxAllocation'][] = $defaultAllocation;
        }
      }
    }

    // convert string date to excel dates and clean keys
    Helper::func2keys(pipe('strtotime', 'Helper::time2excel'), $options, self::DATE_KEYS);
    Helper::unsetNullValues($options);
    Helper::unsetEmptyArrays($options);

    return $options;
  }

  protected function defaultMaxDate(string $maxDate = null): string {
    if (!$maxDate) {
      $maxDate = Setting::where('key', '=', 'maxDate')->first()->value;
    }
    return $maxDate;
  }

  protected function toStringPeriods(array &$periods) {
    foreach ($periods as $key => $timestamp) {
      $periods[$key] = strftime('%F', $timestamp);
    }
    return $periods;
  }

  protected function toTimestampPeriods(array &$periods) {
    foreach ($periods as $key => $string) {
      $periods[$key] = strtotime($string);
    }
    return $periods;
  }

  protected function getDefaultTimestampPeriods(string $maxDate = null): array {
    $maxDate = strtotime($this->defaultMaxDate($maxDate));

    $periods = [
      // '1m' => Helper::shiftTimeMonths($maxDate, -1),
      // '3m' => Helper::shiftTimeMonths($maxDate, -3),
      '6m' => Helper::shiftTimeMonths($maxDate, -6),
      '1y' => strtotime('-1 year', $maxDate),
      '3y' => strtotime('-3 years', $maxDate),
      '5y' => strtotime('-5 years', $maxDate),
      '10y' => strtotime('-10 years', $maxDate),
    ];

    // if a period starts sun or sat, go back to weekday
    $this->shiftPeriodsToWeekday($periods);

    return $periods;
  }

  protected function getDefaultPeriods(string $maxDate = null): array {
    $periods = $this->getDefaultTimestampPeriods($maxDate);
    return $this->toStringPeriods($periods);
  }

  protected function getAllPeriods(array &$indices = [], string $maxDate = null, bool $noLive = false): array {
    // get strategies history start dates and live dates
    $histStartDates = [];
    $liveDates = [];
    $count = count($indices);
    foreach ($indices as $index) {
      // push strategy history start date if any
      $startDate = Helper::get($index, 'historyStartDate', self::ZERO_DATE);
      if ($startDate < self::MIN_DATE) {
        $startDate = self::MIN_DATE;
      }
      if ($startDate !== self::ZERO_DATE) {
        $histStartDates[] = strtotime($startDate);
      }

      // if live period enabled, push strategy live date if any
      if ($noLive !== true) {
        $liveDate = Helper::get($index, 'inceptionDate', self::ZERO_DATE);
        if ($liveDate < self::MIN_DATE) {
          $liveDate = self::MIN_DATE;
        }
        if ($liveDate !== self::ZERO_DATE) {
          $liveDates[] = strtotime($liveDate);
        }
      }

      // default maxDate if 1 strategy only
      if (!$maxDate && $count === 1) {
        $maxDate = Helper::get($index, 'maxDate');
      }
    }

    // get default timestamp periods and minDate
    $periods = $this->getDefaultTimestampPeriods($maxDate);
    $minDate = end($periods);

    // add newest history start date to min period
    if (!empty($histStartDates)) {
      sort($histStartDates);
      $startDate = end($histStartDates);
      $minDate = $startDate;
    }
    $periods['min'] = $minDate;

    // add newest live date to live period if any
    if (!empty($liveDates)) {
      sort($liveDates);
      $liveDate = end($liveDates);
      if ($liveDate < $minDate) {
        $liveDate = $minDate;
      }
      $periods['live'] = $liveDate;
    }

    // convert periods to strings
    foreach ($periods as $key => $timestamp) {
      $periods[$key] = strftime('%F', $timestamp);
    }
    return $periods;
  }

  protected function shiftPeriodsToWeekday(array &$periods) {
    foreach ($periods as $key => $timestamp) {
      $periods[$key] = Helper::shiftTimeToWeekday($timestamp);
    }
  }

  protected function getActiveStrategiesAndPortfolioStrategies(Portfolio $portfolio) {
    $byActive = function($o) { return !$o->suspended && !$o->indexinfo->is_suspended; };
    $portfolioStrategies = $portfolio
      ->getPortfolioStrategiesWithIndexinfo()
      ->filter($byActive);
    return [
      $portfolioStrategies->pluck('indexinfo')->all(), // active strategies
      $portfolioStrategies->all(), // active portfolioStrategies
    ];
  }

  protected function getPortfolioOptions(Portfolio $portfolio, array &$portfolioStrategies, array &$strategies, array &$periods, string $maxDate) {
    // map allocation model
    switch ($portfolio->weighting) {
      case 'equal':
      case 'manual': $allocationModel = 0;  break; // Fixed Equity
      case 'risk':   $allocationModel = 1;  break; // Vol Risk Parity
      case 'erc':    $allocationModel = 2;  break; // Equal Risk Contribution
      case 'custom': $allocationModel = -1; break; // Custom Rebalancing
      default:       $allocationModel = 0;  break; // Fixed Equity by default
    }

    if ($allocationModel === -1) {
      return $this->getCustomPortfolioOptions($portfolio, $strategies, $periods, $maxDate);
    }

    // map list codes
    $listCodes = Helper::map($strategies, 'code');
    
    // default null allocation options
    $allocationOption = json_decode($portfolio->allocationOptions, true);
    if (is_array($allocationOption)) {
      $allocationOption = array_merge(self::DEFAULT_ALLOCATION, $allocationOption);
      $default = false;
    } else {
      $allocationOption = self::DEFAULT_ALLOCATION;
      $default = true;
    }
    Helper::debug('alloc', $allocationOption);

    // map general allocation options
    $listStartDates = array_values($periods);
    $startDate = end($listStartDates);
    $endDate = $maxDate;
    $listRebalancingDates = $this->getRebalancingDates($startDate, $endDate, $allocationOption['rebalancing'], $allocationOption['rebalancing_month'], $allocationOption['rebalancing_date']);
    $rebalancingCost = round($allocationOption['rebalancing_cost'] / 100, self::DEC_IN) ?: null;
    $replicationCost = round($allocationOption['replication_cost'] / 100, self::DEC_IN) ?: null;

    // map scaling options (Fixed Leverage or Target Volatility)
    $scalingType = -1;
    $leverageOrVol = null;
    $targetVolMinLeverage = null;
    $targetVolMaxLeverage = null;
    $volatilityTargetInterval = null;
    switch ($allocationOption['leverage_type']) {
      case 'Fixed Leverage': {
        $scalingType = 1;
        $leverageOrVol = round($allocationOption['fixed_leverage'] / 100, self::DEC_IN);
        break;
      }
      case 'Target Volatility': {
        $scalingType = 2;
        $leverageOrVol = round($allocationOption['target_volatility'] / 100, self::DEC_IN);
        $targetVolMinLeverage = round($allocationOption['leverage_min'] / 100, self::DEC_IN) ?: 0; // if not provided 0 (0%)
        $targetVolMaxLeverage = round($allocationOption['leverage_max'] / 100, self::DEC_IN) ?: 2; // if not provided 2 (200%)
        $volatilityTargetInterval = Helper::str2int($allocationOption['target_interval'] ?: self::DEFAULT_ALLOCATION['target_interval']);
        break;
      }
    }

    // map lending and borrowing rates for scaling options
    $lendingTicker = null;
    $lendingRateAdd = null;
    $borrowTicker = null;
    $borrowRateAdd = null;
    // if ($scalingType > -1) { // right now always none
    //   $currency = $allocationOption['funding_currency'];
    //   if (!empty($currency) && $currency !== self::NONE) {
    //     $lendingTicker = self::TICKERS[$currency . '_RATE'];
    //     $lendingRateAdd = round($allocationOption['lending_spread'] / 100, self::DEC_IN) ?: null;
    //     $borrowTicker = self::TICKERS[$currency . '_RATE'];
    //     $borrowRateAdd = round($allocationOption['borrowing_spread'] / 100, self::DEC_IN) ?: null;
    //   }
    // }

    // map model specific allocation options
    // primary allocation constraint on Strategy or equal/manual weightings
    $listMinAllocation = [];
    $listMaxAllocation = [];
    // secondary allocation constraint on Asset Class or Factor
    $useAssetClassAsFactor = -1; // -1: no secondary; 1: Asset Class; 2: Factor
    $listMinAllocationFactor = [];
    $listMaxAllocationFactor = [];
    // var interval in business days
    $varInterval = null;
    switch ($allocationModel) {
      case 0: { // Fixed Equity
        // disable: Allocation Constraints
        switch ($portfolio->weighting) {
          case 'equal':
            $listMinAllocation = [];
            $listMaxAllocation = [];
            $defaultAllocation = round(1 / count($listCodes), self::DEC_IN);
            foreach($listCodes as $code) {
              $listMinAllocation[] = $defaultAllocation;
              $listMaxAllocation[] = $defaultAllocation;
            }
            break;
          case 'manual':
            $mapWeightings = map(function($o) { return round($o['weighting'] / 100, self::DEC_IN); });
            $weightings = $mapWeightings($portfolioStrategies);
            $listMinAllocation = $weightings;
            $listMaxAllocation = $weightings;
            break;
        }
        break;
      }
      case 1:   // Vol Risk Parity
      case 2: { // Equal Risk Contribution
        // require: varInterval
        $varInterval = Helper::str2int($allocationOption['volatility_interval'] ?: self::DEFAULT_ALLOCATION['volatility_interval']);
        break;
      }
    }
    if ($allocationModel > 0) { // only for advanced models
      // Allocation Constraints ("NONE", "Asset Class", "Factor", "Strategy")
      $primary = $allocationOption['allocationConstraints']['primary'];
      if ($primary['type'] === self::STRATEGY) {
        $minAllocation = $this->getAllocations($primary, 'min', self::STRATEGY);
        $maxAllocation = $this->getAllocations($primary, 'max', self::STRATEGY);
        foreach ($strategies as $index) {
          $listMinAllocation[] = $minAllocation;
          $listMaxAllocation[] = $maxAllocation;
        }
      }
      $secondary = $allocationOption['allocationConstraints']['secondary'];
      if ($secondary['type'] === self::ASSET_CLASS) {
        $useAssetClassAsFactor = 1;
        $listMinAllocationFactor = $this->getAllocations($secondary, 'min', self::ASSET_CLASSES);
        $listMaxAllocationFactor = $this->getAllocations($secondary, 'max', self::ASSET_CLASSES);

      } else if ($secondary['type'] === self::FACTOR) {
        $useAssetClassAsFactor = 2;
        $listMinAllocationFactor = $this->getAllocations($secondary, 'min', self::FACTORS);
        $listMaxAllocationFactor = $this->getAllocations($secondary, 'max', self::FACTORS);
      }
    }

    // map assetClass and factorClass
    $mapCodeAssetAndFactor = map(function($o) { return [
      'code' => $o->code,
      'assetClass' => array_search($o->assetClass, self::ASSET_CLASSES),
      'factorClass' => array_search($o->factor, self::FACTORS),
    ]; });
    $keyBy = curry(['Helper', 'keyBy']);
    $assets = pipe(
      $mapCodeAssetAndFactor,
      $keyBy(__(), 'code')
    )($strategies);

    return array_merge([
      'listCodes' => $listCodes,
      'listStartDates' => $listStartDates,
      'endDate' => $endDate,
      'allocationModel' => $allocationModel,
      'listRebalancingDates' => $listRebalancingDates,
      'rebalancingCost' => $rebalancingCost,
      'replicationCost' => $replicationCost,
      'scalingType' => $scalingType,
      'leverageOrVol' => $leverageOrVol,
      'targetVolMinLeverage' => $targetVolMinLeverage,
      'targetVolMaxLeverage' => $targetVolMaxLeverage,
      'volatilityTargetInterval' => $volatilityTargetInterval,
      'listMinAllocation' => $listMinAllocation,
      'listMaxAllocation' => $listMaxAllocation,
      'useAssetClassAsFactor' => $useAssetClassAsFactor,
      'listMinAllocationFactor' => $listMinAllocationFactor,
      'listMaxAllocationFactor' => $listMaxAllocationFactor,
      'varInterval' => $varInterval,
      'lendingTicker' => $lendingTicker,
      'lendingRateAdd' => $lendingRateAdd,
      'borrowTicker' => $borrowTicker,
      'borrowRateAdd' => $borrowRateAdd,
      'default' => $default,
      'portfolio' => true,
    ], $assets);
  }

  protected function getCustomPortfolioOptions(Portfolio $portfolio, array &$strategies, array &$periods, string $maxDate) {
    // map list codes
    $listCodes = Helper::map($strategies, 'code');

    $listRebalancingDates = [];
    $manualWeights = [];
    $rebalancingCost = [];
    $replicationCost = [];
    $listStartDates = array_values($periods);
    $startDate = end($listStartDates);
    $endDate = $maxDate;

    $customRebalancing = json_decode($portfolio->customRebalancing, true);
    $customAllocation = $customRebalancing['customAllocation'];

    //funding things
    $rateTickers = [];
    $lendRateAdd = [];
    $borrowRateAdd = [];
    $fundingWeights = [];
    $rateMod = [];
    $manualRate = [];

    foreach($customAllocation as $date => $allocation) {
      if ($date <= $endDate && $date >= $startDate) {
        $weightings = [];
        $rebalancingArrayCost = [];
        $replicationArrayCost = [];
        foreach($listCodes as $code) {
          $weightings[] = round($allocation[$code]['weighting'] / 100, self::DEC_IN);
          $rebalancingArrayCost[] = round($allocation[$code]['rebalancing_cost'] / 100, self::DEC_IN) ?: 0;
          $replicationArrayCost[] = round($allocation[$code]['replication_cost'] / 100, self::DEC_IN) ?: 0;
        }
        $listRebalancingDates[] = $date;
        $manualWeights[] = $weightings;
        $replicationCost[] = $replicationArrayCost;
        $rebalancingCost[] = $rebalancingArrayCost;
        $fundingWeights[] = round(isset($allocation['funding_weight']) ? ($allocation['funding_weight']  / 100) : 1, self::DEC_IN);
        //set funding options
        $fundingOption = $allocation['funding_currency'];
        switch ($fundingOption) {
          case 'USD': 
            $rateTickers[] = self::TICKERS['USD_RATE'];
            $rateMod[] = 0;
            break;
          case 'EUR':
            $rateTickers[] = self::TICKERS['EUR_RATE']; 
            $rateMod[] = 0;
            break;
          case 'Manual':
            $rateTickers[] = 'None';
            $rateMod[] = 1;
            $manualRate[] = round($allocation['funding_rate'] / 100, self::DEC_IN) ?: 0;
            break;
          default: 
            $rateTickers[] = 'None'; 
            $rateMod[] = 0;
            break;
        }
        $lendRateAdd[] = round($allocation['lending_spread'] / 100, self::DEC_IN) ?: 0;
        $borrowRateAdd[] = round($allocation['borrowing_spread'] / 100, self::DEC_IN) ?: 0;
      }
    }

    // map assetClass and factorClass
    $mapCodeAssetAndFactor = map(function($o) { return [
      'code' => $o->code,
      'assetClass' => array_search($o->assetClass, self::ASSET_CLASSES),
      'factorClass' => array_search($o->factor, self::FACTORS),
    ]; });

    $keyBy = curry(['Helper', 'keyBy']);
    $assets = pipe(
      $mapCodeAssetAndFactor,
      $keyBy(__(), 'code')
    )($strategies);
    
    return array_merge([
      'listCodes' => $listCodes,
      'listStartDates' => $listStartDates,
      'endDate' => $endDate,
      'allocationModel' => -1,
      'listRebalancingDates' => $listRebalancingDates,
      'manualWeights' => $manualWeights,
      'replicationCost' => $replicationCost,
      'rebalancingCost' => $rebalancingCost,
      'fundingWeights' => $fundingWeights,
      'funding' => [
        'rateTickers' => $rateTickers,
        'lendRateAdd' => $lendRateAdd,
        'borrowRateAdd' => $borrowRateAdd,
        'rateMod' => $rateMod,
        'manualRate' => $manualRate,
      ],
      'default' => false,
      'portfolio' => true,
    ], $assets);
  }

  protected function getRebalancingDates(string $startDate, string $endDate, string $rebalancing = 'Monthly', string $rebalancingMonth = 'Every Month', string $rebalancingDay = 'Last Day'): array {
    $rebalancingDates = [$startDate];
    $startDate = strtotime($startDate);
    $endDate = strtotime($endDate);
    if ($endDate <= $startDate) {
      throw new Exception('The endDate should be superior to startDate');
    }

    switch ($rebalancing) {
      case 'Monthly':     $monthFreq = 1; break;
      case 'Quarterly':   $monthFreq = 3; break;
      case 'Semi-annual': $monthFreq = 6; break;
      case 'Annual':      $monthFreq = 12; break;
      default:            $monthFreq = 1;
    }
    switch (explode(' ', $rebalancingMonth)[0]) {
      case 'Jan': $startMonth = '01'; break;
      case 'Feb': $startMonth = '02'; break;
      case 'Mar': $startMonth = '03'; break;
      case 'Apr': $startMonth = '04'; break;
      case 'May': $startMonth = '05'; break;
      case 'Jun': $startMonth = '06'; break;
      case 'Jul': $startMonth = '07'; break;
      case 'Aug': $startMonth = '08'; break;
      case 'Sep': $startMonth = '09'; break;
      case 'Oct': $startMonth = '10'; break;
      case 'Nov': $startMonth = '11'; break;
      case 'Dec': $startMonth = '12'; break;
      default:    $startMonth = null;
    }
    $rebalancingDay = ($rebalancingDay !== 'Last Day') ? intval($rebalancingDay) : 'last';

    $startDate = Helper::toDate($startDate);
    $endDate = Helper::toDate($endDate);
    $date = Helper::toDate($startDate);
    if ($startMonth) {
      while ($date->format('m') !== $startMonth) {
        $date->modify('first day of next month');
      }
    }
    $date = Helper::shiftDateToDayOfMonth($date, $rebalancingDay);
    $weekDate = Helper::shiftDateToWeekday($date);
    if ($weekDate > $startDate) {
      $rebalancingDates[] = $weekDate->format('Y-m-d');
    }

    while (1) {
      $date = Helper::shiftDateMonths($date, $monthFreq);
      $date = Helper::shiftDateToDayOfMonth($date, $rebalancingDay);
      $weekDate = Helper::shiftDateToWeekday($date);
      if ($weekDate > $endDate) break;
      $rebalancingDates[] = $weekDate->format('Y-m-d');
    }
    return $rebalancingDates;
  }

  protected function getAllocations(array &$allocation, string $type, $keys) {
    $default = ($type === 'min') ? 0 : 1; // if not provided default to min 0 or max 1
    $type = ucfirst($type);
    $getAllocation = function($key) use(&$allocation, $type, $default) {
      $value = Helper::get($allocation['allocations' . $type], $key);
      return is_numeric($value) ? round($value / 100, self::DEC_IN) : $default;
    };
    return is_array($keys) ? array_map($getAllocation, $keys) : $getAllocation($keys);
  }

  protected function getSample(string $minDate, string $maxDate = null, bool $forceDaily = false): string {
    if ($forceDaily !== true) {
      $minDate = strtotime($minDate);
      $maxDate = strtotime($this->defaultMaxDate($maxDate));

      if ($maxDate <= $minDate) {
        throw new Exception('The maxDate should be superior to minDate');
      }

      $diff = $maxDate - $minDate; // in milliseconds
      if ($diff > 93744000) { // if diff > 35 months (self::SEC_IN_DAY * 31 * 35)
        return 'weekly';
      }
    }
    return 'daily';
  }

}
