<?php

namespace App\Services\Lab;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\JsonResponse;
use App\Services\Constant;
use App\Indexinfo;
use App\Portfolio;
use Exception;
use Helper;
use Log;
use Throwable;
use function Tarsana\Functional\{ pipe };

class RegressionServiceImpl extends AbstractLabService implements RegressionService, Constant {

  /**
   * Public constants
   */

  const METRICS_MAP = [ // 'sourceKey' => [factor, 'targetKey']
    'beta'              => [  1, 'beta'       ], // Beta Coefficient
    'betaSE'            => [  1, 'betaSE'     ], // Beta Standard Error
    'tStat'             => [  1, 'tStat'      ], // T-stat
    'pValue'            => [100, 'pValue'     ], // P-value               %
    'lowerBound'        => [  1, 'lowerBound' ], // Lower 95%
    'upperBound'        => [  1, 'upperBound' ], // Upper 95%
    'rSquared'          => [  1, 'rSquared'   ], // R-squared
    'rSquaredAdj'       => [  1, 'rSquaredAdj'], // Adjusted R-squared
    'fStat'             => [  1, 'fStat'      ], // F-stat
    'fProb'             => [  1, 'fProb'      ], // F-prob (Prob > F)
    'varAttribution'    => [100, 'varAttr'    ], // Variance Attribution  %
    'varProp'           => [100, 'varProp'    ], // Variance Attribution  %
    'returnAttribution' => [100, 'returnAttr' ], // Return Attribution    %
    'returnProp'        => [100, 'returnProp' ], // Return Proportion     %
  ];

  const SPECIAL_DEC_OUT = [
    'varAttribution' => 3,
  ];

  /**
   * Public functions
   */

  function calcul($indexOrPortfolio, array $options = []) {
    // default options
    $options = array_merge([
      'maxDate' => null,
      'periods' => null,
      'period' => '5y',
      'listfintech' => null,
      'movingWindow' => 'None', // ['None', 'Yearly', 'Quarterly', 'Monthly']
      'modelInterval' => 250, // in days
      'returnInterval' => 'Daily', // ['Daily', 'Weekly', 'Monthly', 'Quarterly', 'Yearly']
      'metricsOnly' => false,
      'fullTracks' => false,
      'noFactor' => false,
      'selection' => 0, // 1 - 12,
    ], $options);

    // cast the first parameter into index or portfolio
    if ($indexOrPortfolio instanceof Indexinfo) {
      $strategy = clone($indexOrPortfolio);
      $strategies = [$strategy];
    } else if ($indexOrPortfolio instanceof Portfolio) {
      $portfolio = clone($indexOrPortfolio);
      [$strategies, $portfolioStrategies] = $this->getActiveStrategiesAndPortfolioStrategies($portfolio);
    } else {
      throw new Exception('First parameter should be an instance of Indexinfo or Portfolio');
    }

    // get maxDate and periods
    $maxDate = $this->getMaxDate($indexOrPortfolio, $options['maxDate']);
    $periods = $this->getPeriods($strategies, $maxDate, $options['periods']);

    // keep only the specified period
    $period = $options['period'];
    $minDate = ($periods[$period] > self::REG_MIN_DATE) ? $periods[$period] : self::REG_MIN_DATE;

    $listfintech = Helper::get($options, 'listfintech') ?: [];
    $factorfintechs = Indexinfo::whereIn('code', $listfintech)->get()->toArray();

    foreach($factorfintechs as $factorfintech) {
      if ($factorfintech['historyStartDate'] > $minDate) {
        $minDate = $factorfintech['historyStartDate'];
      }
    }

    $options['periods'] = [
      $period => $minDate,
    ];
    // get regression payload
    $options['maxDate'] = $maxDate;
    if (isset($strategy)) {
      $options['listCodes'] = [$strategy->code];
    }
    $payload = $this->getRegressionPayload($options, false);

    if (isset($portfolio)) {
      // add portfolio options
      $portfolioMaxDate = $this->getMaxDate($indexOrPortfolio);
      $portfolioPeriods = $this->getPeriods($strategies, $portfolioMaxDate);
      $portfolioPeriods = ['min' => $portfolioPeriods['min']];
      $portfolioOpts = $this->getPortfolioOptions($portfolio, $portfolioStrategies, $strategies, $portfolioPeriods, $portfolioMaxDate);
      $payload['portfolio'] = $this->getBacktestingInput($portfolioOpts);
    }

    // invoke regression lambda
    $result = $this->invokeRegressionLambda($payload);

    // map output
    $output = [];
    $suffix = '_0';
    $unitsKey = 'units';
    $decimalsKey = 'decimals';
    $datesKey = 'dates';
    $summaryKey = 'summary';
    $trackSuffix = '_track';
    $trackDatesKey = 'trackDates';
    $trackAlphaKey = 'trackAlpha';
    $timeseriesDates = 'timeseriesDates';
    $timeseriesCumDiff = 'timeseriesCumDiff';
    $codes = ['alpha'];
    try {
      foreach ($result['activefintech'] as $fintechIdx) {
        $codes[] = $options['listfintech'][$fintechIdx];
      }
      foreach ($codes as $code) {
        $output[$code] = [];
      }
      $output[$summaryKey] = [];
      $output[$unitsKey] = [];

      // map metrics units
      foreach (self::METRICS_MAP as $sourceKey => list($factor, $targetKey)) {
        if ($options['noFactor'] === true) $factor = 1;
        $output[$unitsKey][$targetKey] = self::UNIT_MAP[abs($factor)];
      }

      // map metrics dec out
      foreach (self::METRICS_MAP as $sourceKey => list($factor, $targetKey)) {
        if ($options['noFactor'] === true) {
          $output[$decimalsKey][$targetKey] = self::DEC_IN;
        } else if (isset(self::SPECIAL_DEC_OUT[$sourceKey])) {
          $output[$decimalsKey][$targetKey] = self::SPECIAL_DEC_OUT[$sourceKey];
        } else {
          $output[$decimalsKey][$targetKey] = self::DEC_OUT;
        }
      }

      // map metrics fields
      foreach (self::METRICS_MAP as $sourceKey => list($factor, $targetKey)) {
        if ($options['noFactor'] === true) $factor = 1;
        $sourceValue = $result[$sourceKey . $suffix];
        if (is_array($sourceValue)) {
          foreach ($sourceValue as $idx => $value) {
            $output[$codes[$idx]][$targetKey] = round($factor * $value, $output[$decimalsKey][$targetKey]);
            if ($options['metricsOnly'] === false) {
              $output[$codes[$idx]][$targetKey . $trackSuffix] = [];
            }
          }
        } else {
          $output[$summaryKey][$targetKey] = round($factor * $sourceValue, $output[$decimalsKey][$targetKey]);
          if ($options['metricsOnly'] === false) {
            $output[$summaryKey][$targetKey . $trackSuffix] = [];
          }
        }
      }

      if ($options['metricsOnly'] === false) {
        // map timeseries to tracks
        $needFillMissingDate = $options['fullTracks'];
        $output[$datesKey] = [];
        $rollingDates = [];
        foreach (self::METRICS_MAP as $sourceKey => list($factor, $targetKey)) {
          $rollingDates = $result['rollingDates'];
          $sourceKey = 'rolling' . ucfirst($sourceKey);
          $sourceKeyValues = $result[$sourceKey];
          if ($needFillMissingDate) {
            Helper::fillMissingDatesAndValue($rollingDates, $sourceKeyValues);
          }
          foreach ($sourceKeyValues as $sourceValue) {
            if (is_array($sourceValue)) {
              foreach ($sourceValue as $idx => $value) {
                $output[$codes[$idx]][$targetKey . $trackSuffix][] = round($factor * $value, $output[$decimalsKey][$targetKey]);
              }
            } else {
              $output[$summaryKey][$targetKey . $trackSuffix][] = round($factor * $sourceValue, $output[$decimalsKey][$targetKey]);
            }
          }
        }
        foreach ($rollingDates as $date) {
          $output[$datesKey][] = strftime('%F', Helper::excel2time($date));
        }
        $output[$trackDatesKey] = [];
        $output[$trackAlphaKey] = [];
        $fac = ($options['noFactor'] === true) ? 1 : 100;
        $dec = ($options['noFactor'] === true) ? self::DEC_IN : self::DEC_OUT;
        $rollingTimeseriesDates = $result['rollingTimeseriesDates'];
        $rollingTimeseriesValue = $result['rollingTimeseriesCumDiff'];
        if ($needFillMissingDate) {
          Helper::fillMissingDatesAndValue($rollingTimeseriesDates, $rollingTimeseriesValue);
        }
        foreach ($rollingTimeseriesDates as $idx => $date) {
          $output[$trackDatesKey][] = strftime('%F', Helper::excel2time($date));
          $output[$trackAlphaKey][] = round($fac * $rollingTimeseriesValue[$idx], $dec);
        }
        // add timeseries alpha track
        $output['alpha']['cumulated_track'] = [];
        $output['alpha']['cumulated_dates'] = [];
        foreach ($result[$timeseriesDates . $suffix] as $idx => $date) {
          $output['alpha']['cumulated_track'][] = round($fac * $result[$timeseriesCumDiff . $suffix][$idx], $dec);
          $output['alpha']['cumulated_dates'][] = strftime('%F', Helper::excel2time($date));
        }
      }

      $output['periods'] = $options['periods'];
    } catch (Throwable $e) {
      Log::error($e);
      $output = [
        'input' => $payload,
        'phpError' => $e->getMessage(),
        'lambda' => $result,
      ];
      $response = new JsonResponse($output, 500);
      return $response;
    }
    return $output;
  }

  function getRegression($indexOrPortfolio, array $options = []) {
    $options = $this->getRegressionOptions($options);
    return $this->calcul($indexOrPortfolio, $options);
  }

  function getHeatmap($indexOrPortfolio, array $options = []) {
    // default options
    $options = array_merge([
      'maxDate' => null,
      'periods' => null,
      'period' => '5y',
      'listfintech' => null,
      'movingWindow' => 'None', // ['None', 'Yearly', 'Quarterly', 'Monthly']
      'modelInterval' => 250, // in days
      'returnInterval' => 'Daily', // ['Daily', 'Weekly', 'Monthly', 'Quarterly', 'Yearly']
      'metricsOnly' => false,
      'noFactor' => false,
      'selection' => 0, // 1 - 12,
    ], $options);

    // cast the first parameter into index or portfolio
    if ($indexOrPortfolio instanceof Indexinfo) {
      $strategy = clone($indexOrPortfolio);
      $strategies = [$strategy];
    } else if ($indexOrPortfolio instanceof Portfolio) {
      $portfolio = clone($indexOrPortfolio);
      [$strategies, $portfolioStrategies] = $this->getActiveStrategiesAndPortfolioStrategies($portfolio);
    } else {
      throw new Exception('First parameter should be an instance of Indexinfo or Portfolio');
    }

    // get maxDate and periods
    $maxDate = $this->getMaxDate($indexOrPortfolio, $options['maxDate']);
    $periods = $this->getPeriods($strategies, $maxDate, $options['periods']);

    // keep only the specified period
    $period = $options['period'];
    $options['periods'] = [
      $period => $periods[$period],
    ];

    // get regression payload
    $options['maxDate'] = $maxDate;
    if (isset($strategy)) {
      $options['listCodes'] = [$strategy->code];
    }
    $payload = $this->getRegressionPayload($options, true);
    if (isset($portfolio)) {
      // add portfolio options
      $portfolioMaxDate = $this->getMaxDate($indexOrPortfolio);
      $portfolioPeriods = ['min' => $periods['min']];
      $portfolioOpts = $this->getPortfolioOptions($portfolio, $portfolioStrategies, $strategies, $portfolioPeriods, $portfolioMaxDate);
      $payload['portfolio'] = $this->getBacktestingInput($portfolioOpts);
    }

    // invoke regression lambda
    $result = $this->invokeHeatmapLambda($payload);
    $output = [];
    try {
      $heatmap= $result['heatmap_0'];
      if($heatmap) {
        foreach ($heatmap as $groupName => $groupFactors) {
          $output[$groupName] = [];
          if ($groupFactors) {
            foreach ($groupFactors as $factorname => $factor) {
              if ($factorname === 'Market Beta') {
                $output[$groupName]['Benchmark'] = round($factor * 100, 2);
              } else {
                $output[$groupName][$factorname] = round($factor * 100, 2);
              }
            }
          }
        }
      }
    } catch (Throwable $e) {
      Log::error($e);
      $output['input'] = $payload;
      $output['phpError'] = $e->getMessage();
      $output['lambda'] = $result;
      $response = new JsonResponse($output, 500);
      return $response;
    }
    
    return $output;
  }

  /**
   * Private functions
   */

  protected function invokeRegressionLambda(array $payload, bool $toArray = true) {
    $result = $this->invokeLambda([
      'FunctionName' => env('SLS_REGR_FUNC'),
      'Payload' => json_encode($payload),
    ]);
    return json_decode($result, $toArray);
  }

  protected function invokeHeatmapLambda(array $payload, bool $toArray = true) {
    $result = $this->invokeLambda([
      'FunctionName' => env('SLS_HEAT_FUNC'),
      'Payload' => json_encode($payload),
    ]);
    return json_decode($result, $toArray);
  }

  protected function getRegressionPayload(array $options, bool $isHeatmap = false) {
    switch ($options['movingWindow']) {
      case 'None':      $movingWindow = -1; break;
      case 'Yearly':    $movingWindow = 1; break;
      case 'Quarterly': $movingWindow = 2; break;
      case 'Monthly':   $movingWindow = 3; break;
      default:          $movingWindow = -1;
    }

    switch ($options['returnInterval']) {
      case 'Daily':     $returnInterval = 0; break;
      case 'Weekly':    $returnInterval = 1; break;
      case 'Monthly':   $returnInterval = 2; break;
      case 'Quarterly': $returnInterval = 3; break;
      case 'Yearly':    $returnInterval = 4; break;
      default:          $returnInterval = 0;
    }

    $payload = [
      'listCodes' => Helper::get($options, 'listCodes'),
      'listStartDates' => array_values($options['periods']),
      'endDate' => $options['maxDate'],
      'listfintech' => $options['listfintech'],
      'movingWindow' => $movingWindow,
      'modelInterval' => intval(Helper::get($options, 'modelInterval', 250)),
      'returnInterval' => $returnInterval,
      'selection' => $options['selection'],
    ];

    if ($isHeatmap) {
      unset($payload['listfintech']);
      unset($payload['selection']);
    }

    // trim some input keys
    Helper::func2keys('trim', $payload, self::CODE_KEYS);

    // convert string date to excel dates and clean keys
    Helper::func2keys(pipe('strtotime', 'Helper::time2excel'), $payload, self::DATE_KEYS);

    return ['regression' => $payload];
  }

  protected function getRegressionOptions(array $options, bool $isHeatmap = false) {
    $payload = [
      'maxDate' => Helper::get($options, 'maxDate'),
      'period' => Helper::get($options, 'period'),
      'movingWindow' => Helper::get($options, 'movingWindow'),
      'modelInterval' => Helper::get($options, 'interval'),
      'returnInterval' => Helper::get($options, 'returnInterval'),
      'periods' => Helper::get($options, 'periods'),
      'metricsOnly' => Helper::get($options, 'metricsOnly') === true,
      'fullTracks' => Helper::get($options, 'fullTracks') === true,
      'noFactor' => Helper::get($options, 'noFactor') === true,
    ];
    if (!$isHeatmap) {
      $payload['selection'] = Helper::get($options, 'lassoFilters') ?: 0;
      $listfintech = [];
      $factorList = Helper::get($options, 'factorList') ?: [];
      foreach($factorList as $factor) {
        $listfintech[] = $factor['code'];
      }
      $payload['listfintech'] = $listfintech;
    }
    return $payload;
  }

}
