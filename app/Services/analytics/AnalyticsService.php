<?php

namespace App\Services\Analytics;

interface AnalyticsService {

  function identify($option);
  function track($option);
  function page($option);

}
