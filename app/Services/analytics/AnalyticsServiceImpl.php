<?php

namespace App\Services\Analytics;
use Segment;
use Throwable;
use Log;


class AnalyticsServiceImpl implements AnalyticsService {

  /**
   * Public constructor
   */

  public function __construct() {
    $key = env('ANALYTICS_KEY');
    if ($key) {
      try {
        Segment::init($key);
        $this->enabled = true;
      } catch(Throwable $e) {
        Log::error($e);
      }
    }
  }

  public function identify($option) {
    if ($this->enabled) {
      try {
        Segment::identify($option);
      } catch(Throwable $e) {
        Log::error($e);
      }
    }
  }

  public function track($option) {
    if ($this->enabled) {
      try {
        Segment::track($option);
      } catch(Throwable $e) {
        Log::error($e);
      }
    }
  }

  public function page($option) {
    if ($this->enabled) {
      try {
        Segment::page($option);
      } catch(Throwable $e) {
        Log::error($e);
      }
    }
  }

}
