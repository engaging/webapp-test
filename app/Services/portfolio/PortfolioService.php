<?php

namespace App\Services\Portfolio;
use App\User;

interface PortfolioService {

  function createFavouriteDraft(User $user);
  // function createPortfolioDraft(User $user, $slug);
}
