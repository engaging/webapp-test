<?php

namespace App\Services\Portfolio;
use App\Portfolio;
use App\PortfolioStrategy;
use App\Indexinfo;
use App\User;
use DB;
use Helper;

class PortfolioServiceImpl implements PortfolioService {

  /**
   * Public functions
   */

  function createFavouriteDraft(User $user){
    $favouritesDraft = Portfolio::where(
      'user_id', '=', $user->id
    )->where(
        'draft_of', '=', 0
    );

    if($favouritesDraft->count() == 0){
      $draft = new Portfolio;
      $draft->draft_of = 0;
      $draft->user_id = $user->id;
      $draft->title = "Favourites";
      $draft->weighting = 'equal';
      $draft->hash = time();
      $draft->save();
    } else {
      $draft = $favouritesDraft->first();
    }
    $deleted = DB::delete('delete from portfolio_strategies WHERE portfolio_id = ' . $draft->id);
    $favourites = $user->favouriteIndices();
    foreach ($favourites->take(100)->get() as $favourite) {
        $PortfolioStrategy = new PortfolioStrategy;
        $PortfolioStrategy->indexinfo_id = $favourite->index_id;
        $PortfolioStrategy->portfolio_id = $draft->id;
        $PortfolioStrategy->user_id = $user->id;
        $PortfolioStrategy->weighting = 0;
        $PortfolioStrategy->save();
    }
    $draft->calculateWeightings("*");
    $draft->save();
    return $draft;
  }

  function createDefaultPortfolio(User $user) {


    $defaultPortfolio = new Portfolio;
    $defaultPortfolio->user_id = $user->id;
    $defaultPortfolio->title = "Default Portfolio";
    $defaultPortfolio->weighting = 'equal';
    $defaultPortfolio->hash = time();
    $defaultPortfolio->save();
   
    $deleted = DB::delete('delete from portfolio_strategies WHERE portfolio_id = ' . $defaultPortfolio->id);

    $defaultStrategieCodes = [];
    $defaultStrategies = Indexinfo::whereIn('code', $defaultStrategieCodes)->get()->all();
    foreach ($defaultStrategies as $strategy) {
      $PortfolioStrategy = new PortfolioStrategy;
      $PortfolioStrategy->indexinfo_id = $strategy->index_id;
      $PortfolioStrategy->portfolio_id = $defaultPortfolio->id;
      $PortfolioStrategy->user_id = $user->id;
      $PortfolioStrategy->weighting = 0;
      $PortfolioStrategy->save();
    }
    $defaultPortfolio->calculateWeightings("*");
    $defaultPortfolio->save();
    return $defaultPortfolio;
  }
}
