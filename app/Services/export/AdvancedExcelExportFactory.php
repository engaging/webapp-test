<?php

namespace App\Services\Export;

use App\Services\Factory;
use Exception;

class AdvancedExcelExportFactory implements Factory {

  static function make(string $name): AdvancedExcelExportService {
    switch ($name) {
      case 'default':
        return new AdvancedExcelExportServiceImpl();
      default:
        throw new Exception('No service associated with ' . $name . ' name');
    }
  }

}
