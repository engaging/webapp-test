<?php

namespace App\Services\Export;
use App\Portfolio;
use App\Indexinfo;

interface AdvancedExcelExportService extends ExcelExportService {

  function exportExcelStrategy(Indexinfo $strategy, array $options = []);
  function exportExcelPCA(Portfolio $portfolio, array $options = []);
  function exportExcelRegressionPortfolio(Portfolio $portfolio, array $options = []);
  function exportExcelRegressionStrategy(Indexinfo $strategy, array $options = []);

}
