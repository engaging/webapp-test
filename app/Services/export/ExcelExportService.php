<?php

namespace App\Services\Export;
use App\Portfolio;

interface ExcelExportService {

  function exportExcelPortfolio(Portfolio $portfolio, array $options = []);

}
