<?php

namespace App\Services\Export;

use App\Services\Constant;
use App\Portfolio;
use Helper;
use Exception;

abstract class AbstractAdvancedExcelExportService extends AbstractExcelExportService implements AdvancedExcelExportService {

  /**
   * Private functions
   */

   protected function appendPCAModel(&$data, $row, $col, $pcaOptions, $maxDate, $minDate, $countDates) {
     $this->appendRow($data, $row++, $col, ['PCA Model', null]);
     $this->appendRow($data, $row++, $col, ['Start Date', $minDate]);
     $this->appendRow($data, $row++, $col, ['End Date', $maxDate]);
     $this->appendRow($data, $row++, $col, ['Cov Type', Helper::get($pcaOptions, 'covType', 'Covariance')]);
     $this->appendRow($data, $row++, $col, ['Return Interval', Helper::get($pcaOptions, 'returnInterval', 'Daily')]);
     $listFilters = Helper::get($pcaOptions, 'listFilters', []);
     $marketBeta = !empty($listFilters) ? $listFilters[0] : self::NONE;
     $threshold = Helper::get($pcaOptions, 'filter', self::NONE);
     if ($marketBeta === self::NONE && $threshold === self::NONE) {
       $this->appendRow($data, $row++, $col, ['Filtering Settings', 'No']);
     } else {
       $this->appendRow($data, $row++, $col, ['Filtering Settings', 'Yes']);
       $this->appendRow($data, $row++, $col, ['Market Beta', $marketBeta]);
       $this->appendRow($data, $row++, $col, ['Threshold', $threshold]);
     }
     $this->appendRow($data, $row++, $col, ['Number of Observation', strval($countDates)]);
   }

   protected function appendRegressionModel(&$data, $row, $col, $regOptions, $maxDate, $minDate) {
     $this->appendRow($data, $row++, $col, ['Regression Model', null]);
     $this->appendRow($data, $row++, $col, ['Start Date', $minDate]);
     $this->appendRow($data, $row++, $col, ['End Date', $maxDate]);
     $this->appendRow($data, $row++, $col, ['Return Interval', Helper::get($regOptions, 'returnInterval', 'Daily')]);
     $movingWindow = Helper::get($regOptions, 'movingWindow', 'None');
     $this->appendRow($data, $row++, $col, ['Moving Window', $movingWindow]);
     if ($movingWindow !== self::NONE) {
       $this->appendRow($data, $row++, $col, ['Window', Helper::get($regOptions, 'interval', 250)]);
     }
   }

}
