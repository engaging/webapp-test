<?php

namespace App\Services\Export;

use Maatwebsite\Excel\Facades\Excel;
use App\Services\Constant;
use App\Services\Right\RightService;
use App\Services\Lab\BacktestingService;
use App\Portfolio;
use App;
use Helper;
use PHPExcel_Cell;
use PHPExcel_Style_Fill;
use PHPExcel_Style_Alignment;
use PHPExcel_Worksheet_Drawing;

class ExcelExportServiceImpl extends AbstractExcelExportService {

  /**
   * Public constructor
   */

  public function __construct(){
    $this->right = App::make(RightService::class);
    $this->backtesting = App::make(BacktestingService::class);
  }

  /**
   * Public functions
   */

  function exportExcelPortfolio(Portfolio $portfolio, array $options = []){
    ini_set('memory_limit', '512M');
    $options = array_merge([
      'period' => 'min', // for portfolio/base100 track
      'noFactor' => true,
      'noLive' => false,
      'noMin' => false,
      'noBase100' => false, // for strategy track
      'filename' => null, // to override filename
      'extension' => 'xlsx', // file extension ('xlsx', 'xls', 'csv')
    ], $options);
    // function helpers
    $styleHeaderCells = function($cells) {
      $this->styleHeaderCells($cells);
    };
    // compute portfolio
    $portfolioName = 'Portfolio ' . $portfolio->title;
    $maxDate = $this->backtesting->getMaxDate($portfolio);
    $periods = $this->backtesting->getPeriods($portfolio, $maxDate);
    $portfolioCal = $this->backtesting->getPortfolio($portfolio, [
      'maxDate' => $maxDate,
      'periods' => $periods,
      'fullTracks' => true,
      'noFactor' => $options['noFactor'],
      'noLive' => $options['noLive'],
      'noMin' => $options['noMin'],
      'rebalancingWeights' => 'all',
    ]);
    // get and compute strategies
    $strategies = [];
    $strategiesCal = [];
    $strategyCodes = $portfolioCal['listCodes'];
    $strategyCount = count($strategyCodes);
    $activeStrategies = $portfolio->getActiveStrategies();
    foreach ($strategyCodes as $code) { // sort strategies same as listCodes
      $strategy = $activeStrategies->first(function($key, $item) use($code) {
        return $item->code === $code;
      });
      $strategies[] = $strategy;
      $strategiesCal[$strategy->code] = $this->backtesting->getStrategy($strategy, [
        'maxDate' => $maxDate,
        'periods' => $periods,
        'metricsOnly' => true,
        'noFactor' => $options['noFactor'],
        'noLive' => $options['noLive'],
        'noMin' => $options['noMin'],
      ]);
    }
    // get dates track
    $period = $this->getActivePeriod($portfolioCal, $periods, $options['period']);
    $dates = $portfolioCal[$period . '_date_track'];
    $portfolioDatesCount = count($dates);
    // Track Record sheet
    $data1 = [
      [], // header logo
      [], // empty line
      [], // empty line
    ];
    // Portfolio Composition
    $data1[] = [$portfolioName];
    $data1[] = ['Name', 'Code', 'Asset Class', 'Currency', 'Region', 'Factor', 'Return Type', 'Style'];
    foreach ($strategies as $strategy) {
      $data1[] = [
        $strategy->shortName,
        $strategy->code,
        $strategy->assetClass,
        $strategy->currency,
        $strategy->region,
        $strategy->factor,
        $strategy->returnType,
        $strategy->style,
      ];
    }
    // Allocation Model
    $row = 4;
    $this->appendAllocationModel($data1, $row, 9, $portfolio, $maxDate, $periods[$period]);
    // Rebalancing Date
    $col = 12;
    $this->appendRow($data1, $row, $col, array_merge(['Rebalancing Date'], $strategyCodes, ['Funding']));
    $rebalancingCount = count($portfolioCal['rebalancingDates']);
    for ($i = $rebalancingCount - 1, $j=0; $i >= 0; $i--, $j++) {
      $strategyAllocations = [];
      foreach ($strategyCodes as $code) {
        $strategyAllocations[] = $portfolioCal['strategyAllocations'][$code][$i] * 100;
      }
      $this->appendRow($data1, $row + $j + 1, $col, array_merge(
        [Helper::time2excel(strtotime($portfolioCal['rebalancingDates'][$i]))],
        $strategyAllocations,
        [$portfolioCal['fundingAllocations'][$i] * 100]
      ));
    }
    // Track Record
    $col += $strategyCount + 3;
    $this->appendRow($data1, $row, $col, ['Date', $portfolioName]);
    $datesCount = count($dates);
    $delta = $datesCount - $portfolioDatesCount;
    for ($i = $datesCount - 1, $j=0; $i >= 0; $i--, $j++) {
      $this->appendRow($data1, $row + $j + 1, $col, [
        Helper::time2excel(strtotime($dates[$i])),
        Helper::get($portfolioCal[$period . '_track'], $i - $delta),
      ]);
    }
    unset($periods['min']); // ignore min period
    $periodCount = count($periods);
    // Risk Metrics sheet
    $data2 = [
      [], // header logo
      [], // empty line
    ];
    // As of Date
    $data2[] = ['As of Date', $maxDate];
    // Risk Metrics
    $row = 4;
    $col = 0;
    $this->appendRow($data2, $row, $col, [$portfolioName]);
    foreach ($strategyCodes as $code) {
      $col += $periodCount + 2;
      $this->appendRow($data2, $row, $col, [$code]);
    }
    $row++;
    $col = 0;
    for ($i=0; $i <= $strategyCount; $i++) {
      $this->appendRow($data2, $row, $col, ['Measure']);
      foreach ($periods as $period => $value) {
        $col++;
        $this->appendRow($data2, $row, $col, [self::PERIOD_MAP[$period]]);
      }
      $col += 2;
    }
    $row++;
    $col = 0;
    foreach (self::METRIC_MAP as list($factor, $metric, $label)) {
      $this->appendRow($data2, $row, $col, [$label]);
      foreach ($periods as $period => $value) {
        $col++;
        $this->appendRow($data2, $row, $col, [floatval($portfolioCal[$period.'_'.$metric])]);
      }
      $col += 2;
      foreach ($strategies as $strategy) {
        $this->appendRow($data2, $row, $col, [$label]);
        foreach ($periods as $period => $value) {
          $col++;
          $this->appendRow($data2, $row, $col, [floatval($strategiesCal[$strategy->code][$period.'_'.$metric])]);
        }
        $col += 2;
      }
      $row++;
      $col = 0;
    }
    // Disclaimer sheet
    $data3 = [
      [], // empty line
    ];
    // Disclaimer
    $data3[] = [null, self::DISCLAIMER];
    $providers = [];
    foreach ($strategies as $strategy) {
      if (!empty($strategy->disclaimer) && !in_array($strategy->provider, $providers)) {
        $data3[] = []; // empty line
        $disclaimerParts = $this->splitDisclaimer($strategy->disclaimer);
        foreach ($disclaimerParts as $disclaimerPart) {
          $data3[] = [null, $disclaimerPart];
        }
        $providers[] = $strategy->provider;
      }
    }
    return Excel::create($options['filename'] ?: 'Track Export_Portfolio ' . $portfolio->title, function($excel) use($data1, $data2, $data3, $strategyCount, $periodCount, $styleHeaderCells) {
      // Track Record sheet
      $excel->sheet('Track Record', function($sheet) use($data1, $strategyCount, $styleHeaderCells) {
        // header logo
        $sheet->setHeight(1, 60);
        $sheet->cells('1', function ($cells) {
          $cells->setBackground('#123B69');
        });
        $objDrawing = new PHPExcel_Worksheet_Drawing;
        $objDrawing->setPath(public_path('img/export/logo.png'));
        $objDrawing->setCoordinates('A1');
        $objDrawing->setResizeProportional(false);
        $objDrawing->setWidthAndHeight(280, 62);
        $objDrawing->setOffsetX(20);
        $objDrawing->setOffsetY(7);
        $objDrawing->setWorksheet($sheet);
        // Portfolio Composition
        $row = 4;
        $sheet->cells("A${row}", $styleHeaderCells);
        $row++;
        $sheet->cells("A${row}:H${row}", $styleHeaderCells);
        // Allocation Model
        $sheet->cells("J${row}:K${row}", $styleHeaderCells);
        // Rebalancing Date
        $col = 12;
        $col1 = PHPExcel_Cell::stringFromColumnIndex($col);
        $col2 = PHPExcel_Cell::stringFromColumnIndex($col + $strategyCount + 1);
        $sheet->cells("${col1}${row}:${col2}${row}", $styleHeaderCells);
        $sheet->setColumnFormat([$col1 => self::FORMAT_DATE]);
        while ($col1 !== $col2) {
          $col1 = PHPExcel_Cell::stringFromColumnIndex(++$col);
          $sheet->setColumnFormat([$col1 => self::FORMAT_NUMBER]);
        }
        // Track Record
        $col += 2;
        $col1 = PHPExcel_Cell::stringFromColumnIndex($col);
        $col2 = PHPExcel_Cell::stringFromColumnIndex($col + 1);
        $sheet->cells("${col1}${row}:${col2}${row}", $styleHeaderCells);
        $sheet->setColumnFormat([$col1 => self::FORMAT_DATE]);
        while ($col1 !== $col2) {
          $col1 = PHPExcel_Cell::stringFromColumnIndex(++$col);
          $sheet->setColumnFormat([$col1 => self::FORMAT_NUMBER]);
          $sheet->getStyle($col1)
            ->getAlignment()
            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        }
        // unselect previous selection
        $sheet->getStyle('IU1');
        // inject sheet data
        $sheet->fromArray(
          $data1, // source
          null, // not used
          'A1', // start cell
          true, // show 0 as value instead of an empty cell (default: false)
          false // disable auto heading generation by indices (default: true)
        );
      });
      // Risk Metrics sheet
      $excel->sheet('Risk Metrics', function($sheet) use($data2, $strategyCount, $periodCount, $styleHeaderCells) {
        // header logo
        $sheet->setHeight(1, 60);
        $sheet->cells('1', function ($cells) {
          $cells->setBackground('#123B69');
        });
        $objDrawing = new PHPExcel_Worksheet_Drawing;
        $objDrawing->setPath(public_path('img/export/logo.png'));
        $objDrawing->setCoordinates('A1');
        $objDrawing->setResizeProportional(false);
        $objDrawing->setWidthAndHeight(280, 62);
        $objDrawing->setOffsetX(20);
        $objDrawing->setOffsetY(7);
        $objDrawing->setWorksheet($sheet);
        // As of Date
        $col = 0;
        $col1 = PHPExcel_Cell::stringFromColumnIndex($col);
        $sheet->cells("${col1}3", $styleHeaderCells);
        // Risk Metrics
        for ($i=0; $i <= $strategyCount; $i++) {
          $col1 = PHPExcel_Cell::stringFromColumnIndex($col);
          $col2 = PHPExcel_Cell::stringFromColumnIndex($col + $periodCount);
          $sheet->cells("${col1}5", $styleHeaderCells);
          $sheet->cells("${col1}6:${col2}6", $styleHeaderCells);
          $col += $periodCount + 2;
        }
        $row = 7;
        foreach (self::METRIC_MAP as list($factor)) {
          $sheet->getStyle("$row:$row")
            ->getNumberFormat()
            ->setFormatCode(self::FORMAT_MAP[$factor]);
          $row++;
        }
        // unselect previous selection
        $sheet->getStyle('IU1');
        // inject sheet data
        $sheet->fromArray(
          $data2, // source
          null, // not used
          'A1', // start cell
          true, // show 0 as value instead of an empty cell (default: false)
          false // disable auto heading generation by indices (default: true)
        );
      });
      // Disclaimer sheet
      $excel->sheet('Disclaimer', function($sheet) use($data3) {
        // Disclaimer
        $sheet->setWidth('B', 100);
        $sheet->getStyle('B')->getAlignment()->setWrapText(true);
        $sheet->getStyle('A:IU')->applyFromArray([
          'fill' => [
            'type' => PHPExcel_Style_Fill::FILL_SOLID,
            'color' => ['rgb' => '123B69'],
          ],
          'font' => [
            'color' => ['rgb' => 'FFFFFF'],
          ],
        ]);
        // unselect previous selection
        $sheet->getStyle('IU1');
        // inject sheet data
        $sheet->fromArray(
          $data3, // source
          null, // not used
          'A1', // start cell
          true, // show 0 as value instead of an empty cell (default: false)
          false // disable auto heading generation by indices (default: true)
        );
      });
      // set default active sheet
      $excel->setActiveSheetIndex(0);
    })->export($options['extension']);
  }

}
