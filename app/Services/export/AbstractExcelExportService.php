<?php

namespace App\Services\Export;

use App\Services\Constant;
use App\Portfolio;
use Helper;
use PHPExcel_Style_NumberFormat;

abstract class AbstractExcelExportService implements ExcelExportService, Constant {

  const FORMAT_TEXT = PHPExcel_Style_NumberFormat::FORMAT_TEXT;
  const FORMAT_DATE = PHPExcel_Style_NumberFormat::FORMAT_DATE_YYYYMMDD2;
  const FORMAT_NUMBER = PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00;
  const FORMAT_PERCENTAGE = PHPExcel_Style_NumberFormat::FORMAT_PERCENTAGE_00;
  const FORMAT_MAP = [
    1 => self::FORMAT_NUMBER,
    100 => self::FORMAT_PERCENTAGE,
  ];
  const MODEL_MAP = [
    'equal' => 'Equal Weighting',
    'manual' => 'Manual Weighting',
    'risk' => 'Vol Risk Parity',
    'erc' => 'ERC Weighting',
    'custom' => 'Custom Rebalancing',
  ];
  const FORBIDDEN = '(1)  Sorry, the track is not available for this strategy. Please contact us on admin@engagingenterprises.co for further information.';
  const PCA_INVALID_TRACK = '(1) The PC track(s) is discarded as its track falls below 0.00';
  const DISCLAIMER = "Disclaimer
    This document is provided by engagingenterprises for registered users only and its access and use are subject to the Terms and Conditions of engagingenterprises.co. engagingenterprises is providing access to documents and information regarding financial Indices. engagingenterprises is not related, nor associated to any of these Indices. Please refer to the corresponding Index Sponsor or Index Calculation Agent for further information. The information contained herein are derived from proprietary models based upon well-recognized financial principles and from data that has been obtained from external sources believed reliable. However engagingenterprises and any external information provider, disclaim any responsibility for the accuracy of estimates and models used in the calculations, any errors or omissions in computing any performance and statistic information. engagingenterprises does not accept any liability whatsoever for any direct or consequential loss arising from any use of the information provided. Performance and statistic information is provided in this document solely for informational purposes and should not be interpreted as an indication of actual or future performance. This document does not constitute an offer, recommendation or solicitation for the purchase or sale of any particular financial instrument or any securities. engagingenterprises does not provide legal advice or tax advice or investment advice nor does engagingenterprises give any investment recommendation or financial analysis.
  ";

  /**
   * Private functions
   */

  protected function getActivePeriod($outputCal, $periods, $period) {
    $found = false;
    foreach (array_reverse($periods) as $periodKey => $minDate) {
      if (!$found && $periodKey === $period) {
        $found = true;
      }
      if ($found && $outputCal[$periodKey . '_active'] === 1) {
        return $periodKey;
      }
    }
    return 'cust'; // should never happen
  }

  protected function styleHeaderCells($cells) {
    $cells->setBackground('#D9D9D9');
    $cells->setBorder('thin', '#123B69', 'thin', '#123B69');
    $cells->setFontColor('#123B69');
    $cells->setFontWeight('bold');
  }

  protected function styleTopBorder($cells) {
    $cells->setBorder('thin', '#123B69');
    $cells->setFontWeight('bold');
  }

  protected function splitDisclaimer($disclaimer) {
    $chunkSize = 2000;

    $strlen = strlen($disclaimer);
    if ($strlen < $chunkSize) {
      return [$disclaimer];
    }

    $lines = explode(PHP_EOL, $disclaimer);
    $linesCount = count($lines);
    $chunkCount = intval($strlen / $chunkSize);
    if ($linesCount > $chunkCount) {
      $chunkCount = $linesCount;
    }
    $chunkSplit = intval($linesCount / $chunkCount);
    $chunkCount = 0;
    $disclaimerParts = [];
    for ($i=0, $j=0; $i < $linesCount; $i++, $j++) {
      if ($j === $chunkSplit) {
        $disclaimerParts[] = [$lines[$i]];
        $chunkCount++;
        $j = 0;
      }
      $disclaimerParts[$chunkCount][] = $lines[$i];
    }
    for ($i=0; $i < count($disclaimerParts); $i++) {
      $disclaimerParts[$i] = join(PHP_EOL, $disclaimerParts[$i]);
    }
    return $disclaimerParts;
  }

  protected function appendRow(&$data, $row, $col, $arr) {
    $hasRow = isset($data[$row]);
    $colCount = $hasRow ? count($data[$row]) : 0;
    if ($hasRow && $colCount === $col) {
      $data[$row] = array_merge($data[$row], $arr);
    } else if ($hasRow && $colCount < $col) {
      for ($i=$colCount; $i < $col; $i++) {
        $data[$row][] = null;
      }
      $data[$row] = array_merge($data[$row], $arr);
    } else if ($hasRow && $colCount > $col) {
      $arrCount = count($arr) + $col;
      $j = 0;
      for ($i=$col; $i < $colCount; $i++) {
        $data[$row][$i] = $arr[$j];
        $j++;
      }
      for ($i=$colCount; $i < $arrCount; $i++) {
        $data[$row][] = $arr[$j];
        $j++;
      }
    } else {
      $rowCount = count($data);
      for ($i=$rowCount; $i < $row; $i++) {
        $data[$i] = [];
      }
      $data[$row] = [];
      for ($i=$colCount; $i < $col; $i++) {
        $data[$row][] = null;
      }
      $data[$row] = array_merge($data[$row], $arr);
    }
  }

  protected function appendAllocationModel(&$data, $row, $col, Portfolio $portfolio, $maxDate, $minDate) {
    $this->appendRow($data, $row++, $col, ['Allocation Model', null]);
    $this->appendRow($data, $row++, $col, ['Start Date', $minDate]);
    $this->appendRow($data, $row++, $col, ['End Date', $maxDate]);
    $allocOpt = json_decode($portfolio->allocationOptions, true);
    if (is_array($allocOpt)) {
      $allocOpt = array_merge(self::DEFAULT_ALLOCATION, $allocOpt);
    } else {
      $allocOpt = self::DEFAULT_ALLOCATION;
    }

    $model = $portfolio->weighting;
    $this->appendRow($data, $row++, $col, ['Model', self::MODEL_MAP[$model]]);
    if ($model === 'custom') return;

    $isFixedEquity = in_array($model, ['equal', 'manual']);
    $allocMap = [
      'rebalancing' => ['Rebalancing', '', 'Monthly'],
      'rebalancing_month' => ['Rebalancing Month', '', 'Every Month'],
      'rebalancing_date' => ['Rebalancing Date', '', 'Last Day'],
      'rebalancing_cost' => ['Rebalancing cost', '%'],
      'replication_cost' => ['Replication cost', '% p.a.'],
    ];

    if (!$isFixedEquity) {
      $allocMap['volatility_interval'] = [
        ($model === 'erc') ? 'ERC Interval' : 'Volatility Interval',
      ];
    }

    if ($allocOpt['leverage_type'] !== self::NONE) {
      switch ($allocOpt['leverage_type']) {
        case 'Fixed Leverage':
          $allocMap['fixed_leverage'] = ['Fixed Leverage', '%'];
          break;
        case 'Target Volatility':
          $allocMap['target_volatility'] = ['Target Volatility', '%'];
          $allocMap['target_interval'] = ['Target Interval'];
          $allocMap['leverage_min'] = ['Min Leverage', '%', 0];
          $allocMap['leverage_max'] = ['Max Leverage', '%', 200];
          break;
      }
      // $allocMap['funding_currency'] = ['Funding'];
      // if ($allocOpt['funding_currency'] !== self::NONE) {
      //   $allocMap['borrowing_spread'] = ['Borrowing Spread', '%'];
      //   $allocMap['lending_spread'] = ['Lending Spread', '%'];
      // }
    }

    foreach ($allocMap as $key => $labels) {
      $count = count($labels);
      $value = Helper::get($allocOpt, $key);
      if (!$value) {
        $default = ($count > 2) ? $labels[2] : null; // default
        $value = $default;
      }
      if ($value !== null) {
        $unit = ($count > 1) ? $labels[1] : ''; // unit
        $this->appendRow($data, $row++, $col, [$labels[0], $value . $unit]);
      }
    }

    if (!$isFixedEquity) {
      $primary_allocation_constraint = Helper::get($allocOpt, 'primary_allocation_constraint', self::NONE);
      if ($primary_allocation_constraint !== self::NONE) {
        $row++;
        $this->appendRow($data, $row++, $col, ['Primary Allocation Constraint', $primary_allocation_constraint]);
        $constraints = count($allocOpt['allocationConstraints']['primary']['allocationsMax']) !== 0
         ? $allocOpt['allocationConstraints']['primary']['allocationsMax']
         : $allocOpt['allocationConstraints']['primary']['allocationsMin'];
        foreach ($constraints as $key => $field) {
          $this->appendRow($data, $row++, $col, ['Min ' . $key, (Helper::get($allocOpt['allocationConstraints']['primary']['allocationsMin'], $key) ?: '0') . '%']);
          $this->appendRow($data, $row++, $col, ['Max ' . $key, (Helper::get($allocOpt['allocationConstraints']['primary']['allocationsMax'], $key) ?: '100') . '%']);
        }
      } else {
        $this->appendRow($data, $row++, $col, ['Primary Allocation Constraint', $primary_allocation_constraint]);
      }

      $secondary_allocation_constraint = Helper::get($allocOpt, 'secondary_allocation_constraint', self::NONE);
      if ($secondary_allocation_constraint !== self::NONE) {
        $row++;
        $this->appendRow($data, $row++, $col, ['Secondary Allocation Constraint', $secondary_allocation_constraint]);
        $constraints = count($allocOpt['allocationConstraints']['secondary']['allocationsMax']) !== 0
         ? $allocOpt['allocationConstraints']['secondary']['allocationsMax']
         : $allocOpt['allocationConstraints']['secondary']['allocationsMin'];
        foreach ($constraints as $key => $field) {
          $this->appendRow($data, $row++, $col, ['Min ' . $key, (Helper::get($allocOpt['allocationConstraints']['secondary']['allocationsMin'], $key) ?: '0') . '%']);
          $this->appendRow($data, $row++, $col, ['Max ' . $key, (Helper::get($allocOpt['allocationConstraints']['secondary']['allocationsMax'], $key) ?: '100') . '%']);
        }
      } else {
        $this->appendRow($data, $row++, $col, ['Secondary Allocation Constraint', $secondary_allocation_constraint]);
      }
    }
  }

}
