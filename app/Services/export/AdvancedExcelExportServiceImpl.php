<?php

namespace App\Services\Export;

use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use App\Services\Constant;
use App\Services\Right\RightService;
use App\Services\Lab\BacktestingService;
use App\Services\Lab\PCAService;
use App\Services\Lab\RegressionService;
use App\Portfolio;
use App\Indexinfo;
use App\Regression;
use App;
use Helper;
use Exception;
use PHPExcel_Cell;
use PHPExcel_Style_Fill;
use PHPExcel_Style_Alignment;
use PHPExcel_Worksheet_Drawing;

class AdvancedExcelExportServiceImpl extends AbstractAdvancedExcelExportService {

  /**
   * Public constructor
   */

  public function __construct(){
    $this->right = App::make(RightService::class);
    $this->backtesting = App::make(BacktestingService::class);
    $this->pca = App::make(PCAService::class);
    $this->regression = App::make(RegressionService::class);
  }

  /**
   * Public functions
   */

  function exportExcelPortfolio(Portfolio $portfolio, array $options = []){
    ini_set('memory_limit', '512M');

    $user = Auth::user();
    $options = array_merge([
      'period' => 'min', // for portfolio/base100 track
      'noFactor' => true,
      'noLive' => false,
      'noMin' => false,
      'noBase100' => false, // for strategy track
      'filename' => null, // to override filename
      'extension' => 'xlsx', // file extension ('xlsx', 'xls', 'csv')
    ], $options);

    // function helpers
    $styleHeaderCells = function($cells) {
      $this->styleHeaderCells($cells);
    };

    // compute portfolio
    $portfolioName = 'Portfolio ' . $portfolio->title;
    $maxDate = $this->backtesting->getMaxDate($portfolio);
    $periods = $this->backtesting->getPeriods($portfolio, $maxDate);
    $portfolioCal = $this->backtesting->getPortfolio($portfolio, [
      'maxDate' => $maxDate,
      'periods' => $periods,
      'fullTracks' => true,
      'noFactor' => $options['noFactor'],
      'noLive' => $options['noLive'],
      'noMin' => $options['noMin'],
      'rebalancingWeights' => 'all',
    ]);

    // get and compute strategies
    $strategies = [];
    $strategiesCal = [];
    $hasExportRight = [];
    $strategyCodes = $portfolioCal['listCodes'];
    $strategyCount = count($strategyCodes);
    $activeStrategies = $portfolio->getActiveStrategies();
    foreach ($strategyCodes as $code) { // sort strategies same as listCodes
      $strategy = $activeStrategies->first(function($key, $item) use($code) {
        return $item->code === $code;
      });
      $strategies[] = $strategy;
      $strategiesCal[$strategy->code] = $this->backtesting->getStrategy($strategy, [
        'maxDate' => $maxDate,
        'periods' => $periods,
        'fullTracks' => true,
        'noFactor' => $options['noFactor'],
        'noLive' => $options['noLive'],
        'noMin' => $options['noMin'],
      ]);
      if ($this->right->hasStrategyRight($user, $strategy, 'export')) {
        $hasExportRight[] = $strategy->code;
      }
    }

    // get dates track
    $period = $this->getActivePeriod($portfolioCal, $periods, $options['period']);
    $dates = $portfolioCal[$period . '_date_track'];
    $portfolioDatesCount = count($dates);

    // get strategies track if base100 disabled
    $strategiesTrack = [];
    if ($options['noBase100'] === true) {
      $maxTrackCount = ['code' => null, 'count' => null];
      $maxExcelDate = Helper::time2excel(strtotime($maxDate));
      $strategiesTrack = $this->backtesting->downloadTrackData($hasExportRight, $maxDate);
      foreach ($strategiesTrack as $code => $track) {
        $track = $strategiesTrack[$code];
        $maxCount = array_search($maxExcelDate, array_column($track, 0)) + 1;
        if (count($track) > $maxCount) {
          array_splice($track, $maxCount);
        }
        if (count($track) > $maxTrackCount['count']) {
          $maxTrackCount = ['code' => $code, 'count' => count($track)];
        }
        $strategiesTrack[$code] = array_reverse($track);
      }
      if ($maxTrackCount['count'] > $portfolioDatesCount) {
        $track = $strategiesTrack[$maxTrackCount['code']];
        $dates =  array_reverse(Helper::map($track, 0, function($date) {
          return strftime('%F', Helper::excel2time($date));
        }));
      }
    }

    // Track Record sheet
    $data1 = [
      [], // header logo
      [], // empty line
      [], // empty line
    ];

    // Portfolio Composition
    $data1[] = [$portfolioName];
    $data1[] = ['Name', 'Code', 'Asset Class', 'Currency', 'Region', 'Factor', 'Return Type', 'Style'];
    foreach ($strategies as $strategy) {
      $data1[] = [
        $strategy->shortName,
        $strategy->code,
        $strategy->assetClass,
        $strategy->currency,
        $strategy->region,
        $strategy->factor,
        $strategy->returnType,
        $strategy->style,
      ];
    }

    // Allocation Model
    $row = 4;
    $this->appendAllocationModel($data1, $row, 9, $portfolio, $maxDate, $periods[$period]);

    // Rebalancing Date
    $col = 12;
    $this->appendRow($data1, $row, $col, array_merge(['Rebalancing Date'], $strategyCodes, ['Funding']));
    $rebalancingCount = count($portfolioCal['rebalancingDates']);
    for ($i = $rebalancingCount - 1, $j=0; $i >= 0; $i--, $j++) {
      $strategyAllocations = [];
      foreach ($strategyCodes as $code) {
        $strategyAllocations[] = $portfolioCal['strategyAllocations'][$code][$i] * 100;
      }
      $this->appendRow($data1, $row + $j + 1, $col, array_merge(
        [Helper::time2excel(strtotime($portfolioCal['rebalancingDates'][$i]))],
        $strategyAllocations,
        [$portfolioCal['fundingAllocations'][$i] * 100]
      ));
    }

    // Track Record
    $col += $strategyCount + 3;
    $this->appendRow($data1, $row, $col, array_merge(['Date', $portfolioName], $strategyCodes));
    $datesCount = count($dates);
    $delta = $datesCount - $portfolioDatesCount;
    for ($i = $datesCount - 1, $j=0; $i >= 0; $i--, $j++) {
      $strategyValues = [];
      foreach ($strategies as $strategy) {
        if (in_array($strategy->code, $hasExportRight)) {
          if ($options['noBase100'] === true) {
            $strategyValues[] = Helper::get($strategiesTrack[$strategy->code], $j, [null, null])[1];
          } else {
            $strategyValues[] = Helper::get($strategiesCal[$strategy->code][$period . '_track'], $i - $delta);
          }
        } else {
          $strategyValues[] = !$j ? '(1)' : null;
        }
      }
      $this->appendRow($data1, $row + $j + 1, $col, array_merge([
        Helper::time2excel(strtotime($dates[$i])),
        Helper::get($portfolioCal[$period . '_track'], $i - $delta),
      ], $strategyValues));
    }
    if (count($hasExportRight) !== $strategyCount) {
      $row = 5;
      $col += $strategyCount + 3;
      $this->appendRow($data1, $row, $col, [self::FORBIDDEN]);
    }

    unset($periods['min']); // ignore min period
    $periodCount = count($periods);

    // Risk Metrics sheet
    $data2 = [
      [], // header logo
      [], // empty line
    ];

    // As of Date
    $data2[] = ['As of Date', $maxDate];

    // Risk Metrics
    $row = 4;
    $col = 0;
    $this->appendRow($data2, $row, $col, [$portfolioName]);
    foreach ($strategyCodes as $code) {
      $col += $periodCount + 2;
      $this->appendRow($data2, $row, $col, [$code]);
    }

    $row++;
    $col = 0;
    for ($i=0; $i <= $strategyCount; $i++) {
      $this->appendRow($data2, $row, $col, ['Measure']);
      foreach ($periods as $period => $value) {
        $col++;
        $this->appendRow($data2, $row, $col, [self::PERIOD_MAP[$period]]);
      }
      $col += 2;
    }

    $row++;
    $col = 0;
    foreach (self::METRIC_MAP as list($factor, $metric, $label)) {
      $this->appendRow($data2, $row, $col, [$label]);
      foreach ($periods as $period => $value) {
        $col++;
        $this->appendRow($data2, $row, $col, [floatval($portfolioCal[$period.'_'.$metric])]);
      }
      $col += 2;
      foreach ($strategies as $strategy) {
        $this->appendRow($data2, $row, $col, [$label]);
        foreach ($periods as $period => $value) {
          $col++;
          $this->appendRow($data2, $row, $col, [floatval($strategiesCal[$strategy->code][$period.'_'.$metric])]);
        }
        $col += 2;
      }
      $row++;
      $col = 0;
    }

    // Disclaimer sheet
    $data3 = [
      [], // empty line
    ];

    // Disclaimer
    $data3[] = [null, self::DISCLAIMER];
    $providers = [];
    foreach ($strategies as $strategy) {
      if (!empty($strategy->disclaimer) && !in_array($strategy->provider, $providers)) {
        $data3[] = []; // empty line
        $disclaimerParts = $this->splitDisclaimer($strategy->disclaimer);
        foreach ($disclaimerParts as $disclaimerPart) {
          $data3[] = [null, $disclaimerPart];
        }
        $providers[] = $strategy->provider;
      }
    }

    return Excel::create($options['filename'] ?: 'Track Export_Portfolio ' . $portfolio->title, function($excel) use($data1, $data2, $data3, $strategyCount, $periodCount, $styleHeaderCells) {
      // Track Record sheet
      $excel->sheet('Track Record', function($sheet) use($data1, $strategyCount, $styleHeaderCells) {
        // header logo
        $sheet->setHeight(1, 60);
        $sheet->cells('1', function ($cells) {
          $cells->setBackground('#123B69');
        });
        $objDrawing = new PHPExcel_Worksheet_Drawing;
        $objDrawing->setPath(public_path('img/export/logo.png'));
        $objDrawing->setCoordinates('A1');
        $objDrawing->setResizeProportional(false);
        $objDrawing->setWidthAndHeight(280, 62);
        $objDrawing->setOffsetX(20);
        $objDrawing->setOffsetY(7);
        $objDrawing->setWorksheet($sheet);

        // Portfolio Composition
        $row = 4;
        $sheet->cells("A${row}", $styleHeaderCells);
        $row++;
        $sheet->cells("A${row}:H${row}", $styleHeaderCells);

        // Allocation Model
        $sheet->cells("J${row}:K${row}", $styleHeaderCells);

        // Rebalancing Date
        $col = 12;
        $col1 = PHPExcel_Cell::stringFromColumnIndex($col);
        $col2 = PHPExcel_Cell::stringFromColumnIndex($col + $strategyCount + 1);
        $sheet->cells("${col1}${row}:${col2}${row}", $styleHeaderCells);
        $sheet->setColumnFormat([$col1 => self::FORMAT_DATE]);
        while ($col1 !== $col2) {
          $col1 = PHPExcel_Cell::stringFromColumnIndex(++$col);
          $sheet->setColumnFormat([$col1 => self::FORMAT_NUMBER]);
        }

        // Track Record
        $col += 2;
        $col1 = PHPExcel_Cell::stringFromColumnIndex($col);
        $col2 = PHPExcel_Cell::stringFromColumnIndex($col + $strategyCount + 1);
        $sheet->cells("${col1}${row}:${col2}${row}", $styleHeaderCells);
        $sheet->setColumnFormat([$col1 => self::FORMAT_DATE]);
        while ($col1 !== $col2) {
          $col1 = PHPExcel_Cell::stringFromColumnIndex(++$col);
          $sheet->setColumnFormat([$col1 => self::FORMAT_NUMBER]);
          $sheet->getStyle($col1)
            ->getAlignment()
            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        }

        // unselect previous selection
        $sheet->getStyle('IU1');

        // inject sheet data
        $sheet->fromArray(
          $data1, // source
          null, // not used
          'A1', // start cell
          true, // show 0 as value instead of an empty cell (default: false)
          false // disable auto heading generation by indices (default: true)
        );
      });
      // Risk Metrics sheet
      $excel->sheet('Risk Metrics', function($sheet) use($data2, $strategyCount, $periodCount, $styleHeaderCells) {
        // header logo
        $sheet->setHeight(1, 60);
        $sheet->cells('1', function ($cells) {
          $cells->setBackground('#123B69');
        });
        $objDrawing = new PHPExcel_Worksheet_Drawing;
        $objDrawing->setPath(public_path('img/export/logo.png'));
        $objDrawing->setCoordinates('A1');
        $objDrawing->setResizeProportional(false);
        $objDrawing->setWidthAndHeight(280, 62);
        $objDrawing->setOffsetX(20);
        $objDrawing->setOffsetY(7);
        $objDrawing->setWorksheet($sheet);

        // As of Date
        $col = 0;
        $col1 = PHPExcel_Cell::stringFromColumnIndex($col);
        $sheet->cells("${col1}3", $styleHeaderCells);

        // Risk Metrics
        for ($i=0; $i <= $strategyCount; $i++) {
          $col1 = PHPExcel_Cell::stringFromColumnIndex($col);
          $col2 = PHPExcel_Cell::stringFromColumnIndex($col + $periodCount);
          $sheet->cells("${col1}5", $styleHeaderCells);
          $sheet->cells("${col1}6:${col2}6", $styleHeaderCells);
          $col += $periodCount + 2;
        }
        $row = 7;
        foreach (self::METRIC_MAP as list($factor)) {
          $sheet->getStyle("$row:$row")
            ->getNumberFormat()
            ->setFormatCode(self::FORMAT_MAP[$factor]);
          $row++;
        }

        // unselect previous selection
        $sheet->getStyle('IU1');

        // inject sheet data
        $sheet->fromArray(
          $data2, // source
          null, // not used
          'A1', // start cell
          true, // show 0 as value instead of an empty cell (default: false)
          false // disable auto heading generation by indices (default: true)
        );
      });
      // Disclaimer sheet
      $excel->sheet('Disclaimer', function($sheet) use($data3) {
        // Disclaimer
        $sheet->setWidth('B', 100);
        $sheet->getStyle('B')->getAlignment()->setWrapText(true);
        $sheet->getStyle('A:IU')->applyFromArray([
          'fill' => [
            'type' => PHPExcel_Style_Fill::FILL_SOLID,
            'color' => ['rgb' => '123B69'],
          ],
          'font' => [
            'color' => ['rgb' => 'FFFFFF'],
          ],
        ]);

        // unselect previous selection
        $sheet->getStyle('IU1');

        // inject sheet data
        $sheet->fromArray(
          $data3, // source
          null, // not used
          'A1', // start cell
          true, // show 0 as value instead of an empty cell (default: false)
          false // disable auto heading generation by indices (default: true)
        );
      });

      // set default active sheet
      $excel->setActiveSheetIndex(0);
    })->export($options['extension']);
  }

  function exportExcelStrategy(Indexinfo $strategy, array $options = []){
    $user = Auth::user();
    $options = array_merge([
      'period' => 'min', // for portfolio/base100 track
      'noFactor' => true,
      'noLive' => false,
      'noMin' => false,
      'noBase100' => false, // for strategy track
      'filename' => null, // to override filename
      'extension' => 'xlsx', // file extension ('xlsx', 'xls', 'csv')
    ], $options);

    // function helpers
    $styleHeaderCells = function($cells) {
      $this->styleHeaderCells($cells);
    };

    // compute strategy
    $maxDate = $this->backtesting->getMaxDate($strategy);
    $periods = $this->backtesting->getPeriods($strategy, $maxDate);
    $strategyCal = $this->backtesting->getStrategy($strategy, [
      'maxDate' => $maxDate,
      'periods' => $periods,
      'fullTracks' => true,
      'noFactor' => $options['noFactor'],
      'noLive' => $options['noLive'],
      'noMin' => $options['noMin'],
    ]);
    $hasExportRight = $this->right->hasStrategyRight($user, $strategy, 'export');

    // get dates track
    $period = $this->getActivePeriod($strategyCal, $periods, $options['period']);
    $dates = $strategyCal[$period . '_date_track'];
    $strategyDatesCount = count($dates);

    // get strategy track if base100 disabled
    $strategyTrack = [];
    if ($options['noBase100'] === true && $hasExportRight) {
      $maxExcelDate = Helper::time2excel(strtotime($maxDate));
      $strategiesTrack = $this->backtesting->downloadTrackData([$strategy->code], $maxDate);
      $strategyTrack = $strategiesTrack[$strategy->code];
      $maxCount = array_search($maxExcelDate, array_column($strategyTrack, 0)) + 1;
      if (count($strategyTrack) > $maxCount) {
        array_splice($strategyTrack, $maxCount);
      }
      $strategyTrack = array_reverse($strategyTrack);
      if (count($strategyTrack) > $strategyDatesCount) {
        $dates =  array_reverse(Helper::map($strategyTrack, 0, function($date) {
          return strftime('%F', Helper::excel2time($date));
        }));
      }
    }

    // Track Record sheet
    $data1 = [
      [], // header logo
      [], // empty line
      [], // empty line
    ];

    // Strategy Information
    $data1[] = ['Name', 'Code', 'Asset Class', 'Currency', 'Region', 'Factor', 'Return Type', 'Style'];
    $data1[] = [
      $strategy->shortName,
      $strategy->code,
      $strategy->assetClass,
      $strategy->currency,
      $strategy->region,
      $strategy->factor,
      $strategy->returnType,
      $strategy->style,
    ];

    // Track Record
    $row = 3;
    $col = 9;
    $this->appendRow($data1, $row, $col, ['Date', $strategy->code]);
    $datesCount = count($dates);
    $delta = $datesCount - $strategyDatesCount;
    for ($i = $datesCount - 1, $j=0; $i >= 0; $i--, $j++) {
      if ($hasExportRight) {
        if ($options['noBase100'] === true) {
          $this->appendRow($data1, $row + $j + 1, $col, [
            Helper::time2excel(strtotime($dates[$i])),
            Helper::get($strategyTrack, $j, [null, null])[1],
          ]);
        } else {
          $this->appendRow($data1, $row + $j + 1, $col, [
            Helper::time2excel(strtotime($dates[$i])),
            Helper::get($strategyCal[$period . '_track'], $i - $delta),
          ]);
        }
      } else {
        $this->appendRow($data1, $row + $j + 1, $col, [
          Helper::time2excel(strtotime($dates[$i])),
          !$j ? '(1)' : null,
        ]);
      }
    }
    if (!$hasExportRight) {
      $row = 4;
      $col += 3;
      $this->appendRow($data1, $row, $col, [self::FORBIDDEN]);
    }

    unset($periods['min']); // ignore min period
    $periodCount = count($periods);

    // Risk Metrics sheet
    $data2 = [
      [], // header logo
      [], // empty line
    ];

    // As of Date
    $data2[] = ['As of Date', $maxDate];

    // Risk Metrics
    $row = 4;
    $col = 0;
    $this->appendRow($data2, $row, $col, [$strategy->code]);

    $row++;
    $col = 0;
    $this->appendRow($data2, $row, $col, ['Measure']);
    foreach ($periods as $period => $value) {
      $col++;
      $this->appendRow($data2, $row, $col, [self::PERIOD_MAP[$period]]);
    }
    $col += 2;

    $row++;
    $col = 0;
    foreach (self::METRIC_MAP as list($factor, $metric, $label)) {
      $this->appendRow($data2, $row, $col, [$label]);
      foreach ($periods as $period => $value) {
        $col++;
        $this->appendRow($data2, $row, $col, [floatval($strategyCal[$period.'_'.$metric])]);
      }
      $row++;
      $col = 0;
    }

    // Disclaimer sheet
    $data3 = [
      [], // empty line
    ];

    // Disclaimer
    $data3[] = [null, self::DISCLAIMER];
    if (!empty($strategy->disclaimer)) {
      $data3[] = []; // empty line
      $disclaimerParts = $this->splitDisclaimer($strategy->disclaimer);
      foreach ($disclaimerParts as $disclaimerPart) {
        $data3[] = [null, $disclaimerPart];
      }
    }

    return Excel::create($options['filename'] ?: 'Track Export_Strategy ' . $strategy->code, function($excel) use($data1, $data2, $data3, $periodCount, $styleHeaderCells) {
      // Track Record sheet
      $excel->sheet('Track Record', function($sheet) use($data1, $styleHeaderCells) {
        // header logo
        $sheet->setHeight(1, 60);
        $sheet->cells('1', function ($cells) {
          $cells->setBackground('#123B69');
        });
        $objDrawing = new PHPExcel_Worksheet_Drawing;
        $objDrawing->setPath(public_path('img/export/logo.png'));
        $objDrawing->setCoordinates('A1');
        $objDrawing->setResizeProportional(false);
        $objDrawing->setWidthAndHeight(280, 62);
        $objDrawing->setOffsetX(20);
        $objDrawing->setOffsetY(7);
        $objDrawing->setWorksheet($sheet);

        // Strategy Information
        $row = 4;
        $sheet->cells("A${row}:H${row}", $styleHeaderCells);

        // Track Record
        $col = 9;
        $col1 = PHPExcel_Cell::stringFromColumnIndex($col);
        $col2 = PHPExcel_Cell::stringFromColumnIndex($col + 1);
        $sheet->cells("${col1}${row}:${col2}${row}", $styleHeaderCells);
        $sheet->setColumnFormat([$col1 => self::FORMAT_DATE]);
        while ($col1 !== $col2) {
          $col1 = PHPExcel_Cell::stringFromColumnIndex(++$col);
          $sheet->setColumnFormat([$col1 => self::FORMAT_NUMBER]);
          $sheet->getStyle($col1)
            ->getAlignment()
            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        }

        // unselect previous selection
        $sheet->getStyle('IU1');

        // inject sheet data
        $sheet->fromArray(
          $data1, // source
          null, // not used
          'A1', // start cell
          true, // show 0 as value instead of an empty cell (default: false)
          false // disable auto heading generation by indices (default: true)
        );
      });
      // Risk Metrics sheet
      $excel->sheet('Risk Metrics', function($sheet) use($data2, $periodCount, $styleHeaderCells) {
        // header logo
        $sheet->setHeight(1, 60);
        $sheet->cells('1', function ($cells) {
          $cells->setBackground('#123B69');
        });
        $objDrawing = new PHPExcel_Worksheet_Drawing;
        $objDrawing->setPath(public_path('img/export/logo.png'));
        $objDrawing->setCoordinates('A1');
        $objDrawing->setResizeProportional(false);
        $objDrawing->setWidthAndHeight(280, 62);
        $objDrawing->setOffsetX(20);
        $objDrawing->setOffsetY(7);
        $objDrawing->setWorksheet($sheet);

        // As of Date
        $col = 0;
        $col1 = PHPExcel_Cell::stringFromColumnIndex($col);
        $sheet->cells("${col1}3", $styleHeaderCells);

        // Risk Metrics
        $col1 = PHPExcel_Cell::stringFromColumnIndex($col);
        $col2 = PHPExcel_Cell::stringFromColumnIndex($col + $periodCount);
        $sheet->cells("${col1}5", $styleHeaderCells);
        $sheet->cells("${col1}6:${col2}6", $styleHeaderCells);
        $col += $periodCount + 2;
        $row = 7;
        foreach (self::METRIC_MAP as list($factor)) {
          $sheet->getStyle("$row:$row")
            ->getNumberFormat()
            ->setFormatCode(self::FORMAT_MAP[$factor]);
          $row++;
        }

        // unselect previous selection
        $sheet->getStyle('IU1');

        // inject sheet data
        $sheet->fromArray(
          $data2, // source
          null, // not used
          'A1', // start cell
          true, // show 0 as value instead of an empty cell (default: false)
          false // disable auto heading generation by indices (default: true)
        );
      });
      // Disclaimer sheet
      $excel->sheet('Disclaimer', function($sheet) use($data3) {
        // Disclaimer
        $sheet->setWidth('B', 100);
        $sheet->getStyle('B')->getAlignment()->setWrapText(true);
        $sheet->getStyle('A:IU')->applyFromArray([
          'fill' => [
            'type' => PHPExcel_Style_Fill::FILL_SOLID,
            'color' => ['rgb' => '123B69'],
          ],
          'font' => [
            'color' => ['rgb' => 'FFFFFF'],
          ],
        ]);

        // unselect previous selection
        $sheet->getStyle('IU1');

        // inject sheet data
        $sheet->fromArray(
          $data3, // source
          null, // not used
          'A1', // start cell
          true, // show 0 as value instead of an empty cell (default: false)
          false // disable auto heading generation by indices (default: true)
        );
      });

      // set default active sheet
      $excel->setActiveSheetIndex(0);
    })->export($options['extension']);
  }

  function exportExcelPCA(Portfolio $portfolio, array $options = []){
    $options = array_merge([
      'maxDate' => null,
      'periods' => null,
      'period' => 'min', // for portfolio/base100 track
      'noFactor' => true,
      'filename' => null, // to override filename
      'extension' => 'xlsx', // file extension ('xlsx', 'xls', 'csv')
    ], $options);

    // function helpers
    $styleHeaderCells = function($cells) {
      $this->styleHeaderCells($cells);
    };

    $styleTopBorder = function($cells) {
      $this->styleTopBorder($cells);
    };

    // get pca options
    if (!$pca = $portfolio->pca) {
      throw new Exception('PCA not found');
    }

    // compute pca
    $period = $options['period'];
    $portfolioName = 'Portfolio ' . $portfolio->title;
    $maxDate = $options['maxDate']; // $this->backtesting->getMaxDate($portfolio);
    $periods = $options['periods']; // $this->backtesting->getPeriods($portfolio, $maxDate);
    $pcaOptions = json_decode($pca->options, true);
    $pcaOptions = array_merge($pcaOptions, [
      'maxDate' => $maxDate,
      'periods' => $periods,
      'period' => $period,
      'noFactor' => $options['noFactor'],
    ]);
    $pcaCal = $this->pca->calcul($portfolio, $pcaOptions);
    $pcCount = $pcaCal['summary']['pcCount'];
    $dates = $pcaCal['dates'];

    // get strategies
    $strategies = [];
    $strategyCodes = $pcaCal['listCodes'];
    $strategyCount = count($strategyCodes);
    $activeStrategies = $portfolio->getActiveStrategies();
    foreach ($strategyCodes as $code) { // sort strategies same as listCodes
      $strategy = $activeStrategies->first(function($key, $item) use($code) {
        return $item->code === $code;
      });
      $strategies[] = $strategy;
    }

    // PCA Results sheet
    $data1 = [
      [], // header logo
      [], // empty line
      [], // empty line
    ];

    // Portfolio Composition
    $data1[] = [$portfolioName];
    $data1[] = ['Name', 'Code', 'Asset Class', 'Currency', 'Region', 'Factor', 'Return Type', 'Style'];
    foreach ($strategies as $strategy) {
      $data1[] = [
        $strategy->shortName,
        $strategy->code,
        $strategy->assetClass,
        $strategy->currency,
        $strategy->region,
        $strategy->factor,
        $strategy->returnType,
        $strategy->style,
      ];
    }

    // PCA Model
    $row = 4;
    $col = 9;
    $this->appendPCAModel($data1, $row, $col, $pcaOptions, $maxDate, $periods[$period], count($dates));

    // Var Proportion
    $row = 4;
    $col += 3;
    $this->appendRow($data1, $row, $col, ['Var Proportion', 'Strategy PCA', 'Portfolio PCA']);
    $row++;
    for ($i=1; $i <= $pcCount; $i++) {
      $this->appendRow($data1, $row, $col, ["PC $i", $pcaCal["pc$i"]['varProp'], $pcaCal['portfolio']['varDecompose']["pc$i"]]);
      $row++;
    }
    $undetermined = isset($pcaCal['portfolio']['varDecompose']['undetermined']) ? $pcaCal['portfolio']['varDecompose']['undetermined'] : 0;
    $this->appendRow($data1, $row, $col, ["Undetermined", null, $undetermined]);

    // Var Distribution
    $row = 4;
    $col += 4;
    $this->appendRow($data1, $row, $col, ['Var Distribution']);
    for ($i=1; $i <= $pcCount; $i++) {
      $this->appendRow($data1, $row, $col + $i, ["PC $i"]);
    }
    $row++;
    foreach ($pcaCal['listCodes'] as $idx => $code) {
      $this->appendRow($data1, $row, $col, [$code]);
      for ($i=1; $i <= $pcCount; $i++) {
        $this->appendRow($data1, $row, $col + $i, [$pcaCal[$code]['varDecompose']["pc$i"]]);
      }
      $row++;
    }
    $this->appendRow($data1, $row, $col, [$portfolioName]);
    for ($i=1; $i <= $pcCount; $i++) {
      $this->appendRow($data1, $row, $col + $i, [$pcaCal['portfolio']['varDecompose']["pc$i"]]);
    }

    // Var Attribution
    $row = 4;
    $col += $pcCount + 2;
    $this->appendRow($data1, $row, $col, ['Var Attribution']);
    for ($i=1; $i <= $pcCount; $i++) {
      $this->appendRow($data1, $row, $col + $i, ["PC $i"]);
    }
    $row++;
    foreach ($pcaCal['listCodes'] as $idx => $code) {
      $this->appendRow($data1, $row, $col, [$code]);
      for ($i=1; $i <= $pcCount; $i++) {
        $this->appendRow($data1, $row, $col + $i, [$pcaCal[$code]['varAttr']["pc$i"]]);
      }
      $row++;
    }
    $this->appendRow($data1, $row, $col, [$portfolioName]);
    for ($i=1; $i <= $pcCount; $i++) {
      $this->appendRow($data1, $row, $col + $i, [$pcaCal['portfolio']['varAttr']["pc$i"]]);
    }

    // Coefficients
    $row = 4;
    $col += $pcCount + 2;
    $this->appendRow($data1, $row, $col, ['Coefficients']);
    for ($i=1; $i <= $pcCount; $i++) {
      $this->appendRow($data1, $row, $col + $i, ["PC $i"]);
    }
    $row++;
    foreach ($pcaCal['listCodes'] as $idx => $code) {
      $this->appendRow($data1, $row, $col, [$code]);
      for ($i=1; $i <= $pcCount; $i++) {
        $this->appendRow($data1, $row, $col + $i, [$pcaCal[$code]['coeff']["pc$i"]]);
      }
      $row++;
    }

    // Track Record
    $row = 4;
    $col += $pcCount + 2;
    $hasErrorTrack = false;
    $this->appendRow($data1, $row, $col, ['Date']);
    for ($i=1; $i <= $pcCount; $i++) {
      $this->appendRow($data1, $row, $col + $i, ["PC $i"]);
    }
    $row++;
    $datesCount = count($dates);
    for ($i = $datesCount - 1; $i >= 0; $i--) {
      $this->appendRow($data1, $row, $col, [Helper::time2excel(strtotime($dates[$i]))]);
      for ($j=1; $j <= $pcCount; $j++) {
        if ($pcaCal["pc$j"]['track_active']) {
          $this->appendRow($data1, $row, $col + $j, [$pcaCal["pc$j"]['pcas'][$i]]);
        } else if ($i === ($datesCount - 1)) {
          $hasErrorTrack = true;
          $this->appendRow($data1, $row, $col + $j, ['(1)']);
        } else {
          $hasErrorTrack = true;
          $this->appendRow($data1, $row, $col + $j, [null]);
        }
      }
      $row++;
    }
    if ($hasErrorTrack) {
      $row = 5;
      $col += $pcCount + 2;
      $this->appendRow($data1, $row, $col, [self::PCA_INVALID_TRACK]);
    }
    // Disclaimer sheet
    $data3 = [
      [], // empty line
    ];

    // Disclaimer
    $data3[] = [null, self::DISCLAIMER];
    $providers = [];
    foreach ($strategies as $strategy) {
      if (!empty($strategy->disclaimer) && !in_array($strategy->provider, $providers)) {
        $data3[] = []; // empty line
        $disclaimerParts = $this->splitDisclaimer($strategy->disclaimer);
        foreach ($disclaimerParts as $disclaimerPart) {
          $data3[] = [null, $disclaimerPart];
        }
        $providers[] = $strategy->provider;
      }
    }

    return Excel::create($options['filename'] ?: 'PCA_Portfolio ' . $portfolio->title, function($excel) use($data1, $data3, $pcCount, $styleHeaderCells, $styleTopBorder) {
      // PCA Results sheet
      $excel->sheet('PCA Results', function($sheet) use($data1, $pcCount, $styleHeaderCells, $styleTopBorder) {
        // header logo
        $sheet->setHeight(1, 60);
        $sheet->cells('1', function ($cells) {
          $cells->setBackground('#123B69');
        });
        $objDrawing = new PHPExcel_Worksheet_Drawing;
        $objDrawing->setPath(public_path('img/export/logo.png'));
        $objDrawing->setCoordinates('A1');
        $objDrawing->setResizeProportional(false);
        $objDrawing->setWidthAndHeight(280, 62);
        $objDrawing->setOffsetX(20);
        $objDrawing->setOffsetY(7);
        $objDrawing->setWorksheet($sheet);

        // Portfolio Composition
        $row = 4;
        $sheet->cells("A${row}", $styleHeaderCells);
        $row++;
        $sheet->cells("A${row}:H${row}", $styleHeaderCells);

        // PCA Model
        $sheet->cells("J${row}:K${row}", $styleHeaderCells);

        // Var Proportion
        $sheet->cells("M${row}:O${row}", $styleHeaderCells);
        $sheet->setColumnFormat(['N' => self::FORMAT_PERCENTAGE]);
        $sheet->setColumnFormat(['O' => self::FORMAT_PERCENTAGE]);

        // Var Distribution
        $col = 16;
        $col1 = PHPExcel_Cell::stringFromColumnIndex($col);
        $col2 = PHPExcel_Cell::stringFromColumnIndex($col + $pcCount);
        $col3 = $col1;
        $sheet->cells("${col1}${row}:${col2}${row}", $styleHeaderCells);
        while ($col1 !== $col2) {
          $col1 = PHPExcel_Cell::stringFromColumnIndex(++$col);
          $sheet->setColumnFormat([$col1 => self::FORMAT_PERCENTAGE]);
        }
        $row += $pcCount + 1;
        $sheet->cells("${col3}${row}:${col2}${row}", $styleTopBorder);

        // Var Attribution
        $row = 5;
        $col += 2;
        $col1 = PHPExcel_Cell::stringFromColumnIndex($col);
        $col2 = PHPExcel_Cell::stringFromColumnIndex($col + $pcCount);
        $col3 = $col1;
        $sheet->cells("${col1}${row}:${col2}${row}", $styleHeaderCells);
        while ($col1 !== $col2) {
          $col1 = PHPExcel_Cell::stringFromColumnIndex(++$col);
          $sheet->setColumnFormat([$col1 => self::FORMAT_PERCENTAGE]);
        }
        $row += $pcCount + 1;
        $sheet->cells("${col3}${row}:${col2}${row}", $styleTopBorder);

        // Coefficients
        $row = 5;
        $col += 2;
        $col1 = PHPExcel_Cell::stringFromColumnIndex($col);
        $col2 = PHPExcel_Cell::stringFromColumnIndex($col + $pcCount);
        $sheet->cells("${col1}${row}:${col2}${row}", $styleHeaderCells);
        while ($col1 !== $col2) {
          $col1 = PHPExcel_Cell::stringFromColumnIndex(++$col);
          $sheet->setColumnFormat([$col1 => self::FORMAT_PERCENTAGE]);
        }

        // Track Record
        $col += 2;
        $col1 = PHPExcel_Cell::stringFromColumnIndex($col);
        $col2 = PHPExcel_Cell::stringFromColumnIndex($col + $pcCount);
        $sheet->cells("${col1}${row}:${col2}${row}", $styleHeaderCells);
        $sheet->setColumnFormat([$col1 => self::FORMAT_DATE]);
        while ($col1 !== $col2) {
          $col1 = PHPExcel_Cell::stringFromColumnIndex(++$col);
          $sheet->setColumnFormat([$col1 => self::FORMAT_NUMBER]);
        }

        // unselect previous selection
        $sheet->getStyle('IU1');

        // inject sheet data
        $sheet->fromArray(
          $data1, // source
          null, // not used
          'A1', // start cell
          true, // show 0 as value instead of an empty cell (default: false)
          false // disable auto heading generation by indices (default: true)
        );
      });
      // Disclaimer sheet
      $excel->sheet('Disclaimer', function($sheet) use($data3) {
        // Disclaimer
        $sheet->setWidth('B', 100);
        $sheet->getStyle('B')->getAlignment()->setWrapText(true);
        $sheet->getStyle('A:IU')->applyFromArray([
          'fill' => [
            'type' => PHPExcel_Style_Fill::FILL_SOLID,
            'color' => ['rgb' => '123B69'],
          ],
          'font' => [
            'color' => ['rgb' => 'FFFFFF'],
          ],
        ]);

        // unselect previous selection
        $sheet->getStyle('IU1');

        // inject sheet data
        $sheet->fromArray(
          $data3, // source
          null, // not used
          'A1', // start cell
          true, // show 0 as value instead of an empty cell (default: false)
          false // disable auto heading generation by indices (default: true)
        );
      });

      // set default active sheet
      $excel->setActiveSheetIndex(0);
    })->export($options['extension']);
  }

  function exportExcelRegressionPortfolio(Portfolio $portfolio, array $options = []){
    $options = array_merge([
      'maxDate' => null,
      'periods' => null,
      'period' => 'min', // for portfolio/base100 track
      'noFactor' => true,
      'filename' => null, // to override filename
      'extension' => 'xlsx', // file extension ('xlsx', 'xls', 'csv')
    ], $options);

    // function helpers
    $styleHeaderCells = function($cells) {
      $this->styleHeaderCells($cells);
    };

    // get regression options
    if (!$regression = $portfolio->regression) {
      throw new Exception('Regression not found');
    }

    // compute regression
    $period = $options['period'];
    $maxDate = $options['maxDate']; //$this->backtesting->getMaxDate($portfolio);
    $periods = $options['periods']; //$this->backtesting->getPeriods($portfolio, $maxDate);
    $regOptions = json_decode($regression->options, true);
    $regOptions = array_merge($regOptions, [
      'maxDate' => $maxDate,
      'periods' => $periods,
      'period' => $period,
      'noFactor' => $options['noFactor'],
    ]);
    $regCal = $this->regression->getRegression($portfolio, $regOptions);
    $dateCount = count($regCal['dates']);
    $periods = $regCal['periods'];

    // get strategies
    $strategies = $portfolio->getActiveStrategies()->all();
    $strategyCount = count($strategies);

    // get factors
    $factors = [];
    $factorList = Helper::get($regOptions, 'factorList') ?: [];
    $factorCodes = [];
    foreach($factorList as $factor) {
      $factorCodes[] = $factor['code'];
    }
    $factorCount = count($factorCodes);
    $unsortedFactors = Indexinfo::whereIn('code', $factorCodes)->get();
    foreach ($factorCodes as $code) { // sort strategies same as listCodes
      $factor = $unsortedFactors->first(function($key, $item) use($code) {
        return $item->code === $code;
      });
      $factors[] = $factor;
    }

    // Regression Results sheet
    $data1 = [
      [], // header logo
      [], // empty line
      [], // empty line
    ];

    // Portfolio Description
    $data1[] = [];
    $data1[] = ['Name', 'Type', 'Model', 'Strategies', 'Constraints'];
    $allocOpt = json_decode($portfolio->allocationOptions, true);
    if (is_array($allocOpt)) {
      $allocOpt = array_merge(self::DEFAULT_ALLOCATION, $allocOpt);
    } else {
      $allocOpt = self::DEFAULT_ALLOCATION;
    }
    $model = $portfolio->weighting;
    $isFixedEquity = in_array($model, ['equal', 'manual']);
    $primary_allocation_constraint = Helper::get($allocOpt, 'primary_allocation_constraint', self::NONE);
    $secondary_allocation_constraint = Helper::get($allocOpt, 'secondary_allocation_constraint', self::NONE);
    $constraints = ($isFixedEquity && (
      $primary_allocation_constraint !== self::NONE || $secondary_allocation_constraint !== self::NONE
    )) ? 'Yes' : 'No';
    $data1[] = [$portfolio->title, 'Portfolio', self::MODEL_MAP[$model], $strategyCount, $constraints];

    // Regression Model
    $row = 4;
    $col = 6;
    $this->appendRegressionModel($data1, $row, $col, $regOptions, $maxDate, $periods[$period]);

    // Factor Model
    $row = 4;
    $col += 3;
    $this->appendRow($data1, $row, $col, ['Factor Model']);
    $row++;
    $this->appendRow($data1, $row, $col, ['Name', 'Code', 'Type', 'Asset Class', 'Currency', 'Region', 'Factor']);
    $row++;
    foreach ($factors as $factor) {
      $this->appendRow($data1, $row++, $col, [
        $factor->shortName,
        $factor->code,
        ($factor->type === 'Pure Factor') ? 'PLB Pure Factor' : 'Market Beta',
        $factor->assetClass,
        $factor->currency,
        $factor->region,
        $factor->factor,
      ]);
    }

    // Regression Metrics
    $row = 2;
    $col += 8;
    $this->appendRow($data1, $row++, $col, ['R Squared', $regCal['summary']['rSquared']]);
    $this->appendRow($data1, $row++, $col, ['F Stat', $regCal['summary']['fStat']]);
    $this->appendRow($data1, $row++, $col, [
      'Regression', 'Coefficient', 'Standard Error', 't-stat', 'p-value', 'Lower 95%', 'Upper 95%',
      'Return p.a. Attribution', 'Return p.a. Proportion', 'Variance Attribution', 'Variance Proportion',
    ]);
    foreach (array_merge(['alpha'], $factorCodes) as $code) {
      $this->appendRow($data1, $row++, $col, [
        ucfirst($code), $regCal[$code]['beta'], $regCal[$code]['betaSE'], $regCal[$code]['tStat'],
        $regCal[$code]['pValue'], $regCal[$code]['lowerBound'], $regCal[$code]['upperBound'],
        $regCal[$code]['returnAttr'], $regCal[$code]['returnProp'], $regCal[$code]['varAttr'],
        $regCal[$code]['varProp'],
      ]);
    }

    // Rolling Dates
    $row = 3;
    $col += 12;
    foreach (array_merge(['Rolling Dates', 'R Squared', 'alpha'], $factorCodes) as $code) {
      $this->appendRow($data1, $row++, $col, [ucfirst($code)]);
    }
    foreach ($regCal['dates'] as $idx => $date) {
      $row = 3;
      $col++;
      $this->appendRow($data1, $row++, $col, [$date]);
      $this->appendRow($data1, $row++, $col, [$regCal['summary']['rSquared_track'][$idx]]);
      foreach (array_merge(['alpha'], $factorCodes) as $code) {
        $this->appendRow($data1, $row++, $col, [$regCal[$code]['beta_track'][$idx]]);
      }
    }

    // Alpha Track Record sheet
    $data2 = [
      [], // header logo
      [], // empty line
      [], // empty line
    ];

    // Track Record
    $data2[] = [];
    $data2[] = ['Cumulated Date', 'Cumulated Alpha'];
    $cumulatedDateCount = count($regCal['alpha']['cumulated_dates']);
    for ($i=$cumulatedDateCount-1; $i >= 0; $i--) {
      $cumulatedDate = Helper::time2excel(strtotime($regCal['alpha']['cumulated_dates'][$i]));
      $cumulatedTrack = $regCal['alpha']['cumulated_track'][$i];
      $data2[] =[$cumulatedDate, $cumulatedTrack];
    }

    $row = 4;
    $col = 3;
    $this->appendRow($data2, $row, $col, ['Rolling Date', 'Rolling Cumulated Alpha']);
    $row++;
    $rollingDateCount = count($regCal['trackDates']);
    for ($i=$rollingDateCount-1; $i >= 0; $i--) {
      $rollingDate = Helper::time2excel(strtotime($regCal['trackDates'][$i]));
      $rollingTrack = $regCal['trackAlpha'][$i];
      $this->appendRow($data2, $row++, $col, [$rollingDate, $rollingTrack]);
    }

    // Disclaimer sheet
    $data3 = [
      [], // empty line
    ];

    // Disclaimer
    $data3[] = [null, self::DISCLAIMER];
    $providers = [];
    foreach ($strategies as $strategy) {
      if (!empty($strategy->disclaimer) && !in_array($strategy->provider, $providers)) {
        $data3[] = []; // empty line
        $disclaimerParts = $this->splitDisclaimer($strategy->disclaimer);
        foreach ($disclaimerParts as $disclaimerPart) {
          $data3[] = [null, $disclaimerPart];
        }
        $providers[] = $strategy->provider;
      }
    }

    return Excel::create($options['filename'] ?: 'Regression_Portfolio ' . $portfolio->title, function($excel) use($data1, $data2, $data3, $factorCount, $dateCount, $styleHeaderCells) {
      // Regression Results sheet
      $excel->sheet('Regression Results', function($sheet) use($data1, $factorCount, $dateCount, $styleHeaderCells) {
        // header logo
        $sheet->setHeight(1, 60);
        $sheet->cells('1', function ($cells) {
          $cells->setBackground('#123B69');
        });
        $objDrawing = new PHPExcel_Worksheet_Drawing;
        $objDrawing->setPath(public_path('img/export/logo.png'));
        $objDrawing->setCoordinates('A1');
        $objDrawing->setResizeProportional(false);
        $objDrawing->setWidthAndHeight(280, 62);
        $objDrawing->setOffsetX(20);
        $objDrawing->setOffsetY(7);
        $objDrawing->setWorksheet($sheet);

        // Portfolio Description
        $row = 5;
        $sheet->cells("A${row}:E${row}", $styleHeaderCells);

        // Regression Model
        $sheet->cells("G${row}:H${row}", $styleHeaderCells);

        // Factor Model
        $sheet->cells("J${row}:J${row}", $styleHeaderCells);
        $row++;
        $sheet->cells("J${row}:P${row}", $styleHeaderCells);

        // Regression Metrics
        $row = 3;
        $sheet->cells("R${row}:R${row}", $styleHeaderCells);
        $row++;
        $sheet->cells("R${row}:R${row}", $styleHeaderCells);
        $row++;
        $sheet->cells("R${row}:AB${row}", $styleHeaderCells);
        $sheet->setColumnFormat(['S' => self::FORMAT_NUMBER]);
        $sheet->setColumnFormat(['T' => self::FORMAT_NUMBER]);
        $sheet->setColumnFormat(['U' => self::FORMAT_NUMBER]);
        $sheet->setColumnFormat(['V' => self::FORMAT_PERCENTAGE]);
        $sheet->setColumnFormat(['W' => self::FORMAT_NUMBER]);
        $sheet->setColumnFormat(['X' => self::FORMAT_NUMBER]);
        $sheet->setColumnFormat(['Y' => self::FORMAT_PERCENTAGE]);
        $sheet->setColumnFormat(['Z' => self::FORMAT_PERCENTAGE]);
        $sheet->setColumnFormat(['AA' => self::FORMAT_PERCENTAGE]);
        $sheet->setColumnFormat(['AB' => self::FORMAT_PERCENTAGE]);
        $sheet->getStyle('S3')->getNumberFormat()->setFormatCode(self::FORMAT_PERCENTAGE);

        // Rolling Dates
        $row = 4;
        $col = 29;
        $col1 = PHPExcel_Cell::stringFromColumnIndex($col);
        $col2 = PHPExcel_Cell::stringFromColumnIndex($col + $dateCount);
        $sheet->cells("${col1}${row}:${col2}${row}", $styleHeaderCells);
        while ($col1 !== $col2) {
          $row1 = $row + 1;
          $col1 = PHPExcel_Cell::stringFromColumnIndex(++$col);
          $sheet->setColumnFormat([$col1 => self::FORMAT_NUMBER]);
          $sheet->getStyle("${col1}${row}")->getNumberFormat()->setFormatCode(self::FORMAT_DATE);
          $sheet->getStyle("${col1}${row1}")->getNumberFormat()->setFormatCode(self::FORMAT_PERCENTAGE);
        }

        // unselect previous selection
        $sheet->getStyle('IU1');

        // inject sheet data
        $sheet->fromArray(
          $data1, // source
          null, // not used
          'A1', // start cell
          true, // show 0 as value instead of an empty cell (default: false)
          false // disable auto heading generation by indices (default: true)
        );
      });
      // Alpha Track Record sheet
      $excel->sheet('Alpha Track Record', function($sheet) use($data2, $styleHeaderCells) {
        // header logo
        $sheet->setHeight(1, 60);
        $sheet->cells('1', function ($cells) {
          $cells->setBackground('#123B69');
        });
        $objDrawing = new PHPExcel_Worksheet_Drawing;
        $objDrawing->setPath(public_path('img/export/logo.png'));
        $objDrawing->setCoordinates('A1');
        $objDrawing->setResizeProportional(false);
        $objDrawing->setWidthAndHeight(280, 62);
        $objDrawing->setOffsetX(20);
        $objDrawing->setOffsetY(7);
        $objDrawing->setWorksheet($sheet);

        // Track Record
        $sheet->cells("A5:B5", $styleHeaderCells);
        $sheet->setColumnFormat(['A' => self::FORMAT_DATE]);
        $sheet->setColumnFormat(['B' => self::FORMAT_PERCENTAGE]);
        $sheet->cells("D5:E5", $styleHeaderCells);
        $sheet->setColumnFormat(['D' => self::FORMAT_DATE]);
        $sheet->setColumnFormat(['E' => self::FORMAT_PERCENTAGE]);

        // unselect previous selection
        $sheet->getStyle('IU1');

        // inject sheet data
        $sheet->fromArray(
          $data2, // source
          null, // not used
          'A1', // start cell
          true, // show 0 as value instead of an empty cell (default: false)
          false // disable auto heading generation by indices (default: true)
        );
      });
      // Disclaimer sheet
      $excel->sheet('Disclaimer', function($sheet) use($data3) {
        // Disclaimer
        $sheet->setWidth('B', 100);
        $sheet->getStyle('B')->getAlignment()->setWrapText(true);
        $sheet->getStyle('A:IU')->applyFromArray([
          'fill' => [
            'type' => PHPExcel_Style_Fill::FILL_SOLID,
            'color' => ['rgb' => '123B69'],
          ],
          'font' => [
            'color' => ['rgb' => 'FFFFFF'],
          ],
        ]);

        // unselect previous selection
        $sheet->getStyle('IU1');

        // inject sheet data
        $sheet->fromArray(
          $data3, // source
          null, // not used
          'A1', // start cell
          true, // show 0 as value instead of an empty cell (default: false)
          false // disable auto heading generation by indices (default: true)
        );
      });

      // set default active sheet
      $excel->setActiveSheetIndex(0);
    })->export($options['extension']);
  }

  function exportExcelRegressionStrategy(Indexinfo $strategy, array $options = []){
    $user = Auth::user();
    $options = array_merge([
      'maxDate' => null,
      'periods' => null,
      'period' => 'min', // for portfolio/base100 track
      'noFactor' => true,
      'filename' => null, // to override filename
      'extension' => 'xlsx', // file extension ('xlsx', 'xls', 'csv')
    ], $options);

    // function helpers
    $styleHeaderCells = function($cells) {
      $this->styleHeaderCells($cells);
    };

    // get regression options
    $regression = Regression::where('user_id', $user->id)
      ->where('indexinfo_id', $strategy->id)
      ->first();
    if (!$regression) {
      throw new Exception('Regression not found');
    }

    // compute regression
    $period = $options['period'];
    $maxDate = $options['maxDate']; //$this->backtesting->getMaxDate($strategy);
    $periods = $options['periods']; //$this->backtesting->getPeriods($strategy, $maxDate);
    $regOptions = json_decode($regression->options, true);
    $regOptions = array_merge($regOptions, [
      'maxDate' => $maxDate,
      'periods' => $periods,
      'period' => $period,
      'noFactor' => $options['noFactor'],
    ]);
    $regCal = $this->regression->getRegression($strategy, $regOptions);
    $dateCount = count($regCal['dates']);
    $periods = $regCal['periods'];

    // get factors
    $factors = [];
    $factorList = Helper::get($regOptions, 'factorList') ?: [];
    $factorCodes = [];
    foreach($factorList as $factor) {
      $factorCodes[] = $factor['code'];
    }
    $factorCount = count($factorCodes);
    $unsortedFactors = Indexinfo::whereIn('code', $factorCodes)->get();
    foreach ($factorCodes as $code) { // sort strategies same as listCodes
      $factor = $unsortedFactors->first(function($key, $item) use($code) {
        return $item->code === $code;
      });
      $factors[] = $factor;
    }

    // Regression Results sheet
    $data1 = [
      [], // header logo
      [], // empty line
      [], // empty line
    ];

    // Strategy Description
    $data1[] = [];
    $data1[] = ['Name', 'Type', 'Code', 'Asset Class', 'Currency', 'Region', 'Factor', 'Return Type', 'Style'];
    $data1[] = [
      $strategy->shortName,
      'Strategy',
      $strategy->code,
      $strategy->assetClass,
      $strategy->currency,
      $strategy->region,
      $strategy->factor,
      $strategy->returnType,
      $strategy->style,
    ];

    // Regression Model
    $row = 4;
    $col = 10;
    $this->appendRegressionModel($data1, $row, $col, $regOptions, $maxDate, $periods[$period]);

    // Factor Model
    $row = 4;
    $col += 3;
    $this->appendRow($data1, $row, $col, ['Factor Model']);
    $row++;
    $this->appendRow($data1, $row, $col, ['Name', 'Code', 'Type', 'Asset Class', 'Currency', 'Region', 'Factor']);
    $row++;
    foreach ($factors as $factor) {
      $this->appendRow($data1, $row++, $col, [
        $factor->shortName,
        $factor->code,
        ($factor->type === 'Pure Factor') ? 'PLB Pure Factor' : 'Market Beta',
        $factor->assetClass,
        $factor->currency,
        $factor->region,
        $factor->factor,
      ]);
    }

    // Regression Metrics
    $row = 2;
    $col += 8;
    $this->appendRow($data1, $row++, $col, ['R Squared', $regCal['summary']['rSquared']]);
    $this->appendRow($data1, $row++, $col, ['F Stat', $regCal['summary']['fStat']]);
    $this->appendRow($data1, $row++, $col, [
      'Regression', 'Coefficient', 'Standard Error', 't-stat', 'p-value', 'Lower 95%', 'Upper 95%',
      'Return p.a. Attribution', 'Return p.a. Proportion', 'Variance Attribution', 'Variance Proportion',
    ]);
    foreach (array_merge(['alpha'], $factorCodes) as $code) {
      $this->appendRow($data1, $row++, $col, [
        ucfirst($code), $regCal[$code]['beta'], $regCal[$code]['betaSE'], $regCal[$code]['tStat'],
        $regCal[$code]['pValue'], $regCal[$code]['lowerBound'], $regCal[$code]['upperBound'],
        $regCal[$code]['returnAttr'], $regCal[$code]['returnProp'], $regCal[$code]['varAttr'],
        $regCal[$code]['varProp'],
      ]);
    }

    // Rolling Dates
    $row = 3;
    $col += 12;
    foreach (array_merge(['Rolling Dates', 'R Squared', 'alpha'], $factorCodes) as $code) {
      $this->appendRow($data1, $row++, $col, [ucfirst($code)]);
    }
    foreach ($regCal['dates'] as $idx => $date) {
      $row = 3;
      $col++;
      $this->appendRow($data1, $row++, $col, [$date]);
      $this->appendRow($data1, $row++, $col, [$regCal['summary']['rSquared_track'][$idx]]);
      foreach (array_merge(['alpha'], $factorCodes) as $code) {
        $this->appendRow($data1, $row++, $col, [$regCal[$code]['beta_track'][$idx]]);
      }
    }

    // Alpha Track Record sheet
    $data2 = [
      [], // header logo
      [], // empty line
      [], // empty line
    ];

    // Track Record
    $data2[] = [];
    $data2[] = ['Cumulated Date', 'Cumulated Alpha'];
    $cumulatedDateCount = count($regCal['alpha']['cumulated_dates']);
    for ($i=$cumulatedDateCount-1; $i >= 0; $i--) {
      $cumulatedDate = Helper::time2excel(strtotime($regCal['alpha']['cumulated_dates'][$i]));
      $cumulatedTrack = $regCal['alpha']['cumulated_track'][$i];
      $data2[] =[$cumulatedDate, $cumulatedTrack];
    }

    $row = 4;
    $col = 3;
    $this->appendRow($data2, $row, $col, ['Rolling Date', 'Rolling Cumulated Alpha']);
    $row++;
    $rollingDateCount = count($regCal['trackDates']);
    for ($i=$rollingDateCount-1; $i >= 0; $i--) {
      $rollingDate = Helper::time2excel(strtotime($regCal['trackDates'][$i]));
      $rollingTrack = $regCal['trackAlpha'][$i];
      $this->appendRow($data2, $row++, $col, [$rollingDate, $rollingTrack]);
    }

    // Disclaimer sheet
    $data3 = [
      [], // empty line
    ];

    // Disclaimer
    $data3[] = [null, self::DISCLAIMER];
    if (!empty($strategy->disclaimer)) {
      $data3[] = []; // empty line
      $disclaimerParts = $this->splitDisclaimer($strategy->disclaimer);
      foreach ($disclaimerParts as $disclaimerPart) {
        $data3[] = [null, $disclaimerPart];
      }
    }

    return Excel::create($options['filename'] ?: 'Regression_Strategy ' . $strategy->code, function($excel) use($data1, $data2, $data3, $factorCount, $dateCount, $styleHeaderCells) {
      // Regression Results sheet
      $excel->sheet('Regression Results', function($sheet) use($data1, $factorCount, $dateCount, $styleHeaderCells) {
        // header logo
        $sheet->setHeight(1, 60);
        $sheet->cells('1', function ($cells) {
          $cells->setBackground('#123B69');
        });
        $objDrawing = new PHPExcel_Worksheet_Drawing;
        $objDrawing->setPath(public_path('img/export/logo.png'));
        $objDrawing->setCoordinates('A1');
        $objDrawing->setResizeProportional(false);
        $objDrawing->setWidthAndHeight(280, 62);
        $objDrawing->setOffsetX(20);
        $objDrawing->setOffsetY(7);
        $objDrawing->setWorksheet($sheet);

        // Strategy Description
        $row = 5;
        $sheet->cells("A${row}:I${row}", $styleHeaderCells);

        // Regression Model
        $sheet->cells("K${row}:L${row}", $styleHeaderCells);

        // Factor Model
        $sheet->cells("N${row}:N${row}", $styleHeaderCells);
        $row++;
        $sheet->cells("N${row}:T${row}", $styleHeaderCells);

        // Regression Metrics
        $row = 3;
        $sheet->cells("V${row}:V${row}", $styleHeaderCells);
        $row++;
        $sheet->cells("V${row}:V${row}", $styleHeaderCells);
        $row++;
        $sheet->cells("V${row}:AF${row}", $styleHeaderCells);
        $sheet->setColumnFormat(['W' => self::FORMAT_NUMBER]);
        $sheet->setColumnFormat(['X' => self::FORMAT_NUMBER]);
        $sheet->setColumnFormat(['Y' => self::FORMAT_NUMBER]);
        $sheet->setColumnFormat(['Z' => self::FORMAT_PERCENTAGE]);
        $sheet->setColumnFormat(['AA' => self::FORMAT_NUMBER]);
        $sheet->setColumnFormat(['AB' => self::FORMAT_NUMBER]);
        $sheet->setColumnFormat(['AC' => self::FORMAT_PERCENTAGE]);
        $sheet->setColumnFormat(['AD' => self::FORMAT_PERCENTAGE]);
        $sheet->setColumnFormat(['AE' => self::FORMAT_PERCENTAGE]);
        $sheet->setColumnFormat(['AF' => self::FORMAT_PERCENTAGE]);
        $sheet->getStyle('W3')->getNumberFormat()->setFormatCode(self::FORMAT_PERCENTAGE);

        // Rolling Dates
        $row = 4;
        $col = 33;
        $col1 = PHPExcel_Cell::stringFromColumnIndex($col);
        $col2 = PHPExcel_Cell::stringFromColumnIndex($col + $dateCount);
        $sheet->cells("${col1}${row}:${col2}${row}", $styleHeaderCells);
        while ($col1 !== $col2) {
          $row1 = $row + 1;
          $col1 = PHPExcel_Cell::stringFromColumnIndex(++$col);
          $sheet->setColumnFormat([$col1 => self::FORMAT_NUMBER]);
          $sheet->getStyle("${col1}${row}")->getNumberFormat()->setFormatCode(self::FORMAT_DATE);
          $sheet->getStyle("${col1}${row1}")->getNumberFormat()->setFormatCode(self::FORMAT_PERCENTAGE);
        }

        // unselect previous selection
        $sheet->getStyle('IU1');

        // inject sheet data
        $sheet->fromArray(
          $data1, // source
          null, // not used
          'A1', // start cell
          true, // show 0 as value instead of an empty cell (default: false)
          false // disable auto heading generation by indices (default: true)
        );
      });
      // Alpha Track Record sheet
      $excel->sheet('Alpha Track Record', function($sheet) use($data2, $styleHeaderCells) {
        // header logo
        $sheet->setHeight(1, 60);
        $sheet->cells('1', function ($cells) {
          $cells->setBackground('#123B69');
        });
        $objDrawing = new PHPExcel_Worksheet_Drawing;
        $objDrawing->setPath(public_path('img/export/logo.png'));
        $objDrawing->setCoordinates('A1');
        $objDrawing->setResizeProportional(false);
        $objDrawing->setWidthAndHeight(280, 62);
        $objDrawing->setOffsetX(20);
        $objDrawing->setOffsetY(7);
        $objDrawing->setWorksheet($sheet);

        // Track Record
        $sheet->cells("A5:B5", $styleHeaderCells);
        $sheet->setColumnFormat(['A' => self::FORMAT_DATE]);
        $sheet->setColumnFormat(['B' => self::FORMAT_PERCENTAGE]);
        $sheet->cells("D5:E5", $styleHeaderCells);
        $sheet->setColumnFormat(['D' => self::FORMAT_DATE]);
        $sheet->setColumnFormat(['E' => self::FORMAT_PERCENTAGE]);

        // unselect previous selection
        $sheet->getStyle('IU1');

        // inject sheet data
        $sheet->fromArray(
          $data2, // source
          null, // not used
          'A1', // start cell
          true, // show 0 as value instead of an empty cell (default: false)
          false // disable auto heading generation by indices (default: true)
        );
      });
      // Disclaimer sheet
      $excel->sheet('Disclaimer', function($sheet) use($data3) {
        // Disclaimer
        $sheet->setWidth('B', 100);
        $sheet->getStyle('B')->getAlignment()->setWrapText(true);
        $sheet->getStyle('A:IU')->applyFromArray([
          'fill' => [
            'type' => PHPExcel_Style_Fill::FILL_SOLID,
            'color' => ['rgb' => '123B69'],
          ],
          'font' => [
            'color' => ['rgb' => 'FFFFFF'],
          ],
        ]);

        // unselect previous selection
        $sheet->getStyle('IU1');

        // inject sheet data
        $sheet->fromArray(
          $data3, // source
          null, // not used
          'A1', // start cell
          true, // show 0 as value instead of an empty cell (default: false)
          false // disable auto heading generation by indices (default: true)
        );
      });

      // set default active sheet
      $excel->setActiveSheetIndex(0);
    })->export($options['extension']);
  }

}
