<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use App\CommunityMember;
use App\ShareGroup;
use App\FeedArticleTag;
use Illuminate\Support\Facades\Auth;
use App\FeedArticleImage;

class FeedArticle extends Model
{
    use Sluggable;

    protected $table = 'feed_articles';
    protected $guarded = ['id'];
    public $appends = ['image', 'provider', 'shareWith', 'url', 'canedit'];
    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */

    public function checkReadAccessOrFail(){
        return true;
    }

    public function removeImages(){
        $toRemove = FeedArticleImage::where('feed_articles_id', '=', $this->id)
            ->get();
        foreach($toRemove as $removeImg){
            $removeImg->delete();
        }
    }

    public function attachImage($path){
        $this->removeImages();
        $newItem = FeedArticleImage::create([
            'feed_articles_id' => $this->id,
            'url' => $path,
            'title' => $this->title
        ]);
        return $newItem->save();
    }

    public function shareWithGroups($groups){
        $toRemove = FeedArticleTag::where('feed_articles_id', '=', $this->id)
            ->where('group', '=', 'access')
            ->get();
        foreach($toRemove as $removeTag){
            $removeTag->delete();
        }

        if(empty($groups) || !is_array($groups)){
            $groups = ['all'];
        }
        foreach($groups as $group){
            $newItem = FeedArticleTag::create([
                'group' => 'access',
                'feed_articles_id' => $this->id,
                'title' => $group
            ]);
            $newItem->save();
        }
        return true;
    }

    public function getUrlAttribute(){
        return '/community/feed/' . $this->slug;
    }

    public function getShareWithAttribute(){
        return FeedArticleTag::where('feed_articles_id', '=', $this->id)
            ->where('group', '=', 'access')
            ->pluck('title');
    }

    public function getImageAttribute(){
        $images = FeedArticleImage::where('feed_articles_id', '=', $this->id);
        if($images->count() > 0){
            return $images->pluck('url')->first();
        }
        return null;
    }


    public function getProviderAttribute(){
        return CommunityMember::where('id', $this->community_member_id)->first();
    }

    public function getCanEditAttribute(){
        return $this->canEdit(Auth::user());
    }

    public function canEdit(User $user){
        if($user->isAdmin()){
            return true;
        }
        if($this->type == 'News'){
            return false;//unless admin.
        }
        $isMember = $this->provider->isMember($user);
        return $isMember;//in case we need roles later.
    }

    public function sluggable(){
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

}
