<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use App\Setting;
use Exception;
use Session;
use DB;

class Indexinfo extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $table = 'indexinfo';
    protected $guarded = ['id'];

    protected $hidden = ['datafeed'];

    public $appends = ['is_editable'];

    public $valueCache = array('max_before' => [], 'max_after' => [], 'min_before' => [], 'min_after' => [], 'min_date' => '');
    public $minDate;
    public $maxDate;
    public $cachedResults;
    public $cachedResultsByIdx;
    public $cachedResultsOrdered;

    public $cachedMaxValueForPeriod = [
        '6m' => 0,
        '1y' => 0,
        '3y' => 0,
        '5y' => 0,
        '10y' => 0,
        '15y' => 0,
    ];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function getIsEditableAttribute(){
      return $this->type === 'Private Strategy' && !$this->datafeed;
    }

    public function sd_square($x, $mean) { return pow($x - $mean,2); }

    // Function to calculate standard deviation (uses sd_square)
    public function sd($array) {
        // Function to calculate square of value - mean
        return sqrt(array_sum(array_map(array($this, "sd_square"), $array, array_fill(0,count($array), (array_sum($array) / count($array)) ) ) ) / (count($array)-1) );
    }

    public function is_leap_year($year)
    {
        return ((($year % 4) == 0) && ((($year % 100) != 0) || (($year %400) == 0)));
    }

    public function getNumDaysInYear($year){
        if($this->is_leap_year($year)){
            //IF THE LEAP YEAR IS WITHIN THE PREVIOUS 12 months of the current cut off date, count it as a leap year.
            return 366;
        }
        return 365;
    }

    public function getValidTime($year, $month, $day, $iteration = 0) {
        $day = $day - $iteration;
        if (checkdate($month, $day, $year)) {
          return strftime('%F 00:00:00', strtotime($year . '-' . $month . '-' . $day));
        }
        $iteration = 1;
        return $this->getValidTime($year, $month, $day, $iteration);
    }

    public function getReturnForYear($year){
        $values = $this->getFirstAndLastValueOfYear($year);
        $return = ($values['end'] / $values['start']) - 1;
        $return = 100 * $return;
        $return = number_format($return, 2);
        return $return;
    }

}
