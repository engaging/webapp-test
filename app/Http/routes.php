<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Since Laravel 5.2.27 all routes are registered in the web middleware group by default.
// https://github.com/laravel/laravel/blob/v5.2.27/app/Providers/RouteServiceProvider.php#L56
// https://github.com/laravel/framework/issues/13000

Route::group(['prefix' => '/health'], function () {
  Route::get('/phpini', 'HealthController@showPhpIniSettings');
});

Route::get('/request', 'Auth\RegisterController@show');
Route::post('/request', 'Auth\RegisterController@register');
Route::get('/activate/{approval_token}', 'Auth\RegisterController@approved');
Route::post('/setpassword', 'Auth\RegisterController@setPassword');
Route::get('/inactive', 'Auth\RegisterController@inactive');

Route::get('/', 'HomeController@show');
Route::get('/product-solution', 'HomeController@showProductSolution');
Route::get('/home', 'HomeController@show');
Route::get('/strategy', 'StrategyController@show');
Route::group(['prefix' => '/analyse'], function () {
  Route::get('/', 'AnalyseController@showPortfolioSelection');
  Route::get('/portfolio/{slug}', 'AnalyseController@showBacktesting');
  Route::get('/pca/portfolio/{slug}', 'AnalyseController@showPCA');
  Route::get('/regression/portfolio/{slug}', 'AnalyseController@portfoloRegression');
  Route::get('/regression/strategy/{code}', 'AnalyseController@strategyRegression');
});
Route::get('/strategy/{code}', 'StrategyController@showOne');
Route::get('/strategy/{code}/export', 'StrategyController@exportTrack');
Route::get('/strategies/{code}/export/backtesting/excel', 'StrategyController@exportTrack');
Route::get('/strategies/{strategy}/export/regression/excel', 'StrategyController@exportRegression');

Route::get('/widgets', 'WidgetController@show');
Route::get('/dashboard', 'WidgetController@show');
Route::get('/dashboard/reset', 'WidgetController@reset');

Route::post('/strategy/{code}/pdf', 'StrategyController@pdfPost');
Route::post('/portfolio/{slug}/pdf', 'StrategyController@pdfPost');

Route::get('/portfolios', 'PortfolioController@show');
Route::post('/portfolios/pdf', 'StrategyController@pdfPost');
Route::get('/portfolios/{slug}', 'PortfolioController@showOne');
Route::get('/portfolios/{slug}/export', 'PortfolioController@exportTrack');
Route::get('/portfolios/{slug}/export/backtesting/excel', 'PortfolioController@exportTrack');
Route::get('/portfolios/{portfolio}/export/pca/excel', 'PortfolioController@exportPCA');
Route::get('/portfolios/{portfolio}/export/regression/excel', 'PortfolioController@exportRegression');

Route::get('/pending', function(){
	return view('pending');
});

Route::get('/contact', 'ContactController@show');
Route::post('/contact', 'ContactController@send');

// Route::get('/premium', 'ContactController@showPremium');
// Route::get('/premium-requested', 'ContactController@showPremiumRequested');

Route::get('/community', 'NetworkController@show');
Route::get('/community/feed/{post}', 'NetworkController@showFeedArticle');
Route::get('/sponsor/{sponsor}', 'ProviderController@show');

Route::group(['prefix' => '/inbox'], function () {
    Route::get('/', ['as' => 'messages', 'uses' => 'InboxController@index']);
    Route::get('create', ['as' => 'messages.create', 'uses' => 'InboxController@create']);
    Route::post('/', ['as' => 'messages.store', 'uses' => 'InboxController@store']);
    Route::get('/message/{id}', ['as' => 'messages.show', 'uses' => 'InboxController@show']);
    Route::put('/message/{id}', ['as' => 'messages.update', 'uses' => 'InboxController@update']);
    Route::get('/notification/{uuid}', ['as' => 'notification.show', 'uses' => 'InboxController@showNotification']);
    Route::get('/announcement/{uuid}', ['as' => 'announcement.show', 'uses' => 'InboxController@showAnnouncement']);
});
