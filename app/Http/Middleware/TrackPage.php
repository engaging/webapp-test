<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Auth;
use App\Services\Analytics\AnalyticsService;
use Closure;

class TrackPage
{

    const BLACKLIST = [
      '/spark/plans',
      '/notifications/recent',
    ];

    public function __construct(AnalyticsService $analytics)
    {
        $this->analytics = $analytics;
    }

    /**
     * Track page.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (env('ANALYTICS_KEY') && $request->method() === 'GET' && $user = Auth::user()) {
          $path = $request->path();
          if (strpos($path, '/') !== 0) {
            $path = '/' . $path;
          }

          if (!in_array($path, self::BLACKLIST)) {
            $this->analytics->page([
              'userId' => $user->id,
              'properties' => [
                'path' => $path,
              ],
              'context' => [
                'ip' => $request->ip(),
                'userAgent' => $request->header('User-Agent'),
              ],
            ]);
          }
        }
        return $next($request);
    }

}
