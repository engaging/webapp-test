<?php

namespace App\Http\Controllers\API;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Constant;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use App\Services\Right\RightService;
use App\UserRight;
use Helper;
use Hash;
use DB;

class AuthController extends Controller implements Constant
{

    public function __construct(RightService $right){
      $this->right = $right;
    }

    public function authorizeWebApiGateway(Request $request){
        // region, awsAccountId, restApiId, stage, method, version, resource, path
        $user = Auth::user();
        $principalId = str_pad("$user->id", 2, '0', STR_PAD_LEFT);
        $resource = $request->input('resource', '');
        $method = $request->input('method', '');
        $path = $request->input('path', '');
        $route = "$method $path";

        $match = function(string $pattern) use($route) {
          return (preg_match($pattern, $route) ? $route : !$route);
        };

        $authorization = true;
        switch ($resource) {

          case '/news':
            switch ($route) {
              case 'GET /news/sources':
              case 'POST /news/query':
              case $match('/PATCH \/news\/.*/'):
                $authorization = $user->isAdmin();
                break;
            }
            break;
        }

        return response()->json([
          'principalId' => $principalId,
          'authorization' => ($authorization === true) ? 'allow' : 'deny',
        ]);
    }

    public function authorizeDataApiGateway(Request $request){
        $client_id = $request->input('client_id');
        if (!$client_id) {
          return response('Unauthorized.', 401);
        }

        $client_secret = $request->input('client_secret');
        if (!$client_secret) {
          return response('Unauthorized.', 401);
        }

        $right = UserRight::where('api_key', $client_id)->first();
        if (!$right) {
          return response('Unauthorized.', 401);
        }

        if (!Hash::check($client_secret, $right->api_secret)) {
          return response('Unauthorized.', 401);
        }

        $user = $right->user;
        if (!$user) {
          return response('Unauthorized.', 401);
        }

        $sub = str_pad("$user->id", 2, '0', STR_PAD_LEFT);
        $admin = $user->isAdmin();
        $scope = strval($right->api_scope);

        $only = $this->right->getAuthorizedStrategies($user)->pluck('code');

        return response()->json([
          'sub' => $sub,
          'name' => $user->fullname,
          'admin' => $admin,
          'team' => Helper::get($user->currentTeam, 'id'),
          'scope' => $scope,
          'only' => $only,
        ]);
    }

}
