<?php

namespace App\Http\Controllers\API;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Constant;
use App\Http\Requests;
use App\Portfolio;
use App\PortfolioStrategy;
use App\Indexinfo;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use App\User;
use App\Share;
use App\Services\Lab\BacktestingService;
use App\Services\Right\RightService;
use App\Services\Portfolio\PortfolioService;
use Throwable;
use Log;
use DB;
use URL;
use Mail;
use Helper;
use function Tarsana\Functional\{ startsWith, endsWith, anySatisfies };

class PortfolioController extends Controller implements Constant
{
    public function __construct(BacktestingService $backtesting, RightService $right, PortfolioService $getDraft){
        $this->backtesting = $backtesting;
        $this->right = $right;
        $this->getDraft = $getDraft;
    }

    public function getPortfolios(){
      $userId = Auth::user()->id;

      $query = Portfolio::where('user_id', $userId);
      if (Input::get('favourite') === 'true') {
        $query->where(function($groupClause) {
          $groupClause->whereNull('draft_of')
            ->orWhere('draft_of', 0);
        });
      } else {
        $query->whereNull('draft_of');
      }

      $portfolios = $query->orderBy('portfolios.updated_at', 'desc')->get()->all();

      if (Input::get('metrics') === 'true') {
        $portfolios = $this->backtesting->getPortfoliosMetrics($portfolios, [
          'userId' => $userId,
          'periodKeys' => Input::get('periodKeys', self::PERIOD_KEYS),
          'activeOnly' => Input::get('activeOnly') === 'true',
        ]);
      }

      if (Input::get('shares') === 'true') {
        foreach ($portfolios as $idx => $portfolio) {
          $shares = Share::where('sharing.element_type', 'portfolio')
            ->where('sharing.element_id', $portfolio['id'])
            ->get();
          $portfolio->shares = count($shares);
          $portfolios[$idx] = $portfolio;
        }
      }

      return $portfolios;
    }

    public function getPortfolioById(Portfolio $portfolio){
      if (!$this->right->hasPortfolioRight(Auth::user(), $portfolio)) {
        abort(403, 'Restricted access');
      }

      if (!$portfolio->activeStrategiesCount) {
        return $portfolio;
      }

      $portfolio = $this->backtesting->getPortfolio($portfolio, [
        'maxDate' => Input::get('maxDate'),
        'periods' => Input::get('periods'),
        'volMeasure' => Input::get('volMeasure', '30d'),
        'cache' => $this->getCacheOption(),
      ]);

      if (Input::get('shares') === 'true') {
        $shares = Share::where('sharing.element_type', 'portfolio')
          ->where('sharing.element_id', $portfolio['id'])
          ->get();
        $portfolio->shares = $shares;
      }

      return $portfolio;
    }

    public function getAllocationById(Portfolio $portfolio) {
      if (!$this->right->hasPortfolioRight(Auth::user(), $portfolio)) {
        abort(403, 'Restricted access');
      }
      $options = Input::all();
      $weightings = $this->backtesting->getAllocation($options);
      return $weightings;
    }

    public function getFundingById(Portfolio $portfolio) {
      if (!$this->right->hasPortfolioRight(Auth::user(), $portfolio)) {
        abort(403, 'Restricted access');
      }
      $options = Input::all();
      $fundings = $this->backtesting->getFunding($options);
      return $fundings;
    }

    public function getPortfolioOptionById(Portfolio $portfolio){
      if (!$this->right->hasPortfolioRight(Auth::user(), $portfolio)) {
        abort(403, 'Restricted access');
      }

      if (!$portfolio->activeStrategiesCount) {
        return $portfolio;
      }
      
      $allocationOptions = json_decode($portfolio->allocationOptions);

      if (empty($allocationOptions)) {
        $allocationOptions = (object)[];
      }

      $allocationOptions->rebalancing = Input::get('rebalancing', 'Monthly');
      $allocationOptions->rebalancing_month = Input::get('rebalancing_month', 'Every Month');
      $allocationOptions->rebalancing_date = Input::get('rebalancing_date', 'Last Day');
      $portfolio->allocationOptions = json_encode($allocationOptions);
      $portfolio->weighting = 'equal';
      $options = $this->backtesting->getPortfolio($portfolio, [
        'maxDate' => Input::get('maxDate'),
        'periods' => Input::get('periods'),
        'optionsOnly' => true,
      ]);
      return $options;
    }

    public function getPeriodsById(Portfolio $portfolio){
      if (!$this->right->hasPortfolioRight(Auth::user(), $portfolio)) {
        abort(403, 'Restricted access');
      }

      $maxDate = $this->backtesting->getMaxDate($portfolio);
      $periods = $this->backtesting->getPeriods($portfolio, $maxDate);

      return [
        'maxDate' => $maxDate,
        'periods' => $periods,
      ];
    }

    

    public function getPortfolioStrategiesById(Portfolio $portfolio){
        if (!$this->right->hasPortfolioRight(Auth::user(), $portfolio)) {
          abort(403, 'Restricted access');
        }

        $strategies = $portfolio->getPortfolioStrategiesWithIndexinfo();

        if (Input::get('metrics') === 'true') {
          $strategies = $this->backtesting->getStrategiesMetrics($strategies, [
            'periodKeys' => Input::get('periodKeys', self::PERIOD_KEYS),
            'activeOnly' => Input::get('activeOnly') === 'true',
            'unauthorized' => true,
          ]);
        }

        return $strategies;
    }

    public function getVolatilityTrackById(Portfolio $portfolio){
      $track = [];

      $this->backtesting->fillVolatilityTrack($portfolio, $track, [
        'maxDate' => Input::get('maxDate'),
        'periods' => Input::get('periods'),
        'period' => Input::get('period', '5y'),
        'measure' => Input::get('measure', '30d'),
      ]);

      return $track;
    }

    public function getBenchmarksCorrelationById(Portfolio $portfolio){
      $portfolioOnly = Input::get('portfolioOnly') === 'true';
      $ids = array_unique(Input::get('ids', []));
      $ids_ordered = implode(',', $ids);

      $benchmarkCodes = Indexinfo::whereIn('id', $ids)
        ->orderBy(DB::raw("FIELD(id,$ids_ordered)")) // array should be ordered for benchmarks correlation
        ->pluck('code')->toArray();

      $correl = $this->backtesting->getCorrelationWithBenchmarks($portfolio, $benchmarkCodes, [
        'maxDate' => Input::get('maxDate'),
        'periods' => Input::get('periods'),
        'period' => Input::get('period', '5y'),
      ]);

      return ($portfolioOnly) ? $correl['p'] : $correl;
    }

    public function getPortfolioDraft($slug) {
      $portfolio = Portfolio::where(
        'user_id', '=', Auth::user()->id
      )->where(
        'draft_of', '=', null
      )->where(
        'slug', '=', $slug
      );

      if($portfolio->count() == 0){
          abort(403, 'Unauthorized action.');
      }

      $draft = $portfolio->first()->getDraft(true, true);

      $maxDate = $this->backtesting->getMaxDate($draft);
      $periods = $this->backtesting->getPeriods($draft, $maxDate);


      $customRebalancing = json_decode($draft->customRebalancing, true);
      $customBaseOptions = null;
      $customAllocation = null;
      if (!empty($customRebalancing)) {
        $customBaseOptions = $customRebalancing['customBaseOptions'];
        $customAllocation = $customRebalancing['customAllocation'];
      }

      return [
        'maxDate' => $maxDate,
        'periods' => $periods,
        'selected' => 'portfolio',
        'analysing' => [
          'slug' => $portfolio->first()->slug, //leave this one as the real one
          'groupBy' => 'all',
          'title' => $draft->title,
          'weighting' => $draft->weighting,
          'portfolio_id' => $draft->id,
          'draft_of' => $draft->draft_of,
          'strategyIds' => $draft->strategyIds,
          'allocationOptions' => json_decode($draft->allocationOptions),
          'customBaseOptions' => $customBaseOptions,
          'rebalancingList' => $customAllocation,
        ],
      ];
    }

    public function getSelectFields(){
        $selections = ['id', 'title', 'slug', 'user_id'];

        $extrafields = Input::get('extrafields');
        if(!empty($extrafields) && is_array($extrafields)){
            $selections = array_merge($selections, $extrafields);
        }

        $selections = implode(", ", $selections);
        return DB::raw($selections);
    }

    public function breakdown(Portfolio $portfolio)
    {
        return $portfolio->cacheBreakdowns();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $rules = array(
            'title'       => 'required',
        );

        $this->validate($request, $rules);

        $portfolios = Portfolio::where(
            'user_id', '=', Auth::user()->id
        )->where(
            'title', '=', Input::get('title')
        )->where(
            'draft_of', '=', null
        );//->withTrashed();

        if($portfolios->count() > 0){
            $message = [
                'title' => ["Please choose a unique title for your portfolio."]
            ];
            return Response::json($message, 500);
        }


        $portfolio = new Portfolio;
        $portfolio->weighting = 'equal';
        $portfolio->title = Input::get('title');
        $portfolio->user_id = Auth::user()->id;
        $portfolio->save();

        $indices = Input::get('indices');
        if($indices == 'favourites'){
            $portfolio->attachAllFavourites();
        }

        return $portfolio;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Portfolio $portfolio)
    {
        if (!$portfolio->activeStrategiesCount) {
          return $portfolio;
        }
        return $this->backtesting->getPortfolio($portfolio, [
          'maxDate' => Input::get('maxDate'),
          'periods' => Input::get('periods'),
        ]);
    }

    public function getJson(Portfolio $portfolio)
    {
        return $this->backtesting->getPortfolio($portfolio, [
          'maxDate' => Input::get('maxDate'),
          'periods' => Input::get('periods'),
        ])->getJson();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    public function removeIndices($indices, $portfolio){
        $ids = [];
        $indexIds = [];
        foreach ($indices as $key => $index) {
           $ids[] = $index['id'];
           $indexIds[] = $index['indexinfo_id'];
        }
        $loadedIndices = $portfolio->hasMany('App\PortfolioStrategy')->whereIn('id', $ids)->get();

        foreach($loadedIndices as $idx => $index){
            $index->delete();
        }

        // if is a favourites draft
        if ($portfolio->draft_of === 0) {
            //means it's a favourites. should remove faves too here.
            $faves = Auth::user()->hasMany('App\FavouriteItem')->whereIn(
                'index_id', $indexIds
            );
            foreach($faves->get() as $idx => $fave){
                $fave->delete();
            }
        }

        $isDraft = $portfolio->draft_of !== null;
        if (!$isDraft) {
          try {
            $this->backtesting->generatePortfoliosMetrics([Auth::user()->id], 'Event');
          } catch(Throwable $e) {
            Log::error($e);
          }
        }

        return true;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){
        $user = Auth::user();

        $mode = Input::get('mode');

        $period = Input::get('period', '6m');

        $portfolio = Portfolio::where('user_id', $user->id)
          ->where('id', $id);

        if ($portfolio->count() == 0) {
            $message = ['Cannot find this portfolio'];
            return Response::json($message, 404);
        }

        $portfolio = $portfolio->first();

        // if is a favourites draft
        if ($portfolio->draft_of === 0) {
            $portfolio->hash = 0;
        }

        $isDraft = $portfolio->draft_of !== null;

        if ($mode === 'saveOriginal') {
            $maxDate = $this->backtesting->getMaxDate($portfolio);
            $periods = $this->backtesting->getPeriods($portfolio, $maxDate);
            // check whether the draft compile before saving
            if ($portfolio->activeStrategiesCount > 0) {
              $this->backtesting->getPortfolio($portfolio, [
                'maxDate' => $maxDate,
                'periods' => $periods,
              ]);
            }
            // saving over the original with ID portfolio.draft_of
            $original = Portfolio::where('id', $portfolio->draft_of);
            if ($original->count() === 0) {
                $message = ['Cannot find this portfolio'];
                return Response::json($message, 404);
            }
            $original = $original->first();
            $original->saveWithDraft($portfolio);
            // generate user portfolios cache after saving
            try {
              $this->backtesting->generatePortfoliosMetrics([$user->id], 'Event');
            } catch(Throwable $e) {
              Log::error($e);
            }
            return [
              'maxDate' => $maxDate,
              'periods' => $periods,
              'portfolio' => $portfolio,
            ];
        }

        if ($mode === 'saveCopy') {
            $maxDate = $this->backtesting->getMaxDate($portfolio);
            $periods = $this->backtesting->getPeriods($portfolio, $maxDate);
            // check whether the draft compile before duplicating
            if ($portfolio->activeStrategiesCount > 0) {
              $this->backtesting->getPortfolio($portfolio, [
                'maxDate' => $maxDate,
                'periods' => $periods,
              ]);
            }
            // saving over the original with ID portfolio.draft_of
            $title = Input::get('title');
            $slug = Str::slug($title, '-');
            $newPortfolio = $portfolio->duplicate($title, $slug);
            $newPortfolio->draft_of = null;
            $newPortfolio->save();
            // generate user portfolios cache after saving
            try {
              $this->backtesting->generatePortfoliosMetrics([$user->id], 'Event');
            } catch(Throwable $e) {
              Log::error($e);
            }
            return [
              'maxDate' => $maxDate,
              'periods' => $periods,
              'portfolio' => $newPortfolio,
            ];
        }

        if ($mode === 'duplicate') {
            $title = Input::get('title');
            $slug = Str::slug($title, '-');
            $newPortfolio = $portfolio->duplicate($title, $slug);
            // generate user portfolios cache after saving
            try {
              $this->backtesting->generatePortfoliosMetrics([$user->id], 'Event');
            } catch(Throwable $e) {
              Log::error($e);
            }
            return [
              'portfolio' => $newPortfolio,
            ];
        }

        if ($mode === 'savepins') {
            $portfolio->pins = Input::get('pins');
            $portfolio->save();
            return [
              'portfolio' => $portfolio,
            ];
        }

        if ($mode === 'rename') {
            $portfolio->title = Input::get('title');
            $portfolio->slug = Str::slug($portfolio->title, '-');
            $portfolio->save();
            return [
              'portfolio' => $portfolio,
            ];
        }

        if ($mode === 'removeIndices') {
            $indices = Input::get('indices');
            $this->removeIndices($indices, $portfolio);
            if (!$isDraft) {
              try {
                $this->backtesting->generatePortfoliosMetrics([$user->id], 'Event');
              } catch(Throwable $e) {
                Log::error($e);
              }
            }
        }

        if ($mode === 'attachindices') {
            $rules = ['indices' => 'required'];
            $this->validate($request, $rules);

            // TODO check that this index isnt already attached to this portfolio
            $indices = Input::get('indices');
            if ($portfolio->strategiesCount + count($indices) > 20) {
              abort(500, 'Portfolio too full');
            }
            foreach ($indices as $idx => $index) {
                if (is_numeric($index)) {
                    $index = ['id' => $index];
                }

                $existingItem = PortfolioStrategy::where('user_id', $user->id)
                  ->where('portfolio_id', $portfolio->id)
                  ->where('indexinfo_id', $index['id']);
                if ($existingItem->count() === 0) {
                    $PortfolioStrategy = new PortfolioStrategy;
                    $PortfolioStrategy->indexinfo_id = $index['id'];
                    $PortfolioStrategy->portfolio_id = $portfolio->id;
                    $PortfolioStrategy->user_id = $user->id;
                    $PortfolioStrategy->weighting = 0;
                    $PortfolioStrategy->save();
                }
            }
            if (!$isDraft) {
              try {
                $this->backtesting->generatePortfoliosMetrics([$user->id], 'Event');
              } catch(Throwable $e) {
                Log::error($e);
              }
            }
        }

        if ($mode === 'changeweighting') {
            $rules = ['weighting' => 'required'];
            $this->validate($request, $rules);
            $portfolio->weighting = Input::get('weighting');
            $portfolio->save();
        }

        if ($mode === 'changeallocation') {
            $rules = ['allocation' => 'required'];
            $this->validate($request, $rules);
            $portfolio->saveAllocation(Input::get('allocation'));
        }

        if ($mode === 'customallocation') {
          $portfolio->weighting = Input::get('weighting');
          $portfolio->customRebalancing = json_encode(Input::get('allocation'));
          $portfolio->save();
        }

        if ($mode === 'batchSuspend' || $mode === 'batchUnsuspend') {

            $indicesPosted = Input::get('indices');
            $posted = [];
            foreach ($indicesPosted as $idx => $index) {
                $posted[$index['id']] = $index;
            }

            $loadedIndices = $portfolio->hasMany('App\PortfolioStrategy')->get();
            foreach ($loadedIndices as $idx => $index) {
                if (!empty($posted[$index->id])) {
                    $index->suspended = ($mode === 'batchUnsuspend') ? 0: 1;

                    $index->equal_weighting = $posted[$index->id]['equal_weighting'];

                    if (!empty($posted[$index->id]['locked'])) {
                        $index->weighting = $posted[$index->id]['weighting'];
                    }

                    $index->locked = $posted[$index->id]['locked'];
                    $index->save();
                    if (!empty($posted[$index->id]['deleted'])) {
                        $index->delete();
                    }

                }
            }
        }

        if ($mode === 'reallocateindices') {
            $rules = ['indices' => 'required'];
            $this->validate($request, $rules);

            $indicesPosted = Input::get('indices');
            $posted = [];
            foreach ($indicesPosted as $idx => $index) {
                $posted[$index['id']] = $index;
            }

            $loadedIndices = $portfolio->hasMany('App\PortfolioStrategy')->get();
            foreach ($loadedIndices as $idx => $index) {
                if (!empty($posted[$index->id])) {
                    $index->suspended = $posted[$index->id]['suspended'];
                    $index->equal_weighting = $posted[$index->id]['equal_weighting'];

                    if (!empty($posted[$index->id]['locked'])) {
                        $index->weighting = $posted[$index->id]['weighting'];
                    }

                    $index->locked = $posted[$index->id]['locked'];
                    $index->save();
                    if (!empty($posted[$index->id]['deleted'])) {
                        $index->delete();
                    }
                }
            }
        }

        if ($mode === 'removeIndices' || $mode === 'reallocateindices'
        || $mode === 'changeweighting' || $mode === 'attachindices'
        || $mode === 'batchSuspend' || $mode === 'batchUnsuspend' || $mode === 'changeallocation') {
            $maxDate = $this->backtesting->getMaxDate($portfolio);
            $periods = $this->backtesting->getPeriods($portfolio, $maxDate);
            $portfolio->hash = 'recompute';
            $portfolio->calculateWeightings('*');
            $customRebalancing = json_decode($portfolio->customRebalancing, true);
            $customAllocation = null;
            if (!empty($customRebalancing)) {
              $customAllocation = $customRebalancing['customAllocation'];
            }
            return [
              'maxDate' => $maxDate,
              'periods' => $periods,
              'portfolio' => $portfolio,
              'rebalancingList' => $customAllocation,
            ];
        }

        if ($mode === 'deletePortfolio') {
            $portfolio->delete();
        } else {
             $message = [
                'success' => true,
                'id' => $portfolio->id,
                'slug' => $portfolio->slug,
                'title' => $portfolio->title,
            ];
            return Response::json($message, 200);
        }
    }

    function randString($length, $charset='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789')
    {
        $str = '';
        $count = strlen($charset);
        while ($length--) {
            $str .= $charset[mt_rand(0, $count-1)];
        }
        return $str;
    }

    public function sharing(Request $request){
        $mode = Input::get('mode'); //bunch of share-related actions


        // the owner revokes a share [does not affect copies]
        if ($mode == 'delete'){
            $id = Input::get('id');
            $shares = Share::where(
                'sharing.id', '=', $id
            )->get();

            $res = [];
            foreach ($shares as $key => $share) {
                $res[] = $share->id;
                $share->delete();
            }
            return $res;
        }

        // the share recipient makes a copy of portfolio
        if ($mode == 'copy'){
            $id = Input::get('id');
            // maybe extra validation here: search for the Share:: where elementtype=portfolio, elementid=id and to_user is curent user
            $portfolio = Portfolio::where(
                'portfolios.id', '=', $id
            );
            if ( count($portfolio) ){
                $portfolio = $portfolio->first();
                $title = 'Copy of '.$portfolio->title;
                $slug = Str::slug($title, '-');
                $newPortfolio = $portfolio->duplicate($title,$slug);
                $newPortfolio->user_id = Auth::user()->id;
                $newPortfolio->description='shared copy of '.$portfolio['id']; // ?? how else to manage this
                $newPortfolio->save();
                return $newPortfolio;
            }
            return [];
        }

        // owner creates a share + shareurl
        if ( $mode == 'add'){
            $id = Input::get('id');
            $emails = explode(',', Input::get('email'));
            $userId = Auth::user()->id;

            $res = array('sent'=>0);
            // get the portfolio
            $portfolio = Portfolio::where(
                'portfolios.user_id', '=', $userId
            )->where(
                'portfolios.id', '=', $id
            );
            if ( count($portfolio) ){
                $portfolio = $portfolio->first();
                $baseurl = URL::to('/portfolios/'.$portfolio->slug).'?u='.$userId;
                $to_emails = [];
                $to_users = [];

                // get info on the users [if they are registered]
                foreach ($emails as $email) {
                    if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                        $to_emails[] = $email;
                        $to_user = User::where('users.email', '=', $email);
                        if ( $to_user->first()['id'] ){
                            $to_user = $to_user->first()['id'];
                            $to_users[] = $to_user;
                        }
                        else {
                            $to_user = null;
                        }
                        $sharecode = $this->randString(20);

                        // make the sharing table row
                        $row = new Share;
                        $row->user_id = $userId;
                        $row->to_email = $email;
                        $row->to_user = $to_user;
                        $row->share_code = $sharecode;
                        $row->element_type = 'portfolio';
                        $row->element_id = $portfolio['id'];
                        $row->save();

                        // get the share URL and send the email
                        $url = $baseurl.'&e='.$email.'&share='.$sharecode;
                        $message0 = Auth::user()->email . " has shared a engagingenterprises Portfolio with you: ".$portfolio->first()->title;
                        $message1= "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam sed libero finibus, tincidunt purus non, cursus nisl. Aenean eget ligula orci. Nullam in diam hendrerit nunc tincidunt hendrerit in non ante. Donec tempor lectus et metus tincidunt imperdiet.<br><br>";
                        $message2="<strong>Click or copy the link below to view this portfolio:</strong><br>";
                        $message3= "<a href='".$url."' target='_blank'>".$url."</a>";

                        Mail::send('emails.base', ['bodyMessage'=>"<h2><strong>".$message0."</h2></strong><br>".$message1.$message2.$message3], function($message) use ($email)
                        {
                            $message->from( 'admin@engagingenterprises.co' , 'engagingenterprises' );
                            $message->to($email)->subject( Auth::user()->email.' shared a Portfolio with you');
                        });
                        $res['sent']++;


                        // make the notification if a user
                        if ( $to_user!=null && Auth::user() ){
                            function notification($message){
                                $notification = app('Laravel\Spark\Contracts\Repositories\NotificationRepository');
                                return $notification->create(auth()->user(), $message);
                            }
                            notification([
                                'user_id'=>$to_user,
                                'created_by'=> Auth::user()->id,
                                'icon' => 'fa-file',
                                'body' => $message0,
                                'action_text' => 'View',
                                'action_url' => $url,
                            ]);
                        }


                    }
                }
                // die();
                // make the duplicates for each user in to_users??
            }
            return $res;
        }
    }

    public function getSharing(){
        $mode = Input::get('mode'); //empty = just get the info // 'all' get the shared object too

        $element_type = Input::get('element_type'); //incase we share other stuff
        $element_id = Input::get('element_id'); //getting shares per portfolio
        $to_user = Input::get('to_user'); //getting shares per user


        $shares = Share::where('sharing.element_type', '=', $element_type);

        if ($element_id){
            $shares->where('sharing.element_id', '=', $element_id);
        }
        if ($to_user){
            $shares->where('sharing.to_user', '=', $to_user);
        }

        $shares = $shares->get();

        if ($mode=='all'){
            // getting all details for portfolios shared w you - used on the Portfolios page
            // is there a faster way?
            foreach ($shares as $key => $share) {
                $portfolios = Portfolio::where('id', $share['element_id'])->get();
                if (count($portfolios)) {
                    $portfolio = $portfolios->first();
                    $portfolio['share'] = $shares[$key];
                    $shares[$key] = $portfolio;
                }
            }
        }

        return $shares;
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    protected function getCacheOption() {
      $cache = [];
      if (Input::get('ignoreCache') === 'true') {
        $cache['ignoreCache'] = true;
      }
      if (Input::get('reCache') === 'true') {
        $cache['reCache'] = true;
      }
      return $cache ?: null;
    }
}
