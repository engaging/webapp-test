<?php

namespace App\Http\Controllers\API;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Constant;
use App\Portfolio;
use App\Pca;
use App\Services\Lab\PCAService;
use App\Services\Right\RightService;
use Exception;
use Helper;

class PCAController extends Controller implements Constant
{
    public function __construct(PCAService $pca, RightService $right){
      $this->pca = $pca;
      $this->right = $right;
    }

    public function getPCA(Portfolio $portfolio) {
      $user = Auth::user();
      if (!$user->isAdmin() && !$user->subscriptions->contains(function($i, $o) {
        return in_array($o->name, ['Standard', 'Premium', 'Premium + Custom Model']);
      })) {
        abort(403, 'Restricted access');
      }
      if (!$this->right->hasPortfolioRight($user, $portfolio)) {
        abort(403, 'Restricted access');
      }

      $input = Input::all();
      return $this->pca->calcul($portfolio, $input);
    }

    public function getOptions(Portfolio $portfolio) {
      if (!$this->right->hasPortfolioRight(Auth::user(), $portfolio)) {
        abort(403, 'Restricted access');
      }
      $pca = $portfolio->pca;
      if (!$pca) {
        $pca = new Pca();
        $pca->options = json_encode(array(
          'returnInterval' => 'Daily',
          'covType' => 'Covariance',
          'listFilters' => [],
          'filter' => 'None',
        ));
        $pca->portfolio_id = $portfolio->id;
        $pca->save();
      }
      return [
        'options' => json_decode($pca->options),
      ];
    }

    public function updateOptions(Portfolio $portfolio) {
      $user = Auth::user();
      if (!$this->right->hasPortfolioRight($user, $portfolio)) {
        abort(403, 'Restricted access');
      }
      $pca = $portfolio->pca;
      if (!$pca) {
        $pca = new Pca();
      }
      $pca->portfolio_id = $portfolio->id;
      $pca->user_id = $user->id;
      $pca->options = json_encode(Input::all());
      $pca->save();
    }

    public function getDates(Portfolio $portfolio) {
      if (!$this->right->hasPortfolioRight(Auth::user(), $portfolio)) {
        abort(403, 'Restricted access');
      }
      $input = Input::all();
      $total = $this->pca->calcul($portfolio, $input);
      $dates = Helper::get($total, 'dates', []);
      return [
        'dates' => $dates,
        'datesCount' => count($dates),
      ];
    }

}
