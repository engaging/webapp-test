<?php

namespace App\Http\Controllers\API;

use App\Widget;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;

class DashboardController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
      $user = Auth::user();
      $widget = $user->widget;
      $count = 0;
      $properties = [];
      $max_y = 0;
      for ($i = 0; $i < count($request->data); $i++) {
        $property = $request->data[$i];
        if (isset( $property['properties']['showMenu'] )) {
          $property['properties']['showMenu'] = false;
        }
        if (isset($property['position'])){
          $y = (int)explode(",",$property['position'])[1];
          $max_y = $max_y > $y ? $max_y : $y;
        }
        $properties[] = $property;
      }
      foreach($properties as $property) {
        if (!isset($property['position'])) {
          $property['position'] = '0,'.$max_y+1;
          $max_y = $max_y+1;
        }
      }
      $widget->properties = json_encode($properties);
      $widget->save();
      return ['success' => true];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
        var_dump('test');die();
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
