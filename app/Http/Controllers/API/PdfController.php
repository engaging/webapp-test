<?php

namespace App\Http\Controllers\API;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Constant;
use Laravel\Spark\Spark;
use App\User;
use App\Indexinfo;
use App\Portfolio;
use App\Services\Lab\BacktestingService;
use App\Services\Right\RightService;
use App\Http\Requests;
use App\Http\Response;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Carbon\Carbon;
use Throwable;
use Exception;
use SnappyPDF;
use Helper;
use App;

class PdfController extends Controller implements Constant
{
  protected $backtesting;
  protected $right;

  public function __construct(BacktestingService $backtesting, RightService $right){
      $this->middleware('dev');
      $this->backtesting = $backtesting;
      $this->right = $right;
      $this->pdfBase = 'pdf/';
      $this->binaryPath = '/usr/local/bin/phantomjs';
      $this->phantomjsScript = base_path('resources/assets/js/bin/generate-pdf.js');
      $this->pdfPath = base_path('storage/app/' . $this->pdfBase);
      $this->publicPath = base_path('public');
  }

  public function generateStrategyPdf($id, Request $request) {
    // $contents = \View::make('my_view', ['name' => 'Rishabh'])->render();
    $type = 'strategy';
    try {
      $query = $this->checkQuery($type, $id, $request);
    } catch(Throwable $e) {
      return response($e->getMessage(), 400);
    }
    $apiToken = explode(' ', $request->header('Authorization'))[1];
    $data = $this->getStrategyNeedData($query);
    $filename = Carbon::now()->timestamp;
    $this->renderStrategyHtml($data, $query, $apiToken, $filename);
    $html = Storage::disk('local')->get($this->pdfBase . $filename . '2.html');
    $pdf = App::make('snappy.pdf.wrapper');
    $pdf->loadHTML($html)
      ->setOption('margin-top', 2)
      ->setOption('margin-bottom', 8)
      ->setOption('margin-left', 2)
      ->setOption('margin-right', 2)
      ->setOption('footer-font-size', 8)
      // ->setOption('disable-smart-shrinking', true)
      // ->setOption('zoom', 0.625)
      ->setOption('footer-left', 'engagingenterprises '.date("Y"))
      ->setOption('footer-right', 'Page [page] of [topage]')
      ->setOption('lowquality', false)
    ;
    Storage::disk('local')->delete([$this->pdfBase . $filename . '.html', $this->pdfBase . $filename . '2.html']);
    switch ($query['disposition']) {
      case 'inline':
        return $pdf->inline($type . '_' . $id . '.pdf');
      case 'attachment':
        return $pdf->download($type . '_' . $id . '.pdf');
    }
  }

  /**
   * Private functions
   */

  protected function checkQuery($type, $id, $request) {

    $main = null;
    $otherStrategies = collect([]);
    $otherPortfolios = collect([]);
    $volatilityDays = [];
    $chartOptions = [];
    $extraItems = [];
    $chartMaps = null;

    $disposition = $request->get('disposition', 'inline');
    if (!in_array($disposition, ['inline', 'attachment'])) {
      throw new Exception('disposition is invalid, should be inline or attachment');
    }

    $userId = $request->get('userId');
    $user = Spark::user()->where('id', $userId)->first();

    if ($type == 'strategy') {
      $searchKey = is_numeric($id) ? 'id' : 'code';
      $main = Indexinfo::where($searchKey, $id)->whereIn('type', self::STRATEGY_TYPES)->first();
    } else if ($type == 'portfolio') {
      $searchKey = is_numeric($id) ? 'id' : 'slug';
      $main = Portfolio::where('user_id', $user->id)->where($searchKey, $id)->first();
    } else {
      throw new Exception('no such api route');
    }

    if ($main == null) {
      throw new Exception('not find');
    }

    $strategies = $request->get('strategies');
    if (isset($strategies) && is_array($strategies) && count($strategies) > 0) {
      $searchKey = is_numeric($strategies[0]) ? 'id' : 'code';
      $otherStrategies = Indexinfo::whereIn($searchKey, $strategies)->where('type', 'Benchmark')->get();
      $extraItems = array_merge($extraItems, $otherStrategies->pluck('code')->toArray());
    }

    $portfolios = $request->get('portfolios');
    if (isset($portfolios) && is_array($portfolios) && count($portfolios) > 0) {
      $searchKey = is_numeric($portfolios[0]) ? 'id' : 'slug';
      $otherPortfolios = Portfolio::where('user_id', $user->id)->whereIn($searchKey, $portfolios)->get();
      $extraItems = array_merge($extraItems, $otherPortfolios->pluck('slug')->toArray());
    }

    if (count($extraItems) > 3) {
      throw new Exception('extra lenthgh should not larger than 3');
    }

    if ($type == 'portfolio') {
      $chartMaps = self::PORTFOLIO_CHARTS_MAP;
    } else {
      $chartMaps = self::STRATEGY_CHARTS_MAP;
    }

    $charts = $request->get('charts');
    if (!isset($charts) || count($charts) < 6) {
      throw new Exception('charts need 6 paramter');
    } else {
      foreach($charts as $idx => $chart) {
        if (!array_key_exists($chart['chart'], $chartMaps)) {
          throw new Exception('unknow chart type ----' . $chart);
        }
        $chartOption = [
          'chart' => $chart['chart'],
          'order' => $chartMaps[$chart['chart']],
          'options' => 'null',
        ];
        switch($chart['chart']){
          case "volatilitytrack":
            if (isset($chart['days'])) {
              if (!in_array($chart['days'], $volatilityDays)) {
                $volatilityDays[] = $chart['days'];
              }
              $chartOption['options'] = json_encode([
                'days' => $chart['days']
              ]);
            }
            break;
          case "correlationscatter":
            if (isset($chart['codes']) && is_array($chart['codes']) && count($chart['codes']) === 2) {
              $chartOption['options'] = json_encode([
                'codes' => $chart['codes']
              ]);
            }
            break;
          case "returnscatter":
            if (isset($chart['type']) && isset($chart['item'])) {
              if ($chart['type'] === 'strategy') {
                $scatterSearchKey = is_numeric($chart['item']) ? 'id' : 'code';
                $scatterDetail = Indexinfo::where($scatterSearchKey, $chart['item'])->whereIn('type', self::STRATEGY_TYPES)->first();
                $scatterId = $scatterDetail->id;
                $isIn = false;
                foreach ($otherStrategies as $otherStrategy) {
                  if ($otherStrategy->id == $scatterId) {
                    $isIn = true;
                  }
                }
                if (!$isIn) {
                  $otherStrategies->push($scatterDetail);
                }
                $chartOption['options'] = json_encode([
                  'type' => $chart['type'],
                  'id' => $scatterId
                ]);
              } else {
                $scatterSearchKey = is_numeric($chart['item']) ? 'id' : 'slug';
                $scatterDetail = Portfolio::where('user_id', $user->id)->where($scatterSearchKey, $chart['item'])->first();
                $scatterId = $scatterDetail->id;
                $isIn = false;
                foreach ($otherPortfolios as $otherPortfolio) {
                  if ($otherPortfolio->id == $scatterId) {
                    $isIn = true;
                  }
                }
                if (!$isIn) {
                  $otherPortfolios->push($scatterDetail);
                }
                $chartOption['options'] = json_encode([
                  'type' => $chart['type'],
                  'id' => $scatterId
                ]);
              }
            }
          default:
            break;
        }
        $chartOptions[] = $chartOption;
      }
    }

    $period = $request->get('period');
    if (!isset($period)) {
      $period = '5y';
    }

    $summary = $request->get('summary', []);
    $measure = $request->get('measure', []);

    return [
      'type' => $type,
      'main' => $main,
      'extraItems' => $extraItems,
      'otherStrategies' => $otherStrategies,
      'otherPortfolios' => $otherPortfolios,
      'volatilityDays' => $volatilityDays,
      'chartOptions' => $chartOptions,
      'summary' => $summary,
      'measure' => $measure,
      'period' => $period,
      'user' => $user,
      'disposition' => $disposition,
    ];
  }

  protected function renderStrategyHtml($data, $query, $apiToken, $filename) {
    $htmlInput = $this->pdfPath . $filename . '.html';
    $htmlOutput = $this->pdfPath . $filename . '2.html';
    $html = \View::make('strategy_info', [
      'monthlyReturns' => $data->monthlyReturns,
      'index' => $data->index,
      'maxDate' => $data->maxDate,
      'componentOptions' => [
        'summary' => $query['summary'],
        'measure' => $query['measure'],
        'extraItems' => $query['extraItems'],
        'publicPath' => $this->publicPath,
        'period' => $data->period,
        'otherIndices' => $data->otherIndices,
      ],
      'templateOptions' => [
        'chartOptions' => $query['chartOptions'],
        'user' => $query['user'],
        'apiToken' => $apiToken,
      ],
      'disclaimer' => $this->getDisclaimer(),
      'related' => Indexinfo::where([
              'shortName' => $data->index->shortName
          ])->where(
              'id','!=',$data->index->id
          )->where([
              'provider' => $data->index->provider
          ])->get()
    ])->render();
    $replaceString = ' src="file://' . $this->publicPath;
    $html = str_replace(' src="', $replaceString, $html);
    Storage::disk('local')->put($this->pdfBase . $filename . '.html', $html);
    $command = implode(' ', [
      $this->binaryPath,
      $this->phantomjsScript,
      $htmlInput,
      $htmlOutput,
    ]);
    shell_exec($command);
    // $process = new Process($command);
    // $process->setTimeout(15);
    // $process->run();
    // $finalHtml = Storage::disk('pdf')->get($filename . '2.html');
    // return $finalHtml;
  }

  protected function getStrategyNeedData($query) {
    $periodKeys = self::PERIOD_KEYS;

    $otherIndices = [];

    // get volatility track options
    $volatilityDays = $query['volatilityDays'];
    $volatilityOptions = '30d';
    if (count($volatilityDays) > 0) {
      $volatilityOptions = array_shift($volatilityDays);
    }

    // get main strategy data for all needed data exclude the returnscatter data.
    $index = $query['main'];
    $maxDate = $this->backtesting->getMaxDate($index);
    $periods = $this->backtesting->getPeriods($index, $maxDate);
    $index = $this->backtesting->getStrategy($index, [
      'maxDate' => $maxDate,
      'periods' => $periods,
      'volMeasure' => $volatilityOptions,
    ]);
    // choose right period
    if ($index[$query['period'] . '_active'] !== 1) {
      $periodKeysRnserve = array_reverse($periodKeys);
      foreach($periodKeysRnserve as $findPeriod) {
        if ($index[$findPeriod . '_active'] === 1) {
          $query['period'] = $findPeriod;
          break;
        }
      }
    }
    $this->getExtraVolatilityTrack($index, $maxDate, $periods, $query['period'], $volatilityDays);
    $index['description'] = str_replace("'", "",$index['description']); // temporary -- quot mark in template js has errors
    $index['returnCategory_'] = $index->returnCategory;
    $index['returnType_'] = $index->returnType;

    $monthlyReturns = [];
    foreach (array_reverse($periodKeys) as $period) {
      $key = $period . '_monthly_returns';
      if (!isset($index[$key])) continue;
      $monthlyReturns = Helper::obj2arr($index[$key]);
      if (!empty($monthlyReturns)) {
        $monthlyReturns = array_reverse($monthlyReturns, true);
        break;
      }
    }

    foreach($query['otherPortfolios'] as $idx => $portfolio) {
      try {
        $portfolioDetail = $this->backtesting->getPortfolio($portfolio, [
          'maxDate' => $maxDate,
          'periods' => $periods,
          'volMeasure' => $volatilityOptions,
        ]);
        $this->getExtraVolatilityTrack($portfolioDetail, $maxDate, $periods, $query['period'], $volatilityDays);
        $otherIndices[] = $portfolioDetail;
      } catch(Throwable $e) {
        abort(500, $e->getMessage());
      }
    }

    foreach($query['otherStrategies'] as $idx => $strategies) {
      try {
        $strategyDetail = $this->backtesting->getStrategy($strategies, [
          'maxDate' => $maxDate,
          'periods' => $periods,
          'volMeasure' => $volatilityOptions,
        ]);
        $this->getExtraVolatilityTrack($strategyDetail, $maxDate, $periods, $query['period'], $volatilityDays);
        $otherIndices[] = $strategyDetail;
      } catch(Throwable $e) {
        abort(500, $e->getMessage());
      }
    }

    $data = (object) [
      'index' => $index,
      'monthlyReturns' => $monthlyReturns,
      'maxDate' => $maxDate,
      'otherIndices' => $otherIndices,
      'period' => $query['period'],
     ];
    return $data;
  }

  protected function getExtraVolatilityTrack(&$indexOrPortfolio, $maxDate, $periods, $period, $measures) {
    if (count($measures) > 0) {
      foreach($measures as $measure) {
        $track = [];
        $this->backtesting->fillVolatilityTrack($indexOrPortfolio, $track, [
          'maxDate' => $maxDate,
          'periods' => $periods,
          'period' => $period,
          'measure' => $measure,
        ]);
        $indexOrPortfolio[$period . '_volatility_track_' . $measure] = $track;
      }
    }
  }

  protected function getDisclaimer(){
    $content = (object) [
      'title' => "PDF Disclaimer",
      'body' => "
        <h4>Disclaimer</h4>
        <p>
          This document is provided by engagingenterprises for registered users only and its access and use are subject to the Terms and Conditions of engagingenterprises.co. engagingenterprises is providing access to documents and information regarding financial Indices. engagingenterprises is not related, nor associated to any of these Indices. Please refer to the corresponding Index Sponsor or Index Calculation Agent for further information. The information contained herein are derived from proprietary models based upon well-recognized financial principles and from data that has been obtained from external sources believed reliable. However engagingenterprises and any external information provider, disclaim any responsibility for the accuracy of estimates and models used in the calculations, any errors or omissions in computing any performance and statistic information. engagingenterprises does not accept any liability whatsoever for any direct or consequential loss arising from any use of the information provided. Performance and statistic information is provided in this document solely for informational purposes and should not be interpreted as an indication of actual or future performance. This document does not constitute an offer, recommendation or solicitation for the purchase or sale of any particular financial instrument or any securities. engagingenterprises does not provide legal advice or tax advice or investment advice nor does engagingenterprises give any investment recommendation or financial analysis.
        </p>
      ",
    ];
    return $content->body;
  }
}
