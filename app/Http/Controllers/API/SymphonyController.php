<?php

namespace App\Http\Controllers\API;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use GuzzleHttp\Client as GuzzleHttpClient;
use App\Http\Controllers\Controller;
use App\UserSymphonyAccount;
use App\User;
use Throwable;
use Helper;
use Log;

class SymphonyController extends Controller
{

    public function getAccount() {
      return Auth::user()->symphonyAccount;
    }

    public function linkAccount() {
      try {
        $user = Auth::user();

        $validator = Validator::make(Input::all(), [
          'email' => 'required|email|unique:user_symphony_accounts,account_email,'
            . $user->id . ',user_id',
        ]);
        if ($validator->fails()) {
          return response($validator->errors(), 422);
        }

        $account = $user->symphonyAccount;
        if ($account && $account->approved) {
          return response(['email' => [
            'A Symphony account is already linked.',
          ]], 422);
        }

        try {
          $client = new GuzzleHttpClient();
          $response = $client->post(env('API_SYMPHONY').'/v1/user/authenticate', [
            'json' => ['email' => Input::get('email')],
          ]);
          $body = json_decode($response->getBody(), true);
        } catch (Throwable $e) {
          Log::error($e);
          return response(['email' => [
            'The email is not associated with any Symphony accounts.',
          ]], 422);
        }

        if ($account) {
          $account->account_id = $body['user']['id'];
          $account->account_email = $body['user']['emailAddress'];
          $account->approval_code = $body['activationCode'];
          $account->save();
        } else {
          $account = new UserSymphonyAccount([
            'account_id' => $body['user']['id'],
            'account_email' => $body['user']['emailAddress'],
            'approval_code' => $body['activationCode'],
          ]);
          $user->symphonyAccount()->save($account);
        }
        return $account;

      } catch (Throwable $e) {
        Log::error($e);
        return response(['email' => [
          'Something went wrong, please retry or contact support on admin@engagingenterprises.co.',
        ]], 422);
      }
    }

    public function verifyAccount() {
      try {
        $validator = Validator::make(Input::all(), [
          'code' => 'required',
        ]);
        if ($validator->fails()) {
          return response($validator->errors(), 422);
        }

        $account = Auth::user()->symphonyAccount;
        if ($account->approval_code !== Input::get('code')) {
          return response(['code' => [
            'The code is incorrect.',
          ]], 422);
        }

        $account->approved = true;
        $account->save();

      } catch (Throwable $e) {
        Log::error($e);
        return response(['code' => [
          'Something went wrong, please retry or contact support on admin@engagingenterprises.co.',
        ]], 422);
      }
    }

    public function unlinkAccount() {
      $account = Auth::user()->symphonyAccount;
      if ($account) {
        $account->delete();
      }
    }

}
