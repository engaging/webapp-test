<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Document;
use App\DocumentAccess;
use App\CommunityMember;
use App\Services\Right\RightService;
use App\Services\Storage\StorageService;
use DB;
use Log;
use Throwable;
use function Tarsana\Functional\{ startsWith };

class DocumentsController extends Controller
{

    protected $right;
    protected $storage;

    public function __construct(RightService $right, StorageService $storage){
        $this->right = $right;
        $this->storage = $storage;
    }

    public function getAccessQuery(CommunityMember $communityMember){

        $published = 1;
        if($communityMember->isMember(Auth::user())){
            $published = 0;
        }
        $query = Document::select(
            '*'
        );
        $query->where('community_member_id', '=', $communityMember->id);
        $query->where('published', '>=', $published);

        return $query;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(CommunityMember $communityMember)
    {
        $user = Auth::user();
        if (!$this->right->hasCommunityMemberRight($user, $communityMember)) {
          abort(403, 'Restricted access');
        }
        $documents = $this->getAccessQuery($communityMember)->get();
        foreach ($documents as $document) {
          if (!empty($document->link)) {
            try {
              $params = [
                'Key' => $document->link,
                'Bucket' => 'files.engagingenterprises.co',
              ];
              $document->link = $this->storage->getSignedUrl('getObject', $params, '+2 hours');
            } catch (Throwable $e) {
              Log::error($e);
            }
          }
        }
        return $documents;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(CommunityMember $communityMember)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CommunityMember $communityMember, Request $request)
    {
        $link = $request->input('link');
        $website = $request->input('website');
        $shareWith = $request->input('shareWith', ['all']);
        if (!is_array($shareWith)) {
          $shareWith = !empty($shareWith) ? [$shareWith] : ['all'];
        }
        if ($link || $website) {
            $fields = [
                'link' => '',
                'website' => '',
                'title' => '',
                'type' => '',
                'locked' => '',
                'description' => '',
            ];
            foreach($fields as  $key => $val){
                $fields[$key] = $request->input($key, $val);
            }

            $document = Document::create(array_merge([
              'team_id' => $communityMember->team_id,
              'community_member_id' => $communityMember->id,
              'published' => 1,
            ], $fields));

            if(in_array('all', $shareWith)){
                $shareWith = ['all'];
            }
            foreach ($shareWith as $group) {
              DocumentAccess::create([
                'document_id' => $document->id,
                'group' => $group,
              ])->save();
            }

            $document = Document::find($document->id);
            if (!empty($document->link)) {
              try {
                $params = [
                  'Key' => $document->link,
                  'Bucket' => 'files.engagingenterprises.co',
                ];
                $document->link = $this->storage->getSignedUrl('getObject', $params, '+2 hours');
              } catch (Throwable $e) {
                Log::error($e);
              }
            }
            return $document;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(CommunityMember $communityMember,$id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(CommunityMember $communityMember,$id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CommunityMember $communityMember, Request $request, $id)
    {
        $document = Document::find($id);

        $fields = [
            'website' => '',
            'title' => '',
            'type' => '',
            'locked' => '',
            'description' => '',
            'link' => ''
        ];
        foreach($fields as  $key => $v){
            $fields[$key] = $request->input($key, '');
        }

        if ($fields['link'] || $fields['website']) {
          if ($fields['link'] && startsWith('http', $fields['link'])) {
            unset($fields['link']);
          }
          $document->update($fields);

          $shareWith = $request->input('shareWith', ['all']);
          if (!is_array($shareWith)) {
            $shareWith = !empty($shareWith) ? [$shareWith] : ['all'];
          }

          $query = DocumentAccess::where('document_id', '=', $id)->get();
          foreach($query as $documentAccess){
              $documentAccess->delete();
          }

          if(in_array('all', $shareWith)){
              $shareWith = ['all'];
          }
          foreach ($shareWith as $group) {
              DocumentAccess::create([
                  'document_id' => $id,
                  'group' => $group,
              ])->save();
          }

          $document = Document::find($id);
          if (!empty($document->link)) {
            try {
              $params = [
                'Key' => $document->link,
                'Bucket' => 'files.engagingenterprises.co',
              ];
              $document->link = $this->storage->getSignedUrl('getObject', $params, '+2 hours');
            } catch (Throwable $e) {
              Log::error($e);
            }
          }
        } else {
          $query = DocumentAccess::where('document_id', '=', $id)->get();
          foreach($query as $documentAccess){
              $documentAccess->delete();
          }
          $document->delete();
          $document = null;
        }
        return $document;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(CommunityMember $communityMember, $id)
    {
        //
    }
}
