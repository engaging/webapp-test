<?php

namespace App\Http\Controllers\API;

use Illuminate\Support\Facades\Auth;
use App\User;
use App\Portfolio;
use App\PortfolioStrategy;
use App\Indexinfo;
use App\CommunityMember;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Constant;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Carbon\Carbon;
use DB;
use Session;
use App\Setting;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Cache;
use App\Services\Lab\BacktestingService;
use App\Services\Storage\StorageService;
use Exception;
use Log;
use Helper;
use DateTime;
use function Tarsana\Functional\{ __, curry, pipe, split, filter, sort, lt };

class IndicesController extends Controller implements Constant
{

    public $header = array();
    protected $backtesting;
    protected $storage;

    public function __construct(BacktestingService $backtesting, StorageService $storage){
        $this->backtesting = $backtesting;
        $this->storage = $storage;
    }

    public function editPrivateStrategy(Request $request){
        $success = false;
        $message = '';

        $user = Auth::user();
        $teamId = Helper::get($user->currentTeam, 'id');

        $strategy = Indexinfo::where('type', 'Private Strategy')
          ->where('owner', $teamId)
          ->where('id', $request->id)
          ->first();

        if($strategy){
            $update = [
                'shortName' => $request->input('shortName', $strategy->shortName),
                'longName' => $request->input('shortName', $strategy->shortName),
                'factor' => $request->input('factor', $strategy->factor),
                'assetClass' => $request->input('assetClass', $strategy->assetClass),
                'returnType' => $request->input('returnType', $strategy->returnType),
                'returnCategory' => $request->input('returnCategory', $strategy->returnCategory),
                'region' => $request->input('region', $strategy->region),
                'volTarget' => $request->input('volTarget', $strategy->volTarget),
            ];
            foreach($update as $key => $val){
                $strategy->$key = trim($val);
            }

            $maxDate = Setting::where('key', 'maxDate')->first()->value;

            if($file = $request->file('fileObject')){
                $success = false;
                $fileObject = $file->openFile();
                $contents = $fileObject->fread($fileObject->getSize());

                $originalRows = pipe(
                  'Helper::normalizeEol',
                  split("\n")
                )($contents);

                $isTrackRow = function($row) {
                  $values = explode(',', $row);
                  return count($values) > 1 && is_numeric($values[1]);
                };
                $rows = pipe(
                  filter($isTrackRow),
                  sort(lt())
                )($originalRows);

                if (count($rows) < 1) {
                  return ['success' => false, 'message' => 'Cannot upload file with empty track'];
                }

                $track = [];
                foreach($rows as $idx => $row){
                    $values = explode(',', $row);
                    $date = strtotime($values[0]);
                    if (!$date) {
                      $originalIdx = array_search($row, $originalRows);
                      return ['success' => false, 'message' => 'Cannot recognise date format YYYY-MM-DD on row ' . ($originalIdx+1)];
                    }
                    $value = floatval($values[1]);
                    if ($value <= 0) {
                      $originalIdx = array_search($row, $originalRows);
                      return ['success' => false, 'message' => 'Cannot upload zero or negative value on row ' . ($originalIdx+1)];
                    }
                    if (!$idx) {
                      $strategy->historyStartDate = strftime('%F', $date);
                    }
                    $track[] = [Helper::time2excel($date), $value];
                }

                // handle Weekend track start date
                [$firstItem, $secondItem] = $track;
                $firstDate = Helper::excel2time($firstItem[0]);
                $secondDate = Helper::excel2time($secondItem[0]);
                if (Helper::isWeekendTime($firstDate) || Helper::isWeekendTime($secondDate)) {
                    // if start date is Sat, but next day is Sun
                    if (Helper::isWeekendTime($firstDate) && Helper::isWeekendTime($secondDate)
                    && ($firstDate + self::SEC_IN_DAY === $secondDate)) {
                        $newStartDate = Helper:: shiftTimeToWeekday($secondDate);
                        $newItem = [Helper::time2excel($newStartDate), $secondItem[1]];
                    }
                    // if start date is Weekend day
                    else {
                        $newStartDate = Helper::shiftTimeToWeekday($firstDate);
                        $newItem = [Helper::time2excel($newStartDate), $firstItem[1]];
                    }
                    $strategy->historyStartDate = strftime('%F', $newStartDate);
                    $track = array_merge([$newItem], $track);
                }

                $strategy->lastDate = strftime('%F', $date);
                // private strategies never suspended
                // $strategy->is_suspended = (strtotime($maxDate) - $date) >= (self::SEC_IN_DAY * 35);

                $this->backtesting->uploadTrackData($strategy->code, $track);
            }

            $strategy->save();

            $this->backtesting->generatePrivateStrategiesMetrics($maxDate, [$teamId], 'Event');

            $success = true;
            return ['success' => $success, 'message' => $message, 'strategy' => $strategy];
        }

        return ['success' => $success, 'message' => $message];
    }

    public function uploadPrivateStrategy(Request $request){
        $success = false;
        $message = '';
        if($file = $request->file('fileObject')){
            $fileObject = $file->openFile();
            $contents = $fileObject->fread($fileObject->getSize());

            $originalRows = pipe(
              'Helper::normalizeEol',
              split("\n")
            )($contents);

            $isTrackRow = function($row) {
              $values = explode(',', $row);
              return count($values) > 1 && is_numeric($values[1]);
            };
            $rows = pipe(
              filter($isTrackRow),
              sort(lt())
            )($originalRows);

            if (count($rows) < 1) {
              return ['success' => false, 'message' => 'Cannot upload file with empty track'];
            }

            $track = [];
            $historyStartDate = '0000-00-00';
            foreach($rows as $idx => $row){
                $values = explode(',', $row);
                $date = strtotime($values[0]);
                if (!$date) {
                  $originalIdx = array_search($row, $originalRows);
                  return ['success' => false, 'message' => 'Cannot recognise date format YYYY-MM-DD on row ' . ($originalIdx+1)];
                }
                $value = floatval($values[1]);
                if ($value <= 0) {
                  $originalIdx = array_search($row, $originalRows);
                  return ['success' => false, 'message' => 'Cannot upload zero or negative value on row ' . ($originalIdx+1)];
                }
                if (!$idx) {
                  $historyStartDate = strftime('%F', $date);
                }
                $track[] = [Helper::time2excel($date), $value];
            }

            // handle Weekend track start date
            [$firstItem, $secondItem] = $track;
            $firstDate = Helper::excel2time($firstItem[0]);
            $secondDate = Helper::excel2time($secondItem[0]);
            if (Helper::isWeekendTime($firstDate) || Helper::isWeekendTime($secondDate)) {
                // if start date is Sat, but next day is Sun
                if (Helper::isWeekendTime($firstDate) && Helper::isWeekendTime($secondDate)
                && ($firstDate + self::SEC_IN_DAY === $secondDate)) {
                    $newStartDate = Helper:: shiftTimeToWeekday($secondDate);
                    $newItem = [Helper::time2excel($newStartDate), $secondItem[1]];
                }
                // if start date is Weekend day
                else {
                    $newStartDate = Helper::shiftTimeToWeekday($firstDate);
                    $newItem = [Helper::time2excel($newStartDate), $firstItem[1]];
                }
                $historyStartDate = strftime('%F', $newStartDate);
                $track = array_merge([$newItem], $track);
            }

            $maxDate = Setting::where('key', 'maxDate')->first()->value;
            $lastDate = strftime('%F', $date);
            // private strategies never suspended
            // $is_suspended = (strtotime($maxDate) - $date) >= (self::SEC_IN_DAY * 35);

            $user = Auth::user();
            $teamId = Helper::get($user->currentTeam, 'id');

            $code = md5(time() . '-' . $teamId . '-' . $request->input('shortName', ''));

            $this->backtesting->uploadTrackData($code, $track);

            $strategy = Indexinfo::create(array_map('trim', [
                'shortName' => $request->input('shortName', ''),
                'longName' => $request->input('shortName', ''),
                'factor' => $request->input('factor', ''),
                'assetClass' => $request->input('assetClass', ''),
                'code' => $code,
                'type' => 'Private Strategy',
                'owner' => $teamId,
                'returnType' => $request->input('returnType', ''),
                'returnCategory' => $request->input('returnCategory', ''),
                'historyStartDate' => $historyStartDate,
                'lastDate' => $lastDate,
                // 'is_suspended' => $is_suspended, // private strategies never suspended
            ]));

            $this->backtesting->generatePrivateStrategiesMetrics($maxDate, [$teamId], 'Event');

            $success = true;
            return ['success' => $success, 'message' => $message, 'strategy' => $strategy];
        }

        return ['success' => $success, 'message' => $message];
    }

    public function deletePrivateStrategy(Request $request){
      $code = $request->input('code');
      $this->backtesting->deleteTrackData($code);
    }

    public function getMarket(Request $request){
        $period = $request->input('period', '6m');

        $selections = [
            'factor as `factor`', 'assetClass as `asset`', 'COUNT(id) as `number_of_strategies`, GROUP_CONCAT(TRIM(code)) as `codes`'
        ];
        $selections[] = '"' . $period . '" as period';
        $selections = implode(' , ', $selections);

        $query = $this->getFiltered($request)
          ->selectRaw($selections)
          ->where('type', 'Strategy')
          ->groupBy(DB::raw('CONCAT(assetClass, factor)'));
        $markets = $query->get()->toArray();

        $avgFunc = function($values) {
          return round((array_sum($values) / count($values) / 100), 4);
        };
        return $this->backtesting->getStrategiesMetricsAggregates($markets, $period, $avgFunc);
    }

    public function getColors(Request $request)
    {
        $out = [
            'benchmarks' => []
        ];
        $numColors = 12;
        for($x = 1; $x <= $numColors; $x++){
            $key = 'color' . $x;
            $v = '';
            if(Setting::where('key', '=', $key)->count() > 0){
                $v = Setting::where('key', '=', $key)->first()->value;
            }
            $out['benchmarks'][$key] = $v;
        }

        $filters = array(
            'assetClass' => [],
            'factor' => [],
        );

        $out = array_merge($out, $filters);

        foreach($filters as $key => $emptyArray){

            $vals = DB::table('indexinfo')
                ->where(
                    'type', '=', 'Strategy'
                )
                ->select($key)->distinct($key)->get();
            $raw = array();
            foreach($vals as $idx => $val){
                $raw[] = trim($val->$key);
            }
            $filters[$key] = $raw;
            for($x = 0; $x < count($raw); $x++){
                $keyInner = $raw[$x];
                $v = '';
                if(Setting::where('key', '=', $keyInner)->count() > 0){
                    $v = Setting::where('key', '=', $keyInner)->first()->value;
                }
                if(empty($v)){
                    $v = $this->random_color();
                }
                $out[$key][$keyInner] = $v;
            }
        }

        if ($request->pca) {
          $max = intval($request->pca);
          for($x = 1; $x <= $max; $x++){
            $key = 'pc' . $x;
            $v = '';
            if(Setting::where('key', '=', $key)->count() > 0){
                $v = Setting::where('key', '=', $key)->first()->value;
            }
            $out['pcas'][$key] = $v;
          }
        }

        return $out;
    }

    public function random_color_part() {
        return str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT);
    }

    public function random_color() {
        return '#' . $this->random_color_part() . $this->random_color_part() . $this->random_color_part();
    }

    public function saveColors(Request $request)
    {
        $out = [];
        $numColors = 12;
        for($x = 1; $x <= $numColors; $x++){
            $key = 'color' . $x;
            if($request->has($key)){
                $value = $request->input($key);
                if(strlen($value) == 7){
                    if(Setting::where('key', '=', $key)->count() == 0){
                        $setting = new Setting;
                        $setting->key = $key;
                        $setting->value = $value;
                        $setting->save();
                    } else {
                        $setting = Setting::where('key', '=', $key)->first();
                        $setting->value = $value;
                        $setting->save();
                    }
                }

                $out[$key] = $value;
            }

        }

        $keys = array('factor', 'assetClass');
        foreach($keys as $key){
            if($request->has($key)){
                $out[$key] = [];
                $values = $request->input($key);
                foreach($values as $keyInner => $value){
                    if(strlen($value) == 7){
                        if(Setting::where('key', '=', $keyInner)->count() == 0){
                            $setting = new Setting;
                            $setting->key = $keyInner;
                            $setting->value = $value;
                            $setting->save();
                        } else {
                            $setting = Setting::where('key', '=', $keyInner)->first();
                            $setting->value = $value;
                            $setting->save();
                        }
                    }
                }
                $out[$key][$keyInner] = $value;
            }
        }


        return $out;
    }

    public function getSelectFields(){
        $selections = [
            'region',
            'currency',
            'shortName',
            'code',
            'factor',
            'type',
            'longName',
            'returnType',
            'returnCategory',
            'indexSource',
            'style',
            'provider',
            'assetClass',
            'volTarget',
            'id',
            'distribution',
            'owner',
            'is_suspended',
        ];

        $mapFields = Input::get('mapFields');
        if(!empty($mapFields) && is_array($mapFields)){
            foreach ($mapFields as $key => $value) {
                $pos = array_search($value, $selections);
                if($pos >= 0){
                    unset($selections[$pos]);
                    $selections[] = "`" . $value . "` AS `" . $key . '`';
                }

            }
        }

        $selections = implode(", ", $selections);
        return DB::raw($selections);
    }

    public function getFiltered(Request $request, $type = 'Strategy', $hideDeleted = true, $with = ''){
        $query = Indexinfo::where('id', '>', 0);
        if(!empty($with)){
            $query->with($with);
        }

        if(!$hideDeleted){
            $query = $query->withTrashed();
        }

        if ($type !== '') {
            $query = $query->where('type', $type);
        } else {
            $types = $request->input('type');
            if (empty($types)) {
              $types = self::STRATEGY_TYPES;
            }
            $query = $query->whereIn('type', $types);
        }

        $regions = $request->input('regions');
        if(!empty($regions)){
            $query = $query->whereIn('region', $regions);
        }

        $assetClasses = $request->input('assetClasses');
        if(!empty($assetClasses)){
            $query = $query->whereIn('assetClass', $assetClasses);
        }

        $currency = $request->input('currencies');
        if(!empty($currency)){
            $query = $query->whereIn('currency', $currency);
        }

        $factors = $request->input('factors');
        if(!empty($factors)){
            $query = $query->whereIn('factor', $factors);
        }

        $styles = $request->input('styles');
        if(!empty($styles)){
            $query = $query->whereIn('style', $styles);
        }

        $returnTypes = $request->input('returnTypes');
        if(!empty($returnTypes)){
            $query = $query->whereIn('returnType', $returnTypes);
        }

        $providers = $request->input('sponsors');
        if(!empty($providers)){
            $mapping = [];
            if(!empty($mapping)){
              $providers = array_map(function($s) use($mapping) {
                return isset($mapping[$s]) ? $mapping[$s] : $s;
              }, $providers);
            }
            $query = $query->whereIn('provider', $providers);
        }

        $history = $request->input('history');

        if(!empty($history)){
            if(!is_array($history)){
                $history = [$history];
            }
            foreach($history as $h){
                $wayBack = time();
                switch($h){
                    default:
                        $wayBack = strtotime("-" . $h, strtotime(Setting::where('key', '=', 'maxDate')->first()->value));
                    break;
                }

                $wayBack = strftime("%F", $wayBack);
                $query = $query->where('HistoryStartDate', '<=', $wayBack);
            }

        }

        $keywords = $request->input('keywords', $request->input('query', ''));
        if(!empty($keywords)){
            $keywords = "%" . $keywords . "%";
            $query = $query->where(
                        function ($queryInner) use ($keywords) {
                            $queryInner->orWhere('shortName', 'like', $keywords)
                                ->orWhere('longName', 'like', $keywords)
                                ->orWhere('code', 'like', $keywords);
                        }
                    );
        }

        $indices = $request->input('i');
        if(!empty($indices) && is_array($indices)){
            $query = $query->whereIn('id', $indices);
        }
        return $query;
    }

    public function getCutOffDate(){
      $maxDate = Setting::where('key', '=', 'maxDate')->first()->value;
      return ['maxDate' => $maxDate];
    }

}
