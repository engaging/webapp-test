<?php

namespace App\Http\Controllers\API;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Constant;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use App\User;
use App\Indexinfo;
use App\Services\Lab\BacktestingService;
use App\Services\Right\RightService;
use App\Services\Bot\BotFactory;
use App\Helpers\StrategyHelper;
use Helper;
use DB;

class StrategyController extends Controller implements Constant
{

    public function __construct(BacktestingService $backtesting, RightService $right){
      $this->backtesting = $backtesting;
      $this->right = $right;
    }

    public function getStrategies(){
      $user = Auth::user();
      $input = Input::all();

      $strategies = StrategyHelper::getBaseQuery($user, $input)->get();

      if (Input::get('full') !== 'true') {
        $strategies = $strategies->map(function($item, $key) {
          return collect($item)->except([
            'indexSource',
            'description',
            'disclaimer',
            'source',
          ]);
        });
      }

      $strategies = $strategies->toArray();
      $strategies = $this->right->filterAuthorizedStrategies($user, $strategies);

      if (Input::get('metrics') === 'true') {
        $strategies = $this->backtesting->getStrategiesMetrics($strategies, [
          'periodKeys' => Input::get('periodKeys', self::PERIOD_KEYS),
          'activeOnly' => Input::get('activeOnly') === 'true',
          'unauthorized' => true,
          'cache' => $this->getCacheOption(),
        ]);
      }

      return $strategies;
    }

    public function getStrategiesFilters(){
      $filterKeys = ['factor', 'assetClass', 'region', 'currency', 'style', 'type', 'returnType', 'provider'];

      Input::merge([
        'full' => 'true',
        'metrics' => 'false',
      ]);
      $strategies = collect($this->getStrategies());

      $filters = [
        'history' => [
          '6 Months',
          '1 Year',
          '3 Year',
          '5 Year',
          '10 Year',
        ],
      ];

      foreach ($filterKeys as $key) {
        $filters[$key] = $strategies->pluck($key)->unique()->filter()->values()->all();
      }
      return $filters;
    }

    public function getStrategyById(Indexinfo $strategy){
      if (!$this->right->hasStrategyRight(Auth::user(), $strategy)) {
        abort(403, 'Restricted access');
      }

      $strategy = $this->backtesting->getStrategy($strategy, [
        'maxDate' => Input::get('maxDate'),
        'periods' => Input::get('periods'),
        'volMeasure' => Input::get('volMeasure', '30d'),
        'cache' => $this->getCacheOption(),
      ]);

      return $strategy;
    }

    public function getPeriodsById(Indexinfo $strategy){
      if (!$this->right->hasStrategyRight(Auth::user(), $strategy)) {
        abort(403, 'Restricted access');
      }

      $maxDate = $this->backtesting->getMaxDate($strategy);
      $periods = $this->backtesting->getPeriods($strategy, $maxDate);

      return [
        'maxDate' => $maxDate,
        'periods' => $periods,
      ];
    }

    public function getVolatilityTrackById(Indexinfo $strategy){
      $track = [];

      $this->backtesting->fillVolatilityTrack($strategy, $track, [
        'maxDate' => Input::get('maxDate'),
        'periods' => Input::get('periods'),
        'period' => Input::get('period', '5y'),
        'measure' => Input::get('measure', '30d'),
      ]);

      return $track;
    }

    public function getBenchmarksCorrelationById(Indexinfo $strategy){
      $ids = array_unique(Input::get('ids', []));
      $ids_ordered = implode(',', $ids);

      $benchmarkCodes = Indexinfo::whereIn('id', $ids)
        ->orderBy(DB::raw("FIELD(id,$ids_ordered)")) // array should be ordered for benchmarks correlation
        ->pluck('code')->toArray();

      $correl = $this->backtesting->getCorrelationWithBenchmarks($strategy, $benchmarkCodes, [
        'maxDate' => Input::get('maxDate'),
        'periods' => Input::get('periods'),
        'period' => Input::get('period', '5y'),
      ]);

      return $correl['i'][$strategy->id];
    }

    public function getStrategiesCorrelation(){
      $ids = array_unique(Input::get('ids', []));

      $strategies = Indexinfo::whereIn('id', $ids)
        ->get()->all(); // array should not be ordered for strategies correlation

      return $this->backtesting->getCorrelationBetweenStrategies($strategies, [
        'maxDate' => Input::get('maxDate'),
        'periods' => Input::get('periods'),
        'period' => Input::get('period', '5y'),
      ]);
    }

    public function sendConstituents(Indexinfo $strategy){
      if (!$this->right->hasStrategyRight(Auth::user(), $strategy)) {
        abort(403, 'Restricted access');
      }

      $bot = BotFactory::make('symphony');
      $bot->sendConstituents($strategy->code);
    }

    protected function getCacheOption() {
      $cache = [];
      if (Input::get('ignoreCache') === 'true') {
        $cache['ignoreCache'] = true;
      }
      if (Input::get('reCache') === 'true') {
        $cache['reCache'] = true;
      }
      return $cache ?: null;
    }

}
