<?php

namespace App\Http\Controllers\API;

use App\Indexinfo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Services\Right\RightService;
use Helper;
use DB;

class IndicesFiltersController extends Controller
{

    protected $right;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(RightService $right){
        $this->right = $right;
    }

    /**
     * @deprecated
     * Get current filters.
     *
     * @return Response
     */
    public function get(Request $request)
    {
        $indices = Indexinfo::where('type', '=', 'Strategy')->get()->all();
        $indicesCount = count($indices);
        $indices = $this->right->filterAuthorizedStrategies(Auth::user(), $indices);
        $needStrategyFiltering = $indicesCount !== count($indices);

        //filters to get
        $filters = array(
            'region' => [],
            'currency' => [],
            'assetClass' => [],
            'factor' => [],
            'style' => [],
            'returnType' => [],
            'provider' => [],
        );

        foreach($filters as $key => $emptyArray){

            $query = Indexinfo::where(
                'type', '=', 'Strategy'
            );
            if ($needStrategyFiltering) {
              $query->whereIn('id', Helper::map($indices, 'id'));
            }
            $vals = $query->select($key)->distinct($key)->get();
            $raw = array();
            foreach($vals as $idx => $val){
                $raw[] = $val->$key;
            }
            $filters[$key] = $raw;
        }
        $filters['history'] = [
            '6 Months',
            '1 Year',
            '3 Year',
            '5 Year',
            '10 Year'
        ];
        $foundMulti = false;
        foreach($filters['factor'] as $idx => $item){
            if($item == 'Multi'){
                $foundMulti = true;
                unset($filters['factor'][$idx]);
                //kill it and add it to the end.
            }
        }

        if($foundMulti){
            $filters['factor'][] = 'Multi';
            $filters['factor'] = array_values($filters['factor']);
        }

        $filters['type'] = ['Strategy', 'Benchmark', 'Private Strategy'];

        return $filters;
        //return $request->user()->indices()->take(20)->get();
    }
}
