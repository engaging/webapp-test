<?php

namespace App\Http\Controllers\API;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Constant;
use App\Http\Requests;
use App\Indexinfo;
use App\FavouriteSet;
use App\FavouriteItem;
use App\Portfolio;
use App\Services\Lab\BacktestingService;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use DB;

class FavouritesController extends Controller implements Constant
{
    protected $backtesting;
    
    public function __construct(BacktestingService $backtesting){
      $this->backtesting = $backtesting;

      //make sure the user has a set first.
      $favouriteSet = FavouriteSet::where(
          'user_id', '=', Auth::user()->id
      );

      if($favouriteSet->count() == 0){
          $favouriteSet = new FavouriteSet;
          $favouriteSet->user_id = Auth::user()->id;
          $favouriteSet->save();
      }
    }

    public function getStrategies(){
      $ids = Auth::user()->favouriteIndicesIds();
      $strategies = Indexinfo::whereIn('id', $ids)->get()->toArray();

      if (Input::get('metrics') === 'true') {
        $strategies = $this->backtesting->getStrategiesMetrics($strategies, [
          'maxDate' => Input::get('maxDate'),
          'periods' => Input::get('periods'),
          'periodKeys' => self::PERIOD_KEYS,
          'unauthorized' => true,
        ]);
      }

      return $strategies;
    }

    public function clearFavourites(){
        $favourites = Auth::user()->favouriteIndices();
        foreach ($favourites->get() as $favourite) {
            $favourite->delete();
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $portfolioId = 0;
        $idsNotIn = array();
        $idsIn = array();

        $keywords = Input::get('keywords');
        if(!empty($keywords)){
            $idsIn = array(0);
            $keywords = "%" . $keywords . "%";
            $indices = Indexinfo::where(
                        function ($queryInner) use ($keywords) {
                            $queryInner->orWhere('shortName', 'like', $keywords)
                                ->orWhere('longName', 'like', $keywords)
                                ->orWhere('code', 'like', $keywords);
                        }
                    )->lists('id');

            foreach ($indices as $key => $value) {
                $idsIn[] = (int)$value;
            }

            //var_dump($idsIn);exit();

        }

        $portfolio = Input::get('portfolio');
        if(!empty($portfolio)){
            $portfolio = Portfolio::where(
                'id', '=', $portfolio
            );
            if($portfolio->count() == 1){
                $portfolio = $portfolio->first();
                $portfolioId = $portfolio->id;
                $alreadyIn = $portfolio->strategyIds;
                foreach ($alreadyIn as $key => $value) {
                    $idsNotIn[] = (int)$value;
                }
            }
        }


        // if($portfolioId == 0){
            // $favourites = Auth::user()->favouriteIndices();
        // } else {
            // $favourites = Auth::user()->favouriteIndices()->
                // whereNotIn('index_id', $idsNotIn);
        // }

        $favourites = Auth::user()->favouriteIndices();
        if(!empty($idsIn)){
            $favourites = $favourites->whereIn('index_id', $idsIn);
        }

        $res = $favourites->paginate(500);
        foreach ($res as $key => $value) {
            if ( in_array($value['index_id'], $idsNotIn) ){
                $value['initDisabled'] = 1;
            }
            else {
                $value['initDisabled'] = 0;

            }
        }

        return $res;
    }


    public function getSet()
    {
        return Auth::user()->favourites()->first();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }


    public function removeIndices($indices, $favouriteSet){
        $ids = [];
        foreach ($indices as $key => $index) {
           $ids[] = $index['id'];
        }

        $loadedIndices = $favouriteSet->favouriteItems()->whereIn('id', $ids)->get();

        // $loadedIndices = $favouriteSet->hasMany('App\FavouriteItem')->whereIn('id', $ids)->get();
        // why does the query use col name 'favourite_set_id' instead of 'favourite_sets_id'?

        foreach($loadedIndices as $idx => $index){
            $index->delete();
        }
        return true;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){

        $mode = Input::get('mode');

        $favouriteSet = FavouriteSet::where(
            'user_id', '=', Auth::user()->id
        );

        $favouriteSet = $favouriteSet->first();

        if($mode == "removeIndices"){
            $indices = Input::get('indices');
            $this->removeIndices($indices, $favouriteSet);
        }

        if($mode == "attachindices"){
            $rules = array(
                'indices'       => 'required',
            );
            $this->validate($request, $rules);
            $indices = Input::get('indices');
            foreach($indices as $idx => $index){
                //check it isn't already in
                $favouriteItem = FavouriteItem::where(
                    'user_id', '=', Auth::user()->id
                )->where(
                    'favourite_sets_id', '=', $favouriteSet->id
                )->where(
                    'index_id', '=', $index['id']
                );
                if($favouriteItem->count() == 0){
                    $favouriteItem = new FavouriteItem;
                    $favouriteItem->index_id = $index['id'];
                    $favouriteItem->favourite_sets_id = $favouriteSet->id;
                    $favouriteItem->user_id = Auth::user()->id;
                    $favouriteItem->save();
                }
            }
        }
        $favouriteSet->afterSave();
        return $favouriteSet;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
