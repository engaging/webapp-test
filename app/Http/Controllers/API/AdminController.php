<?php

namespace App\Http\Controllers\API;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Laravel\Spark\Spark;
use Laravel\Spark\Events\Auth\UserRegistered;
use Laravel\Spark\Contracts\Interactions\Auth\Register;
use Laravel\Spark\Contracts\Http\Requests\Auth\RegisterRequest;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Constant;
use App\Services\Lab\BacktestingService;
use App\Services\Right\RightService;
use App\CommunityMember;
use App\UserRight;
use App\Portfolio;
use App\Setting;
use App\User;
use App\Team;
use Exception;
use Throwable;
use Log;
use Helper;

class AdminController extends Controller implements Constant
{

    public function __construct(BacktestingService $backtesting, RightService $right){
        $this->middleware('dev');
        $this->backtesting = $backtesting;
        $this->right = $right;
        $this->protectedKeys = array_merge(
          ['api_secret_confirmation'],
          self::PROTECTED_KEYS,
          self::SPARK_FORM_KEYS
        );
    }

    public function feedRawData() {
      $this->backtesting->feedRawData();
    }

    public function updateCutOffDate() {
      $maxDate = Input::get('maxDate');

      // update the cut-off date
      $maxDate = $this->backtesting->updateCutOffDate($maxDate);

      return response()->json(['maxDate' => $maxDate]);
    }

    public function generateTrackData() {
      $maxDate = Input::get('maxDate');
      $datafeed = Input::get('datafeed') === 'true';

      // update the cut-off date
      $maxDate = $this->backtesting->updateCutOffDate($maxDate);

      // re generate strategies tracks
      $this->backtesting->generateTrackData($maxDate, $datafeed);

      return response()->json(['maxDate' => $maxDate]);
    }

    public function generateStrategiesMetrics() {
      // get max date setting
      $maxDate = Setting::where('key', 'maxDate')->first()->value;

      // re generate strategies metrics
      $this->backtesting->generateStrategiesMetrics($maxDate);
    }

    public function generatePortfoliosMetricsAndSendReports() {
      // get max date setting
      $maxDate = Setting::where('key', 'maxDate')->first()->value;

      // suspend strategies not updated and send reports
      $this->backtesting->suspendStrategiesNotUpdated($maxDate);

      // re generate private strategies metrics
      $teamIds = Team::where('id', '>', 0)->pluck('id')->toArray();
      $this->backtesting->generatePrivateStrategiesMetrics($maxDate, $teamIds, 'Event');

      // re generate portfolios metrics
      $userIds = User::where('approved', 1)->pluck('id')->toArray();
      $this->backtesting->generatePortfoliosMetrics($userIds, 'Event');
    }

    public function register(RegisterRequest $request){
      $user = Spark::interact(
        Register::class, [$request]
      );

      event(new UserRegistered($user));

      return $user;
    }

    public function sendApproval(User $user){
      $approvalFlow = env('APP_APPROVAL_FLOW', 'activate'); // ['activate', 'password']

      if ($user->approved) {
        throw new Exception('User already approved');
      }

      switch ($approvalFlow) {
        case 'activate':
          $user->sendActivateEmail();
          break;

        case 'password':
          $password = Helper::random_str();
          $this->right->setPasswordAndCreateInvestorRight($user, $password);
          $user->sendPasswordEmail($password);
          break;
      }
    }

    public function copyPortfolioToUser(User $user, Portfolio $portfolio){
      abort_unless($this->right->hasPortfolioRight(Auth::user(), $portfolio), 403, 'Restricted access');
      $title = $portfolio->title;
      $slug = Str::slug($title, '-');
      $copy = $portfolio->duplicate($title, $slug);
      $copy->user_id = $user->id;
      $copy->draft_of = null;
      $copy->save();
      // generate user portfolios cache after saving
      try {
        $this->backtesting->generatePortfoliosMetrics([$user->id], 'Event');
      } catch(Throwable $e) {
        Log::error($e);
      }
    }

    public function createInvestorRight(User $user) {
      try {
        $this->right->createInvestorRight($user);
      } catch (Throwable $e) {
        if ($e->getMessage() === $this->right::RIGHT_ALREADY_CREATED) {
          abort(409, $e->getMessage());
        }
        throw $e;
      }
    }

    public function updateUserRight(User $user) {
      $userRight = UserRight::where('user_id', $user->id)->first();
      if (!$userRight) {
        abort(404, 'UserRight not found');
      }

      $input = Input::all();
      $validator = Validator::make($input, [
        'api_key' => 'size:40|unique:user_rights,api_key,'
          . $user->id . ',user_id',
        'api_secret' => 'password',
        'api_secret_confirmation' => 'required_with:api_secret|same:api_secret',
      ], [
        'api_secret.password' => "
          The api secret must be at least <strong>8 characters</strong> in length
          <strong>and</strong>
          must contain at least <strong>3 of the following 4</strong> types of characters:
          <br> - lower case letters (i.e. a-z)
          <br> - upper case letters (i.e. A-Z)
          <br> - numbers (i.e. 0-9)
          <br> - special characters (e.g. -=[]\;,./~!@#$%^&*()_+{}|:<>?)
        ",
      ]);
      if ($validator->fails()) {
        return response($validator->errors(), 422);
      }

      foreach ($input as $key => $value) {
        if (!in_array($key, $this->protectedKeys)) {
          switch ($key) {
            case 'api_secret':
              if ($value !== '') {
                $userRight->$key = $value ? bcrypt($value) : null;
              }
              break;

            default:
              $userRight->$key = $value;
              break;
          }
        }
      }
      $userRight->save();

      return $userRight;
    }

    public function getUserRights() {
      return UserRight::with('user')->get();
    }

    public function getTeams(){
      return Team::where('name', 'not like', '%@%')
        ->with('users', 'communityMember')
        ->get();
    }

    public function createTeam(){
      $validator = Validator::make(Input::all(), [
        'name' => 'required|unique:teams,name'
      ]);
      if ($validator->fails()) {
        return response($validator->errors(), 422);
      }
      $name = Input::get('name');
      $team = new Team(['name' => $name]);
      $team->save();

      $type = Input::get('type');
      switch ($type) {
        case 'Asset Manager':
        case 'Index Sponsor':
          $team->communityMember()->save(new CommunityMember([
            'name' => $name,
            'type' => $type,
          ]));
          break;
      }
    }

    public function deleteTeam(Team $team){
      abort_unless(!$team->users()->count(), 403, 'Cannot delete team with members');
      $team->communityMember()->delete();
      $team->delete();
    }

    public function addTeamUser(Team $team, User $user){
      $team->users()->save($user);

      if ($oldTeam = Team::where('name', $user->email)->first()) {
        // update strategies owner
        DB::table('indexinfo')->where('owner', $oldTeam->id)->update([
          'owner' => $team->id,
        ]);

        // delete old team
        $oldTeam->delete();
      }

      // if is community member then create sponsor right
      if ($community = $team->communityMember) {
        $this->right->createSponsorRight($user, $community, true);
      }
    }

    public function deleteTeamUser(Team $team, User $user){
      $team->users()->detach($user);

      // create new team
      $newTeam = new Team(['name' => $user->email]);
      $newTeam->save();
      $newTeam->users()->save($user);

      // if was community member then create investor right
      if ($community = $team->communityMember) {
        $this->right->createInvestorRight($user, true);
      }
    }

}
