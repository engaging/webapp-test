<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Laravel\Spark\Spark;
use Laravel\Spark\Subscription;
use Laravel\Spark\Http\Controllers\Controller;
use Laravel\Spark\Contracts\Interactions\Subscribe;
use Laravel\Spark\Events\Subscription\SubscriptionUpdated;
use Laravel\Spark\Events\Subscription\SubscriptionCancelled;
use Laravel\Spark\Http\Requests\Settings\Subscription\UpdateSubscriptionRequest;
use Laravel\Spark\Contracts\Http\Requests\Settings\Subscription\CreateSubscriptionRequest;
use App\User;

class PlanController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('dev');
    }

    /**
     * Create the subscription for the user.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(User $user, Request $request)
    {
        abort_unless($plan = Spark::plans()->where('id', $request->plan)->first(), 404);

        $user->subscriptions()->save(new Subscription([
          'name' => $plan->name,
          'stripe_id' => '',
          'stripe_plan' => $plan->id,
          'quantity' => 1,
        ]));

        return $user->subscriptions()->get();
    }

    /**
     * Update the subscription for the user.
     *
     * @param  Request  $request
     * @return Response
     */
    public function update(User $user, Request $request)
    {
        abort_unless($plan = Spark::plans()->where('id', $request->plan)->first(), 404);

        $user->subscriptions()->update([
          'name' => $plan->name,
          'stripe_plan' => $plan->id,
        ]);

        return $user->subscriptions()->get();
    }

    /**
     * Cancel the user's subscription.
     *
     * @param  Request  $request
     * @return Response
     */
    public function destroy(User $user, Request $request)
    {
        $user->subscriptions()->delete();

        return $user->subscriptions()->get();
    }
}
