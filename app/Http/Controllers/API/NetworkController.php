<?php

namespace App\Http\Controllers\API;

use App\User;
use App\Http\Requests;
use App\CommunityMember;
use App\FeedArticle;
use App\FeedArticleImage;
use App\FeedArticleTag;
use App\ShareGroup;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use App\Connection;
use App\ConnectionRequest;
use App\Services\Storage\StorageService;
use Exception;
use XmlParser;
use Log;

class NetworkController extends Controller {

    protected $storage;

    public function __construct(StorageService $storage){
        $this->storage = $storage;
    }

    public function checkAccessOrFail(){
        return true;
    }

    public function getFeedQuery(){
        $query = FeedArticle::where('published', '=', 1);

        //$tags = request()->input('tags', []);
        $type = request()->input('type', 'All');
        $keywords = request()->input('keywords', '');
        $provider = request()->input('provider', '');

        if(!empty($provider)){
            $query->where('community_member_id', '=', $provider);
        }

        if(!empty($type)){
            switch($type){
                case 'Updates':
                case "News":
                    $query->where('type', '=', $type);
                break;
            }
        }

        if(!empty($keywords)){
            $keywords = "%" . $keywords . "%";
            $query->where('description', 'LIKE', $keywords);
            $query->orWhere('title', 'LIKE', $keywords);
        }

        $query->orderBy('created_at', 'DESC');
        return $query;
    }

    public function addPostToFeed($id){
        $this->checkAccessOrFail();
        $article = FeedArticle::create(['title' => request()->input('title', '')]);
        $article->community_member_id = $id;
        $article->published = 1;
        $article->type = 'Updates';
        $article->user_id = Auth::user()->id;
        $article->description = request()->input('description', '');
        $article->title = request()->input('title', '');
        $article->full_article = request()->input('full_article', '');
        $article->save();
        $article->shareWithGroups(request()->input('shareWith', ['all']));

        $image = request()->input('image', '');
        if(!empty($image)){
            $attached = $article->attachImage($image);
        }
        return $this->getFeedByCommunityMember($id);
    }

    public function removePost($post){
        $this->checkAccessOrFail();
        $article = FeedArticle::findOrFail($post);
        $type = $article->type;
        if($type == 'Updates' && $article->provider){
            $id = $article->provider->id;
        }

        $article->delete();
        if($type == 'News' || empty($id)){
            return $this->getFeed();
        }

        return $this->getFeedByCommunityMember($id);
    }


    public function updatePost($post){
        $this->checkAccessOrFail();
        $article = FeedArticle::findOrFail($post);

        $article->description = request()->input('description', $article->description);
        $article->title = request()->input('title', $article->title);
        $article->source = request()->input('source', $article->source);
        $article->full_article = request()->input('full_article', $article->full_article);
        $article->shareWithGroups(request()->input('shareWith', $article->shareWith));

        $image = request()->input('image');
        if($image === $article->image){
            //do nothing.
        } else if($image == false){
            $article->removeImages();
        } else if(!empty($image)){
            $attached = $article->attachImage($image);
        }

        $article->save();

        if($article->type == 'Updates' && $article->provider){
            $id = $article->provider->id;
            return $this->getFeedByCommunityMember($id);
        } else {
            return $this->getFeed();
        }
    }


    public function getFeed(){
        $perPage = 50;

        $query = $this->getFeedQuery();
        return $query->paginate($perPage);
    }

    public function getFeedByCommunityMember($id){
        $perPage = 50;
        $query = $this->getFeedQuery();
        $query->where('community_member_id', '=', $id);

        return $query->paginate($perPage);
    }

    public function getCommunity(){
        $sponsors = CommunityMember::where('type', 'Index Sponsor')
          ->whereHas('strategies', function ($query) {
            $query->where('type', 'Strategy');
          })->get();

        $assetManagers = CommunityMember::where('type', 'Asset Manager')->get();

        return [
          'sponsors' => $sponsors,
          'assetManagers' => $assetManagers,
        ];
    }

    public function toggleConnectCommunityMember(CommunityMember $communityMember){
        $user = Auth::user();
        $user->requestConnectionToCommunityMember($communityMember);
        $communityMember = CommunityMember::find($communityMember->id);
        return $communityMember;
    }


    public function updateCommunityMember(CommunityMember $communityMember){
        $fieldsToUpdate = request()->input('fields', []);
        $user = Auth::user();
        if(!$user->canEditCommunityMember($communityMember)){
            return response('You cannot edit this page', 503);
        } else {
            foreach($fieldsToUpdate as $key => $value){
                switch($key){
                    case "name":
                    case "image":
                    case "website":
                    case "description":
                    case "bannerImage":
                    case "bannerImageOriginal":
                    case "disclaimer":
                    case "descriptionGraph":
                    case "disclaimerGraph":
                        $communityMember->$key = $value;
                        break;
                }

            }
            $communityMember->save();
        }
        return $communityMember;
    }

    public function getConnectionRequests(CommunityMember $communityMember){
        $response = [];
        if($communityMember->canEdit(Auth::user())){
            $response = [
                'pending' => $communityMember->connectionRequests('pending')->paginate(10),
                'accepted' => $communityMember->connectionRequests('accepted')->paginate(10),
                'rejected' => $communityMember->connectionRequests('rejected')->paginate(10),
            ];
        }
        return $response;
    }

    public function acceptConnectionRequest(CommunityMember $communityMember){
        if($communityMember->canEdit(Auth::user())){
            $connectionRequest = ConnectionRequest::findOrFail(request()->input('id', 0));
            $connectionRequest->status = 'accepted';
            $connectionRequest->save();
        }
        return ['success' => true];
    }

    public function rejectConnectionRequest(CommunityMember $communityMember){
        if($communityMember->canEdit(Auth::user())){
            $connectionRequest = ConnectionRequest::findOrFail(request()->input('id', 0));
            $connectionRequest->status = 'rejected';
            $connectionRequest->save();
        }
        return ['success' => true];
    }

}
