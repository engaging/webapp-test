<?php

namespace App\Http\Controllers\API;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use App\User;
use App\UserPreferences;

class PreferenceController extends Controller
{
  public function updatePreference(string $type) {
    $user = Auth::user();
    $input = Input::all();
    if (!$user->preference) {
      $preference = new UserPreferences([
        'user_id' => $user->id,
        $type => json_encode($input),
      ]);
      $user->preference()->save($preference);
    } else {
      $user->preference[$type] = json_encode($input);
      $user->preference->save();
    }
  }
}