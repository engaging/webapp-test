<?php

namespace App\Http\Controllers\API;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Constant;
use App\Indexinfo;
use App\Portfolio;
use App\User;
use App\Regression;
use App\Services\Lab\RegressionService;
use App\Services\Right\RightService;
use Exception;
use Helper;

class RegressionController extends Controller implements Constant
{
    public function __construct(RegressionService $regression, RightService $right){
      $this->regression = $regression;
      $this->right = $right;
    }

    public function getRegression(string $type, int $id) {
      $user = Auth::user();
      if (!$user->isAdmin() && !$user->subscriptions->contains(function($i, $o) {
        return in_array($o->name, ['Standard', 'Premium', 'Premium + Custom Model']);
      })) {
        abort(403, 'Restricted access');
      }
      switch ($type) {
        case 'strategy':
          $indexOrPortfolio = Indexinfo::findOrFail($id);
          if (!$this->right->hasStrategyRight($user, $indexOrPortfolio)) {
            abort(403, 'Restricted access');
          }
          break;
        case 'portfolio':
          $indexOrPortfolio = Portfolio::findOrFail($id);
          if (!$this->right->hasPortfolioRight($user, $indexOrPortfolio)) {
            abort(403, 'Restricted access');
          }
          break;
        default:
          abort(400, 'Type unknown');
      }
      
      $input = array_merge([
        'fullTracks' => true,
      ], Input::all());
      $output = $this->regression->calcul($indexOrPortfolio, $input);
      if ($output instanceof JsonResponse) {
        return $output;
      }
      unset($output['periods']);
      $listfintech = Input::get('listfintech');
      $listfintech[] = 'alpha';
      foreach($listfintech as $code) {
        if (!isset($output[$code])) {
          continue;
        }
        $allKeys = array_keys($output[$code]);
        $unsetKeys = [];
        foreach($allKeys as $key) {
          if (strpos($key, '_track') && !in_array($key, ['beta_track', 'cumulated_track'])) {
            $unsetKeys[] = $key;
          }
        }
        foreach($unsetKeys as $unsetKey) {
          unset($output[$code][$unsetKey]);
        }
      }
      return $output;
    }

    public function getHeatmap(string $type, int $id) {
      $user = Auth::user();
      switch ($type) {
        case 'strategy':
          $indexOrPortfolio = Indexinfo::findOrFail($id);
          if (!$this->right->hasStrategyRight($user, $indexOrPortfolio)) {
            abort(403, 'Restricted access');
          }
          break;
        case 'portfolio':
          $indexOrPortfolio = Portfolio::findOrFail($id);
          if (!$this->right->hasPortfolioRight($user, $indexOrPortfolio)) {
            abort(403, 'Restricted access');
          }
          break;
        default:
          abort(400, 'Type unknown');
      }

      $input = Input::all();
      return $this->regression->getHeatmap($indexOrPortfolio, $input);
    }


    public function getOptions(string $type, int $id) {
      $user = Auth::user();
      switch ($type) {
        case 'strategy':
          $field = 'indexinfo_id';
          break;
        case 'portfolio':
          $field = 'portfolio_id';
          break;
        default:
          abort(400, 'Type unknown');
      }
      $regression = Regression::where('user_id', $user->id)
        ->where($field, $id)
        ->first();
      if (!$regression) {
        $regression = new Regression();
        $regression->user_id = $user->id;
        $regression->options = json_encode(array(
          'movingWindow' => 'Monthly',
          'returnInterval' => 'Daily',
          'interval' => 250,
          'factorList' => [],
          'lassoFilters' => 0,
          'factorSuspendedList' => [],
        ), JSON_FORCE_OBJECT);
        $regression[$field] = $id;
        $regression->save();
      }
      return [
        'options' => json_decode($regression->options),
      ];
    }

    public function updateOptions(string $type, int $id) {
      $user = Auth::user();
      switch ($type) {
        case 'strategy':
          $strategy = Indexinfo::findOrFail($id);
          if (!$this->right->hasStrategyRight($user, $strategy)) {
            abort(403, 'Restricted access');
          }
          $field = 'indexinfo_id';
          break;
        case 'portfolio':
          $portfolio = Portfolio::findOrFail($id);
          if (!$this->right->hasPortfolioRight($user, $portfolio)) {
            abort(403, 'Restricted access');
          }
          $field = 'portfolio_id';
          break;
        default:
          abort(400, 'Type unknown');
      }
      $regression = Regression::where('user_id', $user->id)
        ->where($field, $id)
        ->first();
      if (!$regression) {
        $regression = new Regression();
      }
      $regression->indexinfo_id = null;
      $regression->portfolio_id = null;
      $regression->$field = $id;
      $regression->options = json_encode(Input::all(), JSON_FORCE_OBJECT);
      $user->regressions()->save($regression);
    }

}
