<?php

namespace App\Http\Controllers\Auth;

use GuzzleHttp\Client as GuzzleHttpClient;

use Laravel\Spark\Http\Controllers\Auth\RegisterController as SparkRegisterController;
use Laravel\Spark\Spark;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Spark\Events\Auth\UserRegistered;
use Laravel\Spark\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RedirectsUsers;
use Laravel\Spark\Contracts\Interactions\Auth\Register;
use Laravel\Spark\Contracts\Http\Requests\Auth\RegisterRequest;
use App\Services\Right\RightService;
use Validator;
use View;

class RegisterController extends SparkRegisterController
{

    protected $right;

    public function __construct(RightService $right)
    {
        $this->right = $right;
        View::share('userPortfolioCount', 0);
    }

    public function inactive(){
        $content = (object) [
          'body' => "",
          'inactiveUserText' => "
            <h2>Your account is currently inactive.</h2>
            <p>
              Please contact us on <a href=\"mailto:admin@engagingenterprises.co\">admin@engagingenterprises.co</a> for further information.
            </p>
          ",
          'banner' => "/img/design/home_banner.jpg",
          'mobileBanner' => "/img/design/home_banner_short_m.jpg",
        ];
        return view('blocked', ['content' => $content]);
    }

	public function show(Request $request)
    {
        if (Spark::promotion() && ! $request->has('coupon')) {
            // If the application is running a site-wide promotion, we will redirect the user
            // to a register URL that contains the promotional coupon ID, which will force
            // all new registrations to use this coupon when creating the subscriptions.
            return redirect($request->fullUrlWithQuery([
                'coupon' => Spark::promotion()
            ]));
        }

        $content = (object) [
          'body' => "
            <h4>User Registration</h4>
            <p>
              Please complete this registration form to create your account and request an access to our platform.
            </p>
          ",
          'banner' => "/img/design/home_banner.jpg",
          'mobileBanner' => "/img/design/home_banner_short_m.jpg",
        ];

        return view('spark::auth.register')->with([
            'content'=> $content
        ]);;
    }

    public function register(RegisterRequest $request)
    {
        $user = Spark::interact(
            Register::class, [$request]
        );

        event(new UserRegistered($user));

        return redirect('/pending');

    }

    public function approved(Request $request, $approval_token = null)
    {
        if(empty($approval_token)){

        }
        $user = Spark::user()
            ->where([
                'approval_token' => $approval_token,
            ])->first();

        $view = ($user->approved == 1) ? 'already_approved': 'approved';

        return view($view)->with([
          'email' => $user->email,
          'token' => $approval_token,
        ]);
    }

    public function setPassword(Request $request)
    {
      $validator = Validator::make($request->all(), [
        'password' => 'required|password',
        'password_confirmation' => 'required|same:password',
      ], [
        'password.password' => "
          The password must be at least <strong>8 characters</strong> in length
          <strong>and</strong>
          must contain at least <strong>3 of the following 4</strong> types of characters:
          <br> - lower case letters (i.e. a-z)
          <br> - upper case letters (i.e. A-Z)
          <br> - numbers (i.e. 0-9)
          <br> - special characters (e.g. -=[]\;,./~!@#$%^&*()_+{}|:<>?)
        ",
      ]);
      if ($validator->fails()) {
        return view('approved')->with([
          'email' => $request->email,
          'token' => $request->token,
          'errors' => $validator->errors(),
        ]);
      }

	    $user = Spark::user()
        ->where('email', $request->email)
        ->where('approval_token', $request->token)
        ->where('approved', 0)
        ->first();

      $this->right->setPasswordAndCreateInvestorRight($user, $request->password);

      Auth::login($user);

      return redirect('strategy');
    }
}
