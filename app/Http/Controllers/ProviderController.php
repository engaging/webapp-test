<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests;
use App\Setting;
use App\CommunityMember;
use App\Services\Right\RightService;
use Session;
use View;

class ProviderController extends Controller
{

    protected $right;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(RightService $right)
    {
        $this->middleware('auth');
        $this->right = $right;

        $maxDate = Setting::where('key', '=', 'maxDate')->first();
        if ($maxDate) {
          View::share('maxDate', $maxDate->value);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function show($id)
    public function show(CommunityMember $sponsor)
    {
        $user = Auth::user();
        if (!$this->right->hasCommunityMemberRight($user, $sponsor)) {
          Session::flash('danger', 'Sorry, you <strong>don\'t have access</strong> to this page. Please contact us on <a href="mailto:admin@engagingenterprises.co" class="alert-link">admin@engagingenterprises.co</a> for further information.');
          return Redirect::to('community');
        }

        $view = ($sponsor->type === 'Asset Manager')
          ? 'user_asset_manager'
          : 'user_provider';

        $sponsor->description = str_ireplace("&quot;", "\"", $sponsor->description);
        $sponsor->disclaimer = str_ireplace("&quot;", "\"", $sponsor->disclaimer);
        $sponsor->descriptionGraph = str_ireplace("&quot;", "\"", $sponsor->descriptionGraph);
        $sponsor->disclaimerGraph = str_ireplace("&quot;", "\"", $sponsor->disclaimerGraph);
        if ($sponsor->type === 'Asset Manager') {
          $sponsor->ownStrategyIds = str_ireplace("&quot;", "\"", $sponsor->getOwnStrategies($sponsor->name));
        }
        return view($view)->with([
          'sponsor'=> false,
          'provider' => $sponsor,
          'canedit' => (int)$sponsor->canEdit($user),
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
