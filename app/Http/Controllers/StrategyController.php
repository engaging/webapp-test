<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use App\Services\Lab\BacktestingService;
use App\Services\Right\RightService;
use App\Services\Bot\BotFactory;
use App\Services\Export\AdvancedExcelExportFactory;
use App\Indexinfo;
use App\Portfolio;
use App\Setting;
use App\UserPreferences;
use GuzzleHttp\Client as GuzzleHttpClient;
use GuzzleHttp\Exception\RequestException;
use App;
use View;
use Session;
use SnappyPDF;
use Helper;

class StrategyController extends Controller implements Constant
{

    protected $backtesting;
    protected $right;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(BacktestingService $backtesting, RightService $right)
    {
        $this->middleware('auth');
        $this->backtesting = $backtesting;
        $this->right = $right;

        $maxDate = Setting::where('key', '=', 'maxDate')->first();
        if ($maxDate) {
          View::share('maxDate', $maxDate->value);
        }

        $userPortfolioCount = 0;
        $loggedin = Auth::check();
        if($loggedin){
            $userPortfolioCount = Portfolio::where(
                'user_id', '=', Auth::user()->id
            )->where(
                'draft_of', '=', null
            )->count();

            View::share('userPortfolioCount', $userPortfolioCount);
        }

        if($loggedin && Auth::user()->blocked > 0){
            return Redirect::to('/inactive')->send();
        }
    }

    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function show()
    {
        $requestFilters = [];

        $types = request()->input('types', []);
        if(!empty($types)){
            $requestFilters['types'] = $types;
        }
        $assetClasses = request()->input('assetClasses', []);
        if(!empty($assetClasses)){
            $requestFilters['assetClasses'] = $assetClasses;
        }
        $factors = request()->input('factors', []);
        if(!empty($factors)){
            $requestFilters['factors'] = $factors;
        }
        $regions = request()->input('regions', []);
        if(!empty($regions)){
            $requestFilters['regions'] = $regions;
        }
        $currencies = request()->input('currencies', []);
        if(!empty($currencies)){
            $requestFilters['currencies'] = $currencies;
        }
        $styles = request()->input('styles', []);
        if(!empty($styles)){
            $requestFilters['styles'] = $styles;
        }
        $returnTypes = request()->input('returnTypes', []);
        if(!empty($returnTypes)){
            $requestFilters['returnTypes'] = $returnTypes;
        }
        $period = request()->input('period', '5y');
        if(!empty($period)){
            $requestFilters['period'] = $period;
        }

        $requestFilters = json_encode($requestFilters);
        return view('strategy')->with([
          'requestFilters' => $requestFilters,
        ]);
    }

    public function exportTrack($code){
      $user = Auth::user();
      if (!$user->isAdmin() && !$user->subscriptions->contains(function($i, $o) {
        return in_array($o->name, ['Premium', 'Premium + Custom Model']);
      })) {
        abort(403, 'Restricted access');
      }

      $strategy = Indexinfo::where('code', $code)
        ->whereIn('type', self::STRATEGY_TYPES)
        ->first();
      if (!$this->right->hasStrategyRight($user, $strategy)) {
        abort(403, 'Restricted access');
      }

      return AdvancedExcelExportFactory::make('default')->exportExcelStrategy($strategy);
    }

    public function exportRegression(Indexinfo $strategy){
      $user = Auth::user();
      if (!$this->right->hasStrategyRight($user, $strategy)) {
        abort(403, 'Restricted access');
      }
      if (!$user->isAdmin() && !$user->subscriptions->contains(function($i, $o) {
        return in_array($o->name, ['Premium', 'Premium + Custom Model']);
      })) {
        abort(403, 'Restricted access');
      }
      $options = Input::all();
      return AdvancedExcelExportFactory::make('default')->exportExcelRegressionStrategy($strategy, $options);
    }

    public function getDisclaimer(){
        $content = (object) [
          'title' => "PDF Disclaimer",
          'body' => "
            <h4>Disclaimer</h4>
            <p>
              This document is provided by engagingenterprises for registered users only and its access and use are subject to the Terms and Conditions of engagingenterprises.co. engagingenterprises is providing access to documents and information regarding financial Indices. engagingenterprises is not related, nor associated to any of these Indices. Please refer to the corresponding Index Sponsor or Index Calculation Agent for further information. The information contained herein are derived from proprietary models based upon well-recognized financial principles and from data that has been obtained from external sources believed reliable. However engagingenterprises and any external information provider, disclaim any responsibility for the accuracy of estimates and models used in the calculations, any errors or omissions in computing any performance and statistic information. engagingenterprises does not accept any liability whatsoever for any direct or consequential loss arising from any use of the information provided. Performance and statistic information is provided in this document solely for informational purposes and should not be interpreted as an indication of actual or future performance. This document does not constitute an offer, recommendation or solicitation for the purchase or sale of any particular financial instrument or any securities. engagingenterprises does not provide legal advice or tax advice or investment advice nor does engagingenterprises give any investment recommendation or financial analysis.
            </p>
          ",
        ];
        return $content->body;
    }

    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function showOne($codeOrId)
    {
        $periodKeys = self::PERIOD_KEYS;
        $column = is_numeric($codeOrId) ? 'id' : 'code';

        $index = Indexinfo::where($column, $codeOrId)
          ->whereIn('type', self::STRATEGY_TYPES)
          ->first();

        if (!$index) {
          Session::flash('danger', 'Sorry, the requested strategy is <strong>not found</strong> in our database. Please contact us on <a href="mailto:admin@engagingenterprises.co" class="alert-link">admin@engagingenterprises.co</a> for further information.');
          return Redirect::to('strategy');
        }

        if (!$this->right->hasStrategyRight(Auth::user(), $index)) {
          Session::flash('danger', 'Sorry, you <strong>don\'t have access</strong> to this page. Please contact us on <a href="mailto:admin@engagingenterprises.co" class="alert-link">admin@engagingenterprises.co</a> for further information.');
          return Redirect::to('strategy');
        }

        $maxDate = $this->backtesting->getMaxDate($index);
        $periods = $this->backtesting->getPeriods($index, $maxDate);
        $index = $this->backtesting->getStrategy($index, [
          'maxDate' => $maxDate,
          'periods' => $periods,
          'noMin' => false,
        ]);
        $indices = [];
        $index['description'] = str_replace("'", "",$index['description']); // temporary -- quot mark in template js has errors
        $index['returnCategory_'] = $index->returnCategory;
        $index['returnType_'] = $index->returnType;

        $monthlyReturns = Helper::obj2arr($index['min_monthly_returns']);
        if (!empty($monthlyReturns)){
          $monthlyReturns = array_reverse($monthlyReturns, true);
        }
        
        $componentOptions = [];
        if ($period = Input::get('period')) {
          $componentOptions['period'] = $period;
        }

        $related = Indexinfo::where('provider', $index->provider)
          ->where('shortName', $index->shortName)
          ->where('id', '<>', $index->id)
          ->get();

        return view('strategy_info')->with([
          'componentOptions' => $componentOptions,
          'monthlyReturns' => $monthlyReturns,
          'maxDate' => $maxDate,
          'periods' => $periods,
          'index' => $index,
          'disclaimer' => $this->getDisclaimer(),
          'related' => $related,
        ]);
    }

    public function pdfPost(Request $request, $codeOrSlug = null){
        $to = $request->input('to');

        // dompdf attempt
        // $pdf =  App::make('dompdf.wrapper');//cant handle SVG
        // $pdf->loadHTML($html);
        // return $pdf->stream();
        // snappy pdf
        // svg renders are img but blurry, can have page breaks inside, and no legend other than the benchmark list....

        $html = $request->html;
        //echo $html;die();
        $pdf = App::make('snappy.pdf.wrapper');
        // $footer = "ABC";
        $pdf->loadHTML($html)
            ->setOption('margin-top', 2)
            ->setOption('margin-bottom', 8)
            ->setOption('margin-left', 2)
            ->setOption('margin-right', 2)
            ->setOption('footer-font-size', 8)
            // ->setOption('disable-smart-shrinking', true)
            // ->setOption('zoom', 0.625)
            ->setOption('footer-left', 'engagingenterprises '.date("Y"))
            ->setOption('footer-right', 'Page [page] of [topage]')
            ->setOption('lowquality', false)
        ;

        if (!$to) { // download
          return $pdf->inline(); // change this to autodownload for production, the user's dom state when gneerating pdf is not publicly sharable
        }

        $user = Auth::user();
        $name = Helper::capitalize($user->firstname) ?: Helper::capitalize($user->name);
        $message = "Hi $name. Thank you for requesting a factsheet for ";
        $ressource = explode('/', $request->path())[0];
        switch ($ressource) {
          case 'strategy':
            $message .= "the strategy <b>$codeOrSlug</b>.";
            $filename = "$codeOrSlug.pdf";
            break;
          case 'portfolio':
            $message .= "the portfolio <b>$codeOrSlug</b>.";
            $filename = "$codeOrSlug.pdf";
            break;
          case 'portfolios':
            $message .= "<b>portfolios</b>.";
            $filename = "portfolios.pdf";
            break;
        }
        $message .= ' Your factsheet is attached below.';

        $bot = BotFactory::make($to);
        $bot->sendMessage($message, [
          'attachment' => [
            'key' => $filename,
            'file' => $pdf->output(),
          ],
        ]);
    }

}
