<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use GuzzleHttp\Exception\RequestException;
use App\Http\Requests;
use App\Services\Lab\BacktestingService;
use App\Widget;
use App\Indexinfo;
use App\Setting;
use Session;
use View;

class WidgetController extends Controller implements Constant
{

    protected $backtesting;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(BacktestingService $backtesting) {
        $this->middleware('auth');
        $this->backtesting = $backtesting;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function show($id)
    public function show()
    {
        $maxDate = Setting::where('key', '=', 'maxDate')->first();
        if ($maxDate) {
          View::share('maxDate', $maxDate->value);
        }

        $user = Auth::user();
        if (!$user) {
          return Redirect::to('/')->send();
        }
        if ($user->blocked) {
          return Redirect::to('/inactive')->send();
        }

        $widget = $user->widget;
        $widgetData = [];
        $widgetDataLayout=[];
        if (!$widget) {
          $widget = new Widget();
          $defaultProperties = [
            [
              "title" => "Market Monitor",
              "size" => 4,
              "period" => "1y",
              "componenttype" => "bubble",
              "showMenu" => false,
              "properties" => [
                "showColMenu" => false,
                "cols" => ['return','volatility','sharpe'],
                "period" => "6m",
                "sortBy"=> "",
                "sortDirection"=>"asc",
                "colorBy"=>"return",
                "groupBy"=>"asset"
              ],
              "position" => "0,0",
              "showTable" => false
            ],
            [
              'size' => 4,
              'componenttype' => 'default',
              "position" => "0,2",
            ],
          ];
          $widget->properties = json_encode($defaultProperties);
          $user->widget()->save($widget);
        }
        $widgets = json_decode($widget->properties);
        foreach($widgets as $item){
          $widgetData[] = $item;
          $widgetDataLayout[] = $item->position;
        }

        $params = [
          'widgetData' => json_encode($widgetData),
          'widgetLayoutInitial' => json_encode($widgetDataLayout),
        ];

        return view('widgets')->with($params);

    }

    public function reset(Request $request){
      // delete all user widgets
      Widget::where(
         'user_id', '=', Auth::user()->id
      )->delete();

      // redirect to dashboard with success message
      Session::flash('success', 'Your dashboard has been reset successfully.');
      return Redirect::to('dashboard');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
