<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use GuzzleHttp\Client as GuzzleHttpClient;
use GuzzleHttp\Exception\RequestException;
use  Illuminate\Http\RedirectResponse;
use Session;
use App\Services\Analytics\AnalyticsService;

class HomeController extends Controller
{

    public function __construct(AnalyticsService $analytics)
    {
        $this->analytics = $analytics;
    }
    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function show()
    {
        if (Auth::check()) {
          $user = Auth::user();
          $canLogin = env('APP_LOGIN', true);
          // if login is disabled and user is neither admin nor impersonated
          if ($canLogin !== true && !$user->isAdmin() && !Session::has('spark:impersonator')) {
              $emails = explode(',', $canLogin);
              // and if user email is not whitelisted
              if (!in_array($user->email, $emails)) {
                  Auth::logout(); // logout and redirect to home page
                  Session::flash('danger', 'Sorry, you tried to login to our <strong>test website</strong>. Please use our production website <a href="https://engagingenterprises.co" class="alert-link">engagingenterprises.co</a> instead.');
                  return Redirect::to('home');
              }
          }
          // if user is blocked and is neither admin nor impersonated
          if ($user->blocked > 0 && !$user->isAdmin() && !Session::has('spark:impersonator')) {
              Auth::logout(); // logout and redirect to home page
              Session::flash('danger', 'Sorry, your account is currently <strong>inactive</strong>. Please contact us on <a href="mailto:admin@engagingenterprises.co" class="alert-link">admin@engagingenterprises.co</a> for further information.');
              return Redirect::to('home');
          }
          return redirect()->action('StrategyController@show');
        }

        $content = (object) [
          'title' => "Homepage",
          'banner' => "/img/design/home-banner.jpg",
          'mobileBanner' => "/img/design/new-home-banner-m.jpg",
          'body' => "
            <h1>POWERFUL INSIGHT. BREATHTAKINGLY EASY.</h1>
            <h3><strong>THE RISK fintech MARKETPLACE</strong></h3>
          ",
          'trustedLogos' => [
	          // "/img/design/logos/google.png",
	          // "/img/design/logos/samsung.png",
	          // "/img/design/logos/hp.png",
	          // "/img/design/logos/accenture.png",
	          // "/img/design/logos/hsbc.png",
          ],
          'featuresBackground' => '/img/design/particles-bg.png',
          'featuresMatrix' => [
            (object) [
              'backgroundImage' => "/img/design/bg-1.png",
              'backgroundImageMobile' => "/img/design/bg-1-m.jpg",
              'image' => "/img/icons/icon-server.png",
              'title' => "Strategy Database",
              'body' => "Search through the engagingenterprises strategy universe of factor indices with our unique database from leading providers across styles and asset classes.",
            ],
            (object) [
              'backgroundImage' => "/img/design/bg-2.png",
              'backgroundImageMobile' => "/img/design/bg-2-m.jpg",
              'image' => "/img/icons/icon-chart.png",
              'title' => "Portfolio Construction",
              'body' => "Develop allocation solution using systematic strategies to match your objectives: portfolio completion, hedge fund replication or diversified risk fintech allocation.",
            ],
            (object) [
              'backgroundImage' => "/img/design/bg-3.png",
              'backgroundImageMobile' => "/img/design/bg-3-m.jpg",
              'image' => "/img/icons/icon-monitoring.png",
              'title' => "Monitoring",
              'body' => "Track portfolio and strategies performance by comparing them with selected benchmarks, and compute advanced risk metrics.",
            ],
            (object) [
              'backgroundImage' => "/img/design/bg-4.png",
              'backgroundImageMobile' => "/img/design/bg-4-m.jpg",
              'image' => "/img/icons/icon-slider.png",
              'title' => "Factor Analysis",
              'body' => "Decompose your portfolio into independent risk factors with the engagingenterprises Pure Factors ® to gain insights into your exposure and performance drivers.",
            ],
            (object) [
              'backgroundImage' => "/img/design/bg-5.png",
              'backgroundImageMobile' => "/img/design/bg-5-m.jpg",
              'image' => "/img/icons/icon-puzzle.png",
              'title' => "Risk Solution",
              'body' => "Automated aggregation of strategy constituents across providers for a look-trough analysis and risk metrics computation under various market shocks & scenarios.",
            ],
          ],
          'quoteList' => [
            // (object) [
            //   'logo' => "/img/design/logos/google.png",
            //   'person' => "John Doe",
            //   'title' => "Investment Manager",
            //   'quote' => "Search through the engagingenterprises strategy universe of over 2,000 factor indices with our unique database from leading providers across styles and asset classes.",
            // ],
            // (object) [
            //   'logo' => "/img/design/logos/google.png",
            //   'person' => "John Doe",
            //   'title' => "Investment Manager",
            //   'quote' => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin eu suscipit erat. Vivamus ultrices ex et purus tristique aliquet. Aliquam a rutrum erat. Nulla vulputate justo eget ex vehicula, vitae euismod lacus varius. Proin nec nisl quis erat faucibus.",
            // ],
            // (object) [
            //   'logo' => "/img/design/logos/google.png",
            //   'person' => "John Doe",
            //   'title' => "Investment Manager",
            //   'quote' => "The fast growing performance engine and unique source of diversification and returns.",
            // ],
          ],
          'floatImage' => "/img/design/marketing-lg-1-v2.png",
          'floatText' => "
            <h2>SYSTEMATIC TRADING STRATEGIES</h2>
            <h3>The fast growing performance engine</h3>
          ",
          'middleBody' => [
            (object) [
              'body' => "
                <p>
                  Transparent and cost efficient, systematic trading strategies have demonstrated their ability to provide a diversified and uncorrelated performance engine.
                </p>
                <p>
                  With very low correlation with traditional asset classes, the greatest opportunity in factor investing resides in a well-constructed portfolio of multiple strategies across risk factors.
                </p>
                <p>
                  engagingenterprises is the independent digital platform providing data and analytics on systematic trading strategies dedicated to institutional investors.
                </p>
              ",
            ],
          ],
          'lowerBodyImage' => "/img/design/banner3.jpg",
          'lowerBody' => "
            <h2>COMBINING FACTORS IN A PORTFOLIO</h2>
            <p>
              engagingenterprises provides you the platform with the necessary statistics and charts to objectively asses the numerous risk fintech strategies being deployed in the market. You can interactively construct your optimized multi factor portfolio that can balance risk/return trade-offs and minimize downside risk.
            </p>
          ",
          'lowerBodyList' => [
            (object) [
              'body' => "Analyse the risk metrics of the factor indices to shortlist the best strategies across asset classes and styles matching your specific needs"
            ],
            (object) [
              'body' => "Design your own portfolios of risk fintech strategies by choosing your preferred allocation of risk factors"
            ],
            (object) [
              'body' => "Visualize the multi strategy portfolios that you have created, monitor their performance and print their investment profile"
            ],
          ],
          'thirdBlockImage' => "/img/design/callout-bg.jpg",
          'thirdBlockImageMobile' => "/img/design/banner-risk-m.jpg",
          'thirdBlock' => "
            <h2>PORTFOLIO RISK FACTORS</h2>
            <h3>The drivers of portfolio performance and risk</h3>
            <p>
              Factor Investing has provided a new perspective into portfolio construction and asset allocation.
            </p>
            <p>
              Investors can objectively assess and evaluate the risk driving a multi-asset portfolio to better pilot its performance.
            </p>
            <p>
              engagingenterprises Pure Factors ® technology enables the independent measure of strategy factor intensity to better control portfolio diversification and reduce downside risk.
            </p>
          ",
          'thirdBlockLogo' => "/img/design/dj_factiva_logo.png",
          'thirdLowerBlockTitle' => "ACCESS TO DAILY CURATED NEWS",
          'thirdLowerBlockContent' => "
            <p>
              engagingenterprises provides you with access to an extensive list of curated, industry specific news on factor &amp; risk fintech investing.
            </p>
            <p>
              Join the community of leading index sponsors &amp; asset management firms.
            </p>
          ",
        ];

        return view('marketing')->with([
            'content'=> $content,
            'userPortfolioCount' => 0
        ]);
    }

    public function showProductSolution()
    {

        $content = (object) [
          'title' => "Product & Solution",
          'banner' => "/img/design/product_solution_banner.jpg",
          'mobileBanner' => "/img/design/product-banner-m.jpg",
          'body' => "
            <h1>PRODUCT & SOLUTION</h1>
          ",
          'productsMatrix' => [
            (object) [
              'image' => "/img/design/products-1.jpg",
              'title' => "Data & Platform",
              'subtitle' => "The Risk fintech Database",
              'body' => "
                <p>
                  engagingenterprises’s platform provides the essential statistics, graphical analysis, and visualization tools to objectively asses the numerous risk fintech strategies deployed in the market.
                </p>
                <ul>
                  <li>Visualize the dynamics of each factor per asset class and benchmark its performance.</li>
        				  <li>Analyse the risk metrics of your preferred factor indices across leading providers.</li>
        				  <li>Interactively construct and optimize a multi-factor portfolio balancing risk/return trade-offs and minimizing downside risk.</li>
                </ul>
        				<p>
                  Join the community of leading index sponsors & asset managers with engagingenterprises’s community page containing an extensive list of carefully curated, industry specific news.
                </p>
              ",
            ],
            (object) [
              'image' => "/img/design/products-2.jpg",
              'title' => "Factor Analytics",
              'subtitle' => "The engagingenterprises Pure Factors ® – Leading Market Risk Factor Analysis",
              'body' => "
                <p>
                  engagingenterprises Pure Factors ® technology allows investors to decompose an investment portfolio, fund or strategy into independent risk factors.
                </p>
                <p>
                  Built from our extensive strategy database, you can gain an unparalleled knowledge of your market risk exposure, across asset classes, investment styles and regions.
                </p>
              ",
            ],
            (object) [
              'image' => "/img/design/products-3.jpg",
              'title' => "Risk Solution",
              'subtitle' => "Streamlining access to systematic indices for superior risk monitoring",
              'body' => "
                <p>
                  The Principal Component Analysis measures the distribution of the risk of your portfolio and its diversification. Generate specific historical scenarios to further assess its robustness.
                <p>
                <p>
                  The Constituent Risk Analysis provides additional perspective into strategy risk exposure. Each constituent within a strategy is reported and the positions are aggregated across strategies to generate the most reliable risk metrics for the portfolio.
                </p>
              ",
            ],
          ],
          'demoCallout' => "
          	<h3>POWERFUL INSIGHT.<br>BREATHTAKINGLY EASY.</h3>
          ",
          'demoCalloutImage' => "/img/design/particles-blue-bg.png",
        ];

        return view('product_solution')->with([
            'content'=> $content,
            'userPortfolioCount' => 0
        ]);
    }
}
