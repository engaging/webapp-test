<?php

namespace App\Http\Controllers;

use App\User;
use Carbon\Carbon;
use Cmgmyr\Messenger\Models\Message;
use Cmgmyr\Messenger\Models\Participant;
use Cmgmyr\Messenger\Models\Thread;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Laravel\Spark\Notification;
use Laravel\Spark\Announcement;
use DB;

class InboxController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        $this->middleware('auth');
    }
  
    /**
     * Show all of the message threads to the user.
     *
     * @return mixed
     */
    public function index()
    {
        // All threads, ignore deleted/archived participants
        $threads = Thread::getAllLatest()->get();
        $notifications = Notification::where('user_id', '=', Auth::user()->id)->orderBy('created_at', 'desc')->get();


        // All threads that user is participating in
        // $threads = Thread::forUser(Auth::id())->latest('updated_at')->get();

        // All threads that user is participating in, with new messages
        // $threads = Thread::forUserWithNewMessages(Auth::id())->latest('updated_at')->get();

        return view('messenger.index', compact('threads', 'notifications'));
    }

    /**
     * Shows a message thread.
     *
     * @param $id
     * @return mixed
     */
    public function show($id)
    {
        try {
            $thread = Thread::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            Session::flash('error_message', 'The thread with ID: ' . $id . ' was not found.');

            return redirect('inbox');
        }

        // show current user in list if not a current participant
        // $users = User::whereNotIn('id', $thread->participantsUserIds())->get();

        // don't show the current user in list
        $userId = Auth::user()->id;
        $users = User::whereNotIn('id', $thread->participantsUserIds($userId))->get();

        $thread->markAsRead($userId);

        $threads = Thread::getAllLatest()->get();
        $notifications = Notification::where('user_id', '=', Auth::user()->id)->orderBy('created_at')->get();

        return view('messenger.show', compact('thread', 'users', 'threads', 'notifications'));
    }

    /**
     * Shows a message thread.
     *
     * @param $id
     * @return mixed
     */
    public function showNotification($uuid)
    {
        try {
            $thread = Notification::findOrFail($uuid);
        } catch (ModelNotFoundException $e) {
            Session::flash('error_message', 'The thread with ID: ' . $id . ' was not found.');

            return redirect('inbox');
        }

        // show current user in list if not a current participant
        // $users = User::whereNotIn('id', $thread->participantsUserIds())->get();

        // don't show the current user in list
        $userId = Auth::user()->id;
        if($userId != $thread->user_id){
            Session::flash('error_message', 'The thread with ID: ' . $id . ' was not found.');
            return redirect('inbox');
        }
        $users = User::whereIn('id', [$userId])->get();
        $thread->read = 1;
        $thread->save();

        if($thread->creator()->count() == 0){
            $creator = new User(['name' => "engagingenterprises"]);
        } else {
            $creator = $thread->creator()->first();
        }

        // $users = User::whereNotIn('id', $thread->participantsUserIds($userId))->get();

        // $thread->markAsRead($userId);
        $threads = Thread::getAllLatest()->get();
        $notifications = Notification::where('user_id', '=', Auth::user()->id)->orderBy('created_at')->get();
        return view('messenger.notification', compact('creator', 'thread', 'users', 'threads', 'notifications'));
    }

    /**
     * Creates a new message thread.
     *
     * @return mixed
     */
    public function create()
    {

        DB::enableQueryLog();

        if(empty($_GET['to'])){
            return redirect('inbox');
        }

        $participants = [$_GET['to'], Auth::user()->id];

        // $query = Thread::where(
        //     'id', '>', 0
        // )->join('messenger_participants', 'messenger_participants.thread_id', 'messenger_threads.id')
        $participants = [$_GET['to']];
        $query = Thread::whereHas('participants', function ($q) use ( $participants ) {
            $q->whereIn('user_id', $participants)
                ->select(DB::raw('DISTINCT(thread_id)'));
        });

        $participants = [Auth::user()->id];
        $query2 = Thread::whereHas('participants', function ($q) use ($participants) {
            $q->whereIn('user_id', $participants)
                ->select(DB::raw('DISTINCT(thread_id)'));
        });

        $threads = $query2->whereIn('id', $query->pluck('id')->toArray());


        if($threads->count() > 0){

            return redirect('inbox/message/' . $threads->first()->id);
        }

        $users = User::where('id', '!=', Auth::id())->get();
        $threads = Thread::getAllLatest()->get();
        $notifications = Notification::where('user_id', '=', Auth::user()->id)->orderBy('created_at')->get();
        if(!$recipient = User::find($_GET['to'])){
            return redirect('inbox');
        }

        if(!Auth::user()->canCommunicateWith($recipient)){
            return redirect('inbox');
        }


        return view('messenger.create', compact('users', 'threads', 'notifications', 'recipient', 'subject'));
    }

    /**
     * Stores a new message thread.
     *
     * @return mixed
     */
    public function store()
    {
        $input = Input::all();

        if(!$recipient = User::find($input['recipients'])){
            return redirect('inbox');
        }

        if(!Auth::user()->canCommunicateWith($recipient)){
            return redirect('inbox');
        }


        $subject = 'Message sent by ' . Auth::user()->name . " to " . $recipient->name;

        $thread = Thread::create(
            [
                'subject' => $subject,
            ]
        );

        // Message
        Message::create(
            [
                'thread_id' => $thread->id,
                'user_id'   => Auth::user()->id,
                'body'      => $input['message'],
            ]
        );

        // Sender
        Participant::create(
            [
                'thread_id' => $thread->id,
                'user_id'   => Auth::user()->id,
                'last_read' => new Carbon,
            ]
        );

        // Recipients
        if (Input::has('recipients')) {
            $thread->addParticipant($input['recipients']);
        }

        return redirect('inbox/message/' . $thread->id);
    }

    /**
     * Adds a new message to a current thread.
     *
     * @param $id
     * @return mixed
     */
    public function update($id)
    {
        try {
            $thread = Thread::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            Session::flash('error_message', 'The thread with ID: ' . $id . ' was not found.');

            return redirect('inbox');
        }

        $thread->activateAllParticipants();

        // Message
        Message::create(
            [
                'thread_id' => $thread->id,
                'user_id'   => Auth::id(),
                'body'      => Input::get('message'),
            ]
        );

        // Add replier as a participant
        $participant = Participant::firstOrCreate(
            [
                'thread_id' => $thread->id,
                'user_id'   => Auth::user()->id,
            ]
        );
        $participant->last_read = new Carbon;
        $participant->save();

        // Recipients
        if (Input::has('recipients')) {
            $thread->addParticipant(Input::get('recipients'));
        }

        return redirect('inbox/message/' . $id);
    }
}
