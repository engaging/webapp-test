<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use GuzzleHttp\Client as GuzzleHttpClient;
use GuzzleHttp\Exception\RequestException;
use App\Http\Requests;
use View;
use Mail;

class ContactController extends Controller
{
    public function __construct()
    {
        View::share('userPortfolioCount', 0);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $content = (object) [
          'body' => "
            <h4>EMAIL</h4>
            <p>
              admin@engagingenterprises.co
            </p>
            <h4>ADDRESS</h4>
            <p>
              <strong>Engaging</strong>
              <br>
              3908 Two Exchange Square, 8 Connaught Place, Central, Hong Kong
            </p>
          ",
          'banner' => "/img/design/home_banner.jpg",
          'mobileBanner' => "/img/design/home_banner_short_m.jpg",
        ];

        return view('contact')->with([
            'content'=> $content,
            'message' => ''
        ]);
    }

    public function send(Request $request)
    {
        // var_dump('aaa');die();

        $client = new GuzzleHttpClient();

        $data = Input::all();
        $rules = array (
            'name' => 'required',
            'email' => 'required|email',
            'message' => 'required|min:1'
        );
        $validator = Validator::make ($data, $rules);

        //Validate data
        if ($validator -> passes()) {
            Mail::send('emails.contact', [
                'name' => $data['name'],
                'email' => $data['email'],
                'bodyMessage' => $data['message'],
            ], function($message) use($data) {
                $message->to('admin@engagingenterprises.co', 'engagingenterprises')
                  ->subject('Contact Request from '.$data['name'])
                  ->replyTo($data['email'], $data['name']);
            });
            $message = 'Your message has been sent! Thank you for contacting us.';
        } else {
            $message = 'Error: Please fill in every field.';
        }

        $content = (object) [
          'body' => "
            <h4>EMAIL</h4>
            <p>
              admin@engagingenterprises.co
            </p>
            <h4>ADDRESS</h4>
            <p>
              <strong>Engaging</strong>
              <br>
              3908 Two Exchange Square, 8 Connaught Place, Central, Hong Kong
            </p>
          ",
          'banner' => "/img/design/home_banner.jpg",
          'mobileBanner' => "/img/design/home_banner_short_m.jpg",
        ];

        return view('contact')->with([
            'content'=> $content,
            'message'=> $message
        ]);

    }


    public function showPremium(){
        $content = (object) [
          'body' => "
            <h4>EMAIL</h4>
            <p>
              admin@engagingenterprises.co
            </p>
            <h4>ADDRESS</h4>
            <p>
              <strong>Engaging</strong>
              <br>
              3908 Two Exchange Square, 8 Connaught Place, Central, Hong Kong
            </p>
          ",
          'banner' => "/img/design/home_banner.jpg",
          'mobileBanner' => "/img/design/home_banner_short_m.jpg",
        ];

        return view('premium')->with([
            'content'=> $content
        ]);
    }

    public function showPremiumRequested(){
        $content = (object) [
          'body' => "
            <h4>EMAIL</h4>
            <p>
              admin@engagingenterprises.co
            </p>
            <h4>ADDRESS</h4>
            <p>
              <strong>Engaging</strong>
              <br>
              3908 Two Exchange Square, 8 Connaught Place, Central, Hong Kong
            </p>
          ",
          'banner' => "/img/design/home_banner.jpg",
          'mobileBanner' => "/img/design/home_banner_short_m.jpg",
        ];

        return view('premium_requested')->with([
            'content'=> $content
        ]);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
