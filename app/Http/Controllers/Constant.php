<?php

namespace App\Http\Controllers;

interface Constant {

  const SEC_IN_DAY = 86400; // seconds in a day
  const PERIOD_KEYS = ['6m', '1y', '3y', '5y', '10y'];
  const METRIC_KEYS = [
    'returnCum',
    'return',
    'volatility',
    'sharpe',
    'skewness',
    'kurtosis',
    'calmar',
    'omega',
    'sortino',
    'var',
    'mvar',
    'cvar',
    'avg_drawdown',
    'mdd',
    'worst20d',
    'posWeeks',
    'posMonths',
  ];
  const STRATEGY_TYPES = [
    'Strategy',
    'Benchmark',
    'Private Strategy',
    'Pure Factor',
  ];
  const PROTECTED_KEYS = [
    'id',
    'user_id',
  ];
  const SPARK_FORM_KEYS = [
    'busy',
    'errors',
    'successful',
  ];
  const STRATEGY_CHARTS_MAP = [
    'radar' => 0,
    'drawdowntrack' => 1,
    'volatilitytrack' => 2,
    'correlationscatter' => 3,
    'returnbar' => 4,
    'returnscatter' => 5,
  ];
  const PORTFOLIO_CHARTS_MAP = [
    'radar' => 0,
    'drawdowntrack' => 1,
    'volatilitytrack' => 2,
    'correlationscatter' => 3,
    'returnbar' => 4,
    'allocation' => 5,
    'returnscatter' => 6,
    'leverage' => 7,
    'varcontribution' => 8,
    'returncontribution' => 9,
    'volatilitycontribution' => 10,
  ];
}
