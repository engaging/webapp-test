<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\FeedArticle;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use App\FeedArticleImage;
use App\FeedArticleTag;
use App\Http\Requests;

class NetworkController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        $this->middleware('auth');
        $loggedin = Auth::check();
        if($loggedin && Auth::user()->blocked > 0){
          return Redirect::to('/inactive')->send();
        }

    }
  
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }




    public function showFeedArticle($post){

        $article = FeedArticle::where('slug', '=', $post)->first();
        $article->checkReadAccessOrFail();
        $canedit = $article->canEdit(Auth::user());
        return view('feed.feed_page')->with(['item' => $article, 'provider' => $article->provider, 'canedit' => $canedit]); // (int)$article->provider->canEdit(Auth::user())]);
    }


    public function show()
    {
        //
        return view('user_network')->with(['sponsor'=>false]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
