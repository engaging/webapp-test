<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use App\Services\Lab\BacktestingService;
use App\Services\Right\RightService;
use App\Services\Export\ExcelExportService;
use App\Services\Export\AdvancedExcelExportFactory;
use App\Indexinfo;
use App\Portfolio;
use App\PortfolioStrategy;
use App\Setting;
use App\Share;
use GuzzleHttp\Client as GuzzleHttpClient;
use GuzzleHttp\Exception\RequestException;
use DB;
use View;
use Helper;
use Session;

class PortfolioController extends Controller implements Constant
{

    protected $backtesting;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(BacktestingService $backtesting, RightService $right, ExcelExportService $excelExport)
    {
        $this->backtesting = $backtesting;
        $this->right = $right;
        $this->excelExport = $excelExport;

        // see if share parameters match;
        $sharing=false;
        if ( isset($_GET['share']) && isset($_GET['u']) && isset($_GET['e']) ){
            $shares = Share::where('sharing.share_code', '=', $_GET['share'])->where('sharing.user_id', '=', $_GET['u'])->where('sharing.to_email', '=', $_GET['e'])->get();
            if ( count($shares) ){
                $sharing = true;
            }
        }

        // Check if logged in/using sharing link
        $loggedin=false;
        if (!$sharing){
            $this->middleware('auth');
            $loggedin = Auth::check();
            if($loggedin && Auth::user()->blocked > 0 ){
                return Redirect::to('/inactive')->send();
            }
        }

        $maxDate = Setting::where('key', '=', 'maxDate')->first();
        if ($maxDate) {
          View::share('maxDate', $maxDate->value);
        }

        $userPortfolioCount = 0;
        if( $loggedin || $sharing ){
            if ($sharing){
                $userId = $_GET['u'];
            }
            else {
                $userId = Auth::user()->id;
            }
            $userPortfolioCount = Portfolio::where(
                'user_id', '=', $userId
            )->where(
                'draft_of', '=', null
            )->count();
            View::share('userPortfolioCount', $userPortfolioCount);

        }
    }

    public function getDisclaimer(){
        $content = (object) [
          'title' => "PDF Disclaimer",
          'body' => "
            <h4>Disclaimer</h4>
            <p>
              This document is provided by engagingenterprises for registered users only and its access and use are subject to the Terms and Conditions of engagingenterprises.co. engagingenterprises is providing access to documents and information regarding financial Indices. engagingenterprises is not related, nor associated to any of these Indices. Please refer to the corresponding Index Sponsor or Index Calculation Agent for further information. The information contained herein are derived from proprietary models based upon well-recognized financial principles and from data that has been obtained from external sources believed reliable. However engagingenterprises and any external information provider, disclaim any responsibility for the accuracy of estimates and models used in the calculations, any errors or omissions in computing any performance and statistic information. engagingenterprises does not accept any liability whatsoever for any direct or consequential loss arising from any use of the information provided. Performance and statistic information is provided in this document solely for informational purposes and should not be interpreted as an indication of actual or future performance. This document does not constitute an offer, recommendation or solicitation for the purchase or sale of any particular financial instrument or any securities. engagingenterprises does not provide legal advice or tax advice or investment advice nor does engagingenterprises give any investment recommendation or financial analysis.
            </p>
          ",
        ];
        return $content->body;
    }

    /**
     * Show the application dashboard.
     *
     * @return Response
     */

    public function show()
    {

        return view('portfolios')->with([
            'disclaimer' => $this->getDisclaimer()
        ]);

    }


    public function exportTrack($slug){
      $user = Auth::user();

      $portfolio = Portfolio::where('slug', $slug)->first();
      if (!$this->right->hasPortfolioRight($user, $portfolio)) {
        abort(403, 'Restricted access');
      }

      if ($user->isAdmin() || $user->subscriptions->contains(function($i, $o) {
        return in_array($o->name, ['Premium', 'Premium + Custom Model']);
      })) {
        return AdvancedExcelExportFactory::make('default')->exportExcelPortfolio($portfolio);
      }

      return $this->excelExport->exportExcelPortfolio($portfolio);
    }

    public function exportPCA(Portfolio $portfolio){
      $user = Auth::user();
      if (!$this->right->hasPortfolioRight($user, $portfolio)) {
        abort(403, 'Restricted access');
      }
      if (!$user->isAdmin() && !$user->subscriptions->contains(function($i, $o) {
        return in_array($o->name, ['Premium', 'Premium + Custom Model']);
      })) {
        abort(403, 'Restricted access');
      }
      $options = Input::all();
      return AdvancedExcelExportFactory::make('default')->exportExcelPCA($portfolio, $options);
    }

    public function exportRegression(Portfolio $portfolio){
      $user = Auth::user();
      if (!$this->right->hasPortfolioRight($user, $portfolio)) {
        abort(403, 'Restricted access');
      }
      if (!$user->isAdmin() && !$user->subscriptions->contains(function($i, $o) {
        return in_array($o->name, ['Premium', 'Premium + Custom Model']);
      })) {
        abort(403, 'Restricted access');
      }
      $options = Input::all();
      return AdvancedExcelExportFactory::make('default')->exportExcelRegressionPortfolio($portfolio, $options);
    }

    public function showOne($slugOrId)
    {
        $periodKeys = self::PERIOD_KEYS;
        $column = is_numeric($slugOrId) ? 'id' : 'slug';
        $user = Auth::user();
        $userId = $user ? $user->id : null; // set User to the current logged in user if any

        $sharing = false;
        if (isset($_GET['share']) && isset($_GET['u']) && isset($_GET['e'])) {
            $shares = Share::where('sharing.share_code', $_GET['share'])
              ->where('sharing.user_id', $_GET['u'])
              ->where('sharing.to_email', $_GET['e'])
              ->get();
            if (count($shares)) {
                $sharing = true;
            }
        }

        if ($sharing) {
            $userId = $_GET['u']; // if you're viewing a share, set it to the portfolio owner.
            // you will get a permission error otherwise, even if you were logged into your own account.
        }

        $portfolio = Portfolio::where('user_id', $userId)
          ->where($column, $slugOrId)
          ->first();

        if (!$portfolio) {
            Session::flash('danger', 'Sorry, the requested portfolio is <strong>not found</strong> in our database. Please contact us on <a href="mailto:admin@engagingenterprises.co" class="alert-link">admin@engagingenterprises.co</a> for further information.');
            return Redirect::to('portfolios');
        }

        if (!$portfolio->strategiesCount) {
            Session::flash('danger', 'Sorry, your portfolio is <strong>empty</strong>. Please add some strategies from <a href="/strategy" class="alert-link">Discover Strategies</a> page.');
            return Redirect::to('portfolios');
        }

        $maxDate = $this->backtesting->getMaxDate($portfolio);
        $periods = $this->backtesting->getPeriods($portfolio, $maxDate);
        $portfolio = $this->backtesting->getPortfolio($portfolio, [
          'maxDate' => $maxDate,
          'periods' => $periods,
          'noMin' => false,  // when true do not return the hash of 'min'
        ]);

        $monthlyReturns = Helper::obj2arr($portfolio['min_monthly_returns']);
        if (!empty($monthlyReturns)){
          $monthlyReturns = array_reverse($monthlyReturns, true);
        }

        $registerModal = !$user; // are they using a share link and not logged in?
        // TODO: if sharing and logged in as user_to duplicate to the user > might had popup/btn instead
        // $newPortfolio = $portfolio->duplicate($title, $portfolio->slug);
        // $newPortfolio->draft_of = NULL;
        // $newPortfolio->save();

        $componentOptions = [];
        if ($period = Input::get('period')) {
          $componentOptions['period'] = $period;
        }

        return view('portfolio_info')->with([
            'monthlyReturns' => $monthlyReturns,
            'componentOptions' => $componentOptions,
            'maxDate' => $maxDate,
            'periods' => $periods,
            'portfolio' => $portfolio,
            'disclaimer' => $this->getDisclaimer(),
            'registerModal' => $registerModal,
        ]);
    }

}
