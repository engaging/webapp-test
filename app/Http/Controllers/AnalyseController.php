<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use App\Portfolio;
use App\PortfolioStrategy;
use App\Indexinfo;
use App\Setting;
use App\Regression;
use App\Pca;
use App\Services\Lab\BacktestingService;
use App\Services\Portfolio\PortfolioService;
use App\Services\Right\RightService;
use Throwable;
use View;
use DB;

class AnalyseController extends Controller implements Constant
{

    protected $backtesting;
    protected $getDraft;
    protected $right;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(BacktestingService $backtesting, PortfolioService $getDraft, RightService $right)
    {
        $this->middleware('auth');
        $this->backtesting = $backtesting;
        $this->getDraft = $getDraft;
        $this->right = $right;

        $maxDate = Setting::where('key', '=', 'maxDate')->first();
        if ($maxDate) {
          View::share('maxDate', $maxDate->value);
        }

        $userPortfolioCount = 0;
        $loggedin = Auth::check();
        if($loggedin){
            $userPortfolioCount = Portfolio::where(
                'user_id', '=', Auth::user()->id
            )->where(
                'draft_of', '=', null
            )->count();

            View::share('userPortfolioCount', $userPortfolioCount);
        } else {
            return Redirect::to('/login')->send();
        }
        if(Auth::user()->blocked > 0){
            return Redirect::to('/inactive')->send();
        }
    }

    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function show()
    {
        return view('analyse')->with([
            'type' => 'favourites',
            'indices' => [],
            'selected' => 'favourites',
            'analysing' => array(
                'url' => '/analyse/', //leave this one as the real one
                'groupBy' => 'all',
                'title' => "Favourites",
                'weighting' => "equal",
                'portfolio_id' => 0,
                'draft_of' => 0,
            )
        ]);
    }

    public function showPortfolioSelection(){
        $draft = null;
        $portfolio = Portfolio::where(
            'user_id', Auth::user()->id
        )->where(
            'draft_of', '=', null
        );

        if ($portfolio->count() === 0) {
          $portfolio = $this->getDraft->createDefaultPortfolio(Auth::user());
        } else {
          $portfolio = $portfolio->first();
        }

        $draft = $portfolio->getDraft(true, true);

        $maxDate = $this->backtesting->getMaxDate($draft);
        $periods = $this->backtesting->getPeriods($draft, $maxDate);

        if (env('APP_DEBUG') === true && $draft->activeStrategiesCount > 0) {
          try {
            $this->backtesting->getPortfolio($draft, [
              'maxDate' => $maxDate,
              'periods' => $periods,
            ]);
          } catch (Throwable $e) {
            echo 'Backtesting: ' . $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine();
          }
        }

        $customRebalancing = json_decode($draft->customRebalancing, true);
        $customBaseOptions = null;
        $customAllocation = null;
        if (!empty($customRebalancing)) {
          $customBaseOptions = $customRebalancing['customBaseOptions'];
          $customAllocation = $customRebalancing['customAllocation'];
        }

        return view('newlab')->with([
          'maxDate' => $maxDate,
          'periods' => $periods,
          'mode' => 'portfolio',
          'analysing' => json_encode(array(
              'slug' => $portfolio->slug , // use orginal slug
              'groupBy' => 'all',
              'strategyIds' => $draft->strategyIds,
              'title' => $draft->title,
              'weighting' => $draft->weighting,
              'portfolio_id' => $draft->id,
              'draft_of' => $draft->draft_of,
              'allocationOptions' => json_decode($draft->allocationOptions),
              'customBaseOptions' => $customBaseOptions,
              'rebalancingList' => $customAllocation,
              'name' => $draft->title,
              'tool' => 'backtesting',
              'type' => 'portfolio',
          ))
        ]);
    }

    public function showBacktesting($slug)
    {

        $portfolio = Portfolio::where(
          'user_id', '=', Auth::user()->id
        )->where(
          'draft_of', '=', null
        )->where(
          'slug', '=', $slug
        );

        if($portfolio->count() == 0){
            abort(403, 'Unauthorized action.');
        }

        $draft = $portfolio->first()->getDraft(true, true);

        $maxDate = $this->backtesting->getMaxDate($draft);
        $periods = $this->backtesting->getPeriods($draft, $maxDate);

        if (env('APP_DEBUG') === true && $draft->activeStrategiesCount > 0) {
          try {
            $this->backtesting->getPortfolio($draft, [
              'maxDate' => $maxDate,
              'periods' => $periods,
            ]);
          } catch (Throwable $e) {
            echo 'Backtesting: ' . $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine();
          }
        }

        $customRebalancing = json_decode($draft->customRebalancing, true);
        $customBaseOptions = null;
        $customAllocation = null;
        if (!empty($customRebalancing)) {
          $customBaseOptions = $customRebalancing['customBaseOptions'];
          $customAllocation = $customRebalancing['customAllocation'];
        }

        return view('newlab')->with([
          'maxDate' => $maxDate,
          'periods' => $periods,
          'mode' => 'portfolio',
          'analysing' => json_encode(array(
              'slug' => $portfolio->first()->slug,
              'groupBy' => 'all',
              'strategyIds' => $draft->strategyIds,
              'title' => $draft->title,
              'weighting' => $draft->weighting,
              'portfolio_id' => $draft->id,
              'draft_of' => $draft->draft_of,
              'name' => $draft->title,
              'customBaseOptions' => $customBaseOptions,
              'rebalancingList' => $customAllocation,
              'allocationOptions' => json_decode($draft->allocationOptions),
              'tool' => 'backtesting',
              'type' => 'portfolio',
          ))
        ]);
    }

    public function showPCA($slug) {
      $portfolio = Portfolio::where(
          'user_id', '=', Auth::user()->id
        )->where(
          'draft_of', '=', null
        )->where(
          'slug', '=', $slug
        );

        if($portfolio->count() == 0){
            abort(403, 'Unauthorized action.');
        }

        $draft = $portfolio->first()->getDraft(true, true);

        $maxDate = $this->backtesting->getMaxDate($draft);
        $periods = $this->backtesting->getPeriods($draft, $maxDate);

        return view('newlab')->with([
          'maxDate' => $maxDate,
          'periods' => $periods,
          'mode' => 'portfolio',
          'analysing' => json_encode(array(
              'slug' => $portfolio->first()->slug,
              'title' => $draft->title,
              'portfolio_id' => $draft->id,
              'draft_of' => $draft->draft_of,
              'name' => $draft->title,
              'tool' => 'pca',
              'type' => 'portfolio',
          ))
        ]);
    }

    public function portfoloRegression($slug) {
      $portfolios = Portfolio::where(
          'user_id', '=', Auth::user()->id
        )->where(
          'draft_of', '=', null
        )->where(
          'slug', '=', $slug
        );

        if($portfolios->count() == 0){
            abort(403, 'Unauthorized action.');
        }
        $portfolio = $portfolios->first();
        $maxDate = $this->backtesting->getMaxDate($portfolio);
        $periods = $this->backtesting->getPeriods($portfolio, $maxDate);

        return view('newlab')->with([
          'maxDate' => $maxDate,
          'periods' => $periods,
          'mode' => 'portfolio',
          'analysing' => json_encode(array(
              'slug' => $portfolio->slug,
              'title' => $portfolio->title,
              'portfolio_id' => $portfolio->id,
              'name' => $portfolio->title,
              'tool' => 'regression',
              'type' => 'portfolio',
          ))
        ]);
    }

    public function strategyRegression($code) {
      $index = Indexinfo::where('code', '=', $code)
          ->whereIn('type', self::STRATEGY_TYPES)
          ->first();

      if (!$index) {
        Session::flash('danger', 'Sorry, the requested strategy is <strong>not found</strong> in our database. Please contact us on <a href="mailto:admin@engagingenterprises.co" class="alert-link">admin@engagingenterprises.co</a> for further information.');
        return Redirect::to('strategy');
      }

      if (!$this->right->hasStrategyRight(Auth::user(), $index)) {
        Session::flash('danger', 'Sorry, you <strong>don\'t have access</strong> to this page. Please contact us on <a href="mailto:admin@engagingenterprises.co" class="alert-link">admin@engagingenterprises.co</a> for further information.');
        return Redirect::to('strategy');
      }

      $maxDate = $this->backtesting->getMaxDate($index);
      $periods = $this->backtesting->getPeriods($index, $maxDate);

        return view('newlab')->with([
          'maxDate' => $maxDate,
          'periods' => $periods,
          'mode' => 'strategy',
          'analysing' => json_encode(array(
              'code' => $index->code,
              'shortName' => $index->shortName,
              'indexinfo_id' => $index->id,
              'name' => $index->title,
              'tool' => 'regression',
              'type' => 'strategy',
          ))
        ]);
    }
}
