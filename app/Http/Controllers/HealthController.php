<?php

namespace App\Http\Controllers;

class HealthController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
        $this->middleware('dev');
    }

    public function showPhpIniSettings() {
      return response()->json([
        'memory_limit' => ini_get('memory_limit'),
        'post_max_size' => ini_get('post_max_size'),
        'upload_max_filesize' => ini_get('upload_max_filesize'),
        'max_input_time' => ini_get('max_input_time'),
        'file_uploads' => ini_get('file_uploads'),
        'max_execution_time' => ini_get('max_execution_time'),
        'LimitRequestBody' => ini_get('LimitRequestBody'),
      ]);
    }

}
