<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register the API routes for your application as
| the routes are automatically authenticated using the API guard and
| loaded automatically by this application's RouteServiceProvider.
|
*/

Route::group(['prefix' => 'api', 'middleware' => 'api'], function () {
    // Public api routes

    // Private api routes
    Route::group(['middleware' => 'auth:api'], function () {
        Route::post('/auth/verify', 'API\AuthController@authorizeWebApiGateway');
        Route::post('/auth/apikey', 'API\AuthController@authorizeDataApiGateway');

        Route::post('/pdf/strategy/{id}', 'API\PdfController@generateStrategyPdf');
        Route::post('/pdf/portfolio/{id}', 'API\PdfController@generatePortfolioPdf');

        Route::get('/strategies', 'API\StrategyController@getStrategies');
        Route::get('/strategies/filters', 'API\StrategyController@getStrategiesFilters');
        Route::get('/strategies/correlation', 'API\StrategyController@getStrategiesCorrelation');
        Route::get('/strategies/{strategy}', 'API\StrategyController@getStrategyById');
        Route::get('/strategies/{strategy}/periods', 'API\StrategyController@getPeriodsById');
        Route::get('/strategies/{strategy}/volatility-track', 'API\StrategyController@getVolatilityTrackById');
        Route::get('/strategies/{strategy}/correlation', 'API\StrategyController@getBenchmarksCorrelationById');
        Route::post('/strategies/{strategy}/constituents/send', 'API\StrategyController@sendConstituents');

        Route::post('/portfolios/sharing', 'API\PortfolioController@sharing');
        Route::get('/portfolios/sharing', 'API\PortfolioController@getSharing');

        Route::get('/portfolios', 'API\PortfolioController@getPortfolios');
        Route::get('/portfolios/{portfolio}', 'API\PortfolioController@getPortfolioById');
        Route::get('/portfolios/{portfolio}/options', 'API\PortfolioController@getPortfolioOptionById');
        Route::get('/portfolios/{portfolio}/periods', 'API\PortfolioController@getPeriodsById');
        Route::get('/portfolios/{portfolio}/strategies', 'API\PortfolioController@getPortfolioStrategiesById');
        Route::get('/portfolios/{portfolio}/volatility-track', 'API\PortfolioController@getVolatilityTrackById');
        Route::get('/portfolios/{portfolio}/correlation', 'API\PortfolioController@getBenchmarksCorrelationById');
        Route::post('/portfolios/{portfolio}/allocation', 'API\PortfolioController@getAllocationById');
        Route::post('/portfolios/{portfolio}/funding', 'API\PortfolioController@getFundingById');
        Route::get('/portfolios/{slug}/draft', 'API\PortfolioController@getPortfolioDraft');

        // @deprecated use /portfolios apis when available
        Route::resource('/portfolio', 'API\PortfolioController');

        Route::get('/regression/{type}/{id}/options', 'API\RegressionController@getOptions');
        Route::post('/regression/{type}/{id}/options', 'API\RegressionController@updateOptions');
        Route::post('/regression/{type}/{id}/heatmap', 'API\RegressionController@getHeatmap');
        Route::post('/regression/{type}/{id}', 'API\RegressionController@getRegression');

        Route::post('/pca/{portfolio}', 'API\PCAController@getPCA');
        Route::post('/pca/{portfolio}/dates', 'API\PCAController@getDates');
        Route::get('/pca/{portfolio}/options', 'API\PCAController@getOptions');
        Route::post('/pca/{portfolio}/options', 'API\PCAController@updateOptions');

        Route::get('/favourites/set', 'API\FavouritesController@getSet');
        Route::get('/favourites/empty', 'API\FavouritesController@clearFavourites');
        Route::get('/favourites/strategies', 'API\FavouritesController@getStrategies');
        Route::resource('/favourites', 'API\FavouritesController');

        Route::get('/communities', 'API\NetworkController@getCommunity');
        Route::put('/communities/{communityMember}', 'API\NetworkController@updateCommunityMember');
        Route::post('/communities/{communityMember}/connect', 'API\NetworkController@toggleConnectCommunityMember');
        Route::get('/communities/{communityMember}/connection_requests', 'API\NetworkController@getConnectionRequests');
        Route::put('/communities/{communityMember}/connection_accept', 'API\NetworkController@acceptConnectionRequest');
        Route::put('/communities/{communityMember}/connection_reject', 'API\NetworkController@rejectConnectionRequest');
        Route::post('/communities/{communityMember}/documents/{id}', 'API\DocumentsController@update');
        Route::resource('/communities/{communityMember}/documents', 'API\DocumentsController');

        Route::post('/feed', 'API\NetworkController@getFeed');
        Route::get('/feed/{id}', 'API\NetworkController@getFeedByCommunityMember');
        Route::post('/feed/{id}', 'API\NetworkController@addPostToFeed');
        Route::put('/feed/{post}', 'API\NetworkController@updatePost');
        Route::delete('/feed/{post}', 'API\NetworkController@removePost');

        Route::get('/cut-off-date', 'API\IndicesController@getCutOffDate');
        Route::get('/colors', 'API\IndicesController@getColors');
        Route::put('/colors', 'API\IndicesController@saveColors');
        Route::post('/indices/upload', 'API\IndicesController@uploadPrivateStrategy');
        Route::post('/indices/edit', 'API\IndicesController@editPrivateStrategy');
        Route::post('/indices/delete', 'API\IndicesController@deletePrivateStrategy');
        Route::post('/market', 'API\IndicesController@getMarket');

        // @deprecated use /strategies/filters api
        Route::get('/filters', 'API\IndicesFiltersController@get');

        Route::post('/dashboard/save', 'API\DashboardController@store');

        Route::group(['prefix' => '/inbox'], function () {
            Route::get('/', [ 'uses' => 'API\InboxController@index']);
            Route::post('/', ['uses' => 'API\InboxController@storeMessage']);
            Route::get('/notification/{uuid}', ['uses' => 'API\InboxController@getMessage']);
            Route::get('/announcement/{uuid}', ['uses' => 'API\InboxController@getAnnouncement']);
            Route::get('/{id}', ['uses' => 'API\InboxController@getMessage']);
            Route::put('/{id}', ['uses' => 'API\InboxController@update']);
        });

        // Administration
        Route::group(['prefix' => '/admin'], function () {
            // Database management
            Route::get('/cut-off-date/update', 'API\AdminController@updateCutOffDate');
            Route::get('/strategies/run-datafeed', 'API\AdminController@feedRawData');
            Route::get('/strategies/gen-tracks', 'API\AdminController@generateTrackData');
            Route::get('/strategies/gen-metrics', 'API\AdminController@generateStrategiesMetrics');
            Route::get('/portfolios/gen-metrics', 'API\AdminController@generatePortfoliosMetricsAndSendReports');

            // Users management
            Route::post('/register', 'API\AdminController@register');
            Route::get('/user-rights', 'API\AdminController@getUserRights');
            Route::group(['prefix' => '/users/{user}'], function () {
                // User management
                Route::get('/send-approval', 'API\AdminController@sendApproval');
                Route::get('/portfolios/{portfolio}/copy', 'API\AdminController@copyPortfolioToUser');
                // Rights management
                Route::post('/right', 'API\AdminController@createInvestorRight');
                Route::put('/right', 'API\AdminController@updateUserRight');
                // Plans management
                Route::post('/subscription', 'API\PlanController@store');
                Route::put('/subscription', 'API\PlanController@update');
                Route::delete('/subscription', 'API\PlanController@destroy');
            });

            // Teams management
            Route::group(['prefix' => '/teams'], function () {
                Route::get('/', 'API\AdminController@getTeams');
                Route::post('/', 'API\AdminController@createTeam');
                Route::delete('/{team}', 'API\AdminController@deleteTeam');
                Route::put('/{team}/users/{user}', 'API\AdminController@addTeamUser');
                Route::delete('/{team}/users/{user}', 'API\AdminController@deleteTeamUser');
            });
        });

        Route::group(['prefix' => '/preference'], function () {
          Route::post('/{type}', 'API\PreferenceController@updatePreference');
        });

        Route::group(['prefix' => '/symphony'], function () {
            Route::get('/', 'API\SymphonyController@getAccount');
            Route::post('/', 'API\SymphonyController@linkAccount');
            Route::post('/verify', 'API\SymphonyController@verifyAccount');
            Route::delete('/{symphonyAccount}', 'API\SymphonyController@unlinkAccount');
        });
    });
});
