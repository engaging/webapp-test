const path = require('path');
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');

const config = {
  entry: [
    '@babel/polyfill',
    './resources/assets/js/app.js',
    './resources/assets/less/app.less',
    './resources/assets/css/premia.css',
  ],
  output: {
    path: path.resolve(__dirname, './public/js'),
    filename: '[name].js',
  },
  optimization: {
    splitChunks: {
      chunks: 'all',
    },
  },
  resolve: {
    extensions: ['.js', '.css', '.vue', '.ts'],
    alias: {
      vue: 'vue/dist/vue.common.js',
    },
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        use: ['babel-loader', 'remove-flow-types-loader'],
      },
      {
        test: /\.ts$/,
        exclude: /(node_modules|bower_components)/,
        use: ['babel-loader'],
      },
      {
        test: /\.vue$/,
        loader: 'vue-loader',
        options: {
          loaders: {
            ts: 'babel-loader',
          },
        },
      },
      {
        test: /\.css$/,
        use: ['vue-style-loader', 'css-loader'],
      },
      {
        test: /\.less$/,
        use: ['vue-style-loader', 'css-loader', 'less-loader'],
      },
      {
        test: /\.woff2?$|\.ttf$|\.eot$|\.svg$/,
        use: 'file-loader',
      },
    ],
  },
  plugins: [
    new ForkTsCheckerWebpackPlugin({
      // tslint: true,
      vue: true,
    }),
  ],
};

module.exports = config;
