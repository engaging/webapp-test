/* eslint-disable import/no-extraneous-dependencies, class-methods-use-this */
const fs = require('fs');
const path = require('path');
const merge = require('webpack-merge');
const common = require('./webpack.common');

class HtmlHashAssetNamePlugin {
  apply(compiler) {
    compiler.hooks.afterEmit.tap('HtmlHashAssetNamePlugin', (compilation) => {
      const stats = compilation.getStats().toJson();
      if (!stats.errors.length) {
        const htmlPath = path.join(__dirname, 'spark/resources/views/layouts/app.blade.php');
        const html = fs.readFileSync(htmlPath, 'utf8');
        console.log(stats.assetsByChunkName);

        const htmlOutput = html
          .replace(
            /<script\s+src=(["'])(.+?)vendors~main\.js\1/i,
            `<script src=$1$2${stats.assetsByChunkName['vendors~main']}$1`,
          )
          .replace(
            /<script\s+src=(["'])(.+?)main\.js\1/i,
            `<script src=$1$2${stats.assetsByChunkName.main}$1`,
          );
        fs.writeFileSync(
          htmlPath,
          htmlOutput,
        );
      }
    });
  }
}

module.exports = merge(common, {
  output: {
    path: path.resolve(__dirname, './public/js'),
    filename: '[name].[contenthash].js',
  },
  plugins: [
    new HtmlHashAssetNamePlugin(),
  ],
});
