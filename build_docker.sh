#!/bin/bash

#fail on all errors
set -e



TIMESTAMP=$(date "+%Y%m%d-%H%M%S")

## log into ecr
aws ecr get-login --profile fintech --region eu-west-1 | bash -

## build docker
docker build -t fintech-app . 

## tag docker 
docker tag fintech-app:latest 116644049570.dkr.ecr.eu-west-1.amazonaws.com/fintech-app:edge 
docker tag fintech-app:latest 116644049570.dkr.ecr.eu-west-1.amazonaws.com/fintech-app:latest 
docker tag fintech-app:latest 116644049570.dkr.ecr.eu-west-1.amazonaws.com/fintech-app:$TIMESTAMP     

## push docker 
docker push 116644049570.dkr.ecr.eu-west-1.amazonaws.com/fintech-app:edge
docker push 116644049570.dkr.ecr.eu-west-1.amazonaws.com/fintech-app:latest
docker push 116644049570.dkr.ecr.eu-west-1.amazonaws.com/fintech-app:$TIMESTAMP