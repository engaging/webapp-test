var BubbleChart, root,
bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

window.valuesById = {};

BubbleChart = (function() {
  function BubbleChart(data) {
    this.do_filter = bind(this.do_filter, this);
    this.use_filters = bind(this.use_filters, this);
    this.hide_details = bind(this.hide_details, this);
    this.show_details = bind(this.show_details, this);
    this.hide_labels = bind(this.hide_labels, this);
    this.display_labels = bind(this.display_labels, this);
    this.move_towards_group = bind(this.move_towards_group, this);
    this.display_by_group = bind(this.display_by_group, this);
    this.move_towards_group_center = bind(this.move_towards_group_center, this);
    this.group_by = bind(this.group_by, this);
    this.get_distinct_values = bind(this.get_distinct_values, this);
    this.color_by = bind(this.color_by, this);
    this.remove_colors = bind(this.remove_colors, this);
    this.sort = bind(this.sort, this);
    this.get_color_map = bind(this.get_color_map, this);

    this.get_type_from_key_name = bind(this.get_type_from_key_name, this);
    this.get_color_map_lookup_set = bind(this.get_color_map_lookup_set, this);
    this.get_color_map_grade = bind(this.get_color_map_grade, this);
    this.get_color_map_achievement = bind(this.get_color_map_achievement, this);
    this.move_towards_center = bind(this.move_towards_center, this);
    this.display_group_all = bind(this.display_group_all, this);
    this.start = bind(this.start, this);
    this.create_vis = bind(this.create_vis, this);
    this.create_nodes = bind(this.create_nodes, this);
    this.data = data;
    this.width = $("#vis").width();
    this.height = 400;
    this.default_radius = 200;
    this.tooltip = CustomTooltip("my_tooltip", 240);
    this.center = {
      x: this.width / 2,
      y: this.height / 2
    };
    this.layout_gravity = -0.01;
    this.damper = 0.5;
    this.vis = null;
    this.force = null;
    this.circles = null;
    this.nodes = [];
    this.create_nodes();
    this.create_vis();
  }

  BubbleChart.prototype.get_node_info = function(d, i, data) {
    d = JSON.parse(JSON.stringify(d));
    var max = 0;
    var min = 0;
    var absScale = 0;
    data.forEach(function(i){
      var r = parseFloat(i['return']);
      if(r<min){
        min = r;
      }
      if(r>max){
        max = r;
      }
    });

    if(max > 20){
      this.default_radius = 200;
    } else if(max > 10){
      this.default_radius = 250;
    } else if(max > 5){
      this.default_radius = 300;
    }

    var returnScale = parseFloat(d['return']);
    var radius = 15 + (this.default_radius * Math.abs(returnScale));
    var halfway = this.height / 2;

    if(returnScale < 0){
        var y  = halfway + ((returnScale/min) * halfway);
    } else if(returnScale > 0){
        var y  = halfway - ((returnScale/max) * halfway);
    } else {
      var y = halfway;
    }


    if(!d.slug){
      function slugify(text){
          return text.toString().toLowerCase()
              .replace(/\s+/g, '-')           // Replace spaces with -
              .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
              .replace(/\-\-+/g, '-')         // Replace multiple - with single -
              .replace(/^-+/, '')             // Trim - from start of text
              .replace(/-+$/, '');            // Trim - from end of text
      }
      var id = 'v-' + slugify(d.asset) + "_____" + slugify(d.factor);
    } else {
      var id = 'v-' + d.slug;
    }

    window.valuesById[id] = returnScale;

    var node = {
      id: id,
      original: d,
      radius: Math.min(radius, 38), // limit bubbles sizes
      value: 99,
      x: Math.random() * this.width,
      y: y,
    };

    return node;

  }

  BubbleChart.prototype.create_nodes = function() {

    return this.data.forEach((function(_this) {
      return function(d, i) {
        var node = _this.get_node_info(d, i, _this.data);
        return _this.nodes.push(node);
      };
    })(this));
  };

  BubbleChart.prototype.get_rgb = function(value, min, max) {
    var green = {r:41,g:185,b:153};
    var red = {r:250,g:41,b:79};
    value = parseFloat(value);
    if(value > 0){
       var valueOnScale =  value / max;
       valueOnScale = valueOnScale * 0.8;
       valueOnScale += 0.2;
       return 'rgba(69,161,99,' + valueOnScale + ")";
       //we want green
    } else if(value < 0){
       var valueOnScale = value / min;
       valueOnScale = valueOnScale * 0.8;
       valueOnScale += 0.2;
       return 'rgba(255,0,0,' + valueOnScale + ")";
    } else {
       return "rgba(236,236,236,1)";//for example
    }
  }

  BubbleChart.prototype.create_vis = function() {
    var that;
    this.vis = d3.select("#vis").append("svg").attr("width", this.width).attr("height", this.height).attr("id", "svg_vis");



    this.circles = d3.select("#svg_vis").selectAll("circle").data(this.nodes, function(d) {


      return d.id;
    });
    that = this;
    var min = 0;
    var max = 0;
    for(var y = 0; y < this.nodes.length; y++){
      var x = parseFloat(this.nodes[y]['original']['return']);
      if(x < min){
          min = x;
      }
      if(x > max){
          max = x;
      }
    }
    $('body').on('touchstart', function(d, i) {
      var this2 = this;
      return setTimeout(function(){ that.hide_details(d, i, this2); }, 100);
    });
    this.circles.enter().append("circle").attr("r", 100).style("fill", (function(_this) {
      return function(d) {
        return that.get_rgb(d.original['return'], min, max);
        return '#cfcfcf';
      };
    })(this)).attr("stroke-width", 2).attr("stroke", (function(_this) {
      return function(d) {
        return '#404040';
      };
    })(this)).attr("id", function(d) {
      function slugify(text){
          return text.toString().toLowerCase()
              .replace(/\s+/g, '-')           // Replace spaces with -
              .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
              .replace(/\-\-+/g, '-')         // Replace multiple - with single -
              .replace(/^-+/, '')             // Trim - from start of text
              .replace(/-+$/, '');            // Trim - from end of text
      }

      var id = slugify(d.original.asset) + "_____" + slugify(d.original.factor);

      return "bubble_" + id;
    }).attr("opacity", 0).on("mouseover", function(d, i) {
      var this2 = this;
      return setTimeout(function(){ that.show_details(d, i, this2); }, 100);
    }).on("touchstart", function(d, i) {
      var this2 = this;
      setTimeout(function(){ that.show_details(d, i, this2); }, 100);
      return false;
    }).on("mouseout", function(d, i) {
      var this2 = this;
      return setTimeout(function(){ that.hide_details(d, i, this2); }, 100);
    });
    return this.circles.transition().duration(2000).attr("opacity", 1).attr("r", function(d) {
      return d.radius;
    });
  };

  BubbleChart.prototype.charge = function(d) {
    if (d.radius === 0) {
      return 0;
    }
    return -Math.pow(d.radius, 2);
  };

  BubbleChart.prototype.gravity = function(d) {
    if (d.radius === 0) {
      return 0;
    }
    return -Math.pow(d.radius, 2);
  };

  BubbleChart.prototype.start = function() {
    this.force = d3.layout.force().nodes(this.nodes).size([this.width, this.height]);

    return this.circles.call(this.force.drag);
  };

  BubbleChart.prototype.display_group_all = function() {
    this.hide_labels();
    this.force.gravity(this.layout_gravit).charge(this.charge).friction(0.9).on("tick", (function(_this) {
      return function(e) {
        return _this.circles.each(_this.move_towards_center(e.alpha)).attr("cx", function(d) {
          return d.x;// + 6;
        }).attr("cy", function(d) {
          return d.y;
        });
      };
    })(this));
    return this.force.start();
  };

  BubbleChart.prototype.move_towards_center = function(alpha) {
    return (function(_this) {
      return function(d) {
        // return d;
        //console.log('aids', d);
        d.x = d.x + (_this.center.x - d.x) * (_this.damper + 0.02) * alpha;
        return d.y = d.y + (_this.center.y - d.y) * (_this.damper + 0.02) * alpha;
      };
    })(this);
  };

  BubbleChart.prototype.get_color_map_lookup_set = function(allValuesArray) {
    var baseArray, color_map, index, j, len, value;
    baseArray = ["#0000D9", "#FF00FF", "#FF0033", "#FFCC66", "#66CC33", "#33FFCC", "#00A0AA", "#FFCCFF", "#FF9933", "#99FF99", "#00BB00", "#CCFFCC", "#333333", "#CCCCCC", "#99CCCC", "#FF0000"];
    index = 0;
    color_map = {};
    for (j = 0, len = allValuesArray.length; j < len; j++) {
      value = allValuesArray[j];
      color_map[value] = baseArray[index];
      index = index + 1;
      if (index >= baseArray.length) {
        index = 0;
      }
    }
    return color_map;
  };

  BubbleChart.prototype.get_type_from_key_name = function(keyName) {
    return "Other";
  };

  BubbleChart.prototype.get_color_map = function(keyName, allValuesArray) {
    var key_type;
    key_type = this.get_type_from_key_name(keyName);
    if(keyName == 'return'){

    }
    switch (key_type) {
      default:
        return this.get_color_map_lookup_set(allValuesArray);
    }
  };

  BubbleChart.prototype.sort = function(keyName, allValuesArray) {
    var key_type;
    key_type = this.get_type_from_key_name(keyName);
    switch (key_type) {
      default:
        return allValuesArray.sort();
    }
  };

  BubbleChart.prototype.remove_colors = function() {
    this.circles.transition().duration(2000).style("fill", "#cfcfcf");
    return hide_color_chart();
  };

  BubbleChart.prototype.color_by = function(what_to_color_by, colorsToUse) {
    var allValuesArray, color_mapper;
    this.what_to_color_by = what_to_color_by;
    allValuesArray = this.get_distinct_values(what_to_color_by);
    color_mapper = this.get_color_map(what_to_color_by, allValuesArray);
    if(what_to_color_by != 'return'){
        if(colorsToUse){
            color_mapper = colorsToUse;
        }
    } else {

        var min = 0;
        var max = 0;
        for(var x in color_mapper){
          x = parseFloat(x);
          if(x < min){
            min = x;
          }
          if(x > max){
              max = x;
          }
        }

        for(var x in color_mapper){
            var newRgb = this.get_rgb(x, min, max);
            color_mapper[x] = newRgb;
        }
    }
    //show_color_chart(what_to_color_by, color_mapper);
    return this.circles.transition().duration(1000).style("fill", (function(_this) {
      return function(d) {
        return color_mapper[d.original[what_to_color_by]];
      };
    })(this));
  };

  BubbleChart.prototype.get_distinct_values = function(what) {
    var allValuesArray = [];
    switch (what) {
      case 'asset':
        allValuesArray = [
          'Commodity',
          'Credit',
          'Equity',
          'Fixed Income',
          'Foreign Exchange',
          'Multi Assets',
        ];
        break;
      case 'factor':
       allValuesArray = [
         'Carry',
         'Momentum',
         'Multi',
         'Other',
         'Value',
         'Volatility',
       ];
       break;
    }
    this.sort(what, allValuesArray);
    return allValuesArray;

    // var allValues, allValuesArray, key, value;
    // allValues = {};
    // this.nodes.forEach((function(_this) {
    //   return function(d) {
    //     var value;
    //     value = d.original[what];
    //     if(what == 'factor'){
    //       if(groupIntoOther.indexOf(value) >= 0){
    //         value = 'Other';
    //       }
    //     }
    //     return allValues[value] = true;
    //   };
    // })(this));
    // allValuesArray = [];
    // for (key in allValues) {
    //   value = allValues[key];
    //   allValuesArray.push(key);
    // }
    // this.sort(what, allValuesArray);
    // return allValuesArray;
  };

  BubbleChart.prototype.group_by = function(what_to_group_by) {
    var allValuesArray, numCenters, position, total_slots;
    this.what_to_group_by = what_to_group_by;
    allValuesArray = this.get_distinct_values(what_to_group_by);
    numCenters = allValuesArray.length;
    this.group_centers = {};
    this.group_labels = {};

    position = 1;
    total_slots = allValuesArray.length + 1;

    var labelposition = 0;
    var l_total_slots = allValuesArray.length;

    allValuesArray.forEach((function(_this) {
      return function(i) {
        var x_position;
        x_position = _this.width * position / total_slots;
        _this.group_centers[i] = {
          x: x_position,
          y: _this.height / 2
        };
        _this.group_labels[i] = (_this.width * labelposition / l_total_slots)+40;
        labelposition = labelposition+1;
        return position = position + 1;
      };
    })(this));
    this.hide_labels();
    this.force.gravity(this.layout_gravity).charge(this.charge).friction(0.9).on("tick", (function(_this) {
      return function(e) {
        return _this.circles.each(_this.move_towards_group_center(e.alpha)).attr("cx", function(d) {
          return d.x;
        }).attr("cy", function(d) {
          return d.y;
        });
      };
    })(this));
    this.force.start();
    return this.display_labels();
  };

  BubbleChart.prototype.move_towards_group_center = function(alpha) {
    return (function(_this) {

      return function(d) {
        var target, value;
        value = d.original[_this.what_to_group_by];
        if(groupIntoOther.indexOf(value) >= 0){
          value = 'Other';
        }


        target = _this.group_centers[value];
        var max = 0;
        var min = 0;
        _this.data.forEach(function(i){
          var r = parseFloat(i['return']);
          if(r<min){
            min = r;
          }
          if(r>max){
            max = r;
          }
        });



        var returnScale = window.valuesById[d.id];// parseFloat(d.original['return']);
        var halfway = _this.height / 2;
        if(returnScale < 0){
            var y  = halfway + ((returnScale/min) * halfway);
            var diff = y - halfway;
            y = halfway + (Math.sqrt(diff) * Math.sqrt(halfway * 0.05));
        } else if(returnScale > 0){
            var y  = halfway - ((returnScale/max) * halfway);
            var diff = halfway - y;
            y = halfway - (Math.sqrt(diff) * Math.sqrt(halfway * 0.003));
        } else {
            var y = halfway;
            var diff = 0;
        }

        target.y = y;

        d.x = d.x + (target.x - d.x) * (_this.damper + 0.02) * alpha * 1.1;

        return d.y = d.y + (target.y - d.y) * (_this.damper + 0.02) * alpha * 1.1;
      };
    })(this);
  };

  BubbleChart.prototype.display_by_group = function() {
    this.force.gravity(this.layout_gravity).charge(this.charge).friction(0.9).on("tick", (function(_this) {
      return function(e) {
        return _this.circles.each(_this.move_towards_group(e.alpha)).attr("cx", function(d) {
          return d.x;
        }).attr("cy", function(d) {
          return d.y;
        });
      };
    })(this));
    this.force.start();
    return this.display_years();
  };

  BubbleChart.prototype.move_towards_group = function(alpha) {
    return (function(_this) {
      return function(d) {
        var target;
        target = _this.group_centers[d.group];
        d.x = d.x + (target.x - d.x) * (_this.damper + 0.02) * alpha * 1.1;
        return d.y = d.y + (target.y - d.y) * (_this.damper + 0.02) * alpha * 1.1;
      };
    })(this);
  };

  BubbleChart.prototype.display_labels = function() {
    var label_data, labels;
    label_data = d3.keys(this.group_labels);
    labels = d3.select("#svg_vis").selectAll(".top_labels").data(label_data);
    return labels.enter().append("text").attr("class", "top_labels").attr("x", (function(_this) {
      return function(d) {
        return _this.group_labels[d];
      };
    })(this)).attr("y", 40).attr("text-anchor", "start").text(function(d) {
      return d;
    });
  };

  BubbleChart.prototype.hide_labels = function() {
    var labels;
    return labels = d3.select("#svg_vis").selectAll(".top_labels").remove();
    return labels = this.vis.selectAll(".top_labels").remove();
  };

  BubbleChart.prototype.show_details = function(data, i, element) {
    var content, key, ref, title, value;
    d3.select(element).attr("stroke", "black");

    ref = data.original;
    if($(element).attr('original')){
      ref = JSON.parse($(element).attr('original'));
    }
    content = "<h4>" + ref.asset + " " + ref.factor + "</h4>";
    var includeItems = ['number_of_strategies', 'return', 'volatility', 'sharpe'];

    for (var a = 0; a < includeItems.length; a++) {
      var key = includeItems[a];
      value = ref[key];
      switch(key){
        case 'factor':
        case 'asset':
        case 'number_of_strategies':
        break;
        case 'sharpe':
          value = (parseFloat(value) * 100).toFixed(2);
        break;
        default:
          value = (parseFloat(value) * 100).toFixed(2) + '%';
        break;
      }
      title = key.substring(key.indexOf(":") + 1);
      switch(title){
        case 'factor':
          title = 'Factor';
          break;
        case 'asset':
          title = 'Asset Class';
          break;
        case 'number_of_strategies':
        case 'number of strategies':
          title = 'Number of Strategies';
          break;
        case 'returnCum':
          title = 'Return';
          break;
        case 'return':
          title = 'Return p.a.';
          break;
        case 'volatility':
          title = 'Volatility';
          break;
        case 'sharpe':
          title = 'Sharpe';
          break;
        case 'skewness':
          title = 'Skewness';
          break;
        case 'kurtosis':
          title = 'Kurtosis';
          break;
        case 'calmar':
          title = 'Calmar';
          break;
        case 'omega':
          title = 'Omega';
          break;
        case 'sortino':
          title = 'Sortino';
          break;
        case 'var':
          title = 'Var';
          break;
        case 'mvar':
          title = 'ModVar';
          break;
        case 'cvar':
          title = 'CVar';
          break;
        case 'avg_drawdown':
          title = 'Avg DD';
          break;
        case 'mdd':
          title = 'Max DD';
          break;
        case 'worst20d':
          title = 'Worst 20d';
          break;
        case 'posWeeks':
          title = '%Up Week';
          break;
        case 'posMonths':
          title = '%Up Month';
          break;
      }
      title = title.replace(/_/g, ' ');

      content += "<span class=\"name\">" + title + ":</span><span class=\"value\"> " + value + "</span><br/>";
    }

    var queryList = function(key) {
      return (ref.filters[key] || [])
        .map(function(val) {
          return '&' + key + '[]=' + val;
        })
        .join('');
    };
    content += "<p><a target='_blank' href='/strategy?types[]=Strategy&assetClasses[]=" + ref.asset + "&factors[]=" + ref.factor + "&period=" + ref.period + queryList('regions') + queryList('currencies') + queryList('styles') + queryList('returnTypes') + "' class='btn btn-default'>Browse Strategies</a></p>";
    $("#popover-test").find('.popover-content').html(content);
    $("#popover-test").addClass('in');

    $("#popover-test").css({
      left: parseInt($(element).attr('cx')) + parseInt($(element).attr('r') / 2) + 20,
      top: parseInt($(element).attr('cy')) + parseInt($(element).attr('r') / 2) - ($("#popover-test").height() / 2) - 20,
      display: 'block',
      'z-index': 99999
    });

  };

  BubbleChart.prototype.hide_details = function(data, i, element) {
    var that = this;
    if ($("#popover-test:hover").length) {
      return $("#popover-test").on("mouseout", function() {
        if (!$("#popover-test:hover").length) {
          d3.select(element).attr("stroke", "#404040");
          $("#popover-test").css({ display: 'none' });
          that.tooltip.hideTooltip();
        }
      });
    }
    d3.select(element).attr("stroke", "#404040");
    $("#popover-test").css({ display: 'none' });
    that.tooltip.hideTooltip();
  };

  BubbleChart.prototype.use_filters = function(filters) {
    return this.nodes.forEach((function(_this) {
      return function(d) {
        d.radius = _this.default_radius;
        filters.discrete.forEach(function(filter) {
          var value;
          value = d.original[filter.target];
          if (filter.removeValues[value] != null) {
            return d.radius = 0;
          }
        });
        return _this.do_filter();
      };
    })(this));
  };

  BubbleChart.prototype.new_data = function() {
    window.circles = this.circles;
    //console.log(window.circles);
    circles.exit().remove()
  };

  BubbleChart.prototype.do_filter = function() {
    this.force.start();
    return this.circles.transition().duration(2000).attr("r", function(d) {
      return d.radius;
    });
  };

  return BubbleChart;

})();

root = typeof exports !== "undefined" && exports !== null ? exports : this;

// $(function() {
// //   var chart, render_chart, render_vis;
// //   chart = null;
// //   render_vis = function(csv) {
// //     render_filters_colors_and_groups(csv);
// //     return render_chart(csv);
// //   };
// //   render_chart = function(csv) {
// //     chart = new BubbleChart(csv);
// //     chart.start();
// //     return root.display_all();
// //   };
// //   root.display_all = (function(_this) {
// //     return function() {
// //       return chart.display_group_all();
// //     };
// //   })(this);
// //   root.group_by = (function(_this) {
// //     return function(groupBy) {
// //       if (groupBy === '') {
// //         return chart.display_group_all();
// //       } else {
// //         return chart.group_by(groupBy);
// //       }
// //     };
// //   })(this);
// //   root.color_by = (function(_this) {
// //     return function(colorBy) {
// //       if (colorBy === '') {
// //         return chart.remove_colors();
// //       } else {
// //         return chart.color_by(colorBy);
// //       }
// //     };
// //   })(this);
// //   root.use_filters = (function(_this) {
// //     return function(filters) {
// //       return chart.use_filters(filters);
// //     };
// //   })(this);
// //   return d3.csv("/test-d3/sciencedata.csv", render_vis);
// });

// ---
// generated by coffee-script 1.9.2
