<?php

/**
 * Laravel - A PHP Framework For Web Artisans
 *
 * @package  Laravel
 * @author   Taylor Otwell <taylorotwell@gmail.com>
 */

/*
|--------------------------------------------------------------------------
| Register The Auto Loader
|--------------------------------------------------------------------------
|
| Composer provides a convenient, automatically generated class loader for
| our application. We just need to utilize it! We'll simply require it
| into the script here so that we don't have to worry about manual
| loading any of our classes later on. It feels nice to relax.
|
*/

require __DIR__.'/../bootstrap/autoload.php';

/*
|--------------------------------------------------------------------------
| Turn On The Lights
|--------------------------------------------------------------------------
|
| We need to illuminate PHP development, so let us turn on the lights.
| This bootstraps the framework and gets it ready for use, then it
| will load up this application so that we can run it and send
| the responses back to the browser and delight our users.
|
*/

$app = require_once __DIR__.'/../bootstrap/app.php';

/*
|--------------------------------------------------------------------------
| Run The Application
|--------------------------------------------------------------------------
|
| Once we have the application, we can handle the incoming request
| through the kernel, and send the associated response back to
| the client's browser allowing them to enjoy the creative
| and wonderful application we have prepared for them.
|
*/

$kernel = $app->make(Illuminate\Contracts\Http\Kernel::class);

$response = $kernel->handle(
    $request = Illuminate\Http\Request::capture()
);

// Redirect Europe users based on Cloudflare IP Geolocation
// $countryCode = $request->server('HTTP_CF_IPCOUNTRY');
// $continent = countryToContinent($countryCode);
// $server = explode('.', $request->server('HTTP_HOST'));
// $subdomain = $server[0];
// $url = $request->fullUrl();
// if ($subdomain !== 'eu' && $continent === 'Europe') {
//     $newUrl = explode($request->server('HTTP_HOST'), $url);
//     array_splice($newUrl, 1, 0, ['eu.engagingenterprises.co']);
//     $newUrl = implode('', $newUrl);
//     header('Location: ' . $newUrl);
//     die();
//     return redirect()->to($newUrl);
// }

$response->send();

$kernel->terminate($request, $response);

// function countryToContinent($country) {
//     if ($country === 'AF') return 'Asia';
//     if ($country === 'AX') return 'Europe';
//     if ($country === 'AL') return 'Europe';
//     if ($country === 'DZ') return 'Africa';
//     if ($country === 'AS') return 'Oceania';
//     if ($country === 'AD') return 'Europe';
//     if ($country === 'AO') return 'Africa';
//     if ($country === 'AI') return 'North America';
//     if ($country === 'AQ') return 'Antarctica';
//     if ($country === 'AG') return 'North America';
//     if ($country === 'AR') return 'South America';
//     if ($country === 'AM') return 'Asia';
//     if ($country === 'AW') return 'North America';
//     if ($country === 'AU') return 'Oceania';
//     if ($country === 'AT') return 'Europe';
//     if ($country === 'AZ') return 'Asia';
//     if ($country === 'BS') return 'North America';
//     if ($country === 'BH') return 'Asia';
//     if ($country === 'BD') return 'Asia';
//     if ($country === 'BB') return 'North America';
//     if ($country === 'BY') return 'Europe';
//     if ($country === 'BE') return 'Europe';
//     if ($country === 'BZ') return 'North America';
//     if ($country === 'BJ') return 'Africa';
//     if ($country === 'BM') return 'North America';
//     if ($country === 'BT') return 'Asia';
//     if ($country === 'BO') return 'South America';
//     if ($country === 'BA') return 'Europe';
//     if ($country === 'BW') return 'Africa';
//     if ($country === 'BV') return 'Antarctica';
//     if ($country === 'BR') return 'South America';
//     if ($country === 'IO') return 'Asia';
//     if ($country === 'VG') return 'North America';
//     if ($country === 'BN') return 'Asia';
//     if ($country === 'BG') return 'Europe';
//     if ($country === 'BF') return 'Africa';
//     if ($country === 'BI') return 'Africa';
//     if ($country === 'KH') return 'Asia';
//     if ($country === 'CM') return 'Africa';
//     if ($country === 'CA') return 'North America';
//     if ($country === 'CV') return 'Africa';
//     if ($country === 'KY') return 'North America';
//     if ($country === 'CF') return 'Africa';
//     if ($country === 'TD') return 'Africa';
//     if ($country === 'CL') return 'South America';
//     if ($country === 'CN') return 'Asia';
//     if ($country === 'CX') return 'Asia';
//     if ($country === 'CC') return 'Asia';
//     if ($country === 'CO') return 'South America';
//     if ($country === 'KM') return 'Africa';
//     if ($country === 'CD') return 'Africa';
//     if ($country === 'CG') return 'Africa';
//     if ($country === 'CK') return 'Oceania';
//     if ($country === 'CR') return 'North America';
//     if ($country === 'CI') return 'Africa';
//     if ($country === 'HR') return 'Europe';
//     if ($country === 'CU') return 'North America';
//     if ($country === 'CY') return 'Asia';
//     if ($country === 'CZ') return 'Europe';
//     if ($country === 'DK') return 'Europe';
//     if ($country === 'DJ') return 'Africa';
//     if ($country === 'DM') return 'North America';
//     if ($country === 'DO') return 'North America';
//     if ($country === 'EC') return 'South America';
//     if ($country === 'EG') return 'Africa';
//     if ($country === 'SV') return 'North America';
//     if ($country === 'GQ') return 'Africa';
//     if ($country === 'ER') return 'Africa';
//     if ($country === 'EE') return 'Europe';
//     if ($country === 'ET') return 'Africa';
//     if ($country === 'FO') return 'Europe';
//     if ($country === 'FK') return 'South America';
//     if ($country === 'FJ') return 'Oceania';
//     if ($country === 'FI') return 'Europe';
//     if ($country === 'FR') return 'Europe';
//     if ($country === 'GF') return 'South America';
//     if ($country === 'PF') return 'Oceania';
//     if ($country === 'TF') return 'Antarctica';
//     if ($country === 'GA') return 'Africa';
//     if ($country === 'GM') return 'Africa';
//     if ($country === 'GE') return 'Asia';
//     if ($country === 'DE') return 'Europe';
//     if ($country === 'GH') return 'Africa';
//     if ($country === 'GI') return 'Europe';
//     if ($country === 'GR') return 'Europe';
//     if ($country === 'GL') return 'North America';
//     if ($country === 'GD') return 'North America';
//     if ($country === 'GP') return 'North America';
//     if ($country === 'GU') return 'Oceania';
//     if ($country === 'GT') return 'North America';
//     if ($country === 'GG') return 'Europe';
//     if ($country === 'GN') return 'Africa';
//     if ($country === 'GW') return 'Africa';
//     if ($country === 'GY') return 'South America';
//     if ($country === 'HT') return 'North America';
//     if ($country === 'HM') return 'Antarctica';
//     if ($country === 'VA') return 'Europe';
//     if ($country === 'HN') return 'North America';
//     if ($country === 'HK') return 'Asia';
//     if ($country === 'HU') return 'Europe';
//     if ($country === 'IS') return 'Europe';
//     if ($country === 'IN') return 'Asia';
//     if ($country === 'ID') return 'Asia';
//     if ($country === 'IR') return 'Asia';
//     if ($country === 'IQ') return 'Asia';
//     if ($country === 'IE') return 'Europe';
//     if ($country === 'IM') return 'Europe';
//     if ($country === 'IL') return 'Asia';
//     if ($country === 'IT') return 'Europe';
//     if ($country === 'JM') return 'North America';
//     if ($country === 'JP') return 'Asia';
//     if ($country === 'JE') return 'Europe';
//     if ($country === 'JO') return 'Asia';
//     if ($country === 'KZ') return 'Asia';
//     if ($country === 'KE') return 'Africa';
//     if ($country === 'KI') return 'Oceania';
//     if ($country === 'KP') return 'Asia';
//     if ($country === 'KR') return 'Asia';
//     if ($country === 'KW') return 'Asia';
//     if ($country === 'KG') return 'Asia';
//     if ($country === 'LA') return 'Asia';
//     if ($country === 'LV') return 'Europe';
//     if ($country === 'LB') return 'Asia';
//     if ($country === 'LS') return 'Africa';
//     if ($country === 'LR') return 'Africa';
//     if ($country === 'LY') return 'Africa';
//     if ($country === 'LI') return 'Europe';
//     if ($country === 'LT') return 'Europe';
//     if ($country === 'LU') return 'Europe';
//     if ($country === 'MO') return 'Asia';
//     if ($country === 'MK') return 'Europe';
//     if ($country === 'MG') return 'Africa';
//     if ($country === 'MW') return 'Africa';
//     if ($country === 'MY') return 'Asia';
//     if ($country === 'MV') return 'Asia';
//     if ($country === 'ML') return 'Africa';
//     if ($country === 'MT') return 'Europe';
//     if ($country === 'MH') return 'Oceania';
//     if ($country === 'MQ') return 'North America';
//     if ($country === 'MR') return 'Africa';
//     if ($country === 'MU') return 'Africa';
//     if ($country === 'YT') return 'Africa';
//     if ($country === 'MX') return 'North America';
//     if ($country === 'FM') return 'Oceania';
//     if ($country === 'MD') return 'Europe';
//     if ($country === 'MC') return 'Europe';
//     if ($country === 'MN') return 'Asia';
//     if ($country === 'ME') return 'Europe';
//     if ($country === 'MS') return 'North America';
//     if ($country === 'MA') return 'Africa';
//     if ($country === 'MZ') return 'Africa';
//     if ($country === 'MM') return 'Asia';
//     if ($country === 'NA') return 'Africa';
//     if ($country === 'NR') return 'Oceania';
//     if ($country === 'NP') return 'Asia';
//     if ($country === 'AN') return 'North America';
//     if ($country === 'NL') return 'Europe';
//     if ($country === 'NC') return 'Oceania';
//     if ($country === 'NZ') return 'Oceania';
//     if ($country === 'NI') return 'North America';
//     if ($country === 'NE') return 'Africa';
//     if ($country === 'NG') return 'Africa';
//     if ($country === 'NU') return 'Oceania';
//     if ($country === 'NF') return 'Oceania';
//     if ($country === 'MP') return 'Oceania';
//     if ($country === 'NO') return 'Europe';
//     if ($country === 'OM') return 'Asia';
//     if ($country === 'PK') return 'Asia';
//     if ($country === 'PW') return 'Oceania';
//     if ($country === 'PS') return 'Asia';
//     if ($country === 'PA') return 'North America';
//     if ($country === 'PG') return 'Oceania';
//     if ($country === 'PY') return 'South America';
//     if ($country === 'PE') return 'South America';
//     if ($country === 'PH') return 'Asia';
//     if ($country === 'PN') return 'Oceania';
//     if ($country === 'PL') return 'Europe';
//     if ($country === 'PT') return 'Europe';
//     if ($country === 'PR') return 'North America';
//     if ($country === 'QA') return 'Asia';
//     if ($country === 'RE') return 'Africa';
//     if ($country === 'RO') return 'Europe';
//     if ($country === 'RU') return 'Europe';
//     if ($country === 'RW') return 'Africa';
//     if ($country === 'BL') return 'North America';
//     if ($country === 'SH') return 'Africa';
//     if ($country === 'KN') return 'North America';
//     if ($country === 'LC') return 'North America';
//     if ($country === 'MF') return 'North America';
//     if ($country === 'PM') return 'North America';
//     if ($country === 'VC') return 'North America';
//     if ($country === 'WS') return 'Oceania';
//     if ($country === 'SM') return 'Europe';
//     if ($country === 'ST') return 'Africa';
//     if ($country === 'SA') return 'Asia';
//     if ($country === 'SN') return 'Africa';
//     if ($country === 'RS') return 'Europe';
//     if ($country === 'SC') return 'Africa';
//     if ($country === 'SL') return 'Africa';
//     if ($country === 'SG') return 'Asia';
//     if ($country === 'SK') return 'Europe';
//     if ($country === 'SI') return 'Europe';
//     if ($country === 'SB') return 'Oceania';
//     if ($country === 'SO') return 'Africa';
//     if ($country === 'ZA') return 'Africa';
//     if ($country === 'GS') return 'Antarctica';
//     if ($country === 'ES') return 'Europe';
//     if ($country === 'LK') return 'Asia';
//     if ($country === 'SD') return 'Africa';
//     if ($country === 'SR') return 'South America';
//     if ($country === 'SJ') return 'Europe';
//     if ($country === 'SZ') return 'Africa';
//     if ($country === 'SE') return 'Europe';
//     if ($country === 'CH') return 'Europe';
//     if ($country === 'SY') return 'Asia';
//     if ($country === 'TW') return 'Asia';
//     if ($country === 'TJ') return 'Asia';
//     if ($country === 'TZ') return 'Africa';
//     if ($country === 'TH') return 'Asia';
//     if ($country === 'TL') return 'Asia';
//     if ($country === 'TG') return 'Africa';
//     if ($country === 'TK') return 'Oceania';
//     if ($country === 'TO') return 'Oceania';
//     if ($country === 'TT') return 'North America';
//     if ($country === 'TN') return 'Africa';
//     if ($country === 'TR') return 'Asia';
//     if ($country === 'TM') return 'Asia';
//     if ($country === 'TC') return 'North America';
//     if ($country === 'TV') return 'Oceania';
//     if ($country === 'UG') return 'Africa';
//     if ($country === 'UA') return 'Europe';
//     if ($country === 'AE') return 'Asia';
//     if ($country === 'GB') return 'Europe';
//     if ($country === 'US') return 'North America';
//     if ($country === 'UM') return 'Oceania';
//     if ($country === 'VI') return 'North America';
//     if ($country === 'UY') return 'South America';
//     if ($country === 'UZ') return 'Asia';
//     if ($country === 'VU') return 'Oceania';
//     if ($country === 'VE') return 'South America';
//     if ($country === 'VN') return 'Asia';
//     if ($country === 'WF') return 'Oceania';
//     if ($country === 'EH') return 'Africa';
//     if ($country === 'YE') return 'Asia';
//     if ($country === 'ZM') return 'Africa';
//     if ($country === 'ZW') return 'Africa';
// }
